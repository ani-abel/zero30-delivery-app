(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["merchant-products-merchant-products-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/user/merchant-products/merchant-products.page.html":
    /*!**********************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/user/merchant-products/merchant-products.page.html ***!
      \**********************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppUserMerchantProductsMerchantProductsPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n  <main id=\"business-details-page\" class=\"bg-offwhite container with-bottom-menu bookmarked\">\n\t\n    <!-- So this <section id=\"header\"> has a backgroud image, I don't know if it's meant to be dynamic but I've left a link to the image in the inline style of the section.  -->\n    <section id=\"header\" class=\"constrain header-transparent-image\"\n      style=\"background: linear-gradient(0deg, rgba(34, 43, 69, 0.7), rgba(34, 43, 69, 0.7)), url(assets/images/banner/banner2.png);\">\n      \n      <div class=\"header-actions\">\n        <a [routerLink]=\"['/user']\" class=\"back link\">\n          <img class=\"svg\" src=\"assets/images/icons/arrow-left.svg\" width=\"18px\" alt=\"Go back\">\n        </a>\n        <!-- Bookmark icon -->\n        <button type=\"button\" class=\"btn bookmark\" (click)=\"addMerchantToBookmark()\">\n          <img class=\"svg\" \n            src=\"assets/images/icons/bookmark.svg\" \n            height=\"18px\" alt=\"Bookmark\" />\n        </button>\n        <button type=\"button\" class=\"link options\">\n          <img class=\"svg\" src=\"assets/images/icons/options.svg\" width=\"18px\" alt=\"Options\">\n        </button>\n      </div>\n  \n      <!-- This shows the name and address of the restaurant -->\n      <div class=\"text\" *ngIf=\"(selectedMerchant$ | async) as merchant\">\n        <h2 class=\"name\">{{ merchant?.companyName | uppercase }}</h2>\n        <p class=\"address\">\n          <img class=\"svg\" src=\"assets/images/icons/map-pointer.svg\" height=\"10px\" alt=\"Store\">\n          <span>{{ merchant?.address | uppercase }}</span>\n        </p>\n        <p class=\"reviews\">\n          <img class=\"svg icon\" alt=\"\" src=\"assets/images/icons/star.svg\">\n          <b>{{ merchant?.rate | number : '1.1-3' }}</b>\n          <span>({{ merchant?.ratingCount || 0 }} Reviews)</span>\n        </p>\n      </div>\n    </section>\n  \n    <!-- This is that menu at the bottom of every page -->\n    <app-user-bottom-navbar></app-user-bottom-navbar>\n  \n    <!-- Tab content -->\n    <!-- This is the Products tab -->\n    <section id=\"products\" class=\"tab-content active\">\n  \n      <div class=\"product-categories-container\" *ngIf=\"(productCategoryGroup$ | async) as productCategoryGroups\">\n        <div id=\"product-categories\" *ngFor=\"let productCategoryGroup of productCategoryGroups\">\n          <div class=\"section-title constrain\">\n            <h5>{{ productCategoryGroup.productCategoryModel?.name | uppercase }} \n              <span>{{ productCategoryGroup?.productDetailModel.length || 0 }} items</span>\n            </h5>\n          </div>\n    \n          <!-- \n            There's a \".bookmarked\" class on the products that are bookmarked by the user, Once the .bookmarked tag is added as a class it styles the bookmark icon.\n          -->\n         <app-product-category-widget\n          (viewProductDetailEvent)=\"openProductDetail($event, product)\"\n          (addProductToBookmarkEvent)=\"addProductToBoomkark($event)\"\n          [product]=\"product\" \n          *ngFor=\"let product of productCategoryGroup?.productDetailModel\">\n        </app-product-category-widget>\n        </div>\n      </div>\n      \n    </section>\n  \n    <!-- \n      This is the popup that comes up when you click the add to cart button (the plus icon beside every product)\n    -->\n    <section id=\"addtocart-popup\" class=\"popup\">\n      \n      <div class=\"popup-content\">\n  \n        <!-- This button closes the popup, it has been implemented. -->\n        <button type=\"button\" class=\"close-popup\">\n          <img class=\"svg\" src=\"assets/images/icons/close-popup.svg\" height=\"10px\" alt=\"Close Popup\">\n        </button>\n  \n        <h5 class=\"title constrain\">Add New Item</h5>\n  \n        <form action=\"\">\n        \n          <div class=\"product-details constrain\">\n  \n            <a href=\"#\" class=\"image\">\n              <img src=\"assets/images/products/4.png\" height=\"120px\" alt=\"Fried Noodles Salad and Chicken Wings\">\n            </a>\n  \n            <div class=\"details\">\n  \n              <a href=\"#\" class=\"name\">Lemon Juice Fresh</a>\n  \n              <p class=\"bag\">\n                <img class=\"svg icon\" alt=\"\" src=\"assets/images/icons/bag.svg\">\n                <span>99+</span>\n              </p>\n  \n              <span class=\"line\"></span>\n  \n              <p class=\"like\">\n                <img class=\"svg icon\" alt=\"\" src=\"assets/images/icons/like.svg\">\n                <span>26</span>\n              </p>\n  \n              <h5 class=\"price\">₦450</h5>\n  \n              <!-- \n                This plus and minus works\n              -->\n              <div class=\"quantity\">\n                <button type=\"button\" class=\"minus btn\"><img class=\"svg icon\" alt=\"\" src=\"assets/images/icons/minus.svg\" height=\"30px\"></button>\n                <span class=\"value\">0</span>\n                <button type=\"button\" class=\"plus btn\"><img class=\"svg icon\" alt=\"\" src=\"assets/images/icons/plus.svg\" height=\"30px\"></button>\n              </div>\n  \n            </div>\n          </div>\n  \n          <div class=\"attribute-title\">\n            <div class=\"h-grid constrain\">\n              <p>Size</p>\n              <p>Price</p>\n            </div>\n          </div>\n  \n          <div class=\"attributes constrain\">\n            <div class=\"h-grid\">\n              <div class=\"checkbox\">\n                <input id=\"size-s\" type=\"checkbox\">\n                <span class=\"check\"></span>\n                <label for=\"size-s\">Size S <span>(500 ml)</span></label>\n              </div>\n  \n              <p class=\"price\">₦0</p>\n            </div>\n  \n            <div class=\"h-grid\">\n              <div class=\"checkbox\">\n                <input id=\"size-m\" type=\"checkbox\">\n                <span class=\"check\"></span>\n                <label for=\"size-m\">Size M <span>(750 ml)</span></label>\n              </div>\n  \n              <p class=\"price\">₦50</p>\n            </div>\n  \n            <div class=\"h-grid\">\n              <div class=\"checkbox\">\n                <input id=\"size-l\" type=\"checkbox\">\n                <span class=\"check\"></span>\n                <label for=\"size-l\">Size L <span>(1100 ml)</span></label>\n              </div>\n  \n              <p class=\"price\">₦130</p>\n            </div>\n  \n          </div>\n  \n          <div class=\"attribute-title\">\n            <div class=\"h-grid constrain\">\n              <p>Size</p>\n            </div>\n          </div>\n  \n          <div class=\"attributes constrain\">\n            <div class=\"h-grid\">\n              <div class=\"checkbox\">\n                <input id=\"lemon\" type=\"checkbox\">\n                <span class=\"check\"></span>\n                <label for=\"lemon\">Add Lemon</label>\n              </div>\n            </div>\n  \n            <div class=\"h-grid\">\n              <div class=\"checkbox\">\n                <input id=\"ice\" type=\"checkbox\">\n                <span class=\"check\"></span>\n                <label for=\"ice\">Add Ice</label>\n              </div>\n            </div>\n          </div>\n  \n          <div class=\"note\">\n            <div class=\"title\">\n              <div class=\"h-grid constrain\">\n                <p>Note</p>\n              </div>\n            </div>\n  \n            <div class=\"constrain\">\n              <textarea name=\"note\" placeholder=\"Leave a note\" id=\"note\"></textarea>\n            </div>\n          </div>\n  \n          <div class=\"constrain\">\n            <!-- This button submits this form. -->\n            <button type=\"submit\" class=\"big-cart-btn btn btn-primary\">\n              <p class=\"items\">1 item</p>\n              <p class=\"price\">₦4,150</p>\n              <p class=\"text\">Add to Cart</p>\n            </button>\n          </div>\n  \n        </form>\n      </div>\n      <!-- <div class=\"popup-overlay\"></div> -->\n    </section>\n    \n\n    <app-footer-expander></app-footer-expander>\n    </main>\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/user/merchant-products/merchant-products-routing.module.ts":
    /*!****************************************************************************!*\
      !*** ./src/app/user/merchant-products/merchant-products-routing.module.ts ***!
      \****************************************************************************/

    /*! exports provided: MerchantProductsPageRoutingModule */

    /***/
    function srcAppUserMerchantProductsMerchantProductsRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MerchantProductsPageRoutingModule", function () {
        return MerchantProductsPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _merchant_products_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./merchant-products.page */
      "./src/app/user/merchant-products/merchant-products.page.ts");

      var routes = [{
        path: '',
        component: _merchant_products_page__WEBPACK_IMPORTED_MODULE_3__["MerchantProductsPage"]
      }];

      var MerchantProductsPageRoutingModule = function MerchantProductsPageRoutingModule() {
        _classCallCheck(this, MerchantProductsPageRoutingModule);
      };

      MerchantProductsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], MerchantProductsPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/user/merchant-products/merchant-products.module.ts":
    /*!********************************************************************!*\
      !*** ./src/app/user/merchant-products/merchant-products.module.ts ***!
      \********************************************************************/

    /*! exports provided: MerchantProductsPageModule */

    /***/
    function srcAppUserMerchantProductsMerchantProductsModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MerchantProductsPageModule", function () {
        return MerchantProductsPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _merchant_products_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./merchant-products-routing.module */
      "./src/app/user/merchant-products/merchant-products-routing.module.ts");
      /* harmony import */


      var _merchant_products_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./merchant-products.page */
      "./src/app/user/merchant-products/merchant-products.page.ts");
      /* harmony import */


      var _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../../shared/shared.module */
      "./src/app/shared/shared.module.ts");

      var MerchantProductsPageModule = function MerchantProductsPageModule() {
        _classCallCheck(this, MerchantProductsPageModule);
      };

      MerchantProductsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"], _merchant_products_routing_module__WEBPACK_IMPORTED_MODULE_5__["MerchantProductsPageRoutingModule"]],
        declarations: [_merchant_products_page__WEBPACK_IMPORTED_MODULE_6__["MerchantProductsPage"]]
      })], MerchantProductsPageModule);
      /***/
    },

    /***/
    "./src/app/user/merchant-products/merchant-products.page.scss":
    /*!********************************************************************!*\
      !*** ./src/app/user/merchant-products/merchant-products.page.scss ***!
      \********************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppUserMerchantProductsMerchantProductsPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXIvbWVyY2hhbnQtcHJvZHVjdHMvbWVyY2hhbnQtcHJvZHVjdHMucGFnZS5zY3NzIn0= */";
      /***/
    },

    /***/
    "./src/app/user/merchant-products/merchant-products.page.ts":
    /*!******************************************************************!*\
      !*** ./src/app/user/merchant-products/merchant-products.page.ts ***!
      \******************************************************************/

    /*! exports provided: MerchantProductsPage */

    /***/
    function srcAppUserMerchantProductsMerchantProductsPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MerchantProductsPage", function () {
        return MerchantProductsPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _ngrx_store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ngrx/store */
      "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");
      /* harmony import */


      var subsink__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! subsink */
      "./node_modules/subsink/dist/es2015/index.js");
      /* harmony import */


      var _user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ../../user/store/actions/user.action */
      "./src/app/user/store/actions/user.action.ts");
      /* harmony import */


      var _utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../../../utils/functions/app.functions */
      "./src/utils/functions/app.functions.ts");
      /* harmony import */


      var _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ../../../utils/types/app.constant */
      "./src/utils/types/app.constant.ts");

      var MerchantProductsPage = /*#__PURE__*/function () {
        function MerchantProductsPage(activatedRoute, router, store, modalController) {
          _classCallCheck(this, MerchantProductsPage);

          this.activatedRoute = activatedRoute;
          this.router = router;
          this.store = store;
          this.modalController = modalController;
          this.subSink = new subsink__WEBPACK_IMPORTED_MODULE_5__["SubSink"]();
        }

        _createClass(MerchantProductsPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var _this = this;

              var _yield$Object, userId;

              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_7__["getDataFromLocalStorage"])(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_8__["LocalStorageKey"].ZERO_30_USER);

                    case 2:
                      _yield$Object = _context.sent;
                      userId = _yield$Object.userId;
                      this.userId = userId;
                      this.subSink.sink = this.activatedRoute.params.subscribe(function (data) {
                        _this.merchantId = data.merchantId;

                        if (!(data === null || data === void 0 ? void 0 : data.merchantId)) {
                          //? Navigate away if merchantId is not found
                          _this.router.navigate(["/user", "nearby"]);
                        }
                      });
                      this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_6__["actions"].GetMerchantDetailInitiatedAction({
                        payload: {
                          typeId: this.merchantId
                        }
                      }));
                      this.selectedMerchant$ = this.store.select(function (data) {
                        return data.User.SelectedMerchant;
                      }); //? Get merchant's products grouped by categories

                      this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_6__["actions"].GetMechantProductsGroupedByCategoryInitiatedAction({
                        payload: {
                          merchantId: this.merchantId
                        }
                      }));
                      this.productCategoryGroup$ = this.store.select(function (data) {
                        return data.User.ProductCategoryGroups;
                      });
                      setTimeout(function () {
                        _this.subSink.sink = _this.selectedMerchant$.subscribe(function (data) {
                          if (data === null || data === void 0 ? void 0 : data.id) {
                            _this.selectedMerchantDataSync = data;
                          } else {
                            //? Navigate away if merchant is not found
                            _this.router.navigate(["/user", "nearby"]);
                          }
                        });
                      }, 2000);

                    case 11:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          } // async openProductDetail(event: any, product: ProductDetailType): Promise<void> {
          //   if(product?.productModel) {
          //     const modal = await this.modalController.create({
          //       component: ProductDetailModalComponent,
          //       componentProps: {
          //         "product": product
          //       }
          //     });
          //     return await modal.present();
          //   }
          // }

        }, {
          key: "openProductDetail",
          value: function openProductDetail(event, product) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      if (product === null || product === void 0 ? void 0 : product.productModel) {
                        this.router.navigateByUrl("/user/product-detail?product=".concat(JSON.stringify(product)));
                      }

                    case 1:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }, {
          key: "addMerchantToBookmark",
          value: function addMerchantToBookmark() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
              var _this2 = this;

              return regeneratorRuntime.wrap(function _callee4$(_context4) {
                while (1) {
                  switch (_context4.prev = _context4.next) {
                    case 0:
                      this.subSink.sink = this.selectedMerchant$.subscribe(function (merchant) {
                        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
                          return regeneratorRuntime.wrap(function _callee3$(_context3) {
                            while (1) {
                              switch (_context3.prev = _context3.next) {
                                case 0:
                                  //? Get the user Id
                                  if (this.userId) {
                                    //? Dispatch action
                                    this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_6__["actions"].AddMerchantToUserBookmarksInitiatedAction({
                                      payload: {
                                        MerchantId: merchant.id,
                                        UserId: this.userId,
                                        Merchant: merchant
                                      }
                                    }));
                                  }

                                case 1:
                                case "end":
                                  return _context3.stop();
                              }
                            }
                          }, _callee3, this);
                        }));
                      });

                    case 1:
                    case "end":
                      return _context4.stop();
                  }
                }
              }, _callee4, this);
            }));
          }
        }, {
          key: "addProductToBoomkark",
          value: function addProductToBoomkark(event) {
            if (this.userId) {
              this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_6__["actions"].AddProductToBookmarkInitiatedAction({
                payload: {
                  productId: event,
                  userId: this.userId
                }
              }));
            }
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            this.subSink.unsubscribe();
          }
        }]);

        return MerchantProductsPage;
      }();

      MerchantProductsPage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: _ngrx_store__WEBPACK_IMPORTED_MODULE_4__["Store"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"]
        }];
      };

      MerchantProductsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-merchant-products',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./merchant-products.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/user/merchant-products/merchant-products.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./merchant-products.page.scss */
        "./src/app/user/merchant-products/merchant-products.page.scss"))["default"]]
      })], MerchantProductsPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=merchant-products-merchant-products-module-es5.js.map