(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["category-list-category-list-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/user/category-list/category-list.page.html":
    /*!**************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/user/category-list/category-list.page.html ***!
      \**************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppUserCategoryListCategoryListPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n  <main id=\"nearby-page\" class=\"container with-bottom-menu bg-offwhite\">\n    <section class=\"header-area\">\n      <div class=\"header-section\">\n         <a [routerLink]=\"['/user', 'discover-items']\" class=\"back link\">\n          <img class=\"svg\" src=\"assets/images/icons/arrow-left.svg\" width=\"18px\" alt=\"Go back\">\n        </a>\n      </div>\n      <div class=\"header-section\">\n        <div class=\"text\">\n          <h2 class=\"name\">Categories</h2>\n        </div>\n      </div>\n    </section>\n  \n    <!-- This is that menu at the bottom of every page -->\n    <app-user-bottom-navbar></app-user-bottom-navbar>\n\n    <!-- Tab content -->\n    <!-- This is the Food tab -->\n    <section id=\"food\" \n      class=\"tab-content nearby-businesses active\">\n      <div class=\"businesses constrain\">\n  \n        <!-- \n          There's a \".bookmarked\" class on the products that are bookmarked by the user, Once the .bookmarked tag is added as a class it styles the bookmark icon on top of the product image.\n        -->\n        <app-category-item-widget \n          [productCategory]=\"category\" \n          *ngFor=\"let category of (productsByMerchant$ | async)?.productProductCategoryIndexModels\">\n        </app-category-item-widget>\n\n        <app-footer-expander></app-footer-expander>\n      </div>\n    </section>\n  </main>\n\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/user/category-list/category-list-routing.module.ts":
    /*!********************************************************************!*\
      !*** ./src/app/user/category-list/category-list-routing.module.ts ***!
      \********************************************************************/

    /*! exports provided: CategoryListPageRoutingModule */

    /***/
    function srcAppUserCategoryListCategoryListRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CategoryListPageRoutingModule", function () {
        return CategoryListPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _category_list_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./category-list.page */
      "./src/app/user/category-list/category-list.page.ts");

      var routes = [{
        path: '',
        component: _category_list_page__WEBPACK_IMPORTED_MODULE_3__["CategoryListPage"]
      }];

      var CategoryListPageRoutingModule = function CategoryListPageRoutingModule() {
        _classCallCheck(this, CategoryListPageRoutingModule);
      };

      CategoryListPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], CategoryListPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/user/category-list/category-list.module.ts":
    /*!************************************************************!*\
      !*** ./src/app/user/category-list/category-list.module.ts ***!
      \************************************************************/

    /*! exports provided: CategoryListPageModule */

    /***/
    function srcAppUserCategoryListCategoryListModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CategoryListPageModule", function () {
        return CategoryListPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _category_list_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./category-list-routing.module */
      "./src/app/user/category-list/category-list-routing.module.ts");
      /* harmony import */


      var _category_list_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./category-list.page */
      "./src/app/user/category-list/category-list.page.ts");
      /* harmony import */


      var _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../../shared/shared.module */
      "./src/app/shared/shared.module.ts");

      var CategoryListPageModule = function CategoryListPageModule() {
        _classCallCheck(this, CategoryListPageModule);
      };

      CategoryListPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"], _category_list_routing_module__WEBPACK_IMPORTED_MODULE_5__["CategoryListPageRoutingModule"]],
        declarations: [_category_list_page__WEBPACK_IMPORTED_MODULE_6__["CategoryListPage"]]
      })], CategoryListPageModule);
      /***/
    },

    /***/
    "./src/app/user/category-list/category-list.page.scss":
    /*!************************************************************!*\
      !*** ./src/app/user/category-list/category-list.page.scss ***!
      \************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppUserCategoryListCategoryListPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXIvY2F0ZWdvcnktbGlzdC9jYXRlZ29yeS1saXN0LnBhZ2Uuc2NzcyJ9 */";
      /***/
    },

    /***/
    "./src/app/user/category-list/category-list.page.ts":
    /*!**********************************************************!*\
      !*** ./src/app/user/category-list/category-list.page.ts ***!
      \**********************************************************/

    /*! exports provided: CategoryListPage */

    /***/
    function srcAppUserCategoryListCategoryListPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CategoryListPage", function () {
        return CategoryListPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ngrx_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ngrx/store */
      "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");
      /* harmony import */


      var src_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/utils/functions/app.functions */
      "./src/utils/functions/app.functions.ts");
      /* harmony import */


      var src_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! src/utils/types/app.constant */
      "./src/utils/types/app.constant.ts");
      /* harmony import */


      var _user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../../user/store/actions/user.action */
      "./src/app/user/store/actions/user.action.ts");

      var CategoryListPage = /*#__PURE__*/function () {
        function CategoryListPage(store) {
          _classCallCheck(this, CategoryListPage);

          this.store = store;
        }

        _createClass(CategoryListPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var authData;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return Object(src_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_3__["getDataFromLocalStorage"])(src_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_4__["LocalStorageKey"].ZERO_30_USER);

                    case 2:
                      authData = _context.sent;

                      if (authData === null || authData === void 0 ? void 0 : authData.userId) {
                        this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetProductsByMerchantInitiatedAction({
                          payload: {
                            userId: authData.userId
                          }
                        }));
                        this.productsByMerchant$ = this.store.select(function (data) {
                          return data.User.ProductsByMerchants;
                        });
                      }

                    case 4:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }]);

        return CategoryListPage;
      }();

      CategoryListPage.ctorParameters = function () {
        return [{
          type: _ngrx_store__WEBPACK_IMPORTED_MODULE_2__["Store"]
        }];
      };

      CategoryListPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-category-list',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./category-list.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/user/category-list/category-list.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./category-list.page.scss */
        "./src/app/user/category-list/category-list.page.scss"))["default"]]
      })], CategoryListPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=category-list-category-list-module-es5.js.map