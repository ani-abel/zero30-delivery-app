(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["favourites-favourites-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/user/favourites/favourites.page.html":
    /*!********************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/user/favourites/favourites.page.html ***!
      \********************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppUserFavouritesFavouritesPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n    <main id=\"favourites-page\" class=\"container with-bottom-menu bg-offwhite\">\n        <section id=\"header\" class=\"constrain header-white\">\n            <div class=\"text\">\n                <h2 class=\"name\">Favourites</h2>\n            </div>\n        </section>\n        \n        <!-- This is that menu at the bottom of every page -->\n        <app-user-bottom-navbar></app-user-bottom-navbar>\n    \n        <!-- \n            This is the tab buttons.\n            When you click the buttons they activate the selected tab, clicking it also adds a \".active\" class to both the button of the tab and the tab content.\n            I have already implemented it using javascript.\n        -->\n        <section id=\"tabs\" class=\"constrain\">\n            <button class=\"tab\" \n                [ngClass]=\"{ 'active': activeTabType === 'PRODUCT' }\" \n                (click)=\"switchTabs('PRODUCT')\">\n                <span>Products</span>\n            </button>\n            <button class=\"tab\" \n                [ngClass]=\"{ 'active': activeTabType === 'MERCHANT' }\" \n                (click)=\"switchTabs('MERCHANT')\">\n                <span>Merchants</span>\n            </button>\n        </section>\n    \n        <!-- Tab content -->\n        <!-- This is the Foods tab -->\n        <section id=\"foods\" class=\"tab-content category active\" *ngIf=\"activeTabType === 'PRODUCT'\">\n            <div class=\"favourites constrain\" *ngIf=\"(favouriteProducts$ | async)?.length > 0; else defaultTemplate\">\n                <app-favorite-food-widget \n                    (viewProductDetailEvent)=\"openProductDetail($event, product)\"\n                    [product]=\"product\"\n                    *ngFor=\"let product of (favouriteProducts$ | async)\">\n                </app-favorite-food-widget>\n            </div>\n        </section>\n    \n        <!-- Tab content -->\n        <!-- This is the Restaurants tab -->\n        <section id=\"restaurants\" class=\"tab-content category active\" *ngIf=\"activeTabType === 'MERCHANT'\">\n            <div class=\"favourites constrain\" *ngIf=\"(favouriteMerchants$ | async)?.length > 0; else defaultTemplate\">\n                <app-favorite-resturant-widget \n                    *ngFor=\"let merchant of (favouriteMerchants$ | async); index as i\"\n                    [merchant]=\"merchant\"\n                    (addToBookmark)=\"addMerchantToBookmark($event)\">\n                </app-favorite-resturant-widget>\n            </div>\n        </section>\n\n        <ng-template #defaultTemplate>\n            <app-no-content-found></app-no-content-found>\n        </ng-template>\n\n        <app-footer-expander></app-footer-expander>\n    </main>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/user/favourites/favourites-routing.module.ts":
    /*!**************************************************************!*\
      !*** ./src/app/user/favourites/favourites-routing.module.ts ***!
      \**************************************************************/

    /*! exports provided: FavouritesPageRoutingModule */

    /***/
    function srcAppUserFavouritesFavouritesRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "FavouritesPageRoutingModule", function () {
        return FavouritesPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _favourites_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./favourites.page */
      "./src/app/user/favourites/favourites.page.ts");

      var routes = [{
        path: '',
        component: _favourites_page__WEBPACK_IMPORTED_MODULE_3__["FavouritesPage"]
      }];

      var FavouritesPageRoutingModule = function FavouritesPageRoutingModule() {
        _classCallCheck(this, FavouritesPageRoutingModule);
      };

      FavouritesPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], FavouritesPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/user/favourites/favourites.module.ts":
    /*!******************************************************!*\
      !*** ./src/app/user/favourites/favourites.module.ts ***!
      \******************************************************/

    /*! exports provided: FavouritesPageModule */

    /***/
    function srcAppUserFavouritesFavouritesModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "FavouritesPageModule", function () {
        return FavouritesPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _favourites_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./favourites-routing.module */
      "./src/app/user/favourites/favourites-routing.module.ts");
      /* harmony import */


      var _favourites_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./favourites.page */
      "./src/app/user/favourites/favourites.page.ts");
      /* harmony import */


      var _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../../shared/shared.module */
      "./src/app/shared/shared.module.ts");

      var FavouritesPageModule = function FavouritesPageModule() {
        _classCallCheck(this, FavouritesPageModule);
      };

      FavouritesPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _favourites_routing_module__WEBPACK_IMPORTED_MODULE_5__["FavouritesPageRoutingModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"]],
        declarations: [_favourites_page__WEBPACK_IMPORTED_MODULE_6__["FavouritesPage"]]
      })], FavouritesPageModule);
      /***/
    },

    /***/
    "./src/app/user/favourites/favourites.page.scss":
    /*!******************************************************!*\
      !*** ./src/app/user/favourites/favourites.page.scss ***!
      \******************************************************/

    /*! exports provided: default */

    /***/
    function srcAppUserFavouritesFavouritesPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXIvZmF2b3VyaXRlcy9mYXZvdXJpdGVzLnBhZ2Uuc2NzcyJ9 */";
      /***/
    },

    /***/
    "./src/app/user/favourites/favourites.page.ts":
    /*!****************************************************!*\
      !*** ./src/app/user/favourites/favourites.page.ts ***!
      \****************************************************/

    /*! exports provided: TabType, FavouritesPage */

    /***/
    function srcAppUserFavouritesFavouritesPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TabType", function () {
        return TabType;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "FavouritesPage", function () {
        return FavouritesPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ngrx_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ngrx/store */
      "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");
      /* harmony import */


      var subsink__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! subsink */
      "./node_modules/subsink/dist/es2015/index.js");
      /* harmony import */


      var _user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../../user/store/actions/user.action */
      "./src/app/user/store/actions/user.action.ts");
      /* harmony import */


      var _utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../../../utils/functions/app.functions */
      "./src/utils/functions/app.functions.ts");
      /* harmony import */


      var _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ../../../utils/types/app.constant */
      "./src/utils/types/app.constant.ts");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");

      var TabType;

      (function (TabType) {
        TabType["PRODUCT"] = "PRODUCT";
        TabType["MERCHANT"] = "MERCHANT";
      })(TabType || (TabType = {}));

      var FavouritesPage = /*#__PURE__*/function () {
        function FavouritesPage(store, router) {
          _classCallCheck(this, FavouritesPage);

          this.store = store;
          this.router = router;
          this.activeTabType = TabType.PRODUCT;
          this.subSink = new subsink__WEBPACK_IMPORTED_MODULE_3__["SubSink"]();
        }

        _createClass(FavouritesPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var _this = this;

              var _yield$Object, userId;

              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      // ? Check for favourites
                      this.favouriteMerchants$ = this.store.select(function (data) {
                        return data.User.FavouriteMerchants;
                      });
                      this.favouriteProducts$ = this.store.select(function (data) {
                        return data.User.FavouriteProducts;
                      });
                      this.subSink.sink = this.favouriteMerchants$.subscribe(function (data) {
                        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                          return regeneratorRuntime.wrap(function _callee$(_context) {
                            while (1) {
                              switch (_context.prev = _context.next) {
                                case 0:
                                  if ((data === null || data === void 0 ? void 0 : data.length) < 1) {
                                    this.isInAppStore = true;
                                  }

                                case 1:
                                case "end":
                                  return _context.stop();
                              }
                            }
                          }, _callee, this);
                        }));
                      });

                      if (!this.isInAppStore) {
                        _context2.next = 9;
                        break;
                      }

                      _context2.next = 6;
                      return Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_5__["getDataFromLocalStorage"])(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_6__["LocalStorageKey"].ZERO_30_USER);

                    case 6:
                      _yield$Object = _context2.sent;
                      userId = _yield$Object.userId;

                      if (userId) {
                        this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_4__["actions"].GetUserFavouritesInitiatedAction({
                          payload: {
                            userId: userId
                          }
                        }));
                      }

                    case 9:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }, {
          key: "openProductDetail",
          value: function openProductDetail(event, product) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                      if (product === null || product === void 0 ? void 0 : product.productModel) {
                        this.router.navigateByUrl("/user/product-detail?product=".concat(JSON.stringify(product)));
                      }

                    case 1:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3, this);
            }));
          }
        }, {
          key: "switchTabs",
          value: function switchTabs(tab) {
            this.activeTabType = tab;
          }
        }, {
          key: "addMerchantToBookmark",
          value: function addMerchantToBookmark(event) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
              var _yield$Object2, userId;

              return regeneratorRuntime.wrap(function _callee4$(_context4) {
                while (1) {
                  switch (_context4.prev = _context4.next) {
                    case 0:
                      _context4.next = 2;
                      return Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_5__["getDataFromLocalStorage"])(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_6__["LocalStorageKey"].ZERO_30_USER);

                    case 2:
                      _yield$Object2 = _context4.sent;
                      userId = _yield$Object2.userId;

                      if (userId) {
                        //? Dispatch action
                        this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_4__["actions"].AddMerchantToUserBookmarksInitiatedAction({
                          payload: {
                            MerchantId: event.id,
                            UserId: userId,
                            Merchant: event
                          }
                        }));
                      }

                    case 5:
                    case "end":
                      return _context4.stop();
                  }
                }
              }, _callee4, this);
            }));
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            this.subSink.unsubscribe();
          }
        }]);

        return FavouritesPage;
      }();

      FavouritesPage.ctorParameters = function () {
        return [{
          type: _ngrx_store__WEBPACK_IMPORTED_MODULE_2__["Store"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"]
        }];
      };

      FavouritesPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-favourites',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./favourites.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/user/favourites/favourites.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./favourites.page.scss */
        "./src/app/user/favourites/favourites.page.scss"))["default"]]
      })], FavouritesPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=favourites-favourites-module-es5.js.map