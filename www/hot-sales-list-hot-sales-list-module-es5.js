(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["hot-sales-list-hot-sales-list-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/user/hot-sales-list/hot-sales-list.page.html":
    /*!****************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/user/hot-sales-list/hot-sales-list.page.html ***!
      \****************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppUserHotSalesListHotSalesListPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n  <main id=\"search-results-page\" class=\"container with-bottom-menu bg-offwhite\">\n    <section class=\"header-area\">\n      <div class=\"header-section\">\n         <a [routerLink]=\"['/user', 'discover-items']\" class=\"back link\">\n          <img class=\"svg\" src=\"assets/images/icons/arrow-left.svg\" width=\"18px\" alt=\"Go back\">\n        </a>\n      </div>\n      <div class=\"header-section\">\n        <div class=\"text\">\n          <h2 class=\"name\">Hot Sale</h2>\n        </div>\n      </div>\n    </section>\n  \n    <!-- This is that menu at the bottom of every page -->\n    <app-user-bottom-navbar></app-user-bottom-navbar>\n\n    <section id=\"search-results\" \n      class=\"constrain\" \n      *ngIf=\"(productsOnHotSale$ | async) as productsOnHotSale\">\n      <!-- \n        There's a \".bookmarked\" class on the search results that are bookmarked by the user, Once the .bookmarked tag is added as a class it styles the bookmark icon on top of the result's logo.\n      -->\n      <app-search-results-widget \n        (addProductToBookmarkType)=\"addProductToBookmark($event, product)\"\n        (viewProductDetailEvent)=\"openProductDetail($event, product)\"\n        *ngFor=\"let product of productsOnHotSale\" \n        [searchResult]=\"product\"></app-search-results-widget>\n    </section>\n\n    <app-footer-expander></app-footer-expander>\n  </main>\n\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/user/hot-sales-list/hot-sales-list-routing.module.ts":
    /*!**********************************************************************!*\
      !*** ./src/app/user/hot-sales-list/hot-sales-list-routing.module.ts ***!
      \**********************************************************************/

    /*! exports provided: HotSalesListPageRoutingModule */

    /***/
    function srcAppUserHotSalesListHotSalesListRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HotSalesListPageRoutingModule", function () {
        return HotSalesListPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _hot_sales_list_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./hot-sales-list.page */
      "./src/app/user/hot-sales-list/hot-sales-list.page.ts");

      var routes = [{
        path: '',
        component: _hot_sales_list_page__WEBPACK_IMPORTED_MODULE_3__["HotSalesListPage"]
      }];

      var HotSalesListPageRoutingModule = function HotSalesListPageRoutingModule() {
        _classCallCheck(this, HotSalesListPageRoutingModule);
      };

      HotSalesListPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], HotSalesListPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/user/hot-sales-list/hot-sales-list.module.ts":
    /*!**************************************************************!*\
      !*** ./src/app/user/hot-sales-list/hot-sales-list.module.ts ***!
      \**************************************************************/

    /*! exports provided: HotSalesListPageModule */

    /***/
    function srcAppUserHotSalesListHotSalesListModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HotSalesListPageModule", function () {
        return HotSalesListPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _hot_sales_list_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./hot-sales-list-routing.module */
      "./src/app/user/hot-sales-list/hot-sales-list-routing.module.ts");
      /* harmony import */


      var _hot_sales_list_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./hot-sales-list.page */
      "./src/app/user/hot-sales-list/hot-sales-list.page.ts");
      /* harmony import */


      var src_app_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/shared/shared.module */
      "./src/app/shared/shared.module.ts");

      var HotSalesListPageModule = function HotSalesListPageModule() {
        _classCallCheck(this, HotSalesListPageModule);
      };

      HotSalesListPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], src_app_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"], _hot_sales_list_routing_module__WEBPACK_IMPORTED_MODULE_5__["HotSalesListPageRoutingModule"]],
        declarations: [_hot_sales_list_page__WEBPACK_IMPORTED_MODULE_6__["HotSalesListPage"]]
      })], HotSalesListPageModule);
      /***/
    },

    /***/
    "./src/app/user/hot-sales-list/hot-sales-list.page.scss":
    /*!**************************************************************!*\
      !*** ./src/app/user/hot-sales-list/hot-sales-list.page.scss ***!
      \**************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppUserHotSalesListHotSalesListPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "#search-results {\n  margin-top: 20px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXNlci9ob3Qtc2FsZXMtbGlzdC9ob3Qtc2FsZXMtbGlzdC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxnQkFBQTtBQUNKIiwiZmlsZSI6InNyYy9hcHAvdXNlci9ob3Qtc2FsZXMtbGlzdC9ob3Qtc2FsZXMtbGlzdC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjc2VhcmNoLXJlc3VsdHMge1xuICAgIG1hcmdpbi10b3A6IDIwcHg7XG59Il19 */";
      /***/
    },

    /***/
    "./src/app/user/hot-sales-list/hot-sales-list.page.ts":
    /*!************************************************************!*\
      !*** ./src/app/user/hot-sales-list/hot-sales-list.page.ts ***!
      \************************************************************/

    /*! exports provided: HotSalesListPage */

    /***/
    function srcAppUserHotSalesListHotSalesListPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HotSalesListPage", function () {
        return HotSalesListPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _ngrx_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ngrx/store */
      "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");
      /* harmony import */


      var _user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../../user/store/actions/user.action */
      "./src/app/user/store/actions/user.action.ts");
      /* harmony import */


      var _utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../../../utils/functions/app.functions */
      "./src/utils/functions/app.functions.ts");
      /* harmony import */


      var _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ../../../utils/types/app.constant */
      "./src/utils/types/app.constant.ts");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");

      var HotSalesListPage = /*#__PURE__*/function () {
        function HotSalesListPage(store, router, modalController) {
          _classCallCheck(this, HotSalesListPage);

          this.store = store;
          this.router = router;
          this.modalController = modalController;
        }

        _createClass(HotSalesListPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_4__["actions"].GetProductsOnHotSaleInitiatedAction());
            this.productsOnHotSale$ = this.store.select(function (data) {
              return data.User.ProductsOnHotSale;
            });
          }
        }, {
          key: "openProductDetail",
          value: function openProductDetail(event, product) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      if (product === null || product === void 0 ? void 0 : product.productModel) {
                        this.router.navigateByUrl("/user/product-detail?product=".concat(JSON.stringify(product)));
                      }

                    case 1:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "addProductToBookmark",
          value: function addProductToBookmark(event, product) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var _yield$Object, userId;

              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.next = 2;
                      return Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_5__["getDataFromLocalStorage"])(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_6__["LocalStorageKey"].ZERO_30_USER);

                    case 2:
                      _yield$Object = _context2.sent;
                      userId = _yield$Object.userId;

                      if (userId) {
                        // ? Dispatch action
                        this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_4__["actions"].AddProductToBookmarkInitiatedAction({
                          payload: {
                            productId: product.productModel.id,
                            userId: userId
                          }
                        }));
                      }

                    case 5:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }]);

        return HotSalesListPage;
      }();

      HotSalesListPage.ctorParameters = function () {
        return [{
          type: _ngrx_store__WEBPACK_IMPORTED_MODULE_3__["Store"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
        }];
      };

      HotSalesListPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-hot-sales-list',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./hot-sales-list.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/user/hot-sales-list/hot-sales-list.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./hot-sales-list.page.scss */
        "./src/app/user/hot-sales-list/hot-sales-list.page.scss"))["default"]]
      })], HotSalesListPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=hot-sales-list-hot-sales-list-module-es5.js.map