(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["address-address-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/user/profile/address/address.page.html":
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/user/profile/address/address.page.html ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n  <main id=\"address-page\" class=\"container with-bottom-menu bg-offwhite\">\n\t<section id=\"header\" class=\"constrain header-white\">\n\t\t<div class=\"header-actions\">\n\t\t\t<a [routerLink]=\"['/profile']\" class=\"back link\">\n\t\t\t\t<img class=\"svg\" src=\"assets/images/icons/arrow-left.svg\" width=\"18px\" alt=\"Go back\">\n\t\t\t</a>\n\t\t</div>\n\t\t<h2>Address</h2>\n\t</section>\n\n\t<section id=\"delivery-address\">\n\t\t<div class=\"addresses constrain\">\n\t\t\t<!-- This is an address -->\n\t\t\t<main *ngIf=\"(userData$ | async) as userData\">\n\t\t\t\t<a class=\"address cursor-pointer\" *ngIf=\"userData?.person.address\">\n\t\t\t\t\t<div class=\"details\">\n\t\t\t\t\t\t<h5>Home</h5>\n\t\t\t\t\t\t<h5 class=\"location-address\">\n\t\t\t\t\t\t\t<img class=\"svg\" src=\"assets/images/icons/map-pointer.svg\" height=\"9px\" alt=\"Store\">\n\t\t\t\t\t\t\t<span>{{ userData?.person.address | uppercase }}</span>\n\t\t\t\t\t\t</h5>\n\t\t\t\t\t\t<p class=\"name\">\n\t\t\t\t\t\t\t<img class=\"svg\" src=\"assets/images/icons/person.svg\" height=\"9px\" alt=\"Store\">\n\t\t\t\t\t\t\t<span>\n\t\t\t\t\t\t\t\t{{ userData?.person.firstName | uppercase }}\n\t\t\t\t\t\t\t\t{{ userData?.person.lastName | uppercase }}\n\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t</p>\n\t\t\t\t\t\t<p class=\"phone\">\n\t\t\t\t\t\t\t<img class=\"svg\" src=\"assets/images/icons/telephone.svg\" height=\"9px\" alt=\"Store\">\n\t\t\t\t\t\t\t<span>{{ userData?.person.phoneNo }}</span>\n\t\t\t\t\t\t</p>\n\t\t\t\t\t</div>\n\t\t\t\t\t<span class=\"chevron-icon\"><img class=\"svg\" src=\"assets/images/icons/chevron-right.svg\" height=\"20px\"></span>\n\t\t\t\t</a>\n\t\t\t</main>\n\n\t\t\t<!-- This is an address -->\n\t\t\t<!-- <a href=\"#\" class=\"address\">\n\t\t\t\t<div class=\"details\">\n\t\t\t\t\t<h5>Work</h5>\n\t\t\t\t\t<h5 class=\"location-address\">\n\t\t\t\t\t\t<img class=\"svg\" src=\"assets/images/icons/map-pointer.svg\" height=\"9px\" alt=\"Store\">\n\t\t\t\t\t\t<span>23 White Boulevard</span>\n\t\t\t\t\t</h5>\n\t\t\t\t\t<p class=\"name\">\n\t\t\t\t\t\t<img class=\"svg\" src=\"assets/images/icons/person.svg\" height=\"9px\" alt=\"Store\">\n\t\t\t\t\t\t<span>Joy Obianaba</span>\n\t\t\t\t\t</p>\n\t\t\t\t\t<p class=\"phone\">\n\t\t\t\t\t\t<img class=\"svg\" src=\"assets/images/icons/telephone.svg\" height=\"9px\" alt=\"Store\">\n\t\t\t\t\t\t<span>+234 (803) 878 2933</span>\n\t\t\t\t\t</p>\n\t\t\t\t</div>\n\t\t\t\t<span class=\"chevron-icon\"><img class=\"svg\" src=\"assets/images/icons/chevron-right.svg\" height=\"20px\"></span>\n\t\t\t</a> -->\n\n\t\t\t<!-- This is the button that adds a new address -->\n\t\t\t<a [routerLink]=\"['/profile', 'add-address']\" class=\"btn\">Add new Address</a>\n\t\t\t\n\t\t</div>\n\t</section>\n\n\t<app-conditional-bottom-navbar \n\t\t[userData]=\"userDataFromLocalStorage\">\n\t</app-conditional-bottom-navbar>\n  </main>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/user/profile/address/address-routing.module.ts":
/*!****************************************************************!*\
  !*** ./src/app/user/profile/address/address-routing.module.ts ***!
  \****************************************************************/
/*! exports provided: AddressPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddressPageRoutingModule", function() { return AddressPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _address_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./address.page */ "./src/app/user/profile/address/address.page.ts");




const routes = [
    {
        path: '',
        component: _address_page__WEBPACK_IMPORTED_MODULE_3__["AddressPage"]
    }
];
let AddressPageRoutingModule = class AddressPageRoutingModule {
};
AddressPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], AddressPageRoutingModule);



/***/ }),

/***/ "./src/app/user/profile/address/address.module.ts":
/*!********************************************************!*\
  !*** ./src/app/user/profile/address/address.module.ts ***!
  \********************************************************/
/*! exports provided: AddressPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddressPageModule", function() { return AddressPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _address_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./address-routing.module */ "./src/app/user/profile/address/address-routing.module.ts");
/* harmony import */ var _address_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./address.page */ "./src/app/user/profile/address/address.page.ts");
/* harmony import */ var src_app_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/shared/shared.module */ "./src/app/shared/shared.module.ts");








let AddressPageModule = class AddressPageModule {
};
AddressPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            src_app_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"],
            _address_routing_module__WEBPACK_IMPORTED_MODULE_5__["AddressPageRoutingModule"]
        ],
        declarations: [_address_page__WEBPACK_IMPORTED_MODULE_6__["AddressPage"]]
    })
], AddressPageModule);



/***/ }),

/***/ "./src/app/user/profile/address/address.page.scss":
/*!********************************************************!*\
  !*** ./src/app/user/profile/address/address.page.scss ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXIvcHJvZmlsZS9hZGRyZXNzL2FkZHJlc3MucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/user/profile/address/address.page.ts":
/*!******************************************************!*\
  !*** ./src/app/user/profile/address/address.page.ts ***!
  \******************************************************/
/*! exports provided: AddressPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddressPage", function() { return AddressPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");
/* harmony import */ var subsink__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! subsink */ "./node_modules/subsink/dist/es2015/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../utils/functions/app.functions */ "./src/utils/functions/app.functions.ts");
/* harmony import */ var _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../utils/types/app.constant */ "./src/utils/types/app.constant.ts");
/* harmony import */ var _user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../user/store/actions/user.action */ "./src/app/user/store/actions/user.action.ts");








let AddressPage = class AddressPage {
    constructor(store) {
        this.store = store;
        this.subSink = new subsink__WEBPACK_IMPORTED_MODULE_3__["SubSink"]();
    }
    ngOnInit() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            // ? get the userId from localStorage
            const userDataFromLocalStorage = yield Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_5__["getDataFromLocalStorage"])(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_6__["LocalStorageKey"].ZERO_30_USER);
            const { userId } = userDataFromLocalStorage;
            this.userDataFromLocalStorage = userDataFromLocalStorage;
            this.subSink.sink =
                this.store
                    .select((data) => data.User.UserData)
                    .subscribe((data) => {
                    if (data === null || data === void 0 ? void 0 : data.userId) {
                        this.userData$ = Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["of"])(data);
                    }
                    else {
                        this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_7__["actions"].GetUserDataInitiatedAction({ payload: { userId } }));
                    }
                });
        });
    }
    ngOnDestroy() {
        this.subSink.unsubscribe();
    }
};
AddressPage.ctorParameters = () => [
    { type: _ngrx_store__WEBPACK_IMPORTED_MODULE_2__["Store"] }
];
AddressPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-address",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./address.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/user/profile/address/address.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./address.page.scss */ "./src/app/user/profile/address/address.page.scss")).default]
    })
], AddressPage);



/***/ })

}]);
//# sourceMappingURL=address-address-module-es2015.js.map