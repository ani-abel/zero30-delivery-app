(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["discover-items-discover-items-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/user/discover-items/discover-items.page.html":
    /*!****************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/user/discover-items/discover-items.page.html ***!
      \****************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppUserDiscoverItemsDiscoverItemsPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n  <main id=\"discover-page\" class=\"bg-offwhite container with-bottom-menu\">\n\n    <!-- \n      .header primary below gives it the orange background.\n     -->\n    <section id=\"search\" class=\"header-primary\">\n      <div class=\"header\">\n        <a *ngIf=\"(userData$ | async)?.person?.address\" \n          [routerLink]=\"['/profile', 'address']\" \n          class=\"delivery-location link constrain\">\n          <h5>DELIVERING TO</h5>\n          <p>\n            <img class=\"svg\" src=\"assets/images/icons/map-pointer.svg\" height=\"10px\" alt=\"Map Pointer\">\n            <span>{{ (userData$ | async)?.person?.address | uppercase }}</span>\n            <span></span>\n            <img class=\"svg\" src=\"assets/images/icons/chevron-down.svg\" height=\"10px\" alt=\"Expand\">\n          </p>\n        </a>\n  \n        <div class=\"search-box constrain\">\n          <form [formGroup]=\"searchForm\" id=\"search-form\" class=\"input-with-icon\" (ngSubmit)=\"onSubmit()\">\n            <input [formControlName]=\"'SearchTerm'\" \n              type=\"search\" \n              id=\"search-input\" \n              placeholder=\"What are you searching for?\" />\n            <button type=\"submit\" class=\"btn\">\n              <img class=\"svg\" src=\"assets/images/icons/search.svg\" height=\"10px\" alt=\"Search\">\n            </button>\n          </form>\n          <a (click)=\"clearSearchTerm()\" class=\"btn close-search cursor-pointer\">\n            <!-- Whenever the search popup is open, the filter.svg icon hides and the \"Exit\" text shows instead just as it was done in the Figma design. It works with the .searching class on the <body> tag. -->\n            <img class=\"svg\" src=\"assets/images/icons/filter.svg\" height=\"15px\" alt=\"Filter\">\n            <p>Exit</p>\n          </a>\n        </div>\n      </div>\n  \n      <div class=\"search-suggestions\">\n        <div class=\"search-history constrain\">\n          <div class=\"section-title with-link p-0\">\n            <h4>History</h4>\n            <a href=\"#\" class=\"link link-secondary\">Clear All</a>\n          </div>\n  \n          <!-- This is the search history of the user -->\n          <div class=\"histories\">\n            <div class=\"history\">\n              <a href=\"#\" class=\"link link-secondary\">Milk Tea</a>\n              <button type=\"button\" class=\"btn\">\n                <img class=\"svg\" src=\"assets/images/icons/clear.svg\" height=\"10px\" alt=\"Clear\">\n              </button>\n            </div>\n            <div class=\"history\">\n              <a href=\"#\" class=\"link link-secondary\">Rice Chicken</a>\n              <button type=\"button\" class=\"btn\">\n                <img class=\"svg\" src=\"assets/images/icons/clear.svg\" height=\"10px\" alt=\"Clear\">\n              </button>\n            </div>\n            <div class=\"history\">\n              <a href=\"#\" class=\"link link-secondary\">Coffee</a>\n              <button type=\"button\" class=\"btn\">\n                <img class=\"svg\" src=\"assets/images/icons/clear.svg\" height=\"10px\" alt=\"Clear\">\n              </button>\n            </div>\n            <!-- This button adds more content and displays older search histories -->\n            <a href=\"#\" class=\"chevron link-primary link\">\n              <span>View more</span>\n              <img class=\"svg\" src=\"assets/images/icons/chevron-down.svg\" height=\"7px\" alt=\"Arrow Down\">\n            </a>\n          </div>\n        </div>\n  \n        <!-- These are product suggestions from the searched keyword -->\n        <div class=\"suggestions constrain\">\n          <div class=\"section-title with-link\">\n            <h4>Suggestions</h4>\n            <a href=\"search-results.html\" class=\"link link-primary\">\n              <span>View all</span>\n              <img class=\"svg\" src=\"assets/images/icons/double-chevron-right.svg\" height=\"7px\"\n                alt=\"Arrow Right\">\n            </a>\n          </div>\n  \n          <!-- <div class=\"slider\"\n          [flickity]=\"{ contain: true, prevNextButtons: false, pageDots: false, wrapAround: false, autoPlay: 10000, pauseAutoPlayOnHover: true, dragThreshold: 10 }\">\n            <div class=\"slide\">\n              <a href=\"#\" class=\"product\">\n                <img class=\"image\" src=\"assets/images/products/5.png\" height=\"100px\" alt=\"Product Results\">\n                <p class=\"name\">Asia Food</p>\n              </a>\n  \n              <a href=\"#\" class=\"product\">\n                <img class=\"image\" src=\"assets/images/products/3.png\" height=\"100px\" alt=\"Product Results\">\n                <p class=\"name\">Fast Food</p>\n              </a>\n            </div>\n  \n            <div class=\"slide\">\n              <a href=\"#\" class=\"product\">\n                <img class=\"image\" src=\"assets/images/products/4.png\" height=\"100px\" alt=\"Product Results\">\n                <p class=\"name\">Asia Food</p>\n              </a>\n  \n              <a href=\"#\" class=\"product\">\n                <img class=\"image\" src=\"assets/images/products/6.png\" height=\"100px\" alt=\"Product Results\">\n                <p class=\"name\">Fast Food</p>\n              </a>\n            </div>\n  \n            <div class=\"slide\">\n              <a href=\"#\" class=\"product\">\n                <img class=\"image\" src=\"assets/images/products/4.png\" height=\"100px\" alt=\"Product Results\">\n                <p class=\"name\">Asia Food</p>\n              </a>\n  \n              <a href=\"#\" class=\"product\">\n                <img class=\"image\" src=\"assets/images/products/6.png\" height=\"100px\" alt=\"Product Results\">\n                <p class=\"name\">Fast Food</p>\n              </a>\n            </div>\n  \n            <div class=\"slide\">\n              <a href=\"#\" class=\"product\">\n                <img class=\"image\" src=\"assets/images/products/4.png\" height=\"100px\" alt=\"Product Results\">\n                <p class=\"name\">Asia Food</p>\n              </a>\n  \n              <a href=\"#\" class=\"product\">\n                <img class=\"image\" src=\"assets/images/products/6.png\" height=\"100px\" alt=\"Product Results\">\n                <p class=\"name\">Fast Food</p>\n              </a>\n            </div>\n          </div> -->\n        </div>\n      </div>\n    </section>\n  \n    <!-- This is that menu at the bottom of every page -->\n    <app-user-bottom-navbar></app-user-bottom-navbar>\n  \n    <swiper [slidesPerView]=\"'auto'\" \n      [centeredSlides]=\"false\"\n      id=\"discover-adverts\"\n      [ngClass]=\"['constrain']\"\n      [spaceBetween]=\"15\"\n      [loop]=\"true\" \n      [autoplay]=\"{ delay: 2500, disableOnInteraction: false }\">\n      <ng-template swiperSlide>\n        <div class=\"advert\">\n          <a class=\"details cursor-pointer\">\n            <div class=\"image\">\n              <img src=\"assets/images/banner/banner1.png\" height=\"100px\" alt=\"Advert\">\n            </div>\n          </a>\n        </div>\n      </ng-template>\n      <ng-template swiperSlide>\n        <div class=\"advert\">\n          <a class=\"details cursor-pointer\">\n            <div class=\"image\">\n              <img src=\"assets/images/banner/banner2.png\" height=\"100px\" alt=\"Advert\">\n            </div>\n          </a>\n        </div>\n      </ng-template>\n      <ng-template swiperSlide>\n        <div class=\"advert\">\n          <a class=\"details cursor-pointer\">\n            <div class=\"image\">\n              <img src=\"assets/images/banner/banner2.png\" height=\"100px\" alt=\"Advert\">\n            </div>\n            <!-- <div class=\"flickity-image-support\">\n              <h5>Advert</h5>\n            </div> -->\n          </a>\n        </div>\n      </ng-template>\n      <ng-template swiperSlide>\n        <div class=\"advert\">\n          <a class=\"details cursor-pointer\">\n            <div class=\"image\">\n              <img src=\"assets/images/banner/banner1.png\" height=\"100px\" alt=\"Advert\">\n            </div>\n          </a>\n        </div>\n      </ng-template>\n    </swiper>\n\n    <section id=\"categories\" \n      class=\"constrain\" \n      *ngIf=\"(productsByMerchant$ | async)?.productProductCategoryIndexModels.length > 0\">\n      <div class=\"section-title with-link\">\n        <h4>Categories</h4>\n        <a [routerLink]=\"['/user', 'category-list']\" class=\"link link-primary\">\n          <span>View all</span>\n          <img class=\"svg\" src=\"assets/images/icons/double-chevron-right.svg\" height=\"7px\" alt=\"Arrow Right\">\n        </a>\n      </div>\n\n      <swiper [slidesPerView]=\"'auto'\"\n        [centeredSlides]=\"false\" \n        [ngClass]=\"['categories', 'constrain']\"\n        [spaceBetween]=\"15\"\n        [loop]=\"true\" \n        [autoplay]=\"{ delay: 10000, disableOnInteraction: false }\">\n      <ng-template \n        swiperSlide \n        *ngFor=\"let category of (productsByMerchant$ | async)?.productProductCategoryIndexModels | sliceContent : 10\">\n        <app-category-hightlight-widget \n          [productCategory]=\"category\">\n        </app-category-hightlight-widget>\n      </ng-template>\n    </swiper>\n    </section>\n  \n    <section id=\"orders\" class=\"constrain\" *ngIf=\"(productOrderHistory$ | async)?.length > 0\">\n      <div class=\"section-title with-link\">\n        <h4>Products Ordered</h4>\n        <a [routerLink]=\"['/user', 'orders']\" class=\"link link-primary\">\n          <span>View all</span>\n          <img class=\"svg\" src=\"assets/images/icons/double-chevron-right.svg\" height=\"7px\" alt=\"Arrow Right\">\n        </a>\n      </div>\n\n      <swiper [slidesPerView]=\"'auto'\" \n        [ngClass]=\"['orders', 'constrain']\"\n        [spaceBetween]=\"15\"\n        [loop]=\"true\" \n        [autoplay]=\"{ delay: 2500, disableOnInteraction: true }\">\n        <ng-template swiperSlide \n        *ngFor=\"let order of (productOrderHistory$ | async) | sliceContent : 8\">\n          <app-ordered-product-highlight-widget \n          (viewProductDetailEvent)=\"openProductDetail($event, order)\"\n          [product]=\"order\">\n        </app-ordered-product-highlight-widget>\n        </ng-template>\n      </swiper>\n    </section>\n  \n    <section id=\"restaurants\" class=\"constrain\" *ngIf=\"(favouriteMerchants$ | async)?.length > 0\">\n      <div class=\"section-title with-link\">\n        <h4>Favourite Restaurants</h4>\n        <a [routerLink]=\"['/user', 'favourites']\" class=\"link link-primary\">\n          <span>View all</span>\n          <img class=\"svg\" src=\"assets/images/icons/double-chevron-right.svg\" height=\"7px\" alt=\"Arrow Right\">\n        </a>\n      </div>\n  \n      <swiper [slidesPerView]=\"'auto'\" \n        [ngClass]=\"['restaurants', 'constrain']\"\n        [spaceBetween]=\"15\"\n        [loop]=\"true\" \n        [autoplay]=\"{ delay: 2500, disableOnInteraction: true }\">\n        <ng-template swiperSlide *ngFor=\"let merchant of (favouriteMerchants$ | async) | sliceContent : 8\">\n          <app-favourite-merchant-hightlight-widget \n          [favouriteMerchant]=\"merchant\">\n        </app-favourite-merchant-hightlight-widget>\n        </ng-template>\n      </swiper>\n    </section>\n  \n    <section id=\"hot-sale\" class=\"constrain\" *ngIf=\"(productsOnHotSale$ | async)?.length > 0\">\n      <div class=\"section-title with-link\">\n        <h4>Hot Sale</h4>\n        <a [routerLink]=\"['/user', 'hot-sales-list']\" \n          class=\"link link-primary\">\n          <span>View all</span>\n          <img class=\"svg\" src=\"assets/images/icons/double-chevron-right.svg\" height=\"7px\" alt=\"Arrow Right\">\n        </a>\n      </div>\n\n      <swiper [slidesPerView]=\"'auto'\" \n        [ngClass]=\"['sales', 'constrain']\"\n        [spaceBetween]=\"15\"\n        [loop]=\"true\" \n        [autoplay]=\"{ delay: 2500, disableOnInteraction: true }\">\n        <ng-template swiperSlide *ngFor=\"let product of (productsOnHotSale$ | async) | sliceContent : 8\">\n          <app-hot-sale-highlight-widget \n            (viewProductDetailEvent)=\"openProductDetail($event, product)\"\n            [product]=\"product\">\n          </app-hot-sale-highlight-widget>\n        </ng-template>\n      </swiper>\n    </section>\n  \n    <section id=\"nearby\" \n      class=\"constrain\" \n      *ngIf=\"(productsByMerchant$ | async)?.productMerchantIndexModels?.length > 0\">\n      <div class=\"section-title with-link\">\n        <h4>Nearby</h4>\n        <a [routerLink]=\"['/user', 'nearby']\" class=\"link link-primary\">\n          <span>View all</span>\n          <img class=\"svg\" src=\"assets/images/icons/double-chevron-right.svg\" height=\"7px\" alt=\"Arrow Right\">\n        </a>\n      </div>\n  \n      <!-- \n        There's a \".bookmarked\" class on the restaurant that are bookmarked by the user, Once the .bookmarked tag is added as a class it styles the bookmark icon.\n      -->\n      <main *ngIf=\"(productsByMerchant$ | async) as productsByMerchant\">\n        <app-merchant-product-listing\n          *ngFor=\"let merchant of productsByMerchant?.productMerchantIndexModels | sliceContent : 8; index as i\"\n          [merchant]=\"merchant\"\n          [isBookmarked]=\"true\"\n          [currentIteration]=\"i\"\n          [arraySize]=\"productsByMerchant?.productMerchantIndexModels.length\"\n          (addToBookmark)=\"addResturantToBookmark($event, merchant)\">\n        </app-merchant-product-listing>\n      </main>\n      \n    </section>\n  </main>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/user/discover-items/discover-items-routing.module.ts":
    /*!**********************************************************************!*\
      !*** ./src/app/user/discover-items/discover-items-routing.module.ts ***!
      \**********************************************************************/

    /*! exports provided: DiscoverItemsPageRoutingModule */

    /***/
    function srcAppUserDiscoverItemsDiscoverItemsRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "DiscoverItemsPageRoutingModule", function () {
        return DiscoverItemsPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _discover_items_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./discover-items.page */
      "./src/app/user/discover-items/discover-items.page.ts");

      var routes = [{
        path: '',
        component: _discover_items_page__WEBPACK_IMPORTED_MODULE_3__["DiscoverItemsPage"]
      }];

      var DiscoverItemsPageRoutingModule = function DiscoverItemsPageRoutingModule() {
        _classCallCheck(this, DiscoverItemsPageRoutingModule);
      };

      DiscoverItemsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], DiscoverItemsPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/user/discover-items/discover-items.module.ts":
    /*!**************************************************************!*\
      !*** ./src/app/user/discover-items/discover-items.module.ts ***!
      \**************************************************************/

    /*! exports provided: DiscoverItemsPageModule */

    /***/
    function srcAppUserDiscoverItemsDiscoverItemsModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "DiscoverItemsPageModule", function () {
        return DiscoverItemsPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _discover_items_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./discover-items-routing.module */
      "./src/app/user/discover-items/discover-items-routing.module.ts");
      /* harmony import */


      var _discover_items_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./discover-items.page */
      "./src/app/user/discover-items/discover-items.page.ts");
      /* harmony import */


      var _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../../shared/shared.module */
      "./src/app/shared/shared.module.ts");

      var DiscoverItemsPageModule = function DiscoverItemsPageModule() {
        _classCallCheck(this, DiscoverItemsPageModule);
      };

      DiscoverItemsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _discover_items_routing_module__WEBPACK_IMPORTED_MODULE_5__["DiscoverItemsPageRoutingModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"]],
        declarations: [_discover_items_page__WEBPACK_IMPORTED_MODULE_6__["DiscoverItemsPage"]]
      })], DiscoverItemsPageModule);
      /***/
    },

    /***/
    "./src/app/user/discover-items/discover-items.page.scss":
    /*!**************************************************************!*\
      !*** ./src/app/user/discover-items/discover-items.page.scss ***!
      \**************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppUserDiscoverItemsDiscoverItemsPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXIvZGlzY292ZXItaXRlbXMvZGlzY292ZXItaXRlbXMucGFnZS5zY3NzIn0= */";
      /***/
    },

    /***/
    "./src/app/user/discover-items/discover-items.page.ts":
    /*!************************************************************!*\
      !*** ./src/app/user/discover-items/discover-items.page.ts ***!
      \************************************************************/

    /*! exports provided: DiscoverItemsPage */

    /***/
    function srcAppUserDiscoverItemsDiscoverItemsPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "DiscoverItemsPage", function () {
        return DiscoverItemsPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ngrx_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ngrx/store */
      "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var subsink__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! subsink */
      "./node_modules/subsink/dist/es2015/index.js");
      /* harmony import */


      var _user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ../../user/store/actions/user.action */
      "./src/app/user/store/actions/user.action.ts");
      /* harmony import */


      var _utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../../../utils/functions/app.functions */
      "./src/utils/functions/app.functions.ts");
      /* harmony import */


      var _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ../../../utils/types/app.constant */
      "./src/utils/types/app.constant.ts");
      /* harmony import */


      var swiper_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! swiper/core */
      "./node_modules/swiper/swiper.esm.js"); // install Swiper components


      swiper_core__WEBPACK_IMPORTED_MODULE_9__["default"].use([swiper_core__WEBPACK_IMPORTED_MODULE_9__["Navigation"], swiper_core__WEBPACK_IMPORTED_MODULE_9__["Pagination"], swiper_core__WEBPACK_IMPORTED_MODULE_9__["A11y"], swiper_core__WEBPACK_IMPORTED_MODULE_9__["Autoplay"]]);

      var DiscoverItemsPage = /*#__PURE__*/function () {
        function DiscoverItemsPage(store, router) {
          _classCallCheck(this, DiscoverItemsPage);

          this.store = store;
          this.router = router;
          this.subSink = new subsink__WEBPACK_IMPORTED_MODULE_5__["SubSink"]();
          this.config = {
            speed: 500,
            spaceBetween: 30,
            slidesPerView: 2,
            loop: true,
            autoplay: {
              delay: 2500,
              disableOnInteraction: true
            },
            pagination: {
              el: '.swiper-pagination',
              clickable: true
            }
          };
        }

        _createClass(DiscoverItemsPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var _this = this;

              var authData;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      this.initForm();
                      _context.next = 3;
                      return Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_7__["getDataFromLocalStorage"])(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_8__["LocalStorageKey"].ZERO_30_USER);

                    case 3:
                      authData = _context.sent;

                      if (authData === null || authData === void 0 ? void 0 : authData.userId) {
                        setTimeout(function () {
                          _this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_6__["actions"].GetProductsByMerchantInitiatedAction({
                            payload: {
                              userId: authData.userId
                            }
                          }));

                          _this.productsByMerchant$ = _this.store.select(function (data) {
                            return data.User.ProductsByMerchants;
                          });
                        }, 500); //? Get the list of favourites merchants

                        this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_6__["actions"].GetUserFavouritesInitiatedAction({
                          payload: {
                            userId: authData === null || authData === void 0 ? void 0 : authData.userId
                          }
                        }));
                        this.favouriteMerchants$ = this.store.select(function (data) {
                          return data.User.FavouriteMerchants;
                        }); //? Get Order history for the user

                        this.productOrderHistory$ = this.store.select(function (data) {
                          return data.User.ProductOrderHistory;
                        });
                        this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_6__["actions"].GetUserOrderHistoryInitiatedAction({
                          payload: {
                            userId: authData.userId
                          }
                        })); //? Get user's personal data

                        this.userData$ = this.store.select(function (data) {
                          return data.User.UserData;
                        });
                        this.subSink.sink = this.userData$.subscribe(function (data) {
                          if (!(data === null || data === void 0 ? void 0 : data.userId)) {
                            _this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_6__["actions"].GetUserDataInitiatedAction({
                              payload: {
                                userId: authData.userId
                              }
                            }));
                          }
                        });
                      }

                      this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_6__["actions"].GetProductsOnHotSaleInitiatedAction());
                      this.productsOnHotSale$ = this.store.select(function (data) {
                        return data.User.ProductsOnHotSale;
                      });

                    case 7:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "initForm",
          value: function initForm() {
            this.searchForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
              SearchTerm: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]))
            });
          }
        }, {
          key: "onSwiper",
          value: function onSwiper(swiper) {
            console.log(swiper);
          }
        }, {
          key: "onSlideChange",
          value: function onSlideChange() {
            console.log('slide change');
          }
        }, {
          key: "addResturantToBookmark",
          value: function addResturantToBookmark(event, product) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var _yield$Object, userId;

              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.next = 2;
                      return Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_7__["getDataFromLocalStorage"])(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_8__["LocalStorageKey"].ZERO_30_USER);

                    case 2:
                      _yield$Object = _context2.sent;
                      userId = _yield$Object.userId;

                      if (userId) {
                        // ? Dispatch action
                        this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_6__["actions"].AddMerchantToUserBookmarksInitiatedAction({
                          payload: {
                            MerchantId: event.MerchantId,
                            UserId: userId,
                            Merchant: product["merchant"]
                          }
                        }));
                      }

                    case 5:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }, {
          key: "onSubmit",
          value: function onSubmit() {
            if (this.searchForm.invalid) {
              return;
            }

            var SearchTerm = this.searchForm.value.SearchTerm;
            this.router.navigate(["/user", "view-search-results"], {
              queryParams: {
                searchTerm: SearchTerm
              }
            });
          }
        }, {
          key: "clearSearchTerm",
          value: function clearSearchTerm() {
            this.searchForm.reset();
          }
        }, {
          key: "openProductDetail",
          value: function openProductDetail(event, product) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                      if (product === null || product === void 0 ? void 0 : product.productModel) {
                        // this.router.navigate(['/user', 'product-detail', { queryParams: { product: JSON.stringify(product) } }]);
                        this.router.navigateByUrl("/user/product-detail?product=".concat(JSON.stringify(product)));
                      }

                    case 1:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3, this);
            }));
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            this.subSink.unsubscribe();
          }
        }]);

        return DiscoverItemsPage;
      }();

      DiscoverItemsPage.ctorParameters = function () {
        return [{
          type: _ngrx_store__WEBPACK_IMPORTED_MODULE_3__["Store"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
        }];
      };

      DiscoverItemsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-discover-items',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./discover-items.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/user/discover-items/discover-items.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./discover-items.page.scss */
        "./src/app/user/discover-items/discover-items.page.scss"))["default"]]
      })], DiscoverItemsPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=discover-items-discover-items-module-es5.js.map