(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["category-products-category-products-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/user/category-products/category-products.page.html":
    /*!**********************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/user/category-products/category-products.page.html ***!
      \**********************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppUserCategoryProductsCategoryProductsPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n  <main id=\"search-results-page\" class=\"container with-bottom-menu bg-offwhite\">\n    <section class=\"header-area\">\n      <div class=\"header-section\">\n         <a [routerLink]=\"['/user', 'category-list']\" class=\"back link\">\n          <img class=\"svg\" src=\"assets/images/icons/arrow-left.svg\" width=\"18px\" alt=\"Go back\">\n        </a>\n      </div>\n      <div class=\"header-section\">\n        <div class=\"text\">\n          <h2 class=\"name\">{{ categoryName | capitalize }}</h2>\n        </div>\n      </div>\n    </section>\n  \n    <!-- This is that menu at the bottom of every page -->\n    <app-user-bottom-navbar></app-user-bottom-navbar>\n\n    <section id=\"search-results\" \n      class=\"constrain\" \n      *ngIf=\"(productsGroupedByCategory$ | async) as productsGroupedByCategory\">\n      <!-- \n        There's a \".bookmarked\" class on the search results that are bookmarked by the user, Once the .bookmarked tag is added as a class it styles the bookmark icon on top of the result's logo.\n      -->\n      <app-search-results-widget \n        (addProductToBookmarkType)=\"addProductToBookmark($event, product)\"\n        (viewProductDetailEvent)=\"openProductDetail($event, product)\"\n        *ngFor=\"let product of productsGroupedByCategory\" \n        [searchResult]=\"product\"></app-search-results-widget>\n    </section>\n\n    <app-footer-expander></app-footer-expander>\n  </main>\n\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/user/category-products/category-products-routing.module.ts":
    /*!****************************************************************************!*\
      !*** ./src/app/user/category-products/category-products-routing.module.ts ***!
      \****************************************************************************/

    /*! exports provided: CategoryProductsPageRoutingModule */

    /***/
    function srcAppUserCategoryProductsCategoryProductsRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CategoryProductsPageRoutingModule", function () {
        return CategoryProductsPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _category_products_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./category-products.page */
      "./src/app/user/category-products/category-products.page.ts");

      var routes = [{
        path: '',
        component: _category_products_page__WEBPACK_IMPORTED_MODULE_3__["CategoryProductsPage"]
      }];

      var CategoryProductsPageRoutingModule = function CategoryProductsPageRoutingModule() {
        _classCallCheck(this, CategoryProductsPageRoutingModule);
      };

      CategoryProductsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], CategoryProductsPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/user/category-products/category-products.module.ts":
    /*!********************************************************************!*\
      !*** ./src/app/user/category-products/category-products.module.ts ***!
      \********************************************************************/

    /*! exports provided: CategoryProductsPageModule */

    /***/
    function srcAppUserCategoryProductsCategoryProductsModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CategoryProductsPageModule", function () {
        return CategoryProductsPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _category_products_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./category-products-routing.module */
      "./src/app/user/category-products/category-products-routing.module.ts");
      /* harmony import */


      var _category_products_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./category-products.page */
      "./src/app/user/category-products/category-products.page.ts");
      /* harmony import */


      var _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../../shared/shared.module */
      "./src/app/shared/shared.module.ts");

      var CategoryProductsPageModule = function CategoryProductsPageModule() {
        _classCallCheck(this, CategoryProductsPageModule);
      };

      CategoryProductsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"], _category_products_routing_module__WEBPACK_IMPORTED_MODULE_5__["CategoryProductsPageRoutingModule"]],
        declarations: [_category_products_page__WEBPACK_IMPORTED_MODULE_6__["CategoryProductsPage"]]
      })], CategoryProductsPageModule);
      /***/
    },

    /***/
    "./src/app/user/category-products/category-products.page.scss":
    /*!********************************************************************!*\
      !*** ./src/app/user/category-products/category-products.page.scss ***!
      \********************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppUserCategoryProductsCategoryProductsPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "#search-results {\n  margin-top: 20px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXNlci9jYXRlZ29yeS1wcm9kdWN0cy9jYXRlZ29yeS1wcm9kdWN0cy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxnQkFBQTtBQUNKIiwiZmlsZSI6InNyYy9hcHAvdXNlci9jYXRlZ29yeS1wcm9kdWN0cy9jYXRlZ29yeS1wcm9kdWN0cy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjc2VhcmNoLXJlc3VsdHMge1xuICAgIG1hcmdpbi10b3A6IDIwcHg7XG59Il19 */";
      /***/
    },

    /***/
    "./src/app/user/category-products/category-products.page.ts":
    /*!******************************************************************!*\
      !*** ./src/app/user/category-products/category-products.page.ts ***!
      \******************************************************************/

    /*! exports provided: CategoryProductsPage */

    /***/
    function srcAppUserCategoryProductsCategoryProductsPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CategoryProductsPage", function () {
        return CategoryProductsPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ngrx_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ngrx/store */
      "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");
      /* harmony import */


      var subsink__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! subsink */
      "./node_modules/subsink/dist/es2015/index.js");
      /* harmony import */


      var _utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../../../utils/functions/app.functions */
      "./src/utils/functions/app.functions.ts");
      /* harmony import */


      var _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ../../../utils/types/app.constant */
      "./src/utils/types/app.constant.ts");
      /* harmony import */


      var _user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../../user/store/actions/user.action */
      "./src/app/user/store/actions/user.action.ts");

      var CategoryProductsPage = /*#__PURE__*/function () {
        function CategoryProductsPage(activatedRoute, store, router) {
          _classCallCheck(this, CategoryProductsPage);

          this.activatedRoute = activatedRoute;
          this.store = store;
          this.router = router;
          this.subSink = new subsink__WEBPACK_IMPORTED_MODULE_4__["SubSink"]();
        }

        _createClass(CategoryProductsPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this = this;

            //? Get the catrory Id
            this.subSink.sink = this.activatedRoute.params.subscribe(function (data) {
              _this.categoryId = data.categoryId;
              _this.categoryName = data.categoryName;
            }); //? Get all products related to this category

            this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_7__["actions"].GetProductsGroupedByCategoryInitiatedAction({
              payload: {
                categoryId: this.categoryId
              }
            }));
            this.productsGroupedByCategory$ = this.store.select(function (data) {
              return data.User.ProductsGroupedByCategory;
            });
          }
        }, {
          key: "openProductDetail",
          value: function openProductDetail(event, product) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      if (product === null || product === void 0 ? void 0 : product.productModel) {
                        this.router.navigateByUrl("/user/product-detail?product=".concat(JSON.stringify(product)));
                      }

                    case 1:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "addProductToBookmark",
          value: function addProductToBookmark(event, product) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var _yield$Object, userId;

              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.next = 2;
                      return Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_5__["getDataFromLocalStorage"])(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_6__["LocalStorageKey"].ZERO_30_USER);

                    case 2:
                      _yield$Object = _context2.sent;
                      userId = _yield$Object.userId;

                      if (userId) {
                        // ? Dispatch action
                        this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_7__["actions"].AddProductToBookmarkInitiatedAction({
                          payload: {
                            productId: product.productModel.id,
                            userId: userId
                          }
                        }));
                      }

                    case 5:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            this.subSink.unsubscribe();
          }
        }]);

        return CategoryProductsPage;
      }();

      CategoryProductsPage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
        }, {
          type: _ngrx_store__WEBPACK_IMPORTED_MODULE_3__["Store"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }];
      };

      CategoryProductsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-category-products',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./category-products.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/user/category-products/category-products.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./category-products.page.scss */
        "./src/app/user/category-products/category-products.page.scss"))["default"]]
      })], CategoryProductsPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=category-products-category-products-module-es5.js.map