(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["view-search-results-view-search-results-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/user/view-search-results/view-search-results.page.html":
    /*!**************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/user/view-search-results/view-search-results.page.html ***!
      \**************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppUserViewSearchResultsViewSearchResultsPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n  <main id=\"search-results-page\" class=\"bg-offwhite container with-bottom-menu\">\n\n    <section id=\"search\" class=\"header-primary\">\n      <div class=\"header\">\n        <div class=\"search-box constrain\">\n          <form [formGroup]=\"searchForm\" id=\"search-form\" (ngSubmit)=\"onSubmit()\" class=\"input-with-icon\">\n            <input \n              [formControlName]=\"'SearchTerm'\" \n              type=\"search\" \n              id=\"search-input\" \n              placeholder=\"What are you searching for?\" />\n            <button type=\"submit\" class=\"btn\">\n              <img class=\"svg\" src=\"assets/images/icons/search.svg\" height=\"10px\" alt=\"Search\" />\n            </button>\n          </form>\n          <a (click)=\"clearSearchTerm()\" class=\"btn close-search cursor-pointer\">\n            <img class=\"svg\" src=\"assets/images/icons/filter.svg\" height=\"15px\" alt=\"Filter\">\n            <p>Exit</p>\n          </a>\n        </div>\n      </div>\n    </section>\n  \n    <!-- This is that menu at the bottom of every page -->\n    <app-user-bottom-navbar></app-user-bottom-navbar>\n\n    <!-- The number of results is supposed to be dynamic. -->\n    <h6 class=\"constrain number-of-results\">Approximately <span>{{ (searchResults$ | async)?.length || 0 }}</span> result(s)</h6>\n  \n    <section id=\"search-results\" \n      class=\"constrain\" \n      *ngIf=\"(searchResults$ | async) as searchResults\">\n      <!-- \n        There's a \".bookmarked\" class on the search results that are bookmarked by the user, Once the .bookmarked tag is added as a class it styles the bookmark icon on top of the result's logo.\n      -->\n      <app-search-results-widget \n        (addProductToBookmarkType)=\"addProductToBookmark($event, searchResult)\"\n        (viewProductDetailEvent)=\"openProductDetail($event, searchResult)\"\n        *ngFor=\"let searchResult of searchResults\" \n        [searchResult]=\"searchResult\"></app-search-results-widget>\n    </section>\n  </main>\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/user/view-search-results/view-search-results-routing.module.ts":
    /*!********************************************************************************!*\
      !*** ./src/app/user/view-search-results/view-search-results-routing.module.ts ***!
      \********************************************************************************/

    /*! exports provided: ViewSearchResultsPageRoutingModule */

    /***/
    function srcAppUserViewSearchResultsViewSearchResultsRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ViewSearchResultsPageRoutingModule", function () {
        return ViewSearchResultsPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _view_search_results_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./view-search-results.page */
      "./src/app/user/view-search-results/view-search-results.page.ts");

      var routes = [{
        path: '',
        component: _view_search_results_page__WEBPACK_IMPORTED_MODULE_3__["ViewSearchResultsPage"]
      }];

      var ViewSearchResultsPageRoutingModule = function ViewSearchResultsPageRoutingModule() {
        _classCallCheck(this, ViewSearchResultsPageRoutingModule);
      };

      ViewSearchResultsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], ViewSearchResultsPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/user/view-search-results/view-search-results.module.ts":
    /*!************************************************************************!*\
      !*** ./src/app/user/view-search-results/view-search-results.module.ts ***!
      \************************************************************************/

    /*! exports provided: ViewSearchResultsPageModule */

    /***/
    function srcAppUserViewSearchResultsViewSearchResultsModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ViewSearchResultsPageModule", function () {
        return ViewSearchResultsPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _view_search_results_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./view-search-results-routing.module */
      "./src/app/user/view-search-results/view-search-results-routing.module.ts");
      /* harmony import */


      var _view_search_results_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./view-search-results.page */
      "./src/app/user/view-search-results/view-search-results.page.ts");
      /* harmony import */


      var _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../../shared/shared.module */
      "./src/app/shared/shared.module.ts");

      var ViewSearchResultsPageModule = function ViewSearchResultsPageModule() {
        _classCallCheck(this, ViewSearchResultsPageModule);
      };

      ViewSearchResultsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _view_search_results_routing_module__WEBPACK_IMPORTED_MODULE_5__["ViewSearchResultsPageRoutingModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"]],
        declarations: [_view_search_results_page__WEBPACK_IMPORTED_MODULE_6__["ViewSearchResultsPage"]]
      })], ViewSearchResultsPageModule);
      /***/
    },

    /***/
    "./src/app/user/view-search-results/view-search-results.page.scss":
    /*!************************************************************************!*\
      !*** ./src/app/user/view-search-results/view-search-results.page.scss ***!
      \************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppUserViewSearchResultsViewSearchResultsPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXIvdmlldy1zZWFyY2gtcmVzdWx0cy92aWV3LXNlYXJjaC1yZXN1bHRzLnBhZ2Uuc2NzcyJ9 */";
      /***/
    },

    /***/
    "./src/app/user/view-search-results/view-search-results.page.ts":
    /*!**********************************************************************!*\
      !*** ./src/app/user/view-search-results/view-search-results.page.ts ***!
      \**********************************************************************/

    /*! exports provided: ViewSearchResultsPage */

    /***/
    function srcAppUserViewSearchResultsViewSearchResultsPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ViewSearchResultsPage", function () {
        return ViewSearchResultsPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ngrx_store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ngrx/store */
      "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");
      /* harmony import */


      var subsink__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! subsink */
      "./node_modules/subsink/dist/es2015/index.js");
      /* harmony import */


      var _user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ../../user/store/actions/user.action */
      "./src/app/user/store/actions/user.action.ts");
      /* harmony import */


      var src_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/utils/functions/app.functions */
      "./src/utils/functions/app.functions.ts");
      /* harmony import */


      var src_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! src/utils/types/app.constant */
      "./src/utils/types/app.constant.ts");

      var ViewSearchResultsPage = /*#__PURE__*/function () {
        function ViewSearchResultsPage(activatedRoute, store, router) {
          _classCallCheck(this, ViewSearchResultsPage);

          this.activatedRoute = activatedRoute;
          this.store = store;
          this.router = router;
          this.subSink = new subsink__WEBPACK_IMPORTED_MODULE_5__["SubSink"]();
          this.viewProductDetailEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        }

        _createClass(ViewSearchResultsPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this = this;

            //? The get search-term from query params
            this.subSink.sink = this.activatedRoute.queryParams.subscribe(function (data) {
              if (data === null || data === void 0 ? void 0 : data.searchTerm) {
                _this.searchTerm = data.searchTerm;

                _this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_6__["actions"].SearchForProductsInitiatedAction({
                  payload: {
                    searchTerm: data.searchTerm
                  }
                }));
              }

              _this.initForm();
            });
            this.searchResults$ = this.store.select(function (data) {
              return data.User.ProductSearchResults;
            });
          }
        }, {
          key: "initForm",
          value: function initForm() {
            this.searchForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
              SearchTerm: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](this.searchTerm, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]))
            });
          }
        }, {
          key: "onSubmit",
          value: function onSubmit() {
            if (this.searchForm.invalid) {
              return;
            }

            var SearchTerm = this.searchForm.value.SearchTerm;
            this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_6__["actions"].SearchForProductsInitiatedAction({
              payload: {
                searchTerm: SearchTerm
              }
            }));
            return;
          }
        }, {
          key: "openProductDetail",
          value: function openProductDetail(event, product) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      if (product === null || product === void 0 ? void 0 : product.productModel) {
                        this.router.navigateByUrl("/user/product-detail?product=".concat(JSON.stringify(product)));
                      }

                    case 1:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "addProductToBookmark",
          value: function addProductToBookmark(event, product) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var _yield$Object, userId;

              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.next = 2;
                      return Object(src_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_7__["getDataFromLocalStorage"])(src_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_8__["LocalStorageKey"].ZERO_30_USER);

                    case 2:
                      _yield$Object = _context2.sent;
                      userId = _yield$Object.userId;

                      if (userId) {
                        // ? Dispatch action
                        this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_6__["actions"].AddProductToBookmarkInitiatedAction({
                          payload: {
                            productId: product.productModel.id,
                            userId: userId
                          }
                        }));
                      }

                    case 5:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }, {
          key: "clearSearchTerm",
          value: function clearSearchTerm() {
            this.searchForm.reset();
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            this.subSink.unsubscribe();
          }
        }]);

        return ViewSearchResultsPage;
      }();

      ViewSearchResultsPage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
        }, {
          type: _ngrx_store__WEBPACK_IMPORTED_MODULE_4__["Store"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }];
      };

      ViewSearchResultsPage.propDecorators = {
        viewProductDetailEvent: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
        }]
      };
      ViewSearchResultsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-view-search-results',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./view-search-results.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/user/view-search-results/view-search-results.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./view-search-results.page.scss */
        "./src/app/user/view-search-results/view-search-results.page.scss"))["default"]]
      })], ViewSearchResultsPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=view-search-results-view-search-results-module-es5.js.map