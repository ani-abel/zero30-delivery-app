(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["orders-orders-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/rider/orders/orders.page.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/rider/orders/orders.page.html ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n  <main id=\"orders-page\" class=\"container with-bottom-menu bg-offwhite\">\n    <section id=\"header\" class=\"constrain header-white\">\n      <div class=\"text\">\n        <h2 class=\"name\">Orders</h2>\n      </div>\n    </section>\n\n    <!-- \n      This is the tab buttons.\n      When you click the buttons they activate the selected tab, clicking it also adds a \".active\" class to both the button of the tab and the tab content.\n      I have already implemented it using javascript.\n    -->\n    <section id=\"tabs\" class=\"constrain\">\n      <button class=\"tab\" \n        (click)=\"switchTabs(productCartStatus.TRANSIT)\" \n        [ngClass]=\"{'active' : activeTabType === productCartStatus.TRANSIT }\">\n        <span>Ongoing</span>\n      </button>\n      <button class=\"tab\"\n        (click)=\"switchTabs(productCartStatus.COMPLETED)\" \n        [ngClass]=\"{'active' : activeTabType === productCartStatus.COMPLETED }\"><span>History</span></button>\n    </section>\n  \n    <!-- Tab content -->\n    <!-- The tabs have empty states already designed. You can display them by adding a \".empty\" class to the tab-content section. It'll look like this: \"tab-content empty\". I've done this for the Ongoing tab already.  -->\n    <!-- This is the Ongoing tab -->\n    <section id=\"ongoing\" \n      *ngIf=\"activeTabType === productCartStatus.TRANSIT\" \n      class=\"tab-content active\" [ngClass]=\"{ 'empty': !((ongoingDeliveries$ | async)?.length > 0) }\">\n      <div class=\"orders constrain\" *ngIf=\"(ongoingDeliveries$ | async) as ongoingDeliveries\">\n        <app-rider-delivery-ongoing-widget\n          [cart]=\"delivery\"\n          *ngFor=\"let delivery of ongoingDeliveries\">\n        </app-rider-delivery-ongoing-widget>\n      </div>\n  \n      <!-- This is the empty state of the Ongoing tab -->\n      <div class=\"empty-state constrain\">\n        <img src=\"assets/images/others/idle-rider.png\" height=\"300px\" alt=\"Idle Rider\" />\n  \n        <div class=\"text\">\n          <h2>Ongoing is Empty</h2>\n          <p>You have no active orders.</p>\n        </div>\n        <a (click)=\"goBackHistory()\" class=\"btn btn-primary cursor-pointer\">Go Back</a>\n      </div>\n    </section>\n  \n    <!-- Tab content -->\n    <!-- This is the History tab -->\n    <section id=\"history\" \n      *ngIf=\"activeTabType === productCartStatus.COMPLETED\" \n      class=\"tab-content active\" \n      [ngClass]=\"{ 'empty': !((completedDeliveries$ | async)?.length > 0) }\">\n      <div class=\"orders constrain\" *ngIf=\"(completedDeliveries$ | async) as completedDeliveries\">\n        <app-rider-delivery-history-widget \n          [cart]=\"delivery\"\n          *ngFor=\"let delivery of completedDeliveries\">\n        </app-rider-delivery-history-widget>\n      </div>\n  \n      <div class=\"empty-state constrain\">\n        <img src=\"assets/images/others/idle-rider.png\" height=\"300px\" alt=\"Idle Rider\" />\n  \n        <div class=\"text\">\n          <h2>History is Empty</h2>\n          <p>You have no active orders.</p>\n        </div>\n        <a (click)=\"goBackHistory()\" class=\"btn btn-primary cursor-pointer\">Go Back</a>\n      </div>\n    </section>\n\n    <app-footer-expander></app-footer-expander>\n    <app-rider-bottom-navbar></app-rider-bottom-navbar>\n  </main>\n</ion-content>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/user/orders/orders.page.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/user/orders/orders.page.html ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n  <main id=\"orders-page\" class=\"container with-bottom-menu bg-offwhite\">\n    <section id=\"header\" class=\"constrain header-white\">\n      <div class=\"text\">\n        <h2 class=\"name\">Orders</h2>\n      </div>\n    </section>\n  \n    <!-- \n      This is the tab buttons.\n      When you click the buttons they activate the selected tab, clicking it also adds a \".active\" class to both the button of the tab and the tab content.\n      I have already implemented it using javascript.\n    -->\n    <section id=\"tabs\" class=\"constrain\">\n      <button class=\"tab\" \n        (click)=\"switchTabs(productCartStatus.TRANSIT)\" \n        [ngClass]=\"{'active' : activeTabType === productCartStatus.TRANSIT }\">\n        <span>Ongoing</span>\n      </button>\n      <button class=\"tab\" \n        (click)=\"switchTabs(productCartStatus.COMPLETED)\" \n        [ngClass]=\"{'active' : activeTabType === productCartStatus.COMPLETED }\">\n        <span>History</span>\n      </button>\n      <button class=\"tab\" \n        (click)=\"switchTabs(productCartStatus.CANCELLED)\" \n        [ngClass]=\"{'active' : activeTabType === productCartStatus.CANCELLED }\">\n        <span>Cancelled</span>\n      </button>\n    </section>\n  \n    <!-- Tab content -->\n    <!-- The tabs have empty states already designed. You can display them by adding a \".empty\" class to the tab-content section. It'll look like this: \"tab-content empty\". I've done this for the Ongoing tab already.  -->\n    <!-- This is the Ongoing tab -->\n    <section id=\"ongoing\" \n      class=\"tab-content active\" \n      *ngIf=\"activeTabType === productCartStatus.TRANSIT\"\n      [ngClass]=\"{ 'empty': !((transitOrders$ | async)?.length > 0) }\">\n      <div class=\"orders constrain\" *ngIf=\"(transitOrders$ | async) as transitOrders\">\n        <app-order-selected-widget-ongoing \n          [cart]=\"order\"\n          *ngFor=\"let order of transitOrders\">\n        </app-order-selected-widget-ongoing>\n      </div>\n  \n      <!-- This is the empty state of the Ongoing tab -->\n      <app-no-orders-found-widget \n        [showButton]=\"true\" \n        [purpose]=\"'Ongoing'\">\n      </app-no-orders-found-widget>\n    </section>\n  \n    <!-- Tab content -->\n    <!-- This is the History tab -->\n    <section id=\"history\" \n      class=\"tab-content active\" \n      *ngIf=\"activeTabType === productCartStatus.COMPLETED\"\n      [ngClass]=\"{ 'empty': !((completedOrders$ | async)?.length > 0) }\">\n      <div class=\"orders constrain\" *ngIf=\"(completedOrders$ | async) as completedOrders\">\n        <app-order-selected-widget-history\n          [cart]=\"order\"\n          *ngFor=\"let order of completedOrders\">\n        </app-order-selected-widget-history>\n      </div>\n  \n      <app-no-orders-found-widget \n      [showButton]=\"true\"\n      [purpose]=\"'History'\">\n    </app-no-orders-found-widget>\n    </section>\n  \n    <!-- Tab content -->\n    <!-- This is the Draft tab -->\n    <section id=\"draft\" \n      class=\"tab-content active\" \n      *ngIf=\"activeTabType === productCartStatus.CANCELLED\"\n      [ngClass]=\"{ 'empty': !((cancelledOrders$ | async)?.length > 0) }\">\n      <div class=\"orders constrain\" *ngIf=\"(cancelledOrders$ | async) as cancelledOrders\">\n        <app-order-selected-widget-draft\n          [cart]=\"order\"\n          *ngFor=\"let order of cancelledOrders\">\n        </app-order-selected-widget-draft>\n      </div>\n  \n      <app-no-orders-found-widget \n        [showButton]=\"true\" \n        [purpose]=\"'Cancelled'\">\n      </app-no-orders-found-widget>\n    </section>\n\n    <app-footer-expander></app-footer-expander>\n\n    <!-- This is that menu at the bottom of every page -->\n    <app-user-bottom-navbar></app-user-bottom-navbar>\n    </main>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/rider/orders/orders-routing.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/rider/orders/orders-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: OrdersPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrdersPageRoutingModule", function() { return OrdersPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _orders_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./orders.page */ "./src/app/rider/orders/orders.page.ts");




const routes = [
    {
        path: '',
        component: _orders_page__WEBPACK_IMPORTED_MODULE_3__["OrdersPage"]
    }
];
let OrdersPageRoutingModule = class OrdersPageRoutingModule {
};
OrdersPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], OrdersPageRoutingModule);



/***/ }),

/***/ "./src/app/rider/orders/orders.module.ts":
/*!***********************************************!*\
  !*** ./src/app/rider/orders/orders.module.ts ***!
  \***********************************************/
/*! exports provided: OrdersPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrdersPageModule", function() { return OrdersPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _orders_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./orders-routing.module */ "./src/app/rider/orders/orders-routing.module.ts");
/* harmony import */ var _orders_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./orders.page */ "./src/app/rider/orders/orders.page.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../shared/shared.module */ "./src/app/shared/shared.module.ts");








let OrdersPageModule = class OrdersPageModule {
};
OrdersPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"],
            _orders_routing_module__WEBPACK_IMPORTED_MODULE_5__["OrdersPageRoutingModule"]
        ],
        declarations: [_orders_page__WEBPACK_IMPORTED_MODULE_6__["OrdersPage"]]
    })
], OrdersPageModule);



/***/ }),

/***/ "./src/app/rider/orders/orders.page.scss":
/*!***********************************************!*\
  !*** ./src/app/rider/orders/orders.page.scss ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3JpZGVyL29yZGVycy9vcmRlcnMucGFnZS5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/rider/orders/orders.page.ts":
/*!*********************************************!*\
  !*** ./src/app/rider/orders/orders.page.ts ***!
  \*********************************************/
/*! exports provided: OrdersPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrdersPage", function() { return OrdersPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");
/* harmony import */ var subsink__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! subsink */ "./node_modules/subsink/dist/es2015/index.js");
/* harmony import */ var _store_actions_rider_action__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../store/actions/rider.action */ "./src/app/rider/store/actions/rider.action.ts");
/* harmony import */ var _user_store_model_user_model__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../user/store/model/user.model */ "./src/app/user/store/model/user.model.ts");
/* harmony import */ var _utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../utils/functions/app.functions */ "./src/utils/functions/app.functions.ts");
/* harmony import */ var _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../utils/types/app.constant */ "./src/utils/types/app.constant.ts");
/* harmony import */ var _app_services_navigation_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../app/services/navigation.service */ "./src/app/services/navigation.service.ts");










let OrdersPage = class OrdersPage {
    constructor(activatedRoute, store, navigationSrv) {
        this.activatedRoute = activatedRoute;
        this.store = store;
        this.navigationSrv = navigationSrv;
        this.activeTabType = _user_store_model_user_model__WEBPACK_IMPORTED_MODULE_6__["ProductCartStatus"].TRANSIT;
        this.productCartStatus = _user_store_model_user_model__WEBPACK_IMPORTED_MODULE_6__["ProductCartStatus"];
        this.subSink = new subsink__WEBPACK_IMPORTED_MODULE_4__["SubSink"]();
    }
    ngOnInit() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const { userId } = yield Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_7__["getDataFromLocalStorage"])(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_8__["LocalStorageKey"].ZERO_30_USER);
            unBundleSVG();
            this.subSink.sink =
                this.activatedRoute
                    .queryParams
                    .subscribe((data) => {
                    if (data === null || data === void 0 ? void 0 : data.type) {
                        this.activeTabType = data.type;
                    }
                });
            if (userId) {
                this.store.dispatch(_store_actions_rider_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetRiderActivityHistoryInitiatedAction({ payload: { userId } }));
                this.completedDeliveries$ = this.store.select((data) => data.Rider.CompletedDeliveries);
                this.ongoingDeliveries$ = this.store.select((data) => data.Rider.OngoingDeliveries);
                this.allDeliveries$ = this.store.select((data) => data.Rider.DeliveryActivities);
            }
        });
    }
    switchTabs(tab) {
        this.activeTabType = tab;
    }
    goBackHistory() {
        this.navigationSrv.goBackOnNative();
    }
    ngOnDestroy() {
        this.subSink.unsubscribe();
    }
};
OrdersPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _ngrx_store__WEBPACK_IMPORTED_MODULE_3__["Store"] },
    { type: _app_services_navigation_service__WEBPACK_IMPORTED_MODULE_9__["NavigationService"] }
];
OrdersPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-orders",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./orders.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/rider/orders/orders.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./orders.page.scss */ "./src/app/rider/orders/orders.page.scss")).default]
    })
], OrdersPage);



/***/ }),

/***/ "./src/app/user/orders/orders-routing.module.ts":
/*!******************************************************!*\
  !*** ./src/app/user/orders/orders-routing.module.ts ***!
  \******************************************************/
/*! exports provided: OrdersPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrdersPageRoutingModule", function() { return OrdersPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _orders_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./orders.page */ "./src/app/user/orders/orders.page.ts");




const routes = [
    {
        path: '',
        component: _orders_page__WEBPACK_IMPORTED_MODULE_3__["OrdersPage"]
    },
    {
        path: 'track-order',
        loadChildren: () => __webpack_require__.e(/*! import() | track-order-track-order-module */ "track-order-track-order-module").then(__webpack_require__.bind(null, /*! ./track-order/track-order.module */ "./src/app/user/orders/track-order/track-order.module.ts")).then(m => m.TrackOrderPageModule)
    },
    {
        path: 'order-info',
        loadChildren: () => Promise.all(/*! import() | order-info-order-info-module */[__webpack_require__.e("default~order-info-order-info-module~orders-order-info-order-info-module"), __webpack_require__.e("common"), __webpack_require__.e("order-info-order-info-module")]).then(__webpack_require__.bind(null, /*! ./order-info/order-info.module */ "./src/app/user/orders/order-info/order-info.module.ts")).then(m => m.OrderInfoPageModule)
    }
];
let OrdersPageRoutingModule = class OrdersPageRoutingModule {
};
OrdersPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], OrdersPageRoutingModule);



/***/ }),

/***/ "./src/app/user/orders/orders.module.ts":
/*!**********************************************!*\
  !*** ./src/app/user/orders/orders.module.ts ***!
  \**********************************************/
/*! exports provided: OrdersPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrdersPageModule", function() { return OrdersPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _orders_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./orders-routing.module */ "./src/app/user/orders/orders-routing.module.ts");
/* harmony import */ var _orders_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./orders.page */ "./src/app/user/orders/orders.page.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../shared/shared.module */ "./src/app/shared/shared.module.ts");








let OrdersPageModule = class OrdersPageModule {
};
OrdersPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _orders_routing_module__WEBPACK_IMPORTED_MODULE_5__["OrdersPageRoutingModule"],
            _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"]
        ],
        declarations: [_orders_page__WEBPACK_IMPORTED_MODULE_6__["OrdersPage"]]
    })
], OrdersPageModule);



/***/ }),

/***/ "./src/app/user/orders/orders.page.scss":
/*!**********************************************!*\
  !*** ./src/app/user/orders/orders.page.scss ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXIvb3JkZXJzL29yZGVycy5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/user/orders/orders.page.ts":
/*!********************************************!*\
  !*** ./src/app/user/orders/orders.page.ts ***!
  \********************************************/
/*! exports provided: OrdersPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrdersPage", function() { return OrdersPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");
/* harmony import */ var _store_model_user_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../store/model/user.model */ "./src/app/user/store/model/user.model.ts");
/* harmony import */ var _user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../user/store/actions/user.action */ "./src/app/user/store/actions/user.action.ts");
/* harmony import */ var _utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../utils/functions/app.functions */ "./src/utils/functions/app.functions.ts");
/* harmony import */ var _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../utils/types/app.constant */ "./src/utils/types/app.constant.ts");







let OrdersPage = class OrdersPage {
    constructor(store) {
        this.store = store;
        this.activeTabType = _store_model_user_model__WEBPACK_IMPORTED_MODULE_3__["ProductCartStatus"].TRANSIT;
        this.productCartStatus = _store_model_user_model__WEBPACK_IMPORTED_MODULE_3__["ProductCartStatus"];
    }
    ngOnInit() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const { userId } = yield Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_5__["getDataFromLocalStorage"])(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_6__["LocalStorageKey"].ZERO_30_USER);
            if (userId) {
                unBundleSVG();
                this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_4__["actions"].GetUserOrderInitiatedAction({ payload: { userId } }));
                this.completedOrders$ = this.store.select((data) => data.User.CompletedOrders);
                this.transitOrders$ = this.store.select((data) => data.User.TransitOrders);
                this.cancelledOrders$ = this.store.select((data) => data.User.CancelledOrders);
                this.unVerifiedOrders$ = this.store.select((data) => data.User.UnverifiedOrders);
            }
        });
    }
    switchTabs(tab) {
        this.activeTabType = tab;
    }
};
OrdersPage.ctorParameters = () => [
    { type: _ngrx_store__WEBPACK_IMPORTED_MODULE_2__["Store"] }
];
OrdersPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-orders",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./orders.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/user/orders/orders.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./orders.page.scss */ "./src/app/user/orders/orders.page.scss")).default]
    })
], OrdersPage);



/***/ })

}]);
//# sourceMappingURL=orders-orders-module-es2015.js.map