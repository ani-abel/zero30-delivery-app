(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["nearby-nearby-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/user/nearby/nearby.page.html":
    /*!************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/user/nearby/nearby.page.html ***!
      \************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppUserNearbyNearbyPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n  <main id=\"nearby-page\" class=\"container with-bottom-menu bg-offwhite\">\n    <section id=\"header\" class=\"constrain header-white\">\n      <div class=\"text\">\n        <h2 class=\"name\">Nearby</h2>\n      </div>\n      <!-- This select is used to change the location of the user so he sees Nearby Restaurants. -->\n      <!-- <div class=\"select\">\n        <div class=\"icon\">\n          <img class=\"svg\" src=\"assets/images/icons/map-pointer.svg\" height=\"15px\" alt=\"Map Pointer\">\n        </div>\n        <select name=\"location\" id=\"location\">\n          <option value=\"Uwani\">Uwani</option>\n          <option value=\"Nsukka\">Nsukka</option>\n        </select>\n        <div class=\"chevron-icon\">\n          <img class=\"svg\" src=\"assets/images/icons/chevron-down.svg\" height=\"7px\" alt=\"Select\">\n        </div>\n      </div> -->\n    </section>\n  \n    <!-- This is that menu at the bottom of every page -->\n    <app-user-bottom-navbar></app-user-bottom-navbar>\n  \n    <!-- \n      This is the tab buttons.\n      When you click the buttons they activate the selected tab, clicking it also adds a \".active\" class to both the button of the tab and the tab content.\n      I have already implemented it using javascript.\n    -->\n    <section id=\"tabs\" class=\"constrain\" \n      *ngIf=\"(merchantTypes$ | async) as merchantTypes\">\n      <button class=\"tab\" \n        *ngFor=\"let merchantType of merchantTypes\"\n        [ngClass]=\"{ 'active': activeTabType.id === merchantType.id }\" \n        (click)=\"switchTabs(merchantType)\">\n        <span>{{ merchantType.name | uppercase }}</span>\n      </button>\n    </section>\n\n    <!-- Tab content -->\n    <!-- This is the Food tab -->\n    <section id=\"food\" \n      class=\"tab-content nearby-businesses active\" \n      *ngIf=\"(merchantsOrderedByType$ | async) as merchants\">\n      <div class=\"businesses constrain\" *ngIf=\"merchants.length > 0\">\n  \n        <!-- \n          There's a \".bookmarked\" class on the products that are bookmarked by the user, Once the .bookmarked tag is added as a class it styles the bookmark icon on top of the product image.\n        -->\n        <app-nearby-item-widget \n          (addToBookmark)=\"addMerchantToBookmark($event)\"\n          [merchant]=\"merchant\" \n          [currentIndex]=\"i\"\n          [arrayLength]=\"(merchants.length - 1)\"\n          *ngFor=\"let merchant of merchants; index as i\">\n        </app-nearby-item-widget>\n      </div>\n    </section>\n  </main>\n\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/user/nearby/nearby-routing.module.ts":
    /*!******************************************************!*\
      !*** ./src/app/user/nearby/nearby-routing.module.ts ***!
      \******************************************************/

    /*! exports provided: NearbyPageRoutingModule */

    /***/
    function srcAppUserNearbyNearbyRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "NearbyPageRoutingModule", function () {
        return NearbyPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _nearby_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./nearby.page */
      "./src/app/user/nearby/nearby.page.ts");

      var routes = [{
        path: '',
        component: _nearby_page__WEBPACK_IMPORTED_MODULE_3__["NearbyPage"]
      }, {
        path: 'business-detail',
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | business-detail-business-detail-module */
          "business-detail-business-detail-module").then(__webpack_require__.bind(null,
          /*! ./business-detail/business-detail.module */
          "./src/app/user/nearby/business-detail/business-detail.module.ts")).then(function (m) {
            return m.BusinessDetailPageModule;
          });
        }
      }];

      var NearbyPageRoutingModule = function NearbyPageRoutingModule() {
        _classCallCheck(this, NearbyPageRoutingModule);
      };

      NearbyPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], NearbyPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/user/nearby/nearby.module.ts":
    /*!**********************************************!*\
      !*** ./src/app/user/nearby/nearby.module.ts ***!
      \**********************************************/

    /*! exports provided: NearbyPageModule */

    /***/
    function srcAppUserNearbyNearbyModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "NearbyPageModule", function () {
        return NearbyPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _nearby_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./nearby-routing.module */
      "./src/app/user/nearby/nearby-routing.module.ts");
      /* harmony import */


      var _nearby_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./nearby.page */
      "./src/app/user/nearby/nearby.page.ts");
      /* harmony import */


      var src_app_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/shared/shared.module */
      "./src/app/shared/shared.module.ts");

      var NearbyPageModule = function NearbyPageModule() {
        _classCallCheck(this, NearbyPageModule);
      };

      NearbyPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], src_app_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"], _nearby_routing_module__WEBPACK_IMPORTED_MODULE_5__["NearbyPageRoutingModule"]],
        declarations: [_nearby_page__WEBPACK_IMPORTED_MODULE_6__["NearbyPage"]]
      })], NearbyPageModule);
      /***/
    },

    /***/
    "./src/app/user/nearby/nearby.page.scss":
    /*!**********************************************!*\
      !*** ./src/app/user/nearby/nearby.page.scss ***!
      \**********************************************/

    /*! exports provided: default */

    /***/
    function srcAppUserNearbyNearbyPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXIvbmVhcmJ5L25lYXJieS5wYWdlLnNjc3MifQ== */";
      /***/
    },

    /***/
    "./src/app/user/nearby/nearby.page.ts":
    /*!********************************************!*\
      !*** ./src/app/user/nearby/nearby.page.ts ***!
      \********************************************/

    /*! exports provided: NearbyPage */

    /***/
    function srcAppUserNearbyNearbyPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "NearbyPage", function () {
        return NearbyPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ngrx_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ngrx/store */
      "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");
      /* harmony import */


      var subsink__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! subsink */
      "./node_modules/subsink/dist/es2015/index.js");
      /* harmony import */


      var _user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../../user/store/actions/user.action */
      "./src/app/user/store/actions/user.action.ts");
      /* harmony import */


      var _utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../../../utils/functions/app.functions */
      "./src/utils/functions/app.functions.ts");
      /* harmony import */


      var _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ../../../utils/types/app.constant */
      "./src/utils/types/app.constant.ts");

      var NearbyPage = /*#__PURE__*/function () {
        function NearbyPage(store) {
          _classCallCheck(this, NearbyPage);

          this.store = store;
          this.subSink = new subsink__WEBPACK_IMPORTED_MODULE_3__["SubSink"]();
          this.merchantTypeExists = false;
        }

        _createClass(NearbyPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var _this = this;

              var userData;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_5__["getDataFromLocalStorage"])(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_6__["LocalStorageKey"].ZERO_30_USER);

                    case 2:
                      userData = _context.sent;

                      if (userData.userId) {
                        this.userId = userData.userId;
                        this.merchantTypes$ = this.store.select(function (data) {
                          return data.User.MerchantType;
                        }); //? Check if merchant Types exist in cache

                        this.subSink.sink = this.merchantTypes$.subscribe(function (data) {
                          if ((data === null || data === void 0 ? void 0 : data.length) > 0) {
                            _this.merchantTypeExists = true;
                          }
                        }); //? If the merchant Types does not exist, call data from API

                        if (!this.merchantTypeExists) {
                          this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_4__["actions"].GetMerchantTypeInitiatedAction());
                        }

                        this.subSink.sink = this.merchantTypes$.subscribe(function (data) {
                          var _a;

                          _this.merchantTypes = data;
                          _this.activeTabType = data.find(function (dt) {
                            return dt.id === 1;
                          });

                          if ((_a = _this.activeTabType) === null || _a === void 0 ? void 0 : _a.id) {
                            _this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_4__["actions"].GetNearbyMerchantsInitiatedAction({
                              payload: {
                                typeId: _this.activeTabType.id,
                                userId: _this.userId
                              }
                            }));
                          }
                        });
                        this.merchantsOrderedByType$ = this.store.select(function (data) {
                          return data.User.NearbyMerchantsByMerchantType;
                        });
                      }

                    case 4:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "switchTabs",
          value: function switchTabs(tab) {
            this.activeTabType = tab;
            this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_4__["actions"].GetNearbyMerchantsInitiatedAction({
              payload: {
                typeId: tab.id,
                userId: this.userId
              }
            }));
          }
        }, {
          key: "addMerchantToBookmark",
          value: function addMerchantToBookmark(event) {
            if (this.userId) {
              //? Dispatch action
              this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_4__["actions"].AddMerchantToUserBookmarksInitiatedAction({
                payload: {
                  MerchantId: event.id,
                  UserId: this.userId,
                  Merchant: event
                }
              }));
            }
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            this.subSink.unsubscribe();
          }
        }]);

        return NearbyPage;
      }();

      NearbyPage.ctorParameters = function () {
        return [{
          type: _ngrx_store__WEBPACK_IMPORTED_MODULE_2__["Store"]
        }];
      };

      NearbyPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-nearby',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./nearby.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/user/nearby/nearby.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./nearby.page.scss */
        "./src/app/user/nearby/nearby.page.scss"))["default"]]
      })], NearbyPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=nearby-nearby-module-es5.js.map