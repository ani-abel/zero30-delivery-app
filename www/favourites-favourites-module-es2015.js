(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["favourites-favourites-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/user/favourites/favourites.page.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/user/favourites/favourites.page.html ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n    <main id=\"favourites-page\" class=\"container with-bottom-menu bg-offwhite\">\n        <section id=\"header\" class=\"constrain header-white\">\n            <div class=\"text\">\n                <h2 class=\"name\">Favourites</h2>\n            </div>\n        </section>\n        \n        <!-- This is that menu at the bottom of every page -->\n        <app-user-bottom-navbar></app-user-bottom-navbar>\n    \n        <!-- \n            This is the tab buttons.\n            When you click the buttons they activate the selected tab, clicking it also adds a \".active\" class to both the button of the tab and the tab content.\n            I have already implemented it using javascript.\n        -->\n        <section id=\"tabs\" class=\"constrain\">\n            <button class=\"tab\" \n                [ngClass]=\"{ 'active': activeTabType === 'PRODUCT' }\" \n                (click)=\"switchTabs('PRODUCT')\">\n                <span>Products</span>\n            </button>\n            <button class=\"tab\" \n                [ngClass]=\"{ 'active': activeTabType === 'MERCHANT' }\" \n                (click)=\"switchTabs('MERCHANT')\">\n                <span>Merchants</span>\n            </button>\n        </section>\n    \n        <!-- Tab content -->\n        <!-- This is the Foods tab -->\n        <section id=\"foods\" class=\"tab-content category active\" *ngIf=\"activeTabType === 'PRODUCT'\">\n            <div class=\"favourites constrain\" *ngIf=\"(favouriteProducts$ | async)?.length > 0; else defaultTemplate\">\n                <app-favorite-food-widget \n                    (viewProductDetailEvent)=\"openProductDetail($event, product)\"\n                    [product]=\"product\"\n                    *ngFor=\"let product of (favouriteProducts$ | async)\">\n                </app-favorite-food-widget>\n            </div>\n        </section>\n    \n        <!-- Tab content -->\n        <!-- This is the Restaurants tab -->\n        <section id=\"restaurants\" class=\"tab-content category active\" *ngIf=\"activeTabType === 'MERCHANT'\">\n            <div class=\"favourites constrain\" *ngIf=\"(favouriteMerchants$ | async)?.length > 0; else defaultTemplate\">\n                <app-favorite-resturant-widget \n                    *ngFor=\"let merchant of (favouriteMerchants$ | async); index as i\"\n                    [merchant]=\"merchant\"\n                    (addToBookmark)=\"addMerchantToBookmark($event)\">\n                </app-favorite-resturant-widget>\n            </div>\n        </section>\n\n        <ng-template #defaultTemplate>\n            <app-no-content-found></app-no-content-found>\n        </ng-template>\n\n        <app-footer-expander></app-footer-expander>\n    </main>\n</ion-content>");

/***/ }),

/***/ "./src/app/user/favourites/favourites-routing.module.ts":
/*!**************************************************************!*\
  !*** ./src/app/user/favourites/favourites-routing.module.ts ***!
  \**************************************************************/
/*! exports provided: FavouritesPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FavouritesPageRoutingModule", function() { return FavouritesPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _favourites_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./favourites.page */ "./src/app/user/favourites/favourites.page.ts");




const routes = [
    {
        path: '',
        component: _favourites_page__WEBPACK_IMPORTED_MODULE_3__["FavouritesPage"]
    }
];
let FavouritesPageRoutingModule = class FavouritesPageRoutingModule {
};
FavouritesPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], FavouritesPageRoutingModule);



/***/ }),

/***/ "./src/app/user/favourites/favourites.module.ts":
/*!******************************************************!*\
  !*** ./src/app/user/favourites/favourites.module.ts ***!
  \******************************************************/
/*! exports provided: FavouritesPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FavouritesPageModule", function() { return FavouritesPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _favourites_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./favourites-routing.module */ "./src/app/user/favourites/favourites-routing.module.ts");
/* harmony import */ var _favourites_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./favourites.page */ "./src/app/user/favourites/favourites.page.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../shared/shared.module */ "./src/app/shared/shared.module.ts");








let FavouritesPageModule = class FavouritesPageModule {
};
FavouritesPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _favourites_routing_module__WEBPACK_IMPORTED_MODULE_5__["FavouritesPageRoutingModule"],
            _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"]
        ],
        declarations: [_favourites_page__WEBPACK_IMPORTED_MODULE_6__["FavouritesPage"]]
    })
], FavouritesPageModule);



/***/ }),

/***/ "./src/app/user/favourites/favourites.page.scss":
/*!******************************************************!*\
  !*** ./src/app/user/favourites/favourites.page.scss ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXIvZmF2b3VyaXRlcy9mYXZvdXJpdGVzLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/user/favourites/favourites.page.ts":
/*!****************************************************!*\
  !*** ./src/app/user/favourites/favourites.page.ts ***!
  \****************************************************/
/*! exports provided: TabType, FavouritesPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabType", function() { return TabType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FavouritesPage", function() { return FavouritesPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");
/* harmony import */ var subsink__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! subsink */ "./node_modules/subsink/dist/es2015/index.js");
/* harmony import */ var _user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../user/store/actions/user.action */ "./src/app/user/store/actions/user.action.ts");
/* harmony import */ var _utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../utils/functions/app.functions */ "./src/utils/functions/app.functions.ts");
/* harmony import */ var _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../utils/types/app.constant */ "./src/utils/types/app.constant.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");








var TabType;
(function (TabType) {
    TabType["PRODUCT"] = "PRODUCT";
    TabType["MERCHANT"] = "MERCHANT";
})(TabType || (TabType = {}));
let FavouritesPage = class FavouritesPage {
    constructor(store, router) {
        this.store = store;
        this.router = router;
        this.activeTabType = TabType.PRODUCT;
        this.subSink = new subsink__WEBPACK_IMPORTED_MODULE_3__["SubSink"]();
    }
    ngOnInit() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            // ? Check for favourites
            this.favouriteMerchants$ = this.store.select((data) => data.User.FavouriteMerchants);
            this.favouriteProducts$ = this.store.select((data) => data.User.FavouriteProducts);
            this.subSink.sink =
                this.favouriteMerchants$.subscribe((data) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                    if ((data === null || data === void 0 ? void 0 : data.length) < 1) {
                        this.isInAppStore = true;
                    }
                }));
            if (this.isInAppStore) {
                const { userId } = yield Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_5__["getDataFromLocalStorage"])(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_6__["LocalStorageKey"].ZERO_30_USER);
                if (userId) {
                    this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_4__["actions"].GetUserFavouritesInitiatedAction({ payload: { userId } }));
                }
            }
        });
    }
    openProductDetail(event, product) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (product === null || product === void 0 ? void 0 : product.productModel) {
                this.router.navigateByUrl(`/user/product-detail?product=${JSON.stringify(product)}`);
            }
        });
    }
    switchTabs(tab) {
        this.activeTabType = tab;
    }
    addMerchantToBookmark(event) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            //? Get the user Id
            const { userId } = yield Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_5__["getDataFromLocalStorage"])(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_6__["LocalStorageKey"].ZERO_30_USER);
            if (userId) {
                //? Dispatch action
                this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_4__["actions"].AddMerchantToUserBookmarksInitiatedAction({ payload: { MerchantId: event.id, UserId: userId, Merchant: event } }));
            }
        });
    }
    ngOnDestroy() {
        this.subSink.unsubscribe();
    }
};
FavouritesPage.ctorParameters = () => [
    { type: _ngrx_store__WEBPACK_IMPORTED_MODULE_2__["Store"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"] }
];
FavouritesPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-favourites',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./favourites.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/user/favourites/favourites.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./favourites.page.scss */ "./src/app/user/favourites/favourites.page.scss")).default]
    })
], FavouritesPage);



/***/ })

}]);
//# sourceMappingURL=favourites-favourites-module-es2015.js.map