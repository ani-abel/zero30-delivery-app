(function () {
  function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e2) { throw _e2; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e3) { didErr = true; err = _e3; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

  function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

  function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

  function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

  function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

  function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

  function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"], {
    /***/
    "./$$_lazy_route_resource lazy recursive":
    /*!******************************************************!*\
      !*** ./$$_lazy_route_resource lazy namespace object ***!
      \******************************************************/

    /*! no static exports found */

    /***/
    function $$_lazy_route_resourceLazyRecursive(module, exports) {
      function webpackEmptyAsyncContext(req) {
        // Here Promise.resolve().then() is used instead of new Promise() to prevent
        // uncaught exception popping up in devtools
        return Promise.resolve().then(function () {
          var e = new Error("Cannot find module '" + req + "'");
          e.code = 'MODULE_NOT_FOUND';
          throw e;
        });
      }

      webpackEmptyAsyncContext.keys = function () {
        return [];
      };

      webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
      module.exports = webpackEmptyAsyncContext;
      webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";
      /***/
    },

    /***/
    "./node_modules/@ionic/core/dist/esm lazy recursive ^\\.\\/.*\\.entry\\.js$ include: \\.entry\\.js$ exclude: \\.system\\.entry\\.js$":
    /*!*****************************************************************************************************************************************!*\
      !*** ./node_modules/@ionic/core/dist/esm lazy ^\.\/.*\.entry\.js$ include: \.entry\.js$ exclude: \.system\.entry\.js$ namespace object ***!
      \*****************************************************************************************************************************************/

    /*! no static exports found */

    /***/
    function node_modulesIonicCoreDistEsmLazyRecursiveEntryJs$IncludeEntryJs$ExcludeSystemEntryJs$(module, exports, __webpack_require__) {
      var map = {
        "./ion-action-sheet.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-action-sheet.entry.js", "common", 0],
        "./ion-alert.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-alert.entry.js", "common", 1],
        "./ion-app_8.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-app_8.entry.js", "common", 2],
        "./ion-avatar_3.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-avatar_3.entry.js", "common", 3],
        "./ion-back-button.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-back-button.entry.js", "common", 4],
        "./ion-backdrop.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-backdrop.entry.js", 5],
        "./ion-button_2.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-button_2.entry.js", "common", 6],
        "./ion-card_5.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-card_5.entry.js", "common", 7],
        "./ion-checkbox.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-checkbox.entry.js", "common", 8],
        "./ion-chip.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-chip.entry.js", "common", 9],
        "./ion-col_3.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-col_3.entry.js", 10],
        "./ion-datetime_3.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-datetime_3.entry.js", "common", 11],
        "./ion-fab_3.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-fab_3.entry.js", "common", 12],
        "./ion-img.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-img.entry.js", 13],
        "./ion-infinite-scroll_2.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-infinite-scroll_2.entry.js", 14],
        "./ion-input.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-input.entry.js", "common", 15],
        "./ion-item-option_3.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-item-option_3.entry.js", "common", 16],
        "./ion-item_8.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-item_8.entry.js", "common", 17],
        "./ion-loading.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-loading.entry.js", "common", 18],
        "./ion-menu_3.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-menu_3.entry.js", "common", 19],
        "./ion-modal.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-modal.entry.js", "common", 20],
        "./ion-nav_2.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-nav_2.entry.js", "common", 21],
        "./ion-popover.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-popover.entry.js", "common", 22],
        "./ion-progress-bar.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-progress-bar.entry.js", "common", 23],
        "./ion-radio_2.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-radio_2.entry.js", "common", 24],
        "./ion-range.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-range.entry.js", "common", 25],
        "./ion-refresher_2.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-refresher_2.entry.js", "common", 26],
        "./ion-reorder_2.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-reorder_2.entry.js", "common", 27],
        "./ion-ripple-effect.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-ripple-effect.entry.js", 28],
        "./ion-route_4.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-route_4.entry.js", "common", 29],
        "./ion-searchbar.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-searchbar.entry.js", "common", 30],
        "./ion-segment_2.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-segment_2.entry.js", "common", 31],
        "./ion-select_3.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-select_3.entry.js", "common", 32],
        "./ion-slide_2.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-slide_2.entry.js", 33],
        "./ion-spinner.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-spinner.entry.js", "common", 34],
        "./ion-split-pane.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-split-pane.entry.js", 35],
        "./ion-tab-bar_2.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-tab-bar_2.entry.js", "common", 36],
        "./ion-tab_2.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-tab_2.entry.js", "common", 37],
        "./ion-text.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-text.entry.js", "common", 38],
        "./ion-textarea.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-textarea.entry.js", "common", 39],
        "./ion-toast.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-toast.entry.js", "common", 40],
        "./ion-toggle.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-toggle.entry.js", "common", 41],
        "./ion-virtual-scroll.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-virtual-scroll.entry.js", 42]
      };

      function webpackAsyncContext(req) {
        if (!__webpack_require__.o(map, req)) {
          return Promise.resolve().then(function () {
            var e = new Error("Cannot find module '" + req + "'");
            e.code = 'MODULE_NOT_FOUND';
            throw e;
          });
        }

        var ids = map[req],
            id = ids[0];
        return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function () {
          return __webpack_require__(id);
        });
      }

      webpackAsyncContext.keys = function webpackAsyncContextKeys() {
        return Object.keys(map);
      };

      webpackAsyncContext.id = "./node_modules/@ionic/core/dist/esm lazy recursive ^\\.\\/.*\\.entry\\.js$ include: \\.entry\\.js$ exclude: \\.system\\.entry\\.js$";
      module.exports = webpackAsyncContext;
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html":
    /*!**************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
      \**************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppAppComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-app>\n    <app-loader *ngIf=\"(isLoading$ | async)\"></app-loader>\n\n    <app-loader *ngIf=\"(isUserLoading$ | async)\"></app-loader> \n\n    <app-loader *ngIf=\"(isRiderLoading$ | async)\"></app-loader>\n    \n    <app-error-message \n        *ngIf=\"(error$ | async) as err\" [error]=\"err\" \n        (closeAlert)=\"clearError()\">\n      </app-error-message>\n\n    <app-error-message \n        *ngIf=\"(userError$ | async) as err\" [error]=\"err\" \n        (closeAlert)=\"clearError()\">\n      </app-error-message>\n\n    <app-error-message \n      *ngIf=\"(riderError$ | async) as err\" [error]=\"err\" \n      (closeAlert)=\"clearError()\">\n      </app-error-message>\n\n    <app-success-message \n      *ngIf=\"(message$ | async) as msg\" \n      [message]=\"msg\"\n      (closeAlert)=\"clearMessage()\"></app-success-message>\n\n    <app-success-message \n      *ngIf=\"(userMessage$ | async) as msg\" \n      [message]=\"msg\"\n      (closeAlert)=\"clearMessage()\"></app-success-message>\n\n    <app-success-message \n      *ngIf=\"(riderMessage$ | async) as msg\" \n      [message]=\"msg\"\n      (closeAlert)=\"clearMessage()\"></app-success-message>\n\n    <app-clickable-mesage \n      *ngIf=\"(activeActionMessage$ | async) as message\"\n      [message]=\"message\">\n    </app-clickable-mesage>\n\n    <app-clickable-mesage \n      *ngIf=\"(riderActiveActionMessage$ | async) as message\"\n      [message]=\"message\">\n    </app-clickable-mesage>\n\n    <app-cart-badge \n      *ngIf=\"(cartItemsCount$ | async) as cartItemCount\" \n      [itemCount]=\"cartItemCount\">\n    </app-cart-badge>\n\n  <ion-router-outlet></ion-router-outlet>\n</ion-app>\n";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/accordian/accordian.component.html":
    /*!*************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/accordian/accordian.component.html ***!
      \*************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppSharedAccordianAccordianComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<main class=\"main\">\n  <header>\n    <div class=\"header-section\">\n      <h4>\n        <img class=\"icon\" \n          [src]=\"cartCheckList.mechantLogo | filePathFormatter\" \n          [alt]=\"cartCheckList.mechantName\" />\n        <ng-template #defaultLogo>\n          <img class=\"icon\" src=\"assets/images/humans/1.png\" [alt]=\"cartCheckList.mechantName\" />\n        </ng-template>&nbsp;\n        {{ cartCheckList.mechantName | capitalize }}\n      </h4>\n    </div>\n    <div class=\"header-section\">\n      <p class=\"text-right\">\n        <i class=\"fas fa-caret-down cursor-pointer\" \n          [ngClass]=\"{ 'fa-caret-up': isAccordianExpanded }\" \n          (click)=\"toggleAccordian()\"></i>\n      </p>\n    </div>\n  </header>\n\n  <div class=\"product-list\" *ngIf=\"isAccordianExpanded\">\n    <ion-item-group>\n      <ion-item \n        [lines]=\"last ? 'none' : null\" \n        *ngFor=\"let product of cartCheckList.productOrderDetails; let last = last\">\n        <ion-label>\n          <img class=\"icon\" \n            [src]=\"product.productLogo | filePathFormatter\" \n            [alt]=\"product.productName\" />\n          <ng-template #defaultProductImage>\n            <img class=\"icon\" \n              src=\"assets/images/humans/rider.png\" \n              [alt]=\"product.productName\" />\n          </ng-template>&nbsp; \n          {{ product.productName | capitalize }} ({{ product.quantity }})\n        </ion-label>\n        <ion-badge color=\"warning\" slot=\"end\">\n          {{ product.totalPrice | currency}}\n        </ion-badge>\n      </ion-item>\n    </ion-item-group>\n  </div>\n</main>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/add-to-cart/add-to-cart.component.html":
    /*!*****************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/add-to-cart/add-to-cart.component.html ***!
      \*****************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppSharedAddToCartAddToCartComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<!-- \n      This is the popup that comes up when you click the add to cart button (the plus icon beside every product)\n    -->\n    <section id=\"addtocart-popup\" class=\"popup\" [ngClass]=\"{ 'active': show }\">\n      <div class=\"popup-content\">\n        <!-- This button closes the popup, it has been implemented. -->\n        <button type=\"button\" class=\"close-popup\" (click)=\"closeAddToCartModal()\">\n          <img class=\"svg\" src=\"assets/images/icons/close-popup.svg\" height=\"10px\" alt=\"Close Popup\">\n        </button>\n  \n        <h5 class=\"title constrain\">Add New Item</h5>\n  \n        <form [formGroup]=\"addDeliveryAddressForm\" (ngSubmit)=\"onSubmit()\">\n          <div class=\"product-details constrain\">\n            <a class=\"image cursor-pointer\">\n              <img [src]=\"productImages[0].path | filePathFormatter\"\n                height=\"120px\" \n                *ngIf=\"productImages?.length > 0; else defaultImage\"\n                [alt]=\"product.productName\" />\n              <ng-template #defaultImage>\n                <img src=\"assets/images/products/4.png\" height=\"120px\" [alt]=\"product.productName\" />\n              </ng-template>\n            </a>\n  \n            <div class=\"details\">\n              <a class=\"name cursor-pointer\">{{ product.productName | uppercase }}</a>\n              <p class=\"bag\">\n                <img class=\"svg icon\" alt=\"\" src=\"assets/images/icons/bag.svg\">\n                <span>99+</span>\n              </p>\n  \n              <span class=\"line\"></span>\n  \n              <p class=\"like\">\n                <img class=\"svg icon\" alt=\"\" src=\"assets/images/icons/like.svg\">\n                <span>26</span>\n              </p>\n              <h5 class=\"price\">{{ currentPrice | currency }}</h5>\n              <!-- \n                This plus and minus works\n              -->\n              <div class=\"quantity\">\n                <button type=\"button\" \n                  (click)=\"subtractItemCount()\"\n                  class=\"minus btn\">\n                  <img class=\"svg icon\" alt=\"subtract-item\" src=\"assets/images/icons/minus.svg\" height=\"30px\" />\n                </button>\n                <span class=\"value\">{{ product.quantity || 0 }}</span>\n                <button type=\"button\" \n                  (click)=\"addItemCount()\"\n                  class=\"plus btn\">\n                  <img class=\"svg icon\" alt=\"add-item\" src=\"assets/images/icons/plus.svg\" height=\"30px\" />\n                </button>\n              </div>\n  \n            </div>\n          </div>\n  \n          <div class=\"attribute-title\" *ngIf=\"(cartItems$ | async)?.productCheckouts?.length > 0\">\n            <div class=\"h-grid constrain\">\n              <p>Cart Items</p>\n              <p>Price</p>\n            </div>\n          </div>\n  \n          <section *ngIf=\"(cartItems$ | async) as cartItems\">\n            <div class=\"attributes constrain\" *ngFor=\"let cartItem of cartItems.productCheckouts\">\n              <div class=\"h-grid\">\n                <div class=\"checkbox\">\n                  <input id=\"size-s\" type=\"checkbox\">\n                  <span class=\"check\"></span>\n                  <label for=\"size-s\">\n                    {{ cartItem.productName | capitalize }} \n                    <span>({{ cartItem.quantity }})</span>\n                  </label>\n                </div>\n                <p class=\"price\">{{ cartItem.cost | currency }}</p>\n              </div>\n            </div>\n          </section>\n  \n          <!-- <div class=\"attribute-title\">\n            <div class=\"h-grid constrain\">\n              <p>Size</p>\n            </div>\n          </div>\n  \n          <div class=\"attributes constrain\">\n            <div class=\"h-grid\">\n              <div class=\"checkbox\">\n                <input id=\"lemon\" type=\"checkbox\">\n                <span class=\"check\"></span>\n                <label for=\"lemon\">Add Lemon</label>\n              </div>\n            </div>\n          </div> -->\n  \n          <div class=\"note\">\n            <div class=\"title\">\n              <div class=\"h-grid constrain\">\n                <p>Delivery address</p>\n              </div>\n            </div>\n  \n            <div class=\"constrain\">\n              <textarea name=\"note\" \n                [formControlName]=\"'Address'\" \n                placeholder=\"Enter your delivery address\" \n                id=\"note\">\n              </textarea>\n            </div>\n          </div>\n  \n          <div class=\"constrain\">\n            <!-- This button submits this form. -->\n            <button type=\"submit\" \n              class=\"big-cart-btn btn btn-primary\" \n              [disabled]=\"addDeliveryAddressForm.invalid\">\n              <p class=\"items\">{{ (cartItems$ | async)?.productCheckouts?.length || 0 }} item(s)</p>\n              <p class=\"price\" *ngIf=\"(cartItems$ | async) as cartItems; else defaultPrice\">\n                {{ cartItems?.productCheckouts | productCostCalculator | currency }}</p>\n              <ng-template #defaultPrice>\n                <p class=\"price\">{{ 0 | currency }}</p>\n              </ng-template>\n              <p class=\"text\">Add to Cart</p>\n            </button>\n          </div>\n        </form>\n      </div>\n      <!-- <div class=\"popup-overlay\"></div> -->\n    </section>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/cart-badge/cart-badge.component.html":
    /*!***************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/cart-badge/cart-badge.component.html ***!
      \***************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppSharedCartBadgeCartBadgeComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<a class=\"cursor-pointer\" [routerLink]=\"['/user', 'product-cart']\">\n  <main id=\"main-2\">\n    <p class=\"text-right cursor-pointer\">\n      <i class=\"fa cart-icon\">&#xf07a;</i>\n      <span class='badge badge-warning' id='lblCartCount'>{{ itemCount || 0 }}</span>\n    </p>\n  </main>\n</a>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/category-hightlight-widget/category-hightlight-widget.component.html":
    /*!***********************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/category-hightlight-widget/category-hightlight-widget.component.html ***!
      \***********************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppSharedCategoryHightlightWidgetCategoryHightlightWidgetComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<div class=\"category\">\n  <a [routerLink]=\"['/user', 'category-products', productCategory.productCategory.id, productCategory.productCategory.name]\" \n    class=\"details cursor-pointer\">\n    <div class=\"image\">\n      <img [src]=\"productCategory?.productDetails[0].productImageModels[0].path | filePathFormatter\"\n         height=\"50px\" \n         [alt]=\"productCategory?.productCategory.name\" \n        *ngIf=\"productCategory?.productDetails?.length > 0 \n          && productCategory?.productDetails[0].productImageModels.length > 0; else defaultImage\"/>\n\n          <ng-template #defaultImage>\n            <img src=\"assets/images/categories/1.png\" \n              height=\"50px\" \n              [alt]=\"productCategory?.productCategory.name\" />\n          </ng-template>\n    </div>\n    <h5 class=\"name\">{{ productCategory?.productCategory.name | capitalize }}</h5>\n  </a>\n</div>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/category-item-widget/category-item-widget.component.html":
    /*!***********************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/category-item-widget/category-item-widget.component.html ***!
      \***********************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppSharedCategoryItemWidgetCategoryItemWidgetComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<div class=\"business\">\n  <a [routerLink]=\"['/user', 'category-products', productCategory.productCategory.id, productCategory.productCategory.name]\" class=\"image\">\n    <img [src]=\"productCategory.productDetails[0].productImageModels[0].path | filePathFormatter\" \n      height=\"85px\" \n      *ngIf=\"productCategory.productDetails.length > 0 \n        && productCategory.productDetails[0].productImageModels.length > 0; \n        else defaultImage\"\n      src=\"assets/images/restaurants/kichi.png\" />\n\n    <ng-template #defaultImage>\n      <img src=\"assets/images/restaurants/kichi.png\" \n      height=\"85px\" />\n    </ng-template>\n  </a>\n  <div class=\"details\">\n    <a [routerLink]=\"['/user', 'category-products', productCategory.productCategory.id, productCategory.productCategory.name]\" class=\"name\">\n      <h5>{{ productCategory?.productCategory.name | capitalize }}</h5>\n    </a>\n\n    <!-- Bookmark icon -->\n    <!-- <button class=\"btn bookmark\">\n      <img class=\"svg\" src=\"assets/images/icons/bookmark.svg\" height=\"18px\" alt=\"Bookmark\">\n    </button> -->\n    <!-- <p class=\"address\">\n      <img class=\"svg\" src=\"assets/images/icons/map-pointer.svg\" height=\"10px\" alt=\"Store\">\n      <span>No, 5 fox road</span>\n    </p> -->\n    \n    <div class=\"bottom\">\n      <p class=\"reviews\">\n        <!-- <img class=\"svg icon\" alt=\"Rating Icon\" src=\"assets/images/icons/star.svg\">\n        <b>3.0</b> -->\n        <span>({{ productCategory?.productDetails.length || 0 }} Product(s))</span>\n      </p>\n\n      <!-- <p class=\"time\">30 Min <span>•</span> 20 Km</p> -->\n    </div>\n  </div>\n</div>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/conditional-bottom-navbar/conditional-bottom-navbar.component.html":
    /*!*********************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/conditional-bottom-navbar/conditional-bottom-navbar.component.html ***!
      \*********************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppSharedConditionalBottomNavbarConditionalBottomNavbarComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<app-user-bottom-navbar \n  *ngIf=\"userData.role === appRole.CUSTOMER; else defaultTemplate\">\n</app-user-bottom-navbar>\n<ng-template #defaultTemplate>\n  <app-rider-bottom-navbar></app-rider-bottom-navbar>\n</ng-template>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/favorite-food-widget/favorite-food-widget.component.html":
    /*!***********************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/favorite-food-widget/favorite-food-widget.component.html ***!
      \***********************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppSharedFavoriteFoodWidgetFavoriteFoodWidgetComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<!-- Because this is the Favorites page, all products are bookmarked. So no work has to be done for the bookmark icon. It's already styled from the .favorite class. -->\n<div class=\"favourite\">\n  <a (click)=\"openProductDetail()\" class=\"image cursor-pointer\">\n    <img [alt]=\"product.productModel.name\" \n        height=\"85px\" \n        *ngIf=\"product.productImageModels?.length > 0; else defaultTemplate\"\n        [src]=\"product.productImageModels[0].path | filePathFormatter\">\n      <ng-template #defaultTemplate>\n        <img src=\"assets/images/products/10.png\" \n            height=\"85px\" \n            [alt]=\"product.productModel?.name\" />\n      </ng-template>\n  </a>\n  <div class=\"details\">\n      <a (click)=\"openProductDetail()\" class=\"name cursor-pointer\">\n          <h5>{{ product.productModel?.name | uppercase }}</h5>\n      </a>\n      <button class=\"btn bookmark\">\n          <img class=\"svg\" src=\"assets/images/icons/bookmark.svg\" height=\"18px\" alt=\"Bookmark\">\n      </button>\n      <p class=\"restaurant-name\">\n          <img class=\"svg\" src=\"assets/images/icons/store.svg\" height=\"15px\" alt=\"Store\">\n          <span>{{ product.productModel[\"productCategoryModel\"][\"name\"] | uppercase }}</span>\n      </p>\n      <div class=\"bottom\">\n          <p class=\"reviews\">\n              <img class=\"svg icon\" alt=\"\" src=\"assets/images/icons/star.svg\">\n              <b>{{ product[\"productRating\"] | number : '1.1-3' }}</b>\n              <span>(1256)</span>\n          </p>\n\n          <p class=\"price\">{{ product.productCostModel.unitPrice | currency }}</p>\n      </div>\n  </div>\n</div>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/favorite-resturant-widget/favorite-resturant-widget.component.html":
    /*!*********************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/favorite-resturant-widget/favorite-resturant-widget.component.html ***!
      \*********************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppSharedFavoriteResturantWidgetFavoriteResturantWidgetComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<div class=\"favourite\">\n  <a [routerLink]=\"['/user', 'nearby', 'business-detail', merchant.id]\" class=\"image\">\n      <img [src]=\"merchant.logo | filePathFormatter\" \n        height=\"85px\" \n        *ngIf=\"merchant.logo; else defaultImage\"\n        [alt]=\"merchant.companyName\" />\n      <ng-template #defaultImage> \n        <img src=\"assets/images/restaurants/kfc.png\" height=\"85px\" [alt]=\"merchant.companyName\" />\n      </ng-template>\n  </a>\n  <div class=\"details\">\n      <a [routerLink]=\"['/user', 'nearby', 'business-detail', merchant.id]\" class=\"name\">\n          <h5>{{ merchant.companyName | uppercase }}</h5>\n      </a>\n      <button class=\"btn bookmark\" (click)=\"addBookmark(merchant)\">\n          <img class=\"svg\" src=\"assets/images/icons/bookmark.svg\" height=\"18px\" alt=\"Bookmark\">\n      </button>\n      <p class=\"restaurant-name\">\n          <img class=\"svg\" src=\"assets/images/icons/store.svg\" height=\"15px\" alt=\"Store\">\n          <span>{{ merchant.address | uppercase }}</span>\n      </p>\n      <div class=\"bottom\">\n          <p class=\"reviews\">\n              <img class=\"svg icon\" alt=\"\" src=\"assets/images/icons/star.svg\">\n              <b>{{ merchant.rate | number : '1.1-3' }}</b>\n              <span>(1256)</span>\n          </p>\n\n          <!-- <p class=\"price\">₦1,500</p> -->\n      </div>\n  </div>\n</div>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/favourite-merchant-hightlight-widget/favourite-merchant-hightlight-widget.component.html":
    /*!*******************************************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/favourite-merchant-hightlight-widget/favourite-merchant-hightlight-widget.component.html ***!
      \*******************************************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppSharedFavouriteMerchantHightlightWidgetFavouriteMerchantHightlightWidgetComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<a [routerLink]=\"['/user', 'nearby', 'business-detail', favouriteMerchant.id]\" class=\"restaurant\">\n  <div class=\"details bookmarked\">\n    <div class=\"image\">\n      <div class=\"widget-img-holder\">\n        <img [src]=\"favouriteMerchant?.logo | filePathFormatter\" \n          height=\"120px\" \n          [alt]=\"favouriteMerchant.companyName\"\n          *ngIf=\"favouriteMerchant?.logo; else defaultTemplate\" />\n\n        <ng-template #defaultTemplate>\n          <img src=\"assets/images/restaurants/kfc.png\" height=\"120px\" [alt]=\"favouriteMerchant.companyName\" />\n        </ng-template>\n      </div>\n      \n      <!-- Bookmark icon -->\n      <button class=\"btn bookmark\">\n        <img class=\"svg\" src=\"assets/images/icons/bookmark.svg\" height=\"18px\" alt=\"Bookmark\">\n      </button>\n    </div>\n    <div class=\"text\">\n      <h5>{{ favouriteMerchant.companyName | capitalize }}</h5>\n      <p>\n        <img class=\"svg\" \n          src=\"assets/images/icons/map-pointer.svg\" \n          height=\"10px\" \n          [alt]=\"favouriteMerchant.companyName\" />\n        <span>{{ favouriteMerchant.address | capitalize }}</span>\n      </p>\n    </div>\n  </div>\n</a>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/graph-widget/graph-widget.component.html":
    /*!*******************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/graph-widget/graph-widget.component.html ***!
      \*******************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppSharedGraphWidgetGraphWidgetComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<div class=\"statistic\">\n  <div class=\"title\">\n    <h2 class=\"period\">{{ graphLabel | capitalize }}</h2>\n    <p class=\"duration\">\n      {{ graphData.startDate | date : 'mediumDate' }} - {{ endOfWeek(graphData.startDate) | date : 'mediumDate' }}\n    </p>\n  </div>\n  <div class=\"chart data-is-full\" #thisWeekStats></div>\n</div>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/hot-sale-highlight-widget/hot-sale-highlight-widget.component.html":
    /*!*********************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/hot-sale-highlight-widget/hot-sale-highlight-widget.component.html ***!
      \*********************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppSharedHotSaleHighlightWidgetHotSaleHighlightWidgetComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<a class=\"sale bookmarked cursor-pointer\" (click)=\"viewProductDetail()\">\n  <div class=\"details\">\n    <div class=\"image\">\n      <div class=\"widget-img-holder\">\n        <img *ngIf=\"product.productImageModels?.length > 0\" \n        [src]=\"product.productImageModels[0].path\" \n        height=\"120px\" \n        [alt]=\"product.productModel.name\" />\n\n      <ng-template #defaultImage>\n        <img src=\"assets/images/products/3.png\" height=\"120px\" [alt]=\"product.productModel.name\" />\n      </ng-template>\n    </div>\n      <button class=\"btn bookmark\">\n        <img class=\"svg\" src=\"assets/images/icons/bookmark.svg\" height=\"18px\" alt=\"Bookmark\">\n      </button>\n    </div>\n    <div class=\"text\">\n      <h5>{{ product.productModel.name | capitalize }}</h5>\n      <p>\n        <img class=\"svg\" src=\"assets/images/icons/store.svg\" height=\"10px\" alt=\"Store\">\n        <span>{{ product?.productModel[\"productCategoryModel\"][\"name\"] | capitalize }}</span>\n      </p>\n    </div>\n  </div>\n</a>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/merchant-hot-sale-product/merchant-hot-sale-product.component.html":
    /*!*********************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/merchant-hot-sale-product/merchant-hot-sale-product.component.html ***!
      \*********************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppSharedMerchantHotSaleProductMerchantHotSaleProductComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<!-- \n          There's a \".bookmarked\" class on the products that are bookmarked by the user, Once the .bookmarked tag is added as a class it styles the bookmark icon on top of the product image.\n        -->\n        <div class=\"product bookmarked\">\n          <div class=\"details\">\n            <a class=\"image\">\n              <div class=\"widget-img-holder\" (click)=\"viewProductDetail()\">\n                <img [src]=\"product.productImageModels[0]?.path\" \n                  *ngIf=\"product.productImageModels.length > 0; else defaultImage\"\n                  height=\"120px\" \n                  [alt]=\"product?.productModel?.name\" />\n\n                <ng-template #defaultImage>\n                  <img src=\"assets/images/products/1.png\" height=\"120px\" [alt]=\"product?.productModel?.name\" />\n                </ng-template>\n              </div>\n              <button class=\"btn bookmark\" (click)=\"addProductToBookmark(product.productModel?.id)\">\n                <img class=\"svg\" src=\"assets/images/icons/bookmark.svg\" height=\"18px\" alt=\"Bookmark\">\n              </button>\n            </a>\n            <div class=\"text\">\n              <a (click)=\"viewProductDetail()\" class=\"name\">{{ product?.productModel?.name | uppercase }}</a>\n              <p>\n                <span class=\"price\">{{ product?.productCostModel.unitPrice | currency }}</span>\n  \n                <!-- Clicking the button below brings up the Add to Cart popup. -->\n                <button type=\"button\" class=\"btn add-to-cart\" (click)=\"addToCart(product)\">\n                  <img class=\"svg\" src=\"assets/images/icons/add.svg\" height=\"18px\" alt=\"Add to Cart\">\n                </button>\n              </p>\n            </div>\n          </div>\n        </div>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/merchant-product-listing/merchant-product-listing.component.html":
    /*!*******************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/merchant-product-listing/merchant-product-listing.component.html ***!
      \*******************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppSharedMerchantProductListingMerchantProductListingComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = " <!-- There's a \".bookmarked\" class on the restaurant that are bookmarked by the user, Once the .bookmarked tag is added as a class it styles the bookmark icon.-->\n<div class=\"business\" [ngClass]=\"{ 'bookmarked': isBookmarked }\">\n        <div class=\"details h-grid\">\n          <a [routerLink]=\"['/user', 'nearby', 'business-detail', merchant?.merchant.id]\" class=\"image\">\n            <img [src]=\"merchant.merchant.logo | filePathFormatter\" \n              height=\"85px\" \n              *ngIf=\"merchant.merchant.logo; else defaultImage\"\n              [alt]=\"merchant.merchant.companyName\" />\n          </a>\n          <ng-template #defaultImage>\n            <img src=\"assets/images/restaurants/kichi.png\"  \n              height=\"85px\" \n              [alt]=\"merchant.merchant.companyName\" />\n          </ng-template>\n          <div class=\"text\">\n            <div class=\"h-grid first-row\">\n              <div class=\"v-grid\">\n                <a [routerLink]=\"['/user', 'nearby', 'business-detail', merchant?.merchant.id]\" class=\"name\">\n                  <h5>{{ merchant.merchant.companyName | capitalize }}</h5>\n                </a>\n                <p>\n                  <img class=\"svg\" src=\"assets/images/icons/map-pointer.svg\" height=\"10px\" alt=\"Store\">\n                  <span>{{ merchant.merchant.address }}</span>\n                </p>\n              </div>\n              \n              <!-- Bookmark icon -->\n              <button class=\"btn bookmark\" (click)=\"addBookmark(merchant.merchant.id)\">\n                <img class=\"svg\" src=\"assets/images/icons/bookmark.svg\" height=\"18px\" alt=\"Bookmark\">\n              </button>\n            </div>\n            <div class=\"h-grid second-row\">\n              <p class=\"reviews\">\n                <img class=\"svg icon\" alt=\"\" src=\"assets/images/icons/star.svg\">\n                <b>{{ merchant.merchant.rate | number : '1.1-2' }}</b>\n                <span>({{ merchant?.merchant.ratingCount }})</span>\n              </p>\n  \n              <!-- <p class=\"time\" *ngIf=\"merchant?.merchant?.distance !== -1\">\n                30 Min <span>•</span> {{ merchant?.merchant?.distance }} Km\n              </p> -->\n              <p class=\"time\" *ngIf=\"merchant?.merchant?.distance !== -1\">\n                <span>•</span> {{ merchant?.merchant?.distance | number : '1.1-1' }} Km away\n              </p>\n            </div>\n            <!-- <div class=\"third-row\">\n              <div class=\"tag tag-outline\">Free Shipping</div>\n            </div> -->\n          </div>\n        </div>\n  \n        <div class=\"products\">\n          <app-single-product-widget \n            (viewProductDetailEvent)=\"openProductDetail($event, product)\"\n            [product]=\"product\" \n            *ngFor=\"let product of merchant.productDetails\">\n          </app-single-product-widget>\n        </div>\n</div>\n\n<!-- This section has a padding to allow footer to be visible -->\n<div class=\"footer-expander\" *ngIf=\"currentIteration === (arraySize - 1)\"></div>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/nearby-item-widget/nearby-item-widget.component.html":
    /*!*******************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/nearby-item-widget/nearby-item-widget.component.html ***!
      \*******************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppSharedNearbyItemWidgetNearbyItemWidgetComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<div class=\"business\" [ngClass]=\"{ 'bookmarked': isBookmarked }\">\n  <a [routerLink]=\"['/user', 'nearby', 'business-detail', merchant.id]\" class=\"image\">\n    <img src=\"assets/images/restaurants/kichi.png\" \n      height=\"85px\" \n      [src]=\"merchant.logo | filePathFormatter\"\n      *ngIf=\"merchant.logo; else defaultImage\"\n      [alt]=\"merchant?.companyName | uppercase\" />\n\n    <ng-template #defaultImage>\n      <img src=\"assets/images/restaurants/kichi.png\" \n      height=\"85px\" \n      [alt]=\"merchant?.companyName | uppercase\" />\n    </ng-template>\n  </a>\n  <div class=\"details\">\n    <a [routerLink]=\"['/user', 'nearby', 'business-detail', merchant.id]\" class=\"name\">\n      <h5>{{ merchant?.companyName | uppercase }}</h5>\n    </a>\n\n    <!-- Bookmark icon -->\n    <button class=\"btn bookmark\" (click)=\"addBookmark(merchant)\">\n      <img class=\"svg\" src=\"assets/images/icons/bookmark.svg\" height=\"18px\" alt=\"Bookmark\">\n    </button>\n    <p class=\"address\">\n      <img class=\"svg\" src=\"assets/images/icons/map-pointer.svg\" height=\"10px\" alt=\"Store\">\n      <span>{{ merchant.address | uppercase }}</span>\n    </p>\n    \n    <div class=\"bottom\">\n      <p class=\"reviews\">\n        <img class=\"svg icon\" alt=\"Rating Icon\" src=\"assets/images/icons/star.svg\">\n        <b>{{ merchant?.rate | number : '1.1-1' }}</b>\n        <span>({{ merchant?.ratingCount || 0 }} Reviews)</span>\n      </p>\n\n      <p class=\"time\"><span>•</span> {{ merchant?.distance | number : '1.1-1' }} Km away</p>\n      <!-- <p class=\"time\">30 Min <span>•</span> {{ merchant?.distance }} Km</p> -->\n    </div>\n  </div>\n</div>\n<app-footer-expander *ngIf=\"currentIndex === arrayLength\"></app-footer-expander>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/no-content-found/no-content-found.component.html":
    /*!***************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/no-content-found/no-content-found.component.html ***!
      \***************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppSharedNoContentFoundNoContentFoundComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-card>\n  <ion-card-content>\n    <img src=\"../../../assets/undraw_No_data_re_kwbl.svg\" alt=\"No content found\">\n  </ion-card-content>\n  <ion-card-header>\n    <h2 class=\"text-center\">No content</h2>\n  </ion-card-header>\n</ion-card>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/no-internet-modal/no-internet-modal.component.html":
    /*!*****************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/no-internet-modal/no-internet-modal.component.html ***!
      \*****************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppSharedNoInternetModalNoInternetModalComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<style>\n  main {\n    padding: 150px 20px 20px 20px;\n  }\n\n  .image-section {\n    width: 70%;\n    height: 30vh;\n    margin: 0 auto;\n  }\n\n  .image-section img {\n    width: 100%;\n    height: 100%;\n  }\n\n  h2 {\n    padding-top: 30px;\n    text-align: center;\n    font-weight: 400;\n    color: #fa6400;\n    letter-spacing: 1px;\n    line-height: 35px;\n    font-size: 25px;\n    text-shadow: 1px 1px #ddd;\n    /* font-family: 'comic sans ms' */\n  }\n</style>\n\n<main>\n  <section class=\"image-section\">\n    <img src=\"../../../assets/undraw_connected_world_wuay.svg\" alt=\"No Internet\" />\n  </section>\n  <h2>\n    OOPS, PLEASE CHECK YOUR INTERNET CONNECTION\n  </h2>\n</main>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/no-orders-found-widget/no-orders-found-widget.component.html":
    /*!***************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/no-orders-found-widget/no-orders-found-widget.component.html ***!
      \***************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppSharedNoOrdersFoundWidgetNoOrdersFoundWidgetComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<!-- This is the empty state of the Ongoing tab -->\n<div class=\"empty-state constrain\">\n  <img src=\"assets/images/others/idle-rider.png\" height=\"300px\" alt=\"Idle Rider\">\n\n  <div class=\"text\">\n    <h2>{{ purpose }} is Empty</h2>\n    <!-- <p>You can go to Discover to add products.</p> -->\n    <p>Try to Discover &amp; add products.</p>\n  </div>\n\n  <a *ngIf=\"showButton\" [routerLink]=\"['/user', 'discover-items']\" class=\"btn btn-primary\">Go Discover</a>\n</div>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/order-selected-widget-draft/order-selected-widget-draft.component.html":
    /*!*************************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/order-selected-widget-draft/order-selected-widget-draft.component.html ***!
      \*************************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppSharedOrderSelectedWidgetDraftOrderSelectedWidgetDraftComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<!-- <a [routerLink]=\"['/user/', 'orders', 'track-order']\" \n    [queryParams]=\"{ cartId: cart?.cartId, status: productCartStatus.CANCELLED }\"\n    class=\"order\">\n  <div class=\"image\">\n    <img src=\"assets/images/restaurants/kichi.png\" \n      height=\"95px\" \n      [alt]=\"cart?.dateCreated | date : 'short'\" />\n\n      <ng-template #defaultImage>\n        <img src=\"assets/images/restaurants/kichi.png\" \n          height=\"95px\" \n          [alt]=\"cart?.dateCreated | date : 'short'\" />\n      </ng-template>\n  </div>\n  <div class=\"details\">\n    <h5 class=\"name\">{{ cart?.dateCreated | date : 'short' }}</h5>\n    <p class=\"address\">\n      <img class=\"svg\" src=\"assets/images/icons/map-pointer.svg\" height=\"10px\" alt=\"Store\">\n      <span>{{ cart?.deliveryAddress | capitalize }}</span>\n    </p>\n    <p class=\"items\">\n     {{ cart.productCheckouts?.length || 0 }} Item\n    </p>\n  </div>\n</a> -->\n\n<a [routerLink]=\"['/user', 'orders', 'order-info']\"\n[queryParams]=\"{ cartId: cart?.cartId, status: productCartStatus.CANCELLED }\"  class=\"order\">\n  <div class=\"image\">\n    <img [src]=\"getProductImageToPreview(cart.productCheckouts) | filePathFormatter\" \n      *ngIf=\"getProductImageToPreview(cart.productCheckouts); else defaultImage\"\n      height=\"95px\" \n      [alt]=\"cart?.dateCreated | date : 'short'\" />\n\n    <ng-template #defaultImage>\n      <img src=\"assets/images/restaurants/kichi.png\" \n        height=\"95px\" \n        [alt]=\"cart?.dateCreated | date : 'short'\" /> \n    </ng-template>\n  </div>\n  <div class=\"details\">\n    <h5 class=\"name\">{{ cart?.dateCreated | date : 'short' }}</h5>\n    <p class=\"items\">\n      {{ cart.productCheckouts?.length || 0 }} Items\n    </p>\n    <p class=\"date\">{{ cart?.dateCreated | date : 'short' }}</p>\n  </div>\n</a>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/order-selected-widget-history/order-selected-widget-history.component.html":
    /*!*****************************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/order-selected-widget-history/order-selected-widget-history.component.html ***!
      \*****************************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppSharedOrderSelectedWidgetHistoryOrderSelectedWidgetHistoryComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<a [routerLink]=\"['/user', 'orders', 'order-info']\"\n[queryParams]=\"{ cartId: cart?.cartId, status: productCartStatus.COMPLETED }\"  class=\"order\">\n  <div class=\"image\">\n    <img [src]=\"getProductImageToPreview(cart.productCheckouts) | filePathFormatter\"\n      height=\"95px\" \n      *ngIf=\"getProductImageToPreview(cart.productCheckouts); else defaultImage\"\n      [alt]=\"cart?.dateCreated | date : 'short'\" />\n    <ng-template #defaultImage>\n      <img src=\"assets/images/restaurants/kichi.png\" height=\"95px\" [alt]=\"cart?.dateCreated | date : 'short'\" />\n    </ng-template>\n  </div>\n  <div class=\"details\">\n    <h5 class=\"name\">{{ cart?.dateCreated | date : 'short' }}</h5>\n    <p class=\"items\">\n      {{ cart.productCheckouts?.length || 0 }} Items\n    </p>\n    <p class=\"date\">{{ cart?.dateCreated | date : 'short' }}</p>\n  </div>\n</a>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/order-selected-widget-ongoing/order-selected-widget-ongoing.component.html":
    /*!*****************************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/order-selected-widget-ongoing/order-selected-widget-ongoing.component.html ***!
      \*****************************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppSharedOrderSelectedWidgetOngoingOrderSelectedWidgetOngoingComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<a [routerLink]=\"['/user', 'orders', 'track-order']\"\n  [queryParams]=\"{ cartId: cart?.cartId, status: productCartStatus.TRANSIT }\"\n  class=\"order\">\n  <div class=\"image\">\n    <img [src]=\"getProductImageToPreview(cart.productCheckouts) | filePathFormatter\"\n     height=\"95px\" \n     *ngIf=\"getProductImageToPreview(cart.productCheckouts); else defaultImage\"\n     [alt]=\"cart?.dateCreated | date : 'short'\" />\n\n    <ng-template #defaultImage>\n      <img src=\"assets/images/restaurants/kichi.png\" \n      height=\"95px\" \n      [alt]=\"cart?.dateCreated | date : 'short'\" />\n    </ng-template>\n  </div>\n  <div class=\"details\">\n    <h5 class=\"name\">{{ cart?.dateCreated | date : 'short' }}</h5>\n    <p class=\"id\" *ngIf=\"cart?.cartId\">\n      <span>ID:</span> {{ cart?.cartId }}\n    </p>\n    <p class=\"items\">\n      {{ cart.productCheckouts?.length || 0 }} Item\n    </p>\n    <!-- This is the status of the order, The statuses have different classes to change the colors -->\n    <p class=\"status waiting\">\n      {{ cart.status| capitalize }}\n    </p>\n  </div>\n</a>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/ordered-product-highlight-widget/ordered-product-highlight-widget.component.html":
    /*!***********************************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/ordered-product-highlight-widget/ordered-product-highlight-widget.component.html ***!
      \***********************************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppSharedOrderedProductHighlightWidgetOrderedProductHighlightWidgetComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<a (click)=\"viewProductDetail()\" class=\"order bookmarked cursor-pointer\">\n  <div class=\"details\">\n    <div class=\"image\">\n      <div class=\"widget-img-holder\">\n        <img [src]=\"product.productImageModels[0].path | filePathFormatter\"\n          *ngIf=\"product.productImageModels.length > 0; else defaultImage\"\n          height=\"120px\" \n          [alt]=\"product?.productModel?.name\" />\n\n        <ng-template #defaultImage>\n          <img src=\"assets/images/products/2.png\" \n            height=\"120px\" \n            [alt]=\"product?.productModel?.name\" />\n        </ng-template>\n      </div>\n      <!-- Bookmark icon -->\n      <button class=\"btn bookmark\">\n        <img class=\"svg\" src=\"assets/images/icons/bookmark.svg\" height=\"18px\" alt=\"Bookmark\">\n      </button>\n    </div>\n    <div class=\"text\">\n      <h5>{{ product?.productModel?.name | capitalize }}</h5>\n      <p>\n        <img class=\"svg\" src=\"assets/images/icons/store.svg\" height=\"10px\" alt=\"Store\">\n        <span>{{ product?.productModel[\"productCategoryModel\"][\"name\"] | capitalize }}</span>\n      </p>\n    </div>\n  </div>\n</a>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/product-category-widget/product-category-widget.component.html":
    /*!*****************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/product-category-widget/product-category-widget.component.html ***!
      \*****************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppSharedProductCategoryWidgetProductCategoryWidgetComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<div class=\"product constrain bookmarked\">\n  <a (click)=\"viewProductDetail()\" class=\"image\">\n    <img [src]=\"product.productImageModels[0].path | filePathFormatter\" \n      height=\"120px\" \n      *ngIf=\"product.productImageModels.length > 0; else defaultImage\"\n      [alt]=\"product?.productModel.name\" />\n    \n    <ng-template #defaultImage>\n      <img src=\"assets/images/products/4.png\" height=\"120px\" [alt]=\"product?.productModel.name\" />\n    </ng-template>\n  </a>\n\n  <div class=\"details\">\n\n    <a (click)=\"viewProductDetail()\" class=\"name\">\n      <h5>{{ product?.productModel.name | uppercase }}</h5>\n    </a>\n\n    <button class=\"btn bookmark\" (click)=\"addProductToBookmark(product.productModel?.id)\">\n      <img class=\"svg\" src=\"assets/images/icons/bookmark.svg\" height=\"18px\" alt=\"Bookmark\">\n    </button>\n\n    <p class=\"description\" *ngIf=\"product.productModel.description\">\n      {{ product.productModel.description | capitalize }}\n    </p>\n\n    <h5 class=\"price\">{{ product.productCostModel.unitPrice | currency }}</h5>\n\n    <p class=\"bag\">\n      <img class=\"svg icon\" alt=\"Total Likes\" src=\"assets/images/icons/bag.svg\">\n      <span>99+</span>\n    </p>\n\n    <span class=\"line\"></span>\n\n    <p class=\"like\">\n      <img class=\"svg icon\" alt=\"Like Product\" src=\"assets/images/icons/like.svg\">\n      <span>26</span>\n    </p>\n\n    <button class=\"btn add-to-cart\" (click)=\"addToCart(product)\">\n      <img class=\"svg\" src=\"assets/images/icons/add.svg\" height=\"18px\" alt=\"Add to Cart\" />\n    </button>\n\n  </div>\n</div>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/product-detail-modal/product-detail-modal.component.html":
    /*!***********************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/product-detail-modal/product-detail-modal.component.html ***!
      \***********************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppSharedProductDetailModalProductDetailModalComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-grid>\n      <ion-row>\n        <ion-col size=\"10\">\n          <ion-title>{{ product?.productModel?.name | uppercase }}</ion-title>\n        </ion-col>\n        <ion-col>\n          <a appCloseModal class=\"cursor-pointer\">Close</a>\n        </ion-col>\n      </ion-row>\n      </ion-grid>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n <section>\n  <ion-card>\n    <ion-card-header>\n      <ion-grid>\n        <ion-row>\n          <ion-col>\n            <ion-item lines=\"none\">\n              <ion-button fill=\"outline\" slot=\"start\" (click)=\"addProductToCart(product)\">\n                Add to cart\n              </ion-button>\n            </ion-item>\n          </ion-col>\n          <ion-col>\n            <ion-item lines=\"none\">\n              <button class=\"btn bookmark\" (click)=\"addProductToBookmark(product.productModel.id)\">\n                <img class=\"svg\" src=\"assets/images/icons/bookmark.svg\" height=\"18px\" alt=\"Bookmark\">\n              </button>\n            </ion-item>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n      <ion-card-title>{{ product?.productModel?.name | uppercase }}</ion-card-title>\n      <ion-card-subtitle *ngIf=\"product?.productModel['productCategoryModel']\">\n        {{ product?.productModel[\"productCategoryModel\"][\"name\"] | uppercase }}\n      </ion-card-subtitle>\n      <ion-card-subtitle color=\"dark\">{{ product.productCostModel.unitPrice | currency }}</ion-card-subtitle>\n    </ion-card-header>\n  \n    <ion-card-content>\n      <div *ngIf=\"product.productImageModels?.length > 0\">\n        <swiper\n          *ngIf=\"product.productImageModels.length > 1; else defaultImage\"\n          id=\"productImage\"\n          [slidesPerView]=\"'1'\"\n          [spaceBetween]=\"30\"\n          [centeredSlides]=\"false\"\n          [loop]=\"true\"\n          [autoplay]=\"{ delay: 2500, disableOnInteraction: true }\">\n          <ng-template swiperSlide *ngFor=\"let image of product.productImageModels\">\n            <div class=\"img-holder\">\n              <img [src]=\"image.path | filePathFormatter\" \n                [alt]=\"product?.productModel?.name\">\n            </div>\n          </ng-template>\n        </swiper>\n        <ng-template #defaultImage>\n          <div class=\"img-holder\">\n            <img [src]=\"product.productImageModels[0]?.path | filePathFormatter\" \n              [alt]=\"product?.productModel?.name\">\n          </div>\n        </ng-template>\n      </div>\n     <p class=\"description\" *ngIf=\"product.productModel.description\">\n       {{ product.productModel.description | capitalize }}\n     </p>\n    </ion-card-content>\n  </ion-card>\n </section>\n\n <app-add-to-cart \n      *ngIf=\"openCartModal\"\n      [product]=\"productAddedToCart\"\n      [productImages]=\"productAddedToCartImages\"\n      [userId]=\"userId\"\n      [show]=\"openCartModal\"\n      (closeAddToCartModalEvent)=\"closeCartModal()\">\n    </app-add-to-cart>\n</ion-content>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/product-item-widget/product-item-widget.component.html":
    /*!*********************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/product-item-widget/product-item-widget.component.html ***!
      \*********************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppSharedProductItemWidgetProductItemWidgetComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<p>\n  product-item-widget works!\n</p>\n";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/rating-comment/rating-comment.component.html":
    /*!***********************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/rating-comment/rating-comment.component.html ***!
      \***********************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppSharedRatingCommentRatingCommentComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<!-- This is a rating with everything: star rating, comment, and media. -->\n<div class=\"comment\">\n  <!-- Image of the reviewer -->\n  <div class=\"user-image\">\n    <img [src]=\"merchantReview?.user.person?.passportUrl | filePathFormatter\" \n      height=\"40px\" \n      [alt]=\"fullName\"\n      *ngIf=\"merchantReview?.user.person.passportUrl; else defaultImage\" />\n\n    <ng-template #defaultImage>\n      <img src=\"assets/images/humans/1.png\" height=\"40px\" [alt]=\"fullName\" />\n    </ng-template>\n  </div>\n  <div class=\"details\">\n    <h5 class=\"name\">{{ fullName | uppercase }}</h5>\n\n    <!-- \n      The number of stars will be changed to 3 stars when you change \"--rating: 5;\" to \"--rating: 3; in the inline styling below. It can also be in decimal format to have precise ratings.\"\n    -->\n    <div class=\"stars\" \n      [style]=\"styleString\" \n      [attr.aria-label]=\"ariaLabel\">\n    </div>\n    \n    <p class=\"date\">{{ merchantReview?.dateCreated | date : 'mediumDate' }}</p>\n    <p class=\"note\" \n      *ngIf=\"merchantReview?.review && merchantReview?.review !== ''\">\n      {{ merchantReview?.review | capitalize }}\n    </p>\n    <!-- <div class=\"comment-media\">\n      <a href=\"#\" class=\"media\"><img src=\"assets/images/products/16.png\" height=\"70px\" alt=\"Review Media\"></a>\n    </div> -->\n  </div>\n</div>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/rider-bottom-navbar/rider-bottom-navbar.component.html":
    /*!*********************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/rider-bottom-navbar/rider-bottom-navbar.component.html ***!
      \*********************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppSharedRiderBottomNavbarRiderBottomNavbarComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<!-- This is that menu at the bottom of every page -->\n<section id=\"bottom-menu\">\n  <div class=\"menu-links\">\n    <a [routerLink]=\"['/rider', 'home']\" [routerLinkActive]=\"'active'\" class=\"link link-secondary\">\n      <img class=\"svg\" src=\"assets/images/icons/menu/home.svg\" height=\"20px\" alt=\"Home\" />\n      <span>Home</span>\n    </a>\n\n    <a [routerLink]=\"['/rider', 'orders']\" [routerLinkActive]=\"'active'\" class=\"link link-secondary\">\n      <img class=\"svg\" src=\"assets/images/icons/menu/orders.svg\" height=\"20px\" alt=\"Orders\" />\n      <span>Orders</span>\n    </a>\n\n    <a [routerLink]=\"['/rider', 'statistics']\" [routerLinkActive]=\"'active'\" class=\"link link-secondary\">\n      <img class=\"svg\" src=\"assets/images/icons/menu/bar-chart.svg\" height=\"20px\" alt=\"Statistics\" />\n      <span>Statistics</span>\n    </a>\n\n    <a [routerLink]=\"['/profile']\" [routerLinkActive]=\"'active'\" class=\"link link-secondary\">\n      <img class=\"svg\" src=\"assets/images/icons/menu/profile.svg\" height=\"20px\" alt=\"Profile\" />\n      <span>Profile</span>\n    </a>\n  </div>\n</section>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/rider-delivery-history-widget/rider-delivery-history-widget.component.html":
    /*!*****************************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/rider-delivery-history-widget/rider-delivery-history-widget.component.html ***!
      \*****************************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppSharedRiderDeliveryHistoryWidgetRiderDeliveryHistoryWidgetComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<a *ngIf=\"cart\" \n  [routerLink]=\"['/rider', 'order-info', cart.cart.id]\" class=\"order\">\n  <div class=\"image\">\n    <img src=\"assets/images/restaurants/kichi.png\" height=\"95px\" alt=\"kichi\">\n  </div>\n  <div class=\"details\">\n    <h5 class=\"name\">{{ cart.cart?.dateCreated | date : 'short' }}</h5>\n    <p class=\"items\">\n      {{ cart?.orders?.length || 0 }} Items\n    </p>\n    <p class=\"date\">{{ cart.cart?.dateCreated | date : 'short' }}</p>\n  </div>\n</a>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/rider-delivery-ongoing-widget/rider-delivery-ongoing-widget.component.html":
    /*!*****************************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/rider-delivery-ongoing-widget/rider-delivery-ongoing-widget.component.html ***!
      \*****************************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppSharedRiderDeliveryOngoingWidgetRiderDeliveryOngoingWidgetComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<a *ngIf=\"cart\" \n  [routerLink]=\"['/rider', 'order-info', cart.cart.id]\" class=\"order\">\n  <div class=\"image\">\n    <img src=\"assets/images/restaurants/kichi.png\" height=\"95px\" alt=\"kichi\">\n  </div>\n  <div class=\"details\">\n    <h5 class=\"name\">{{ cart.cart?.dateCreated | date : 'short' }}</h5>\n    <p class=\"id\">\n      <span>ID:</span> {{ cart.cart.id }}\n    </p>\n    <p class=\"items\">\n      {{ cart.orders.length || 0 }} Item\n    </p>\n    <!-- This is the status of the order, The statuses have different classes to change the colors -->\n    <!-- <p class=\"status waiting\">Waiting</p> -->\n    <p class=\"status waiting\">{{ cart?.cart?.status | capitalize }}</p>\n  </div>\n</a>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/search-results-widget/search-results-widget.component.html":
    /*!*************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/search-results-widget/search-results-widget.component.html ***!
      \*************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppSharedSearchResultsWidgetSearchResultsWidgetComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<div class=\"business\" [ngClass]=\"{ 'bookmarked': isBookmarked }\">\n  <div class=\"details h-grid\">\n    <a class=\"image cursor-pointer\" (click)=\"viewProductDetail()\">\n      <img \n        [src]=\"searchResult?.productImageModels[0].path\" height=\"85px\" \n        [alt]=\"searchResult.productModel.name\"\n        *ngIf=\"searchResult?.productImageModels.length > 0; else defaultImage\"/>\n      <ng-template #defaultImage>\n        <img src=\"assets/images/restaurants/kichi.png\" height=\"85px\" \n          [alt]=\"searchResult.productModel.name\" />\n      </ng-template>\n    </a>\n    <div class=\"text\">\n      <div class=\"h-grid first-row\">\n        <div class=\"v-grid\">\n          <a (click)=\"viewProductDetail()\" class=\"cursor-pointer name\">\n            <h5>{{ searchResult.productModel.name | uppercase }}</h5>\n          </a>\n          <p>\n            <img class=\"svg\" src=\"assets/images/icons/map-pointer.svg\" height=\"10px\" alt=\"Store\" />\n            <span>{{ searchResult.productModel.description }}</span>\n          </p>\n        </div>\n        <!-- Bookmark-icon -->\n        <button class=\"btn bookmark\" (click)=\"addProductToBookmark()\">\n          <img class=\"svg\" src=\"assets/images/icons/bookmark.svg\" height=\"18px\" alt=\"Bookmark\">\n        </button>\n      </div>\n      <div class=\"h-grid second-row\">\n        <p class=\"reviews\">\n          <img class=\"svg icon\" alt=\"\" src=\"assets/images/icons/star.svg\">\n          <span>{{ searchResult.productRating | number }}</span>\n        </p>\n        <p class=\"reviews\">\n          <span>{{ searchResult.productCostModel.unitPrice | currency }}</span>\n        </p>\n        <p class=\"time\" *ngIf=\"searchResult.productModel['merchantModel']['companyName']\">\n          {{ searchResult.productModel[\"merchantModel\"][\"companyName\"] | uppercase }}\n        </p>\n      </div>\n    </div>\n  </div>\n</div>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/single-product-widget/single-product-widget.component.html":
    /*!*************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/single-product-widget/single-product-widget.component.html ***!
      \*************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppSharedSingleProductWidgetSingleProductWidgetComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<a (click)=\"viewProductDetail()\" class=\"product\">\n  <div class=\"image\">\n    <img [src]=\"product?.productImageModels[0]?.path | filePathFormatter\" \n      height=\"60px\" \n      [alt]=\"product?.productModel.name\" \n      *ngIf=\"product?.productImageModels.length > 0; else defaultImage\" />\n\n    <ng-template #defaultImage>\n      <img src=\"assets/images/products/4.png\" />\n    </ng-template>\n  </div>\n  <div class=\"text\">\n    <p class=\"name\">{{ product?.productModel.name | capitalize }}</p>\n    <p class=\"price\">\n      <b>{{ product.productCostModel.unitPrice | currency }}</b>\n    </p>\n  </div>\n</a>";
      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/user-bottom-navbar/user-bottom-navbar.component.html":
    /*!*******************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/user-bottom-navbar/user-bottom-navbar.component.html ***!
      \*******************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppSharedUserBottomNavbarUserBottomNavbarComponentHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<section id=\"bottom-menu\">\n  <div class=\"menu-links\">\n    <a [routerLink]=\"['/user', 'discover-items']\" class=\"link link-secondary\" [routerLinkActive]=\"'active'\">\n      <img class=\"svg\" src=\"assets/images/icons/menu/home.svg\" height=\"20px\" alt=\"Home\">\n      <span>Home</span>\n    </a>\n\n    <a [routerLink]=\"['/user', 'nearby']\" [routerLinkActive]=\"'active'\" class=\"link link-secondary\">\n      <img class=\"svg\" src=\"assets/images/icons/menu/nearby.svg\" height=\"20px\" alt=\"Nearby\">\n      <span>Nearby</span>\n    </a>\n\n    <a [routerLink]=\"['/user', 'orders']\" [routerLinkActive]=\"'active'\" class=\"link link-secondary\">\n      <img class=\"svg\" src=\"assets/images/icons/menu/orders.svg\" height=\"20px\" alt=\"Orders\">\n      <span>Orders</span>\n    </a>\n\n    <a [routerLink]=\"['/user', 'favourites']\" [routerLinkActive]=\"'active'\" class=\"link link-secondary\">\n      <img class=\"svg\" src=\"assets/images/icons/menu/favourites.svg\" height=\"20px\" alt=\"Favourites\">\n      <span>Favourites</span>\n    </a>\n\n    <a [routerLink]=\"['/profile']\" [routerLinkActive]=\"'active'\" class=\"link link-secondary\">\n      <img class=\"svg\" src=\"assets/images/icons/menu/profile.svg\" height=\"20px\" alt=\"Profile\">\n      <span>Profile</span>\n    </a>\n  </div>\n</section>";
      /***/
    },

    /***/
    "./src/app/app-routing.module.ts":
    /*!***************************************!*\
      !*** ./src/app/app-routing.module.ts ***!
      \***************************************/

    /*! exports provided: AppRoutingModule */

    /***/
    function srcAppAppRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function () {
        return AppRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _auth_guards_auth_guard__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./auth/guards/auth.guard */
      "./src/app/auth/guards/auth.guard.ts");
      /* harmony import */


      var _auth_guards_customer_guard__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./auth/guards/customer.guard */
      "./src/app/auth/guards/customer.guard.ts");
      /* harmony import */


      var _auth_guards_rider_guard__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./auth/guards/rider.guard */
      "./src/app/auth/guards/rider.guard.ts");

      var routes = [{
        path: "home",
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | home-home-module */
          "home-home-module").then(__webpack_require__.bind(null,
          /*! ./home/home.module */
          "./src/app/home/home.module.ts")).then(function (m) {
            return m.HomePageModule;
          });
        }
      }, {
        path: "",
        redirectTo: "home",
        pathMatch: "full"
      }, {
        path: "profile",
        canActivate: [_auth_guards_auth_guard__WEBPACK_IMPORTED_MODULE_3__["AuthGuard"]],
        canActivateChild: [_auth_guards_auth_guard__WEBPACK_IMPORTED_MODULE_3__["AuthGuard"]],
        children: [{
          path: "",
          pathMatch: "full",
          loadChildren: function loadChildren() {
            return __webpack_require__.e(
            /*! import() | user-profile-profile-module */
            "user-profile-profile-module").then(__webpack_require__.bind(null,
            /*! ./user/profile/profile.module */
            "./src/app/user/profile/profile.module.ts")).then(function (m) {
              return m.ProfilePageModule;
            });
          }
        }, {
          path: "edit-profile",
          loadChildren: function loadChildren() {
            return __webpack_require__.e(
            /*! import() | user-profile-edit-profile-edit-profile-module */
            "edit-profile-edit-profile-module").then(__webpack_require__.bind(null,
            /*! ./user/profile/edit-profile/edit-profile.module */
            "./src/app/user/profile/edit-profile/edit-profile.module.ts")).then(function (m) {
              return m.EditProfilePageModule;
            });
          }
        }, {
          path: "payment-methods",
          loadChildren: function loadChildren() {
            return __webpack_require__.e(
            /*! import() | user-profile-payment-method-payment-method-module */
            "payment-method-payment-method-module").then(__webpack_require__.bind(null,
            /*! ./user/profile/payment-method/payment-method.module */
            "./src/app/user/profile/payment-method/payment-method.module.ts")).then(function (m) {
              return m.PaymentMethodPageModule;
            });
          }
        }, {
          path: "address",
          loadChildren: function loadChildren() {
            return __webpack_require__.e(
            /*! import() | user-profile-address-address-module */
            "address-address-module").then(__webpack_require__.bind(null,
            /*! ./user/profile/address/address.module */
            "./src/app/user/profile/address/address.module.ts")).then(function (m) {
              return m.AddressPageModule;
            });
          }
        }, {
          path: "add-address",
          loadChildren: function loadChildren() {
            return __webpack_require__.e(
            /*! import() | user-profile-add-address-add-address-module */
            "add-address-add-address-module").then(__webpack_require__.bind(null,
            /*! ./user/profile/add-address/add-address.module */
            "./src/app/user/profile/add-address/add-address.module.ts")).then(function (m) {
              return m.AddAddressPageModule;
            });
          }
        }]
      }, {
        path: "sign-up-otp-verification",
        loadChildren: function loadChildren() {
          return Promise.all(
          /*! import() | auth-sign-up-otp-verification-sign-up-otp-verification-module */
          [__webpack_require__.e("common"), __webpack_require__.e("sign-up-otp-verification-sign-up-otp-verification-module")]).then(__webpack_require__.bind(null,
          /*! ./auth/sign-up-otp-verification/sign-up-otp-verification.module */
          "./src/app/auth/sign-up-otp-verification/sign-up-otp-verification.module.ts")).then(function (m) {
            return m.SignUpOtpVerificationPageModule;
          });
        }
      }, {
        path: "auth",
        loadChildren: function loadChildren() {
          return Promise.resolve().then(__webpack_require__.bind(null,
          /*! ./auth/auth.module */
          "./src/app/auth/auth.module.ts")).then(function (m) {
            return m.AuthModule;
          });
        }
      }, {
        path: "user",
        canActivate: [_auth_guards_customer_guard__WEBPACK_IMPORTED_MODULE_4__["CustomerGuard"]],
        canActivateChild: [_auth_guards_customer_guard__WEBPACK_IMPORTED_MODULE_4__["CustomerGuard"]],
        loadChildren: function loadChildren() {
          return Promise.resolve().then(__webpack_require__.bind(null,
          /*! ./user/user.module */
          "./src/app/user/user.module.ts")).then(function (m) {
            return m.UserModule;
          });
        }
      }, {
        path: "rider",
        canActivate: [_auth_guards_rider_guard__WEBPACK_IMPORTED_MODULE_5__["RiderGuard"]],
        canActivateChild: [_auth_guards_rider_guard__WEBPACK_IMPORTED_MODULE_5__["RiderGuard"]],
        loadChildren: function loadChildren() {
          return Promise.resolve().then(__webpack_require__.bind(null,
          /*! ./rider/rider.module */
          "./src/app/rider/rider.module.ts")).then(function (m) {
            return m.RiderModule;
          });
        }
      }, {
        path: "live-order-tracking",
        loadChildren: function loadChildren() {
          return __webpack_require__.e(
          /*! import() | rider-live-order-tracking-live-order-tracking-module */
          "rider-live-order-tracking-live-order-tracking-module").then(__webpack_require__.bind(null,
          /*! ./rider/live-order-tracking/live-order-tracking.module */
          "./src/app/rider/live-order-tracking/live-order-tracking.module.ts")).then(function (m) {
            return m.LiveOrderTrackingPageModule;
          });
        }
      }];

      var AppRoutingModule = function AppRoutingModule() {
        _classCallCheck(this, AppRoutingModule);
      };

      AppRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes, {
          preloadingStrategy: _angular_router__WEBPACK_IMPORTED_MODULE_2__["PreloadAllModules"],
          useHash: true
        })],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], AppRoutingModule);
      /***/
    },

    /***/
    "./src/app/app.component.scss":
    /*!************************************!*\
      !*** ./src/app/app.component.scss ***!
      \************************************/

    /*! exports provided: default */

    /***/
    function srcAppAppComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */";
      /***/
    },

    /***/
    "./src/app/app.component.ts":
    /*!**********************************!*\
      !*** ./src/app/app.component.ts ***!
      \**********************************/

    /*! exports provided: AppComponent */

    /***/
    function srcAppAppComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AppComponent", function () {
        return AppComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _capacitor_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @capacitor/core */
      "./node_modules/@capacitor/core/dist/esm/index.js");
      /* harmony import */


      var _ngrx_store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ngrx/store */
      "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! rxjs */
      "./node_modules/rxjs/_esm2015/index.js");
      /* harmony import */


      var subsink__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! subsink */
      "./node_modules/subsink/dist/es2015/index.js");
      /* harmony import */


      var _auth_store_actions_auth_action__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./auth/store/actions/auth.action */
      "./src/app/auth/store/actions/auth.action.ts");
      /* harmony import */


      var _user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ./user/store/actions/user.action */
      "./src/app/user/store/actions/user.action.ts");
      /* harmony import */


      var _rider_store_actions_rider_action__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ./rider/store/actions/rider.action */
      "./src/app/rider/store/actions/rider.action.ts");
      /* harmony import */


      var _services_signalr_websocket_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! ./services/signalr-websocket.service */
      "./src/app/services/signalr-websocket.service.ts");
      /* harmony import */


      var _services_fcm_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! ./services/fcm.service */
      "./src/app/services/fcm.service.ts");
      /* harmony import */


      var _services_location_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
      /*! ./services/location.service */
      "./src/app/services/location.service.ts");
      /* harmony import */


      var _services_navigation_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
      /*! ./services/navigation.service */
      "./src/app/services/navigation.service.ts");

      var SplashScreen = _capacitor_core__WEBPACK_IMPORTED_MODULE_3__["Plugins"].SplashScreen;

      var AppComponent = /*#__PURE__*/function () {
        function AppComponent(platform, store, locationSrv, signalRSrv, // tslint:disable-next-line: variable-name
        _ngZone, navigationSrv, fcmSrv) {
          _classCallCheck(this, AppComponent);

          this.platform = platform;
          this.store = store;
          this.locationSrv = locationSrv;
          this.signalRSrv = signalRSrv;
          this._ngZone = _ngZone;
          this.navigationSrv = navigationSrv;
          this.fcmSrv = fcmSrv;
          this.subSink = new subsink__WEBPACK_IMPORTED_MODULE_6__["SubSink"]();
          this.initializeApp();
        }

        _createClass(AppComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.riderMessage$ = this.store.select(function (data) {
              return data.Rider.ActiveMessage;
            });
            this.riderError$ = this.store.select(function (data) {
              return data.Rider.ActiveError;
            });
            this.message$ = this.store.select(function (data) {
              return data.Auth.ActiveMessage;
            });
            this.userMessage$ = this.store.select(function (data) {
              return data.User.ActiveMessage;
            });
            this.error$ = this.store.select(function (data) {
              return data.Auth.ActiveError;
            });
            this.userError$ = this.store.select(function (data) {
              return data.User.ActiveError;
            });
            this.isLoading$ = this.store.select(function (data) {
              return data.Auth.IsLoading;
            });
            this.isUserLoading$ = this.store.select(function (data) {
              return data.User.IsLoading;
            });
            this.isRiderLoading$ = this.store.select(function (data) {
              return data.Rider.IsLoading;
            });
            this.cartItemsCount$ = this.store.select(function (data) {
              return data.User.CartItemCount;
            });
            this.activeActionMessage$ = this.store.select(function (data) {
              return data.User.ActiveActionMessage;
            });
            this.riderActiveActionMessage$ = this.store.select(function (data) {
              return data.Rider.ActiveActionMessage;
            }); // ? Open websocket connection

            this.signalRSrv.startConnection(); // ? Listen for cart confirmation

            this.signalRSrv.confirmCartVerificationRequest();
          }
        }, {
          key: "initializeApp",
          value: function initializeApp() {
            var _this = this;

            this.platform.ready().then(function () {
              Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["timer"])(5000).subscribe(function () {
                SplashScreen.hide();
              });
              /**
               * Subscribe to Socket events to make them global
               */

              _this.subscribeToEvents();
            });
          }
        }, {
          key: "clearError",
          value: function clearError() {
            this.store.dispatch(_auth_store_actions_auth_action__WEBPACK_IMPORTED_MODULE_7__["actions"].ClearActiveErrorAction());
            this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_8__["actions"].ClearActiveErrorAction());
            this.store.dispatch(_rider_store_actions_rider_action__WEBPACK_IMPORTED_MODULE_9__["actions"].ClearActiveErrorAction());
          }
        }, {
          key: "clearMessage",
          value: function clearMessage() {
            this.store.dispatch(_auth_store_actions_auth_action__WEBPACK_IMPORTED_MODULE_7__["actions"].ClearActiveMessageAction());
            this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_8__["actions"].ClearActiveMessageAction());
            this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_8__["actions"].ClearActiveActionMessageAction());
            this.store.dispatch(_rider_store_actions_rider_action__WEBPACK_IMPORTED_MODULE_9__["actions"].ClearActiveMessageAction());
            this.store.dispatch(_rider_store_actions_rider_action__WEBPACK_IMPORTED_MODULE_9__["actions"].ClearActiveActionMessageAction());
          }
        }, {
          key: "subscribeToEvents",
          value: function subscribeToEvents() {
            var _this2 = this;

            this.subSink.sink = this.signalRSrv.cartAssignedToRider.subscribe(function (payload) {
              _this2._ngZone.run(function () {
                console.log({
                  payload: payload
                });

                if (payload === null || payload === void 0 ? void 0 : payload.person) {
                  // ? Open local push notifications
                  // this.localNotificationSrv.initPush({
                  //   Id: 1,
                  //   Title: "Cart Assigned",
                  //   Body: "New cart assigned to you",
                  //   ExtraData: {
                  //     Id: payload.cart.id,
                  //     PushNotificationType: PushNotificationCategory.CART_ASSIGNMENT
                  //   }
                  // });
                  _this2.store.dispatch(_rider_store_actions_rider_action__WEBPACK_IMPORTED_MODULE_9__["actions"].NewCartAssignedToRiderAction({
                    payload: {
                      cart: payload
                    }
                  }));
                }
              });
            }); // ? Emit the rider current location every x seconds

            this.subSink.sink = this.locationSrv.getRiderCurrentGeoPosition.subscribe(function (_ref) {
              var Payload = _ref.Payload,
                  UserId = _ref.UserId;

              _this2._ngZone.run(function () {
                var runningInterval;

                if (Payload && Payload > 0) {
                  runningInterval = setInterval(function () {
                    var thenAble = _this2.locationSrv.getUserCurrentPosition();

                    thenAble.then(function (_ref2) {
                      var lat = _ref2.lat,
                          lng = _ref2.lng;
                      console.log({
                        riderLocation: {
                          lat: lat,
                          lng: lng
                        }
                      });

                      _this2.store.dispatch(_rider_store_actions_rider_action__WEBPACK_IMPORTED_MODULE_9__["actions"].SaveRiderGeolocationInitiatedAction({
                        payload: {
                          latitude: lat,
                          longitude: lng,
                          userId: UserId
                        }
                      }));
                    });
                  }, 60000 * 2);
                } else {
                  clearInterval(runningInterval);
                }
              });
            });
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            this.subSink.unsubscribe();
          }
        }]);

        return AppComponent;
      }();

      AppComponent.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"]
        }, {
          type: _ngrx_store__WEBPACK_IMPORTED_MODULE_4__["Store"]
        }, {
          type: _services_location_service__WEBPACK_IMPORTED_MODULE_12__["LocationService"]
        }, {
          type: _services_signalr_websocket_service__WEBPACK_IMPORTED_MODULE_10__["SignalrWebsocketService"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"]
        }, {
          type: _services_navigation_service__WEBPACK_IMPORTED_MODULE_13__["NavigationService"]
        }, {
          type: _services_fcm_service__WEBPACK_IMPORTED_MODULE_11__["FcmService"]
        }];
      };

      AppComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-root",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./app.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./app.component.scss */
        "./src/app/app.component.scss"))["default"]]
      })], AppComponent);
      /***/
    },

    /***/
    "./src/app/app.module.ts":
    /*!*******************************!*\
      !*** ./src/app/app.module.ts ***!
      \*******************************/

    /*! exports provided: AppModule */

    /***/
    function srcAppAppModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AppModule", function () {
        return AppModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/platform-browser */
      "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ngrx_store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ngrx/store */
      "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");
      /* harmony import */


      var _ngrx_effects__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ngrx/effects */
      "./node_modules/@ngrx/effects/__ivy_ngcc__/fesm2015/ngrx-effects.js");
      /* harmony import */


      var _ngrx_store_devtools__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ngrx/store-devtools */
      "./node_modules/@ngrx/store-devtools/__ivy_ngcc__/fesm2015/ngrx-store-devtools.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! @ionic-native/splash-screen/ngx */
      "./node_modules/@ionic-native/splash-screen/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! @ionic-native/status-bar/ngx */
      "./node_modules/@ionic-native/status-bar/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
      /* harmony import */


      var _app_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! ./app.component */
      "./src/app/app.component.ts");
      /* harmony import */


      var _app_routing_module__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
      /*! ./app-routing.module */
      "./src/app/app-routing.module.ts");
      /* harmony import */


      var _shared_shared_module__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
      /*! ./shared/shared.module */
      "./src/app/shared/shared.module.ts");
      /* harmony import */


      var _environments_environment__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
      /*! ../environments/environment */
      "./src/environments/environment.ts");
      /* harmony import */


      var _auth_interceptors_auth_header_interceptor__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
      /*! ./auth/interceptors/auth-header.interceptor */
      "./src/app/auth/interceptors/auth-header.interceptor.ts");
      /* harmony import */


      var _auth_interceptors_internet_connection_interceptor__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
      /*! ./auth/interceptors/internet-connection.interceptor */
      "./src/app/auth/interceptors/internet-connection.interceptor.ts");
      /* harmony import */


      var _auth_interceptors_cache_interceptor__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(
      /*! ./auth/interceptors/cache.interceptor */
      "./src/app/auth/interceptors/cache.interceptor.ts");
      /* harmony import */


      var _auth_auth_module__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(
      /*! ./auth/auth.module */
      "./src/app/auth/auth.module.ts");
      /* harmony import */


      var _rider_rider_module__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(
      /*! ./rider/rider.module */
      "./src/app/rider/rider.module.ts");
      /* harmony import */


      var _user_user_module__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(
      /*! ./user/user.module */
      "./src/app/user/user.module.ts");
      /* harmony import */


      var _store_reducer_app_reducer__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(
      /*! ./store/reducer/app.reducer */
      "./src/app/store/reducer/app.reducer.ts");

      var AppModule = function AppModule() {
        _classCallCheck(this, AppModule);
      };

      AppModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_app_component__WEBPACK_IMPORTED_MODULE_11__["AppComponent"]],
        entryComponents: [],
        imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["BrowserModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["IonicModule"].forRoot(), _app_routing_module__WEBPACK_IMPORTED_MODULE_12__["AppRoutingModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_13__["SharedModule"], _auth_auth_module__WEBPACK_IMPORTED_MODULE_18__["AuthModule"], _rider_rider_module__WEBPACK_IMPORTED_MODULE_19__["RiderModule"], _user_user_module__WEBPACK_IMPORTED_MODULE_20__["UserModule"], _ngrx_store__WEBPACK_IMPORTED_MODULE_4__["StoreModule"].forRoot([_store_reducer_app_reducer__WEBPACK_IMPORTED_MODULE_21__["AppReducer"]]), _ngrx_effects__WEBPACK_IMPORTED_MODULE_5__["EffectsModule"].forRoot([]), _ngrx_store_devtools__WEBPACK_IMPORTED_MODULE_6__["StoreDevtoolsModule"].instrument({
          maxAge: 25,
          logOnly: _environments_environment__WEBPACK_IMPORTED_MODULE_14__["environment"].production
        })],
        providers: [_ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_9__["StatusBar"], _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_8__["SplashScreen"], {
          provide: _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouteReuseStrategy"],
          useClass: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["IonicRouteStrategy"]
        }, {
          provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_10__["HTTP_INTERCEPTORS"],
          useClass: _auth_interceptors_auth_header_interceptor__WEBPACK_IMPORTED_MODULE_15__["AuthHeaderInterceptor"],
          multi: true
        }, {
          provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_10__["HTTP_INTERCEPTORS"],
          useClass: _auth_interceptors_internet_connection_interceptor__WEBPACK_IMPORTED_MODULE_16__["InternetConnectionInterceptor"],
          multi: true
        }, {
          provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_10__["HTTP_INTERCEPTORS"],
          useClass: _auth_interceptors_cache_interceptor__WEBPACK_IMPORTED_MODULE_17__["CacheInterceptor"],
          multi: true
        }, {
          provide: _angular_core__WEBPACK_IMPORTED_MODULE_1__["DEFAULT_CURRENCY_CODE"],
          useValue: "₦"
        }],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_11__["AppComponent"]]
      })], AppModule);
      /***/
    },

    /***/
    "./src/app/auth/auth-routing.module.ts":
    /*!*********************************************!*\
      !*** ./src/app/auth/auth-routing.module.ts ***!
      \*********************************************/

    /*! exports provided: AuthRoutingModule */

    /***/
    function srcAppAuthAuthRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthRoutingModule", function () {
        return AuthRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");

      var routes = [{
        path: "",
        children: [{
          path: "",
          pathMatch: "full",
          redirectTo: "login"
        }, {
          path: 'login',
          loadChildren: function loadChildren() {
            return __webpack_require__.e(
            /*! import() | login-login-module */
            "login-login-module").then(__webpack_require__.bind(null,
            /*! ./login/login.module */
            "./src/app/auth/login/login.module.ts")).then(function (m) {
              return m.LoginPageModule;
            });
          }
        }, {
          path: 'forgot-password',
          loadChildren: function loadChildren() {
            return __webpack_require__.e(
            /*! import() | forgot-password-forgot-password-module */
            "forgot-password-forgot-password-module").then(__webpack_require__.bind(null,
            /*! ./forgot-password/forgot-password.module */
            "./src/app/auth/forgot-password/forgot-password.module.ts")).then(function (m) {
              return m.ForgotPasswordPageModule;
            });
          }
        }, {
          path: 'sign-up',
          loadChildren: function loadChildren() {
            return __webpack_require__.e(
            /*! import() | sign-up-sign-up-module */
            "sign-up-sign-up-module").then(__webpack_require__.bind(null,
            /*! ./sign-up/sign-up.module */
            "./src/app/auth/sign-up/sign-up.module.ts")).then(function (m) {
              return m.SignUpPageModule;
            });
          }
        }, {
          path: "sign-up-otp-validation/:phoneNumber",
          loadChildren: function loadChildren() {
            return Promise.all(
            /*! import() | sign-up-otp-verification-sign-up-otp-verification-module */
            [__webpack_require__.e("common"), __webpack_require__.e("sign-up-otp-verification-sign-up-otp-verification-module")]).then(__webpack_require__.bind(null,
            /*! ./sign-up-otp-verification/sign-up-otp-verification.module */
            "./src/app/auth/sign-up-otp-verification/sign-up-otp-verification.module.ts")).then(function (m) {
              return m.SignUpOtpVerificationPageModule;
            });
          }
        }, {
          path: 'validate-otp/:phoneNumber',
          loadChildren: function loadChildren() {
            return Promise.all(
            /*! import() | validate-otp-validate-otp-module */
            [__webpack_require__.e("common"), __webpack_require__.e("validate-otp-validate-otp-module")]).then(__webpack_require__.bind(null,
            /*! ./validate-otp/validate-otp.module */
            "./src/app/auth/validate-otp/validate-otp.module.ts")).then(function (m) {
              return m.ValidateOtpPageModule;
            });
          }
        }, {
          path: 'reset-password/:phoneNumber',
          loadChildren: function loadChildren() {
            return __webpack_require__.e(
            /*! import() | reset-password-reset-password-module */
            "reset-password-reset-password-module").then(__webpack_require__.bind(null,
            /*! ./reset-password/reset-password.module */
            "./src/app/auth/reset-password/reset-password.module.ts")).then(function (m) {
              return m.ResetPasswordPageModule;
            });
          }
        }, {
          path: 'update-facebook-user-detail/:facebookId',
          loadChildren: function loadChildren() {
            return __webpack_require__.e(
            /*! import() | update-facebook-user-detail-update-facebook-user-detail-module */
            "update-facebook-user-detail-update-facebook-user-detail-module").then(__webpack_require__.bind(null,
            /*! ./update-facebook-user-detail/update-facebook-user-detail.module */
            "./src/app/auth/update-facebook-user-detail/update-facebook-user-detail.module.ts")).then(function (m) {
              return m.UpdateFacebookUserDetailPageModule;
            });
          }
        }]
      }];

      var AuthRoutingModule = function AuthRoutingModule() {
        _classCallCheck(this, AuthRoutingModule);
      };

      AuthRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], AuthRoutingModule);
      /***/
    },

    /***/
    "./src/app/auth/auth.module.ts":
    /*!*************************************!*\
      !*** ./src/app/auth/auth.module.ts ***!
      \*************************************/

    /*! exports provided: AuthModule */

    /***/
    function srcAppAuthAuthModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthModule", function () {
        return AuthModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _ngrx_effects__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ngrx/effects */
      "./node_modules/@ngrx/effects/__ivy_ngcc__/fesm2015/ngrx-effects.js");
      /* harmony import */


      var _ngrx_store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ngrx/store */
      "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");
      /* harmony import */


      var _shared_shared_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../shared/shared.module */
      "./src/app/shared/shared.module.ts");
      /* harmony import */


      var _auth_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./auth-routing.module */
      "./src/app/auth/auth-routing.module.ts");
      /* harmony import */


      var _store_effects_auth_effect_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./store/effects/auth-effect.service */
      "./src/app/auth/store/effects/auth-effect.service.ts");
      /* harmony import */


      var _store_reducer_auth_reducer__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ./store/reducer/auth.reducer */
      "./src/app/auth/store/reducer/auth.reducer.ts");

      var AuthModule = function AuthModule() {
        _classCallCheck(this, AuthModule);
      };

      AuthModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [],
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_5__["SharedModule"], _auth_routing_module__WEBPACK_IMPORTED_MODULE_6__["AuthRoutingModule"], _ngrx_effects__WEBPACK_IMPORTED_MODULE_3__["EffectsModule"].forFeature([_store_effects_auth_effect_service__WEBPACK_IMPORTED_MODULE_7__["AuthEffectService"]]), _ngrx_store__WEBPACK_IMPORTED_MODULE_4__["StoreModule"].forFeature("Auth", _store_reducer_auth_reducer__WEBPACK_IMPORTED_MODULE_8__["AuthReducer"])],
        exports: [_auth_routing_module__WEBPACK_IMPORTED_MODULE_6__["AuthRoutingModule"]]
      })], AuthModule);
      /***/
    },

    /***/
    "./src/app/auth/auth.service.ts":
    /*!**************************************!*\
      !*** ./src/app/auth/auth.service.ts ***!
      \**************************************/

    /*! exports provided: AuthService */

    /***/
    function srcAppAuthAuthServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthService", function () {
        return AuthService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _capacitor_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @capacitor/core */
      "./node_modules/@capacitor/core/dist/esm/index.js");
      /* harmony import */


      var _capacitor_community_facebook_login__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @capacitor-community/facebook-login */
      "./node_modules/@capacitor-community/facebook-login/dist/esm/index.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _ngrx_store__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @ngrx/store */
      "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");
      /* harmony import */


      var _utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ../../utils/functions/app.functions */
      "./src/utils/functions/app.functions.ts");
      /* harmony import */


      var _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ../../utils/types/app.constant */
      "./src/utils/types/app.constant.ts");
      /* harmony import */


      var _shared_no_internet_modal_no_internet_modal_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! ../shared/no-internet-modal/no-internet-modal.component */
      "./src/app/shared/no-internet-modal/no-internet-modal.component.ts");
      /* harmony import */


      var _store_actions_auth_action__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! ./store/actions/auth.action */
      "./src/app/auth/store/actions/auth.action.ts");

      Object(_capacitor_core__WEBPACK_IMPORTED_MODULE_3__["registerWebPlugin"])(_capacitor_community_facebook_login__WEBPACK_IMPORTED_MODULE_4__["FacebookLogin"]);

      var AuthService = /*#__PURE__*/function () {
        function AuthService(store, router, httpClient, modalController) {
          _classCallCheck(this, AuthService);

          this.store = store;
          this.router = router;
          this.httpClient = httpClient;
          this.modalController = modalController;
          this.fbToken = null;
        } // ? Open on internet modal


        _createClass(AuthService, [{
          key: "openInternetConnectionStatusModal",
          value: function openInternetConnectionStatusModal() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var _this3 = this;

              var modal;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.next = 2;
                      return this.modalController.create({
                        component: _shared_no_internet_modal_no_internet_modal_component__WEBPACK_IMPORTED_MODULE_10__["NoInternetModalComponent"]
                      });

                    case 2:
                      modal = _context2.sent;
                      _context2.next = 5;
                      return modal.present();

                    case 5:
                      setTimeout(function () {
                        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this3, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                          return regeneratorRuntime.wrap(function _callee$(_context) {
                            while (1) {
                              switch (_context.prev = _context.next) {
                                case 0:
                                  _context.next = 2;
                                  return modal.dismiss();

                                case 2:
                                case "end":
                                  return _context.stop();
                              }
                            }
                          }, _callee);
                        }));
                      }, 5000);

                    case 6:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }, {
          key: "registerDeviceToken",
          value: function registerDeviceToken(deviceToken, userId) {
            this.store.dispatch(_store_actions_auth_action__WEBPACK_IMPORTED_MODULE_11__["actions"].SaveDeviceTokenInitiatedAction({
              payload: {
                token: deviceToken,
                userId: userId
              }
            }));
          } // ? FB login (Begins)

        }, {
          key: "setupFbLogin",
          value: function setupFbLogin() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
              var FacebookLogin;
              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                      if (Object(_ionic_angular__WEBPACK_IMPORTED_MODULE_6__["isPlatform"])("desktop")) {
                        this.fbLogin = _capacitor_community_facebook_login__WEBPACK_IMPORTED_MODULE_4__["FacebookLogin"];
                      } else {
                        // Use the native implementation inside a real app!
                        // tslint:disable-next-line: no-shadowed-variable
                        FacebookLogin = _capacitor_core__WEBPACK_IMPORTED_MODULE_3__["Plugins"].FacebookLogin;
                        this.fbLogin = FacebookLogin;
                      }

                    case 1:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3, this);
            }));
          }
        }, {
          key: "facebookLogin",
          value: function facebookLogin() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
              var FACEBOOK_PERMISSIONS, result;
              return regeneratorRuntime.wrap(function _callee4$(_context4) {
                while (1) {
                  switch (_context4.prev = _context4.next) {
                    case 0:
                      // const FACEBOOK_PERMISSIONS = ['email', 'user_birthday'];
                      FACEBOOK_PERMISSIONS = ["email"];
                      _context4.next = 3;
                      return this.fbLogin.login({
                        permissions: FACEBOOK_PERMISSIONS
                      });

                    case 3:
                      result = _context4.sent;

                      if (!(result.accessToken && result.accessToken.userId)) {
                        _context4.next = 11;
                        break;
                      }

                      this.fbToken = result.accessToken;
                      _context4.next = 8;
                      return this.loadUserData();

                    case 8:
                      return _context4.abrupt("return", _context4.sent);

                    case 11:
                      if (!(result.accessToken && !result.accessToken.userId)) {
                        _context4.next = 17;
                        break;
                      }

                      _context4.next = 14;
                      return this.getCurrentToken();

                    case 14:
                      return _context4.abrupt("return", _context4.sent);

                    case 17:
                    case "end":
                      return _context4.stop();
                  }
                }
              }, _callee4, this);
            }));
          }
        }, {
          key: "getCurrentToken",
          value: function getCurrentToken() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
              var result;
              return regeneratorRuntime.wrap(function _callee5$(_context5) {
                while (1) {
                  switch (_context5.prev = _context5.next) {
                    case 0:
                      _context5.next = 2;
                      return this.fbLogin.getCurrentAccessToken();

                    case 2:
                      result = _context5.sent;

                      if (!result.accessToken) {
                        _context5.next = 10;
                        break;
                      }

                      this.fbToken = result.accessToken;
                      _context5.next = 7;
                      return this.loadUserData();

                    case 7:
                      return _context5.abrupt("return", _context5.sent);

                    case 10:
                    case "end":
                      return _context5.stop();
                  }
                }
              }, _callee5, this);
            }));
          }
        }, {
          key: "getUserJWTToken",
          value: function getUserJWTToken() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
              var userData;
              return regeneratorRuntime.wrap(function _callee6$(_context6) {
                while (1) {
                  switch (_context6.prev = _context6.next) {
                    case 0:
                      _context6.next = 2;
                      return Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_8__["getDataFromLocalStorage"])(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_9__["LocalStorageKey"].ZERO_30_USER);

                    case 2:
                      userData = _context6.sent;
                      return _context6.abrupt("return", userData === null || userData === void 0 ? void 0 : userData.authToken);

                    case 4:
                    case "end":
                      return _context6.stop();
                  }
                }
              }, _callee6);
            }));
          }
        }, {
          key: "loadUserData",
          value: function loadUserData() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee7() {
              var url, userData;
              return regeneratorRuntime.wrap(function _callee7$(_context7) {
                while (1) {
                  switch (_context7.prev = _context7.next) {
                    case 0:
                      url = "https://graph.facebook.com/".concat(this.fbToken.userId, "?fields=id,name,picture.width(720),birthday,email&access_token=").concat(this.fbToken.token);
                      _context7.next = 3;
                      return this.httpClient.get(url).toPromise();

                    case 3:
                      userData = _context7.sent;
                      return _context7.abrupt("return", userData);

                    case 5:
                    case "end":
                      return _context7.stop();
                  }
                }
              }, _callee7, this);
            }));
          } // ? FB login (Ends)

        }, {
          key: "afterLogin",
          value: function afterLogin(authPayload) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee8() {
              return regeneratorRuntime.wrap(function _callee8$(_context8) {
                while (1) {
                  switch (_context8.prev = _context8.next) {
                    case 0:
                      _context8.prev = 0;

                      if (!(authPayload === null || authPayload === void 0 ? void 0 : authPayload.role)) {
                        _context8.next = 6;
                        break;
                      }

                      _context8.next = 4;
                      return Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_8__["saveDataToLocalStorage"])(authPayload, _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_9__["LocalStorageKey"].ZERO_30_USER);

                    case 4:
                      if (authPayload.role === _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_9__["Zero30Role"].CUSTOMER) {
                        this.router.navigate(["/user"]);
                      }

                      if (authPayload.role === _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_9__["Zero30Role"].RIDER) {
                        this.router.navigate(["/rider"]);
                      }

                    case 6:
                      _context8.next = 11;
                      break;

                    case 8:
                      _context8.prev = 8;
                      _context8.t0 = _context8["catch"](0);
                      throw _context8.t0;

                    case 11:
                    case "end":
                      return _context8.stop();
                  }
                }
              }, _callee8, this, [[0, 8]]);
            }));
          }
        }, {
          key: "afterSignUpOTPValidation",
          value: function afterSignUpOTPValidation() {
            this.router.navigate(["/auth", "login"]);
          }
        }, {
          key: "afterLogout",
          value: function afterLogout() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee9() {
              return regeneratorRuntime.wrap(function _callee9$(_context9) {
                while (1) {
                  switch (_context9.prev = _context9.next) {
                    case 0:
                      _context9.prev = 0;
                      _context9.next = 3;
                      return Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_8__["deleteDataFromLocalStorage"])(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_9__["LocalStorageKey"].ZERO_30_USER);

                    case 3:
                      _context9.next = 5;
                      return Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_8__["deleteDataFromLocalStorage"])(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_9__["LocalStorageKey"].BOOKMARKS);

                    case 5:
                      _context9.next = 7;
                      return Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_8__["deleteDataFromLocalStorage"])(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_9__["LocalStorageKey"].URL_HISTORY);

                    case 7:
                      this.router.navigate(["/auth", "login"]);
                      _context9.next = 13;
                      break;

                    case 10:
                      _context9.prev = 10;
                      _context9.t0 = _context9["catch"](0);
                      throw _context9.t0;

                    case 13:
                    case "end":
                      return _context9.stop();
                  }
                }
              }, _callee9, this, [[0, 10]]);
            }));
          }
        }, {
          key: "afterPasswordRecoveryIsInitiated",
          value: function afterPasswordRecoveryIsInitiated(phoneNumber) {
            this.router.navigate(["/auth", "validate-otp", phoneNumber]);
          }
        }, {
          key: "isUserCustomer",
          value: function isUserCustomer() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee10() {
              var isUserCustomer, authData;
              return regeneratorRuntime.wrap(function _callee10$(_context10) {
                while (1) {
                  switch (_context10.prev = _context10.next) {
                    case 0:
                      isUserCustomer = true;
                      _context10.next = 3;
                      return Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_8__["getDataFromLocalStorage"])(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_9__["LocalStorageKey"].ZERO_30_USER);

                    case 3:
                      authData = _context10.sent;

                      if (!((authData === null || authData === void 0 ? void 0 : authData.role) && authData.role === _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_9__["Zero30Role"].CUSTOMER)) {
                        isUserCustomer = false;
                      }

                      return _context10.abrupt("return", isUserCustomer);

                    case 6:
                    case "end":
                      return _context10.stop();
                  }
                }
              }, _callee10);
            }));
          }
        }, {
          key: "isUserRider",
          value: function isUserRider() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee11() {
              var isUserRider, authData;
              return regeneratorRuntime.wrap(function _callee11$(_context11) {
                while (1) {
                  switch (_context11.prev = _context11.next) {
                    case 0:
                      isUserRider = true;
                      _context11.next = 3;
                      return Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_8__["getDataFromLocalStorage"])(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_9__["LocalStorageKey"].ZERO_30_USER);

                    case 3:
                      authData = _context11.sent;

                      if (!((authData === null || authData === void 0 ? void 0 : authData.role) && authData.role === _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_9__["Zero30Role"].RIDER)) {
                        isUserRider = false;
                      }

                      return _context11.abrupt("return", isUserRider);

                    case 6:
                    case "end":
                      return _context11.stop();
                  }
                }
              }, _callee11);
            }));
          }
        }, {
          key: "isUserAuthenticated",
          value: function isUserAuthenticated() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee12() {
              var isUserRider, authData;
              return regeneratorRuntime.wrap(function _callee12$(_context12) {
                while (1) {
                  switch (_context12.prev = _context12.next) {
                    case 0:
                      isUserRider = true;
                      _context12.next = 3;
                      return Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_8__["getDataFromLocalStorage"])(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_9__["LocalStorageKey"].ZERO_30_USER);

                    case 3:
                      authData = _context12.sent;

                      if (!(authData === null || authData === void 0 ? void 0 : authData.authToken)) {
                        isUserRider = false;
                      }

                      return _context12.abrupt("return", isUserRider);

                    case 6:
                    case "end":
                      return _context12.stop();
                  }
                }
              }, _callee12);
            }));
          }
        }, {
          key: "loginAsync",
          value: function loginAsync() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee13() {
              var storageData, role;
              return regeneratorRuntime.wrap(function _callee13$(_context13) {
                while (1) {
                  switch (_context13.prev = _context13.next) {
                    case 0:
                      _context13.prev = 0;
                      _context13.next = 3;
                      return Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_8__["getDataFromLocalStorage"])(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_9__["LocalStorageKey"].ZERO_30_USER);

                    case 3:
                      storageData = _context13.sent;

                      if ((storageData === null || storageData === void 0 ? void 0 : storageData.userId) && (storageData === null || storageData === void 0 ? void 0 : storageData.authToken)) {
                        role = storageData.role; // const lastUrlVisited: string = await this.getLastExistingRoute();
                        // if (lastUrlVisited?.includes(role.toLowerCase())) {
                        //   console.log({ message: "Last url was used", lastUrlVisited });
                        //   this.router.navigateByUrl(lastUrlVisited);
                        // }
                        // ? if exists check for role & navigate

                        if (role === _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_9__["Zero30Role"].RIDER) {
                          this.router.navigate(["/rider"]);
                        }

                        if (role === _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_9__["Zero30Role"].CUSTOMER) {
                          this.router.navigate(["/user"]);
                        }
                      }

                      _context13.next = 10;
                      break;

                    case 7:
                      _context13.prev = 7;
                      _context13.t0 = _context13["catch"](0);
                      throw _context13.t0;

                    case 10:
                    case "end":
                      return _context13.stop();
                  }
                }
              }, _callee13, this, [[0, 7]]);
            }));
          } // async loginAsync(): Promise<void> {
          //   try {
          //     // ? check for token in localstorage
          //     const storageData: AuthResponseType = await getDataFromLocalStorage<AuthResponseType>(LocalStorageKey.ZERO_30_USER);
          //     if (storageData?.userId && storageData?.authToken) {
          //       const { role } = storageData;
          //       const lastUrlVisited: string = await this.getLastExistingRoute();
          //       if (lastUrlVisited) {
          //         this.router.navigateByUrl(lastUrlVisited);
          //       } else {
          //         console.log({ lastUrl: "Was not found" });
          //         // ? if exists check for role & navigate
          //         if (role === Zero30Role.RIDER) {
          //           this.router.navigate(["/rider"]);
          //         }
          //         if (role === Zero30Role.CUSTOMER) {
          //           this.router.navigate(["/user"]);
          //         }
          //       }
          //     }
          //   }
          //   catch (ex) {
          //     throw ex;
          //   }
          // }
          // async loginAsync(): Promise<void> {
          //   try {
          //     // ? check for token in localstorage
          //     const storageData: AuthResponseType = await getDataFromLocalStorage<AuthResponseType>(LocalStorageKey.ZERO_30_USER);
          //     if (storageData?.userId && storageData?.authToken) {
          //       const { role } = storageData;
          //       // ? if exists check for role & navigate
          //       if (role === Zero30Role.RIDER) {
          //         this.router.navigate(["/rider"]);
          //       }
          //       if (role === Zero30Role.CUSTOMER) {
          //         this.router.navigate(["/user"]);
          //       }
          //     }
          //   }
          //   catch (ex) {
          //     throw ex;
          //   }
          // }

        }, {
          key: "getLastExistingRoute",
          value: function getLastExistingRoute() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee14() {
              var urls;
              return regeneratorRuntime.wrap(function _callee14$(_context14) {
                while (1) {
                  switch (_context14.prev = _context14.next) {
                    case 0:
                      _context14.next = 2;
                      return Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_8__["getDataFromLocalStorage"])(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_9__["LocalStorageKey"].URL_HISTORY);

                    case 2:
                      urls = _context14.sent;

                      if (!((urls === null || urls === void 0 ? void 0 : urls.length) > 0)) {
                        _context14.next = 5;
                        break;
                      }

                      return _context14.abrupt("return", urls[urls.length - 2]);

                    case 5:
                    case "end":
                      return _context14.stop();
                  }
                }
              }, _callee14);
            }));
          }
        }]);

        return AuthService;
      }();

      AuthService.ctorParameters = function () {
        return [{
          type: _ngrx_store__WEBPACK_IMPORTED_MODULE_7__["Store"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ModalController"]
        }];
      };

      AuthService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: "root"
      })], AuthService);
      /***/
    },

    /***/
    "./src/app/auth/guards/auth.guard.ts":
    /*!*******************************************!*\
      !*** ./src/app/auth/guards/auth.guard.ts ***!
      \*******************************************/

    /*! exports provided: AuthGuard */

    /***/
    function srcAppAuthGuardsAuthGuardTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthGuard", function () {
        return AuthGuard;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../auth.service */
      "./src/app/auth/auth.service.ts");

      var AuthGuard = /*#__PURE__*/function () {
        function AuthGuard(authSrv, router) {
          _classCallCheck(this, AuthGuard);

          this.authSrv = authSrv;
          this.router = router;
        }

        _createClass(AuthGuard, [{
          key: "canActivate",
          value: function canActivate(next, state) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee15() {
              return regeneratorRuntime.wrap(function _callee15$(_context15) {
                while (1) {
                  switch (_context15.prev = _context15.next) {
                    case 0:
                      _context15.next = 2;
                      return this.handleRouteActivation();

                    case 2:
                      return _context15.abrupt("return", _context15.sent);

                    case 3:
                    case "end":
                      return _context15.stop();
                  }
                }
              }, _callee15, this);
            }));
          }
        }, {
          key: "canActivateChild",
          value: function canActivateChild(next, state) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee16() {
              return regeneratorRuntime.wrap(function _callee16$(_context16) {
                while (1) {
                  switch (_context16.prev = _context16.next) {
                    case 0:
                      _context16.next = 2;
                      return this.handleRouteActivation();

                    case 2:
                      return _context16.abrupt("return", _context16.sent);

                    case 3:
                    case "end":
                      return _context16.stop();
                  }
                }
              }, _callee16, this);
            }));
          }
        }, {
          key: "handleRouteActivation",
          value: function handleRouteActivation() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee17() {
              var userToken;
              return regeneratorRuntime.wrap(function _callee17$(_context17) {
                while (1) {
                  switch (_context17.prev = _context17.next) {
                    case 0:
                      userToken = this.authSrv.isUserAuthenticated();

                      if (!userToken) {
                        _context17.next = 5;
                        break;
                      }

                      return _context17.abrupt("return", this.authSrv.isUserAuthenticated());

                    case 5:
                      this.router.navigate(["/auth", "login"]);
                      return _context17.abrupt("return", false);

                    case 7:
                    case "end":
                      return _context17.stop();
                  }
                }
              }, _callee17, this);
            }));
          }
        }]);

        return AuthGuard;
      }();

      AuthGuard.ctorParameters = function () {
        return [{
          type: _auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }];
      };

      AuthGuard = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: "root"
      })], AuthGuard);
      /***/
    },

    /***/
    "./src/app/auth/guards/customer.guard.ts":
    /*!***********************************************!*\
      !*** ./src/app/auth/guards/customer.guard.ts ***!
      \***********************************************/

    /*! exports provided: CustomerGuard */

    /***/
    function srcAppAuthGuardsCustomerGuardTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CustomerGuard", function () {
        return CustomerGuard;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../auth.service */
      "./src/app/auth/auth.service.ts");

      var CustomerGuard = /*#__PURE__*/function () {
        function CustomerGuard(authSrv, router) {
          _classCallCheck(this, CustomerGuard);

          this.authSrv = authSrv;
          this.router = router;
        }

        _createClass(CustomerGuard, [{
          key: "canActivate",
          value: function canActivate(next, state) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee18() {
              return regeneratorRuntime.wrap(function _callee18$(_context18) {
                while (1) {
                  switch (_context18.prev = _context18.next) {
                    case 0:
                      _context18.next = 2;
                      return this.handleRouteActivation();

                    case 2:
                      return _context18.abrupt("return", _context18.sent);

                    case 3:
                    case "end":
                      return _context18.stop();
                  }
                }
              }, _callee18, this);
            }));
          }
        }, {
          key: "canActivateChild",
          value: function canActivateChild(next, state) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee19() {
              return regeneratorRuntime.wrap(function _callee19$(_context19) {
                while (1) {
                  switch (_context19.prev = _context19.next) {
                    case 0:
                      _context19.next = 2;
                      return this.handleRouteActivation();

                    case 2:
                      return _context19.abrupt("return", _context19.sent);

                    case 3:
                    case "end":
                      return _context19.stop();
                  }
                }
              }, _callee19, this);
            }));
          }
        }, {
          key: "handleRouteActivation",
          value: function handleRouteActivation() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee20() {
              var isCustomer;
              return regeneratorRuntime.wrap(function _callee20$(_context20) {
                while (1) {
                  switch (_context20.prev = _context20.next) {
                    case 0:
                      _context20.next = 2;
                      return this.authSrv.isUserCustomer();

                    case 2:
                      isCustomer = _context20.sent;

                      if (!isCustomer) {
                        _context20.next = 7;
                        break;
                      }

                      return _context20.abrupt("return", true);

                    case 7:
                      this.router.navigate(["/auth", "login"]);
                      return _context20.abrupt("return", false);

                    case 9:
                    case "end":
                      return _context20.stop();
                  }
                }
              }, _callee20, this);
            }));
          }
        }]);

        return CustomerGuard;
      }();

      CustomerGuard.ctorParameters = function () {
        return [{
          type: _auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }];
      };

      CustomerGuard = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: "root"
      })], CustomerGuard);
      /***/
    },

    /***/
    "./src/app/auth/guards/rider.guard.ts":
    /*!********************************************!*\
      !*** ./src/app/auth/guards/rider.guard.ts ***!
      \********************************************/

    /*! exports provided: RiderGuard */

    /***/
    function srcAppAuthGuardsRiderGuardTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RiderGuard", function () {
        return RiderGuard;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../auth.service */
      "./src/app/auth/auth.service.ts");

      var RiderGuard = /*#__PURE__*/function () {
        function RiderGuard(authSrv, router) {
          _classCallCheck(this, RiderGuard);

          this.authSrv = authSrv;
          this.router = router;
        }

        _createClass(RiderGuard, [{
          key: "canActivate",
          value: function canActivate(next, state) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee21() {
              return regeneratorRuntime.wrap(function _callee21$(_context21) {
                while (1) {
                  switch (_context21.prev = _context21.next) {
                    case 0:
                      _context21.next = 2;
                      return this.handleRouteActivation();

                    case 2:
                      return _context21.abrupt("return", _context21.sent);

                    case 3:
                    case "end":
                      return _context21.stop();
                  }
                }
              }, _callee21, this);
            }));
          }
        }, {
          key: "canActivateChild",
          value: function canActivateChild(next, state) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee22() {
              return regeneratorRuntime.wrap(function _callee22$(_context22) {
                while (1) {
                  switch (_context22.prev = _context22.next) {
                    case 0:
                      _context22.next = 2;
                      return this.handleRouteActivation();

                    case 2:
                      return _context22.abrupt("return", _context22.sent);

                    case 3:
                    case "end":
                      return _context22.stop();
                  }
                }
              }, _callee22, this);
            }));
          }
        }, {
          key: "handleRouteActivation",
          value: function handleRouteActivation() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee23() {
              var isRider;
              return regeneratorRuntime.wrap(function _callee23$(_context23) {
                while (1) {
                  switch (_context23.prev = _context23.next) {
                    case 0:
                      _context23.next = 2;
                      return this.authSrv.isUserRider();

                    case 2:
                      isRider = _context23.sent;

                      if (!isRider) {
                        _context23.next = 7;
                        break;
                      }

                      return _context23.abrupt("return", true);

                    case 7:
                      this.router.navigate(["/auth", "login"]);
                      return _context23.abrupt("return", false);

                    case 9:
                    case "end":
                      return _context23.stop();
                  }
                }
              }, _callee23, this);
            }));
          }
        }]);

        return RiderGuard;
      }();

      RiderGuard.ctorParameters = function () {
        return [{
          type: _auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }];
      };

      RiderGuard = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: "root"
      })], RiderGuard);
      /***/
    },

    /***/
    "./src/app/auth/interceptors/auth-header.interceptor.ts":
    /*!**************************************************************!*\
      !*** ./src/app/auth/interceptors/auth-header.interceptor.ts ***!
      \**************************************************************/

    /*! exports provided: AuthHeaderInterceptor */

    /***/
    function srcAppAuthInterceptorsAuthHeaderInterceptorTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthHeaderInterceptor", function () {
        return AuthHeaderInterceptor;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! rxjs */
      "./node_modules/rxjs/_esm2015/index.js");
      /* harmony import */


      var _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../../../utils/types/app.constant */
      "./src/utils/types/app.constant.ts");
      /* harmony import */


      var _utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../../../utils/functions/app.functions */
      "./src/utils/functions/app.functions.ts");

      var AuthHeaderInterceptor = /*#__PURE__*/function () {
        function AuthHeaderInterceptor() {
          _classCallCheck(this, AuthHeaderInterceptor);
        }

        _createClass(AuthHeaderInterceptor, [{
          key: "intercept",
          value: function intercept(request, next) {
            try {
              // convert promise to observable using 'from' operator
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["from"])(this.handle(request, next));
            } catch (ex) {
              throw ex;
            }
          }
        }, {
          key: "handle",
          value: function handle(request, next) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee24() {
              var authData, authRequest;
              return regeneratorRuntime.wrap(function _callee24$(_context24) {
                while (1) {
                  switch (_context24.prev = _context24.next) {
                    case 0:
                      _context24.next = 2;
                      return Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_4__["getDataFromLocalStorage"])(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_3__["LocalStorageKey"].ZERO_30_USER);

                    case 2:
                      authData = _context24.sent;

                      if (!(authData === null || authData === void 0 ? void 0 : authData.authToken)) {
                        _context24.next = 6;
                        break;
                      }

                      authRequest = request.clone({
                        headers: request.headers.set("Authorization", "Bearer ".concat(authData.authToken))
                      });
                      return _context24.abrupt("return", next.handle(authRequest).toPromise());

                    case 6:
                      return _context24.abrupt("return", next.handle(request).toPromise());

                    case 7:
                    case "end":
                      return _context24.stop();
                  }
                }
              }, _callee24);
            }));
          }
        }]);

        return AuthHeaderInterceptor;
      }();

      AuthHeaderInterceptor = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()], AuthHeaderInterceptor);
      /***/
    },

    /***/
    "./src/app/auth/interceptors/cache.interceptor.ts":
    /*!********************************************************!*\
      !*** ./src/app/auth/interceptors/cache.interceptor.ts ***!
      \********************************************************/

    /*! exports provided: CacheInterceptor */

    /***/
    function srcAppAuthInterceptorsCacheInterceptorTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CacheInterceptor", function () {
        return CacheInterceptor;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! rxjs */
      "./node_modules/rxjs/_esm2015/index.js");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! rxjs/operators */
      "./node_modules/rxjs/_esm2015/operators/index.js");
      /* harmony import */


      var _services_http_cache_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../../services/http-cache.service */
      "./src/app/services/http-cache.service.ts");

      var CacheInterceptor = /*#__PURE__*/function () {
        function CacheInterceptor(cacheService) {
          _classCallCheck(this, CacheInterceptor);

          this.cacheService = cacheService;
        }

        _createClass(CacheInterceptor, [{
          key: "intercept",
          value: function intercept(req, next) {
            var _this4 = this;

            // pass along non-cacheable requests and invalidate cache
            if (req.method !== "GET") {
              console.log("Invalidating cache: ".concat(req.method, " ").concat(req.url));
              this.cacheService.invalidateCache();
              return next.handle(req);
            } // attempt to retrieve a cached response


            var cachedResponse = this.cacheService.get(req.url); // return cached response

            if (cachedResponse) {
              console.log("Returning a cached response: ".concat(cachedResponse.url));
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(cachedResponse);
            } // send request to server and add response to cache


            return next.handle(req).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (event) {
              if (event instanceof _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpResponse"]) {
                console.log("Adding item to cache: ".concat(req.url));

                _this4.cacheService.put(req.url, event);
              }
            }));
          }
        }]);

        return CacheInterceptor;
      }();

      CacheInterceptor.ctorParameters = function () {
        return [{
          type: _services_http_cache_service__WEBPACK_IMPORTED_MODULE_5__["HttpCacheService"]
        }];
      };

      CacheInterceptor = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])()], CacheInterceptor);
      /***/
    },

    /***/
    "./src/app/auth/interceptors/internet-connection.interceptor.ts":
    /*!**********************************************************************!*\
      !*** ./src/app/auth/interceptors/internet-connection.interceptor.ts ***!
      \**********************************************************************/

    /*! exports provided: InternetConnectionInterceptor */

    /***/
    function srcAppAuthInterceptorsInternetConnectionInterceptorTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "InternetConnectionInterceptor", function () {
        return InternetConnectionInterceptor;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! rxjs */
      "./node_modules/rxjs/_esm2015/index.js");
      /* harmony import */


      var _auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../auth.service */
      "./src/app/auth/auth.service.ts");

      var InternetConnectionInterceptor = /*#__PURE__*/function () {
        function InternetConnectionInterceptor(authSrv) {
          _classCallCheck(this, InternetConnectionInterceptor);

          this.authSrv = authSrv;
        }

        _createClass(InternetConnectionInterceptor, [{
          key: "intercept",
          value: function intercept(request, next) {
            // ? Check for internet connection
            if (!navigator.onLine) {
              this.authSrv.openInternetConnectionStatusModal();
              var response = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpResponse"]({
                status: 400,
                body: {
                  Message: "No internet connection detected"
                }
              });
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(response);
            }

            return next.handle(request);
          }
        }]);

        return InternetConnectionInterceptor;
      }();

      InternetConnectionInterceptor.ctorParameters = function () {
        return [{
          type: _auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]
        }];
      };

      InternetConnectionInterceptor = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()], InternetConnectionInterceptor);
      /***/
    },

    /***/
    "./src/app/auth/store/actions/auth.action.ts":
    /*!***************************************************!*\
      !*** ./src/app/auth/store/actions/auth.action.ts ***!
      \***************************************************/

    /*! exports provided: AuthActionType, actions */

    /***/
    function srcAppAuthStoreActionsAuthActionTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthActionType", function () {
        return AuthActionType;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "actions", function () {
        return actions;
      });
      /* harmony import */


      var _ngrx_store__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @ngrx/store */
      "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");

      var AuthActionType;

      (function (AuthActionType) {
        AuthActionType["CLEAR_ACTIVE_ERROR"] = "[ERROR_AUTH] CLEAR_ACTIVE_ERROR";
        AuthActionType["CLEAR_ACTIVE_MESSAGE"] = "[ERROR_AUTH] CLEAR_ACTIVE_MESSAGE";
        AuthActionType["LOGIN_INITIATED"] = "[AUTH] LOGIN_INITIATED";
        AuthActionType["LOGIN_FAILED"] = "[AUTH] LOGIN_FAILED";
        AuthActionType["LOGIN_SUCCESSFUL"] = "[AUTH] LOGIN_SUCCESSFUL";
        AuthActionType["LOGOUT_INITIATED"] = "[AUTH] LOGOUT_INTIATED";
        AuthActionType["LOGOUT_FAILED"] = "[AUTH] LOGOUT_FAILED";
        AuthActionType["LOGOUT_SUCCESSFUL"] = "[AUTH] LOGOUT_SUCCESSFUL";
        AuthActionType["SIGN_UP_INITIATED"] = "[AUTH] SIGN_UP_INITIATED";
        AuthActionType["SIGN_UP_FAILED"] = "[AUTH] SIGN_UP_FAILED";
        AuthActionType["SIGN_UP_SUCCESSFUL"] = "[AUTH] SIGN_UP_SUCCESSFUL";
        AuthActionType["VERIFY_OTP_TOKEN_INITIATED"] = "[VERIFY_OTP] VERIFY_TOP_TOKEN_INTIATED";
        AuthActionType["VERIFY_OTP_TOKEN_FAILED"] = "[VERIFY_OTP] VERIFY_OTP_TOKEN_FAILED";
        AuthActionType["VERIFY_OTP_TOKEN_SUCCESSFUL"] = "[VERIFY_OTP] VERIFY_OTP_TOKEN_SUCCESSFUL";
        AuthActionType["VERIFY_OTP_AFTER_SIGN_UP_INITIATED"] = "[SIGN_UP_VERIFICATION] VERIFY_OTP_AFTER_SIGN_UP_INITIATED";
        AuthActionType["VERIFY_OTP_AFTER_SIGN_UP_FAILED"] = "[SIGN_UP_VERIFICATION] VERIFY_OTP_AFTER_SIGN_UP_FAILED";
        AuthActionType["VERIFY_OTP_AFTER_SIGN_UP_SUCCESSFUL"] = "[SIGN_UP_VERIFICATION] VERIFY_OTP_AFTER_SIGN_UP_SUCCESSFUL";
        AuthActionType["PASSWORD_RECOVERY_INITIATED"] = "[FORGOT_PASSWORD] PASSWORD_RECOVERY_INITIATED";
        AuthActionType["PASSWORD_RECOVERY_FAILED"] = "[FORGOT_PASSWORD] PASSWORD_RECOVERY_FAILED";
        AuthActionType["PASSWORD_RECOVERY_SUCCESSFUL"] = "[FORGOT_PASSWORD] PASSWORD_RECOVERY_SUCCESSFUL";
        AuthActionType["CHANGE_PASSWORD_INTITIATED"] = "[CHANGE_PASSWORD] CHANGE_PASSWORD_INTITIATED";
        AuthActionType["CHANGE_PASSWORD_FAILED"] = "[CHANGE_PASSWORD] CHANGE_PASSWORD_FAILED";
        AuthActionType["CHANGE_PASSWORD_SUCCESSFUL"] = "[CHANGE_PASSWORD] CHANGE_PASSWORD_SUCCESSFUL";
        AuthActionType["RESEND_OTP_TOKEN_INITIATED"] = "[RESEND_OTP] RESEND_OTP_TOKEN_INITIATED";
        AuthActionType["RESEND_OTP_TOKEN_FAILED"] = "[RESEND_OTP] RESEND_OTP_TOKEN_FAILED";
        AuthActionType["RESEND_OTP_TOKEN_SUCCESSFUL"] = "[RESEND_OTP] RESEND_OTP_TOKEN_SUCCESSFUL";
        AuthActionType["FACEBOOK_LOGIN_INITIATED"] = "[FACEBOOK_LOGIN] FACEBOOK_LOGIN_INITIATED";
        AuthActionType["FACEBOOK_LOGIN_FAILED"] = "[FACEBOOK_LOGIN] FACEBOOK_LOGIN_FAILED";
        AuthActionType["FACEBOOK_LOGIN_SUCCESSFUL"] = "[FACEBOOK_LOGIN] FACEBOOK_LOGIN_SUCCESSFUL";
        AuthActionType["UPDATE_FACEBOOK_USER_DETAILS_INITIATED"] = "[FACEBOOK_LOGIN] UPDATE_FACEBOOK_USER_DETAILS_INITIATED";
        AuthActionType["UPDATE_FACEBOOK_USER_DETAILS_FAILED"] = "[FACEBOOK_LOGIN] UPDATE_FACEBOOK_USER_DETAILS_FAILED";
        AuthActionType["UPDATE_FACEBOOK_USER_DETAILS_SUCCESSFUL"] = "[FACEBOOK_LOGIN] UPDATE_FACEBOOK_USER_DETAILS_SUCCESSFUL";
        AuthActionType["SAVE_DEVICE_TOKEN_INITIATED"] = "[DEVICE_NOTIFICATION] SAVE_DEVICE_TOKEN_INITIATED";
        AuthActionType["SAVE_DEVICE_TOKEN_FAILED"] = "[DEVICE_NOTIFICATION] SAVE_DEVICE_TOKEN_FAILED";
        AuthActionType["SAVE_DEVICE_TOKEN_SUCCESSFUL"] = "[DEVICE_NOTIFICATION] SAVE_DEVICE_TOKEN_SUCCESSFUL";
      })(AuthActionType || (AuthActionType = {}));

      var actions = {
        ClearActiveErrorAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(AuthActionType.CLEAR_ACTIVE_ERROR),
        ClearActiveMessageAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(AuthActionType.CLEAR_ACTIVE_MESSAGE),
        SignUpInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(AuthActionType.SIGN_UP_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        SignUpFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(AuthActionType.SIGN_UP_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        SignUpSuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(AuthActionType.SIGN_UP_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        VerifyOTPAfterSignUpInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(AuthActionType.VERIFY_OTP_AFTER_SIGN_UP_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        VerifyOTPAfterSignUpFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(AuthActionType.VERIFY_OTP_AFTER_SIGN_UP_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        VerifyOTPAfterSignUpSuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(AuthActionType.VERIFY_OTP_AFTER_SIGN_UP_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        LoginInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(AuthActionType.LOGIN_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        LoginFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(AuthActionType.LOGIN_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        LoginSuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(AuthActionType.LOGIN_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        LogoutInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(AuthActionType.LOGOUT_INITIATED),
        LogoutFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(AuthActionType.LOGOUT_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        LogoutSuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(AuthActionType.LOGOUT_SUCCESSFUL),
        VerifyOTPTokenInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(AuthActionType.VERIFY_OTP_TOKEN_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        VerifyOTPTokenFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(AuthActionType.VERIFY_OTP_TOKEN_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        VerifyOTPTokenSuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(AuthActionType.VERIFY_OTP_TOKEN_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        PasswordRecoveryInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(AuthActionType.PASSWORD_RECOVERY_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        PasswordRecoveryFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(AuthActionType.PASSWORD_RECOVERY_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        PasswordRecoverySuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(AuthActionType.PASSWORD_RECOVERY_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        ChangePasswordInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(AuthActionType.CHANGE_PASSWORD_INTITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        ChangePasswordFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(AuthActionType.CHANGE_PASSWORD_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        ChangePasswordSuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(AuthActionType.CHANGE_PASSWORD_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        ResendOTPInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(AuthActionType.RESEND_OTP_TOKEN_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        ResendOTPFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(AuthActionType.RESEND_OTP_TOKEN_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        ResendOTPSuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(AuthActionType.RESEND_OTP_TOKEN_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        FacebookLoginInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(AuthActionType.FACEBOOK_LOGIN_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        FacebookLoginFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(AuthActionType.FACEBOOK_LOGIN_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        FacebookLoginSuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(AuthActionType.FACEBOOK_LOGIN_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        UpdateFacebookUserDetailsInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(AuthActionType.UPDATE_FACEBOOK_USER_DETAILS_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        UpdateFacebookUserDetailsFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(AuthActionType.UPDATE_FACEBOOK_USER_DETAILS_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        UpdateFacebookUserDetailsSuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(AuthActionType.UPDATE_FACEBOOK_USER_DETAILS_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        SaveDeviceTokenInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(AuthActionType.SAVE_DEVICE_TOKEN_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        SaveDeviceTokenFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(AuthActionType.SAVE_DEVICE_TOKEN_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        SaveDeviceTokenSuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(AuthActionType.SAVE_DEVICE_TOKEN_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])())
      };
      /***/
    },

    /***/
    "./src/app/auth/store/effects/auth-effect.service.ts":
    /*!***********************************************************!*\
      !*** ./src/app/auth/store/effects/auth-effect.service.ts ***!
      \***********************************************************/

    /*! exports provided: AuthEffectService */

    /***/
    function srcAppAuthStoreEffectsAuthEffectServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthEffectService", function () {
        return AuthEffectService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ngrx_effects__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ngrx/effects */
      "./node_modules/@ngrx/effects/__ivy_ngcc__/fesm2015/ngrx-effects.js");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! rxjs */
      "./node_modules/rxjs/_esm2015/index.js");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! rxjs/operators */
      "./node_modules/rxjs/_esm2015/operators/index.js");
      /* harmony import */


      var src_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! src/utils/functions/app.functions */
      "./src/utils/functions/app.functions.ts");
      /* harmony import */


      var _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../../../../utils/types/app.constant */
      "./src/utils/types/app.constant.ts");
      /* harmony import */


      var _auth_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ../../auth.service */
      "./src/app/auth/auth.service.ts");
      /* harmony import */


      var _actions_auth_action__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ../actions/auth.action */
      "./src/app/auth/store/actions/auth.action.ts");
      /* harmony import */


      var _auth_http_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! ./auth-http.service */
      "./src/app/auth/store/effects/auth-http.service.ts");

      var AuthEffectService = function AuthEffectService(authHttpSrv, actions$, authSrv, router) {
        var _this5 = this;

        _classCallCheck(this, AuthEffectService);

        this.authHttpSrv = authHttpSrv;
        this.actions$ = actions$;
        this.authSrv = authSrv;
        this.router = router;
        this.signUpCustomer$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_3__["createEffect"])(function () {
          return _this5.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_3__["ofType"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_9__["actions"].SignUpInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["switchMap"])(function (action) {
            return _this5.authHttpSrv.signUpCustomer(action.payload).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (data) {
              return _actions_auth_action__WEBPACK_IMPORTED_MODULE_9__["actions"].SignUpSuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["of"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_9__["actions"].SignUpFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.login$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_3__["createEffect"])(function () {
          return _this5.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_3__["ofType"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_9__["actions"].LoginInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["switchMap"])(function (action) {
            return _this5.authHttpSrv.login(action.payload).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (data) {
              return _actions_auth_action__WEBPACK_IMPORTED_MODULE_9__["actions"].LoginSuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["tap"])(function (data) {
              return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this5, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee25() {
                var deviceToken;
                return regeneratorRuntime.wrap(function _callee25$(_context25) {
                  while (1) {
                    switch (_context25.prev = _context25.next) {
                      case 0:
                        if (!("payload" in data)) {
                          _context25.next = 7;
                          break;
                        }

                        _context25.next = 3;
                        return Object(src_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_6__["getDataFromLocalStorage"])(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_7__["LocalStorageKey"].DEVICE_KEY);

                      case 3:
                        deviceToken = _context25.sent;

                        if (deviceToken) {
                          this.authSrv.registerDeviceToken(deviceToken, data.payload.userId);
                        } //? Navigate in authorized routes


                        _context25.next = 7;
                        return this.authSrv.afterLogin(data.payload);

                      case 7:
                      case "end":
                        return _context25.stop();
                    }
                  }
                }, _callee25, this);
              }));
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["of"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_9__["actions"].LoginFailedAction({
                payload: error
              }));
            }));
          }));
        });
        /**
         * Gets run after the user signs up and an OTP is sent to them,
         * this effects validates the OTP
         */

        this.verifyOTPAfterSignUp$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_3__["createEffect"])(function () {
          return _this5.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_3__["ofType"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_9__["actions"].VerifyOTPAfterSignUpInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["switchMap"])(function (action) {
            return _this5.authHttpSrv.verifyPhoneNumberAfterSignUp(action.payload).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (data) {
              return _actions_auth_action__WEBPACK_IMPORTED_MODULE_9__["actions"].VerifyOTPAfterSignUpSuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["tap"])(function (data) {
              if (data.payload === true) {
                //? Run side-effect
                _this5.authSrv.afterSignUpOTPValidation();
              }
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["of"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_9__["actions"].VerifyOTPAfterSignUpFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.getVefiricationCodeAfterForgetPassword$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_3__["createEffect"])(function () {
          return _this5.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_3__["ofType"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_9__["actions"].PasswordRecoveryInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["switchMap"])(function (action) {
            return _this5.authHttpSrv.getVefiricationCodeAfterForgetPassword(action.payload).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (data) {
              return _actions_auth_action__WEBPACK_IMPORTED_MODULE_9__["actions"].PasswordRecoverySuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["tap"])(function (data) {
              if (data.payload === true) {}
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["of"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_9__["actions"].PasswordRecoveryFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.logout$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_3__["createEffect"])(function () {
          return _this5.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_3__["ofType"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_9__["actions"].LogoutInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function () {
            return _actions_auth_action__WEBPACK_IMPORTED_MODULE_9__["actions"].LogoutSuccessfulAction();
          }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["tap"])(function () {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this5, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee26() {
              return regeneratorRuntime.wrap(function _callee26$(_context26) {
                while (1) {
                  switch (_context26.prev = _context26.next) {
                    case 0:
                      _context26.next = 2;
                      return this.authSrv.afterLogout();

                    case 2:
                      return _context26.abrupt("return", _context26.sent);

                    case 3:
                    case "end":
                      return _context26.stop();
                  }
                }
              }, _callee26, this);
            }));
          }));
        });
        this.verifyOTP$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_3__["createEffect"])(function () {
          return _this5.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_3__["ofType"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_9__["actions"].VerifyOTPTokenInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["switchMap"])(function (action) {
            return _this5.authHttpSrv.verifyToken(action.payload).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (data) {
              return _actions_auth_action__WEBPACK_IMPORTED_MODULE_9__["actions"].VerifyOTPTokenSuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["tap"])(function (data) {
              if (data.payload === true) {
                _this5.router.navigate(["/auth", "reset-password"]);
              }
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["of"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_9__["actions"].VerifyOTPTokenFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.changePassword$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_3__["createEffect"])(function () {
          return _this5.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_3__["ofType"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_9__["actions"].ChangePasswordInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["switchMap"])(function (action) {
            return _this5.authHttpSrv.changePassword(action.payload).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (data) {
              return _actions_auth_action__WEBPACK_IMPORTED_MODULE_9__["actions"].ChangePasswordSuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["tap"])(function (data) {
              var _a;

              if ((_a = data.payload) === null || _a === void 0 ? void 0 : _a.userId) {
                _this5.router.navigate(["/auth", "login"]);
              }
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["of"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_9__["actions"].ChangePasswordFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.facebookLogin$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_3__["createEffect"])(function () {
          return _this5.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_3__["ofType"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_9__["actions"].FacebookLoginInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["switchMap"])(function (action) {
            var facebookId = action.payload.facebookId;
            return _this5.authHttpSrv.facebookLogin(action.payload).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (data) {
              return _actions_auth_action__WEBPACK_IMPORTED_MODULE_9__["actions"].FacebookLoginSuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["tap"])(function (data) {
              return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this5, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee27() {
                var _data$payload, notExist, userId, deviceToken, authPayload;

                return regeneratorRuntime.wrap(function _callee27$(_context27) {
                  while (1) {
                    switch (_context27.prev = _context27.next) {
                      case 0:
                        _data$payload = data.payload, notExist = _data$payload.notExist, userId = _data$payload.userId; //? Save the device token after successful login

                        _context27.next = 3;
                        return Object(src_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_6__["getDataFromLocalStorage"])(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_7__["LocalStorageKey"].DEVICE_KEY);

                      case 3:
                        deviceToken = _context27.sent;

                        if (deviceToken) {
                          this.authSrv.registerDeviceToken(deviceToken, userId);
                        }

                        if (!(notExist === true)) {
                          _context27.next = 9;
                          break;
                        }

                        this.router.navigate(["/auth", "update-facebook-user-detail", facebookId]);
                        _context27.next = 12;
                        break;

                      case 9:
                        authPayload = Object.assign(Object.assign({}, data.payload), {
                          notExist: undefined
                        });
                        _context27.next = 12;
                        return this.authSrv.afterLogin(authPayload);

                      case 12:
                      case "end":
                        return _context27.stop();
                    }
                  }
                }, _callee27, this);
              }));
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["of"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_9__["actions"].FacebookLoginFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.updateFacebookUserDetail$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_3__["createEffect"])(function () {
          return _this5.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_3__["ofType"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_9__["actions"].UpdateFacebookUserDetailsInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["switchMap"])(function (action) {
            return _this5.authHttpSrv.updateFacebookUserDetails(action.payload).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (data) {
              return _actions_auth_action__WEBPACK_IMPORTED_MODULE_9__["actions"].UpdateFacebookUserDetailsSuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["tap"])(function (data) {
              return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this5, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee28() {
                var authPayload;
                return regeneratorRuntime.wrap(function _callee28$(_context28) {
                  while (1) {
                    switch (_context28.prev = _context28.next) {
                      case 0:
                        authPayload = Object.assign(Object.assign({}, data.payload), {
                          notExist: undefined
                        });
                        _context28.next = 3;
                        return this.authSrv.afterLogin(authPayload);

                      case 3:
                      case "end":
                        return _context28.stop();
                    }
                  }
                }, _callee28, this);
              }));
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["of"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_9__["actions"].UpdateFacebookUserDetailsFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.saveDeviceToken$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_3__["createEffect"])(function () {
          return _this5.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_3__["ofType"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_9__["actions"].SaveDeviceTokenInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["switchMap"])(function (action) {
            var _action$payload = action.payload,
                token = _action$payload.token,
                userId = _action$payload.userId;
            return _this5.authHttpSrv.savePushNotificationToken(userId, token).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (data) {
              return _actions_auth_action__WEBPACK_IMPORTED_MODULE_9__["actions"].SaveDeviceTokenSuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["of"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_9__["actions"].SaveDeviceTokenFailedAction({
                payload: error
              }));
            }));
          }));
        });
      };

      AuthEffectService.ctorParameters = function () {
        return [{
          type: _auth_http_service__WEBPACK_IMPORTED_MODULE_10__["AuthHttpService"]
        }, {
          type: _ngrx_effects__WEBPACK_IMPORTED_MODULE_3__["Actions"]
        }, {
          type: _auth_service__WEBPACK_IMPORTED_MODULE_8__["AuthService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }];
      };

      AuthEffectService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], AuthEffectService);
      /***/
    },

    /***/
    "./src/app/auth/store/effects/auth-http.service.ts":
    /*!*********************************************************!*\
      !*** ./src/app/auth/store/effects/auth-http.service.ts ***!
      \*********************************************************/

    /*! exports provided: AuthHttpService */

    /***/
    function srcAppAuthStoreEffectsAuthHttpServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthHttpService", function () {
        return AuthHttpService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! rxjs/operators */
      "./node_modules/rxjs/_esm2015/operators/index.js");
      /* harmony import */


      var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../../../../environments/environment */
      "./src/environments/environment.ts");

      var AuthHttpService = /*#__PURE__*/function () {
        function AuthHttpService(httpClientSrv) {
          _classCallCheck(this, AuthHttpService);

          this.httpClientSrv = httpClientSrv;
        }

        _createClass(AuthHttpService, [{
          key: "signUpCustomer",
          value: function signUpCustomer(payload) {
            try {
              return this.httpClientSrv.post("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Account/SignUp"), payload).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
            } catch (ex) {
              throw ex;
            }
          }
        }, {
          key: "verifyPhoneNumberAfterSignUp",
          value: function verifyPhoneNumberAfterSignUp(payload) {
            try {
              var PhoneNumber = payload.PhoneNumber,
                  token = payload.token;
              return this.httpClientSrv.post("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Account/SignUpOTPVerifyToken?PhoneNumber=").concat(PhoneNumber, "&token=").concat(token), {}).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
            } catch (ex) {
              throw ex;
            }
          }
        }, {
          key: "login",
          value: function login(payload) {
            try {
              return this.httpClientSrv.post("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Account/Login"), payload).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
            } catch (ex) {
              throw ex;
            }
          }
        }, {
          key: "getVefiricationCodeAfterForgetPassword",
          value: function getVefiricationCodeAfterForgetPassword(phoneNumber) {
            try {
              return this.httpClientSrv.post("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Account/OTPVerificationRequestWithoutUsername"), {
                phoneNumber: phoneNumber
              }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
            } catch (ex) {
              throw ex;
            }
          }
        }, {
          key: "verifyToken",
          value: function verifyToken(payload) {
            var PhoneNumber = payload.PhoneNumber,
                token = payload.token;
            return this.httpClientSrv.post("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Account/OTPVerifyToken?PhoneNumber=").concat(PhoneNumber, "&token=").concat(token), {}).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
          }
        }, {
          key: "changePassword",
          value: function changePassword(payload) {
            try {
              var PhoneNo = payload.PhoneNo,
                  Password = payload.Password;
              return this.httpClientSrv.post("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Account/changeUserPassword?PhoneNo=").concat(PhoneNo, "&Password=").concat(Password), {}).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
            } catch (ex) {
              throw ex;
            }
          }
        }, {
          key: "facebookLogin",
          value: function facebookLogin(payload) {
            try {
              return this.httpClientSrv.post("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Account/FacebookLogin"), payload).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
            } catch (ex) {
              throw ex;
            }
          }
        }, {
          key: "updateFacebookUserDetails",
          value: function updateFacebookUserDetails(payload) {
            try {
              return this.httpClientSrv.post("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Account/UpdateFacebookUser"), payload).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
            } catch (ex) {
              throw ex;
            }
          }
        }, {
          key: "savePushNotificationToken",
          value: function savePushNotificationToken(userId, token) {
            try {
              return this.httpClientSrv.post("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Shopping/SavePushNotificationToken?UserId=").concat(userId, "&Token=").concat(token), {}).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
            } catch (ex) {
              throw ex;
            }
          }
        }]);

        return AuthHttpService;
      }();

      AuthHttpService.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
        }];
      };

      AuthHttpService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], AuthHttpService);
      /***/
    },

    /***/
    "./src/app/auth/store/model/auth.model.ts":
    /*!************************************************!*\
      !*** ./src/app/auth/store/model/auth.model.ts ***!
      \************************************************/

    /*! exports provided: InitialAuthState */

    /***/
    function srcAppAuthStoreModelAuthModelTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "InitialAuthState", function () {
        return InitialAuthState;
      });

      var InitialAuthState = {
        ActiveError: undefined,
        ActiveMessage: undefined,
        IsLoading: false,
        AuthenticatedUser: undefined,
        UserOTP: undefined,
        PhoneNumber: undefined
      };
      /***/
    },

    /***/
    "./src/app/auth/store/reducer/auth.reducer.ts":
    /*!****************************************************!*\
      !*** ./src/app/auth/store/reducer/auth.reducer.ts ***!
      \****************************************************/

    /*! exports provided: AuthReducer */

    /***/
    function srcAppAuthStoreReducerAuthReducerTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthReducer", function () {
        return AuthReducer;
      });
      /* harmony import */


      var _ngrx_store__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @ngrx/store */
      "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");
      /* harmony import */


      var _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ../../../../utils/types/app.constant */
      "./src/utils/types/app.constant.ts");
      /* harmony import */


      var _actions_auth_action__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../actions/auth.action */
      "./src/app/auth/store/actions/auth.action.ts");
      /* harmony import */


      var _model_auth_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../model/auth.model */
      "./src/app/auth/store/model/auth.model.ts");

      var AuthReducer = Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createReducer"])(_model_auth_model__WEBPACK_IMPORTED_MODULE_3__["InitialAuthState"], Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_2__["actions"].ClearActiveErrorAction, function (state) {
        return Object.assign(Object.assign({}, state), {
          ActiveError: undefined,
          ActiveMessage: undefined,
          IsLoading: false
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_2__["actions"].ClearActiveMessageAction, function (state) {
        return Object.assign(Object.assign({}, state), {
          ActiveMessage: undefined,
          ActiveError: undefined,
          IsLoading: false
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_2__["actions"].SignUpInitiatedAction, function (state) {
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_2__["actions"].SignUpFailedAction, function (state, _ref3) {
        var payload = _ref3.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_2__["actions"].SignUpSuccessfulAction, function (state, _ref4) {
        var payload = _ref4.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveMessage: "".concat(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_1__["APIMessage"].CREATED, " successfully. Account needs to be verified")
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_2__["actions"].VerifyOTPAfterSignUpInitiatedAction, function (state, _ref5) {
        var payload = _ref5.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_2__["actions"].VerifyOTPAfterSignUpFailedAction, function (state, _ref6) {
        var payload = _ref6.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_2__["actions"].VerifyOTPAfterSignUpSuccessfulAction, function (state, _ref7) {
        var payload = _ref7.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveMessage: payload === true ? _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_1__["APIMessage"].SUCCESSFUL : _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_1__["APIMessage"].BAD_OPERATION
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_2__["actions"].LoginInitiatedAction, function (state, _ref8) {
        var payload = _ref8.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_2__["actions"].LoginFailedAction, function (state, _ref9) {
        var payload = _ref9.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_2__["actions"].LoginSuccessfulAction, function (state, _ref10) {
        var payload = _ref10.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          AuthenticatedUser: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_2__["actions"].LogoutInitiatedAction, function (state) {
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_2__["actions"].LogoutSuccessfulAction, function (state) {
        return Object.assign({}, _model_auth_model__WEBPACK_IMPORTED_MODULE_3__["InitialAuthState"]);
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_2__["actions"].VerifyOTPTokenInitiatedAction, function (state, _ref11) {
        var payload = _ref11.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: true,
          UserOTP: payload.token
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_2__["actions"].VerifyOTPTokenFailedAction, function (state, _ref12) {
        var payload = _ref12.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_2__["actions"].VerifyOTPTokenSuccessfulAction, function (state, _ref13) {
        var payload = _ref13.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveMessage: payload ? _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_1__["APIMessage"].VALIDATION_SUCCESSFUL : undefined
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_2__["actions"].PasswordRecoveryInitiatedAction, function (state, _ref14) {
        var payload = _ref14.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_2__["actions"].PasswordRecoveryFailedAction, function (state, _ref15) {
        var payload = _ref15.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_2__["actions"].PasswordRecoverySuccessfulAction, function (state, _ref16) {
        var payload = _ref16.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveMessage: payload === true ? _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_1__["APIMessage"].CREATED : undefined
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_2__["actions"].ChangePasswordInitiatedAction, function (state, _ref17) {
        var payload = _ref17.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_2__["actions"].ChangePasswordFailedAction, function (state, _ref18) {
        var payload = _ref18.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_2__["actions"].ChangePasswordSuccessfulAction, function (state, _ref19) {
        var payload = _ref19.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveMessage: _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_1__["APIMessage"].UPDATE
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_2__["actions"].FacebookLoginInitiatedAction, function (state, _ref20) {
        var payload = _ref20.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_2__["actions"].FacebookLoginFailedAction, function (state, _ref21) {
        var payload = _ref21.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_2__["actions"].FacebookLoginSuccessfulAction, function (state, _ref22) {
        var payload = _ref22.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveMessage: payload.notExist === true ? _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_1__["APIMessage"].CREATED : undefined
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_2__["actions"].UpdateFacebookUserDetailsInitiatedAction, function (state, _ref23) {
        var payload = _ref23.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_2__["actions"].UpdateFacebookUserDetailsFailedAction, function (state, _ref24) {
        var payload = _ref24.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_2__["actions"].UpdateFacebookUserDetailsSuccessfulAction, function (state, _ref25) {
        var payload = _ref25.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveMessage: _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_1__["APIMessage"].UPDATE
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_2__["actions"].SaveDeviceTokenInitiatedAction, function (state, _ref26) {
        var payload = _ref26.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_2__["actions"].SaveDeviceTokenFailedAction, function (state, _ref27) {
        var payload = _ref27.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_auth_action__WEBPACK_IMPORTED_MODULE_2__["actions"].SaveDeviceTokenSuccessfulAction, function (state, _ref28) {
        var payload = _ref28.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false
        });
      }));
      /***/
    },

    /***/
    "./src/app/resolvers/cart-location.resolver.ts":
    /*!*****************************************************!*\
      !*** ./src/app/resolvers/cart-location.resolver.ts ***!
      \*****************************************************/

    /*! exports provided: CartLocationResolver */

    /***/
    function srcAppResolversCartLocationResolverTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CartLocationResolver", function () {
        return CartLocationResolver;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ngrx_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ngrx/store */
      "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");
      /* harmony import */


      var _rider_store_effects_rider_http_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../rider/store/effects/rider-http.service */
      "./src/app/rider/store/effects/rider-http.service.ts");
      /* harmony import */


      var _user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../user/store/actions/user.action */
      "./src/app/user/store/actions/user.action.ts");

      var CartLocationResolver = /*#__PURE__*/function () {
        function CartLocationResolver(riderHttpSrv, store) {
          _classCallCheck(this, CartLocationResolver);

          this.riderHttpSrv = riderHttpSrv;
          this.store = store;
        }

        _createClass(CartLocationResolver, [{
          key: "resolve",
          value: function resolve(route, state) {
            var _a;

            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee29() {
              var cartId, selectedCart, geoTag;
              return regeneratorRuntime.wrap(function _callee29$(_context29) {
                while (1) {
                  switch (_context29.prev = _context29.next) {
                    case 0:
                      cartId = (_a = route.paramMap.get("cartId")) !== null && _a !== void 0 ? _a : route.queryParamMap.get("cartId"); // ? Visually add a loader

                      this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_4__["actions"].StartLoaderRemotelyAction());
                      _context29.next = 4;
                      return this.riderHttpSrv.getSelectedCart(cartId).toPromise();

                    case 4:
                      selectedCart = _context29.sent;

                      if (!(selectedCart === null || selectedCart === void 0 ? void 0 : selectedCart.id)) {
                        _context29.next = 11;
                        break;
                      }

                      _context29.next = 8;
                      return this.riderHttpSrv.reverseGeocodingForAddress(selectedCart.deliveryAddress).toPromise();

                    case 8:
                      geoTag = _context29.sent;
                      this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_4__["actions"].EndLoaderRemotelyAction());
                      return _context29.abrupt("return", {
                        cart: selectedCart,
                        locationMarker: geoTag
                      });

                    case 11:
                    case "end":
                      return _context29.stop();
                  }
                }
              }, _callee29, this);
            }));
          }
        }]);

        return CartLocationResolver;
      }();

      CartLocationResolver.ctorParameters = function () {
        return [{
          type: _rider_store_effects_rider_http_service__WEBPACK_IMPORTED_MODULE_3__["RiderHttpService"]
        }, {
          type: _ngrx_store__WEBPACK_IMPORTED_MODULE_2__["Store"]
        }];
      };

      CartLocationResolver = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: "root"
      })], CartLocationResolver);
      /***/
    },

    /***/
    "./src/app/rider/rider-routing.module.ts":
    /*!***********************************************!*\
      !*** ./src/app/rider/rider-routing.module.ts ***!
      \***********************************************/

    /*! exports provided: RiderRoutingModule */

    /***/
    function srcAppRiderRiderRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RiderRoutingModule", function () {
        return RiderRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _resolvers_cart_location_resolver__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../resolvers/cart-location.resolver */
      "./src/app/resolvers/cart-location.resolver.ts");

      var routes = [{
        path: "",
        children: [{
          path: "",
          pathMatch: "full",
          redirectTo: "home"
        }, {
          path: "home",
          loadChildren: function loadChildren() {
            return __webpack_require__.e(
            /*! import() | home-home-module */
            "home-home-module").then(__webpack_require__.bind(null,
            /*! ./home/home.module */
            "./src/app/rider/home/home.module.ts")).then(function (m) {
              return m.HomePageModule;
            });
          }
        }, {
          path: "statistics",
          loadChildren: function loadChildren() {
            return __webpack_require__.e(
            /*! import() | statistics-statistics-module */
            "statistics-statistics-module").then(__webpack_require__.bind(null,
            /*! ./statistics/statistics.module */
            "./src/app/rider/statistics/statistics.module.ts")).then(function (m) {
              return m.StatisticsPageModule;
            });
          }
        }, {
          path: "orders",
          loadChildren: function loadChildren() {
            return __webpack_require__.e(
            /*! import() | orders-orders-module */
            "orders-orders-module").then(__webpack_require__.bind(null,
            /*! ./orders/orders.module */
            "./src/app/rider/orders/orders.module.ts")).then(function (m) {
              return m.OrdersPageModule;
            });
          }
        }, {
          path: "order-info/:cartId",
          loadChildren: function loadChildren() {
            return Promise.all(
            /*! import() | order-info-order-info-module */
            [__webpack_require__.e("default~order-info-order-info-module~orders-order-info-order-info-module"), __webpack_require__.e("common"), __webpack_require__.e("order-info-order-info-module")]).then(__webpack_require__.bind(null,
            /*! ./order-info/order-info.module */
            "./src/app/rider/order-info/order-info.module.ts")).then(function (m) {
              return m.OrderInfoPageModule;
            });
          }
        }, {
          path: "new-order-detail/:cartId",
          resolve: {
            cartLocation: _resolvers_cart_location_resolver__WEBPACK_IMPORTED_MODULE_3__["CartLocationResolver"]
          },
          loadChildren: function loadChildren() {
            return __webpack_require__.e(
            /*! import() | new-order-detail-new-order-detail-module */
            "new-order-detail-new-order-detail-module").then(__webpack_require__.bind(null,
            /*! ./new-order-detail/new-order-detail.module */
            "./src/app/rider/new-order-detail/new-order-detail.module.ts")).then(function (m) {
              return m.NewOrderDetailPageModule;
            });
          }
        }, {
          path: "accept-manual-payment/:cartId",
          loadChildren: function loadChildren() {
            return __webpack_require__.e(
            /*! import() | accept-manual-payment-accept-manual-payment-module */
            "accept-manual-payment-accept-manual-payment-module").then(__webpack_require__.bind(null,
            /*! ./accept-manual-payment/accept-manual-payment.module */
            "./src/app/rider/accept-manual-payment/accept-manual-payment.module.ts")).then(function (m) {
              return m.AcceptManualPaymentPageModule;
            });
          }
        }, {
          path: "view-cart-detail/:cartId",
          loadChildren: function loadChildren() {
            return Promise.all(
            /*! import() | view-cart-detail-view-cart-detail-module */
            [__webpack_require__.e("common"), __webpack_require__.e("view-cart-detail-view-cart-detail-module")]).then(__webpack_require__.bind(null,
            /*! ./view-cart-detail/view-cart-detail.module */
            "./src/app/rider/view-cart-detail/view-cart-detail.module.ts")).then(function (m) {
              return m.ViewCartDetailPageModule;
            });
          }
        }]
      }];

      var RiderRoutingModule = function RiderRoutingModule() {
        _classCallCheck(this, RiderRoutingModule);
      };

      RiderRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], RiderRoutingModule);
      /***/
    },

    /***/
    "./src/app/rider/rider.module.ts":
    /*!***************************************!*\
      !*** ./src/app/rider/rider.module.ts ***!
      \***************************************/

    /*! exports provided: RiderModule */

    /***/
    function srcAppRiderRiderModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RiderModule", function () {
        return RiderModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _ngrx_effects__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ngrx/effects */
      "./node_modules/@ngrx/effects/__ivy_ngcc__/fesm2015/ngrx-effects.js");
      /* harmony import */


      var _ngrx_store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ngrx/store */
      "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");
      /* harmony import */


      var _shared_shared_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../shared/shared.module */
      "./src/app/shared/shared.module.ts");
      /* harmony import */


      var _rider_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./rider-routing.module */
      "./src/app/rider/rider-routing.module.ts");
      /* harmony import */


      var _store_effects_rider_effect_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./store/effects/rider-effect.service */
      "./src/app/rider/store/effects/rider-effect.service.ts");
      /* harmony import */


      var _store_reducer_rider_reducer__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ./store/reducer/rider.reducer */
      "./src/app/rider/store/reducer/rider.reducer.ts");

      var RiderModule = function RiderModule() {
        _classCallCheck(this, RiderModule);
      };

      RiderModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [],
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_5__["SharedModule"], _rider_routing_module__WEBPACK_IMPORTED_MODULE_6__["RiderRoutingModule"], _ngrx_effects__WEBPACK_IMPORTED_MODULE_3__["EffectsModule"].forFeature([_store_effects_rider_effect_service__WEBPACK_IMPORTED_MODULE_7__["RiderEffectService"]]), _ngrx_store__WEBPACK_IMPORTED_MODULE_4__["StoreModule"].forFeature("Rider", _store_reducer_rider_reducer__WEBPACK_IMPORTED_MODULE_8__["RiderReducer"])],
        exports: [_rider_routing_module__WEBPACK_IMPORTED_MODULE_6__["RiderRoutingModule"]]
      })], RiderModule);
      /***/
    },

    /***/
    "./src/app/rider/store/actions/rider.action.ts":
    /*!*****************************************************!*\
      !*** ./src/app/rider/store/actions/rider.action.ts ***!
      \*****************************************************/

    /*! exports provided: RiderActionType, actions */

    /***/
    function srcAppRiderStoreActionsRiderActionTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RiderActionType", function () {
        return RiderActionType;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "actions", function () {
        return actions;
      });
      /* harmony import */


      var _ngrx_store__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @ngrx/store */
      "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");

      var RiderActionType;

      (function (RiderActionType) {
        RiderActionType["CLEAR_ACTIVE_ERROR"] = "[ERROR_RIDER] CLEAR_ACTIVE_ERROR";
        RiderActionType["CLEAR_ACTIVE_MESSAGE"] = "[ERROR_RIDER] CLEAR_ACTIVE_MESSAGE";
        RiderActionType["CLEAR_ACTIVE_ACTION_MESSAGE"] = "[ERROR_RIDER] CLEAR_ACTIVE_ACTION_MESSAGE";
        RiderActionType["GET_RIDER_ACTIVITY_HISTORY_INITIATED"] = "[RIDER] GET_RIDER_ACTIVITY_HISTORY_INITIATED";
        RiderActionType["GET_RIDER_ACTIVITY_HISTORY_SUCCESSFUL"] = "[RIDER] GET_RIDER_ACTIVITY_HISTORY_SUCCESSFUL";
        RiderActionType["GET_RIDER_ACTIVITY_HISTORY_FAILED"] = "[RIDER] GET_RIDER_ACTIVITY_HISTORY_FAILED";
        RiderActionType["GET_RIDER_RATING_INITIATED"] = "[RIDER_RATING] GET_RIDER_RATING_INITIATED";
        RiderActionType["GET_RIDER_RATING_FAILED"] = "[RIDER_RATING] GET_RIDER_RATING_FAILED";
        RiderActionType["GET_RIDER_RATING_SUCCESSFUL"] = "[RIDER_RATING] GET_RIDER_RATING_SUCCESSFUL";
        RiderActionType["SAVE_RIDER_GEOLOCATION_INITIATED"] = "[REALTIME_TRACKING] SAVE_RIDER_GEOLOCATION_INITIATED";
        RiderActionType["SAVE_RIDER_GEOLOCATION_FAILED"] = "[REALTIME_TRACVKING] SAVE_RIDER_GEOLOCATION_FAILED";
        RiderActionType["SAVE_RIDER_GEOLOCATION_SUCCESSFUL"] = " [REALTIME_TRACKING] SAVE_RIDER_GEOLOCATION_SUCCESSFUL";
        RiderActionType["GET_RIDER_GEOLOCATION_HISTORY_FOR_DELIVERY_INITIATED"] = "[REALTIME TRACKING] GET_RIDER_GEOLOCATION_HISTORY_FOR_DELIVERY_INITIATED";
        RiderActionType["GET_RIDER_GEOLOCATION_HISTORY_FOR_DELIVERY_FAILED"] = "[REALTIME_TRACKING] GET_RIDER_GEOLOCATION_HISTORY_FOR_DELIVERY_FAILED";
        RiderActionType["GET_RIDER_GEOLOCATION_HISTORY_FOR_DELIVERY_SUCCESFUL"] = "[REALTIME_TRACKING] GET_RIDER_GEOLOCATION_HISTORY_FOR_DELIVERY_SUCCESSFUL";
        RiderActionType["GET_SELECTED_CART_INITIATED"] = "[ORDERS] GET_SELECTED_CART_INITIATED";
        RiderActionType["GET_SELECTED_CART_FAILED"] = "[ORDERS] GET_SELECTED_CART_FAILED";
        RiderActionType["GET_SELECTED_CART_SUCCESSFUL"] = "[ORDERS] GET_SELECTED_CART_SUCCESSFUL";
        RiderActionType["REVERSE_GEOCODING_FOR_ADDRESS_INITIATED"] = "[LOCATION_TRACKING] REVERSE_GEOCODING_FOR_ADDRESS_INITIATED";
        RiderActionType["REVERSE_GEOCODING_FOR_ADDRESS_FAILED"] = "[LOCATION_TRACKING] REVERSE_GEOCODING_FOR_ADDRESS_FAILED";
        RiderActionType["REVERSE_GEOCODING_FOR_ADDRESS_SUCCESSFUL"] = "[LOCATION_TRACKING] REVERSE_GEOCODING_FOR_ADDRESS_SUCCESSFUL";
        RiderActionType["ACCEPT_CART_DELIVERY_INITIATED"] = "[RIDER_DELIVERY] ACCEPT_CART_DELIVERY_INITIATED";
        RiderActionType["ACCEPT_CART_DELIVERY_FAILED"] = "[RIDER_DELIVERY] ACCEPT_CART_DELIVERY_FAILED";
        RiderActionType["ACCEPT_CART_DELIVERY_SUCCESSFUL"] = "[RIDER_DELIVERY] ACCEPT_CART_DELIVERY_SUCCESSFUL";
        RiderActionType["CANCEL_CART_DELIVERY_INITIATED"] = "[RIDER_DELIVERY] CANCEL_CART_DELIVERY_INITIATED";
        RiderActionType["CANCEL_CART_DELIVERY_FAILED"] = "[RIDER_DELIVERY] CANCEL_CART_DELIVERY_FAILED";
        RiderActionType["CANCEL_CART_DELIVERY_SUCCESSFUL"] = "[RIDER_DELIVERY] CANCEL_CART_DELIVERY_SUCCESSFUL";
        RiderActionType["GET_MERCHANT_PROXIMITY_TO_RIDER_INITIATED"] = "[RIDER_DELIVERY] GET_MERCHANT_PROXIMITY_TO_RIDER_INITIATED";
        RiderActionType["GET_MERCHANT_PROXIMITY_TO_RIDER_FAILED"] = "[RIDER_DELIVERY] GET_MERCHANT_PROXIMITY_TO_RIDER_FAILED";
        RiderActionType["GET_MERCHANT_PROXIMITY_TO_RIDER_SUCCESSFUL"] = "[RIDER_DELIVERY] GET_MERCHANT_PROXIMITY_TO_RIDER_SUCCESSFUL";
        RiderActionType["GET_ORDER_INFO_INITIATED"] = "[DELIVERY_ORDER] GET_ORDER_INFO_INITIATED";
        RiderActionType["GET_ORDER_INFO_FAILED"] = "[DELIVERY_ORDER] GET_ORDER_INFO_FAILED";
        RiderActionType["GET_ORDER_INFO_SUCCESSFUL"] = "[DELIVERY_ORDER] GET_ORDER_INFO_SUCCESSFUL";
        RiderActionType["GET_MERCHANTS_RELATED_TO_CART_INITIATED"] = "[DELIVERY_ORDER] GET_MERCHANTS_RELATED_TO_CART_INITIATED";
        RiderActionType["GET_MERCHANTS_RELATED_TO_CART_FAILED"] = "[DELIVERY_ORDER] GET_MERCHANTS_RELATED_TO_CART_FAILED";
        RiderActionType["GET_MERCHANTS_RELATED_TO_CART_SUCCESSFUL"] = "[DELIVERY_ORDER] GET_MERCHANTS_RELATED_TO_CART_SUCCESSFUL";
        RiderActionType["GET_RIDER_DELIVERY_GRAPH_SUMMARY_INITIATED"] = "[GRAPHICAL_SUMMARY] GET_RIDER_DELIVERY_GRAPH_SUMMARY_INITIATED";
        RiderActionType["GET_RIDER_DELIVERY_GRAPH_SUMMARY_FAILED"] = "[GRAPHICAL_SUMMARY] GET_RIDER_DELIVERY_GRAPH_SUMMARY_FAILED";
        RiderActionType["GET_RIDER_DELIVERY_GRAPH_SUMMARY_SUCCESSFUL"] = "[GRAPHICAL_SUMMARY] GET_RIDER_DELIVERY_GRAPH_SUMMARY_SUCCESSFUL";
        RiderActionType["MARK_DELIVERY_AS_COMPLETE_INITIATED"] = "[RIDER_DELIVERY] MARK_DELIVERY_AS_COMPLETE_INITIATED";
        RiderActionType["MARK_DELIVERY_AS_COMPLETE_FAILED"] = "[RIDER_DELIVERY] MARK_DELIVERY_AS_COMPLETE_FAILED";
        RiderActionType["MARK_DELIVERY_AS_COMPLETE_SUCCESSFUL"] = "[RIDER_DELIVERY] MARK_DELIVERY_AS_COMPLETE_SUCCESSFUL";
        RiderActionType["CONFIRM_MANUAL_PAYMENT_INITIATED"] = "[RIDER_DELIVERY] CONFIRM_MANUAL_PAYMENT_INITIATED";
        RiderActionType["CONFIRM_MANUAL_PAYMENT_FAILED"] = "[RIDER_DELIVERY] CONFIRM_MANUAL_PAYMENT_FAILED";
        RiderActionType["CONFIRM_MANUAL_PAYMENT_SUCCESSFUL"] = "[RIDER_DELIVERY] CONFIRM_MANUAL_PAYMENT_SUCCESSFUL";
        RiderActionType["GET_CART_CHECKLIST_INITIATED"] = "[RIDER_DELIVERY] GET_CART_CHECKLIST_INITIATED";
        RiderActionType["GET_CART_CHECKLIST_FAILED"] = "[RIDER_DELIVERY] GET_CART_CHECKLIST_FAILED";
        RiderActionType["GET_CART_CHECKLIST_SUCCESSFUL"] = "[RIDER_DELIVERY] GET_CART_CHECKLIST_SUCCESSFUL";
        RiderActionType["CLEAR_RIDER_STATE"] = "[AUTH] CLEAR_RIDER_STATE";
        RiderActionType["NEW_CART_ASSIGNED_TO_RIDER"] = "[RIDER] NEW_CART_ASSIGNED_TO_RIDER";
      })(RiderActionType || (RiderActionType = {}));

      var actions = {
        ClearActiveErrorAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.CLEAR_ACTIVE_ERROR),
        ClearActiveMessageAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.CLEAR_ACTIVE_MESSAGE),
        ClearActiveActionMessageAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.CLEAR_ACTIVE_ACTION_MESSAGE),
        ClearRiderStateAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.CLEAR_RIDER_STATE),
        GetRiderActivityHistoryInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.GET_RIDER_ACTIVITY_HISTORY_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetRiderActivityHistoryFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.GET_RIDER_ACTIVITY_HISTORY_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetRiderActivityHistorySuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.GET_RIDER_ACTIVITY_HISTORY_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        NewCartAssignedToRiderAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.NEW_CART_ASSIGNED_TO_RIDER, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetRiderRatingInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.GET_RIDER_RATING_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetRiderRatingFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.GET_RIDER_RATING_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetRiderRatingSuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.GET_RIDER_RATING_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        SaveRiderGeolocationInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.SAVE_RIDER_GEOLOCATION_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        SaveRiderGeolocationFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.SAVE_RIDER_GEOLOCATION_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        SaveRiderGeolocationSuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.SAVE_RIDER_GEOLOCATION_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetRiderGeolocationHistoryForDeliveryInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.GET_RIDER_GEOLOCATION_HISTORY_FOR_DELIVERY_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetRiderGeolocationHistoryForDeliveryFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.GET_RIDER_GEOLOCATION_HISTORY_FOR_DELIVERY_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetRiderGeolocationHistoryForDeliverySuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.GET_RIDER_GEOLOCATION_HISTORY_FOR_DELIVERY_SUCCESFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetSelectedCartInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.GET_SELECTED_CART_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetSelectedCartFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.GET_SELECTED_CART_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetSelectedCartSuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.GET_SELECTED_CART_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        ReverseGeocodingForAddressInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.REVERSE_GEOCODING_FOR_ADDRESS_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        ReverseGeocodingForAddressFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.REVERSE_GEOCODING_FOR_ADDRESS_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        ReverseGeocodingForAddressSuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.REVERSE_GEOCODING_FOR_ADDRESS_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        AcceptCartDeliveryInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.ACCEPT_CART_DELIVERY_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        AcceptCartDeliveryFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.ACCEPT_CART_DELIVERY_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        AcceptCartDeliverySuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.ACCEPT_CART_DELIVERY_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        CancelCartDeliveryInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.CANCEL_CART_DELIVERY_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        CancelCartDeliveryFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.CANCEL_CART_DELIVERY_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        CancelCartDeliverySuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.CANCEL_CART_DELIVERY_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetMerchantProximityToRiderInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.GET_MERCHANT_PROXIMITY_TO_RIDER_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetMerchantProximityToRiderFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.GET_MERCHANT_PROXIMITY_TO_RIDER_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetMerchantProximityToRiderSuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.GET_MERCHANT_PROXIMITY_TO_RIDER_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetOrderInfoInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.GET_ORDER_INFO_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetOrderInfoFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.GET_ORDER_INFO_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetOrderInfoSuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.GET_ORDER_INFO_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetMerchantsRelatedToCartInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.GET_MERCHANTS_RELATED_TO_CART_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetMerchantsRelatedToCartFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.GET_MERCHANTS_RELATED_TO_CART_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetMerchantsRelatedToCartSuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.GET_MERCHANTS_RELATED_TO_CART_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetRiderDeliveryGraphSummaryInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.GET_RIDER_DELIVERY_GRAPH_SUMMARY_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetRiderDeliveryGraphSummaryFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.GET_RIDER_DELIVERY_GRAPH_SUMMARY_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetRiderDeliveryGraphSummarySuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.GET_RIDER_DELIVERY_GRAPH_SUMMARY_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        MarkDeliveryAsCompleteInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.MARK_DELIVERY_AS_COMPLETE_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        MarkDeliveryAsCompleteFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.MARK_DELIVERY_AS_COMPLETE_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        MarkDeliveryAsCompleteSuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.MARK_DELIVERY_AS_COMPLETE_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        ConfirmManualPaymentInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.CONFIRM_MANUAL_PAYMENT_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        ConfirmManualPaymentFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.CONFIRM_MANUAL_PAYMENT_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        ConfirmManualPaymentSuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.CONFIRM_MANUAL_PAYMENT_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetCartCheckListInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.GET_CART_CHECKLIST_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetCartCheckListFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.GET_CART_CHECKLIST_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetCartCheckListSuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(RiderActionType.GET_CART_CHECKLIST_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])())
      };
      /***/
    },

    /***/
    "./src/app/rider/store/effects/rider-effect.service.ts":
    /*!*************************************************************!*\
      !*** ./src/app/rider/store/effects/rider-effect.service.ts ***!
      \*************************************************************/

    /*! exports provided: RiderEffectService */

    /***/
    function srcAppRiderStoreEffectsRiderEffectServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RiderEffectService", function () {
        return RiderEffectService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ngrx_effects__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ngrx/effects */
      "./node_modules/@ngrx/effects/__ivy_ngcc__/fesm2015/ngrx-effects.js");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! rxjs */
      "./node_modules/rxjs/_esm2015/index.js");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! rxjs/operators */
      "./node_modules/rxjs/_esm2015/operators/index.js");
      /* harmony import */


      var _actions_rider_action__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../actions/rider.action */
      "./src/app/rider/store/actions/rider.action.ts");
      /* harmony import */


      var _rider_http_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./rider-http.service */
      "./src/app/rider/store/effects/rider-http.service.ts");

      var RiderEffectService = function RiderEffectService(riderHttpSrv, actions$) {
        var _this6 = this;

        _classCallCheck(this, RiderEffectService);

        this.riderHttpSrv = riderHttpSrv;
        this.actions$ = actions$;
        this.getRiderDeliveryHistory$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["createEffect"])(function () {
          return _this6.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetRiderActivityHistoryInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (action) {
            return _this6.riderHttpSrv.getRiderDeliveryHistory(action.payload.userId).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) {
              return _actions_rider_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetRiderActivityHistorySuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetRiderActivityHistoryFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.getRiderRating$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["createEffect"])(function () {
          return _this6.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetRiderRatingInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (action) {
            return _this6.riderHttpSrv.getRiderRating(action.payload.userId).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) {
              return _actions_rider_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetRiderRatingSuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetRiderRatingFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.saveRiderCurrentGeolocation$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["createEffect"])(function () {
          return _this6.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_5__["actions"].SaveRiderGeolocationInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (action) {
            var _action$payload2 = action.payload,
                userId = _action$payload2.userId,
                longitude = _action$payload2.longitude,
                latitude = _action$payload2.latitude;
            return _this6.riderHttpSrv.saveRiderCurrentGeolocation(userId, longitude, latitude).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) {
              return _actions_rider_action__WEBPACK_IMPORTED_MODULE_5__["actions"].SaveRiderGeolocationSuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_5__["actions"].SaveRiderGeolocationFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.getRiderDeliveryGeoPoints$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["createEffect"])(function () {
          return _this6.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetRiderGeolocationHistoryForDeliveryInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (action) {
            return _this6.riderHttpSrv.getRiderLocationTrackingForCartDelivery(action.payload.cartId).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) {
              return _actions_rider_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetRiderGeolocationHistoryForDeliverySuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetRiderGeolocationHistoryForDeliveryFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.getSelectedCartById$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["createEffect"])(function () {
          return _this6.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetSelectedCartInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (action) {
            return _this6.riderHttpSrv.getSelectedCart(action.payload.cartId).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) {
              return _actions_rider_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetSelectedCartSuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetSelectedCartFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.getReverseGeocodingForAddress$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["createEffect"])(function () {
          return _this6.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_5__["actions"].ReverseGeocodingForAddressInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (action) {
            return _this6.riderHttpSrv.reverseGeocodingForAddress(action.payload.address).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) {
              return _actions_rider_action__WEBPACK_IMPORTED_MODULE_5__["actions"].ReverseGeocodingForAddressSuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_5__["actions"].ReverseGeocodingForAddressFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.acceptCartDelivery$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["createEffect"])(function () {
          return _this6.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_5__["actions"].AcceptCartDeliveryInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (action) {
            var _action$payload3 = action.payload,
                cartId = _action$payload3.cartId,
                userId = _action$payload3.userId;
            return _this6.riderHttpSrv.startRiderDelivery(cartId, userId).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) {
              return _actions_rider_action__WEBPACK_IMPORTED_MODULE_5__["actions"].AcceptCartDeliverySuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_5__["actions"].AcceptCartDeliveryFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.cancelCartDelivery$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["createEffect"])(function () {
          return _this6.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_5__["actions"].CancelCartDeliveryInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (action) {
            var _action$payload4 = action.payload,
                cartId = _action$payload4.cartId,
                userId = _action$payload4.userId;
            return _this6.riderHttpSrv.cancelRiderDelivery(cartId, userId).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) {
              return _actions_rider_action__WEBPACK_IMPORTED_MODULE_5__["actions"].CancelCartDeliverySuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_5__["actions"].CancelCartDeliveryFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.getMerchantInProximityToRider$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["createEffect"])(function () {
          return _this6.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetMerchantProximityToRiderInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (action) {
            var _action$payload5 = action.payload,
                cartId = _action$payload5.cartId,
                longitude = _action$payload5.longitude,
                latitude = _action$payload5.latitude;
            return _this6.riderHttpSrv.getMerchantInProximityToRider(cartId, latitude, longitude).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) {
              return _actions_rider_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetMerchantProximityToRiderSuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetMerchantProximityToRiderFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.getOrderInfo$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["createEffect"])(function () {
          return _this6.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetOrderInfoInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (action) {
            return _this6.riderHttpSrv.getOrderInfo(action.payload.cartId).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) {
              return _actions_rider_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetOrderInfoSuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetOrderInfoFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.getMerchantsRelatedToCart$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["createEffect"])(function () {
          return _this6.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetMerchantsRelatedToCartInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (action) {
            return _this6.riderHttpSrv.getMerchantsRelatedToCart(action.payload.cartId).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) {
              return _actions_rider_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetMerchantsRelatedToCartSuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetMerchantsRelatedToCartFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.getRiderDeliveryGraphSummary$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["createEffect"])(function () {
          return _this6.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetRiderDeliveryGraphSummaryInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (action) {
            return _this6.riderHttpSrv.getRiderDeliveryGraphSummary(action.payload.riderId).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) {
              return _actions_rider_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetRiderDeliveryGraphSummarySuccessfulAction({
                payload: _toConsumableArray(data)
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetRiderDeliveryGraphSummaryFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.markDeliveryAsComplete$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["createEffect"])(function () {
          return _this6.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_5__["actions"].MarkDeliveryAsCompleteInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (action) {
            return _this6.riderHttpSrv.markDeliveryAsComplete(action.payload.cartId).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) {
              return _actions_rider_action__WEBPACK_IMPORTED_MODULE_5__["actions"].MarkDeliveryAsCompleteSuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_5__["actions"].MarkDeliveryAsCompleteFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.confirmManualPayment$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["createEffect"])(function () {
          return _this6.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_5__["actions"].ConfirmManualPaymentInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (action) {
            var _action$payload6 = action.payload,
                cartId = _action$payload6.cartId,
                riderId = _action$payload6.riderId,
                paymentOption = _action$payload6.paymentOption;
            return _this6.riderHttpSrv.confirmManualPayment(cartId, riderId, paymentOption).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) {
              return _actions_rider_action__WEBPACK_IMPORTED_MODULE_5__["actions"].ConfirmManualPaymentSuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_5__["actions"].ConfirmManualPaymentFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.getCartCheckList$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["createEffect"])(function () {
          return _this6.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetCartCheckListInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (action) {
            return _this6.riderHttpSrv.getCartCheckList(action.payload.cartId).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) {
              return _actions_rider_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetCartCheckListSuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetCartCheckListFailedAction({
                payload: error
              }));
            }));
          }));
        });
      };

      RiderEffectService.ctorParameters = function () {
        return [{
          type: _rider_http_service__WEBPACK_IMPORTED_MODULE_6__["RiderHttpService"]
        }, {
          type: _ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["Actions"]
        }];
      };

      RiderEffectService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: "root"
      })], RiderEffectService);
      /***/
    },

    /***/
    "./src/app/rider/store/effects/rider-http.service.ts":
    /*!***********************************************************!*\
      !*** ./src/app/rider/store/effects/rider-http.service.ts ***!
      \***********************************************************/

    /*! exports provided: RiderHttpService */

    /***/
    function srcAppRiderStoreEffectsRiderHttpServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RiderHttpService", function () {
        return RiderHttpService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! rxjs/operators */
      "./node_modules/rxjs/_esm2015/operators/index.js");
      /* harmony import */


      var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../../../../environments/environment */
      "./src/environments/environment.ts");

      var RiderHttpService = /*#__PURE__*/function () {
        function RiderHttpService(httpClient) {
          _classCallCheck(this, RiderHttpService);

          this.httpClient = httpClient;
        }

        _createClass(RiderHttpService, [{
          key: "getRiderDeliveryHistory",
          value: function getRiderDeliveryHistory(userId) {
            try {
              return this.httpClient.get("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Shopping/GetCartByRiderId?RiderId=").concat(userId, "&IncludePayment=true&IncludeOrder=true&CustomerDetail=true&IsActive=true")).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
            } catch (ex) {
              throw ex;
            }
          }
        }, {
          key: "getRiderRating",
          value: function getRiderRating(userId) {
            return this.httpClient.get("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Rider/GetRiderRatingModel?RiderId=").concat(userId)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
          }
        }, {
          key: "saveRiderCurrentGeolocation",
          value: function saveRiderCurrentGeolocation(userId, longitude, latitude) {
            try {
              return this.httpClient.post("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Rider/SaveRiderCartGeolocation?longitude=").concat(longitude, "&latitude=").concat(latitude, "&UserId=").concat(userId), {}).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
            } catch (ex) {
              throw ex;
            }
          }
        }, {
          key: "getRiderLocationTrackingForCartDelivery",
          value: function getRiderLocationTrackingForCartDelivery(cartId) {
            try {
              return this.httpClient.get("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Rider/GetCartRiderLocation?CartId=").concat(cartId)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
            } catch (ex) {
              throw ex;
            }
          }
        }, {
          key: "getSelectedCart",
          value: function getSelectedCart(cartId) {
            try {
              return this.httpClient.get("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Shopping/GetCartById?CartId=").concat(cartId)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
            } catch (ex) {
              throw ex;
            }
          }
        }, {
          key: "reverseGeocodingForAddress",
          value: function reverseGeocodingForAddress(address) {
            try {
              return this.httpClient.post("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Account/GetLocation?street=").concat(address), {}).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
            } catch (ex) {
              throw ex;
            }
          }
        }, {
          key: "startRiderDelivery",
          value: function startRiderDelivery(cartId, userId) {
            try {
              return this.httpClient.post("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Rider/RiderStartDelivery?UserId=").concat(userId, "&CartId=").concat(cartId), {}).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
            } catch (ex) {
              throw ex;
            }
          }
        }, {
          key: "cancelRiderDelivery",
          value: function cancelRiderDelivery(cartId, userId) {
            try {
              return this.httpClient.post("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Rider/RiderCancelDelivery?CartId=").concat(cartId, "&UserId=").concat(userId), {}).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
            } catch (ex) {
              throw ex;
            }
          }
        }, {
          key: "getMerchantInProximityToRider",
          value: function getMerchantInProximityToRider(cartId, latitude, longitude) {
            return this.httpClient.get("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Shopping/GetMerchantProximityToRider?CartId=").concat(cartId, "&RiderLatitude=").concat(latitude, "&RiderLongitude=").concat(longitude)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
          }
        }, {
          key: "getOrderInfo",
          value: function getOrderInfo(cartId) {
            return this.httpClient.get("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Payment/GetPaymentByCartId?CartId=").concat(cartId, "&IncludeCart=true")).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
          }
        }, {
          key: "getMerchantsRelatedToCart",
          value: function getMerchantsRelatedToCart(cartId) {
            try {
              return this.httpClient.get("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Shopping/GetMerchantByCartId?CartId=").concat(cartId)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
            } catch (ex) {
              throw ex;
            }
          }
        }, {
          key: "getRiderDeliveryGraphSummary",
          value: function getRiderDeliveryGraphSummary(riderId) {
            try {
              return this.httpClient.get("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Shopping/RiderDelievrySummary?RiderId=").concat(riderId)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
            } catch (ex) {
              throw ex;
            }
          }
        }, {
          key: "markDeliveryAsComplete",
          value: function markDeliveryAsComplete(cartId) {
            try {
              return this.httpClient.post("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Shopping/CompleteDelivery?CartId=").concat(cartId), {}).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
            } catch (ex) {
              throw ex;
            }
          }
        }, {
          key: "confirmManualPayment",
          value: function confirmManualPayment(cartId, riderId, paymentOption) {
            try {
              return this.httpClient.post("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Payment/ConfirmManualPayment?CartId=").concat(cartId, "&UserId=").concat(riderId, "&PaymentOption=").concat(paymentOption), {}).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
            } catch (ex) {
              throw ex;
            }
          }
        }, {
          key: "getCartCheckList",
          value: function getCartCheckList(cartId) {
            try {
              return this.httpClient.get("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Shopping/GetCartProductDetail?CartId=").concat(cartId)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
            } catch (ex) {
              throw ex;
            }
          }
        }]);

        return RiderHttpService;
      }();

      RiderHttpService.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]
        }];
      };

      RiderHttpService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: "root"
      })], RiderHttpService);
      /***/
    },

    /***/
    "./src/app/rider/store/model/rider.model.ts":
    /*!**************************************************!*\
      !*** ./src/app/rider/store/model/rider.model.ts ***!
      \**************************************************/

    /*! exports provided: InitialRiderState */

    /***/
    function srcAppRiderStoreModelRiderModelTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "InitialRiderState", function () {
        return InitialRiderState;
      });

      var InitialRiderState = {
        DeliveryActivities: [],
        CompletedDeliveries: [],
        OngoingDeliveries: [],
        RiderRating: undefined,
        ActiveActionMessage: undefined,
        ActiveError: undefined,
        ActiveMessage: undefined,
        RiderLocationGeoPoints: [],
        IsLoading: false,
        SelectedCart: undefined,
        SelectedAddressToLocation: undefined,
        MerchantsInProximityToRider: [],
        MerchantsRelatedToCart: [],
        SelectedOrderInfo: undefined,
        RiderDeliveryGraphData: [],
        SelectedCartCheckList: []
      };
      /***/
    },

    /***/
    "./src/app/rider/store/reducer/rider.reducer.ts":
    /*!******************************************************!*\
      !*** ./src/app/rider/store/reducer/rider.reducer.ts ***!
      \******************************************************/

    /*! exports provided: RiderReducer */

    /***/
    function srcAppRiderStoreReducerRiderReducerTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RiderReducer", function () {
        return RiderReducer;
      });
      /* harmony import */


      var _ngrx_store__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @ngrx/store */
      "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");
      /* harmony import */


      var _model_rider_model__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ../model/rider.model */
      "./src/app/rider/store/model/rider.model.ts");
      /* harmony import */


      var _actions_rider_action__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../actions/rider.action */
      "./src/app/rider/store/actions/rider.action.ts");
      /* harmony import */


      var _user_store_model_user_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../../../user/store/model/user.model */
      "./src/app/user/store/model/user.model.ts");
      /* harmony import */


      var _utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../../../../utils/functions/app.functions */
      "./src/utils/functions/app.functions.ts");

      var RiderReducer = Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createReducer"])(_model_rider_model__WEBPACK_IMPORTED_MODULE_1__["InitialRiderState"], Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].ClearActiveErrorAction, function (state) {
        return Object.assign(Object.assign({}, state), {
          ActiveError: undefined,
          ActiveMessage: undefined,
          IsLoading: false
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].ClearActiveMessageAction, function (state) {
        return Object.assign(Object.assign({}, state), {
          ActiveError: undefined,
          ActiveMessage: undefined,
          IsLoading: false
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].ClearActiveActionMessageAction, function (state) {
        return Object.assign(Object.assign({}, state), {
          ActiveActionMessage: undefined,
          ActiveMessage: undefined,
          IsLoading: false
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].ClearRiderStateAction, function (state) {
        return Object.assign({}, _model_rider_model__WEBPACK_IMPORTED_MODULE_1__["InitialRiderState"]);
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetRiderActivityHistoryInitiatedAction, function (state, _ref29) {
        var payload = _ref29.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetRiderActivityHistoryFailedAction, function (state, _ref30) {
        var payload = _ref30.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetRiderActivityHistorySuccessfulAction, function (state, _ref31) {
        var payload = _ref31.payload;
        var completedActivities = payload === null || payload === void 0 ? void 0 : payload.filter(function (data) {
          return data.cart.status === _user_store_model_user_model__WEBPACK_IMPORTED_MODULE_3__["ProductCartStatus"].COMPLETED;
        });
        var ongoingActivities = payload === null || payload === void 0 ? void 0 : payload.filter(function (data) {
          return data.cart.status === _user_store_model_user_model__WEBPACK_IMPORTED_MODULE_3__["ProductCartStatus"].TRANSIT;
        }); // const verifiedActivities: RiderCartType[] = payload?.filter((data) => data.cart.status === ProductCartStatus.VERIFIED); // New

        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          DeliveryActivities: _toConsumableArray(payload),
          // OngoingDeliveries: [...ongoingActivities, ...verifiedActivities],
          OngoingDeliveries: _toConsumableArray(ongoingActivities),
          CompletedDeliveries: _toConsumableArray(completedActivities)
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].NewCartAssignedToRiderAction, function (state, _ref32) {
        var cart = _ref32.payload.cart;

        if (cart.cart.status === _user_store_model_user_model__WEBPACK_IMPORTED_MODULE_3__["ProductCartStatus"].COMPLETED) {
          return Object.assign(Object.assign({}, state), {
            ActiveActionMessage: {
              Message: "New cart assigned to you",
              Type: _user_store_model_user_model__WEBPACK_IMPORTED_MODULE_3__["MessageActionType"].RIDER_ASSIGNED_TO_CART,
              Label: "Open"
            },
            DeliveryActivities: Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_4__["pushUniqueArrayValues"])(cart, state.DeliveryActivities, "START"),
            CompletedDeliveries: Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_4__["pushUniqueArrayValues"])(cart, state.CompletedDeliveries, "START")
          });
        }

        if (cart.cart.status === _user_store_model_user_model__WEBPACK_IMPORTED_MODULE_3__["ProductCartStatus"].TRANSIT) {
          return Object.assign(Object.assign({}, state), {
            ActiveActionMessage: {
              Message: "New cart assigned to you",
              Type: _user_store_model_user_model__WEBPACK_IMPORTED_MODULE_3__["MessageActionType"].RIDER_ASSIGNED_TO_CART,
              Label: "Open"
            },
            DeliveryActivities: Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_4__["pushUniqueArrayValues"])(cart, state.DeliveryActivities, "START"),
            OngoingDeliveries: Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_4__["pushUniqueArrayValues"])(cart, state.OngoingDeliveries, "START")
          });
        } else {
          return Object.assign(Object.assign({}, state), {
            ActiveActionMessage: {
              Message: "New cart assigned to you",
              Type: _user_store_model_user_model__WEBPACK_IMPORTED_MODULE_3__["MessageActionType"].RIDER_ASSIGNED_TO_CART,
              Label: "Open"
            },
            DeliveryActivities: Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_4__["pushUniqueArrayValues"])(cart, state.DeliveryActivities, "START")
          });
        }
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetRiderRatingInitiatedAction, function (state, _ref33) {
        var payload = _ref33.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetRiderRatingFailedAction, function (state, _ref34) {
        var payload = _ref34.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetRiderRatingSuccessfulAction, function (state, _ref35) {
        var payload = _ref35.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          RiderRating: Object.assign({}, payload)
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].SaveRiderGeolocationInitiatedAction, function (state, _ref36) {
        var payload = _ref36.payload;
        return Object.assign({}, state);
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].SaveRiderGeolocationFailedAction, function (state, _ref37) {
        var payload = _ref37.payload;
        return Object.assign(Object.assign({}, state), {
          // IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].SaveRiderGeolocationSuccessfulAction, function (state, _ref38) {
        var payload = _ref38.payload;
        return Object.assign({}, state);
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetRiderGeolocationHistoryForDeliveryInitiatedAction, function (state, _ref39) {
        var payload = _ref39.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetRiderGeolocationHistoryForDeliveryFailedAction, function (state, _ref40) {
        var payload = _ref40.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetRiderGeolocationHistoryForDeliverySuccessfulAction, function (state, _ref41) {
        var payload = _ref41.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          RiderLocationGeoPoints: _toConsumableArray(payload)
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetSelectedCartInitiatedAction, function (state, _ref42) {
        var payload = _ref42.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetSelectedCartFailedAction, function (state, _ref43) {
        var payload = _ref43.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetSelectedCartSuccessfulAction, function (state, _ref44) {
        var payload = _ref44.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          SelectedCart: Object.assign({}, payload)
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].ReverseGeocodingForAddressInitiatedAction, function (state, _ref45) {
        var payload = _ref45.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].ReverseGeocodingForAddressFailedAction, function (state, _ref46) {
        var payload = _ref46.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].ReverseGeocodingForAddressSuccessfulAction, function (state, _ref47) {
        var payload = _ref47.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          SelectedAddressToLocation: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].AcceptCartDeliveryInitiatedAction, function (state, _ref48) {
        var cartId = _ref48.payload.cartId;

        var _a, _b;

        var _Object = Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_4__["markCartAsInTransit"])(cartId, state.DeliveryActivities, state.OngoingDeliveries, _user_store_model_user_model__WEBPACK_IMPORTED_MODULE_3__["ProductCartStatus"].TRANSIT),
            All = _Object.All,
            Ongoing = _Object.Ongoing;

        return Object.assign(Object.assign({}, state), {
          IsLoading: true,
          DeliveryActivities: (_a = _toConsumableArray(All)) !== null && _a !== void 0 ? _a : _toConsumableArray(state.DeliveryActivities),
          OngoingDeliveries: (_b = _toConsumableArray(Ongoing)) !== null && _b !== void 0 ? _b : _toConsumableArray(state.OngoingDeliveries)
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].AcceptCartDeliveryFailedAction, function (state, _ref49) {
        var payload = _ref49.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].AcceptCartDeliverySuccessfulAction, function (state, _ref50) {
        var payload = _ref50.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveMessage: payload === true ? "You just accepted delivery" : undefined
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].CancelCartDeliveryInitiatedAction, function (state, _ref51) {
        var cartId = _ref51.payload.cartId;

        var _a, _b;

        var _Object2 = Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_4__["cancelCartDelivery"])(cartId, state.DeliveryActivities, state.OngoingDeliveries),
            All = _Object2.All,
            Ongoing = _Object2.Ongoing;

        return Object.assign(Object.assign({}, state), {
          IsLoading: true,
          DeliveryActivities: (_a = _toConsumableArray(All)) !== null && _a !== void 0 ? _a : _toConsumableArray(state.DeliveryActivities),
          OngoingDeliveries: (_b = _toConsumableArray(Ongoing)) !== null && _b !== void 0 ? _b : _toConsumableArray(state.OngoingDeliveries)
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].CancelCartDeliveryFailedAction, function (state, _ref52) {
        var payload = _ref52.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].CancelCartDeliverySuccessfulAction, function (state, _ref53) {
        var payload = _ref53.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveMessage: payload === true ? "You just canceled a delivery" : undefined
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetMerchantProximityToRiderInitiatedAction, function (state, _ref54) {
        var payload = _ref54.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetMerchantProximityToRiderFailedAction, function (state, _ref55) {
        var payload = _ref55.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetMerchantProximityToRiderSuccessfulAction, function (state, _ref56) {
        var payload = _ref56.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          MerchantsInProximityToRider: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetOrderInfoInitiatedAction, function (state, _ref57) {
        var payload = _ref57.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetOrderInfoFailedAction, function (state, _ref58) {
        var payload = _ref58.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveMessage: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetOrderInfoSuccessfulAction, function (state, _ref59) {
        var payload = _ref59.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          SelectedOrderInfo: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetMerchantsRelatedToCartInitiatedAction, function (state, _ref60) {
        var payload = _ref60.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetMerchantsRelatedToCartFailedAction, function (state, _ref61) {
        var payload = _ref61.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetMerchantsRelatedToCartSuccessfulAction, function (state, _ref62) {
        var payload = _ref62.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          MerchantsRelatedToCart: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetRiderDeliveryGraphSummaryInitiatedAction, function (state, _ref63) {
        var payload = _ref63.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetRiderDeliveryGraphSummaryFailedAction, function (state, _ref64) {
        var payload = _ref64.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetRiderDeliveryGraphSummarySuccessfulAction, function (state, _ref65) {
        var payload = _ref65.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          RiderDeliveryGraphData: _toConsumableArray(payload)
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].MarkDeliveryAsCompleteInitiatedAction, function (state, _ref66) {
        var cartId = _ref66.payload.cartId;

        var _a, _b;

        var _Object3 = Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_4__["markCartAsInTransit"])(cartId, state.DeliveryActivities, state.OngoingDeliveries, _user_store_model_user_model__WEBPACK_IMPORTED_MODULE_3__["ProductCartStatus"].COMPLETED),
            All = _Object3.All,
            Ongoing = _Object3.Ongoing;

        return Object.assign(Object.assign({}, state), {
          IsLoading: true,
          DeliveryActivities: (_a = _toConsumableArray(All)) !== null && _a !== void 0 ? _a : _toConsumableArray(state.DeliveryActivities),
          OngoingDeliveries: (_b = _toConsumableArray(Ongoing)) !== null && _b !== void 0 ? _b : _toConsumableArray(state.OngoingDeliveries)
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].MarkDeliveryAsCompleteFailedAction, function (state, _ref67) {
        var payload = _ref67.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].MarkDeliveryAsCompleteSuccessfulAction, function (state, _ref68) {
        var payload = _ref68.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].ConfirmManualPaymentInitiatedAction, function (state, _ref69) {
        var payload = _ref69.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].ConfirmManualPaymentFailedAction, function (state, _ref70) {
        var payload = _ref70.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].ConfirmManualPaymentSuccessfulAction, function (state, _ref71) {
        var payload = _ref71.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveMessage: payload ? "Payment confirmed" : undefined
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetCartCheckListInitiatedAction, function (state, _ref72) {
        var payload = _ref72.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetCartCheckListFailedAction, function (state, _ref73) {
        var payload = _ref73.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_rider_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetCartCheckListSuccessfulAction, function (state, _ref74) {
        var payload = _ref74.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          SelectedCartCheckList: _toConsumableArray(payload)
        });
      }));
      /***/
    },

    /***/
    "./src/app/services/fcm.service.ts":
    /*!*****************************************!*\
      !*** ./src/app/services/fcm.service.ts ***!
      \*****************************************/

    /*! exports provided: FcmService */

    /***/
    function srcAppServicesFcmServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "FcmService", function () {
        return FcmService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _capacitor_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @capacitor/core */
      "./node_modules/@capacitor/core/dist/esm/index.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../../utils/functions/app.functions */
      "./src/utils/functions/app.functions.ts");
      /* harmony import */


      var _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../../utils/types/app.constant */
      "./src/utils/types/app.constant.ts");

      var PushNotifications = _capacitor_core__WEBPACK_IMPORTED_MODULE_2__["Plugins"].PushNotifications;

      var FcmService = /*#__PURE__*/function () {
        function FcmService(router, // tslint:disable-next-line: variable-name
        _ngZone) {
          _classCallCheck(this, FcmService);

          this.router = router;
          this._ngZone = _ngZone;
          this.initPush();
        }

        _createClass(FcmService, [{
          key: "initPush",
          value: function initPush() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee30() {
              return regeneratorRuntime.wrap(function _callee30$(_context30) {
                while (1) {
                  switch (_context30.prev = _context30.next) {
                    case 0:
                      if (!(_capacitor_core__WEBPACK_IMPORTED_MODULE_2__["Capacitor"].platform !== "web")) {
                        _context30.next = 4;
                        break;
                      }

                      _context30.next = 3;
                      return this.registerPush();

                    case 3:
                      console.log({
                        message: "FCM push notification was initialized successfully"
                      });

                    case 4:
                    case "end":
                      return _context30.stop();
                  }
                }
              }, _callee30, this);
            }));
          }
        }, {
          key: "registerPush",
          value: function registerPush() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee31() {
              var _this7 = this;

              var permission;
              return regeneratorRuntime.wrap(function _callee31$(_context31) {
                while (1) {
                  switch (_context31.prev = _context31.next) {
                    case 0:
                      _context31.next = 2;
                      return PushNotifications.requestPermission();

                    case 2:
                      permission = _context31.sent;

                      if (permission.granted) {
                        // Register with Apple / Google to receive push via APNS/FCM
                        PushNotifications.register();
                      } else {// No permission for push granted
                      } // On success, we should be able to receive notifications


                      PushNotifications.addListener(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_5__["PushNotificationEvent"].PUSH_REGISTRATION, function (notification) {
                        _this7.notificationRegistrationHandler(notification);
                      }); // Some issue with our setup and push will not work

                      PushNotifications.addListener(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_5__["PushNotificationEvent"].PUSH_REGISTRATION_ERROR, function (notification) {
                        _this7.notificationErrorHandler(notification);
                      }); // Show us the notification payload if the app is open on our device

                      PushNotifications.addListener(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_5__["PushNotificationEvent"].PUSH_NOTIFICATION_RECIEVED, function (notification) {
                        _this7.notificationRecievedHandler(notification);
                      }); // Method called when tapping on a notification

                      PushNotifications.addListener(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_5__["PushNotificationEvent"].PUSH_NOTIFICATION_ACTION, function (notification) {
                        _this7.notificationActionPerformedHandler(notification);
                      });

                    case 8:
                    case "end":
                      return _context31.stop();
                  }
                }
              }, _callee31);
            }));
          }
        }, {
          key: "notificationRegistrationHandler",
          value: function notificationRegistrationHandler(token) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee32() {
              return regeneratorRuntime.wrap(function _callee32$(_context32) {
                while (1) {
                  switch (_context32.prev = _context32.next) {
                    case 0:
                      _context32.next = 2;
                      return Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_4__["saveDataToLocalStorage"])(token.value, _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_5__["LocalStorageKey"].DEVICE_KEY);

                    case 2:
                    case "end":
                      return _context32.stop();
                  }
                }
              }, _callee32);
            }));
          }
        }, {
          key: "notificationErrorHandler",
          value: function notificationErrorHandler(error) {
            console.log("Error: ".concat(JSON.stringify(error)));
          }
        }, {
          key: "notificationRecievedHandler",
          value: function notificationRecievedHandler(notification) {
            console.log("Push received: ".concat(JSON.stringify(notification)));
          }
        }, {
          key: "notificationActionPerformedHandler",
          value: function notificationActionPerformedHandler(notification) {
            console.log("Push action performed: ".concat(JSON.stringify(notification.notification)));
            var extraData = notification.notification.data;
            this.navigationOptions(extraData);
          }
        }, {
          key: "navigationOptions",
          value: function navigationOptions(data) {
            var _this8 = this;

            if (data === null || data === void 0 ? void 0 : data.Id) {
              var Id = data.Id,
                  PushNotificationType = data.PushNotificationType,
                  SecondaryId = data.SecondaryId;

              if (PushNotificationType === _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_5__["PushNotificationCategory"].CART_ASSIGNMENT) {
                this._ngZone.run(function () {
                  _this8.router.navigate(["/rider", "order-info", Id]);
                });
              }

              if (PushNotificationType === _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_5__["PushNotificationCategory"].CART_VERIFICATION) {
                this._ngZone.run(function () {
                  _this8.router.navigate(["/user", "make-payment", Id]);
                });
              }

              if (PushNotificationType === _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_5__["PushNotificationCategory"].PRODUCT_REMOVAL) {// ? Actions when a product is removed from the cart
              }
            }
          }
        }, {
          key: "resetBadgeCount",
          value: function resetBadgeCount() {
            PushNotifications.removeAllDeliveredNotifications();
          }
        }]);

        return FcmService;
      }();

      FcmService.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"]
        }];
      };

      FcmService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: "root"
      })], FcmService);
      /***/
    },

    /***/
    "./src/app/services/http-cache.service.ts":
    /*!************************************************!*\
      !*** ./src/app/services/http-cache.service.ts ***!
      \************************************************/

    /*! exports provided: HttpCacheService */

    /***/
    function srcAppServicesHttpCacheServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HttpCacheService", function () {
        return HttpCacheService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var HttpCacheService = /*#__PURE__*/function () {
        function HttpCacheService() {
          _classCallCheck(this, HttpCacheService);

          this.requests = {};
        }

        _createClass(HttpCacheService, [{
          key: "put",
          value: function put(url, response) {
            this.requests[url] = response;
          }
        }, {
          key: "get",
          value: function get(url) {
            return this.requests[url];
          }
        }, {
          key: "getAll",
          value: function getAll() {
            return this.requests;
          }
        }, {
          key: "invalidateCache",
          value: function invalidateCache() {
            this.requests = {};
          }
        }]);

        return HttpCacheService;
      }();

      HttpCacheService.ctorParameters = function () {
        return [];
      };

      HttpCacheService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: "root"
      })], HttpCacheService);
      /***/
    },

    /***/
    "./src/app/services/location.service.ts":
    /*!**********************************************!*\
      !*** ./src/app/services/location.service.ts ***!
      \**********************************************/

    /*! exports provided: LocationService */

    /***/
    function srcAppServicesLocationServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LocationService", function () {
        return LocationService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ngrx_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ngrx/store */
      "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");
      /* harmony import */


      var _capacitor_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @capacitor/core */
      "./node_modules/@capacitor/core/dist/esm/index.js");
      /* harmony import */


      var _rider_store_actions_rider_action__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../rider/store/actions/rider.action */
      "./src/app/rider/store/actions/rider.action.ts");
      /* harmony import */


      var _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../../utils/types/app.constant */
      "./src/utils/types/app.constant.ts");
      /* harmony import */


      var _utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ../../utils/functions/app.functions */
      "./src/utils/functions/app.functions.ts");
      /* harmony import */


      var _environments_environment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../../environments/environment */
      "./src/environments/environment.ts");

      var Geolocation = _capacitor_core__WEBPACK_IMPORTED_MODULE_3__["Plugins"].Geolocation;

      var LocationService = /*#__PURE__*/function () {
        function LocationService(store) {
          var _this9 = this;

          _classCallCheck(this, LocationService);

          this.store = store;
          this.getRiderCurrentGeoPosition = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
          Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_6__["mountGoogleMapScript"])(_environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].googleAPIKey);

          if (Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_6__["isScriptImported"])("googleMaps")) {
            // ? Setting the timeout makes sure that google maps loads first before setting the map icons
            setTimeout(function () {
              _this9.customerIcon = {
                label: "CUSTOMER",
                metadata: {
                  url: "assets/images/map-icons/user-map-icon.png",
                  // This marker is 20 pixels wide by 32 pixels high.
                  // size: new google.maps.Size(20, 32),
                  scaledSize: new google.maps.Size(70, 70),
                  // The origin for this image is (0, 0).
                  origin: new google.maps.Point(0, 0),
                  // The anchor for this image is the base of the flagpole at (0, 32).
                  anchor: new google.maps.Point(0, 32)
                }
              };
              _this9.riderIcon = {
                label: "RIDER",
                metadata: {
                  url: "assets/images/map-icons/motocycle-icon-3.png",
                  // This marker is 20 pixels wide by 32 pixels high.
                  // size: new google.maps.Size(20, 32),
                  scaledSize: new google.maps.Size(70, 70),
                  // The origin for this image is (0, 0).
                  origin: new google.maps.Point(0, 0),
                  // The anchor for this image is the base of the flagpole at (0, 32).
                  anchor: new google.maps.Point(0, 32)
                }
              };
            }, 5000);
          }
        }

        _createClass(LocationService, [{
          key: "sendRiderLocationData",
          value: function sendRiderLocationData(userId, latitude, longitude) {
            this.store.dispatch(_rider_store_actions_rider_action__WEBPACK_IMPORTED_MODULE_4__["actions"].SaveRiderGeolocationInitiatedAction({
              payload: {
                latitude: latitude,
                longitude: longitude,
                userId: userId
              }
            }));
          }
        }, {
          key: "getUserCurrentPosition",
          value: function getUserCurrentPosition() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee33() {
              var coordinates, _coordinates$coords, latitude, longitude;

              return regeneratorRuntime.wrap(function _callee33$(_context33) {
                while (1) {
                  switch (_context33.prev = _context33.next) {
                    case 0:
                      _context33.next = 2;
                      return Geolocation.getCurrentPosition({
                        enableHighAccuracy: true,
                        timeout: 10000,
                        maximumAge: 0
                      });

                    case 2:
                      coordinates = _context33.sent;
                      _coordinates$coords = coordinates.coords, latitude = _coordinates$coords.latitude, longitude = _coordinates$coords.longitude;

                      if (!(latitude && longitude)) {
                        _context33.next = 6;
                        break;
                      }

                      return _context33.abrupt("return", {
                        lat: latitude,
                        lng: longitude
                      });

                    case 6:
                    case "end":
                      return _context33.stop();
                  }
                }
              }, _callee33);
            }));
          }
        }, {
          key: "calculateAndDisplayRoute",
          value: function calculateAndDisplayRoute(directionsService, directionsRenderer, pointOfOrigin, pointOfDestination) {
            var transportMode = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_5__["TransportType"].DRIVING;
            directionsService.route({
              origin: Object.assign({}, pointOfOrigin),
              destination: Object.assign({}, pointOfDestination),
              // Note that Javascript allows us to access the constant
              // using square brackets and a string value as its
              // "property."
              travelMode: google.maps.TravelMode[transportMode],
              drivingOptions: {
                departureTime: new Date()
              }
            }, function (response, status) {
              if (status === "OK") {
                console.log({
                  mapResponse: response
                });
                directionsRenderer.setDirections(response);
              } else {
                console.error("Directions request failed due to " + status);
              }
            });
          } // ? Start geolocation remotely

        }, {
          key: "startGeolocationRemotely",
          value: function startGeolocationRemotely() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee34() {
              var _this10 = this;

              var userData, ongoingDeliveries$;
              return regeneratorRuntime.wrap(function _callee34$(_context34) {
                while (1) {
                  switch (_context34.prev = _context34.next) {
                    case 0:
                      _context34.prev = 0;
                      _context34.next = 3;
                      return Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_6__["getDataFromLocalStorage"])(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_5__["LocalStorageKey"].ZERO_30_USER);

                    case 3:
                      userData = _context34.sent;

                      if ((userData === null || userData === void 0 ? void 0 : userData.userId) && (userData === null || userData === void 0 ? void 0 : userData.role) === _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_5__["Zero30Role"].RIDER) {
                        this.store.dispatch(_rider_store_actions_rider_action__WEBPACK_IMPORTED_MODULE_4__["actions"].GetRiderActivityHistoryInitiatedAction({
                          payload: {
                            userId: userData.userId
                          }
                        }));
                        ongoingDeliveries$ = this.store.select(function (data) {
                          return data.Rider.OngoingDeliveries;
                        });
                        ongoingDeliveries$.subscribe(function (deliverydata) {
                          if (deliverydata.length > 0) {
                            // ? Pass in the number of carts that are in transit, this data will be used to Push rider's location to server
                            _this10.getRiderCurrentGeoPosition.emit({
                              Payload: deliverydata.length,
                              UserId: userData.userId
                            });
                          } else {
                            // ? Emit null if no deliveries are left for the rider
                            _this10.getRiderCurrentGeoPosition.emit({
                              Payload: null,
                              UserId: userData.userId
                            });
                          }
                        });
                      }

                      _context34.next = 10;
                      break;

                    case 7:
                      _context34.prev = 7;
                      _context34.t0 = _context34["catch"](0);
                      throw _context34.t0;

                    case 10:
                    case "end":
                      return _context34.stop();
                  }
                }
              }, _callee34, this, [[0, 7]]);
            }));
          }
        }, {
          key: "addMapMarker",
          value: function addMapMarker(map, locationInfo, title, position, iconType) {
            var infoWindow = new google.maps.InfoWindow({
              content: locationInfo
            });
            var marker = new google.maps.Marker({
              position: position,
              icon: iconType === "RIDER" ? this.riderIcon.metadata : this.customerIcon.metadata,
              map: map,
              title: title
            });
            marker.addListener("click", function () {
              infoWindow.open(map, marker);
            });
          }
        }]);

        return LocationService;
      }();

      LocationService.ctorParameters = function () {
        return [{
          type: _ngrx_store__WEBPACK_IMPORTED_MODULE_2__["Store"]
        }];
      };

      LocationService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: "root"
      })], LocationService);
      /***/
    },

    /***/
    "./src/app/services/navigation.service.ts":
    /*!************************************************!*\
      !*** ./src/app/services/navigation.service.ts ***!
      \************************************************/

    /*! exports provided: NavigationService */

    /***/
    function srcAppServicesNavigationServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "NavigationService", function () {
        return NavigationService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../../utils/functions/app.functions */
      "./src/utils/functions/app.functions.ts");
      /* harmony import */


      var _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../../utils/types/app.constant */
      "./src/utils/types/app.constant.ts");

      var NavigationService = /*#__PURE__*/function () {
        function NavigationService(location, router) {
          var _this11 = this;

          _classCallCheck(this, NavigationService);

          // setTimeout(async () => {
          //   this.history = await getDataFromLocalStorage<string[]>(LocalStorageKey.URL_HISTORY) || [];
          //   this.router.events.subscribe((event) => {
          //     if (event instanceof NavigationEnd) {
          //       // ? Clear the localStorage if the number os cached URLS exceed 10 (Avoid clutter in localStorage)
          //       this.history = this.clearNavigationHistory(this.history);
          //       this.history.push(event.urlAfterRedirects);
          //       saveDataToLocalStorage(this.history, LocalStorageKey.URL_HISTORY);
          //     }
          //   });
          // }, 1000);
          this.location = location;
          this.router = router;
          setTimeout(function () {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this11, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee35() {
              var _this12 = this;

              return regeneratorRuntime.wrap(function _callee35$(_context35) {
                while (1) {
                  switch (_context35.prev = _context35.next) {
                    case 0:
                      _context35.next = 2;
                      return Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_4__["getDataFromLocalStorage"])(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_5__["LocalStorageKey"].URL_HISTORY);

                    case 2:
                      _context35.t0 = _context35.sent;

                      if (_context35.t0) {
                        _context35.next = 5;
                        break;
                      }

                      _context35.t0 = [];

                    case 5:
                      this.history = _context35.t0;
                      this.router.events.subscribe(function (event) {
                        if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_3__["NavigationEnd"]) {
                          // ? Clear the localStorage if the number os cached URLS exceed 10 (Avoid clutter in localStorage)
                          _this12.history = _this12.clearNavigationHistory(_this12.history);

                          if (_this12.appendURLIfNotPopped(_this12.history, event.urlAfterRedirects)) {
                            _this12.history.push(event.urlAfterRedirects);

                            Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_4__["saveDataToLocalStorage"])(_this12.history, _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_5__["LocalStorageKey"].URL_HISTORY);
                          }
                        }
                      });

                    case 7:
                    case "end":
                      return _context35.stop();
                  }
                }
              }, _callee35, this);
            }));
          }, 1000);
        }

        _createClass(NavigationService, [{
          key: "appendURLIfNotPopped",
          value: function appendURLIfNotPopped(history, newURL) {
            var returnData = true;

            if ((history === null || history === void 0 ? void 0 : history.length) > 0) {
              if (history[history.length - 1] === newURL) {
                returnData = false;
              }
            }

            return returnData;
          } // ? This version works on the WEB by using the browser history & location APIs

        }, {
          key: "goBackOnWeb",
          value: function goBackOnWeb() {
            var _a;

            this.history.pop();

            if (((_a = this.history) === null || _a === void 0 ? void 0 : _a.length) > 0) {
              this.location.back();
            } else {
              this.router.navigate(["/"]);
            }
          } // ? This version works both on Web and native apps by using the localStorage API

        }, {
          key: "goBackOnNative",
          value: function goBackOnNative() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee36() {
              var urlHistory, previousUrl;
              return regeneratorRuntime.wrap(function _callee36$(_context36) {
                while (1) {
                  switch (_context36.prev = _context36.next) {
                    case 0:
                      _context36.next = 2;
                      return Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_4__["getDataFromLocalStorage"])(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_5__["LocalStorageKey"].URL_HISTORY);

                    case 2:
                      urlHistory = _context36.sent;

                      if ((urlHistory === null || urlHistory === void 0 ? void 0 : urlHistory.length) > 0) {
                        previousUrl = urlHistory[urlHistory.length - 2];
                        console.log({
                          previousUrl: previousUrl
                        });
                        this.router.navigateByUrl(previousUrl);
                      } else {
                        this.router.navigate(["/"]);
                      }

                    case 4:
                    case "end":
                      return _context36.stop();
                  }
                }
              }, _callee36, this);
            }));
          }
        }, {
          key: "clearNavigationHistory",
          value: function clearNavigationHistory(history) {
            if ((history === null || history === void 0 ? void 0 : history.length) > 10) {
              return [];
            } else {
              return _toConsumableArray(history);
            }
          }
        }]);

        return NavigationService;
      }();

      NavigationService.ctorParameters = function () {
        return [{
          type: _angular_common__WEBPACK_IMPORTED_MODULE_2__["Location"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
        }];
      };

      NavigationService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: "root"
      })], NavigationService);
      /***/
    },

    /***/
    "./src/app/services/signalr-websocket.service.ts":
    /*!*******************************************************!*\
      !*** ./src/app/services/signalr-websocket.service.ts ***!
      \*******************************************************/

    /*! exports provided: SignalrWebsocketService */

    /***/
    function srcAppServicesSignalrWebsocketServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SignalrWebsocketService", function () {
        return SignalrWebsocketService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _microsoft_signalr__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @microsoft/signalr */
      "./node_modules/@microsoft/signalr/dist/esm/index.js");
      /* harmony import */


      var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../../environments/environment */
      "./src/environments/environment.ts");

      var SignalrWebsocketService = /*#__PURE__*/function () {
        function SignalrWebsocketService() {
          var _this13 = this;

          _classCallCheck(this, SignalrWebsocketService);

          this.currentCartLocation = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
          this.cartAssignedToRider = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
          this.cartVerified = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();

          this.startConnection = function () {
            _this13.hubConnection = new _microsoft_signalr__WEBPACK_IMPORTED_MODULE_2__["HubConnectionBuilder"]().withUrl(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].websocketRoot).withAutomaticReconnect().configureLogging(_microsoft_signalr__WEBPACK_IMPORTED_MODULE_2__["LogLevel"].Debug).build();
            _this13.hubConnection.keepAliveIntervalInMilliseconds = 1000 * 60 * 5; // 5 minutes

            _this13.hubConnection.serverTimeoutInMilliseconds = 1000 * 60 * 10; // 10 minutes

            _this13.thenAble = _this13.hubConnection.start();

            _this13.thenAble.then(function () {
              console.log("%cWebsocket connection started", "color: #2FA823; font-size: 13px;");
            })["catch"](function (error) {
              console.log("%cError while starting connection: ".concat(error), "color: red; font-size: 13px;");
            });
          };

          this.confirmCartVerificationRequest = function () {
            try {
              _this13.hubConnection.on("ConfirmedCartVerification", function (payload) {
                _this13.cartVerified.emit(payload);
              });
            } catch (ex) {
              console.error(ex);
            }
          };

          this.cartAssignedToCartRider = function () {
            try {
              _this13.hubConnection.on("AssignedCart", function (payload) {
                _this13.cartAssignedToRider.emit(payload);
              });
            } catch (ex) {
              throw ex;
            }
          };

          this.makeCartVerificationRequest = function (cartId) {
            _this13.thenAble.then(function () {
              _this13.hubConnection.invoke("CartVerificationRequest", cartId).then(function () {
                console.log({
                  message: "Verification request was sent successfully"
                });
              });
            });
          };

          this.findCurrentLocationOfCart = function () {
            try {
              _this13.hubConnection.on("CartLocation", function (payload) {
                _this13.currentCartLocation.emit(payload);
              });
            } catch (ex) {
              throw ex;
            }
          };

          this.joinGroupToViewCartLocation = function (cartId) {
            try {
              _this13.thenAble.then(function () {
                _this13.hubConnection.invoke("CustomerAddToCartGroup", cartId).then(function (payload) {
                  console.log({
                    message: "Geolocation payload for cart can be gotten"
                  });

                  _this13.currentCartLocation.emit(payload);
                });
              });
            } catch (ex) {
              throw ex;
            }
          };

          this.addRiderToGroup = function (userId) {
            _this13.thenAble.then(function () {
              _this13.hubConnection.invoke("AddRiderToGroup", userId).then(function () {
                console.log({
                  message: "Rider joined group connection"
                });
              });
            });
          };

          this.reconnectToWebSocket = function () {
            _this13.hubConnection.onclose(function () {
              setTimeout(function () {
                _this13.startConnection();
              }, 5000);
            });
          };
        }

        _createClass(SignalrWebsocketService, [{
          key: "disconnect",
          value: function disconnect() {
            try {
              this.hubConnection.stop();
            } catch (ex) {
              console.error(ex);
            }
          }
        }]);

        return SignalrWebsocketService;
      }();

      SignalrWebsocketService.ctorParameters = function () {
        return [];
      };

      SignalrWebsocketService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: "root"
      })], SignalrWebsocketService);
      /***/
    },

    /***/
    "./src/app/shared/accordian/accordian.component.scss":
    /*!***********************************************************!*\
      !*** ./src/app/shared/accordian/accordian.component.scss ***!
      \***********************************************************/

    /*! exports provided: default */

    /***/
    function srcAppSharedAccordianAccordianComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".main {\n  font-family: \"Quicksand\", sans-serif;\n  border-top: none;\n  background: #fff;\n  margin-bottom: 15px;\n}\n.main .icon {\n  width: 40px;\n  height: 40px;\n  border-radius: 100%;\n  vertical-align: middle;\n}\n.main h4 {\n  font-family: \"Quicksand\", sans-serif;\n}\n.main header {\n  display: flex;\n  flex-direction: row;\n  justify-content: center;\n  align-items: center;\n  border-bottom: 1px solid #DEDEDE;\n  padding: 10px 15px;\n}\n.main header .header-section {\n  width: 50%;\n}\n.main i {\n  font-size: 20px;\n}\n.main .product-list {\n  padding: 0 15px;\n}\n.collapsed {\n  max-height: 0 !important;\n}\n.expand-wrapper {\n  transition: max-height 0.9s ease-in-out;\n  overflow: hidden;\n  height: auto;\n}\n.squeeze-wrapper {\n  transition: max-height 0.9s ease-in-out;\n  height: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL2FjY29yZGlhbi9hY2NvcmRpYW4uY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxvQ0FBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtBQUNKO0FBQ0k7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7QUFDUjtBQUVJO0VBQ0ksb0NBQUE7QUFBUjtBQUdJO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGdDQUFBO0VBQ0Esa0JBQUE7QUFEUjtBQUdRO0VBQ0ksVUFBQTtBQURaO0FBTUk7RUFDSSxlQUFBO0FBSlI7QUFPSTtFQUNJLGVBQUE7QUFMUjtBQVVBO0VBQ0ksd0JBQUE7QUFQSjtBQVVBO0VBQ0ksdUNBQUE7RUFDQSxnQkFBQTtFQUNBLFlBQUE7QUFQSjtBQVVBO0VBQ0ksdUNBQUE7RUFFQSxTQUFBO0FBUkoiLCJmaWxlIjoic3JjL2FwcC9zaGFyZWQvYWNjb3JkaWFuL2FjY29yZGlhbi5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tYWluIHtcbiAgICBmb250LWZhbWlseTogJ1F1aWNrc2FuZCcsIHNhbnMtc2VyaWY7XG4gICAgYm9yZGVyLXRvcDogbm9uZTtcbiAgICBiYWNrZ3JvdW5kOiAjZmZmO1xuICAgIG1hcmdpbi1ib3R0b206IDE1cHg7XG5cbiAgICAuaWNvbiB7XG4gICAgICAgIHdpZHRoOiA0MHB4O1xuICAgICAgICBoZWlnaHQ6IDQwcHg7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDEwMCU7XG4gICAgICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XG4gICAgfVxuXG4gICAgaDQge1xuICAgICAgICBmb250LWZhbWlseTogJ1F1aWNrc2FuZCcsIHNhbnMtc2VyaWY7XG4gICAgfVxuXG4gICAgaGVhZGVyIHtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjREVERURFO1xuICAgICAgICBwYWRkaW5nOiAxMHB4IDE1cHg7XG4gICAgXG4gICAgICAgIC5oZWFkZXItc2VjdGlvbiB7XG4gICAgICAgICAgICB3aWR0aDogNTAlO1xuICAgIFxuICAgICAgICB9XG4gICAgfVxuXG4gICAgaSB7XG4gICAgICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICB9XG5cbiAgICAucHJvZHVjdC1saXN0IHtcbiAgICAgICAgcGFkZGluZzogMCAxNXB4O1xuICAgIH1cblxufVxuXG4uY29sbGFwc2VkIHtcbiAgICBtYXgtaGVpZ2h0OiAwICFpbXBvcnRhbnQ7XG59XG5cbi5leHBhbmQtd3JhcHBlciB7XG4gICAgdHJhbnNpdGlvbjogbWF4LWhlaWdodCAwLjlzIGVhc2UtaW4tb3V0O1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgaGVpZ2h0OiBhdXRvO1xufVxuXG4uc3F1ZWV6ZS13cmFwcGVyIHtcbiAgICB0cmFuc2l0aW9uOiBtYXgtaGVpZ2h0IDAuOXMgZWFzZS1pbi1vdXQ7XG4gICAgLy8gb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICBoZWlnaHQ6IDA7XG59Il19 */";
      /***/
    },

    /***/
    "./src/app/shared/accordian/accordian.component.ts":
    /*!*********************************************************!*\
      !*** ./src/app/shared/accordian/accordian.component.ts ***!
      \*********************************************************/

    /*! exports provided: AccordianComponent */

    /***/
    function srcAppSharedAccordianAccordianComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AccordianComponent", function () {
        return AccordianComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var AccordianComponent = /*#__PURE__*/function () {
        function AccordianComponent() {
          _classCallCheck(this, AccordianComponent);

          this.isAccordianExpanded = true;
        }

        _createClass(AccordianComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "toggleAccordian",
          value: function toggleAccordian() {
            this.isAccordianExpanded = !this.isAccordianExpanded;
          }
        }]);

        return AccordianComponent;
      }();

      AccordianComponent.ctorParameters = function () {
        return [];
      };

      AccordianComponent.propDecorators = {
        cartCheckList: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }]
      };
      AccordianComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-accordian",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./accordian.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/accordian/accordian.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./accordian.component.scss */
        "./src/app/shared/accordian/accordian.component.scss"))["default"]]
      })], AccordianComponent);
      /***/
    },

    /***/
    "./src/app/shared/add-to-cart/add-to-cart.component.scss":
    /*!***************************************************************!*\
      !*** ./src/app/shared/add-to-cart/add-to-cart.component.scss ***!
      \***************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppSharedAddToCartAddToCartComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "#addtocart-popup {\n  height: 100vh;\n  position: fixed;\n  top: 0;\n  left: 0;\n  z-index: 3;\n  height: 100%;\n  background-color: #222b45;\n  background-color: rgba(34, 43, 69, 0.7);\n  -webkit-backdrop-filter: blur(5px);\n  backdrop-filter: blur(5px);\n  opacity: 0;\n  visibility: hidden;\n  transition: all 0.2s linear 0s;\n  width: 100%;\n  overflow-x: hidden;\n}\n#addtocart-popup .popup-content {\n  position: fixed;\n  bottom: 0;\n  left: 0;\n  max-height: 90%;\n  height: 100%;\n  width: 100%;\n  z-index: 1;\n  background-color: #ffffff;\n  border-radius: 10px 10px 0px 0px;\n  overflow-y: scroll;\n  overflow-x: hidden;\n  position: fixed;\n  bottom: 0;\n  background-color: #fefefe;\n  width: 100%;\n  bottom: -300px;\n  opacity: 0;\n  transition: all 0.35s cubic-bezier(0.48, 1.42, 0.48, 1.45) 0s;\n}\n#addtocart-popup .close-popup {\n  width: 100%;\n  padding: 15px;\n  line-height: 0;\n  border-bottom: 1px solid #e6e8ef;\n}\n#addtocart-popup .popup-content .close-popup:active {\n  background-color: rgba(197, 206, 224, 0.3);\n}\n#addtocart-popup .popup-content .close-popup img,\n#addtocart-popup .popup-content .close-popup svg {\n  height: 11px;\n}\n#addtocart-popup .popup-content .close-popup img path,\n#addtocart-popup .popup-content .close-popup svg path {\n  fill: #C5CEE0;\n}\n#addtocart-popup .popup-content .title {\n  font-size: 17px;\n  line-height: 17px;\n  font-weight: 600;\n  padding: 30px 15px 15px 15px;\n}\n#addtocart-popup .popup-content .product-details {\n  display: grid;\n  grid-template-columns: auto 1fr;\n  grid-gap: 10px;\n  grid-template-rows: auto;\n  grid-template-areas: \"image details\";\n  align-items: center;\n  border-bottom: 1px solid #e6e8ef;\n}\n#addtocart-popup .popup-content .product-details .image {\n  display: grid;\n  width: 100px;\n  height: 100px;\n  border-radius: 8px;\n  overflow: hidden;\n  grid-row: 1;\n  grid-column: 1;\n  grid-area: image;\n}\n#addtocart-popup .popup-content .product-details .image:active {\n  transform: scale(0.95);\n}\n#addtocart-popup .popup-content .product-details .image img {\n  width: 100%;\n  height: 100%;\n  -o-object-fit: cover;\n  object-fit: cover;\n}\n#addtocart-popup .popup-content .product-details .details {\n  display: grid;\n  grid-template-columns: auto auto auto 1fr;\n  grid-template-rows: auto;\n  grid-gap: 10px;\n  grid-row: 1;\n  grid-column: 2;\n  grid-area: details;\n  align-items: center;\n  grid-template-areas: \"name name name name\" \"bag line like .\" \"price price price quantity\";\n}\n#addtocart-popup .popup-content .product-details .details .name {\n  grid-row: 1;\n  grid-column: 1;\n  grid-area: name;\n  color: #222b45;\n  font-weight: 600;\n  font-size: 15px;\n  line-height: 18px;\n  margin-bottom: 0;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  max-height: 36px;\n}\n#addtocart-popup .popup-content .product-details .details .price {\n  grid-row: 2;\n  grid-column: 1;\n  grid-area: price;\n  font-size: 15px;\n  line-height: 15px;\n  color: #fa6400;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: nowrap;\n}\n#addtocart-popup .popup-content .product-details .details .bag {\n  grid-row: 2;\n  grid-column: 1;\n  grid-area: bag;\n}\n#addtocart-popup .popup-content .product-details .details .line {\n  width: 1px;\n  height: 16px;\n  background: #e0e0e0;\n  grid-row: 2;\n  grid-column: 2;\n  grid-area: line;\n}\n#addtocart-popup .popup-content .product-details .details .like {\n  grid-row: 2;\n  grid-column: 3;\n  grid-area: like;\n}\n#addtocart-popup .popup-content .product-details .details .bag,\n#addtocart-popup .popup-content .product-details .details .like {\n  display: grid;\n  grid-auto-flow: column;\n  grid-gap: 5px;\n  grid-template-columns: -webkit-max-content -webkit-max-content;\n  grid-template-columns: max-content max-content;\n  align-items: center;\n}\n#addtocart-popup .popup-content .product-details .details .bag img,\n#addtocart-popup .popup-content .product-details .details .bag svg,\n#addtocart-popup .popup-content .product-details .details .like img,\n#addtocart-popup .popup-content .product-details .details .like svg {\n  width: auto;\n  height: 12px;\n}\n#addtocart-popup .popup-content .product-details .details .bag img path,\n#addtocart-popup .popup-content .product-details .details .bag svg path,\n#addtocart-popup .popup-content .product-details .details .like img path,\n#addtocart-popup .popup-content .product-details .details .like svg path {\n  fill: #8f9bb3;\n}\n#addtocart-popup .popup-content .product-details .details .bag span,\n#addtocart-popup .popup-content .product-details .details .like span {\n  font-size: 12px;\n  line-height: 12px;\n  color: #8f9bb3;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: nowrap;\n}\n#addtocart-popup .popup-content .product-details .details .quantity {\n  grid-row: 3;\n  grid-column: 4;\n  grid-area: quantity;\n  display: grid;\n  grid-auto-flow: column;\n  grid-gap: 10px;\n  align-items: center;\n  grid-template-columns: -webkit-max-content -webkit-max-content -webkit-max-content;\n  grid-template-columns: max-content max-content max-content;\n  justify-content: right;\n}\n#addtocart-popup .popup-content .product-details .details .quantity .btn {\n  padding: 0;\n}\n#addtocart-popup .popup-content .product-details .details .quantity .btn img,\n#addtocart-popup .popup-content .product-details .details .quantity .btn svg {\n  width: 30px;\n  height: auto;\n}\n#addtocart-popup .popup-content .product-details .details .quantity .btn img rect,\n#addtocart-popup .popup-content .product-details .details .quantity .btn svg rect {\n  fill: #fa6400;\n  opacity: 1;\n}\n#addtocart-popup .popup-content .product-details .details .quantity .btn img path,\n#addtocart-popup .popup-content .product-details .details .quantity .btn svg path {\n  fill: #ffffff;\n  opacity: 1;\n}\n#addtocart-popup .popup-content .product-details .details .quantity span {\n  padding: 0;\n  border-radius: 0;\n  border: none;\n  font-size: 17px;\n  line-height: 17px;\n  font-weight: 600;\n  min-width: 20px;\n  text-align: center;\n  color: #222b45;\n}\n#addtocart-popup .popup-content .attribute-title {\n  background-color: #edf1f7;\n}\n#addtocart-popup .popup-content .attribute-title p:nth-child(2) {\n  justify-self: end;\n}\n#addtocart-popup .popup-content .attributes .h-grid {\n  grid-gap: 10px;\n  grid-template-columns: 1fr auto;\n  align-items: center;\n}\n#addtocart-popup .popup-content .attributes .price {\n  font-weight: 600;\n  color: #222b45;\n}\n#addtocart-popup .popup-content .attributes .checkbox label {\n  font-weight: 600;\n}\n#addtocart-popup .popup-content .attributes .checkbox label span {\n  font-weight: 400;\n  color: #8f9bb3;\n}\n#addtocart-popup .popup-content .note .title {\n  background-color: #edf1f7;\n  padding: 0;\n}\n#addtocart-popup .popup-content .note textarea {\n  background-color: #F7F9FC;\n  width: 100%;\n  height: 80px;\n  border-radius: 4px;\n  resize: none;\n}\n#addtocart-popup .popup-content .big-cart-btn {\n  display: grid;\n  grid-template-columns: auto 1fr;\n  grid-template-rows: auto;\n  grid-gap: 15px;\n  align-items: center;\n  width: 100%;\n  padding: 20px 30px;\n  grid-template-areas: \"items text\" \"price text\";\n}\n#addtocart-popup .popup-content .big-cart-btn .items {\n  grid-row: 3;\n  grid-column: 1;\n  grid-area: items;\n  font-size: 12px;\n  line-height: 12px;\n  color: #ffffff;\n  text-transform: uppercase;\n}\n#addtocart-popup .popup-content .big-cart-btn .price {\n  grid-row: 2;\n  grid-column: 1;\n  grid-area: price;\n  font-size: 17px;\n  line-height: 17px;\n  font-weight: 600;\n  color: #ffffff;\n}\n#addtocart-popup .popup-content .big-cart-btn .text {\n  grid-row: 1;\n  -ms-grid-row-span: 2;\n  grid-column: 2;\n  grid-area: text;\n  font-size: 17px;\n  line-height: 17px;\n  color: #ffffff;\n  font-weight: 600;\n  justify-self: end;\n}\n#addtocart-popup.active {\n  overflow: hidden;\n  opacity: 1;\n  visibility: visible;\n}\n#addtocart-popup.active .popup-content {\n  bottom: 0;\n  opacity: 1;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL2FkZC10by1jYXJ0L2FkZC10by1jYXJ0LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsYUFBQTtFQUNBLGVBQUE7RUFDQSxNQUFBO0VBQ0EsT0FBQTtFQUNBLFVBQUE7RUFDQSxZQUFBO0VBQ0EseUJBQUE7RUFDQSx1Q0FBQTtFQUNFLGtDQUFBO0VBQ1EsMEJBQUE7RUFDVixVQUFBO0VBQ0Esa0JBQUE7RUFFQSw4QkFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtBQUNGO0FBQ0U7RUFDRSxlQUFBO0VBQ0EsU0FBQTtFQUNBLE9BQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxVQUFBO0VBQ0EseUJBQUE7RUFDQSxnQ0FBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsU0FBQTtFQUNBLHlCQUFBO0VBQ0EsV0FBQTtFQUNBLGNBQUE7RUFDQSxVQUFBO0VBRUEsNkRBQUE7QUFDSjtBQUVFO0VBQ0ksV0FBQTtFQUNBLGFBQUE7RUFDQSxjQUFBO0VBQ0EsZ0NBQUE7QUFBTjtBQUlBO0VBQ0ksMENBQUE7QUFESjtBQUlFOztFQUVFLFlBQUE7QUFESjtBQUlFOztFQUVFLGFBQUE7QUFESjtBQUlDO0VBQ0csZUFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSw0QkFBQTtBQURKO0FBSUU7RUFFRSxhQUFBO0VBRUksK0JBQUE7RUFDSixjQUFBO0VBRUksd0JBQUE7RUFDQSxvQ0FBQTtFQUdJLG1CQUFBO0VBQ1IsZ0NBQUE7QUFESjtBQUlFO0VBRUUsYUFBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUVBLFdBQUE7RUFFQSxjQUFBO0VBQ0EsZ0JBQUE7QUFESjtBQUlFO0VBRVUsc0JBQUE7QUFEWjtBQUlFO0VBQ0UsV0FBQTtFQUNBLFlBQUE7RUFDQSxvQkFBQTtFQUNHLGlCQUFBO0FBRFA7QUFJRTtFQUVFLGFBQUE7RUFDSSx5Q0FBQTtFQUNBLHdCQUFBO0VBQ0osY0FBQTtFQUVBLFdBQUE7RUFFQSxjQUFBO0VBQ0Esa0JBQUE7RUFHUSxtQkFBQTtFQUNKLHlGQUFBO0FBRFI7QUFJRTtFQUVFLFdBQUE7RUFFQSxjQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSx1QkFBQTtFQUNBLGdCQUFBO0FBREo7QUFJRTtFQUVFLFdBQUE7RUFFQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FBREo7QUFJRTtFQUVFLFdBQUE7RUFFQSxjQUFBO0VBQ0EsY0FBQTtBQURKO0FBSUU7RUFDRSxVQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBRUEsV0FBQTtFQUVBLGNBQUE7RUFDQSxlQUFBO0FBREo7QUFJRTtFQUVFLFdBQUE7RUFFQSxjQUFBO0VBQ0EsZUFBQTtBQURKO0FBSUU7O0VBR0UsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsYUFBQTtFQUVJLDhEQUFBO0VBQ0EsOENBQUE7RUFHSSxtQkFBQTtBQURaO0FBSUU7Ozs7RUFJRSxXQUFBO0VBQ0EsWUFBQTtBQURKO0FBSUE7Ozs7RUFJRSxhQUFBO0FBREY7QUFJQTs7RUFFRSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FBREY7QUFJQTtFQUVFLFdBQUE7RUFFQSxjQUFBO0VBQ0EsbUJBQUE7RUFFQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxjQUFBO0VBR1EsbUJBQUE7RUFFSixrRkFBQTtFQUNBLDBEQUFBO0VBR0ksc0JBQUE7QUFEVjtBQUlBO0VBQ0UsVUFBQTtBQURGO0FBSUE7O0VBRUUsV0FBQTtFQUNBLFlBQUE7QUFERjtBQUlBOztFQUVFLGFBQUE7RUFDQSxVQUFBO0FBREY7QUFJQTs7RUFFRSxhQUFBO0VBQ0EsVUFBQTtBQURGO0FBSUE7RUFDRSxVQUFBO0VBQ0EsZ0JBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0FBREY7QUFJQTtFQUNJLHlCQUFBO0FBREo7QUFJRTtFQUVNLGlCQUFBO0FBRFI7QUFJRTtFQUNFLGNBQUE7RUFFSSwrQkFBQTtFQUdJLG1CQUFBO0FBRFo7QUFJRTtFQUNFLGdCQUFBO0VBQ0EsY0FBQTtBQURKO0FBSUU7RUFDRSxnQkFBQTtBQURKO0FBSUU7RUFDRSxnQkFBQTtFQUNBLGNBQUE7QUFESjtBQUlDO0VBQ0cseUJBQUE7RUFDQSxVQUFBO0FBREo7QUFJRTtFQUNFLHlCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7QUFESjtBQUlDO0VBRUcsYUFBQTtFQUVJLCtCQUFBO0VBRUEsd0JBQUE7RUFDSixjQUFBO0VBR1EsbUJBQUE7RUFDUixXQUFBO0VBQ0Esa0JBQUE7RUFDSSw4Q0FBQTtBQURSO0FBSUU7RUFFRSxXQUFBO0VBRUEsY0FBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLHlCQUFBO0FBREo7QUFJRztFQUVDLFdBQUE7RUFFQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7QUFESjtBQUlDO0VBRUcsV0FBQTtFQUNBLG9CQUFBO0VBRUEsY0FBQTtFQUNBLGVBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7RUFFSSxpQkFBQTtBQURSO0FBSUU7RUFDRSxnQkFBQTtFQUNBLFVBQUE7RUFDQSxtQkFBQTtBQURKO0FBSUM7RUFDRyxTQUFBO0VBQ0EsVUFBQTtBQURKIiwiZmlsZSI6InNyYy9hcHAvc2hhcmVkL2FkZC10by1jYXJ0L2FkZC10by1jYXJ0LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI2FkZHRvY2FydC1wb3B1cCB7XG4gIGhlaWdodDogMTAwdmg7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgdG9wOiAwO1xuICBsZWZ0OiAwO1xuICB6LWluZGV4OiAzO1xuICBoZWlnaHQ6IDEwMCU7XG4gIGJhY2tncm91bmQtY29sb3I6ICMyMjJiNDU7XG4gIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMzQsIDQzLCA2OSwgMC43KTtcbiAgICAtd2Via2l0LWJhY2tkcm9wLWZpbHRlcjogYmx1cig1cHgpO1xuICAgICAgICAgICAgYmFja2Ryb3AtZmlsdGVyOiBibHVyKDVweCk7XG4gIG9wYWNpdHk6IDA7XG4gIHZpc2liaWxpdHk6IGhpZGRlbjtcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAwLjJzIGxpbmVhciAwcztcbiAgdHJhbnNpdGlvbjogYWxsIDAuMnMgbGluZWFyIDBzO1xuICB3aWR0aDogMTAwJTtcbiAgb3ZlcmZsb3cteDogaGlkZGVuOy8vPyBOZXdseSBhZGRlZFxuXG4gIC5wb3B1cC1jb250ZW50IHtcbiAgICBwb3NpdGlvbjogZml4ZWQ7XG4gICAgYm90dG9tOiAwO1xuICAgIGxlZnQ6IDA7XG4gICAgbWF4LWhlaWdodDogOTAlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICB6LWluZGV4OiAxO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XG4gICAgYm9yZGVyLXJhZGl1czogMTBweCAxMHB4IDBweCAwcHg7XG4gICAgb3ZlcmZsb3cteTogc2Nyb2xsO1xuICAgIG92ZXJmbG93LXg6IGhpZGRlbjtcbiAgICBwb3NpdGlvbjogZml4ZWQ7XG4gICAgYm90dG9tOiAwO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZWZlZmU7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgYm90dG9tOiAtMzAwcHg7XG4gICAgb3BhY2l0eTogMDtcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAwLjM1cyBjdWJpYy1iZXppZXIoMC40OCwgMS40MiwgMC40OCwgMS40NSkgMHM7XG4gICAgdHJhbnNpdGlvbjogYWxsIDAuMzVzIGN1YmljLWJlemllcigwLjQ4LCAxLjQyLCAwLjQ4LCAxLjQ1KSAwcztcbiAgfVxuXG4gIC5jbG9zZS1wb3B1cCB7XG4gICAgICB3aWR0aDogMTAwJTtcbiAgICAgIHBhZGRpbmc6IDE1cHg7XG4gICAgICBsaW5lLWhlaWdodDogMDtcbiAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZTZlOGVmO1xuICB9XG59XG5cbiNhZGR0b2NhcnQtcG9wdXAgLnBvcHVwLWNvbnRlbnQgLmNsb3NlLXBvcHVwOmFjdGl2ZSB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgxOTcsIDIwNiwgMjI0LCAwLjMpO1xuICB9XG4gIFxuICAjYWRkdG9jYXJ0LXBvcHVwIC5wb3B1cC1jb250ZW50IC5jbG9zZS1wb3B1cCBpbWcsXG4gICNhZGR0b2NhcnQtcG9wdXAgLnBvcHVwLWNvbnRlbnQgLmNsb3NlLXBvcHVwIHN2ZyB7XG4gICAgaGVpZ2h0OiAxMXB4O1xuICB9XG4gIFxuICAjYWRkdG9jYXJ0LXBvcHVwIC5wb3B1cC1jb250ZW50IC5jbG9zZS1wb3B1cCBpbWcgcGF0aCxcbiAgI2FkZHRvY2FydC1wb3B1cCAucG9wdXAtY29udGVudCAuY2xvc2UtcG9wdXAgc3ZnIHBhdGgge1xuICAgIGZpbGw6ICNDNUNFRTA7XG4gIH1cbiAgXG4gI2FkZHRvY2FydC1wb3B1cCAucG9wdXAtY29udGVudCAudGl0bGUge1xuICAgIGZvbnQtc2l6ZTogMTdweDtcbiAgICBsaW5lLWhlaWdodDogMTdweDtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIHBhZGRpbmc6IDMwcHggMTVweCAxNXB4IDE1cHg7XG4gIH1cblxuICAjYWRkdG9jYXJ0LXBvcHVwIC5wb3B1cC1jb250ZW50IC5wcm9kdWN0LWRldGFpbHMge1xuICAgIGRpc3BsYXk6IC1tcy1ncmlkO1xuICAgIGRpc3BsYXk6IGdyaWQ7XG4gICAgLW1zLWdyaWQtY29sdW1uczogYXV0byAxZnI7XG4gICAgICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogYXV0byAxZnI7XG4gICAgZ3JpZC1nYXA6IDEwcHg7XG4gICAgLW1zLWdyaWQtcm93czogYXV0bztcbiAgICAgICAgZ3JpZC10ZW1wbGF0ZS1yb3dzOiBhdXRvO1xuICAgICAgICBncmlkLXRlbXBsYXRlLWFyZWFzOiBcImltYWdlIGRldGFpbHNcIjtcbiAgICAtd2Via2l0LWJveC1hbGlnbjogY2VudGVyO1xuICAgICAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2U2ZThlZjtcbiAgfVxuICBcbiAgI2FkZHRvY2FydC1wb3B1cCAucG9wdXAtY29udGVudCAucHJvZHVjdC1kZXRhaWxzIC5pbWFnZSB7XG4gICAgZGlzcGxheTogLW1zLWdyaWQ7XG4gICAgZGlzcGxheTogZ3JpZDtcbiAgICB3aWR0aDogMTAwcHg7XG4gICAgaGVpZ2h0OiAxMDBweDtcbiAgICBib3JkZXItcmFkaXVzOiA4cHg7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICAtbXMtZ3JpZC1yb3c6IDE7XG4gICAgZ3JpZC1yb3c6IDE7XG4gICAgLW1zLWdyaWQtY29sdW1uOiAxO1xuICAgIGdyaWQtY29sdW1uOiAxO1xuICAgIGdyaWQtYXJlYTogaW1hZ2U7XG4gIH1cbiAgXG4gICNhZGR0b2NhcnQtcG9wdXAgLnBvcHVwLWNvbnRlbnQgLnByb2R1Y3QtZGV0YWlscyAuaW1hZ2U6YWN0aXZlIHtcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogc2NhbGUoMC45NSk7XG4gICAgICAgICAgICB0cmFuc2Zvcm06IHNjYWxlKDAuOTUpO1xuICB9XG4gIFxuICAjYWRkdG9jYXJ0LXBvcHVwIC5wb3B1cC1jb250ZW50IC5wcm9kdWN0LWRldGFpbHMgLmltYWdlIGltZyB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIC1vLW9iamVjdC1maXQ6IGNvdmVyO1xuICAgICAgIG9iamVjdC1maXQ6IGNvdmVyO1xuICB9XG4gIFxuICAjYWRkdG9jYXJ0LXBvcHVwIC5wb3B1cC1jb250ZW50IC5wcm9kdWN0LWRldGFpbHMgLmRldGFpbHMge1xuICAgIGRpc3BsYXk6IC1tcy1ncmlkO1xuICAgIGRpc3BsYXk6IGdyaWQ7XG4gICAgICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogYXV0byBhdXRvIGF1dG8gMWZyO1xuICAgICAgICBncmlkLXRlbXBsYXRlLXJvd3M6IGF1dG87XG4gICAgZ3JpZC1nYXA6IDEwcHg7XG4gICAgLW1zLWdyaWQtcm93OiAxO1xuICAgIGdyaWQtcm93OiAxO1xuICAgIC1tcy1ncmlkLWNvbHVtbjogMjtcbiAgICBncmlkLWNvbHVtbjogMjtcbiAgICBncmlkLWFyZWE6IGRldGFpbHM7XG4gICAgLXdlYmtpdC1ib3gtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIGdyaWQtdGVtcGxhdGUtYXJlYXM6IFwibmFtZSBuYW1lIG5hbWUgbmFtZVwiIFwiYmFnIGxpbmUgbGlrZSAuXCIgXCJwcmljZSBwcmljZSBwcmljZSBxdWFudGl0eVwiO1xuICB9XG4gIFxuICAjYWRkdG9jYXJ0LXBvcHVwIC5wb3B1cC1jb250ZW50IC5wcm9kdWN0LWRldGFpbHMgLmRldGFpbHMgLm5hbWUge1xuICAgIC1tcy1ncmlkLXJvdzogMTtcbiAgICBncmlkLXJvdzogMTtcbiAgICAtbXMtZ3JpZC1jb2x1bW46IDE7XG4gICAgZ3JpZC1jb2x1bW46IDE7XG4gICAgZ3JpZC1hcmVhOiBuYW1lO1xuICAgIGNvbG9yOiAjMjIyYjQ1O1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgZm9udC1zaXplOiAxNXB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxOHB4O1xuICAgIG1hcmdpbi1ib3R0b206IDA7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcbiAgICBtYXgtaGVpZ2h0OiAzNnB4O1xuICB9XG4gIFxuICAjYWRkdG9jYXJ0LXBvcHVwIC5wb3B1cC1jb250ZW50IC5wcm9kdWN0LWRldGFpbHMgLmRldGFpbHMgLnByaWNlIHtcbiAgICAtbXMtZ3JpZC1yb3c6IDI7XG4gICAgZ3JpZC1yb3c6IDI7XG4gICAgLW1zLWdyaWQtY29sdW1uOiAxO1xuICAgIGdyaWQtY29sdW1uOiAxO1xuICAgIGdyaWQtYXJlYTogcHJpY2U7XG4gICAgZm9udC1zaXplOiAxNXB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxNXB4O1xuICAgIGNvbG9yOiAjZmE2NDAwO1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XG4gICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgfVxuICBcbiAgI2FkZHRvY2FydC1wb3B1cCAucG9wdXAtY29udGVudCAucHJvZHVjdC1kZXRhaWxzIC5kZXRhaWxzIC5iYWcge1xuICAgIC1tcy1ncmlkLXJvdzogMjtcbiAgICBncmlkLXJvdzogMjtcbiAgICAtbXMtZ3JpZC1jb2x1bW46IDE7XG4gICAgZ3JpZC1jb2x1bW46IDE7XG4gICAgZ3JpZC1hcmVhOiBiYWc7XG4gIH1cbiAgXG4gICNhZGR0b2NhcnQtcG9wdXAgLnBvcHVwLWNvbnRlbnQgLnByb2R1Y3QtZGV0YWlscyAuZGV0YWlscyAubGluZSB7XG4gICAgd2lkdGg6IDFweDtcbiAgICBoZWlnaHQ6IDE2cHg7XG4gICAgYmFja2dyb3VuZDogI2UwZTBlMDtcbiAgICAtbXMtZ3JpZC1yb3c6IDI7XG4gICAgZ3JpZC1yb3c6IDI7XG4gICAgLW1zLWdyaWQtY29sdW1uOiAyO1xuICAgIGdyaWQtY29sdW1uOiAyO1xuICAgIGdyaWQtYXJlYTogbGluZTtcbiAgfVxuXG4gICNhZGR0b2NhcnQtcG9wdXAgLnBvcHVwLWNvbnRlbnQgLnByb2R1Y3QtZGV0YWlscyAuZGV0YWlscyAubGlrZSB7XG4gICAgLW1zLWdyaWQtcm93OiAyO1xuICAgIGdyaWQtcm93OiAyO1xuICAgIC1tcy1ncmlkLWNvbHVtbjogMztcbiAgICBncmlkLWNvbHVtbjogMztcbiAgICBncmlkLWFyZWE6IGxpa2U7XG4gIH1cbiAgXG4gICNhZGR0b2NhcnQtcG9wdXAgLnBvcHVwLWNvbnRlbnQgLnByb2R1Y3QtZGV0YWlscyAuZGV0YWlscyAuYmFnLFxuICAjYWRkdG9jYXJ0LXBvcHVwIC5wb3B1cC1jb250ZW50IC5wcm9kdWN0LWRldGFpbHMgLmRldGFpbHMgLmxpa2Uge1xuICAgIGRpc3BsYXk6IC1tcy1ncmlkO1xuICAgIGRpc3BsYXk6IGdyaWQ7XG4gICAgZ3JpZC1hdXRvLWZsb3c6IGNvbHVtbjtcbiAgICBncmlkLWdhcDogNXB4O1xuICAgIC1tcy1ncmlkLWNvbHVtbnM6IG1heC1jb250ZW50IG1heC1jb250ZW50O1xuICAgICAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IC13ZWJraXQtbWF4LWNvbnRlbnQgLXdlYmtpdC1tYXgtY29udGVudDtcbiAgICAgICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiBtYXgtY29udGVudCBtYXgtY29udGVudDtcbiAgICAtd2Via2l0LWJveC1hbGlnbjogY2VudGVyO1xuICAgICAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgfVxuICBcbiAgI2FkZHRvY2FydC1wb3B1cCAucG9wdXAtY29udGVudCAucHJvZHVjdC1kZXRhaWxzIC5kZXRhaWxzIC5iYWcgaW1nLFxuICAjYWRkdG9jYXJ0LXBvcHVwIC5wb3B1cC1jb250ZW50IC5wcm9kdWN0LWRldGFpbHMgLmRldGFpbHMgLmJhZyBzdmcsXG4gICNhZGR0b2NhcnQtcG9wdXAgLnBvcHVwLWNvbnRlbnQgLnByb2R1Y3QtZGV0YWlscyAuZGV0YWlscyAubGlrZSBpbWcsXG4gI2FkZHRvY2FydC1wb3B1cCAucG9wdXAtY29udGVudCAucHJvZHVjdC1kZXRhaWxzIC5kZXRhaWxzIC5saWtlIHN2ZyB7XG4gICAgd2lkdGg6IGF1dG87XG4gICAgaGVpZ2h0OiAxMnB4O1xuICB9XG5cbiNhZGR0b2NhcnQtcG9wdXAgLnBvcHVwLWNvbnRlbnQgLnByb2R1Y3QtZGV0YWlscyAuZGV0YWlscyAuYmFnIGltZyBwYXRoLFxuI2FkZHRvY2FydC1wb3B1cCAucG9wdXAtY29udGVudCAucHJvZHVjdC1kZXRhaWxzIC5kZXRhaWxzIC5iYWcgc3ZnIHBhdGgsXG4jYWRkdG9jYXJ0LXBvcHVwIC5wb3B1cC1jb250ZW50IC5wcm9kdWN0LWRldGFpbHMgLmRldGFpbHMgLmxpa2UgaW1nIHBhdGgsXG4jYWRkdG9jYXJ0LXBvcHVwIC5wb3B1cC1jb250ZW50IC5wcm9kdWN0LWRldGFpbHMgLmRldGFpbHMgLmxpa2Ugc3ZnIHBhdGgge1xuICBmaWxsOiAjOGY5YmIzO1xufVxuXG4jYWRkdG9jYXJ0LXBvcHVwIC5wb3B1cC1jb250ZW50IC5wcm9kdWN0LWRldGFpbHMgLmRldGFpbHMgLmJhZyBzcGFuLFxuI2FkZHRvY2FydC1wb3B1cCAucG9wdXAtY29udGVudCAucHJvZHVjdC1kZXRhaWxzIC5kZXRhaWxzIC5saWtlIHNwYW4ge1xuICBmb250LXNpemU6IDEycHg7XG4gIGxpbmUtaGVpZ2h0OiAxMnB4O1xuICBjb2xvcjogIzhmOWJiMztcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG59XG5cbiNhZGR0b2NhcnQtcG9wdXAgLnBvcHVwLWNvbnRlbnQgLnByb2R1Y3QtZGV0YWlscyAuZGV0YWlscyAucXVhbnRpdHkge1xuICAtbXMtZ3JpZC1yb3c6IDM7XG4gIGdyaWQtcm93OiAzO1xuICAtbXMtZ3JpZC1jb2x1bW46IDQ7XG4gIGdyaWQtY29sdW1uOiA0O1xuICBncmlkLWFyZWE6IHF1YW50aXR5O1xuICBkaXNwbGF5OiAtbXMtZ3JpZDtcbiAgZGlzcGxheTogZ3JpZDtcbiAgZ3JpZC1hdXRvLWZsb3c6IGNvbHVtbjtcbiAgZ3JpZC1nYXA6IDEwcHg7XG4gIC13ZWJraXQtYm94LWFsaWduOiBjZW50ZXI7XG4gICAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xuICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIC1tcy1ncmlkLWNvbHVtbnM6IG1heC1jb250ZW50IG1heC1jb250ZW50IG1heC1jb250ZW50O1xuICAgICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiAtd2Via2l0LW1heC1jb250ZW50IC13ZWJraXQtbWF4LWNvbnRlbnQgLXdlYmtpdC1tYXgtY29udGVudDtcbiAgICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogbWF4LWNvbnRlbnQgbWF4LWNvbnRlbnQgbWF4LWNvbnRlbnQ7XG4gIC13ZWJraXQtYm94LXBhY2s6IHJpZ2h0O1xuICAgICAgLW1zLWZsZXgtcGFjazogcmlnaHQ7XG4gICAgICAgICAganVzdGlmeS1jb250ZW50OiByaWdodDtcbn1cblxuI2FkZHRvY2FydC1wb3B1cCAucG9wdXAtY29udGVudCAucHJvZHVjdC1kZXRhaWxzIC5kZXRhaWxzIC5xdWFudGl0eSAuYnRuIHtcbiAgcGFkZGluZzogMDtcbn1cblxuI2FkZHRvY2FydC1wb3B1cCAucG9wdXAtY29udGVudCAucHJvZHVjdC1kZXRhaWxzIC5kZXRhaWxzIC5xdWFudGl0eSAuYnRuIGltZyxcbiNhZGR0b2NhcnQtcG9wdXAgLnBvcHVwLWNvbnRlbnQgLnByb2R1Y3QtZGV0YWlscyAuZGV0YWlscyAucXVhbnRpdHkgLmJ0biBzdmcge1xuICB3aWR0aDogMzBweDtcbiAgaGVpZ2h0OiBhdXRvO1xufVxuXG4jYWRkdG9jYXJ0LXBvcHVwIC5wb3B1cC1jb250ZW50IC5wcm9kdWN0LWRldGFpbHMgLmRldGFpbHMgLnF1YW50aXR5IC5idG4gaW1nIHJlY3QsXG4jYWRkdG9jYXJ0LXBvcHVwIC5wb3B1cC1jb250ZW50IC5wcm9kdWN0LWRldGFpbHMgLmRldGFpbHMgLnF1YW50aXR5IC5idG4gc3ZnIHJlY3Qge1xuICBmaWxsOiAjZmE2NDAwO1xuICBvcGFjaXR5OiAxO1xufVxuXG4jYWRkdG9jYXJ0LXBvcHVwIC5wb3B1cC1jb250ZW50IC5wcm9kdWN0LWRldGFpbHMgLmRldGFpbHMgLnF1YW50aXR5IC5idG4gaW1nIHBhdGgsXG4jYWRkdG9jYXJ0LXBvcHVwIC5wb3B1cC1jb250ZW50IC5wcm9kdWN0LWRldGFpbHMgLmRldGFpbHMgLnF1YW50aXR5IC5idG4gc3ZnIHBhdGgge1xuICBmaWxsOiAjZmZmZmZmO1xuICBvcGFjaXR5OiAxO1xufVxuXG4jYWRkdG9jYXJ0LXBvcHVwIC5wb3B1cC1jb250ZW50IC5wcm9kdWN0LWRldGFpbHMgLmRldGFpbHMgLnF1YW50aXR5IHNwYW4ge1xuICBwYWRkaW5nOiAwO1xuICBib3JkZXItcmFkaXVzOiAwO1xuICBib3JkZXI6IG5vbmU7XG4gIGZvbnQtc2l6ZTogMTdweDtcbiAgbGluZS1oZWlnaHQ6IDE3cHg7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIG1pbi13aWR0aDogMjBweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBjb2xvcjogIzIyMmI0NTtcbn1cblxuI2FkZHRvY2FydC1wb3B1cCAucG9wdXAtY29udGVudCAuYXR0cmlidXRlLXRpdGxlIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWRmMWY3O1xuICB9XG4gIFxuICAjYWRkdG9jYXJ0LXBvcHVwIC5wb3B1cC1jb250ZW50IC5hdHRyaWJ1dGUtdGl0bGUgcDpudGgtY2hpbGQoMikge1xuICAgIC1tcy1ncmlkLWNvbHVtbi1hbGlnbjogZW5kO1xuICAgICAgICBqdXN0aWZ5LXNlbGY6IGVuZDtcbiAgfVxuICBcbiAgI2FkZHRvY2FydC1wb3B1cCAucG9wdXAtY29udGVudCAuYXR0cmlidXRlcyAuaC1ncmlkIHtcbiAgICBncmlkLWdhcDogMTBweDtcbiAgICAtbXMtZ3JpZC1jb2x1bW5zOiAxZnIgYXV0bztcbiAgICAgICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiAxZnIgYXV0bztcbiAgICAtd2Via2l0LWJveC1hbGlnbjogY2VudGVyO1xuICAgICAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgfVxuICBcbiAgI2FkZHRvY2FydC1wb3B1cCAucG9wdXAtY29udGVudCAuYXR0cmlidXRlcyAucHJpY2Uge1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgY29sb3I6ICMyMjJiNDU7XG4gIH1cblxuICAjYWRkdG9jYXJ0LXBvcHVwIC5wb3B1cC1jb250ZW50IC5hdHRyaWJ1dGVzIC5jaGVja2JveCBsYWJlbCB7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgfVxuICBcbiAgI2FkZHRvY2FydC1wb3B1cCAucG9wdXAtY29udGVudCAuYXR0cmlidXRlcyAuY2hlY2tib3ggbGFiZWwgc3BhbiB7XG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgICBjb2xvcjogIzhmOWJiMztcbiAgfVxuICBcbiAjYWRkdG9jYXJ0LXBvcHVwIC5wb3B1cC1jb250ZW50IC5ub3RlIC50aXRsZSB7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2VkZjFmNztcbiAgICBwYWRkaW5nOiAwO1xuICB9XG4gIFxuICAjYWRkdG9jYXJ0LXBvcHVwIC5wb3B1cC1jb250ZW50IC5ub3RlIHRleHRhcmVhIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRjdGOUZDO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogODBweDtcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XG4gICAgcmVzaXplOiBub25lO1xuICB9XG4gIFxuICNhZGR0b2NhcnQtcG9wdXAgLnBvcHVwLWNvbnRlbnQgLmJpZy1jYXJ0LWJ0biB7XG4gICAgZGlzcGxheTogLW1zLWdyaWQ7XG4gICAgZGlzcGxheTogZ3JpZDtcbiAgICAtbXMtZ3JpZC1jb2x1bW5zOiBhdXRvIDFmcjtcbiAgICAgICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiBhdXRvIDFmcjtcbiAgICAtbXMtZ3JpZC1yb3dzOiBhdXRvO1xuICAgICAgICBncmlkLXRlbXBsYXRlLXJvd3M6IGF1dG87XG4gICAgZ3JpZC1nYXA6IDE1cHg7XG4gICAgLXdlYmtpdC1ib3gtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgcGFkZGluZzogMjBweCAzMHB4O1xuICAgICAgICBncmlkLXRlbXBsYXRlLWFyZWFzOiBcIml0ZW1zIHRleHRcIiBcInByaWNlIHRleHRcIjtcbiAgfVxuXG4gICNhZGR0b2NhcnQtcG9wdXAgLnBvcHVwLWNvbnRlbnQgLmJpZy1jYXJ0LWJ0biAuaXRlbXMge1xuICAgIC1tcy1ncmlkLXJvdzogMztcbiAgICBncmlkLXJvdzogMztcbiAgICAtbXMtZ3JpZC1jb2x1bW46IDE7XG4gICAgZ3JpZC1jb2x1bW46IDE7XG4gICAgZ3JpZC1hcmVhOiBpdGVtcztcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgbGluZS1oZWlnaHQ6IDEycHg7XG4gICAgY29sb3I6ICNmZmZmZmY7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgfVxuICBcbiAgICNhZGR0b2NhcnQtcG9wdXAgLnBvcHVwLWNvbnRlbnQgLmJpZy1jYXJ0LWJ0biAucHJpY2Uge1xuICAgIC1tcy1ncmlkLXJvdzogMjtcbiAgICBncmlkLXJvdzogMjtcbiAgICAtbXMtZ3JpZC1jb2x1bW46IDE7XG4gICAgZ3JpZC1jb2x1bW46IDE7XG4gICAgZ3JpZC1hcmVhOiBwcmljZTtcbiAgICBmb250LXNpemU6IDE3cHg7XG4gICAgbGluZS1oZWlnaHQ6IDE3cHg7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBjb2xvcjogI2ZmZmZmZjtcbiAgfVxuICBcbiAjYWRkdG9jYXJ0LXBvcHVwIC5wb3B1cC1jb250ZW50IC5iaWctY2FydC1idG4gLnRleHQge1xuICAgIC1tcy1ncmlkLXJvdzogMTtcbiAgICBncmlkLXJvdzogMTtcbiAgICAtbXMtZ3JpZC1yb3ctc3BhbjogMjtcbiAgICAtbXMtZ3JpZC1jb2x1bW46IDI7XG4gICAgZ3JpZC1jb2x1bW46IDI7XG4gICAgZ3JpZC1hcmVhOiB0ZXh0O1xuICAgIGZvbnQtc2l6ZTogMTdweDtcbiAgICBsaW5lLWhlaWdodDogMTdweDtcbiAgICBjb2xvcjogI2ZmZmZmZjtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIC1tcy1ncmlkLWNvbHVtbi1hbGlnbjogZW5kO1xuICAgICAgICBqdXN0aWZ5LXNlbGY6IGVuZDtcbiAgfVxuICBcbiAgI2FkZHRvY2FydC1wb3B1cC5hY3RpdmUge1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgb3BhY2l0eTogMTtcbiAgICB2aXNpYmlsaXR5OiB2aXNpYmxlO1xuICB9XG4gIFxuICNhZGR0b2NhcnQtcG9wdXAuYWN0aXZlIC5wb3B1cC1jb250ZW50IHtcbiAgICBib3R0b206IDA7XG4gICAgb3BhY2l0eTogMTtcbiAgfVxuICAiXX0= */";
      /***/
    },

    /***/
    "./src/app/shared/add-to-cart/add-to-cart.component.ts":
    /*!*************************************************************!*\
      !*** ./src/app/shared/add-to-cart/add-to-cart.component.ts ***!
      \*************************************************************/

    /*! exports provided: AddToCartComponent */

    /***/
    function srcAppSharedAddToCartAddToCartComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AddToCartComponent", function () {
        return AddToCartComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ngrx_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ngrx/store */
      "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");
      /* harmony import */


      var subsink__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! subsink */
      "./node_modules/subsink/dist/es2015/index.js");
      /* harmony import */


      var _user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../../user/store/actions/user.action */
      "./src/app/user/store/actions/user.action.ts");

      var AddToCartComponent = /*#__PURE__*/function () {
        function AddToCartComponent(store) {
          _classCallCheck(this, AddToCartComponent);

          this.store = store;
          this.closeAddToCartModalEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
          this.subSink = new subsink__WEBPACK_IMPORTED_MODULE_4__["SubSink"]();
        }

        _createClass(AddToCartComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this14 = this;

            if (this.userId) {
              this.initForm();
              this.currentPrice = this.product.cost || 0; //? Pull the current items in cart to be displayed

              this.cartItems$ = this.store.select(function (data) {
                return data.User.CartItems;
              }); //? Get user data from the store

              this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetUserDataInitiatedAction({
                payload: {
                  userId: this.userId
                }
              }));
              this.userData$ = this.store.select(function (data) {
                return data.User.UserData;
              });
              this.subSink.sink = this.userData$.subscribe(function (data) {
                if (data === null || data === void 0 ? void 0 : data.userId) {
                  _this14.initForm(data);
                }
              });
            }
          }
        }, {
          key: "closeAddToCartModal",
          value: function closeAddToCartModal() {
            this.closeAddToCartModalEvent.emit(true);
          }
        }, {
          key: "subtractItemCount",
          value: function subtractItemCount() {
            this.product.quantity = this.product.quantity > 1 ? this.product.quantity -= 1 : 1;
            this.product.cost = this.product.cost > this.currentPrice ? this.product.cost -= this.currentPrice : this.product.cost;
          }
        }, {
          key: "addItemCount",
          value: function addItemCount() {
            this.product.quantity += 1;
            this.product.cost = this.currentPrice * this.product.quantity;
          }
        }, {
          key: "initForm",
          value: function initForm(userData) {
            var _a;

            this.addDeliveryAddressForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
              Address: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](((_a = userData === null || userData === void 0 ? void 0 : userData.person) === null || _a === void 0 ? void 0 : _a.address) || null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]))
            });
          }
        }, {
          key: "onSubmit",
          value: function onSubmit() {
            if (this.addDeliveryAddressForm.invalid) {
              return;
            }

            var Address = this.addDeliveryAddressForm.value.Address;
            this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].AddProductToCartInitiatedAction({
              payload: {
                deliveryAddress: Address,
                userId: this.userId,
                product: Object.assign({}, this.product)
              }
            }));
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            this.subSink.unsubscribe();
          }
        }]);

        return AddToCartComponent;
      }();

      AddToCartComponent.ctorParameters = function () {
        return [{
          type: _ngrx_store__WEBPACK_IMPORTED_MODULE_3__["Store"]
        }];
      };

      AddToCartComponent.propDecorators = {
        closeAddToCartModalEvent: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
        }],
        show: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        product: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        productImages: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        userId: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }]
      };
      AddToCartComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-add-to-cart',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./add-to-cart.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/add-to-cart/add-to-cart.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./add-to-cart.component.scss */
        "./src/app/shared/add-to-cart/add-to-cart.component.scss"))["default"]]
      })], AddToCartComponent);
      /***/
    },

    /***/
    "./src/app/shared/back-button.directive.ts":
    /*!*************************************************!*\
      !*** ./src/app/shared/back-button.directive.ts ***!
      \*************************************************/

    /*! exports provided: BackButtonDirective */

    /***/
    function srcAppSharedBackButtonDirectiveTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "BackButtonDirective", function () {
        return BackButtonDirective;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _services_navigation_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../services/navigation.service */
      "./src/app/services/navigation.service.ts");

      var BackButtonDirective = /*#__PURE__*/function () {
        function BackButtonDirective(navigation) {
          _classCallCheck(this, BackButtonDirective);

          this.navigation = navigation;
        }

        _createClass(BackButtonDirective, [{
          key: "onClick",
          value: function onClick() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee37() {
              return regeneratorRuntime.wrap(function _callee37$(_context37) {
                while (1) {
                  switch (_context37.prev = _context37.next) {
                    case 0:
                      _context37.next = 2;
                      return this.navigation.goBackOnNative();

                    case 2:
                    case "end":
                      return _context37.stop();
                  }
                }
              }, _callee37, this);
            }));
          }
        }]);

        return BackButtonDirective;
      }();

      BackButtonDirective.ctorParameters = function () {
        return [{
          type: _services_navigation_service__WEBPACK_IMPORTED_MODULE_2__["NavigationService"]
        }];
      };

      BackButtonDirective.propDecorators = {
        onClick: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"],
          args: ["click"]
        }]
      };
      BackButtonDirective = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({
        selector: "[backButton]"
      })], BackButtonDirective);
      /***/
    },

    /***/
    "./src/app/shared/capitalize.pipe.ts":
    /*!*******************************************!*\
      !*** ./src/app/shared/capitalize.pipe.ts ***!
      \*******************************************/

    /*! exports provided: CapitalizePipe */

    /***/
    function srcAppSharedCapitalizePipeTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CapitalizePipe", function () {
        return CapitalizePipe;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../../utils/functions/app.functions */
      "./src/utils/functions/app.functions.ts");

      var CapitalizePipe = /*#__PURE__*/function () {
        function CapitalizePipe() {
          _classCallCheck(this, CapitalizePipe);
        }

        _createClass(CapitalizePipe, [{
          key: "transform",
          value: function transform(value) {
            if (value) {
              var valueToArray = value.split(".").map(function (data) {
                return data.trim();
              });

              if ((valueToArray === null || valueToArray === void 0 ? void 0 : valueToArray.length) > 1) {
                var capitalizedStringArray = [];

                var _iterator = _createForOfIteratorHelper(valueToArray),
                    _step;

                try {
                  for (_iterator.s(); !(_step = _iterator.n()).done;) {
                    var st = _step.value;
                    var capitalizedString = Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_2__["capitalizeFirstCharacter"])(st);
                    capitalizedStringArray.push(capitalizedString);
                  }
                } catch (err) {
                  _iterator.e(err);
                } finally {
                  _iterator.f();
                }

                return capitalizedStringArray.join(". ").trim();
              }

              return Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_2__["capitalizeFirstCharacter"])(value);
            }
          }
        }]);

        return CapitalizePipe;
      }();

      CapitalizePipe = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
        name: 'capitalize'
      })], CapitalizePipe);
      /***/
    },

    /***/
    "./src/app/shared/cart-badge/cart-badge.component.scss":
    /*!*************************************************************!*\
      !*** ./src/app/shared/cart-badge/cart-badge.component.scss ***!
      \*************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppSharedCartBadgeCartBadgeComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "main {\n  width: 20%;\n  z-index: 99;\n  position: fixed;\n  top: 0;\n  right: 0;\n  padding: 15px 20px;\n}\nmain p {\n  background: #DEDEDE;\n  box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06);\n  width: 50px;\n  height: 50px;\n  padding: 13px 8px 8px 8px;\n  border-radius: 100%;\n}\nmain p:hover {\n  background: whitesmoke;\n}\nmain .badge {\n  padding-left: 9px;\n  padding-right: 9px;\n  border-radius: 9px;\n}\nmain .label-warning[href],\nmain .badge-warning[href] {\n  background-color: #c67605;\n}\nmain #lblCartCount {\n  font-size: 12px;\n  background: #FAA078;\n  color: #000;\n  padding: 2px 5px;\n  vertical-align: top;\n  margin-left: -10px;\n}\nmain .cart-icon {\n  font-size: 24px;\n  color: #000 !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL2NhcnQtYmFkZ2UvY2FydC1iYWRnZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFVBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLE1BQUE7RUFDQSxRQUFBO0VBQ0Esa0JBQUE7QUFDSjtBQUNJO0VBQ0ksbUJBQUE7RUFDQSwyRUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EseUJBQUE7RUFDQSxtQkFBQTtBQUNSO0FBRUk7RUFDSSxzQkFBQTtBQUFSO0FBR0k7RUFDSSxpQkFBQTtFQUNBLGtCQUFBO0VBR0Esa0JBQUE7QUFEUjtBQUlJOztFQUVJLHlCQUFBO0FBRlI7QUFLSTtFQUNJLGVBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7QUFIUjtBQU1JO0VBQ0ksZUFBQTtFQUNBLHNCQUFBO0FBSlIiLCJmaWxlIjoic3JjL2FwcC9zaGFyZWQvY2FydC1iYWRnZS9jYXJ0LWJhZGdlLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsibWFpbiB7XG4gICAgd2lkdGg6IDIwJTtcbiAgICB6LWluZGV4OiA5OTtcbiAgICBwb3NpdGlvbjogZml4ZWQ7XG4gICAgdG9wOiAwO1xuICAgIHJpZ2h0OiAwO1xuICAgIHBhZGRpbmc6IDE1cHggMjBweDtcblxuICAgIHAge1xuICAgICAgICBiYWNrZ3JvdW5kOiAjREVERURFO1xuICAgICAgICBib3gtc2hhZG93OiAwIDFweCAzcHggMCByZ2JhKDAsIDAsIDAsIDAuMSksIDAgMXB4IDJweCAwIHJnYmEoMCwgMCwgMCwgMC4wNik7XG4gICAgICAgIHdpZHRoOiA1MHB4O1xuICAgICAgICBoZWlnaHQ6IDUwcHg7XG4gICAgICAgIHBhZGRpbmc6IDEzcHggOHB4IDhweCA4cHg7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDEwMCU7XG4gICAgfVxuXG4gICAgcDpob3ZlciB7XG4gICAgICAgIGJhY2tncm91bmQ6IHdoaXRlc21va2U7XG4gICAgfVxuXG4gICAgLmJhZGdlIHtcbiAgICAgICAgcGFkZGluZy1sZWZ0OiA5cHg7XG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDlweDtcbiAgICAgICAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiA5cHg7XG4gICAgICAgIC1tb3otYm9yZGVyLXJhZGl1czogOXB4O1xuICAgICAgICBib3JkZXItcmFkaXVzOiA5cHg7XG4gICAgfVxuICAgICAgXG4gICAgLmxhYmVsLXdhcm5pbmdbaHJlZl0sXG4gICAgLmJhZGdlLXdhcm5pbmdbaHJlZl0ge1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjYzY3NjA1O1xuICAgIH1cbiAgICBcbiAgICAjbGJsQ2FydENvdW50IHtcbiAgICAgICAgZm9udC1zaXplOiAxMnB4O1xuICAgICAgICBiYWNrZ3JvdW5kOiAjRkFBMDc4O1xuICAgICAgICBjb2xvcjogIzAwMDtcbiAgICAgICAgcGFkZGluZzogMnB4IDVweDtcbiAgICAgICAgdmVydGljYWwtYWxpZ246IHRvcDtcbiAgICAgICAgbWFyZ2luLWxlZnQ6IC0xMHB4OyBcbiAgICB9XG4gICAgXG4gICAgLmNhcnQtaWNvbiB7XG4gICAgICAgIGZvbnQtc2l6ZTogMjRweDtcbiAgICAgICAgY29sb3I6ICMwMDAgIWltcG9ydGFudDtcbiAgICB9XG59Il19 */";
      /***/
    },

    /***/
    "./src/app/shared/cart-badge/cart-badge.component.ts":
    /*!***********************************************************!*\
      !*** ./src/app/shared/cart-badge/cart-badge.component.ts ***!
      \***********************************************************/

    /*! exports provided: CartBadgeComponent */

    /***/
    function srcAppSharedCartBadgeCartBadgeComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CartBadgeComponent", function () {
        return CartBadgeComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var CartBadgeComponent = /*#__PURE__*/function () {
        function CartBadgeComponent() {
          _classCallCheck(this, CartBadgeComponent);
        }

        _createClass(CartBadgeComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return CartBadgeComponent;
      }();

      CartBadgeComponent.ctorParameters = function () {
        return [];
      };

      CartBadgeComponent.propDecorators = {
        itemCount: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }]
      };
      CartBadgeComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-cart-badge",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./cart-badge.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/cart-badge/cart-badge.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./cart-badge.component.scss */
        "./src/app/shared/cart-badge/cart-badge.component.scss"))["default"]]
      })], CartBadgeComponent);
      /***/
    },

    /***/
    "./src/app/shared/category-hightlight-widget/category-hightlight-widget.component.scss":
    /*!*********************************************************************************************!*\
      !*** ./src/app/shared/category-hightlight-widget/category-hightlight-widget.component.scss ***!
      \*********************************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppSharedCategoryHightlightWidgetCategoryHightlightWidgetComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9jYXRlZ29yeS1oaWdodGxpZ2h0LXdpZGdldC9jYXRlZ29yeS1oaWdodGxpZ2h0LXdpZGdldC5jb21wb25lbnQuc2NzcyJ9 */";
      /***/
    },

    /***/
    "./src/app/shared/category-hightlight-widget/category-hightlight-widget.component.ts":
    /*!*******************************************************************************************!*\
      !*** ./src/app/shared/category-hightlight-widget/category-hightlight-widget.component.ts ***!
      \*******************************************************************************************/

    /*! exports provided: CategoryHightlightWidgetComponent */

    /***/
    function srcAppSharedCategoryHightlightWidgetCategoryHightlightWidgetComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CategoryHightlightWidgetComponent", function () {
        return CategoryHightlightWidgetComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var CategoryHightlightWidgetComponent = /*#__PURE__*/function () {
        function CategoryHightlightWidgetComponent() {
          _classCallCheck(this, CategoryHightlightWidgetComponent);
        }

        _createClass(CategoryHightlightWidgetComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return CategoryHightlightWidgetComponent;
      }();

      CategoryHightlightWidgetComponent.ctorParameters = function () {
        return [];
      };

      CategoryHightlightWidgetComponent.propDecorators = {
        productCategory: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }]
      };
      CategoryHightlightWidgetComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-category-hightlight-widget',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./category-hightlight-widget.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/category-hightlight-widget/category-hightlight-widget.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./category-hightlight-widget.component.scss */
        "./src/app/shared/category-hightlight-widget/category-hightlight-widget.component.scss"))["default"]]
      })], CategoryHightlightWidgetComponent);
      /***/
    },

    /***/
    "./src/app/shared/category-item-widget/category-item-widget.component.scss":
    /*!*********************************************************************************!*\
      !*** ./src/app/shared/category-item-widget/category-item-widget.component.scss ***!
      \*********************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppSharedCategoryItemWidgetCategoryItemWidgetComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9jYXRlZ29yeS1pdGVtLXdpZGdldC9jYXRlZ29yeS1pdGVtLXdpZGdldC5jb21wb25lbnQuc2NzcyJ9 */";
      /***/
    },

    /***/
    "./src/app/shared/category-item-widget/category-item-widget.component.ts":
    /*!*******************************************************************************!*\
      !*** ./src/app/shared/category-item-widget/category-item-widget.component.ts ***!
      \*******************************************************************************/

    /*! exports provided: CategoryItemWidgetComponent */

    /***/
    function srcAppSharedCategoryItemWidgetCategoryItemWidgetComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CategoryItemWidgetComponent", function () {
        return CategoryItemWidgetComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var CategoryItemWidgetComponent = /*#__PURE__*/function () {
        function CategoryItemWidgetComponent() {
          _classCallCheck(this, CategoryItemWidgetComponent);
        }

        _createClass(CategoryItemWidgetComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return CategoryItemWidgetComponent;
      }();

      CategoryItemWidgetComponent.ctorParameters = function () {
        return [];
      };

      CategoryItemWidgetComponent.propDecorators = {
        productCategory: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }]
      };
      CategoryItemWidgetComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-category-item-widget',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./category-item-widget.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/category-item-widget/category-item-widget.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./category-item-widget.component.scss */
        "./src/app/shared/category-item-widget/category-item-widget.component.scss"))["default"]]
      })], CategoryItemWidgetComponent);
      /***/
    },

    /***/
    "./src/app/shared/clickable-mesage/clickable-mesage.component.scss":
    /*!*************************************************************************!*\
      !*** ./src/app/shared/clickable-mesage/clickable-mesage.component.scss ***!
      \*************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppSharedClickableMesageClickableMesageComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9jbGlja2FibGUtbWVzYWdlL2NsaWNrYWJsZS1tZXNhZ2UuY29tcG9uZW50LnNjc3MifQ== */";
      /***/
    },

    /***/
    "./src/app/shared/clickable-mesage/clickable-mesage.component.ts":
    /*!***********************************************************************!*\
      !*** ./src/app/shared/clickable-mesage/clickable-mesage.component.ts ***!
      \***********************************************************************/

    /*! exports provided: ClickableMesageComponent */

    /***/
    function srcAppSharedClickableMesageClickableMesageComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ClickableMesageComponent", function () {
        return ClickableMesageComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _user_store_model_user_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../../user/store/model/user.model */
      "./src/app/user/store/model/user.model.ts");

      var ClickableMesageComponent = /*#__PURE__*/function () {
        function ClickableMesageComponent(toastController, router) {
          _classCallCheck(this, ClickableMesageComponent);

          this.toastController = toastController;
          this.router = router;
        }

        _createClass(ClickableMesageComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee38() {
              return regeneratorRuntime.wrap(function _callee38$(_context38) {
                while (1) {
                  switch (_context38.prev = _context38.next) {
                    case 0:
                      _context38.next = 2;
                      return this.presentToastWithOptions();

                    case 2:
                    case "end":
                      return _context38.stop();
                  }
                }
              }, _callee38, this);
            }));
          }
        }, {
          key: "presentToastWithOptions",
          value: function presentToastWithOptions() {
            var _a;

            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee39() {
              var _this15 = this;

              var toast;
              return regeneratorRuntime.wrap(function _callee39$(_context39) {
                while (1) {
                  switch (_context39.prev = _context39.next) {
                    case 0:
                      _context39.next = 2;
                      return this.toastController.create({
                        message: this.message.Message,
                        position: 'top',
                        duration: 15000,
                        buttons: [{
                          text: (_a = this.message) === null || _a === void 0 ? void 0 : _a.Label,
                          role: 'cancel',
                          handler: function handler() {
                            if (_this15.message.Type === _user_store_model_user_model__WEBPACK_IMPORTED_MODULE_4__["MessageActionType"].CART_VERIFIED) {
                              _this15.router.navigate(["/user", "product-cart"]);
                            }

                            if (_this15.message.Type === _user_store_model_user_model__WEBPACK_IMPORTED_MODULE_4__["MessageActionType"].RIDER_ASSIGNED_TO_CART) {
                              _this15.router.navigate(["/rider", "orders"]);
                            }
                          }
                        }]
                      });

                    case 2:
                      toast = _context39.sent;
                      toast.present();

                    case 4:
                    case "end":
                      return _context39.stop();
                  }
                }
              }, _callee39, this);
            }));
          }
        }]);

        return ClickableMesageComponent;
      }();

      ClickableMesageComponent.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
        }];
      };

      ClickableMesageComponent.propDecorators = {
        message: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }]
      };
      ClickableMesageComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-clickable-mesage',
        template: "",
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./clickable-mesage.component.scss */
        "./src/app/shared/clickable-mesage/clickable-mesage.component.scss"))["default"]]
      })], ClickableMesageComponent);
      /***/
    },

    /***/
    "./src/app/shared/close-modal.directive.ts":
    /*!*************************************************!*\
      !*** ./src/app/shared/close-modal.directive.ts ***!
      \*************************************************/

    /*! exports provided: CloseModalDirective */

    /***/
    function srcAppSharedCloseModalDirectiveTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CloseModalDirective", function () {
        return CloseModalDirective;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");

      var CloseModalDirective = /*#__PURE__*/function () {
        function CloseModalDirective(modalCtrl) {
          _classCallCheck(this, CloseModalDirective);

          this.modalCtrl = modalCtrl;
        }

        _createClass(CloseModalDirective, [{
          key: "onClick",
          value: function onClick() {
            // using the injected ModalController this page
            // can "dismiss" itself and optionally pass back data
            this.modalCtrl.dismiss({
              'dismissed': true
            });
          }
        }]);

        return CloseModalDirective;
      }();

      CloseModalDirective.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
        }];
      };

      CloseModalDirective.propDecorators = {
        onClick: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"],
          args: ["click"]
        }]
      };
      CloseModalDirective = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({
        selector: '[appCloseModal]'
      })], CloseModalDirective);
      /***/
    },

    /***/
    "./src/app/shared/conditional-bottom-navbar/conditional-bottom-navbar.component.scss":
    /*!*******************************************************************************************!*\
      !*** ./src/app/shared/conditional-bottom-navbar/conditional-bottom-navbar.component.scss ***!
      \*******************************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppSharedConditionalBottomNavbarConditionalBottomNavbarComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9jb25kaXRpb25hbC1ib3R0b20tbmF2YmFyL2NvbmRpdGlvbmFsLWJvdHRvbS1uYXZiYXIuY29tcG9uZW50LnNjc3MifQ== */";
      /***/
    },

    /***/
    "./src/app/shared/conditional-bottom-navbar/conditional-bottom-navbar.component.ts":
    /*!*****************************************************************************************!*\
      !*** ./src/app/shared/conditional-bottom-navbar/conditional-bottom-navbar.component.ts ***!
      \*****************************************************************************************/

    /*! exports provided: ConditionalBottomNavbarComponent */

    /***/
    function srcAppSharedConditionalBottomNavbarConditionalBottomNavbarComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ConditionalBottomNavbarComponent", function () {
        return ConditionalBottomNavbarComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../../../utils/types/app.constant */
      "./src/utils/types/app.constant.ts");

      var ConditionalBottomNavbarComponent = /*#__PURE__*/function () {
        function ConditionalBottomNavbarComponent() {
          _classCallCheck(this, ConditionalBottomNavbarComponent);

          this.appRole = _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_2__["Zero30Role"];
        }

        _createClass(ConditionalBottomNavbarComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            unBundleSVG();
          }
        }]);

        return ConditionalBottomNavbarComponent;
      }();

      ConditionalBottomNavbarComponent.ctorParameters = function () {
        return [];
      };

      ConditionalBottomNavbarComponent.propDecorators = {
        userData: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }]
      };
      ConditionalBottomNavbarComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-conditional-bottom-navbar",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./conditional-bottom-navbar.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/conditional-bottom-navbar/conditional-bottom-navbar.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./conditional-bottom-navbar.component.scss */
        "./src/app/shared/conditional-bottom-navbar/conditional-bottom-navbar.component.scss"))["default"]]
      })], ConditionalBottomNavbarComponent);
      /***/
    },

    /***/
    "./src/app/shared/error-message/error-message.component.scss":
    /*!*******************************************************************!*\
      !*** ./src/app/shared/error-message/error-message.component.scss ***!
      \*******************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppSharedErrorMessageErrorMessageComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9lcnJvci1tZXNzYWdlL2Vycm9yLW1lc3NhZ2UuY29tcG9uZW50LnNjc3MifQ== */";
      /***/
    },

    /***/
    "./src/app/shared/error-message/error-message.component.ts":
    /*!*****************************************************************!*\
      !*** ./src/app/shared/error-message/error-message.component.ts ***!
      \*****************************************************************/

    /*! exports provided: ErrorMessageComponent */

    /***/
    function srcAppSharedErrorMessageErrorMessageComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ErrorMessageComponent", function () {
        return ErrorMessageComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");

      var ErrorMessageComponent = /*#__PURE__*/function () {
        function ErrorMessageComponent(toastController) {
          _classCallCheck(this, ErrorMessageComponent);

          this.toastController = toastController;
          this.closeAlert = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        }

        _createClass(ErrorMessageComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _a;

            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee40() {
              return regeneratorRuntime.wrap(function _callee40$(_context40) {
                while (1) {
                  switch (_context40.prev = _context40.next) {
                    case 0:
                      if (!((_a = this.error) === null || _a === void 0 ? void 0 : _a.message)) {
                        if (this.error["error"]) {
                          this.error = this.error["error"];
                        } else {
                          this.error = new Error("An error occured");
                        }
                      }

                      _context40.next = 3;
                      return this.presentToastWithOptions();

                    case 3:
                    case "end":
                      return _context40.stop();
                  }
                }
              }, _callee40, this);
            }));
          }
        }, {
          key: "presentToast",
          value: function presentToast() {
            var _a;

            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee41() {
              var toast;
              return regeneratorRuntime.wrap(function _callee41$(_context41) {
                while (1) {
                  switch (_context41.prev = _context41.next) {
                    case 0:
                      _context41.next = 2;
                      return this.toastController.create({
                        message: (_a = this.error) === null || _a === void 0 ? void 0 : _a.message,
                        duration: 5000
                      });

                    case 2:
                      toast = _context41.sent;
                      toast.present();

                    case 4:
                    case "end":
                      return _context41.stop();
                  }
                }
              }, _callee41, this);
            }));
          }
        }, {
          key: "presentToastWithOptions",
          value: function presentToastWithOptions() {
            var _a;

            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee42() {
              var _this16 = this;

              var toast;
              return regeneratorRuntime.wrap(function _callee42$(_context42) {
                while (1) {
                  switch (_context42.prev = _context42.next) {
                    case 0:
                      _context42.next = 2;
                      return this.toastController.create({
                        message: (_a = this.error) === null || _a === void 0 ? void 0 : _a.message,
                        duration: 5000,
                        position: 'top',
                        color: "danger",
                        buttons: [{
                          text: 'Close',
                          role: 'cancel',
                          handler: function handler() {
                            _this16.closeAlert.emit(true);
                          }
                        }]
                      });

                    case 2:
                      toast = _context42.sent;
                      toast.present();

                    case 4:
                    case "end":
                      return _context42.stop();
                  }
                }
              }, _callee42, this);
            }));
          }
        }]);

        return ErrorMessageComponent;
      }();

      ErrorMessageComponent.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]
        }];
      };

      ErrorMessageComponent.propDecorators = {
        error: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        closeAlert: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
        }]
      };
      ErrorMessageComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-error-message',
        template: "",
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./error-message.component.scss */
        "./src/app/shared/error-message/error-message.component.scss"))["default"]]
      })], ErrorMessageComponent);
      /***/
    },

    /***/
    "./src/app/shared/favorite-food-widget/favorite-food-widget.component.scss":
    /*!*********************************************************************************!*\
      !*** ./src/app/shared/favorite-food-widget/favorite-food-widget.component.scss ***!
      \*********************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppSharedFavoriteFoodWidgetFavoriteFoodWidgetComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9mYXZvcml0ZS1mb29kLXdpZGdldC9mYXZvcml0ZS1mb29kLXdpZGdldC5jb21wb25lbnQuc2NzcyJ9 */";
      /***/
    },

    /***/
    "./src/app/shared/favorite-food-widget/favorite-food-widget.component.ts":
    /*!*******************************************************************************!*\
      !*** ./src/app/shared/favorite-food-widget/favorite-food-widget.component.ts ***!
      \*******************************************************************************/

    /*! exports provided: FavoriteFoodWidgetComponent */

    /***/
    function srcAppSharedFavoriteFoodWidgetFavoriteFoodWidgetComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "FavoriteFoodWidgetComponent", function () {
        return FavoriteFoodWidgetComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var FavoriteFoodWidgetComponent = /*#__PURE__*/function () {
        function FavoriteFoodWidgetComponent() {
          _classCallCheck(this, FavoriteFoodWidgetComponent);

          this.viewProductDetailEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        }

        _createClass(FavoriteFoodWidgetComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            unBundleSVG();
          }
        }, {
          key: "openProductDetail",
          value: function openProductDetail() {
            this.viewProductDetailEvent.emit(true);
          }
        }]);

        return FavoriteFoodWidgetComponent;
      }();

      FavoriteFoodWidgetComponent.ctorParameters = function () {
        return [];
      };

      FavoriteFoodWidgetComponent.propDecorators = {
        product: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        viewProductDetailEvent: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
        }]
      };
      FavoriteFoodWidgetComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-favorite-food-widget',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./favorite-food-widget.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/favorite-food-widget/favorite-food-widget.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./favorite-food-widget.component.scss */
        "./src/app/shared/favorite-food-widget/favorite-food-widget.component.scss"))["default"]]
      })], FavoriteFoodWidgetComponent);
      /***/
    },

    /***/
    "./src/app/shared/favorite-resturant-widget/favorite-resturant-widget.component.scss":
    /*!*******************************************************************************************!*\
      !*** ./src/app/shared/favorite-resturant-widget/favorite-resturant-widget.component.scss ***!
      \*******************************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppSharedFavoriteResturantWidgetFavoriteResturantWidgetComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9mYXZvcml0ZS1yZXN0dXJhbnQtd2lkZ2V0L2Zhdm9yaXRlLXJlc3R1cmFudC13aWRnZXQuY29tcG9uZW50LnNjc3MifQ== */";
      /***/
    },

    /***/
    "./src/app/shared/favorite-resturant-widget/favorite-resturant-widget.component.ts":
    /*!*****************************************************************************************!*\
      !*** ./src/app/shared/favorite-resturant-widget/favorite-resturant-widget.component.ts ***!
      \*****************************************************************************************/

    /*! exports provided: FavoriteResturantWidgetComponent */

    /***/
    function srcAppSharedFavoriteResturantWidgetFavoriteResturantWidgetComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "FavoriteResturantWidgetComponent", function () {
        return FavoriteResturantWidgetComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var FavoriteResturantWidgetComponent = /*#__PURE__*/function () {
        function FavoriteResturantWidgetComponent() {
          _classCallCheck(this, FavoriteResturantWidgetComponent);

          this.addToBookmark = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        }

        _createClass(FavoriteResturantWidgetComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            unBundleSVG();
          }
        }, {
          key: "addBookmark",
          value: function addBookmark(merchant) {
            this.addToBookmark.emit(merchant);
          }
        }]);

        return FavoriteResturantWidgetComponent;
      }();

      FavoriteResturantWidgetComponent.ctorParameters = function () {
        return [];
      };

      FavoriteResturantWidgetComponent.propDecorators = {
        merchant: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        addToBookmark: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
        }]
      };
      FavoriteResturantWidgetComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-favorite-resturant-widget',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./favorite-resturant-widget.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/favorite-resturant-widget/favorite-resturant-widget.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./favorite-resturant-widget.component.scss */
        "./src/app/shared/favorite-resturant-widget/favorite-resturant-widget.component.scss"))["default"]]
      })], FavoriteResturantWidgetComponent);
      /***/
    },

    /***/
    "./src/app/shared/favourite-merchant-hightlight-widget/favourite-merchant-hightlight-widget.component.scss":
    /*!*****************************************************************************************************************!*\
      !*** ./src/app/shared/favourite-merchant-hightlight-widget/favourite-merchant-hightlight-widget.component.scss ***!
      \*****************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppSharedFavouriteMerchantHightlightWidgetFavouriteMerchantHightlightWidgetComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9mYXZvdXJpdGUtbWVyY2hhbnQtaGlnaHRsaWdodC13aWRnZXQvZmF2b3VyaXRlLW1lcmNoYW50LWhpZ2h0bGlnaHQtd2lkZ2V0LmNvbXBvbmVudC5zY3NzIn0= */";
      /***/
    },

    /***/
    "./src/app/shared/favourite-merchant-hightlight-widget/favourite-merchant-hightlight-widget.component.ts":
    /*!***************************************************************************************************************!*\
      !*** ./src/app/shared/favourite-merchant-hightlight-widget/favourite-merchant-hightlight-widget.component.ts ***!
      \***************************************************************************************************************/

    /*! exports provided: FavouriteMerchantHightlightWidgetComponent */

    /***/
    function srcAppSharedFavouriteMerchantHightlightWidgetFavouriteMerchantHightlightWidgetComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "FavouriteMerchantHightlightWidgetComponent", function () {
        return FavouriteMerchantHightlightWidgetComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var FavouriteMerchantHightlightWidgetComponent = /*#__PURE__*/function () {
        function FavouriteMerchantHightlightWidgetComponent() {
          _classCallCheck(this, FavouriteMerchantHightlightWidgetComponent);
        }

        _createClass(FavouriteMerchantHightlightWidgetComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return FavouriteMerchantHightlightWidgetComponent;
      }();

      FavouriteMerchantHightlightWidgetComponent.ctorParameters = function () {
        return [];
      };

      FavouriteMerchantHightlightWidgetComponent.propDecorators = {
        favouriteMerchant: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }]
      };
      FavouriteMerchantHightlightWidgetComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-favourite-merchant-hightlight-widget',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./favourite-merchant-hightlight-widget.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/favourite-merchant-hightlight-widget/favourite-merchant-hightlight-widget.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./favourite-merchant-hightlight-widget.component.scss */
        "./src/app/shared/favourite-merchant-hightlight-widget/favourite-merchant-hightlight-widget.component.scss"))["default"]]
      })], FavouriteMerchantHightlightWidgetComponent);
      /***/
    },

    /***/
    "./src/app/shared/file-path-formatter.pipe.ts":
    /*!****************************************************!*\
      !*** ./src/app/shared/file-path-formatter.pipe.ts ***!
      \****************************************************/

    /*! exports provided: FilePathFormatterPipe */

    /***/
    function srcAppSharedFilePathFormatterPipeTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "FilePathFormatterPipe", function () {
        return FilePathFormatterPipe;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../../environments/environment */
      "./src/environments/environment.ts");

      var FilePathFormatterPipe = /*#__PURE__*/function () {
        function FilePathFormatterPipe() {
          _classCallCheck(this, FilePathFormatterPipe);
        }

        _createClass(FilePathFormatterPipe, [{
          key: "transform",
          value: function transform(value) {
            var _a;

            if (value) {
              //? Get the apis root url
              var regExp = /https?:\/\/udevalentine-001-site1\.itempurl\.com\//ig; //? Search for the url at the start of the image

              if (((_a = value === null || value === void 0 ? void 0 : value.match(regExp)) === null || _a === void 0 ? void 0 : _a.length) > 1) {
                var returnValue = value.replace(regExp, "");
                return "".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiRoot, "/").concat(returnValue);
              }

              if (!(value === null || value === void 0 ? void 0 : value.startsWith(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiRoot))) {
                return "".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiRoot, "/").concat(value);
              }

              return value;
            }
          }
        }]);

        return FilePathFormatterPipe;
      }();

      FilePathFormatterPipe = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
        name: 'filePathFormatter'
      })], FilePathFormatterPipe);
      /***/
    },

    /***/
    "./src/app/shared/footer-expander/footer-expander.component.ts":
    /*!*********************************************************************!*\
      !*** ./src/app/shared/footer-expander/footer-expander.component.ts ***!
      \*********************************************************************/

    /*! exports provided: FooterExpanderComponent */

    /***/
    function srcAppSharedFooterExpanderFooterExpanderComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "FooterExpanderComponent", function () {
        return FooterExpanderComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var FooterExpanderComponent = /*#__PURE__*/function () {
        function FooterExpanderComponent() {
          _classCallCheck(this, FooterExpanderComponent);
        }

        _createClass(FooterExpanderComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return FooterExpanderComponent;
      }();

      FooterExpanderComponent.ctorParameters = function () {
        return [];
      };

      FooterExpanderComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-footer-expander',
        template: "<div class=\"footer-expander\"></div>"
      })], FooterExpanderComponent);
      /***/
    },

    /***/
    "./src/app/shared/graph-widget/graph-widget.component.scss":
    /*!*****************************************************************!*\
      !*** ./src/app/shared/graph-widget/graph-widget.component.scss ***!
      \*****************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppSharedGraphWidgetGraphWidgetComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".chart {\n  min-height: 200px;\n  max-height: 500px;\n  width: 100%;\n}\n\n.data-is-full {\n  min-height: 400px;\n  max-height: 500px;\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL2dyYXBoLXdpZGdldC9ncmFwaC13aWRnZXQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxpQkFBQTtFQUNBLGlCQUFBO0VBQ0EsV0FBQTtBQUNKOztBQUVBO0VBQ0ksaUJBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7QUFDSiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9ncmFwaC13aWRnZXQvZ3JhcGgtd2lkZ2V0LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNoYXJ0IHtcbiAgICBtaW4taGVpZ2h0OiAyMDBweDtcbiAgICBtYXgtaGVpZ2h0OiA1MDBweDtcbiAgICB3aWR0aDogMTAwJTtcbn1cblxuLmRhdGEtaXMtZnVsbCB7XG4gICAgbWluLWhlaWdodDogNDAwcHg7XG4gICAgbWF4LWhlaWdodDogNTAwcHg7XG4gICAgd2lkdGg6IDEwMCU7IFxufSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/shared/graph-widget/graph-widget.component.ts":
    /*!***************************************************************!*\
      !*** ./src/app/shared/graph-widget/graph-widget.component.ts ***!
      \***************************************************************/

    /*! exports provided: GraphWidgetComponent */

    /***/
    function srcAppSharedGraphWidgetGraphWidgetComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "GraphWidgetComponent", function () {
        return GraphWidgetComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var date_fns__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! date-fns */
      "./node_modules/date-fns/esm/index.js");

      var GraphWidgetComponent = /*#__PURE__*/function () {
        function GraphWidgetComponent() {
          _classCallCheck(this, GraphWidgetComponent);
        }

        _createClass(GraphWidgetComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.graphData = Object.assign(Object.assign({}, this.graphData), {
              weekOrderCounts: this.graphData.weekOrderCounts.map(function (data) {
                return Object.assign(Object.assign({}, data), {
                  labels: "".concat(data.count, " deliveries")
                });
              })
            });
            this.xValue = this.graphData.weekOrderCounts.map(function (data) {
              var _a;

              return (_a = data.weekDay) === null || _a === void 0 ? void 0 : _a.slice(0, 3);
            });
            this.yValue = this.graphData.weekOrderCounts.map(function (data) {
              return data.count;
            });
            this.graphLabel = this.setGraphLabel(this.index);
          }
        }, {
          key: "ngAfterViewInit",
          value: function ngAfterViewInit() {
            var statsForThisWeek = [{
              text: this.yValue.map(String),
              textposition: "auto",
              hoverinfo: "none",
              x: _toConsumableArray(this.xValue),
              y: _toConsumableArray(this.yValue),
              autosize: false,
              height: 500,
              margin: {
                l: 50,
                r: 50,
                b: 100,
                t: 100,
                pad: 4
              },
              marker: {
                color: ["rgba(0,126,204, 0.9)", "rgba(0, 135, 176, 0.8)", "rgba(222,45,38,0.8)", "rgba(0,126,204, 0.9)", "rgba(0, 135, 176, 0.8)", "rgba(222,45,38,0.8)", "rgba(0,126,204, 0.9)"]
              },
              name: "Deliveries for ".concat(this.graphLabel),
              type: "bar"
            }];
            Plotly.newPlot(this.thisWeekStatsEl.nativeElement, statsForThisWeek);
          }
        }, {
          key: "endOfWeek",
          value: function endOfWeek(startDate) {
            return Object(date_fns__WEBPACK_IMPORTED_MODULE_2__["endOfWeek"])(new Date(startDate));
          }
        }, {
          key: "setGraphLabel",
          value: function setGraphLabel(index) {
            var returnedData = "".concat(index + 1, " weeks ago");

            if (index === 0) {
              returnedData = "This Week";
            }

            if (index === 1) {
              returnedData = "Last Week";
            }

            return returnedData;
          }
        }]);

        return GraphWidgetComponent;
      }();

      GraphWidgetComponent.ctorParameters = function () {
        return [];
      };

      GraphWidgetComponent.propDecorators = {
        graphData: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        index: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        thisWeekStatsEl: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: ["thisWeekStats", {
            read: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"]
          }]
        }]
      };
      GraphWidgetComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-graph-widget",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./graph-widget.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/graph-widget/graph-widget.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./graph-widget.component.scss */
        "./src/app/shared/graph-widget/graph-widget.component.scss"))["default"]]
      })], GraphWidgetComponent);
      /***/
    },

    /***/
    "./src/app/shared/hot-sale-highlight-widget/hot-sale-highlight-widget.component.scss":
    /*!*******************************************************************************************!*\
      !*** ./src/app/shared/hot-sale-highlight-widget/hot-sale-highlight-widget.component.scss ***!
      \*******************************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppSharedHotSaleHighlightWidgetHotSaleHighlightWidgetComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9ob3Qtc2FsZS1oaWdobGlnaHQtd2lkZ2V0L2hvdC1zYWxlLWhpZ2hsaWdodC13aWRnZXQuY29tcG9uZW50LnNjc3MifQ== */";
      /***/
    },

    /***/
    "./src/app/shared/hot-sale-highlight-widget/hot-sale-highlight-widget.component.ts":
    /*!*****************************************************************************************!*\
      !*** ./src/app/shared/hot-sale-highlight-widget/hot-sale-highlight-widget.component.ts ***!
      \*****************************************************************************************/

    /*! exports provided: HotSaleHighlightWidgetComponent */

    /***/
    function srcAppSharedHotSaleHighlightWidgetHotSaleHighlightWidgetComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HotSaleHighlightWidgetComponent", function () {
        return HotSaleHighlightWidgetComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var HotSaleHighlightWidgetComponent = /*#__PURE__*/function () {
        function HotSaleHighlightWidgetComponent() {
          _classCallCheck(this, HotSaleHighlightWidgetComponent);

          this.viewProductDetailEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        }

        _createClass(HotSaleHighlightWidgetComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "viewProductDetail",
          value: function viewProductDetail() {
            this.viewProductDetailEvent.emit(true);
          }
        }]);

        return HotSaleHighlightWidgetComponent;
      }();

      HotSaleHighlightWidgetComponent.ctorParameters = function () {
        return [];
      };

      HotSaleHighlightWidgetComponent.propDecorators = {
        product: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        viewProductDetailEvent: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
        }]
      };
      HotSaleHighlightWidgetComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-hot-sale-highlight-widget',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./hot-sale-highlight-widget.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/hot-sale-highlight-widget/hot-sale-highlight-widget.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./hot-sale-highlight-widget.component.scss */
        "./src/app/shared/hot-sale-highlight-widget/hot-sale-highlight-widget.component.scss"))["default"]]
      })], HotSaleHighlightWidgetComponent);
      /***/
    },

    /***/
    "./src/app/shared/loader/loader.component.scss":
    /*!*****************************************************!*\
      !*** ./src/app/shared/loader/loader.component.scss ***!
      \*****************************************************/

    /*! exports provided: default */

    /***/
    function srcAppSharedLoaderLoaderComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".my-custom-class {\n  color: var(--bg-primary);\n}\n\n.modal-container {\n  display: block;\n  z-index: 99999;\n  position: fixed;\n  height: 100vh;\n  width: 100vw;\n  background: rgba(0, 0, 0, 0.7);\n}\n\n.loader {\n  border: 12px solid #f3f3f3;\n  border-top: 12px solid var(--bg-primary);\n  border-radius: 50%;\n  width: 60px;\n  height: 60px;\n  -webkit-animation: spin 1.5s linear infinite;\n          animation: spin 1.5s linear infinite;\n  margin: 0 auto;\n  margin-top: 55% !important;\n}\n\n@-webkit-keyframes spin {\n  0% {\n    transform: rotate(0deg);\n  }\n  100% {\n    transform: rotate(360deg);\n  }\n}\n\n@keyframes spin {\n  0% {\n    transform: rotate(0deg);\n  }\n  100% {\n    transform: rotate(360deg);\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL2xvYWRlci9sb2FkZXIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSx3QkFBQTtBQUNKOztBQUVBO0VBQ0ksY0FBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsYUFBQTtFQUNBLFlBQUE7RUFDQSw4QkFBQTtBQUNKOztBQUVFO0VBQ0UsMEJBQUE7RUFDQSx3Q0FBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSw0Q0FBQTtVQUFBLG9DQUFBO0VBQ0EsY0FBQTtFQUNBLDBCQUFBO0FBQ0o7O0FBRUU7RUFDRTtJQUFLLHVCQUFBO0VBRVA7RUFERTtJQUFPLHlCQUFBO0VBSVQ7QUFDRjs7QUFQRTtFQUNFO0lBQUssdUJBQUE7RUFFUDtFQURFO0lBQU8seUJBQUE7RUFJVDtBQUNGIiwiZmlsZSI6InNyYy9hcHAvc2hhcmVkL2xvYWRlci9sb2FkZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubXktY3VzdG9tLWNsYXNzIHtcbiAgICBjb2xvcjogdmFyKC0tYmctcHJpbWFyeSk7XG59XG5cbi5tb2RhbC1jb250YWluZXIge1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIHotaW5kZXg6IDk5OTk5O1xuICAgIHBvc2l0aW9uOiBmaXhlZDtcbiAgICBoZWlnaHQ6IDEwMHZoO1xuICAgIHdpZHRoOiAxMDB2dztcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDAuNyk7XG4gIH1cblxuICAubG9hZGVyIHtcbiAgICBib3JkZXI6IDEycHggc29saWQgI2YzZjNmMztcbiAgICBib3JkZXItdG9wOiAxMnB4IHNvbGlkIHZhcigtLWJnLXByaW1hcnkpO1xuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICB3aWR0aDogNjBweDtcbiAgICBoZWlnaHQ6IDYwcHg7XG4gICAgYW5pbWF0aW9uOiBzcGluIDEuNXMgbGluZWFyIGluZmluaXRlO1xuICAgIG1hcmdpbjogMCBhdXRvO1xuICAgIG1hcmdpbi10b3A6IDU1JSAhaW1wb3J0YW50O1xuICB9XG5cbiAgQGtleWZyYW1lcyBzcGluIHtcbiAgICAwJSB7IHRyYW5zZm9ybTogcm90YXRlKDBkZWcpOyB9XG4gICAgMTAwJSB7IHRyYW5zZm9ybTogcm90YXRlKDM2MGRlZyk7IH1cbiAgfSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/shared/loader/loader.component.ts":
    /*!***************************************************!*\
      !*** ./src/app/shared/loader/loader.component.ts ***!
      \***************************************************/

    /*! exports provided: LoaderComponent */

    /***/
    function srcAppSharedLoaderLoaderComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoaderComponent", function () {
        return LoaderComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");

      var LoaderComponent = /*#__PURE__*/function () {
        // async ngOnInit(): Promise<void> {
        //   // const el: HTMLIonLoadingElement = await this.loadingController.create({
        //   //   message: 'Please wait...',
        //   //   spinner: "bubbles",
        //   //   keyboardClose: true,
        //   //   backdropDismiss: false
        //   // });
        //   // await el.present();
        // }
        function LoaderComponent(loadingController) {
          _classCallCheck(this, LoaderComponent);

          this.loadingController = loadingController;
        }

        _createClass(LoaderComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {// try {
            //   setTimeout(async () => {
            //     //? Wait for 1ms, then dismiss the loader
            //     await this.loadingController.dismiss();
            //   }, 1000);
            // }
            // catch(ex) {
            //   throw ex;
            // }
          }
        }]);

        return LoaderComponent;
      }();

      LoaderComponent.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]
        }];
      };

      LoaderComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-loader',
        template: "\n    <div class=\"modal-container\">\n      <div class=\"loader\"></div>\n    </div>\n  ",
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./loader.component.scss */
        "./src/app/shared/loader/loader.component.scss"))["default"]]
      })], LoaderComponent);
      /***/
    },

    /***/
    "./src/app/shared/merchant-hot-sale-product/merchant-hot-sale-product.component.scss":
    /*!*******************************************************************************************!*\
      !*** ./src/app/shared/merchant-hot-sale-product/merchant-hot-sale-product.component.scss ***!
      \*******************************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppSharedMerchantHotSaleProductMerchantHotSaleProductComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9tZXJjaGFudC1ob3Qtc2FsZS1wcm9kdWN0L21lcmNoYW50LWhvdC1zYWxlLXByb2R1Y3QuY29tcG9uZW50LnNjc3MifQ== */";
      /***/
    },

    /***/
    "./src/app/shared/merchant-hot-sale-product/merchant-hot-sale-product.component.ts":
    /*!*****************************************************************************************!*\
      !*** ./src/app/shared/merchant-hot-sale-product/merchant-hot-sale-product.component.ts ***!
      \*****************************************************************************************/

    /*! exports provided: MerchantHotSaleProductComponent */

    /***/
    function srcAppSharedMerchantHotSaleProductMerchantHotSaleProductComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MerchantHotSaleProductComponent", function () {
        return MerchantHotSaleProductComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var MerchantHotSaleProductComponent = /*#__PURE__*/function () {
        function MerchantHotSaleProductComponent() {
          _classCallCheck(this, MerchantHotSaleProductComponent);

          this.viewProductDetailEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
          this.addProductToBookmarkEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
          this.addToCartEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        }

        _createClass(MerchantHotSaleProductComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            unBundleSVG();
          }
        }, {
          key: "viewProductDetail",
          value: function viewProductDetail() {
            this.viewProductDetailEvent.emit(true);
          }
        }, {
          key: "addProductToBookmark",
          value: function addProductToBookmark(productId) {
            this.addProductToBookmarkEvent.emit(productId);
          }
        }, {
          key: "addToCart",
          value: function addToCart(product) {
            var _product$productModel = product.productModel,
                id = _product$productModel.id,
                name = _product$productModel.name,
                unitPrice = product.productCostModel.unitPrice,
                productImageModels = product.productImageModels;
            this.addToCartEvent.emit({
              status: true,
              payload: {
                productId: id,
                productName: name,
                cost: unitPrice,
                quantity: 1
              },
              productImages: productImageModels
            });
          }
        }]);

        return MerchantHotSaleProductComponent;
      }();

      MerchantHotSaleProductComponent.ctorParameters = function () {
        return [];
      };

      MerchantHotSaleProductComponent.propDecorators = {
        viewProductDetailEvent: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
        }],
        addProductToBookmarkEvent: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
        }],
        addToCartEvent: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
        }],
        product: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }]
      };
      MerchantHotSaleProductComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-merchant-hot-sale-product',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./merchant-hot-sale-product.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/merchant-hot-sale-product/merchant-hot-sale-product.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./merchant-hot-sale-product.component.scss */
        "./src/app/shared/merchant-hot-sale-product/merchant-hot-sale-product.component.scss"))["default"]]
      })], MerchantHotSaleProductComponent);
      /***/
    },

    /***/
    "./src/app/shared/merchant-product-listing/merchant-product-listing.component.scss":
    /*!*****************************************************************************************!*\
      !*** ./src/app/shared/merchant-product-listing/merchant-product-listing.component.scss ***!
      \*****************************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppSharedMerchantProductListingMerchantProductListingComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".business {\n  margin-bottom: 15px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL21lcmNoYW50LXByb2R1Y3QtbGlzdGluZy9tZXJjaGFudC1wcm9kdWN0LWxpc3RpbmcuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxtQkFBQTtBQUNKIiwiZmlsZSI6InNyYy9hcHAvc2hhcmVkL21lcmNoYW50LXByb2R1Y3QtbGlzdGluZy9tZXJjaGFudC1wcm9kdWN0LWxpc3RpbmcuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYnVzaW5lc3Mge1xuICAgIG1hcmdpbi1ib3R0b206IDE1cHg7XG59Il19 */";
      /***/
    },

    /***/
    "./src/app/shared/merchant-product-listing/merchant-product-listing.component.ts":
    /*!***************************************************************************************!*\
      !*** ./src/app/shared/merchant-product-listing/merchant-product-listing.component.ts ***!
      \***************************************************************************************/

    /*! exports provided: MerchantProductListingComponent */

    /***/
    function srcAppSharedMerchantProductListingMerchantProductListingComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MerchantProductListingComponent", function () {
        return MerchantProductListingComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");

      var MerchantProductListingComponent = /*#__PURE__*/function () {
        function MerchantProductListingComponent(router) {
          _classCallCheck(this, MerchantProductListingComponent);

          this.router = router;
          this.addToBookmark = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        }

        _createClass(MerchantProductListingComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            unBundleSVG();
          }
        }, {
          key: "addBookmark",
          value: function addBookmark(merchantId) {
            this.addToBookmark.emit({
              IsBookmarked: true,
              MerchantId: merchantId
            });
          }
        }, {
          key: "openProductDetail",
          value: function openProductDetail(event, product) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee43() {
              return regeneratorRuntime.wrap(function _callee43$(_context43) {
                while (1) {
                  switch (_context43.prev = _context43.next) {
                    case 0:
                      if (product === null || product === void 0 ? void 0 : product.productModel) {
                        this.router.navigateByUrl("/user/product-detail?product=".concat(JSON.stringify(product)));
                      }

                    case 1:
                    case "end":
                      return _context43.stop();
                  }
                }
              }, _callee43, this);
            }));
          }
        }]);

        return MerchantProductListingComponent;
      }();

      MerchantProductListingComponent.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }];
      };

      MerchantProductListingComponent.propDecorators = {
        addToBookmark: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
        }],
        isBookmarked: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        merchant: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        currentIteration: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        arraySize: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }]
      };
      MerchantProductListingComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-merchant-product-listing',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./merchant-product-listing.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/merchant-product-listing/merchant-product-listing.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./merchant-product-listing.component.scss */
        "./src/app/shared/merchant-product-listing/merchant-product-listing.component.scss"))["default"]]
      })], MerchantProductListingComponent);
      /***/
    },

    /***/
    "./src/app/shared/nearby-item-widget/nearby-item-widget.component.scss":
    /*!*****************************************************************************!*\
      !*** ./src/app/shared/nearby-item-widget/nearby-item-widget.component.scss ***!
      \*****************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppSharedNearbyItemWidgetNearbyItemWidgetComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9uZWFyYnktaXRlbS13aWRnZXQvbmVhcmJ5LWl0ZW0td2lkZ2V0LmNvbXBvbmVudC5zY3NzIn0= */";
      /***/
    },

    /***/
    "./src/app/shared/nearby-item-widget/nearby-item-widget.component.ts":
    /*!***************************************************************************!*\
      !*** ./src/app/shared/nearby-item-widget/nearby-item-widget.component.ts ***!
      \***************************************************************************/

    /*! exports provided: NearbyItemWidgetComponent */

    /***/
    function srcAppSharedNearbyItemWidgetNearbyItemWidgetComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "NearbyItemWidgetComponent", function () {
        return NearbyItemWidgetComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../../../utils/functions/app.functions */
      "./src/utils/functions/app.functions.ts");

      var NearbyItemWidgetComponent = /*#__PURE__*/function () {
        function NearbyItemWidgetComponent() {
          _classCallCheck(this, NearbyItemWidgetComponent);

          this.addToBookmark = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
          this.isBookmarked = false;
        }

        _createClass(NearbyItemWidgetComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee44() {
              var bookmark;
              return regeneratorRuntime.wrap(function _callee44$(_context44) {
                while (1) {
                  switch (_context44.prev = _context44.next) {
                    case 0:
                      _context44.next = 2;
                      return Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_2__["findUserBookmark"])(this.merchant.id);

                    case 2:
                      bookmark = _context44.sent;

                      if (bookmark === null || bookmark === void 0 ? void 0 : bookmark.Id) {
                        this.isBookmarked = true;
                      }

                      unBundleSVG();

                    case 5:
                    case "end":
                      return _context44.stop();
                  }
                }
              }, _callee44, this);
            }));
          }
        }, {
          key: "addBookmark",
          value: function addBookmark(merchant) {
            this.addToBookmark.emit(merchant);
          }
        }]);

        return NearbyItemWidgetComponent;
      }();

      NearbyItemWidgetComponent.ctorParameters = function () {
        return [];
      };

      NearbyItemWidgetComponent.propDecorators = {
        merchant: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        currentIndex: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        arrayLength: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        addToBookmark: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
        }]
      };
      NearbyItemWidgetComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-nearby-item-widget',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./nearby-item-widget.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/nearby-item-widget/nearby-item-widget.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./nearby-item-widget.component.scss */
        "./src/app/shared/nearby-item-widget/nearby-item-widget.component.scss"))["default"]]
      })], NearbyItemWidgetComponent);
      /***/
    },

    /***/
    "./src/app/shared/no-content-found/no-content-found.component.scss":
    /*!*************************************************************************!*\
      !*** ./src/app/shared/no-content-found/no-content-found.component.scss ***!
      \*************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppSharedNoContentFoundNoContentFoundComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-card-content {\n  height: 300px;\n}\nion-card-content img {\n  width: 100%;\n  height: 100%;\n}\nion-card-header h2 {\n  padding-top: 30px;\n  text-align: center;\n  font-weight: 400;\n  color: #fa6400;\n  letter-spacing: 1px;\n  line-height: 35px;\n  font-size: 25px;\n  text-shadow: 1px 1px #ddd;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL25vLWNvbnRlbnQtZm91bmQvbm8tY29udGVudC1mb3VuZC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGFBQUE7QUFDSjtBQUNJO0VBQ0ksV0FBQTtFQUNBLFlBQUE7QUFDUjtBQUlJO0VBQ0ksaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0VBQ0EseUJBQUE7QUFEUiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9uby1jb250ZW50LWZvdW5kL25vLWNvbnRlbnQtZm91bmQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY2FyZC1jb250ZW50IHtcbiAgICBoZWlnaHQ6IDMwMHB4O1xuXG4gICAgaW1nIHtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIGhlaWdodDogMTAwJTtcbiAgICB9XG59XG5cbmlvbi1jYXJkLWhlYWRlcntcbiAgICBoMiB7XG4gICAgICAgIHBhZGRpbmctdG9wOiAzMHB4O1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgICAgIGNvbG9yOiAjZmE2NDAwO1xuICAgICAgICBsZXR0ZXItc3BhY2luZzogMXB4O1xuICAgICAgICBsaW5lLWhlaWdodDogMzVweDtcbiAgICAgICAgZm9udC1zaXplOiAyNXB4O1xuICAgICAgICB0ZXh0LXNoYWRvdzogMXB4IDFweCAjZGRkO1xuICAgIH1cbn0iXX0= */";
      /***/
    },

    /***/
    "./src/app/shared/no-content-found/no-content-found.component.ts":
    /*!***********************************************************************!*\
      !*** ./src/app/shared/no-content-found/no-content-found.component.ts ***!
      \***********************************************************************/

    /*! exports provided: NoContentFoundComponent */

    /***/
    function srcAppSharedNoContentFoundNoContentFoundComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "NoContentFoundComponent", function () {
        return NoContentFoundComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var NoContentFoundComponent = /*#__PURE__*/function () {
        function NoContentFoundComponent() {
          _classCallCheck(this, NoContentFoundComponent);
        }

        _createClass(NoContentFoundComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return NoContentFoundComponent;
      }();

      NoContentFoundComponent.ctorParameters = function () {
        return [];
      };

      NoContentFoundComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-no-content-found',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./no-content-found.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/no-content-found/no-content-found.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./no-content-found.component.scss */
        "./src/app/shared/no-content-found/no-content-found.component.scss"))["default"]]
      })], NoContentFoundComponent);
      /***/
    },

    /***/
    "./src/app/shared/no-internet-modal/no-internet-modal.component.scss":
    /*!***************************************************************************!*\
      !*** ./src/app/shared/no-internet-modal/no-internet-modal.component.scss ***!
      \***************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppSharedNoInternetModalNoInternetModalComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "main {\n  padding: 150px 20px 20px 20px;\n}\nmain .image-section {\n  width: 70%;\n  height: 30vh;\n  margin: 0 auto;\n}\nmain .image-section img {\n  width: 100%;\n  height: 100%;\n}\nmain .image-section h2 {\n  padding-top: 30px;\n  text-align: center;\n  font-weight: 400;\n  color: #fa6400;\n  letter-spacing: 1px;\n  line-height: 35px;\n  font-size: 25px;\n  text-shadow: 1px 1px #ddd;\n  font-family: \"comic sans ms\";\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL25vLWludGVybmV0LW1vZGFsL25vLWludGVybmV0LW1vZGFsLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksNkJBQUE7QUFDSjtBQUNJO0VBQ0ksVUFBQTtFQUNBLFlBQUE7RUFDQSxjQUFBO0FBQ1I7QUFDUTtFQUNJLFdBQUE7RUFDQSxZQUFBO0FBQ1o7QUFFUTtFQUNJLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLHlCQUFBO0VBQ0EsNEJBQUE7QUFBWiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9uby1pbnRlcm5ldC1tb2RhbC9uby1pbnRlcm5ldC1tb2RhbC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIm1haW4ge1xuICAgIHBhZGRpbmc6IDE1MHB4IDIwcHggMjBweCAyMHB4O1xuXG4gICAgLmltYWdlLXNlY3Rpb24ge1xuICAgICAgICB3aWR0aDogNzAlO1xuICAgICAgICBoZWlnaHQ6IDMwdmg7XG4gICAgICAgIG1hcmdpbjogMCBhdXRvO1xuXG4gICAgICAgIGltZyB7XG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgICAgIGhlaWdodDogMTAwJTtcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgaDIge1xuICAgICAgICAgICAgcGFkZGluZy10b3A6IDMwcHg7XG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgICBmb250LXdlaWdodDogNDAwO1xuICAgICAgICAgICAgY29sb3I6ICNmYTY0MDA7XG4gICAgICAgICAgICBsZXR0ZXItc3BhY2luZzogMXB4O1xuICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDM1cHg7XG4gICAgICAgICAgICBmb250LXNpemU6IDI1cHg7XG4gICAgICAgICAgICB0ZXh0LXNoYWRvdzogMXB4IDFweCAjZGRkO1xuICAgICAgICAgICAgZm9udC1mYW1pbHk6ICdjb21pYyBzYW5zIG1zJ1xuICAgICAgICB9XG4gICAgfVxuICAgICBcbn0iXX0= */";
      /***/
    },

    /***/
    "./src/app/shared/no-internet-modal/no-internet-modal.component.ts":
    /*!*************************************************************************!*\
      !*** ./src/app/shared/no-internet-modal/no-internet-modal.component.ts ***!
      \*************************************************************************/

    /*! exports provided: NoInternetModalComponent */

    /***/
    function srcAppSharedNoInternetModalNoInternetModalComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "NoInternetModalComponent", function () {
        return NoInternetModalComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var NoInternetModalComponent = /*#__PURE__*/function () {
        function NoInternetModalComponent() {
          _classCallCheck(this, NoInternetModalComponent);
        }

        _createClass(NoInternetModalComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return NoInternetModalComponent;
      }();

      NoInternetModalComponent.ctorParameters = function () {
        return [];
      };

      NoInternetModalComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-no-internet-modal',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./no-internet-modal.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/no-internet-modal/no-internet-modal.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./no-internet-modal.component.scss */
        "./src/app/shared/no-internet-modal/no-internet-modal.component.scss"))["default"]]
      })], NoInternetModalComponent);
      /***/
    },

    /***/
    "./src/app/shared/no-orders-found-widget/no-orders-found-widget.component.scss":
    /*!*************************************************************************************!*\
      !*** ./src/app/shared/no-orders-found-widget/no-orders-found-widget.component.scss ***!
      \*************************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppSharedNoOrdersFoundWidgetNoOrdersFoundWidgetComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9uby1vcmRlcnMtZm91bmQtd2lkZ2V0L25vLW9yZGVycy1mb3VuZC13aWRnZXQuY29tcG9uZW50LnNjc3MifQ== */";
      /***/
    },

    /***/
    "./src/app/shared/no-orders-found-widget/no-orders-found-widget.component.ts":
    /*!***********************************************************************************!*\
      !*** ./src/app/shared/no-orders-found-widget/no-orders-found-widget.component.ts ***!
      \***********************************************************************************/

    /*! exports provided: NoOrdersFoundWidgetComponent */

    /***/
    function srcAppSharedNoOrdersFoundWidgetNoOrdersFoundWidgetComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "NoOrdersFoundWidgetComponent", function () {
        return NoOrdersFoundWidgetComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var NoOrdersFoundWidgetComponent = /*#__PURE__*/function () {
        function NoOrdersFoundWidgetComponent() {
          _classCallCheck(this, NoOrdersFoundWidgetComponent);
        }

        _createClass(NoOrdersFoundWidgetComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return NoOrdersFoundWidgetComponent;
      }();

      NoOrdersFoundWidgetComponent.ctorParameters = function () {
        return [];
      };

      NoOrdersFoundWidgetComponent.propDecorators = {
        purpose: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        showButton: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }]
      };
      NoOrdersFoundWidgetComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-no-orders-found-widget',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./no-orders-found-widget.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/no-orders-found-widget/no-orders-found-widget.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./no-orders-found-widget.component.scss */
        "./src/app/shared/no-orders-found-widget/no-orders-found-widget.component.scss"))["default"]]
      })], NoOrdersFoundWidgetComponent);
      /***/
    },

    /***/
    "./src/app/shared/order-selected-widget-draft/order-selected-widget-draft.component.scss":
    /*!***********************************************************************************************!*\
      !*** ./src/app/shared/order-selected-widget-draft/order-selected-widget-draft.component.scss ***!
      \***********************************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppSharedOrderSelectedWidgetDraftOrderSelectedWidgetDraftComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9vcmRlci1zZWxlY3RlZC13aWRnZXQtZHJhZnQvb3JkZXItc2VsZWN0ZWQtd2lkZ2V0LWRyYWZ0LmNvbXBvbmVudC5zY3NzIn0= */";
      /***/
    },

    /***/
    "./src/app/shared/order-selected-widget-draft/order-selected-widget-draft.component.ts":
    /*!*********************************************************************************************!*\
      !*** ./src/app/shared/order-selected-widget-draft/order-selected-widget-draft.component.ts ***!
      \*********************************************************************************************/

    /*! exports provided: OrderSelectedWidgetDraftComponent */

    /***/
    function srcAppSharedOrderSelectedWidgetDraftOrderSelectedWidgetDraftComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "OrderSelectedWidgetDraftComponent", function () {
        return OrderSelectedWidgetDraftComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _user_store_model_user_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../../user/store/model/user.model */
      "./src/app/user/store/model/user.model.ts");

      var OrderSelectedWidgetDraftComponent = /*#__PURE__*/function () {
        function OrderSelectedWidgetDraftComponent() {
          _classCallCheck(this, OrderSelectedWidgetDraftComponent);

          this.productCartStatus = _user_store_model_user_model__WEBPACK_IMPORTED_MODULE_2__["ProductCartStatus"];
        }

        _createClass(OrderSelectedWidgetDraftComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            unBundleSVG();
          }
        }, {
          key: "getProductImageToPreview",
          value: function getProductImageToPreview(products) {
            var imagePath;

            if (products[0].productImage) {
              imagePath = products[0].productImage;
            } else {
              var productsWithImages = products.filter(function (data) {
                return data.productImage;
              });

              if ((productsWithImages === null || productsWithImages === void 0 ? void 0 : productsWithImages.length) > 0) {
                imagePath = productsWithImages[0].productImage;
              }
            }

            return imagePath;
          }
        }]);

        return OrderSelectedWidgetDraftComponent;
      }();

      OrderSelectedWidgetDraftComponent.ctorParameters = function () {
        return [];
      };

      OrderSelectedWidgetDraftComponent.propDecorators = {
        cart: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }]
      };
      OrderSelectedWidgetDraftComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-order-selected-widget-draft',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./order-selected-widget-draft.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/order-selected-widget-draft/order-selected-widget-draft.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./order-selected-widget-draft.component.scss */
        "./src/app/shared/order-selected-widget-draft/order-selected-widget-draft.component.scss"))["default"]]
      })], OrderSelectedWidgetDraftComponent);
      /***/
    },

    /***/
    "./src/app/shared/order-selected-widget-history/order-selected-widget-history.component.scss":
    /*!***************************************************************************************************!*\
      !*** ./src/app/shared/order-selected-widget-history/order-selected-widget-history.component.scss ***!
      \***************************************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppSharedOrderSelectedWidgetHistoryOrderSelectedWidgetHistoryComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9vcmRlci1zZWxlY3RlZC13aWRnZXQtaGlzdG9yeS9vcmRlci1zZWxlY3RlZC13aWRnZXQtaGlzdG9yeS5jb21wb25lbnQuc2NzcyJ9 */";
      /***/
    },

    /***/
    "./src/app/shared/order-selected-widget-history/order-selected-widget-history.component.ts":
    /*!*************************************************************************************************!*\
      !*** ./src/app/shared/order-selected-widget-history/order-selected-widget-history.component.ts ***!
      \*************************************************************************************************/

    /*! exports provided: OrderSelectedWidgetHistoryComponent */

    /***/
    function srcAppSharedOrderSelectedWidgetHistoryOrderSelectedWidgetHistoryComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "OrderSelectedWidgetHistoryComponent", function () {
        return OrderSelectedWidgetHistoryComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _user_store_model_user_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../../user/store/model/user.model */
      "./src/app/user/store/model/user.model.ts");

      var OrderSelectedWidgetHistoryComponent = /*#__PURE__*/function () {
        function OrderSelectedWidgetHistoryComponent() {
          _classCallCheck(this, OrderSelectedWidgetHistoryComponent);

          this.productCartStatus = _user_store_model_user_model__WEBPACK_IMPORTED_MODULE_2__["ProductCartStatus"];
        }

        _createClass(OrderSelectedWidgetHistoryComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            unBundleSVG();
          }
        }, {
          key: "getProductImageToPreview",
          value: function getProductImageToPreview(products) {
            var imagePath;

            if (products[0].productImage) {
              imagePath = products[0].productImage;
            } else {
              var productsWithImages = products.filter(function (data) {
                return data.productImage;
              });

              if ((productsWithImages === null || productsWithImages === void 0 ? void 0 : productsWithImages.length) > 0) {
                imagePath = productsWithImages[0].productImage;
              }
            }

            return imagePath;
          }
        }]);

        return OrderSelectedWidgetHistoryComponent;
      }();

      OrderSelectedWidgetHistoryComponent.ctorParameters = function () {
        return [];
      };

      OrderSelectedWidgetHistoryComponent.propDecorators = {
        cart: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }]
      };
      OrderSelectedWidgetHistoryComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-order-selected-widget-history',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./order-selected-widget-history.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/order-selected-widget-history/order-selected-widget-history.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./order-selected-widget-history.component.scss */
        "./src/app/shared/order-selected-widget-history/order-selected-widget-history.component.scss"))["default"]]
      })], OrderSelectedWidgetHistoryComponent);
      /***/
    },

    /***/
    "./src/app/shared/order-selected-widget-ongoing/order-selected-widget-ongoing.component.scss":
    /*!***************************************************************************************************!*\
      !*** ./src/app/shared/order-selected-widget-ongoing/order-selected-widget-ongoing.component.scss ***!
      \***************************************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppSharedOrderSelectedWidgetOngoingOrderSelectedWidgetOngoingComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9vcmRlci1zZWxlY3RlZC13aWRnZXQtb25nb2luZy9vcmRlci1zZWxlY3RlZC13aWRnZXQtb25nb2luZy5jb21wb25lbnQuc2NzcyJ9 */";
      /***/
    },

    /***/
    "./src/app/shared/order-selected-widget-ongoing/order-selected-widget-ongoing.component.ts":
    /*!*************************************************************************************************!*\
      !*** ./src/app/shared/order-selected-widget-ongoing/order-selected-widget-ongoing.component.ts ***!
      \*************************************************************************************************/

    /*! exports provided: OrderSelectedWidgetOngoingComponent */

    /***/
    function srcAppSharedOrderSelectedWidgetOngoingOrderSelectedWidgetOngoingComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "OrderSelectedWidgetOngoingComponent", function () {
        return OrderSelectedWidgetOngoingComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _user_store_model_user_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../../user/store/model/user.model */
      "./src/app/user/store/model/user.model.ts");

      var OrderSelectedWidgetOngoingComponent = /*#__PURE__*/function () {
        function OrderSelectedWidgetOngoingComponent() {
          _classCallCheck(this, OrderSelectedWidgetOngoingComponent);

          this.productCartStatus = _user_store_model_user_model__WEBPACK_IMPORTED_MODULE_2__["ProductCartStatus"];
        }

        _createClass(OrderSelectedWidgetOngoingComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            unBundleSVG();
          }
        }, {
          key: "getProductImageToPreview",
          value: function getProductImageToPreview(products) {
            var imagePath;

            if (products[0].productImage) {
              imagePath = products[0].productImage;
            } else {
              var productsWithImages = products.filter(function (data) {
                return data.productImage;
              });

              if ((productsWithImages === null || productsWithImages === void 0 ? void 0 : productsWithImages.length) > 0) {
                imagePath = productsWithImages[0].productImage;
              }
            }

            return imagePath;
          }
        }]);

        return OrderSelectedWidgetOngoingComponent;
      }();

      OrderSelectedWidgetOngoingComponent.ctorParameters = function () {
        return [];
      };

      OrderSelectedWidgetOngoingComponent.propDecorators = {
        cart: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }]
      };
      OrderSelectedWidgetOngoingComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-order-selected-widget-ongoing',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./order-selected-widget-ongoing.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/order-selected-widget-ongoing/order-selected-widget-ongoing.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./order-selected-widget-ongoing.component.scss */
        "./src/app/shared/order-selected-widget-ongoing/order-selected-widget-ongoing.component.scss"))["default"]]
      })], OrderSelectedWidgetOngoingComponent);
      /***/
    },

    /***/
    "./src/app/shared/ordered-product-highlight-widget/ordered-product-highlight-widget.component.scss":
    /*!*********************************************************************************************************!*\
      !*** ./src/app/shared/ordered-product-highlight-widget/ordered-product-highlight-widget.component.scss ***!
      \*********************************************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppSharedOrderedProductHighlightWidgetOrderedProductHighlightWidgetComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".widget-img-holder {\n  padding-left: 0;\n  padding-right: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL29yZGVyZWQtcHJvZHVjdC1oaWdobGlnaHQtd2lkZ2V0L29yZGVyZWQtcHJvZHVjdC1oaWdobGlnaHQtd2lkZ2V0LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksZUFBQTtFQUNBLGdCQUFBO0FBQ0oiLCJmaWxlIjoic3JjL2FwcC9zaGFyZWQvb3JkZXJlZC1wcm9kdWN0LWhpZ2hsaWdodC13aWRnZXQvb3JkZXJlZC1wcm9kdWN0LWhpZ2hsaWdodC13aWRnZXQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIud2lkZ2V0LWltZy1ob2xkZXIge1xuICAgIHBhZGRpbmctbGVmdDogMDtcbiAgICBwYWRkaW5nLXJpZ2h0OiAwO1xufSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/shared/ordered-product-highlight-widget/ordered-product-highlight-widget.component.ts":
    /*!*******************************************************************************************************!*\
      !*** ./src/app/shared/ordered-product-highlight-widget/ordered-product-highlight-widget.component.ts ***!
      \*******************************************************************************************************/

    /*! exports provided: OrderedProductHighlightWidgetComponent */

    /***/
    function srcAppSharedOrderedProductHighlightWidgetOrderedProductHighlightWidgetComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "OrderedProductHighlightWidgetComponent", function () {
        return OrderedProductHighlightWidgetComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var OrderedProductHighlightWidgetComponent = /*#__PURE__*/function () {
        function OrderedProductHighlightWidgetComponent() {
          _classCallCheck(this, OrderedProductHighlightWidgetComponent);

          this.viewProductDetailEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        }

        _createClass(OrderedProductHighlightWidgetComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "viewProductDetail",
          value: function viewProductDetail() {
            this.viewProductDetailEvent.emit(true);
          }
        }]);

        return OrderedProductHighlightWidgetComponent;
      }();

      OrderedProductHighlightWidgetComponent.ctorParameters = function () {
        return [];
      };

      OrderedProductHighlightWidgetComponent.propDecorators = {
        product: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        viewProductDetailEvent: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
        }]
      };
      OrderedProductHighlightWidgetComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-ordered-product-highlight-widget',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./ordered-product-highlight-widget.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/ordered-product-highlight-widget/ordered-product-highlight-widget.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./ordered-product-highlight-widget.component.scss */
        "./src/app/shared/ordered-product-highlight-widget/ordered-product-highlight-widget.component.scss"))["default"]]
      })], OrderedProductHighlightWidgetComponent);
      /***/
    },

    /***/
    "./src/app/shared/product-category-widget/product-category-widget.component.scss":
    /*!***************************************************************************************!*\
      !*** ./src/app/shared/product-category-widget/product-category-widget.component.scss ***!
      \***************************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppSharedProductCategoryWidgetProductCategoryWidgetComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9wcm9kdWN0LWNhdGVnb3J5LXdpZGdldC9wcm9kdWN0LWNhdGVnb3J5LXdpZGdldC5jb21wb25lbnQuc2NzcyJ9 */";
      /***/
    },

    /***/
    "./src/app/shared/product-category-widget/product-category-widget.component.ts":
    /*!*************************************************************************************!*\
      !*** ./src/app/shared/product-category-widget/product-category-widget.component.ts ***!
      \*************************************************************************************/

    /*! exports provided: ProductCategoryWidgetComponent */

    /***/
    function srcAppSharedProductCategoryWidgetProductCategoryWidgetComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ProductCategoryWidgetComponent", function () {
        return ProductCategoryWidgetComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var ProductCategoryWidgetComponent = /*#__PURE__*/function () {
        function ProductCategoryWidgetComponent() {
          _classCallCheck(this, ProductCategoryWidgetComponent);

          this.viewProductDetailEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
          this.addProductToBookmarkEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
          this.addToCartEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        }

        _createClass(ProductCategoryWidgetComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "viewProductDetail",
          value: function viewProductDetail() {
            this.viewProductDetailEvent.emit(true);
          }
        }, {
          key: "addProductToBookmark",
          value: function addProductToBookmark(productId) {
            this.addProductToBookmarkEvent.emit(productId);
          }
        }, {
          key: "addToCart",
          value: function addToCart(product) {
            var _a;

            var _product$productModel2 = product.productModel,
                id = _product$productModel2.id,
                name = _product$productModel2.name,
                unitPrice = product.productCostModel.unitPrice,
                productImageModels = product.productImageModels;
            this.addToCartEvent.emit({
              status: true,
              payload: {
                productId: id,
                productName: name,
                productImage: ((_a = product.productImageModels[0]) === null || _a === void 0 ? void 0 : _a.path) || undefined,
                cost: unitPrice,
                quantity: 1
              },
              productImages: productImageModels
            });
          }
        }]);

        return ProductCategoryWidgetComponent;
      }();

      ProductCategoryWidgetComponent.ctorParameters = function () {
        return [];
      };

      ProductCategoryWidgetComponent.propDecorators = {
        viewProductDetailEvent: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
        }],
        addProductToBookmarkEvent: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
        }],
        addToCartEvent: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
        }],
        product: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }]
      };
      ProductCategoryWidgetComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-product-category-widget',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./product-category-widget.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/product-category-widget/product-category-widget.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./product-category-widget.component.scss */
        "./src/app/shared/product-category-widget/product-category-widget.component.scss"))["default"]]
      })], ProductCategoryWidgetComponent);
      /***/
    },

    /***/
    "./src/app/shared/product-cost-calculator.pipe.ts":
    /*!********************************************************!*\
      !*** ./src/app/shared/product-cost-calculator.pipe.ts ***!
      \********************************************************/

    /*! exports provided: ProductCostCalculatorPipe */

    /***/
    function srcAppSharedProductCostCalculatorPipeTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ProductCostCalculatorPipe", function () {
        return ProductCostCalculatorPipe;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var ProductCostCalculatorPipe = /*#__PURE__*/function () {
        function ProductCostCalculatorPipe() {
          _classCallCheck(this, ProductCostCalculatorPipe);
        }

        _createClass(ProductCostCalculatorPipe, [{
          key: "transform",
          value: function transform(value) {
            var allPrices = value.map(function (data) {
              return data.cost;
            }) || [];
            return allPrices.reduce(function (a, b) {
              return a + b;
            }, 0);
          }
        }]);

        return ProductCostCalculatorPipe;
      }();

      ProductCostCalculatorPipe = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
        name: 'productCostCalculator'
      })], ProductCostCalculatorPipe);
      /***/
    },

    /***/
    "./src/app/shared/product-detail-modal/product-detail-modal.component.scss":
    /*!*********************************************************************************!*\
      !*** ./src/app/shared/product-detail-modal/product-detail-modal.component.scss ***!
      \*********************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppSharedProductDetailModalProductDetailModalComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "section {\n  width: 90%;\n  margin: 0 auto;\n  padding: 50px 0;\n}\nsection .row-1 {\n  display: flex;\n  flex-direction: row;\n  width: 100%;\n}\nsection .row-1 .column {\n  width: 50%;\n}\nsection .row-1 .column:nth-child(2) .btn {\n  float: right;\n}\nsection .row-2 {\n  padding: 50px 0;\n}\nsection .row-2 .img-holder {\n  width: 60%;\n  height: 250px;\n  margin: 0 auto;\n  box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06);\n}\nsection .row-2 .img-holder img {\n  width: 100%;\n  height: 100%;\n}\nsection .row-3 p {\n  text-align: center;\n  font-size: 18px;\n  font-weight: 100;\n}\n#productImage .swiper-wrapper {\n  transition-timing-function: ease !important;\n  z-index: auto !important;\n}\n#productImage.swiper-container {\n  width: 100%;\n  height: 100%;\n  z-index: auto !important;\n}\n.full-width {\n  width: 100%;\n}\n.btn.bookmark {\n  position: absolute;\n  right: 0;\n}\nion-toolbar {\n  --background: #fa6400;\n  --color: #fff;\n}\nion-card-title {\n  --color: #fa6400;\n}\nion-button {\n  --border-color: #fa6400;\n  --color: #fa6400;\n}\n.img-holder {\n  width: 100%;\n  height: 100%;\n  margin: 0 auto;\n  box-shadow: 0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -1px rgba(0, 0, 0, 0.06);\n}\n.img-holder img {\n  width: 100% !important;\n  max-height: 320px !important;\n  height: auto !important;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\n.description {\n  margin-top: 20px;\n  font-size: 18px;\n}\n.description::first-letter {\n  font-size: 25px !important;\n  color: #fa6400;\n  margin-left: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL3Byb2R1Y3QtZGV0YWlsLW1vZGFsL3Byb2R1Y3QtZGV0YWlsLW1vZGFsLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksVUFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0FBQ0o7QUFDSTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7QUFDUjtBQUNRO0VBQ0ksVUFBQTtBQUNaO0FBR1k7RUFDSSxZQUFBO0FBRGhCO0FBTUk7RUFDSSxlQUFBO0FBSlI7QUFNUTtFQUNJLFVBQUE7RUFDQSxhQUFBO0VBQ0EsY0FBQTtFQUNBLDJFQUFBO0FBSlo7QUFNWTtFQUNJLFdBQUE7RUFDQSxZQUFBO0FBSmhCO0FBVVE7RUFDSSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQVJaO0FBYUE7RUFDSSwyQ0FBQTtFQUNBLHdCQUFBO0FBVko7QUFhQTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0Esd0JBQUE7QUFWSjtBQWFBO0VBQ0ksV0FBQTtBQVZKO0FBYUE7RUFDSSxrQkFBQTtFQUNBLFFBQUE7QUFWSjtBQWFBO0VBQ0kscUJBQUE7RUFDQSxhQUFBO0FBVko7QUFhQTtFQUNJLGdCQUFBO0FBVko7QUFhQTtFQUNJLHVCQUFBO0VBQ0EsZ0JBQUE7QUFWSjtBQWFBO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSxjQUFBO0VBQ0EsaUZBQUE7QUFWSjtBQWFJO0VBQ0ksc0JBQUE7RUFDQSw0QkFBQTtFQUNBLHVCQUFBO0VBQ0Esc0JBQUE7S0FBQSxtQkFBQTtBQVhSO0FBZUE7RUFDSSxnQkFBQTtFQUNBLGVBQUE7QUFaSjtBQWVBO0VBQ0ksMEJBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7QUFaSiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9wcm9kdWN0LWRldGFpbC1tb2RhbC9wcm9kdWN0LWRldGFpbC1tb2RhbC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbInNlY3Rpb24ge1xuICAgIHdpZHRoOiA5MCU7XG4gICAgbWFyZ2luOiAwIGF1dG87XG4gICAgcGFkZGluZzogNTBweCAwO1xuXG4gICAgLnJvdy0xIHtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAgd2lkdGg6IDEwMCU7XG5cbiAgICAgICAgLmNvbHVtbiB7XG4gICAgICAgICAgICB3aWR0aDogNTAlO1xuICAgICAgICB9XG5cbiAgICAgICAgLmNvbHVtbjpudGgtY2hpbGQoMikge1xuICAgICAgICAgICAgLmJ0biB7XG4gICAgICAgICAgICAgICAgZmxvYXQ6IHJpZ2h0O1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgLnJvdy0yIHtcbiAgICAgICAgcGFkZGluZzogNTBweCAwO1xuXG4gICAgICAgIC5pbWctaG9sZGVyIHtcbiAgICAgICAgICAgIHdpZHRoOiA2MCU7XG4gICAgICAgICAgICBoZWlnaHQ6IDI1MHB4O1xuICAgICAgICAgICAgbWFyZ2luOiAwIGF1dG87XG4gICAgICAgICAgICBib3gtc2hhZG93OiAwIDFweCAzcHggMCByZ2JhKDAsIDAsIDAsIDAuMSksIDAgMXB4IDJweCAwIHJnYmEoMCwgMCwgMCwgMC4wNik7O1xuXG4gICAgICAgICAgICBpbWcge1xuICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAgICAgICAgIGhlaWdodDogMTAwJTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxuICAgIC5yb3ctMyB7XG4gICAgICAgIHAge1xuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICAgICAgZm9udC1zaXplOiAxOHB4O1xuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDEwMDtcbiAgICAgICAgfVxuICAgIH1cbn1cblxuI3Byb2R1Y3RJbWFnZSAuc3dpcGVyLXdyYXBwZXIge1xuICAgIHRyYW5zaXRpb24tdGltaW5nLWZ1bmN0aW9uOiBlYXNlICFpbXBvcnRhbnQ7XG4gICAgei1pbmRleDogYXV0byAhaW1wb3J0YW50O1xufVxuXG4jcHJvZHVjdEltYWdlLnN3aXBlci1jb250YWluZXIge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICB6LWluZGV4OiBhdXRvICFpbXBvcnRhbnQ7XG59XG5cbi5mdWxsLXdpZHRoIHtcbiAgICB3aWR0aDogMTAwJTtcbn1cblxuLmJ0bi5ib29rbWFyayB7XG4gICAgcG9zaXRpb246IGFic29sdXRlOyBcbiAgICByaWdodDogMDtcbn1cblxuaW9uLXRvb2xiYXIge1xuICAgIC0tYmFja2dyb3VuZDogI2ZhNjQwMDtcbiAgICAtLWNvbG9yOiAjZmZmO1xufVxuXG5pb24tY2FyZC10aXRsZSB7XG4gICAgLS1jb2xvcjogI2ZhNjQwMDtcbn1cblxuaW9uLWJ1dHRvbiB7XG4gICAgLS1ib3JkZXItY29sb3I6ICNmYTY0MDA7XG4gICAgLS1jb2xvcjogI2ZhNjQwMDtcbn1cblxuLmltZy1ob2xkZXIge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICBtYXJnaW46IDAgYXV0bztcbiAgICBib3gtc2hhZG93OiAwIDRweCA2cHggLTFweCByZ2JhKDAsIDAsIDAsIDAuMSksXG4gICAgMCAycHggNHB4IC0xcHggcmdiYSgwLCAwLCAwLCAwLjA2KTtcblxuICAgIGltZyB7XG4gICAgICAgIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XG4gICAgICAgIG1heC1oZWlnaHQ6IDMyMHB4ICFpbXBvcnRhbnQ7XG4gICAgICAgIGhlaWdodDogYXV0byAhaW1wb3J0YW50O1xuICAgICAgICBvYmplY3QtZml0OiBjb250YWluO1xuICAgIH1cbn1cblxuLmRlc2NyaXB0aW9uIHtcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xuICAgIGZvbnQtc2l6ZTogMThweDtcbn1cblxuLmRlc2NyaXB0aW9uOjpmaXJzdC1sZXR0ZXIge1xuICAgIGZvbnQtc2l6ZTogMjVweCAhaW1wb3J0YW50O1xuICAgIGNvbG9yOiAgI2ZhNjQwMDtcbiAgICBtYXJnaW4tbGVmdDogMTBweDtcbn0iXX0= */";
      /***/
    },

    /***/
    "./src/app/shared/product-detail-modal/product-detail-modal.component.ts":
    /*!*******************************************************************************!*\
      !*** ./src/app/shared/product-detail-modal/product-detail-modal.component.ts ***!
      \*******************************************************************************/

    /*! exports provided: ProductDetailModalComponent */

    /***/
    function srcAppSharedProductDetailModalProductDetailModalComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ProductDetailModalComponent", function () {
        return ProductDetailModalComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ngrx_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ngrx/store */
      "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");
      /* harmony import */


      var _user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../../user/store/actions/user.action */
      "./src/app/user/store/actions/user.action.ts");
      /* harmony import */


      var _utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../../../utils/functions/app.functions */
      "./src/utils/functions/app.functions.ts");
      /* harmony import */


      var _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../../../utils/types/app.constant */
      "./src/utils/types/app.constant.ts");
      /* harmony import */


      var swiper_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! swiper/core */
      "./node_modules/swiper/swiper.esm.js"); // install Swiper components


      swiper_core__WEBPACK_IMPORTED_MODULE_6__["default"].use([swiper_core__WEBPACK_IMPORTED_MODULE_6__["Navigation"], swiper_core__WEBPACK_IMPORTED_MODULE_6__["Pagination"], swiper_core__WEBPACK_IMPORTED_MODULE_6__["A11y"], swiper_core__WEBPACK_IMPORTED_MODULE_6__["Autoplay"]]);

      var ProductDetailModalComponent = /*#__PURE__*/function () {
        function ProductDetailModalComponent(store) {
          _classCallCheck(this, ProductDetailModalComponent);

          this.store = store;
          this.openCartModal = false;
        }

        _createClass(ProductDetailModalComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            unBundleSVG();
          }
        }, {
          key: "addProductToCart",
          value: function addProductToCart(_ref75) {
            var productImageModels = _ref75.productImageModels,
                productModel = _ref75.productModel,
                productCostModel = _ref75.productCostModel;

            var _a;

            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee45() {
              var _yield$Object, userId, id, name;

              return regeneratorRuntime.wrap(function _callee45$(_context45) {
                while (1) {
                  switch (_context45.prev = _context45.next) {
                    case 0:
                      _context45.next = 2;
                      return Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_4__["getDataFromLocalStorage"])(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_5__["LocalStorageKey"].ZERO_30_USER);

                    case 2:
                      _yield$Object = _context45.sent;
                      userId = _yield$Object.userId;

                      if (userId) {
                        this.userId = userId;
                        id = productModel.id, name = productModel.name;
                        this.openCartModal = true;
                        this.productAddedToCart = {
                          cost: productCostModel.unitPrice,
                          quantity: 1,
                          productImage: ((_a = productImageModels[0]) === null || _a === void 0 ? void 0 : _a.path) || undefined,
                          productId: id,
                          productName: name
                        };
                        this.productAddedToCartImages = productImageModels;
                      }

                    case 5:
                    case "end":
                      return _context45.stop();
                  }
                }
              }, _callee45, this);
            }));
          }
        }, {
          key: "closeCartModal",
          value: function closeCartModal() {
            this.openCartModal = false;
          }
        }, {
          key: "addProductToBookmark",
          value: function addProductToBookmark(productId) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee46() {
              var _yield$Object2, userId;

              return regeneratorRuntime.wrap(function _callee46$(_context46) {
                while (1) {
                  switch (_context46.prev = _context46.next) {
                    case 0:
                      _context46.next = 2;
                      return Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_4__["getDataFromLocalStorage"])(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_5__["LocalStorageKey"].ZERO_30_USER);

                    case 2:
                      _yield$Object2 = _context46.sent;
                      userId = _yield$Object2.userId;

                      if (userId) {
                        // ? Dispatch action
                        this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_3__["actions"].AddProductToBookmarkInitiatedAction({
                          payload: {
                            userId: userId,
                            productId: productId
                          }
                        }));
                      }

                    case 5:
                    case "end":
                      return _context46.stop();
                  }
                }
              }, _callee46, this);
            }));
          }
        }]);

        return ProductDetailModalComponent;
      }();

      ProductDetailModalComponent.ctorParameters = function () {
        return [{
          type: _ngrx_store__WEBPACK_IMPORTED_MODULE_2__["Store"]
        }];
      };

      ProductDetailModalComponent.propDecorators = {
        product: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }]
      };
      ProductDetailModalComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-product-detail-modal',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./product-detail-modal.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/product-detail-modal/product-detail-modal.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./product-detail-modal.component.scss */
        "./src/app/shared/product-detail-modal/product-detail-modal.component.scss"))["default"]]
      })], ProductDetailModalComponent);
      /***/
    },

    /***/
    "./src/app/shared/product-item-widget/product-item-widget.component.scss":
    /*!*******************************************************************************!*\
      !*** ./src/app/shared/product-item-widget/product-item-widget.component.scss ***!
      \*******************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppSharedProductItemWidgetProductItemWidgetComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9wcm9kdWN0LWl0ZW0td2lkZ2V0L3Byb2R1Y3QtaXRlbS13aWRnZXQuY29tcG9uZW50LnNjc3MifQ== */";
      /***/
    },

    /***/
    "./src/app/shared/product-item-widget/product-item-widget.component.ts":
    /*!*****************************************************************************!*\
      !*** ./src/app/shared/product-item-widget/product-item-widget.component.ts ***!
      \*****************************************************************************/

    /*! exports provided: ProductItemWidgetComponent */

    /***/
    function srcAppSharedProductItemWidgetProductItemWidgetComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ProductItemWidgetComponent", function () {
        return ProductItemWidgetComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var ProductItemWidgetComponent = /*#__PURE__*/function () {
        function ProductItemWidgetComponent() {
          _classCallCheck(this, ProductItemWidgetComponent);
        }

        _createClass(ProductItemWidgetComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return ProductItemWidgetComponent;
      }();

      ProductItemWidgetComponent.ctorParameters = function () {
        return [];
      };

      ProductItemWidgetComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-product-item-widget',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./product-item-widget.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/product-item-widget/product-item-widget.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./product-item-widget.component.scss */
        "./src/app/shared/product-item-widget/product-item-widget.component.scss"))["default"]]
      })], ProductItemWidgetComponent);
      /***/
    },

    /***/
    "./src/app/shared/rating-comment/rating-comment.component.scss":
    /*!*********************************************************************!*\
      !*** ./src/app/shared/rating-comment/rating-comment.component.scss ***!
      \*********************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppSharedRatingCommentRatingCommentComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9yYXRpbmctY29tbWVudC9yYXRpbmctY29tbWVudC5jb21wb25lbnQuc2NzcyJ9 */";
      /***/
    },

    /***/
    "./src/app/shared/rating-comment/rating-comment.component.ts":
    /*!*******************************************************************!*\
      !*** ./src/app/shared/rating-comment/rating-comment.component.ts ***!
      \*******************************************************************/

    /*! exports provided: RatingCommentComponent */

    /***/
    function srcAppSharedRatingCommentRatingCommentComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RatingCommentComponent", function () {
        return RatingCommentComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var RatingCommentComponent = /*#__PURE__*/function () {
        function RatingCommentComponent() {
          _classCallCheck(this, RatingCommentComponent);
        }

        _createClass(RatingCommentComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this$merchantReview = this.merchantReview,
                _this$merchantReview$ = _this$merchantReview.user.person,
                firstName = _this$merchantReview$.firstName,
                lastName = _this$merchantReview$.lastName,
                rating = _this$merchantReview.rating;
            this.fullName = "".concat(firstName, " ").concat(lastName);
            this.styleString = "--rating: ".concat(rating);
            this.ariaLabel = "Rating of this product is ".concat(rating, " out of 5.");
          }
        }]);

        return RatingCommentComponent;
      }();

      RatingCommentComponent.ctorParameters = function () {
        return [];
      };

      RatingCommentComponent.propDecorators = {
        merchantReview: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }]
      };
      RatingCommentComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-rating-comment',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./rating-comment.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/rating-comment/rating-comment.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./rating-comment.component.scss */
        "./src/app/shared/rating-comment/rating-comment.component.scss"))["default"]]
      })], RatingCommentComponent);
      /***/
    },

    /***/
    "./src/app/shared/rider-bottom-navbar/rider-bottom-navbar.component.scss":
    /*!*******************************************************************************!*\
      !*** ./src/app/shared/rider-bottom-navbar/rider-bottom-navbar.component.scss ***!
      \*******************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppSharedRiderBottomNavbarRiderBottomNavbarComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9yaWRlci1ib3R0b20tbmF2YmFyL3JpZGVyLWJvdHRvbS1uYXZiYXIuY29tcG9uZW50LnNjc3MifQ== */";
      /***/
    },

    /***/
    "./src/app/shared/rider-bottom-navbar/rider-bottom-navbar.component.ts":
    /*!*****************************************************************************!*\
      !*** ./src/app/shared/rider-bottom-navbar/rider-bottom-navbar.component.ts ***!
      \*****************************************************************************/

    /*! exports provided: RiderBottomNavbarComponent */

    /***/
    function srcAppSharedRiderBottomNavbarRiderBottomNavbarComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RiderBottomNavbarComponent", function () {
        return RiderBottomNavbarComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var RiderBottomNavbarComponent = /*#__PURE__*/function () {
        function RiderBottomNavbarComponent() {
          _classCallCheck(this, RiderBottomNavbarComponent);
        }

        _createClass(RiderBottomNavbarComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            unBundleSVG();
          }
        }]);

        return RiderBottomNavbarComponent;
      }();

      RiderBottomNavbarComponent.ctorParameters = function () {
        return [];
      };

      RiderBottomNavbarComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-rider-bottom-navbar",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./rider-bottom-navbar.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/rider-bottom-navbar/rider-bottom-navbar.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./rider-bottom-navbar.component.scss */
        "./src/app/shared/rider-bottom-navbar/rider-bottom-navbar.component.scss"))["default"]]
      })], RiderBottomNavbarComponent);
      /***/
    },

    /***/
    "./src/app/shared/rider-delivery-history-widget/rider-delivery-history-widget.component.scss":
    /*!***************************************************************************************************!*\
      !*** ./src/app/shared/rider-delivery-history-widget/rider-delivery-history-widget.component.scss ***!
      \***************************************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppSharedRiderDeliveryHistoryWidgetRiderDeliveryHistoryWidgetComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9yaWRlci1kZWxpdmVyeS1oaXN0b3J5LXdpZGdldC9yaWRlci1kZWxpdmVyeS1oaXN0b3J5LXdpZGdldC5jb21wb25lbnQuc2NzcyJ9 */";
      /***/
    },

    /***/
    "./src/app/shared/rider-delivery-history-widget/rider-delivery-history-widget.component.ts":
    /*!*************************************************************************************************!*\
      !*** ./src/app/shared/rider-delivery-history-widget/rider-delivery-history-widget.component.ts ***!
      \*************************************************************************************************/

    /*! exports provided: RiderDeliveryHistoryWidgetComponent */

    /***/
    function srcAppSharedRiderDeliveryHistoryWidgetRiderDeliveryHistoryWidgetComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RiderDeliveryHistoryWidgetComponent", function () {
        return RiderDeliveryHistoryWidgetComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var RiderDeliveryHistoryWidgetComponent = /*#__PURE__*/function () {
        function RiderDeliveryHistoryWidgetComponent() {
          _classCallCheck(this, RiderDeliveryHistoryWidgetComponent);
        }

        _createClass(RiderDeliveryHistoryWidgetComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return RiderDeliveryHistoryWidgetComponent;
      }();

      RiderDeliveryHistoryWidgetComponent.ctorParameters = function () {
        return [];
      };

      RiderDeliveryHistoryWidgetComponent.propDecorators = {
        cart: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }]
      };
      RiderDeliveryHistoryWidgetComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-rider-delivery-history-widget',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./rider-delivery-history-widget.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/rider-delivery-history-widget/rider-delivery-history-widget.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./rider-delivery-history-widget.component.scss */
        "./src/app/shared/rider-delivery-history-widget/rider-delivery-history-widget.component.scss"))["default"]]
      })], RiderDeliveryHistoryWidgetComponent);
      /***/
    },

    /***/
    "./src/app/shared/rider-delivery-ongoing-widget/rider-delivery-ongoing-widget.component.scss":
    /*!***************************************************************************************************!*\
      !*** ./src/app/shared/rider-delivery-ongoing-widget/rider-delivery-ongoing-widget.component.scss ***!
      \***************************************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppSharedRiderDeliveryOngoingWidgetRiderDeliveryOngoingWidgetComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9yaWRlci1kZWxpdmVyeS1vbmdvaW5nLXdpZGdldC9yaWRlci1kZWxpdmVyeS1vbmdvaW5nLXdpZGdldC5jb21wb25lbnQuc2NzcyJ9 */";
      /***/
    },

    /***/
    "./src/app/shared/rider-delivery-ongoing-widget/rider-delivery-ongoing-widget.component.ts":
    /*!*************************************************************************************************!*\
      !*** ./src/app/shared/rider-delivery-ongoing-widget/rider-delivery-ongoing-widget.component.ts ***!
      \*************************************************************************************************/

    /*! exports provided: RiderDeliveryOngoingWidgetComponent */

    /***/
    function srcAppSharedRiderDeliveryOngoingWidgetRiderDeliveryOngoingWidgetComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RiderDeliveryOngoingWidgetComponent", function () {
        return RiderDeliveryOngoingWidgetComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var RiderDeliveryOngoingWidgetComponent = /*#__PURE__*/function () {
        function RiderDeliveryOngoingWidgetComponent() {
          _classCallCheck(this, RiderDeliveryOngoingWidgetComponent);
        }

        _createClass(RiderDeliveryOngoingWidgetComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return RiderDeliveryOngoingWidgetComponent;
      }();

      RiderDeliveryOngoingWidgetComponent.ctorParameters = function () {
        return [];
      };

      RiderDeliveryOngoingWidgetComponent.propDecorators = {
        cart: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }]
      };
      RiderDeliveryOngoingWidgetComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-rider-delivery-ongoing-widget",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./rider-delivery-ongoing-widget.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/rider-delivery-ongoing-widget/rider-delivery-ongoing-widget.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./rider-delivery-ongoing-widget.component.scss */
        "./src/app/shared/rider-delivery-ongoing-widget/rider-delivery-ongoing-widget.component.scss"))["default"]]
      })], RiderDeliveryOngoingWidgetComponent);
      /***/
    },

    /***/
    "./src/app/shared/search-results-widget/search-results-widget.component.scss":
    /*!***********************************************************************************!*\
      !*** ./src/app/shared/search-results-widget/search-results-widget.component.scss ***!
      \***********************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppSharedSearchResultsWidgetSearchResultsWidgetComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9zZWFyY2gtcmVzdWx0cy13aWRnZXQvc2VhcmNoLXJlc3VsdHMtd2lkZ2V0LmNvbXBvbmVudC5zY3NzIn0= */";
      /***/
    },

    /***/
    "./src/app/shared/search-results-widget/search-results-widget.component.ts":
    /*!*********************************************************************************!*\
      !*** ./src/app/shared/search-results-widget/search-results-widget.component.ts ***!
      \*********************************************************************************/

    /*! exports provided: SearchResultsWidgetComponent */

    /***/
    function srcAppSharedSearchResultsWidgetSearchResultsWidgetComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SearchResultsWidgetComponent", function () {
        return SearchResultsWidgetComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var SearchResultsWidgetComponent = /*#__PURE__*/function () {
        function SearchResultsWidgetComponent() {
          _classCallCheck(this, SearchResultsWidgetComponent);

          this.viewProductDetailEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
          this.addProductToBookmarkType = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        }

        _createClass(SearchResultsWidgetComponent, [{
          key: "viewProductDetail",
          value: function viewProductDetail() {
            this.viewProductDetailEvent.emit(true);
          }
        }, {
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "addProductToBookmark",
          value: function addProductToBookmark() {
            this.addProductToBookmarkType.emit(true);
          }
        }]);

        return SearchResultsWidgetComponent;
      }();

      SearchResultsWidgetComponent.ctorParameters = function () {
        return [];
      };

      SearchResultsWidgetComponent.propDecorators = {
        viewProductDetailEvent: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
        }],
        addProductToBookmarkType: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
        }],
        searchResult: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        isBookmarked: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }]
      };
      SearchResultsWidgetComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-search-results-widget',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./search-results-widget.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/search-results-widget/search-results-widget.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./search-results-widget.component.scss */
        "./src/app/shared/search-results-widget/search-results-widget.component.scss"))["default"]]
      })], SearchResultsWidgetComponent);
      /***/
    },

    /***/
    "./src/app/shared/shared.module.ts":
    /*!*****************************************!*\
      !*** ./src/app/shared/shared.module.ts ***!
      \*****************************************/

    /*! exports provided: SharedModule */

    /***/
    function srcAppSharedSharedModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SharedModule", function () {
        return SharedModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ngx_tiny_date_picker__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @ngx-tiny/date-picker */
      "./node_modules/@ngx-tiny/date-picker/__ivy_ngcc__/fesm2015/ngx-tiny-date-picker.js");
      /* harmony import */


      var swiper_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! swiper/angular */
      "./node_modules/swiper/__ivy_ngcc__/angular/fesm2015/swiper_angular.js");
      /* harmony import */


      var _user_bottom_navbar_user_bottom_navbar_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ./user-bottom-navbar/user-bottom-navbar.component */
      "./src/app/shared/user-bottom-navbar/user-bottom-navbar.component.ts");
      /* harmony import */


      var _order_selected_widget_history_order_selected_widget_history_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ./order-selected-widget-history/order-selected-widget-history.component */
      "./src/app/shared/order-selected-widget-history/order-selected-widget-history.component.ts");
      /* harmony import */


      var _order_selected_widget_draft_order_selected_widget_draft_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! ./order-selected-widget-draft/order-selected-widget-draft.component */
      "./src/app/shared/order-selected-widget-draft/order-selected-widget-draft.component.ts");
      /* harmony import */


      var _order_selected_widget_ongoing_order_selected_widget_ongoing_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! ./order-selected-widget-ongoing/order-selected-widget-ongoing.component */
      "./src/app/shared/order-selected-widget-ongoing/order-selected-widget-ongoing.component.ts");
      /* harmony import */


      var _no_orders_found_widget_no_orders_found_widget_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
      /*! ./no-orders-found-widget/no-orders-found-widget.component */
      "./src/app/shared/no-orders-found-widget/no-orders-found-widget.component.ts");
      /* harmony import */


      var _favorite_food_widget_favorite_food_widget_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
      /*! ./favorite-food-widget/favorite-food-widget.component */
      "./src/app/shared/favorite-food-widget/favorite-food-widget.component.ts");
      /* harmony import */


      var _favorite_resturant_widget_favorite_resturant_widget_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
      /*! ./favorite-resturant-widget/favorite-resturant-widget.component */
      "./src/app/shared/favorite-resturant-widget/favorite-resturant-widget.component.ts");
      /* harmony import */


      var _nearby_item_widget_nearby_item_widget_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
      /*! ./nearby-item-widget/nearby-item-widget.component */
      "./src/app/shared/nearby-item-widget/nearby-item-widget.component.ts");
      /* harmony import */


      var _loader_loader_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
      /*! ./loader/loader.component */
      "./src/app/shared/loader/loader.component.ts");
      /* harmony import */


      var _success_message_success_message_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(
      /*! ./success-message/success-message.component */
      "./src/app/shared/success-message/success-message.component.ts");
      /* harmony import */


      var _error_message_error_message_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(
      /*! ./error-message/error-message.component */
      "./src/app/shared/error-message/error-message.component.ts");
      /* harmony import */


      var _no_internet_modal_no_internet_modal_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(
      /*! ./no-internet-modal/no-internet-modal.component */
      "./src/app/shared/no-internet-modal/no-internet-modal.component.ts");
      /* harmony import */


      var _merchant_product_listing_merchant_product_listing_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(
      /*! ./merchant-product-listing/merchant-product-listing.component */
      "./src/app/shared/merchant-product-listing/merchant-product-listing.component.ts");
      /* harmony import */


      var _search_results_widget_search_results_widget_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(
      /*! ./search-results-widget/search-results-widget.component */
      "./src/app/shared/search-results-widget/search-results-widget.component.ts");
      /* harmony import */


      var _single_product_widget_single_product_widget_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(
      /*! ./single-product-widget/single-product-widget.component */
      "./src/app/shared/single-product-widget/single-product-widget.component.ts");
      /* harmony import */


      var _category_hightlight_widget_category_hightlight_widget_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(
      /*! ./category-hightlight-widget/category-hightlight-widget.component */
      "./src/app/shared/category-hightlight-widget/category-hightlight-widget.component.ts");
      /* harmony import */


      var _favourite_merchant_hightlight_widget_favourite_merchant_hightlight_widget_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(
      /*! ./favourite-merchant-hightlight-widget/favourite-merchant-hightlight-widget.component */
      "./src/app/shared/favourite-merchant-hightlight-widget/favourite-merchant-hightlight-widget.component.ts");
      /* harmony import */


      var _file_path_formatter_pipe__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(
      /*! ./file-path-formatter.pipe */
      "./src/app/shared/file-path-formatter.pipe.ts");
      /* harmony import */


      var _hot_sale_highlight_widget_hot_sale_highlight_widget_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(
      /*! ./hot-sale-highlight-widget/hot-sale-highlight-widget.component */
      "./src/app/shared/hot-sale-highlight-widget/hot-sale-highlight-widget.component.ts");
      /* harmony import */


      var _ordered_product_highlight_widget_ordered_product_highlight_widget_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(
      /*! ./ordered-product-highlight-widget/ordered-product-highlight-widget.component */
      "./src/app/shared/ordered-product-highlight-widget/ordered-product-highlight-widget.component.ts");
      /* harmony import */


      var _close_modal_directive__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(
      /*! ./close-modal.directive */
      "./src/app/shared/close-modal.directive.ts");
      /* harmony import */


      var _product_detail_modal_product_detail_modal_component__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(
      /*! ./product-detail-modal/product-detail-modal.component */
      "./src/app/shared/product-detail-modal/product-detail-modal.component.ts");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _slice_content_pipe__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(
      /*! ./slice-content.pipe */
      "./src/app/shared/slice-content.pipe.ts");
      /* harmony import */


      var _capitalize_pipe__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(
      /*! ./capitalize.pipe */
      "./src/app/shared/capitalize.pipe.ts");
      /* harmony import */


      var _product_category_widget_product_category_widget_component__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(
      /*! ./product-category-widget/product-category-widget.component */
      "./src/app/shared/product-category-widget/product-category-widget.component.ts");
      /* harmony import */


      var _footer_expander_footer_expander_component__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(
      /*! ./footer-expander/footer-expander.component */
      "./src/app/shared/footer-expander/footer-expander.component.ts");
      /* harmony import */


      var _rating_comment_rating_comment_component__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(
      /*! ./rating-comment/rating-comment.component */
      "./src/app/shared/rating-comment/rating-comment.component.ts");
      /* harmony import */


      var _merchant_hot_sale_product_merchant_hot_sale_product_component__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(
      /*! ./merchant-hot-sale-product/merchant-hot-sale-product.component */
      "./src/app/shared/merchant-hot-sale-product/merchant-hot-sale-product.component.ts");
      /* harmony import */


      var _category_item_widget_category_item_widget_component__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(
      /*! ./category-item-widget/category-item-widget.component */
      "./src/app/shared/category-item-widget/category-item-widget.component.ts");
      /* harmony import */


      var _product_item_widget_product_item_widget_component__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(
      /*! ./product-item-widget/product-item-widget.component */
      "./src/app/shared/product-item-widget/product-item-widget.component.ts");
      /* harmony import */


      var _cart_badge_cart_badge_component__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(
      /*! ./cart-badge/cart-badge.component */
      "./src/app/shared/cart-badge/cart-badge.component.ts");
      /* harmony import */


      var _add_to_cart_add_to_cart_component__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(
      /*! ./add-to-cart/add-to-cart.component */
      "./src/app/shared/add-to-cart/add-to-cart.component.ts");
      /* harmony import */


      var _product_cost_calculator_pipe__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(
      /*! ./product-cost-calculator.pipe */
      "./src/app/shared/product-cost-calculator.pipe.ts");
      /* harmony import */


      var _back_button_directive__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(
      /*! ./back-button.directive */
      "./src/app/shared/back-button.directive.ts");
      /* harmony import */


      var _no_content_found_no_content_found_component__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(
      /*! ./no-content-found/no-content-found.component */
      "./src/app/shared/no-content-found/no-content-found.component.ts");
      /* harmony import */


      var _clickable_mesage_clickable_mesage_component__WEBPACK_IMPORTED_MODULE_44__ = __webpack_require__(
      /*! ./clickable-mesage/clickable-mesage.component */
      "./src/app/shared/clickable-mesage/clickable-mesage.component.ts");
      /* harmony import */


      var _rider_bottom_navbar_rider_bottom_navbar_component__WEBPACK_IMPORTED_MODULE_45__ = __webpack_require__(
      /*! ./rider-bottom-navbar/rider-bottom-navbar.component */
      "./src/app/shared/rider-bottom-navbar/rider-bottom-navbar.component.ts");
      /* harmony import */


      var _conditional_bottom_navbar_conditional_bottom_navbar_component__WEBPACK_IMPORTED_MODULE_46__ = __webpack_require__(
      /*! ./conditional-bottom-navbar/conditional-bottom-navbar.component */
      "./src/app/shared/conditional-bottom-navbar/conditional-bottom-navbar.component.ts");
      /* harmony import */


      var _rider_delivery_history_widget_rider_delivery_history_widget_component__WEBPACK_IMPORTED_MODULE_47__ = __webpack_require__(
      /*! ./rider-delivery-history-widget/rider-delivery-history-widget.component */
      "./src/app/shared/rider-delivery-history-widget/rider-delivery-history-widget.component.ts");
      /* harmony import */


      var _rider_delivery_ongoing_widget_rider_delivery_ongoing_widget_component__WEBPACK_IMPORTED_MODULE_48__ = __webpack_require__(
      /*! ./rider-delivery-ongoing-widget/rider-delivery-ongoing-widget.component */
      "./src/app/shared/rider-delivery-ongoing-widget/rider-delivery-ongoing-widget.component.ts");
      /* harmony import */


      var _graph_widget_graph_widget_component__WEBPACK_IMPORTED_MODULE_49__ = __webpack_require__(
      /*! ./graph-widget/graph-widget.component */
      "./src/app/shared/graph-widget/graph-widget.component.ts");
      /* harmony import */


      var _accordian_accordian_component__WEBPACK_IMPORTED_MODULE_50__ = __webpack_require__(
      /*! ./accordian/accordian.component */
      "./src/app/shared/accordian/accordian.component.ts");

      var SharedModule = function SharedModule() {
        _classCallCheck(this, SharedModule);
      };

      SharedModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_user_bottom_navbar_user_bottom_navbar_component__WEBPACK_IMPORTED_MODULE_8__["UserBottomNavbarComponent"], _clickable_mesage_clickable_mesage_component__WEBPACK_IMPORTED_MODULE_44__["ClickableMesageComponent"], _order_selected_widget_history_order_selected_widget_history_component__WEBPACK_IMPORTED_MODULE_9__["OrderSelectedWidgetHistoryComponent"], _order_selected_widget_draft_order_selected_widget_draft_component__WEBPACK_IMPORTED_MODULE_10__["OrderSelectedWidgetDraftComponent"], _order_selected_widget_ongoing_order_selected_widget_ongoing_component__WEBPACK_IMPORTED_MODULE_11__["OrderSelectedWidgetOngoingComponent"], _no_orders_found_widget_no_orders_found_widget_component__WEBPACK_IMPORTED_MODULE_12__["NoOrdersFoundWidgetComponent"], _favorite_food_widget_favorite_food_widget_component__WEBPACK_IMPORTED_MODULE_13__["FavoriteFoodWidgetComponent"], _favorite_resturant_widget_favorite_resturant_widget_component__WEBPACK_IMPORTED_MODULE_14__["FavoriteResturantWidgetComponent"], _nearby_item_widget_nearby_item_widget_component__WEBPACK_IMPORTED_MODULE_15__["NearbyItemWidgetComponent"], _loader_loader_component__WEBPACK_IMPORTED_MODULE_16__["LoaderComponent"], _success_message_success_message_component__WEBPACK_IMPORTED_MODULE_17__["SuccessMessageComponent"], _error_message_error_message_component__WEBPACK_IMPORTED_MODULE_18__["ErrorMessageComponent"], _no_internet_modal_no_internet_modal_component__WEBPACK_IMPORTED_MODULE_19__["NoInternetModalComponent"], _merchant_product_listing_merchant_product_listing_component__WEBPACK_IMPORTED_MODULE_20__["MerchantProductListingComponent"], _search_results_widget_search_results_widget_component__WEBPACK_IMPORTED_MODULE_21__["SearchResultsWidgetComponent"], _single_product_widget_single_product_widget_component__WEBPACK_IMPORTED_MODULE_22__["SingleProductWidgetComponent"], _category_hightlight_widget_category_hightlight_widget_component__WEBPACK_IMPORTED_MODULE_23__["CategoryHightlightWidgetComponent"], _favourite_merchant_hightlight_widget_favourite_merchant_hightlight_widget_component__WEBPACK_IMPORTED_MODULE_24__["FavouriteMerchantHightlightWidgetComponent"], _hot_sale_highlight_widget_hot_sale_highlight_widget_component__WEBPACK_IMPORTED_MODULE_26__["HotSaleHighlightWidgetComponent"], _ordered_product_highlight_widget_ordered_product_highlight_widget_component__WEBPACK_IMPORTED_MODULE_27__["OrderedProductHighlightWidgetComponent"], _product_detail_modal_product_detail_modal_component__WEBPACK_IMPORTED_MODULE_29__["ProductDetailModalComponent"], _product_category_widget_product_category_widget_component__WEBPACK_IMPORTED_MODULE_33__["ProductCategoryWidgetComponent"], _product_item_widget_product_item_widget_component__WEBPACK_IMPORTED_MODULE_38__["ProductItemWidgetComponent"], _footer_expander_footer_expander_component__WEBPACK_IMPORTED_MODULE_34__["FooterExpanderComponent"], _rating_comment_rating_comment_component__WEBPACK_IMPORTED_MODULE_35__["RatingCommentComponent"], _merchant_hot_sale_product_merchant_hot_sale_product_component__WEBPACK_IMPORTED_MODULE_36__["MerchantHotSaleProductComponent"], _category_item_widget_category_item_widget_component__WEBPACK_IMPORTED_MODULE_37__["CategoryItemWidgetComponent"], _cart_badge_cart_badge_component__WEBPACK_IMPORTED_MODULE_39__["CartBadgeComponent"], _add_to_cart_add_to_cart_component__WEBPACK_IMPORTED_MODULE_40__["AddToCartComponent"], _no_content_found_no_content_found_component__WEBPACK_IMPORTED_MODULE_43__["NoContentFoundComponent"], _rider_bottom_navbar_rider_bottom_navbar_component__WEBPACK_IMPORTED_MODULE_45__["RiderBottomNavbarComponent"], _conditional_bottom_navbar_conditional_bottom_navbar_component__WEBPACK_IMPORTED_MODULE_46__["ConditionalBottomNavbarComponent"], _rider_delivery_history_widget_rider_delivery_history_widget_component__WEBPACK_IMPORTED_MODULE_47__["RiderDeliveryHistoryWidgetComponent"], _rider_delivery_ongoing_widget_rider_delivery_ongoing_widget_component__WEBPACK_IMPORTED_MODULE_48__["RiderDeliveryOngoingWidgetComponent"], _accordian_accordian_component__WEBPACK_IMPORTED_MODULE_50__["AccordianComponent"], _graph_widget_graph_widget_component__WEBPACK_IMPORTED_MODULE_49__["GraphWidgetComponent"], _file_path_formatter_pipe__WEBPACK_IMPORTED_MODULE_25__["FilePathFormatterPipe"], _close_modal_directive__WEBPACK_IMPORTED_MODULE_28__["CloseModalDirective"], _slice_content_pipe__WEBPACK_IMPORTED_MODULE_31__["SliceContentPipe"], _capitalize_pipe__WEBPACK_IMPORTED_MODULE_32__["CapitalizePipe"], _product_cost_calculator_pipe__WEBPACK_IMPORTED_MODULE_41__["ProductCostCalculatorPipe"], _back_button_directive__WEBPACK_IMPORTED_MODULE_42__["BackButtonDirective"]],
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_30__["IonicModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"], _ngx_tiny_date_picker__WEBPACK_IMPORTED_MODULE_6__["NgxDatePickerModule"], swiper_angular__WEBPACK_IMPORTED_MODULE_7__["SwiperModule"]],
        exports: [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"], _user_bottom_navbar_user_bottom_navbar_component__WEBPACK_IMPORTED_MODULE_8__["UserBottomNavbarComponent"], _order_selected_widget_history_order_selected_widget_history_component__WEBPACK_IMPORTED_MODULE_9__["OrderSelectedWidgetHistoryComponent"], _order_selected_widget_draft_order_selected_widget_draft_component__WEBPACK_IMPORTED_MODULE_10__["OrderSelectedWidgetDraftComponent"], _order_selected_widget_ongoing_order_selected_widget_ongoing_component__WEBPACK_IMPORTED_MODULE_11__["OrderSelectedWidgetOngoingComponent"], _no_orders_found_widget_no_orders_found_widget_component__WEBPACK_IMPORTED_MODULE_12__["NoOrdersFoundWidgetComponent"], _favorite_food_widget_favorite_food_widget_component__WEBPACK_IMPORTED_MODULE_13__["FavoriteFoodWidgetComponent"], _favorite_resturant_widget_favorite_resturant_widget_component__WEBPACK_IMPORTED_MODULE_14__["FavoriteResturantWidgetComponent"], _nearby_item_widget_nearby_item_widget_component__WEBPACK_IMPORTED_MODULE_15__["NearbyItemWidgetComponent"], _loader_loader_component__WEBPACK_IMPORTED_MODULE_16__["LoaderComponent"], _success_message_success_message_component__WEBPACK_IMPORTED_MODULE_17__["SuccessMessageComponent"], _error_message_error_message_component__WEBPACK_IMPORTED_MODULE_18__["ErrorMessageComponent"], _no_internet_modal_no_internet_modal_component__WEBPACK_IMPORTED_MODULE_19__["NoInternetModalComponent"], _merchant_product_listing_merchant_product_listing_component__WEBPACK_IMPORTED_MODULE_20__["MerchantProductListingComponent"], _search_results_widget_search_results_widget_component__WEBPACK_IMPORTED_MODULE_21__["SearchResultsWidgetComponent"], _single_product_widget_single_product_widget_component__WEBPACK_IMPORTED_MODULE_22__["SingleProductWidgetComponent"], _category_hightlight_widget_category_hightlight_widget_component__WEBPACK_IMPORTED_MODULE_23__["CategoryHightlightWidgetComponent"], _favourite_merchant_hightlight_widget_favourite_merchant_hightlight_widget_component__WEBPACK_IMPORTED_MODULE_24__["FavouriteMerchantHightlightWidgetComponent"], _hot_sale_highlight_widget_hot_sale_highlight_widget_component__WEBPACK_IMPORTED_MODULE_26__["HotSaleHighlightWidgetComponent"], _ordered_product_highlight_widget_ordered_product_highlight_widget_component__WEBPACK_IMPORTED_MODULE_27__["OrderedProductHighlightWidgetComponent"], _product_detail_modal_product_detail_modal_component__WEBPACK_IMPORTED_MODULE_29__["ProductDetailModalComponent"], _product_category_widget_product_category_widget_component__WEBPACK_IMPORTED_MODULE_33__["ProductCategoryWidgetComponent"], _footer_expander_footer_expander_component__WEBPACK_IMPORTED_MODULE_34__["FooterExpanderComponent"], _rating_comment_rating_comment_component__WEBPACK_IMPORTED_MODULE_35__["RatingCommentComponent"], _merchant_hot_sale_product_merchant_hot_sale_product_component__WEBPACK_IMPORTED_MODULE_36__["MerchantHotSaleProductComponent"], _category_item_widget_category_item_widget_component__WEBPACK_IMPORTED_MODULE_37__["CategoryItemWidgetComponent"], _product_item_widget_product_item_widget_component__WEBPACK_IMPORTED_MODULE_38__["ProductItemWidgetComponent"], _cart_badge_cart_badge_component__WEBPACK_IMPORTED_MODULE_39__["CartBadgeComponent"], _add_to_cart_add_to_cart_component__WEBPACK_IMPORTED_MODULE_40__["AddToCartComponent"], _no_content_found_no_content_found_component__WEBPACK_IMPORTED_MODULE_43__["NoContentFoundComponent"], _clickable_mesage_clickable_mesage_component__WEBPACK_IMPORTED_MODULE_44__["ClickableMesageComponent"], _rider_bottom_navbar_rider_bottom_navbar_component__WEBPACK_IMPORTED_MODULE_45__["RiderBottomNavbarComponent"], _conditional_bottom_navbar_conditional_bottom_navbar_component__WEBPACK_IMPORTED_MODULE_46__["ConditionalBottomNavbarComponent"], _rider_delivery_history_widget_rider_delivery_history_widget_component__WEBPACK_IMPORTED_MODULE_47__["RiderDeliveryHistoryWidgetComponent"], _rider_delivery_ongoing_widget_rider_delivery_ongoing_widget_component__WEBPACK_IMPORTED_MODULE_48__["RiderDeliveryOngoingWidgetComponent"], _graph_widget_graph_widget_component__WEBPACK_IMPORTED_MODULE_49__["GraphWidgetComponent"], _accordian_accordian_component__WEBPACK_IMPORTED_MODULE_50__["AccordianComponent"], _file_path_formatter_pipe__WEBPACK_IMPORTED_MODULE_25__["FilePathFormatterPipe"], _slice_content_pipe__WEBPACK_IMPORTED_MODULE_31__["SliceContentPipe"], _capitalize_pipe__WEBPACK_IMPORTED_MODULE_32__["CapitalizePipe"], _product_cost_calculator_pipe__WEBPACK_IMPORTED_MODULE_41__["ProductCostCalculatorPipe"], _close_modal_directive__WEBPACK_IMPORTED_MODULE_28__["CloseModalDirective"], _back_button_directive__WEBPACK_IMPORTED_MODULE_42__["BackButtonDirective"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"], _ngx_tiny_date_picker__WEBPACK_IMPORTED_MODULE_6__["NgxDatePickerModule"], swiper_angular__WEBPACK_IMPORTED_MODULE_7__["SwiperModule"]]
      })], SharedModule);
      /***/
    },

    /***/
    "./src/app/shared/single-product-widget/single-product-widget.component.scss":
    /*!***********************************************************************************!*\
      !*** ./src/app/shared/single-product-widget/single-product-widget.component.scss ***!
      \***********************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppSharedSingleProductWidgetSingleProductWidgetComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9zaW5nbGUtcHJvZHVjdC13aWRnZXQvc2luZ2xlLXByb2R1Y3Qtd2lkZ2V0LmNvbXBvbmVudC5zY3NzIn0= */";
      /***/
    },

    /***/
    "./src/app/shared/single-product-widget/single-product-widget.component.ts":
    /*!*********************************************************************************!*\
      !*** ./src/app/shared/single-product-widget/single-product-widget.component.ts ***!
      \*********************************************************************************/

    /*! exports provided: SingleProductWidgetComponent */

    /***/
    function srcAppSharedSingleProductWidgetSingleProductWidgetComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SingleProductWidgetComponent", function () {
        return SingleProductWidgetComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var SingleProductWidgetComponent = /*#__PURE__*/function () {
        function SingleProductWidgetComponent() {
          _classCallCheck(this, SingleProductWidgetComponent);

          this.viewProductDetailEvent = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        }

        _createClass(SingleProductWidgetComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "viewProductDetail",
          value: function viewProductDetail() {
            this.viewProductDetailEvent.emit(true);
          }
        }]);

        return SingleProductWidgetComponent;
      }();

      SingleProductWidgetComponent.ctorParameters = function () {
        return [];
      };

      SingleProductWidgetComponent.propDecorators = {
        product: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        viewProductDetailEvent: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
        }]
      };
      SingleProductWidgetComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-single-product-widget',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./single-product-widget.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/single-product-widget/single-product-widget.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./single-product-widget.component.scss */
        "./src/app/shared/single-product-widget/single-product-widget.component.scss"))["default"]]
      })], SingleProductWidgetComponent);
      /***/
    },

    /***/
    "./src/app/shared/slice-content.pipe.ts":
    /*!**********************************************!*\
      !*** ./src/app/shared/slice-content.pipe.ts ***!
      \**********************************************/

    /*! exports provided: SliceContentPipe */

    /***/
    function srcAppSharedSliceContentPipeTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SliceContentPipe", function () {
        return SliceContentPipe;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /**
       * This pipe splits an array into a specified number of values allowing a specific number
       * Used for Previews
       */


      var SliceContentPipe = /*#__PURE__*/function () {
        function SliceContentPipe() {
          _classCallCheck(this, SliceContentPipe);
        }

        _createClass(SliceContentPipe, [{
          key: "transform",
          value: function transform(value, numberOfElements) {
            if ((value === null || value === void 0 ? void 0 : value.length) > 0) {
              if (numberOfElements <= value.length - 1) {
                return value.slice(0, numberOfElements);
              }
            }

            return value;
          }
        }]);

        return SliceContentPipe;
      }();

      SliceContentPipe = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
        name: 'sliceContent'
      })], SliceContentPipe);
      /***/
    },

    /***/
    "./src/app/shared/success-message/success-message.component.scss":
    /*!***********************************************************************!*\
      !*** ./src/app/shared/success-message/success-message.component.scss ***!
      \***********************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppSharedSuccessMessageSuccessMessageComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9zdWNjZXNzLW1lc3NhZ2Uvc3VjY2Vzcy1tZXNzYWdlLmNvbXBvbmVudC5zY3NzIn0= */";
      /***/
    },

    /***/
    "./src/app/shared/success-message/success-message.component.ts":
    /*!*********************************************************************!*\
      !*** ./src/app/shared/success-message/success-message.component.ts ***!
      \*********************************************************************/

    /*! exports provided: SuccessMessageComponent */

    /***/
    function srcAppSharedSuccessMessageSuccessMessageComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SuccessMessageComponent", function () {
        return SuccessMessageComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");

      var SuccessMessageComponent = /*#__PURE__*/function () {
        function SuccessMessageComponent(toastController) {
          _classCallCheck(this, SuccessMessageComponent);

          this.toastController = toastController;
          this.closeAlert = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        }

        _createClass(SuccessMessageComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee47() {
              return regeneratorRuntime.wrap(function _callee47$(_context47) {
                while (1) {
                  switch (_context47.prev = _context47.next) {
                    case 0:
                      _context47.next = 2;
                      return this.presentToastWithOptions();

                    case 2:
                    case "end":
                      return _context47.stop();
                  }
                }
              }, _callee47, this);
            }));
          }
        }, {
          key: "presentToast",
          value: function presentToast() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee48() {
              var toast;
              return regeneratorRuntime.wrap(function _callee48$(_context48) {
                while (1) {
                  switch (_context48.prev = _context48.next) {
                    case 0:
                      _context48.next = 2;
                      return this.toastController.create({
                        message: this.message,
                        duration: 5000
                      });

                    case 2:
                      toast = _context48.sent;
                      toast.present();

                    case 4:
                    case "end":
                      return _context48.stop();
                  }
                }
              }, _callee48, this);
            }));
          }
        }, {
          key: "presentToastWithOptions",
          value: function presentToastWithOptions() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee49() {
              var _this17 = this;

              var toast;
              return regeneratorRuntime.wrap(function _callee49$(_context49) {
                while (1) {
                  switch (_context49.prev = _context49.next) {
                    case 0:
                      _context49.next = 2;
                      return this.toastController.create({
                        message: this.message,
                        duration: 5000,
                        position: 'top',
                        keyboardClose: true,
                        color: "dark",
                        buttons: [{
                          text: 'Close',
                          role: 'cancel',
                          handler: function handler() {
                            _this17.closeAlert.emit(true);
                          }
                        }]
                      });

                    case 2:
                      toast = _context49.sent;
                      toast.present();

                    case 4:
                    case "end":
                      return _context49.stop();
                  }
                }
              }, _callee49, this);
            }));
          }
        }]);

        return SuccessMessageComponent;
      }();

      SuccessMessageComponent.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]
        }];
      };

      SuccessMessageComponent.propDecorators = {
        message: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        closeAlert: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
        }]
      };
      SuccessMessageComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-success-message',
        template: "",
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./success-message.component.scss */
        "./src/app/shared/success-message/success-message.component.scss"))["default"]]
      })], SuccessMessageComponent);
      /***/
    },

    /***/
    "./src/app/shared/user-bottom-navbar/user-bottom-navbar.component.scss":
    /*!*****************************************************************************!*\
      !*** ./src/app/shared/user-bottom-navbar/user-bottom-navbar.component.scss ***!
      \*****************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppSharedUserBottomNavbarUserBottomNavbarComponentScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC91c2VyLWJvdHRvbS1uYXZiYXIvdXNlci1ib3R0b20tbmF2YmFyLmNvbXBvbmVudC5zY3NzIn0= */";
      /***/
    },

    /***/
    "./src/app/shared/user-bottom-navbar/user-bottom-navbar.component.ts":
    /*!***************************************************************************!*\
      !*** ./src/app/shared/user-bottom-navbar/user-bottom-navbar.component.ts ***!
      \***************************************************************************/

    /*! exports provided: UserBottomNavbarComponent */

    /***/
    function srcAppSharedUserBottomNavbarUserBottomNavbarComponentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UserBottomNavbarComponent", function () {
        return UserBottomNavbarComponent;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var UserBottomNavbarComponent = /*#__PURE__*/function () {
        function UserBottomNavbarComponent() {
          _classCallCheck(this, UserBottomNavbarComponent);
        }

        _createClass(UserBottomNavbarComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            unBundleSVG();
          }
        }]);

        return UserBottomNavbarComponent;
      }();

      UserBottomNavbarComponent.ctorParameters = function () {
        return [];
      };

      UserBottomNavbarComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-user-bottom-navbar",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./user-bottom-navbar.component.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/user-bottom-navbar/user-bottom-navbar.component.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./user-bottom-navbar.component.scss */
        "./src/app/shared/user-bottom-navbar/user-bottom-navbar.component.scss"))["default"]]
      })], UserBottomNavbarComponent);
      /***/
    },

    /***/
    "./src/app/store/actions/app.action.ts":
    /*!*********************************************!*\
      !*** ./src/app/store/actions/app.action.ts ***!
      \*********************************************/

    /*! exports provided: AppActionType, actions */

    /***/
    function srcAppStoreActionsAppActionTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AppActionType", function () {
        return AppActionType;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "actions", function () {
        return actions;
      });
      /* harmony import */


      var _ngrx_store__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @ngrx/store */
      "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");

      var AppActionType;

      (function (AppActionType) {
        AppActionType["CLEAR_APP_STATE"] = "[APP_ROOT] CLEAR_APP_STATE";
      })(AppActionType || (AppActionType = {}));

      var actions = {
        ClearAppStateAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(AppActionType.CLEAR_APP_STATE)
      };
      /***/
    },

    /***/
    "./src/app/store/model/app.model.ts":
    /*!******************************************!*\
      !*** ./src/app/store/model/app.model.ts ***!
      \******************************************/

    /*! exports provided: InitialAppState */

    /***/
    function srcAppStoreModelAppModelTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "InitialAppState", function () {
        return InitialAppState;
      });

      var InitialAppState = {
        User: undefined,
        Rider: undefined,
        Auth: undefined
      };
      /***/
    },

    /***/
    "./src/app/store/reducer/app.reducer.ts":
    /*!**********************************************!*\
      !*** ./src/app/store/reducer/app.reducer.ts ***!
      \**********************************************/

    /*! exports provided: AppReducer */

    /***/
    function srcAppStoreReducerAppReducerTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AppReducer", function () {
        return AppReducer;
      });
      /* harmony import */


      var _ngrx_store__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @ngrx/store */
      "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");
      /* harmony import */


      var _model_app_model__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ../model/app.model */
      "./src/app/store/model/app.model.ts");
      /* harmony import */


      var _actions_app_action__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../actions/app.action */
      "./src/app/store/actions/app.action.ts");

      var AppReducer = Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createReducer"])(_model_app_model__WEBPACK_IMPORTED_MODULE_1__["InitialAppState"], Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_app_action__WEBPACK_IMPORTED_MODULE_2__["actions"].ClearAppStateAction, function (state) {
        return {
          User: undefined,
          Auth: undefined,
          Rider: undefined
        };
      }));
      /***/
    },

    /***/
    "./src/app/user/store/actions/user.action.ts":
    /*!***************************************************!*\
      !*** ./src/app/user/store/actions/user.action.ts ***!
      \***************************************************/

    /*! exports provided: UserActionType, actions */

    /***/
    function srcAppUserStoreActionsUserActionTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UserActionType", function () {
        return UserActionType;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "actions", function () {
        return actions;
      });
      /* harmony import */


      var _ngrx_store__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @ngrx/store */
      "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");

      var UserActionType;

      (function (UserActionType) {
        UserActionType["CLEAR_ACTIVE_ERROR"] = "[ERROR_USER] CLEAR_ACTIVE_ERROR";
        UserActionType["CLEAR_ACTIVE_MESSAGE"] = "[ERROR_USER] CLEAR_ACTIVE_MESSAGE";
        UserActionType["CLEAR_ACTIVE_ACTION_MESSAGE"] = "[ERROR_USER] CLEAR_ACTIVE_ACTION_MESSAGE";
        UserActionType["GET_PRODUCTS_BY_MERCHANT_INITIATED"] = "[PRODUCT DISCOVERY] GET_PRODUCTS_BY_MERCHANT_INITIATED";
        UserActionType["GET_PRODUCTS_BY_MERCHANT_FAILED"] = "[PRODUCT DISCOVERY] GET_PRODUCTS_BY_MERCHANT_FAILED";
        UserActionType["GET_PRODUCTS_BY_MERCHANT_SUCCESSFUL"] = "[PRODUCT DISCOVERY] GET_PRODUCTS_BY_MERCHANT_SUCCESSFUL";
        UserActionType["SEARCH_FOR_PRODUCTS_INITIATED"] = "[VIEW_SEARCH_RESULTS] SEARCH_FOR_PRODUCTS_INITIATED";
        UserActionType["SEARCH_FOR_PRODUCTS_FAILED"] = "[VIEW_SEARCH_RESULTS] SEARCH_FOR_PRODUCTS_FAILED";
        UserActionType["SEARCH_FOR_PRODUCTS_SUCCESSFUL"] = "[VIEW_SEARCH_RESULTS] SEARCH_FOR_PRODUCTS_SUCCESSFUL";
        UserActionType["GET_USER_FAVOURITES_INITIATED"] = "[PRODUCT_DISCOVERY] GET_USER_FAVOURITES_INITIATED";
        UserActionType["GET_USER_FAVOURITES_FAILED"] = "[PRODUCT_DISCOVERY] GET_USER_FAVOURITES_FAILED";
        UserActionType["GET_USER_FAVOURITES_SUCCESSFUL"] = "[PRODUCT_DISCOVERY] GET_USER_FAVOURITES_SUCCESSFUL";
        UserActionType["ADD_MERCHANT_TO_USER_BOOKMARKS_INITIATED"] = "[BOOKMARKS] ADD_MERCHANT_TO_USER_BOOKMARKS_INITIATED";
        UserActionType["ADD_MERCHANT_TO_USER_BOOKMARKS_FAILED"] = "[BOOKMARKS] ADD_MERCHANT_TO_USER_BOOKMARKS_FAILED";
        UserActionType["ADD_MERCHANT_TO_USER_BOOKMARKS_SUCCESSFUL"] = "[BOOKMARKS] ADD_MERCHANT_TO_USER_BOOKMARKS_SUCCESSFUL";
        UserActionType["GET_PRODUCTS_ON_HOT_SALE_INITIATED"] = "[HOT_SALE] GET_PRODUCTS_ON_HOT_SALE_INITIATED";
        UserActionType["GET_PRODUCTS_ON_HOT_SALE_FAILED"] = "[HOT_SALE] GET_PRODUCTS_ON_HOT_SALE_FAILED";
        UserActionType["GET_PRODUCTS_ON_HOT_SALE_SUCCESSFUL"] = "[HOT_SALE] GET_PRODUCTS_ON_HOT_SALE_SUCCESSFUL";
        UserActionType["GET_USER_ORDER_HISTORY_INITIATED"] = "[ORDER_HISTORY] GET_USER_ORDER_HISTORY_INITIATED";
        UserActionType["GET_USER_ORDER_HISTORY_FAILED"] = "[ORDER_HISTORY] GET_USER_ORDER_HISTORY_FAILED";
        UserActionType["GET_USER_ORDER_HISTORY_SUCCESSFUL"] = "[ORDER_HISTORY] GET_USER_ORDER_HISTORY_SUCCESSFUL";
        UserActionType["ADD_PRODUCT_TO_BOOKMARK_INITIATED"] = "[BOOKMARK_PRODUCT] ADD_PRODUCT_TO_BOOKMARK_INITIATED";
        UserActionType["ADD_PRODUCT_TO_BOOKMARK_FAILED"] = "[BOOKMARK_PRODUCT] ADD_PRODUCT_TO_BOOKMARK_FAILED";
        UserActionType["ADD_PRODUCT_TO_BOOKMARK_SUCCESSFUL"] = "[BOOKMARK_PRODUCT] ADD_PRODUCT_TO_BOOKMARK_SUCCESSFUL";
        UserActionType["GET_USER_INFO_INITIATED"] = "[USER_INFO] GET_USER_INFO_INITIATED";
        UserActionType["GET_USER_INFO_FAILED"] = "[USER_INFO] GET_USER_INFO_FAILED";
        UserActionType["GET_USER_INFO_SUCCESSFUL"] = "[USER_INFO] GET_USER_INFO_SUCCESSFUL";
        UserActionType["UPDATE_USER_PROFILE_INITIATED"] = "[USER_PROFILE] UPDATE_USER_PROFILE_INITIATED";
        UserActionType["UPDATE_USER_PROFILE_FAILED"] = "[USER_PROFILE] UPDATE_USER_PROFILE_FAILED";
        UserActionType["UPDATE_USER_PROFILE_SUCCESSFUL"] = "[USER_PROFILE] UPDATE_USER_PROFILE_SUCCESSFUL";
        UserActionType["UPLOAD_PROFILE_IMAGE_INITIATED"] = "[USER_PROFILE] UPLOAD_PROFILE_IMAGE_INITIATED";
        UserActionType["UPLOAD_PROFILE_IMAGE_FAILED"] = "[USER_PROFILE] UPLOAD_PROFILE_IMAGE_FAILED";
        UserActionType["UPLOAD_PROFILE_IMAGE_SUCCESSFUL"] = "[USER_PROFILE] UPLOAD_PROFILE_IMAGE_SUCCESSFUL";
        UserActionType["GET_MERCHANT_TYPE_INITIATED"] = "[MERCHANT_TYPE] GET_MERCHANT_TYPE_INITIATED";
        UserActionType["GET_MERCHANT_TYPE_FAILED"] = "[MERCHANT_TYPE] GET_MERCHANT_TYPE_FAILED";
        UserActionType["GET_MERCHANT_TYPE_SUCCESSFUL"] = "[MERCHANT_TYPE] GET_MERCHANT_TYPE_SUCCESSFUL";
        UserActionType["GET_MERCHANT_BY_TYPE_INITIATED"] = "[MERCHANTS] GET_MERCHANT_BY_TYPE_INITIATED";
        UserActionType["GET_MERCHANT_BY_TYPE_FAILED"] = "[MERCHANTS] GET_MERCHANT_BY_TYPE_FAILED";
        UserActionType["GET_MERCHANT_BY_TYPE_SUCCESSFUL"] = "[MERCHANTS] GET_MERCHANT_BY_TYPE_SUCCESSFUL";
        UserActionType["GET_MERCHANT_DETAIL_INITIATED"] = "[BUSINESS_DETAIL] GET_BUSINESS_DETAIL_INITIATED";
        UserActionType["GET_MERCHANT_DETAIL_FAILED"] = "[BUSINESS_DETAIL] GET_BUSINESS_DETAIL_FAILED";
        UserActionType["GET_MERCHANT_DETAIL_SUCCESSFUL"] = "[BUSINESS_DETAIL] GET_BUSINESS_DETAIL_SUCCESSFUL";
        UserActionType["ADD_MERCHANT_RATING_INITIATED"] = "[RATING] ADD_MERCHANT_RATING_INITIATED";
        UserActionType["ADD_MERCHANT_RATING_FAILED"] = "[RATING] ADD_RATE_MERCHANT_FAILEDADD_MERCHANT_RATING_FAILED";
        UserActionType["ADD_MERCHANT_RATING_SUCCESSFUL"] = "[RATING] ADD_MERCHANT_RATING_SUCCESSFUL";
        UserActionType["GET_MERCHANT_REVIEWS_INTIIATED"] = "[RATING] GET_MERCHANT_REVIEWS_INTIIATED";
        UserActionType["GET_MERCHANT_REVIEWS_FAILED"] = "[RATING] GET_MERCHANT_REVIEWS_FAILED";
        UserActionType["GET_MERCHANT_REVIEWS_SUCCESSFUL"] = "[RATING] GET_MERCHANT_REVIEWS_SUCCESSFUL";
        UserActionType["GET_HOT_SALE_PRODUCT_FOR_MERCHANT_INITIATED"] = "[HOT_SALE] GET_HOT_SALE_PRODUCT_FOR_MERCHANT_INITIATED";
        UserActionType["GET_HOT_SALE_PRODUCT_FOR_MERCHANT_FAILED"] = "[HOT_SALE] GET_HOT_SALE_PRODUCT_FOR_MERCHANT_FAILED";
        UserActionType["GET_HOT_SALE_PRODUCT_FOR_MERCHANT_SUCCESSFUL"] = "[HOT_SALE] GET_HOT_SALE_PRODUCT_FOR_MERCHANT_SUCCESSFUL";
        UserActionType["GET_MERCHANT_PRODUCTS_GROUPED_BY_CATEGORY_INITIATED"] = "[HOT SALE] GET_MERCHANT_PRODUCTS_GROUPED_BY_CATEGORY_INITIATED";
        UserActionType["GET_MERCHANT_PRODUCTS_GROUPED_BY_CATEGORY_FAILED"] = "[HOT SALE] GET_MERCHANT_PRODUCTS_GROUPED_BY_CATEGORY_FAILED";
        UserActionType["GET_MERCHANT_PRODUCTS_GROUPED_BY_CATEGORY_SUCCESSFUL"] = "[HOT SALE] GET_MERCHANT_PRODUCTS_GROUPED_BY_CATEGORY_SUCCESSFUL";
        UserActionType["GET_PRODUCTS_GROUPED_BY_CATEGORY_INITIATED"] = "[PRODUCTS] GET_PRODUCTS_GROUPED_BY_CATEGORY_INITIATED";
        UserActionType["GET_PRODUCTS_GROUPED_BY_CATEGORY_FAILED"] = "[PRODUCTS] GET_PRODUCTS_GROUPED_BY_CATEGORY_FAILES";
        UserActionType["GET_PRODUCTS_GROUPED_BY_CATEGORY_SUCCESSFUL"] = "[PRODUCTS] GET_PRODUCTS_GROUPED_BY_CATEGORY_SUCCESSFUL";
        UserActionType["ADD_PRODUCT_TO_CART_INITIATED"] = "[CART] ADD_PRODUCT_TO_CART_INITIATED";
        UserActionType["ADD_PRODUCT_TO_CART_FAILED"] = "[CART] ADD_PRODUCT_TO_CART_FAILED";
        UserActionType["ADD_PRODUCT_TO_CART_SUCCESSFUL"] = "[CART] ADD_PRODUCT_TO_CART_SUCCESSFUL";
        UserActionType["CART_CHECKOUT_INITIATED"] = "[CART] CART_CHECKOUT_INITIATED";
        UserActionType["CART_CHECKOUT_FAILED"] = "[CART] CART_CHECKOUT_FAILED";
        UserActionType["CART_CHECKOUT_SUCCESSFUL"] = "[CART] CART_CHECKOUT_SUCCESSFUL";
        UserActionType["REMOVE_PRODUCT_FROM_CART_INITIATED"] = "[CART] REMOVE_PRODUCT_FROM_CART_INITIATED";
        UserActionType["REMOVE_PRODUCT_FROM_CART_FAILED"] = "[CART] REMOVE_PRODUCT_FROM_CART_FAILED";
        UserActionType["REMOVE_PRODUCT_FROM_CART_SUCCESSFUL"] = "[CART] REMOVE_PRODUCT_FROM_CART_SUCCESSFUL";
        UserActionType["GET_SINGLE_PRODUCT_INITIATED"] = "[CART] GET_SINGLE_PRODUCT_INITIATED";
        UserActionType["GET_SINGLE_PRODUCT_FAILED"] = "[CART] GET_SINGLE_PRODUCT_FAILED";
        UserActionType["GET_SINGLE_PRODUCT_SUCCESSFUL"] = "[CART] GET_SINGLE_PRODUCT_SUCCESSFUL";
        UserActionType["MAKE_PAYMENT_INITIATED"] = "[PAYMENT] MAKE_PAYMENT_INITIATED";
        UserActionType["MAKE_PAYMENT_FAILED"] = "[PAYMENT] MAKE_PAYMENT_FAILED";
        UserActionType["MAKE_PAYMENT_SUCCESSFUL"] = "[PAYMENT] MAKE_PAYMENT_SUCCESSFUL";
        UserActionType["CONFIRM_PAYSTACK_PAYMENT_INITIATED"] = "[PAYMENT] CONFIRM_PAYSTACK_PAYMENT_INITIATED";
        UserActionType["CONFIRM_PAYSTACK_PAYMENT_FAILED"] = "[PAYMENT] CONFIRM_PAYSTACK_PAYMENT_FAILED";
        UserActionType["CONFIRM_PAYSTACK_PAYMENT_SUCCESSFUL"] = "[PAYMENT] CONFIRM_PAYSTACK_PAYMENT_SUCCESSFUL";
        UserActionType["GET_USER_ORDERS_INITIATED"] = "[USER_ORDERS] GET_ORDERS_INITIATED";
        UserActionType["GET_USER_ORDERS_FAILED"] = "[USER_ORDERS] GET_ORDERS_FAILED";
        UserActionType["GET_USER_ORDERS_SUCCESSFUL"] = "[USER_ORDERS] GET_ORDERS_SUCCESSFUL";
        UserActionType["GET_SELECTED_CART_INITIATED"] = "[ORDER] GET_SELECTED_CART_INITIATED";
        UserActionType["GET_SELECTED_CART_FAILED"] = "[ORDER] GET_SELECTED_CART_FAILED";
        UserActionType["GET_SELECTED_CART_SUCCESSFUL"] = "[ORDER] GET_SELECTED_CART_SUCCESSFUL";
        UserActionType["RATE_RIDER_INITIATED"] = "[RIDER_RATING] RATE_RIDER_INITIATED";
        UserActionType["RATE_RIDER_FAILED"] = "[RIDER_RATING] RATE_RIDER_FAILED";
        UserActionType["RATE_RIDER_SUCCESSFUL"] = "[RIDER_RATING] RATE_RIDER_SUCCESSFUL";
        UserActionType["GET_CART_RIDER_DATA_INITIATED"] = "[RIDER] GET_CART_RIDER_DATA_INITIATED";
        UserActionType["GET_CART_RIDER_DATA_FAILED"] = "[RIDER] GET_CART_RIDER_DATA_FAILED";
        UserActionType["GET_CART_RIDER_DATA_SUCCESSFUL"] = "[RIDER] GET_CART_RIDER_DATA_SUCCESSFUL";
        UserActionType["GET_RIDER_DELIVERY_RATING_INITIATED"] = "[CART_DELIVERY] GET_RIDER_DELIVERY_RATING_INITIATED";
        UserActionType["GET_RIDER_DELIVERY_RATING_FAILED"] = "[CART_DELIVERY] GET_RIDER_DELIVERY_RATING_FAILED";
        UserActionType["GET_RIDER_DELIVERY_RATING_SUCCESSFUL"] = "[CART_DELIVERY] GET_RIDER_DELIVERY_RATING_SUCCESSFUL";
        UserActionType["VERIFY_PROCESSED_CART"] = "[CART] VERIFY_PROCESSED_CART";
        UserActionType["CLEAR_PROCESSED_CART"] = "[CART] VERIFY_PROCESSED_CART";
        UserActionType["CART_IS_NOT_IN_TRANSIT"] = "[CART] CART_IS_NOT_IN_TRANSIT";
        UserActionType["GET_NEARBY_MERCHANTS_INITIATED"] = "[MERCHANT_LISTING] GET_NEARBY_MERCHANTS_INITIATED";
        UserActionType["GET_NEARBY_MERCHANTS_FAILED"] = "[MERCHANT_LISTING] GET_NEARBY_MERCHANTS_FAILED";
        UserActionType["GET_NEARBY_MERCHANTS_SUCCESSFUL"] = "[MERCHANT_LISTING] GET_NEARBY_MERCHANTS_SUCCESSFUL";
        UserActionType["START_LOADER_REMOTELY"] = "[USER_LOADER] START_LOADER_REMOTELY";
        UserActionType["END_LOADER_REMOTELY"] = "[USER_LOADER] END_LOADER_REMOTELY";
        UserActionType["CLEAR_USER_STATE"] = "[LOGOUT] CLEAR_USER_STATE";
      })(UserActionType || (UserActionType = {}));

      var actions = {
        ClearActiveErrorAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.CLEAR_ACTIVE_ERROR),
        ClearActiveMessageAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.CLEAR_ACTIVE_MESSAGE),
        ClearActiveActionMessageAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.CLEAR_ACTIVE_ACTION_MESSAGE),
        GetProductsByMerchantInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_PRODUCTS_BY_MERCHANT_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetProductsByMerchantFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_PRODUCTS_BY_MERCHANT_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetProductsByMerchantSuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_PRODUCTS_BY_MERCHANT_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        SearchForProductsInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.SEARCH_FOR_PRODUCTS_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        SearchForProductsFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.SEARCH_FOR_PRODUCTS_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        SearchForProductsSuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.SEARCH_FOR_PRODUCTS_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetUserFavouritesInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_USER_FAVOURITES_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetUserFavouritesFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_USER_FAVOURITES_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetUserFavouritesSuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_USER_FAVOURITES_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        AddMerchantToUserBookmarksInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.ADD_MERCHANT_TO_USER_BOOKMARKS_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        AddMerchantToUserBookmarksFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.ADD_MERCHANT_TO_USER_BOOKMARKS_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        AddMerchantToUserBookmarksSuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.ADD_MERCHANT_TO_USER_BOOKMARKS_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetProductsOnHotSaleInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_PRODUCTS_BY_MERCHANT_INITIATED),
        GetProductsOnHotSaleFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_PRODUCTS_BY_MERCHANT_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetProductsOnHotSaleSuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_PRODUCTS_ON_HOT_SALE_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetUserOrderHistoryInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_USER_ORDER_HISTORY_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetUserOrderHistoryFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_USER_ORDER_HISTORY_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetUserOrderHistorySuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_USER_ORDER_HISTORY_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        AddProductToBookmarkInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.ADD_PRODUCT_TO_BOOKMARK_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        AddProductToBookmarkFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.ADD_PRODUCT_TO_BOOKMARK_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        AddProductToBookmarkSuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.ADD_PRODUCT_TO_BOOKMARK_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetUserDataInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_USER_INFO_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetUserDataFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_USER_INFO_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetUserDataSuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_USER_INFO_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        UpdateUserProfileInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.UPDATE_USER_PROFILE_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        UpdateUserProfileFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.UPDATE_USER_PROFILE_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        UpdateUserProfileSuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.UPDATE_USER_PROFILE_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        UploadProfileImageInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.UPLOAD_PROFILE_IMAGE_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        UploadProfileImageFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.UPLOAD_PROFILE_IMAGE_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        UploadProfileImageSuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.UPLOAD_PROFILE_IMAGE_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        ClearUserStateAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.CLEAR_USER_STATE),
        GetMerchantTypeInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_MERCHANT_TYPE_INITIATED),
        GetMerchantTypeFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_MERCHANT_TYPE_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetMerchantTypeSuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_MERCHANT_TYPE_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetMerchantByTypeInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_MERCHANT_BY_TYPE_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetMerchantByTypeFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_MERCHANT_BY_TYPE_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetMerchantByTypeSuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_MERCHANT_BY_TYPE_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetMerchantDetailInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_MERCHANT_DETAIL_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetMerchantDetailFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_MERCHANT_DETAIL_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetMerchantDetailSuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_MERCHANT_DETAIL_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        AddMerchantRatingInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.ADD_MERCHANT_RATING_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        AddMerchantRatingFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.ADD_MERCHANT_RATING_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        AddMerchantRatingSuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.ADD_MERCHANT_RATING_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetMerchantReviewsInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_MERCHANT_REVIEWS_INTIIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetMerchantReviewsFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_MERCHANT_REVIEWS_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetMerchantReviewsSuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_MERCHANT_REVIEWS_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetHotSaleProductForMerchantInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_HOT_SALE_PRODUCT_FOR_MERCHANT_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetHotSaleProductForMerchantSuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_HOT_SALE_PRODUCT_FOR_MERCHANT_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetHotSaleProductForMerchantFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_HOT_SALE_PRODUCT_FOR_MERCHANT_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetMechantProductsGroupedByCategoryInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_MERCHANT_PRODUCTS_GROUPED_BY_CATEGORY_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetMechantProductsGroupedByCategoryFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_MERCHANT_PRODUCTS_GROUPED_BY_CATEGORY_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetMechantProductsGroupedByCategorySuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_MERCHANT_PRODUCTS_GROUPED_BY_CATEGORY_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetProductsGroupedByCategoryInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_PRODUCTS_GROUPED_BY_CATEGORY_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetProductsGroupedByCategoryFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_PRODUCTS_GROUPED_BY_CATEGORY_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetProductsGroupedByCategorySuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_PRODUCTS_GROUPED_BY_CATEGORY_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        AddProductToCartInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.ADD_PRODUCT_TO_CART_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        AddProductToCartFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.ADD_PRODUCT_TO_CART_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        AddProductToCartSuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.ADD_PRODUCT_TO_CART_SUCCESSFUL),
        CartCheckoutInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.CART_CHECKOUT_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        CartCheckoutFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.CART_CHECKOUT_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        CartCheckoutSuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.CART_CHECKOUT_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        RemoveProductFromCartInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.REMOVE_PRODUCT_FROM_CART_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        RemoveProductFromCartFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.REMOVE_PRODUCT_FROM_CART_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        RemoveProductFromCartSuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.REMOVE_PRODUCT_FROM_CART_SUCCESSFUL),
        GetSingleProductInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_SINGLE_PRODUCT_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetSingleProductFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_SINGLE_PRODUCT_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetSingleProductSuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_SINGLE_PRODUCT_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        MakePaymentInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.MAKE_PAYMENT_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        MakePaymentFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.MAKE_PAYMENT_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        MakePaymentSuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.MAKE_PAYMENT_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        ConfirmPaystackPaymentInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.CONFIRM_PAYSTACK_PAYMENT_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        ConfirmPaystackPaymentFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.CONFIRM_PAYSTACK_PAYMENT_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        ConfirmPaystackPaymentSuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.CONFIRM_PAYSTACK_PAYMENT_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetUserOrderInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_USER_ORDERS_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetUserOrderFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_USER_ORDERS_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetUserOrderSuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_USER_ORDERS_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetSelectedCartInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_SELECTED_CART_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetSelectedCartFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_SELECTED_CART_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetSelectedCartSuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_SELECTED_CART_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        VerifyProcessedCartAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.VERIFY_PROCESSED_CART, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        ClearProcessedCartAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.CLEAR_PROCESSED_CART),
        CartIsNotInTransitAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.CART_IS_NOT_IN_TRANSIT),
        RateRiderInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.RATE_RIDER_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        RateRiderFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.RATE_RIDER_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        RateRiderSuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.RATE_RIDER_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetCartRiderDataInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_CART_RIDER_DATA_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetCartRiderDataFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_CART_RIDER_DATA_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetCartRiderDataSuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_CART_RIDER_DATA_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetRiderDeliveryRatingInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_RIDER_DELIVERY_RATING_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetRiderDeliveryRatingFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_RIDER_DELIVERY_RATING_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetRiderDeliveryRatingSuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_RIDER_DELIVERY_RATING_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        StartLoaderRemotelyAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.START_LOADER_REMOTELY),
        EndLoaderRemotelyAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.END_LOADER_REMOTELY),
        GetNearbyMerchantsInitiatedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_NEARBY_MERCHANTS_INITIATED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetNearbyMerchantsFailedAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_NEARBY_MERCHANTS_FAILED, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])()),
        GetNearbyMerchantsSuccessfulAction: Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createAction"])(UserActionType.GET_NEARBY_MERCHANTS_SUCCESSFUL, Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["props"])())
      };
      /***/
    },

    /***/
    "./src/app/user/store/effects/user-effect.service.ts":
    /*!***********************************************************!*\
      !*** ./src/app/user/store/effects/user-effect.service.ts ***!
      \***********************************************************/

    /*! exports provided: UserEffectService */

    /***/
    function srcAppUserStoreEffectsUserEffectServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UserEffectService", function () {
        return UserEffectService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ngrx_effects__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ngrx/effects */
      "./node_modules/@ngrx/effects/__ivy_ngcc__/fesm2015/ngrx-effects.js");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! rxjs */
      "./node_modules/rxjs/_esm2015/index.js");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! rxjs/operators */
      "./node_modules/rxjs/_esm2015/operators/index.js");
      /* harmony import */


      var _actions_user_action__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../actions/user.action */
      "./src/app/user/store/actions/user.action.ts");
      /* harmony import */


      var _user_http_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./user-http.service */
      "./src/app/user/store/effects/user-http.service.ts");
      /* harmony import */


      var _utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../../../../utils/functions/app.functions */
      "./src/utils/functions/app.functions.ts");
      /* harmony import */


      var _model_user_model__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ../model/user.model */
      "./src/app/user/store/model/user.model.ts");

      var UserEffectService = function UserEffectService(userHttpSrv, actions$) {
        var _this18 = this;

        _classCallCheck(this, UserEffectService);

        this.userHttpSrv = userHttpSrv;
        this.actions$ = actions$;
        this.getProductsByMerchants$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["createEffect"])(function () {
          return _this18.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetProductsByMerchantInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (action) {
            var _a;

            return _this18.userHttpSrv.getProductsByNearestMerchant((_a = action.payload) === null || _a === void 0 ? void 0 : _a.userId).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) {
              return _actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetProductsByMerchantSuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetProductsByMerchantFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.searchForProducts$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["createEffect"])(function () {
          return _this18.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].SearchForProductsInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (action) {
            return _this18.userHttpSrv.searchForProducts(action.payload.searchTerm).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) {
              return _actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].SearchForProductsSuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].SearchForProductsFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.getUserFavourites$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["createEffect"])(function () {
          return _this18.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetUserFavouritesInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (action) {
            return _this18.userHttpSrv.getUserFavourites(action.payload.userId).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) {
              return _actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetUserFavouritesSuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetUserFavouritesFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.addMerchantToBookmark$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["createEffect"])(function () {
          return _this18.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].AddMerchantToUserBookmarksInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (action) {
            var _action$payload7 = action.payload,
                Merchant = _action$payload7.Merchant,
                MerchantId = _action$payload7.MerchantId,
                UserId = _action$payload7.UserId;
            return _this18.userHttpSrv.addMerchantToBookmarks(UserId, MerchantId).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function () {
              return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this18, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee50() {
                return regeneratorRuntime.wrap(function _callee50$(_context50) {
                  while (1) {
                    switch (_context50.prev = _context50.next) {
                      case 0:
                        _context50.next = 2;
                        return Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_7__["addBookmarkToLocalStorage"])({
                          Id: MerchantId,
                          Type: _model_user_model__WEBPACK_IMPORTED_MODULE_8__["BookmarkType"].MERCHANT
                        });

                      case 2:
                      case "end":
                        return _context50.stop();
                    }
                  }
                }, _callee50);
              }));
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) {
              return _actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].AddMerchantToUserBookmarksSuccessfulAction({
                payload: {
                  result: data,
                  Merchant: Merchant
                }
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].AddMerchantToUserBookmarksFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.getProductsOnHotSaleList$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["createEffect"])(function () {
          return _this18.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetProductsOnHotSaleInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function () {
            ;
            return _this18.userHttpSrv.getProductsOnHotSaleList().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) {
              return _actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetProductsOnHotSaleSuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetProductsByMerchantFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.getUserOrderHistory$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["createEffect"])(function () {
          return _this18.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetUserOrderHistoryInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (action) {
            return _this18.userHttpSrv.getUserOrderHistory(action.payload.userId).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) {
              return _actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetUserOrderHistorySuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetUserOrderHistoryFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.addProductToBookmarks$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["createEffect"])(function () {
          return _this18.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].AddProductToBookmarkInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (action) {
            var _action$payload8 = action.payload,
                userId = _action$payload8.userId,
                productId = _action$payload8.productId;
            return _this18.userHttpSrv.addProductToBookmark(productId, userId).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function () {
              return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this18, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee51() {
                return regeneratorRuntime.wrap(function _callee51$(_context51) {
                  while (1) {
                    switch (_context51.prev = _context51.next) {
                      case 0:
                        _context51.next = 2;
                        return Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_7__["addBookmarkToLocalStorage"])({
                          Id: productId,
                          Type: _model_user_model__WEBPACK_IMPORTED_MODULE_8__["BookmarkType"].PRODUCT
                        });

                      case 2:
                      case "end":
                        return _context51.stop();
                    }
                  }
                }, _callee51);
              }));
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) {
              return _actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].AddProductToBookmarkSuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].AddProductToBookmarkFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.getUserData$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["createEffect"])(function () {
          return _this18.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetUserDataInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (action) {
            return _this18.userHttpSrv.getUserData(action.payload.userId).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) {
              return _actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetUserDataSuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetUserDataFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.updateUserProfile$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["createEffect"])(function () {
          return _this18.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].UpdateUserProfileInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (action) {
            var payload = action.payload;
            return _this18.userHttpSrv.updateUserProfile(payload).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) {
              return _actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].UpdateUserProfileSuccessfulAction({
                payload: {
                  result: data,
                  updatedUserData: payload
                }
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].UpdateUserProfileFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.changeProfileImage$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["createEffect"])(function () {
          return _this18.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].UploadProfileImageInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (action) {
            var _action$payload9 = action.payload,
                file = _action$payload9.file,
                userId = _action$payload9.userId;
            return _this18.userHttpSrv.changeUserPassport(file, userId).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) {
              return _actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].UploadProfileImageSuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].UploadProfileImageFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.getMerchantTypes$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["createEffect"])(function () {
          return _this18.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetMerchantTypeInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function () {
            return _this18.userHttpSrv.getMerchantType().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) {
              return _actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetMerchantTypeSuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetMerchantTypeFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.getMerchantsByType$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["createEffect"])(function () {
          return _this18.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetMerchantByTypeInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (action) {
            return _this18.userHttpSrv.getMerchantByType(action.payload.typeId).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) {
              return _actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetMerchantByTypeSuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetMerchantByTypeFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.getSelectedMerchant$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["createEffect"])(function () {
          return _this18.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetMerchantDetailInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (action) {
            return _this18.userHttpSrv.getMerchantById(action.payload.typeId).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) {
              return _actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetMerchantDetailSuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetMerchantDetailFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.addMerchantRating$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["createEffect"])(function () {
          return _this18.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].AddMerchantRatingInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (action) {
            return _this18.userHttpSrv.addMerchantRating(action.payload).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) {
              return _actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].AddMerchantRatingSuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].AddMerchantRatingFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.getMerchantReviews$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["createEffect"])(function () {
          return _this18.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetMerchantReviewsInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (action) {
            return _this18.userHttpSrv.getMerchantReviews(action.payload.merchantId).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) {
              return _actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetMerchantReviewsSuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetMerchantReviewsFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.getMerchantHotSaleProducts$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["createEffect"])(function () {
          return _this18.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetHotSaleProductForMerchantInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (action) {
            return _this18.userHttpSrv.getHotSaleProductsForMerchant(action.payload.merchantId).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) {
              return _actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetHotSaleProductForMerchantSuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetHotSaleProductForMerchantFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.getMerchantGroupedProducts$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["createEffect"])(function () {
          return _this18.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetMechantProductsGroupedByCategoryInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (action) {
            return _this18.userHttpSrv.getMerchantGroupedProducts(action.payload.merchantId).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) {
              return _actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetMechantProductsGroupedByCategorySuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetMechantProductsGroupedByCategoryFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.getCategoryGroupedProducts$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["createEffect"])(function () {
          return _this18.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetProductsGroupedByCategoryInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (action) {
            return _this18.userHttpSrv.getCategoryGroupedProducts(action.payload.categoryId).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) {
              return _actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetProductsGroupedByCategorySuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetProductsGroupedByCategoryFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.getSingleProductById$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["createEffect"])(function () {
          return _this18.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetSingleProductInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (action) {
            return _this18.userHttpSrv.getSelectedProduct(action.payload.productId).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) {
              return _actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetSingleProductSuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetSingleProductFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.addProductToCart$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["createEffect"])(function () {
          return _this18.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].AddProductToCartInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function () {
            return _actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].AddProductToCartSuccessfulAction();
          }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].AddProductToCartFailedAction({
              payload: error
            }));
          }));
        });
        this.removeProductToCart$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["createEffect"])(function () {
          return _this18.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].RemoveProductFromCartInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function () {
            return _actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].RemoveProductFromCartSuccessfulAction();
          }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].RemoveProductFromCartFailedAction({
              payload: error
            }));
          }));
        });
        this.processCart$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["createEffect"])(function () {
          return _this18.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].CartCheckoutInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (action) {
            return _this18.userHttpSrv.productCheckout(action.payload).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) {
              return _actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].CartCheckoutSuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].CartCheckoutFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.makePayment$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["createEffect"])(function () {
          return _this18.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].MakePaymentInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (action) {
            var _action$payload10 = action.payload,
                cartId = _action$payload10.cartId,
                paymentOption = _action$payload10.paymentOption;
            return _this18.userHttpSrv.makePayment(cartId, paymentOption).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) {
              return _actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].MakePaymentSuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].MakePaymentFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.confirmPaystackPayment$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["createEffect"])(function () {
          return _this18.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].ConfirmPaystackPaymentInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (action) {
            return _this18.userHttpSrv.confirmPaystackPayment(action.payload.refrenceNo).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) {
              return _actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].ConfirmPaystackPaymentSuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].ConfirmPaystackPaymentFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.getUserOrders$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["createEffect"])(function () {
          return _this18.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetUserOrderInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (action) {
            return _this18.userHttpSrv.getUserProductOrders(action.payload.userId).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) {
              return _actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetUserOrderSuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetUserOrderFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.getSelectedCartById$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["createEffect"])(function () {
          return _this18.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetSelectedCartInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (action) {
            return _this18.userHttpSrv.getCartById(action.payload.cartId).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) {
              return _actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetSelectedCartSuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetSelectedCartFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.rateRider$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["createEffect"])(function () {
          return _this18.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].RateRiderInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (action) {
            var _action$payload11 = action.payload,
                review = _action$payload11.review,
                rating = _action$payload11.rating,
                cartId = _action$payload11.cartId,
                riderQualityReview = _action$payload11.riderQualityReview;
            return _this18.userHttpSrv.rateRider(cartId, rating, review, riderQualityReview).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) {
              return _actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].RateRiderSuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].RateRiderFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.getCartRiderData$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["createEffect"])(function () {
          return _this18.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetCartRiderDataInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (action) {
            return _this18.userHttpSrv.getCartRiderData(action.payload.cartId).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) {
              return _actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetCartRiderDataSuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetCartRiderDataFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.getRiderCartDeliveryFeedback$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["createEffect"])(function () {
          return _this18.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetRiderDeliveryRatingInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (action) {
            return _this18.userHttpSrv.getRiderDeliveryFeedback(action.payload.cartId).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) {
              return _actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetRiderDeliveryRatingSuccessfulAction({
                payload: data
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetRiderDeliveryRatingFailedAction({
                payload: error
              }));
            }));
          }));
        });
        this.getNearbyMerchants$ = Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["createEffect"])(function () {
          return _this18.actions$.pipe(Object(_ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["ofType"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetNearbyMerchantsInitiatedAction), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["switchMap"])(function (action) {
            var _action$payload12 = action.payload,
                userId = _action$payload12.userId,
                typeId = _action$payload12.typeId;
            return _this18.userHttpSrv.getNearbyMerchants(userId).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (data) {
              return _actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetNearbyMerchantsSuccessfulAction({
                payload: {
                  data: data,
                  typeId: typeId
                }
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetNearbyMerchantsFailedAction({
                payload: error
              }));
            }));
          }));
        });
      };

      UserEffectService.ctorParameters = function () {
        return [{
          type: _user_http_service__WEBPACK_IMPORTED_MODULE_6__["UserHttpService"]
        }, {
          type: _ngrx_effects__WEBPACK_IMPORTED_MODULE_2__["Actions"]
        }];
      };

      UserEffectService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: "root"
      })], UserEffectService);
      /***/
    },

    /***/
    "./src/app/user/store/effects/user-http.service.ts":
    /*!*********************************************************!*\
      !*** ./src/app/user/store/effects/user-http.service.ts ***!
      \*********************************************************/

    /*! exports provided: UserHttpService */

    /***/
    function srcAppUserStoreEffectsUserHttpServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UserHttpService", function () {
        return UserHttpService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! rxjs/operators */
      "./node_modules/rxjs/_esm2015/operators/index.js");
      /* harmony import */


      var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../../../../environments/environment */
      "./src/environments/environment.ts");

      var UserHttpService = /*#__PURE__*/function () {
        function UserHttpService(httpClient) {
          _classCallCheck(this, UserHttpService);

          this.httpClient = httpClient;
        }

        _createClass(UserHttpService, [{
          key: "getProductsByMerchant",
          value: function getProductsByMerchant() {
            try {
              return this.httpClient.get("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Product/GetProductIndex?includeMerchant=true&includeCategory=true")).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
            } catch (ex) {
              throw ex;
            }
          }
        }, {
          key: "getProductsByNearestMerchant",
          value: function getProductsByNearestMerchant(userId) {
            try {
              return this.httpClient.get("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Product/GetProductIndexCloserToUser?UserId=").concat(userId, "&includeMerchant=true&includeCategory=true")).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
            } catch (ex) {
              throw ex;
            }
          }
        }, {
          key: "searchForProducts",
          value: function searchForProducts(searchTerm) {
            return this.httpClient.get("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Product/SearchProductsByName?Name=").concat(searchTerm, "&includeRating=true&includeImages=true&includeCost=true")).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
          }
        }, {
          key: "getUserFavourites",
          value: function getUserFavourites(userId) {
            try {
              return this.httpClient.get("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Product/GetFavouriteItem?UserId=").concat(userId, "&includeMerchant=true&includeProduct=true")).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
            } catch (ex) {
              throw ex;
            }
          }
        }, {
          key: "addMerchantToBookmarks",
          value: function addMerchantToBookmarks(userId, merchantId) {
            try {
              return this.httpClient.post("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Merchant/AddFavouriteProduct?UserId=").concat(userId, "&MerchantId=").concat(merchantId), {}).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
            } catch (ex) {
              throw ex;
            }
          }
        }, {
          key: "getProductsOnHotSaleList",
          value: function getProductsOnHotSaleList() {
            try {
              return this.httpClient.get("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Product/GetHotSaleProducts?includeRating=true&includeImages=true&includeCost=true")).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
            } catch (ex) {
              throw ex;
            }
          }
        }, {
          key: "getUserOrderHistory",
          value: function getUserOrderHistory(userId) {
            try {
              return this.httpClient.get("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Shopping/GetUserOrderHistory?User=").concat(userId, "&includeRating=true&includeImages=true&includeCost=true")).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
            } catch (ex) {
              throw ex;
            }
          }
        }, {
          key: "addProductToBookmark",
          value: function addProductToBookmark(productId, userId) {
            try {
              return this.httpClient.post("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Product/AddFavouriteProduct?UserId=").concat(userId, "&ProductId=").concat(productId), {}).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
            } catch (ex) {
              throw ex;
            }
          }
        }, {
          key: "getUserData",
          value: function getUserData(userId) {
            try {
              return this.httpClient.get("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Customer/Get?userId=").concat(userId, "&includeperson=true&includeOrder=false&includeActiveCart=false&includeAllCart=false&completedCart=false")).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
            } catch (ex) {
              throw ex;
            }
          }
        }, {
          key: "getMerchantType",
          value: function getMerchantType() {
            try {
              return this.httpClient.get("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/MerchantType")).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
            } catch (ex) {
              throw ex;
            }
          }
        }, {
          key: "updateUserProfile",
          value: function updateUserProfile(payload) {
            try {
              return this.httpClient.post("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Customer/EditCustomerProfile"), payload).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
            } catch (ex) {
              throw ex;
            }
          }
        }, {
          key: "changeUserPassport",
          value: function changeUserPassport(file, userId) {
            try {
              var formData = new FormData();
              formData.append("UserId", userId);
              formData.append("File", file, file.name);
              /**
               * { responseType: "text" as "json" }
               * This is done to specify that this endpoint returns a string(filePath)
               * It stop httpClient from expecting a Json request payload
               */

              return this.httpClient.post("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Account/AddPassport"), formData, {
                responseType: "text"
              }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
            } catch (ex) {
              throw ex;
            }
          }
        }, {
          key: "getMerchantByType",
          value: function getMerchantByType(merchantTypeId) {
            try {
              return this.httpClient.get("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Merchant/FindMerchantByType?typeId=").concat(merchantTypeId, "&all=true&isActive=true")).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
            } catch (ex) {
              throw ex;
            }
          }
        }, {
          key: "getMerchantById",
          value: function getMerchantById(id) {
            try {
              return this.httpClient.get("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Merchant/GetById?id=").concat(id)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
            } catch (ex) {
              throw ex;
            }
          }
        }, {
          key: "addMerchantRating",
          value: function addMerchantRating(payload) {
            try {
              var MerchantId = payload.MerchantId,
                  Rate = payload.Rate,
                  Review = payload.Review,
                  UserId = payload.UserId;
              return this.httpClient.post("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Merchant/RateMerchant?UserId=").concat(UserId, "&MerchantId=").concat(MerchantId, "&Rate=").concat(Rate, "&Review=").concat(Review), {}).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
            } catch (ex) {
              throw ex;
            }
          }
        }, {
          key: "getMerchantReviews",
          value: function getMerchantReviews(merchantId) {
            try {
              return this.httpClient.get("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Merchant/GetMerchantReviews?MerchantId=").concat(merchantId)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
            } catch (ex) {
              throw ex;
            }
          }
        }, {
          key: "getHotSaleProductsForMerchant",
          value: function getHotSaleProductsForMerchant(merchantId) {
            try {
              return this.httpClient.get("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Product/GetMerchantHotSaleProducts?MerchantId=").concat(merchantId, "&includeRating=true&includeImages=true&includeCost=true")).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
            } catch (ex) {
              throw ex;
            }
          }
        }, {
          key: "getMerchantGroupedProducts",
          value: function getMerchantGroupedProducts(merchantId) {
            try {
              return this.httpClient.get("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Product/GetMerchantGroupedProducts?MerchantId=").concat(merchantId, "&includeRating=true&includeImages=true&includeCost=true")).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
            } catch (ex) {
              throw ex;
            }
          }
        }, {
          key: "getCategoryGroupedProducts",
          value: function getCategoryGroupedProducts(categoryId) {
            try {
              return this.httpClient.get("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Product/GetProductsByCategory?CategoryId=").concat(categoryId, "&includeRating=true&includeImages=true&includeCost=true")).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
            } catch (ex) {
              throw ex;
            }
          }
        }, {
          key: "getSelectedProduct",
          value: function getSelectedProduct(productId) {
            return this.httpClient.get("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Product/GetProduct?ProductId=").concat(productId, "&includeRating=true&includeImages=true&includeCost=true&includeOrdercount=true")).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
          }
        }, {
          key: "productCheckout",
          value: function productCheckout(payload) {
            try {
              return this.httpClient.post("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Shopping/CheckOut"), payload).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
            } catch (ex) {
              throw ex;
            }
          }
        }, {
          key: "makePayment",
          value: function makePayment(cartId, paymentOption) {
            return this.httpClient.post("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Payment/MakePayment?CartId=").concat(cartId, "&PaymentOption=").concat(paymentOption), {}).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
          }
        }, {
          key: "confirmPaystackPayment",
          value: function confirmPaystackPayment(referenceNo) {
            try {
              return this.httpClient.post("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Payment/ConfirmPaystackPayment?referenceNo=").concat(referenceNo), {}).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
            } catch (ex) {
              throw ex;
            }
          }
        }, {
          key: "getUserProductOrders",
          value: function getUserProductOrders(userId) {
            try {
              return this.httpClient.get("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Shopping/GetCartByUserId?UserId=").concat(userId, "&includeOrder=true&includeActiveCart=true&includeAllCart=true&completedCart=true")).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
            } catch (ex) {
              throw ex;
            }
          }
        }, {
          key: "getCartById",
          value: function getCartById(cartId) {
            try {
              return this.httpClient.get("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Shopping/GetCartByCartId?CartId=").concat(cartId, "&includeOrder=true")).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
            } catch (ex) {
              throw ex;
            }
          }
        }, {
          key: "rateRider",
          value: function rateRider(cartId, rate, review, riderQualityReview) {
            var url = "".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Rider/RateRider?CartId=").concat(cartId, "&Rate=").concat(rate, "&review=").concat(review, "&IsEnthusiastic=").concat(riderQualityReview.Enthusiastic, "&IsFast=").concat(riderQualityReview.Fast, "&IsFriendly=").concat(riderQualityReview.Friendly);
            return this.httpClient.post(url, {}).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
          }
        }, {
          key: "getRiderDeliveryFeedback",
          value: function getRiderDeliveryFeedback(cartId) {
            try {
              return this.httpClient.get("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Rider/GetRiderCartRatingModel?CartId=").concat(cartId)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
            } catch (ex) {
              throw ex;
            }
          }
        }, {
          key: "getCartRiderData",
          value: function getCartRiderData(cartId) {
            try {
              return this.httpClient.get("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Shopping/GetRiderBy?CartId=").concat(cartId)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
            } catch (ex) {
              throw ex;
            }
          }
        }, {
          key: "getNearbyMerchants",
          value: function getNearbyMerchants(userId) {
            try {
              return this.httpClient.get("".concat(_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].apiRoot, "/api/Merchant/GetCloseByMerchant?UserId=").concat(userId)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["throttleTime"])(500), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["retry"])(3));
            } catch (ex) {
              throw ex;
            }
          }
        }]);

        return UserHttpService;
      }();

      UserHttpService.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
        }];
      };

      UserHttpService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: "root"
      })], UserHttpService);
      /***/
    },

    /***/
    "./src/app/user/store/model/user.model.ts":
    /*!************************************************!*\
      !*** ./src/app/user/store/model/user.model.ts ***!
      \************************************************/

    /*! exports provided: PaymentMethodType, PaymentMethodTextType, ProductCartStatus, OrderStatus, MessageActionType, BookmarkType, InitialUserState */

    /***/
    function srcAppUserStoreModelUserModelTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PaymentMethodType", function () {
        return PaymentMethodType;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PaymentMethodTextType", function () {
        return PaymentMethodTextType;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ProductCartStatus", function () {
        return ProductCartStatus;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "OrderStatus", function () {
        return OrderStatus;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MessageActionType", function () {
        return MessageActionType;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "BookmarkType", function () {
        return BookmarkType;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "InitialUserState", function () {
        return InitialUserState;
      });

      var PaymentMethodType;

      (function (PaymentMethodType) {
        PaymentMethodType[PaymentMethodType["CASH"] = 0] = "CASH";
        PaymentMethodType[PaymentMethodType["POS"] = 1] = "POS";
        PaymentMethodType[PaymentMethodType["PAYSTACK"] = 2] = "PAYSTACK";
        PaymentMethodType[PaymentMethodType["TRANSFER"] = 3] = "TRANSFER";
      })(PaymentMethodType || (PaymentMethodType = {}));

      var PaymentMethodTextType;

      (function (PaymentMethodTextType) {
        PaymentMethodTextType["CASH"] = "CASH";
        PaymentMethodTextType["POS"] = "POS";
        PaymentMethodTextType["PAYSTACK"] = "PAYSTACK";
        PaymentMethodTextType["TRANSFER"] = "TRANSFER";
      })(PaymentMethodTextType || (PaymentMethodTextType = {}));

      var ProductCartStatus;

      (function (ProductCartStatus) {
        ProductCartStatus["VERIFIED"] = "VERIFIED";
        ProductCartStatus["UNVERIFIED"] = "UNVERIFIED";
        ProductCartStatus["TRANSIT"] = "TRANSIT";
        ProductCartStatus["COMPLETED"] = "COMPLETED";
        ProductCartStatus["CANCELLED"] = "CANCELLED";
      })(ProductCartStatus || (ProductCartStatus = {}));

      var OrderStatus;

      (function (OrderStatus) {
        OrderStatus[OrderStatus["UNVERIFIED"] = 0] = "UNVERIFIED";
        OrderStatus[OrderStatus["VERIFIED"] = 1] = "VERIFIED";
        OrderStatus[OrderStatus["TRANSIT"] = 2] = "TRANSIT";
        OrderStatus[OrderStatus["COMPLETED"] = 3] = "COMPLETED";
        OrderStatus[OrderStatus["CANCELLED"] = 4] = "CANCELLED";
      })(OrderStatus || (OrderStatus = {}));

      var MessageActionType;

      (function (MessageActionType) {
        MessageActionType["CART_VERIFIED"] = "CART_VERIFIED";
        MessageActionType["RIDER_ASSIGNED_TO_CART"] = "RIDER_ASSIGNED_TO_CART";
      })(MessageActionType || (MessageActionType = {}));

      var BookmarkType;

      (function (BookmarkType) {
        BookmarkType["PRODUCT"] = "PRODUCT";
        BookmarkType["MERCHANT"] = "MERCHANT";
      })(BookmarkType || (BookmarkType = {}));

      var InitialUserState = {
        IsLoading: false,
        ActiveError: undefined,
        ActiveMessage: undefined,
        ProductsByMerchants: undefined,
        ProductSearchResults: [],
        FavouriteMerchants: [],
        FavouriteProducts: [],
        ProductsOnHotSale: [],
        ProductOrderHistory: [],
        MerchantOrderHistory: [],
        MerchantOrderedByType: [],
        MerchantType: [],
        SelectedMerchant: undefined,
        SelectedMerchantReviews: [],
        SelectedMerchantHotSaleProducts: [],
        ProductCategoryGroups: [],
        ProductsGroupedByCategory: [],
        SelectedCartProductsList: [],
        UserData: undefined,
        CartItems: undefined,
        ProcessedProductCart: undefined,
        CartItemCount: 0,
        UnverifiedOrders: [],
        VerifiedOrders: [],
        TransitOrders: [],
        CompletedOrders: [],
        CancelledOrders: [],
        NearbyMerchantsByMerchantType: [],
        SelectedCart: undefined,
        IsProcessedCartVerified: undefined,
        ActiveActionMessage: undefined,
        SelectedCartRiderData: undefined,
        SelectedCartDeliveryFeedback: undefined
      };
      /***/
    },

    /***/
    "./src/app/user/store/reducer/user.reducer.ts":
    /*!****************************************************!*\
      !*** ./src/app/user/store/reducer/user.reducer.ts ***!
      \****************************************************/

    /*! exports provided: UserReducer */

    /***/
    function srcAppUserStoreReducerUserReducerTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UserReducer", function () {
        return UserReducer;
      });
      /* harmony import */


      var _ngrx_store__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @ngrx/store */
      "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");
      /* harmony import */


      var _model_user_model__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ../model/user.model */
      "./src/app/user/store/model/user.model.ts");
      /* harmony import */


      var _actions_user_action__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../actions/user.action */
      "./src/app/user/store/actions/user.action.ts");
      /* harmony import */


      var _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../../../../utils/types/app.constant */
      "./src/utils/types/app.constant.ts");
      /* harmony import */


      var _utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../../../../utils/functions/app.functions */
      "./src/utils/functions/app.functions.ts");

      var UserReducer = Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["createReducer"])(_model_user_model__WEBPACK_IMPORTED_MODULE_1__["InitialUserState"], Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].ClearActiveErrorAction, function (state) {
        return Object.assign(Object.assign({}, state), {
          ActiveError: undefined,
          ActiveMessage: undefined,
          IsLoading: false
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].ClearActiveMessageAction, function (state) {
        return Object.assign(Object.assign({}, state), {
          ActiveError: undefined,
          ActiveMessage: undefined,
          IsLoading: false
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].ClearActiveActionMessageAction, function (state) {
        return Object.assign(Object.assign({}, state), {
          ActiveActionMessage: undefined,
          ActiveMessage: undefined,
          IsLoading: false
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetProductsByMerchantInitiatedAction, function (state, _ref76) {
        var payload = _ref76.payload;
        return Object.assign(Object.assign({}, state), {
          ActiveError: undefined,
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetProductsByMerchantFailedAction, function (state, _ref77) {
        var payload = _ref77.payload;
        return Object.assign(Object.assign({}, state), {
          ActiveError: payload,
          IsLoading: false
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetProductsByMerchantSuccessfulAction, function (state, _ref78) {
        var payload = _ref78.payload;
        return Object.assign(Object.assign({}, state), {
          ProductsByMerchants: payload,
          IsLoading: false
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].SearchForProductsInitiatedAction, function (state, _ref79) {
        var payload = _ref79.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].SearchForProductsFailedAction, function (state, _ref80) {
        var payload = _ref80.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].SearchForProductsSuccessfulAction, function (state, _ref81) {
        var payload = _ref81.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ProductSearchResults: _toConsumableArray(payload)
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetUserFavouritesInitiatedAction, function (state, _ref82) {
        var payload = _ref82.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetUserFavouritesFailedAction, function (state, _ref83) {
        var payload = _ref83.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetUserFavouritesSuccessfulAction, function (state, _ref84) {
        var payload = _ref84.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          FavouriteMerchants: _toConsumableArray(payload === null || payload === void 0 ? void 0 : payload.merchants),
          FavouriteProducts: _toConsumableArray(payload === null || payload === void 0 ? void 0 : payload.productDetails)
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetProductsOnHotSaleFailedAction, function (state, _ref85) {
        var payload = _ref85.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetProductsOnHotSaleSuccessfulAction, function (state, _ref86) {
        var payload = _ref86.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ProductsOnHotSale: _toConsumableArray(payload)
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetUserOrderHistoryInitiatedAction, function (state, _ref87) {
        var payload = _ref87.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetUserOrderHistoryFailedAction, function (state, _ref88) {
        var payload = _ref88.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetUserOrderHistorySuccessfulAction, function (state, _ref89) {
        var payload = _ref89.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ProductOrderHistory: _toConsumableArray(payload)
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].AddProductToBookmarkInitiatedAction, function (state, _ref90) {
        var payload = _ref90.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].AddProductToBookmarkFailedAction, function (state, _ref91) {
        var payload = _ref91.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].AddProductToBookmarkSuccessfulAction, function (state, _ref92) {
        var payload = _ref92.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].AddMerchantToUserBookmarksSuccessfulAction, function (state, _ref93) {
        var payload = _ref93.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].AddMerchantToUserBookmarksInitiatedAction, function (state, _ref94) {
        var payload = _ref94.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].AddMerchantToUserBookmarksFailedAction, function (state, _ref95) {
        var payload = _ref95.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].AddMerchantToUserBookmarksSuccessfulAction, function (state, _ref96) {
        var payload = _ref96.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          // ActiveMessage: "Bookmark added",
          FavouriteMerchants: [].concat(_toConsumableArray(state.FavouriteMerchants), [Object.assign({}, payload.Merchant)])
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetUserDataInitiatedAction, function (state, _ref97) {
        var payload = _ref97.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetUserDataFailedAction, function (state, _ref98) {
        var payload = _ref98.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetUserDataSuccessfulAction, function (state, _ref99) {
        var payload = _ref99.payload;
        // tslint:disable-next-line: no-string-literal
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          UserData: payload["user"]
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].UpdateUserProfileInitiatedAction, function (state, _ref100) {
        var payload = _ref100.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].UpdateUserProfileFailedAction, function (state, _ref101) {
        var payload = _ref101.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].UpdateUserProfileSuccessfulAction, function (state, _ref102) {
        var payload = _ref102.payload;

        if (state === null || state === void 0 ? void 0 : state.UserData) {
          var newUserData = Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_4__["swapUserDataAfterUpdate"])(payload.updatedUserData, state.UserData);
          return Object.assign(Object.assign({}, state), {
            IsLoading: false,
            ActiveMessage: _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_3__["APIMessage"].UPDATE,
            UserData: newUserData
          });
        } else {
          return Object.assign(Object.assign({}, state), {
            IsLoading: false,
            ActiveMessage: _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_3__["APIMessage"].UPDATE
          });
        }
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].UploadProfileImageInitiatedAction, function (state, _ref103) {
        var payload = _ref103.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].UploadProfileImageFailedAction, function (state, _ref104) {
        var payload = _ref104.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].UploadProfileImageSuccessfulAction, function (state, _ref105) {
        var payload = _ref105.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveMessage: _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_3__["APIMessage"].UPDATE
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].ClearUserStateAction, function (state) {
        // console.log({ ...InitialUserState });
        // !!! This is just a hack, got to build a root reducer ans modify data there
        return Object.assign(Object.assign({}, state), {
          UserData: undefined
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetMerchantTypeInitiatedAction, function (state) {
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetMerchantTypeFailedAction, function (state, _ref106) {
        var payload = _ref106.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetMerchantTypeSuccessfulAction, function (state, _ref107) {
        var payload = _ref107.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          MerchantType: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetMerchantByTypeInitiatedAction, function (state, _ref108) {
        var payload = _ref108.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetMerchantByTypeFailedAction, function (state, _ref109) {
        var payload = _ref109.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetMerchantByTypeSuccessfulAction, function (state, _ref110) {
        var payload = _ref110.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          MerchantOrderedByType: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetMerchantDetailInitiatedAction, function (state, _ref111) {
        var payload = _ref111.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetMerchantDetailFailedAction, function (state, _ref112) {
        var payload = _ref112.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetMerchantDetailSuccessfulAction, function (state, _ref113) {
        var payload = _ref113.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          SelectedMerchant: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].AddMerchantRatingInitiatedAction, function (state, _ref114) {
        var payload = _ref114.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].AddMerchantRatingFailedAction, function (state, _ref115) {
        var payload = _ref115.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].AddMerchantRatingSuccessfulAction, function (state, _ref116) {
        var payload = _ref116.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveMessage: "Rating added"
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetMerchantReviewsInitiatedAction, function (state, _ref117) {
        var payload = _ref117.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetMerchantReviewsFailedAction, function (state, _ref118) {
        var payload = _ref118.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetMerchantReviewsSuccessfulAction, function (state, _ref119) {
        var payload = _ref119.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          SelectedMerchantReviews: _toConsumableArray(payload)
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetHotSaleProductForMerchantInitiatedAction, function (state, _ref120) {
        var payload = _ref120.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetHotSaleProductForMerchantSuccessfulAction, function (state, _ref121) {
        var payload = _ref121.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          SelectedMerchantHotSaleProducts: _toConsumableArray(payload)
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetHotSaleProductForMerchantFailedAction, function (state, _ref122) {
        var payload = _ref122.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetMechantProductsGroupedByCategoryInitiatedAction, function (state, _ref123) {
        var payload = _ref123.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetMechantProductsGroupedByCategoryFailedAction, function (state, _ref124) {
        var payload = _ref124.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetMechantProductsGroupedByCategorySuccessfulAction, function (state, _ref125) {
        var payload = _ref125.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ProductCategoryGroups: payload.length > 0 ? _toConsumableArray(payload) : []
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetProductsGroupedByCategoryInitiatedAction, function (state, _ref126) {
        var payload = _ref126.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetProductsGroupedByCategoryFailedAction, function (state, _ref127) {
        var payload = _ref127.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetProductsGroupedByCategorySuccessfulAction, function (state, _ref128) {
        var payload = _ref128.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ProductsGroupedByCategory: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].AddProductToCartInitiatedAction, function (state, _ref129) {
        var _ref129$payload = _ref129.payload,
            product = _ref129$payload.product,
            deliveryAddress = _ref129$payload.deliveryAddress,
            userId = _ref129$payload.userId;

        var _a, _b;

        var currentCart = Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_4__["addProductToCart"])(product, state.CartItems, userId, deliveryAddress);
        /**
         * Save cart to localStorage
         */

        Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_4__["saveDataToLocalStorage"])(currentCart, _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_3__["LocalStorageKey"].CART);
        return Object.assign(Object.assign({}, state), {
          IsLoading: true,
          CartItems: currentCart,
          CartItemCount: (_b = (_a = currentCart === null || currentCart === void 0 ? void 0 : currentCart.productCheckouts) === null || _a === void 0 ? void 0 : _a.length) !== null && _b !== void 0 ? _b : 0
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].AddProductToCartFailedAction, function (state, _ref130) {
        var payload = _ref130.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].AddProductToCartSuccessfulAction, function (state) {
        return Object.assign(Object.assign({}, state), {
          IsLoading: false
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].CartCheckoutInitiatedAction, function (state, _ref131) {
        var payload = _ref131.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].CartCheckoutFailedAction, function (state, _ref132) {
        var payload = _ref132.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].CartCheckoutSuccessfulAction, function (state, _ref133) {
        var payload = _ref133.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveMessage: payload.status === _model_user_model__WEBPACK_IMPORTED_MODULE_1__["ProductCartStatus"].UNVERIFIED ? "Checkout completed. Please wait while your cart is verified" : undefined,
          ProcessedProductCart: Object.assign({}, payload)
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].RemoveProductFromCartInitiatedAction, function (state, _ref134) {
        var payload = _ref134.payload;

        var _a, _b; // ? Remove from state


        var currentCart = Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_4__["removeProductFromCart"])(payload.productId, state.CartItems);
        Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_4__["saveDataToLocalStorage"])(currentCart, _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_3__["LocalStorageKey"].CART);
        return Object.assign(Object.assign({}, state), {
          IsLoading: true,
          CartItems: currentCart,
          CartItemCount: (_b = (_a = currentCart === null || currentCart === void 0 ? void 0 : currentCart.productCheckouts) === null || _a === void 0 ? void 0 : _a.length) !== null && _b !== void 0 ? _b : 0
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].RemoveProductFromCartFailedAction, function (state, _ref135) {
        var payload = _ref135.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].RemoveProductFromCartSuccessfulAction, function (state) {
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveMessage: "Product Removed"
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetSingleProductInitiatedAction, function (state, _ref136) {
        var payload = _ref136.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetSingleProductFailedAction, function (state, _ref137) {
        var payload = _ref137.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetSingleProductSuccessfulAction, function (state, _ref138) {
        var payload = _ref138.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          SelectedCartProductsList: [].concat(_toConsumableArray(state.SelectedCartProductsList), [payload])
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].MakePaymentInitiatedAction, function (state, _ref139) {
        var payload = _ref139.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].MakePaymentFailedAction, function (state, _ref140) {
        var payload = _ref140.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].MakePaymentSuccessfulAction, function (state, _ref141) {
        var payload = _ref141.payload;

        if (payload) {
          Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_4__["deleteDataFromLocalStorage"])(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_3__["LocalStorageKey"].CART);
          return Object.assign(Object.assign({}, state), {
            ProcessedProductCart: undefined,
            CartItems: undefined,
            CartItemCount: undefined,
            IsLoading: false,
            ActiveMessage: payload ? "Payment completed" : undefined
          });
        } else {
          return Object.assign(Object.assign({}, state), {
            IsLoading: false
          });
        }
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].ConfirmPaystackPaymentInitiatedAction, function (state, _ref142) {
        var payload = _ref142.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].ConfirmPaystackPaymentFailedAction, function (state, _ref143) {
        var payload = _ref143.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].ConfirmPaystackPaymentSuccessfulAction, function (state, _ref144) {
        var payload = _ref144.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ProcessedProductCart: undefined,
          CartItems: undefined,
          ActiveMessage: payload === true ? "Payment Completed" : undefined
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetUserOrderInitiatedAction, function (state, _ref145) {
        var payload = _ref145.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetUserOrderFailedAction, function (state, _ref146) {
        var payload = _ref146.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetUserOrderSuccessfulAction, function (state, _ref147) {
        var payload = _ref147.payload;
        var groupedResult = Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_4__["groupUserOrders"])(payload);
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          UnverifiedOrders: groupedResult.unverified,
          VerifiedOrders: groupedResult.verified,
          TransitOrders: groupedResult.transit,
          CancelledOrders: groupedResult.cancelled,
          CompletedOrders: groupedResult.completed
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetSelectedCartInitiatedAction, function (state, _ref148) {
        var payload = _ref148.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetSelectedCartFailedAction, function (state, _ref149) {
        var payload = _ref149.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetSelectedCartSuccessfulAction, function (state, _ref150) {
        var payload = _ref150.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          SelectedCart: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].ClearProcessedCartAction, function (state) {
        return Object.assign(Object.assign({}, state), {
          IsProcessedCartVerified: undefined,
          ActiveMessage: undefined,
          ProcessedProductCart: undefined
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].VerifyProcessedCartAction, function (state, _ref151) {
        var _ref151$payload = _ref151.payload,
            _ref151$payload$statu = _ref151$payload.status,
            status = _ref151$payload$statu.status,
            message = _ref151$payload$statu.message,
            processedCart = _ref151$payload.processedCart;
        var currentState = Object.assign(Object.assign({}, state), {
          IsProcessedCartVerified: status,
          ActiveActionMessage: {
            Message: message,
            Type: _model_user_model__WEBPACK_IMPORTED_MODULE_1__["MessageActionType"].CART_VERIFIED,
            Label: "Pay"
          },
          ProcessedProductCart: Object.assign(Object.assign({}, processedCart), {
            status: status
          })
        });
        return currentState;
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].CartIsNotInTransitAction, function (state) {
        return Object.assign(Object.assign({}, state), {
          ActiveMessage: "This cart is not in transit"
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].RateRiderInitiatedAction, function (state, _ref152) {
        var payload = _ref152.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].RateRiderFailedAction, function (state, _ref153) {
        var payload = _ref153.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].RateRiderSuccessfulAction, function (state, _ref154) {
        var payload = _ref154.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveMessage: payload ? "Rating successful" : undefined
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetCartRiderDataInitiatedAction, function (state, _ref155) {
        var payload = _ref155.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetCartRiderDataFailedAction, function (state, _ref156) {
        var payload = _ref156.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetCartRiderDataSuccessfulAction, function (state, _ref157) {
        var payload = _ref157.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          SelectedCartRiderData: Object.assign({}, payload)
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetRiderDeliveryRatingInitiatedAction, function (state, _ref158) {
        var payload = _ref158.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetRiderDeliveryRatingFailedAction, function (state, _ref159) {
        var payload = _ref159.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetRiderDeliveryRatingSuccessfulAction, function (state, _ref160) {
        var payload = _ref160.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          SelectedCartDeliveryFeedback: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].StartLoaderRemotelyAction, function (state) {
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].EndLoaderRemotelyAction, function (state) {
        return Object.assign(Object.assign({}, state), {
          IsLoading: false
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetNearbyMerchantsInitiatedAction, function (state, _ref161) {
        var payload = _ref161.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: true
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetNearbyMerchantsFailedAction, function (state, _ref162) {
        var payload = _ref162.payload;
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          ActiveError: payload
        });
      }), Object(_ngrx_store__WEBPACK_IMPORTED_MODULE_0__["on"])(_actions_user_action__WEBPACK_IMPORTED_MODULE_2__["actions"].GetNearbyMerchantsSuccessfulAction, function (state, _ref163) {
        var _ref163$payload = _ref163.payload,
            data = _ref163$payload.data,
            typeId = _ref163$payload.typeId;
        var merchants = Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_4__["filterMerchantByType"])(data, typeId);
        return Object.assign(Object.assign({}, state), {
          IsLoading: false,
          NearbyMerchantsByMerchantType: _toConsumableArray(merchants)
        });
      }));
      /***/
    },

    /***/
    "./src/app/user/user-routing.module.ts":
    /*!*********************************************!*\
      !*** ./src/app/user/user-routing.module.ts ***!
      \*********************************************/

    /*! exports provided: UserRoutingModule */

    /***/
    function srcAppUserUserRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UserRoutingModule", function () {
        return UserRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _resolvers_cart_location_resolver__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../resolvers/cart-location.resolver */
      "./src/app/resolvers/cart-location.resolver.ts");

      var routes = [{
        path: "",
        children: [{
          path: "",
          pathMatch: "full",
          redirectTo: "discover-items"
        }, {
          path: "discover-items",
          loadChildren: function loadChildren() {
            return __webpack_require__.e(
            /*! import() | discover-items-discover-items-module */
            "discover-items-discover-items-module").then(__webpack_require__.bind(null,
            /*! ./discover-items/discover-items.module */
            "./src/app/user/discover-items/discover-items.module.ts")).then(function (m) {
              return m.DiscoverItemsPageModule;
            });
          }
        }, {
          path: "favourites",
          loadChildren: function loadChildren() {
            return __webpack_require__.e(
            /*! import() | favourites-favourites-module */
            "favourites-favourites-module").then(__webpack_require__.bind(null,
            /*! ./favourites/favourites.module */
            "./src/app/user/favourites/favourites.module.ts")).then(function (m) {
              return m.FavouritesPageModule;
            });
          }
        }, {
          path: "view-search-results",
          loadChildren: function loadChildren() {
            return __webpack_require__.e(
            /*! import() | view-search-results-view-search-results-module */
            "view-search-results-view-search-results-module").then(__webpack_require__.bind(null,
            /*! ./view-search-results/view-search-results.module */
            "./src/app/user/view-search-results/view-search-results.module.ts")).then(function (m) {
              return m.ViewSearchResultsPageModule;
            });
          }
        }, {
          path: "merchant-products/:merchantId",
          loadChildren: function loadChildren() {
            return __webpack_require__.e(
            /*! import() | merchant-products-merchant-products-module */
            "merchant-products-merchant-products-module").then(__webpack_require__.bind(null,
            /*! ./merchant-products/merchant-products.module */
            "./src/app/user/merchant-products/merchant-products.module.ts")).then(function (m) {
              return m.MerchantProductsPageModule;
            });
          }
        }, {
          path: "category-products/:categoryId/:categoryName",
          loadChildren: function loadChildren() {
            return __webpack_require__.e(
            /*! import() | category-products-category-products-module */
            "category-products-category-products-module").then(__webpack_require__.bind(null,
            /*! ./category-products/category-products.module */
            "./src/app/user/category-products/category-products.module.ts")).then(function (m) {
              return m.CategoryProductsPageModule;
            });
          }
        }, {
          path: "category-list",
          loadChildren: function loadChildren() {
            return __webpack_require__.e(
            /*! import() | category-list-category-list-module */
            "category-list-category-list-module").then(__webpack_require__.bind(null,
            /*! ./category-list/category-list.module */
            "./src/app/user/category-list/category-list.module.ts")).then(function (m) {
              return m.CategoryListPageModule;
            });
          }
        }, {
          path: "hot-sales-list",
          loadChildren: function loadChildren() {
            return __webpack_require__.e(
            /*! import() | hot-sales-list-hot-sales-list-module */
            "hot-sales-list-hot-sales-list-module").then(__webpack_require__.bind(null,
            /*! ./hot-sales-list/hot-sales-list.module */
            "./src/app/user/hot-sales-list/hot-sales-list.module.ts")).then(function (m) {
              return m.HotSalesListPageModule;
            });
          }
        }, {
          path: "product-cart",
          loadChildren: function loadChildren() {
            return __webpack_require__.e(
            /*! import() | product-cart-product-cart-module */
            "product-cart-product-cart-module").then(__webpack_require__.bind(null,
            /*! ./product-cart/product-cart.module */
            "./src/app/user/product-cart/product-cart.module.ts")).then(function (m) {
              return m.ProductCartPageModule;
            });
          }
        }, {
          path: "make-payment/:cartId",
          loadChildren: function loadChildren() {
            return __webpack_require__.e(
            /*! import() | make-payment-make-payment-module */
            "make-payment-make-payment-module").then(__webpack_require__.bind(null,
            /*! ./make-payment/make-payment.module */
            "./src/app/user/make-payment/make-payment.module.ts")).then(function (m) {
              return m.MakePaymentPageModule;
            });
          }
        }, {
          path: "product-detail",
          loadChildren: function loadChildren() {
            return __webpack_require__.e(
            /*! import() | product-detail-product-detail-module */
            "product-detail-product-detail-module").then(__webpack_require__.bind(null,
            /*! ./product-detail/product-detail.module */
            "./src/app/user/product-detail/product-detail.module.ts")).then(function (m) {
              return m.ProductDetailPageModule;
            });
          }
        }, {
          path: "nearby",
          children: [{
            path: "",
            pathMatch: "full",
            loadChildren: function loadChildren() {
              return __webpack_require__.e(
              /*! import() | nearby-nearby-module */
              "nearby-nearby-module").then(__webpack_require__.bind(null,
              /*! ./nearby/nearby.module */
              "./src/app/user/nearby/nearby.module.ts")).then(function (m) {
                return m.NearbyPageModule;
              });
            }
          }, {
            path: "business-detail/:merchantId",
            loadChildren: function loadChildren() {
              return __webpack_require__.e(
              /*! import() | nearby-business-detail-business-detail-module */
              "business-detail-business-detail-module").then(__webpack_require__.bind(null,
              /*! ./nearby/business-detail/business-detail.module */
              "./src/app/user/nearby/business-detail/business-detail.module.ts")).then(function (m) {
                return m.BusinessDetailPageModule;
              });
            }
          }]
        }, {
          path: "orders",
          children: [{
            path: "",
            pathMatch: "full",
            loadChildren: function loadChildren() {
              return __webpack_require__.e(
              /*! import() | orders-orders-module */
              "orders-orders-module").then(__webpack_require__.bind(null,
              /*! ./orders/orders.module */
              "./src/app/user/orders/orders.module.ts")).then(function (m) {
                return m.OrdersPageModule;
              });
            }
          }, {
            path: "track-order",
            resolve: {
              cartLocation: _resolvers_cart_location_resolver__WEBPACK_IMPORTED_MODULE_3__["CartLocationResolver"]
            },
            loadChildren: function loadChildren() {
              return __webpack_require__.e(
              /*! import() | orders-track-order-track-order-module */
              "track-order-track-order-module").then(__webpack_require__.bind(null,
              /*! ./orders/track-order/track-order.module */
              "./src/app/user/orders/track-order/track-order.module.ts")).then(function (m) {
                return m.TrackOrderPageModule;
              });
            }
          }, {
            path: "order-info",
            loadChildren: function loadChildren() {
              return __webpack_require__.e(
              /*! import() | orders-order-info-order-info-module */
              "default~order-info-order-info-module~orders-order-info-order-info-module").then(__webpack_require__.bind(null,
              /*! ./orders/order-info/order-info.module */
              "./src/app/user/orders/order-info/order-info.module.ts")).then(function (m) {
                return m.OrderInfoPageModule;
              });
            }
          }]
        }]
      }];

      var UserRoutingModule = function UserRoutingModule() {
        _classCallCheck(this, UserRoutingModule);
      };

      UserRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], UserRoutingModule);
      /***/
    },

    /***/
    "./src/app/user/user.module.ts":
    /*!*************************************!*\
      !*** ./src/app/user/user.module.ts ***!
      \*************************************/

    /*! exports provided: UserModule */

    /***/
    function srcAppUserUserModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UserModule", function () {
        return UserModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _ngrx_effects__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ngrx/effects */
      "./node_modules/@ngrx/effects/__ivy_ngcc__/fesm2015/ngrx-effects.js");
      /* harmony import */


      var _ngrx_store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ngrx/store */
      "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");
      /* harmony import */


      var _shared_shared_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../shared/shared.module */
      "./src/app/shared/shared.module.ts");
      /* harmony import */


      var _user_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./user-routing.module */
      "./src/app/user/user-routing.module.ts");
      /* harmony import */


      var _store_effects_user_effect_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./store/effects/user-effect.service */
      "./src/app/user/store/effects/user-effect.service.ts");
      /* harmony import */


      var _store_reducer_user_reducer__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ./store/reducer/user.reducer */
      "./src/app/user/store/reducer/user.reducer.ts");

      var UserModule = function UserModule() {
        _classCallCheck(this, UserModule);
      };

      UserModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [],
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_5__["SharedModule"], _user_routing_module__WEBPACK_IMPORTED_MODULE_6__["UserRoutingModule"], _ngrx_effects__WEBPACK_IMPORTED_MODULE_3__["EffectsModule"].forFeature([_store_effects_user_effect_service__WEBPACK_IMPORTED_MODULE_7__["UserEffectService"]]), _ngrx_store__WEBPACK_IMPORTED_MODULE_4__["StoreModule"].forFeature("User", _store_reducer_user_reducer__WEBPACK_IMPORTED_MODULE_8__["UserReducer"])],
        exports: [_user_routing_module__WEBPACK_IMPORTED_MODULE_6__["UserRoutingModule"]]
      })], UserModule);
      /***/
    },

    /***/
    "./src/environments/environment.ts":
    /*!*****************************************!*\
      !*** ./src/environments/environment.ts ***!
      \*****************************************/

    /*! exports provided: environment */

    /***/
    function srcEnvironmentsEnvironmentTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "environment", function () {
        return environment;
      }); // This file can be replaced during build by using the `fileReplacements` array.
      // `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
      // The list of file replacements can be found in `angular.json`.


      var environment = {
        production: false,
        apiRoot: "http://udeolisa-001-site1.gtempurl.com",
        apiRoot2: "https://b9d26a5093eb.ngrok.io",
        websocketRoot: "http://udeolisa-001-site1.gtempurl.com/chat",
        googleAPIKey: "AIzaSyDGi8jX0dEEC8C7M6dWXdc6Rjo94fLaMGU",
        mapQuestAPIKey: "lYrP4vF3Uk5zgTiGGuEzQGwGIVDGuy24",
        paystackPublicTestKey: "pk_test_deb472537dfaa30e50cb8e612fca9e3ff333f341"
      };
      /*
       * For easier debugging in development mode, you can import the following file
       * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
       *
       * This import should be commented out in production mode because it will have a negative impact
       * on performance if an error is thrown.
       */
      // import 'zone.js/dist/zone-error';  // Included with Angular CLI.

      /***/
    },

    /***/
    "./src/main.ts":
    /*!*********************!*\
      !*** ./src/main.ts ***!
      \*********************/

    /*! no exports provided */

    /***/
    function srcMainTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/platform-browser-dynamic */
      "./node_modules/@angular/platform-browser-dynamic/__ivy_ngcc__/fesm2015/platform-browser-dynamic.js");
      /* harmony import */


      var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./app/app.module */
      "./src/app/app.module.ts");
      /* harmony import */


      var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./environments/environment */
      "./src/environments/environment.ts");

      if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
      }

      Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])["catch"](function (err) {
        return console.log(err);
      });
      /***/
    },

    /***/
    "./src/utils/functions/app.functions.ts":
    /*!**********************************************!*\
      !*** ./src/utils/functions/app.functions.ts ***!
      \**********************************************/

    /*! exports provided: saveDataToLocalStorage, getDataFromLocalStorage, deleteDataFromLocalStorage, findBookmarkInLocalStorage, addBookmarkToLocalStorage, findUserBookmark, swapUserDataAfterUpdate, capitalizeFirstCharacter, addProductToCart, removeProductFromCart, groupUserOrders, cancelCartDelivery, markCartAsInTransit, pushUniqueArrayValues, filterMerchantByType, clearObjectValues, mountGoogleMapScript, mountPlotlyScript, isScriptImported */

    /***/
    function srcUtilsFunctionsAppFunctionsTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "saveDataToLocalStorage", function () {
        return saveDataToLocalStorage;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "getDataFromLocalStorage", function () {
        return getDataFromLocalStorage;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "deleteDataFromLocalStorage", function () {
        return deleteDataFromLocalStorage;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "findBookmarkInLocalStorage", function () {
        return findBookmarkInLocalStorage;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "addBookmarkToLocalStorage", function () {
        return addBookmarkToLocalStorage;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "findUserBookmark", function () {
        return findUserBookmark;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "swapUserDataAfterUpdate", function () {
        return swapUserDataAfterUpdate;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "capitalizeFirstCharacter", function () {
        return capitalizeFirstCharacter;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "addProductToCart", function () {
        return addProductToCart;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "removeProductFromCart", function () {
        return removeProductFromCart;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "groupUserOrders", function () {
        return groupUserOrders;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "cancelCartDelivery", function () {
        return cancelCartDelivery;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "markCartAsInTransit", function () {
        return markCartAsInTransit;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "pushUniqueArrayValues", function () {
        return pushUniqueArrayValues;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "filterMerchantByType", function () {
        return filterMerchantByType;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "clearObjectValues", function () {
        return clearObjectValues;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "mountGoogleMapScript", function () {
        return mountGoogleMapScript;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "mountPlotlyScript", function () {
        return mountPlotlyScript;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "isScriptImported", function () {
        return isScriptImported;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _capacitor_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @capacitor/core */
      "./node_modules/@capacitor/core/dist/esm/index.js");
      /* harmony import */


      var _app_user_store_model_user_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../../app/user/store/model/user.model */
      "./src/app/user/store/model/user.model.ts");
      /* harmony import */


      var _types_app_constant__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../types/app.constant */
      "./src/utils/types/app.constant.ts");

      var Storage = _capacitor_core__WEBPACK_IMPORTED_MODULE_1__["Plugins"].Storage;

      var saveDataToLocalStorage = function saveDataToLocalStorage(payload, key) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(void 0, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee52() {
          return regeneratorRuntime.wrap(function _callee52$(_context52) {
            while (1) {
              switch (_context52.prev = _context52.next) {
                case 0:
                  _context52.next = 2;
                  return Storage.set({
                    key: key,
                    value: JSON.stringify(payload)
                  });

                case 2:
                case "end":
                  return _context52.stop();
              }
            }
          }, _callee52);
        }));
      };

      var getDataFromLocalStorage = function getDataFromLocalStorage(key) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(void 0, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee53() {
          var ret;
          return regeneratorRuntime.wrap(function _callee53$(_context53) {
            while (1) {
              switch (_context53.prev = _context53.next) {
                case 0:
                  _context53.next = 2;
                  return Storage.get({
                    key: key
                  });

                case 2:
                  ret = _context53.sent;
                  return _context53.abrupt("return", JSON.parse(ret.value));

                case 4:
                case "end":
                  return _context53.stop();
              }
            }
          }, _callee53);
        }));
      };

      var deleteDataFromLocalStorage = function deleteDataFromLocalStorage(key) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(void 0, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee54() {
          return regeneratorRuntime.wrap(function _callee54$(_context54) {
            while (1) {
              switch (_context54.prev = _context54.next) {
                case 0:
                  _context54.next = 2;
                  return Storage.remove({
                    key: key
                  });

                case 2:
                  return _context54.abrupt("return", _context54.sent);

                case 3:
                case "end":
                  return _context54.stop();
              }
            }
          }, _callee54);
        }));
      };

      var findBookmarkInLocalStorage = function findBookmarkInLocalStorage(id, allBookmarks) {
        return allBookmarks.find(function (data) {
          return data.Id === id;
        });
      };

      var addBookmarkToLocalStorage = function addBookmarkToLocalStorage(payload) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(void 0, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee55() {
          var bookmarks;
          return regeneratorRuntime.wrap(function _callee55$(_context55) {
            while (1) {
              switch (_context55.prev = _context55.next) {
                case 0:
                  _context55.next = 2;
                  return getDataFromLocalStorage(_types_app_constant__WEBPACK_IMPORTED_MODULE_3__["LocalStorageKey"].BOOKMARKS);

                case 2:
                  _context55.t0 = _context55.sent;

                  if (_context55.t0) {
                    _context55.next = 5;
                    break;
                  }

                  _context55.t0 = [];

                case 5:
                  bookmarks = _context55.t0;

                  if (findBookmarkInLocalStorage(payload.Id, bookmarks)) {
                    _context55.next = 10;
                    break;
                  }

                  bookmarks.push(payload);
                  _context55.next = 10;
                  return saveDataToLocalStorage(bookmarks, _types_app_constant__WEBPACK_IMPORTED_MODULE_3__["LocalStorageKey"].BOOKMARKS);

                case 10:
                case "end":
                  return _context55.stop();
              }
            }
          }, _callee55);
        }));
      };
      /**
       * Get the bookmark from localstorage
       * @param id
       */


      var findUserBookmark = function findUserBookmark(id) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(void 0, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee56() {
          var bookmarks;
          return regeneratorRuntime.wrap(function _callee56$(_context56) {
            while (1) {
              switch (_context56.prev = _context56.next) {
                case 0:
                  _context56.next = 2;
                  return getDataFromLocalStorage(_types_app_constant__WEBPACK_IMPORTED_MODULE_3__["LocalStorageKey"].BOOKMARKS);

                case 2:
                  _context56.t0 = _context56.sent;

                  if (_context56.t0) {
                    _context56.next = 5;
                    break;
                  }

                  _context56.t0 = [];

                case 5:
                  bookmarks = _context56.t0;
                  return _context56.abrupt("return", findBookmarkInLocalStorage(id, bookmarks));

                case 7:
                case "end":
                  return _context56.stop();
              }
            }
          }, _callee56);
        }));
      };
      /**
       * Update the user details that were changed by a user in state before,
       * updating online
       * @param updatedUserData
       * @param userData
       */


      var swapUserDataAfterUpdate = function swapUserDataAfterUpdate(updatedUserData, userData) {
        var address = updatedUserData.address,
            dob = updatedUserData.dob,
            email = updatedUserData.email,
            firstName = updatedUserData.firstName,
            lastName = updatedUserData.lastName;
        var newUserData = Object.assign(Object.assign({}, userData), {
          username: email,
          person: Object.assign(Object.assign({}, userData.person), {
            address: address,
            firstName: firstName,
            lastName: lastName,
            dob: dob
          })
        });
        return newUserData;
      };

      var capitalizeFirstCharacter = function capitalizeFirstCharacter(s) {
        var _a;

        if (typeof s !== "string") {
          return "";
        }

        return "".concat(s.charAt(0).toUpperCase()).concat((_a = s.slice(1)) === null || _a === void 0 ? void 0 : _a.toLowerCase());
      };

      var addProductToCart = function addProductToCart(newProduct, cartItems, userId, deliveryAddress) {
        var _a;

        try {
          // ? Check if this value is undefined
          // ? Initialize the store
          if (typeof cartItems === "undefined" || !cartItems) {
            cartItems = {
              userId: userId,
              deliveryAddress: deliveryAddress,
              productCheckouts: [Object.assign({}, newProduct)]
            };
          } else {
            // ? Add a new product to store
            // ? Check if that product already exists
            // ? If (true) Increase the quantity and save
            var productExistIndex = cartItems.productCheckouts.findIndex(function (product) {
              return product.productId === newProduct.productId;
            });

            if (productExistIndex !== -1) {
              var updatedProducts = (_a = cartItems === null || cartItems === void 0 ? void 0 : cartItems.productCheckouts) === null || _a === void 0 ? void 0 : _a.map(function (product, index) {
                if (productExistIndex === index) {
                  return Object.assign(Object.assign({}, product), {
                    quantity: newProduct.quantity,
                    cost: newProduct.cost
                  });
                } else {
                  return Object.assign({}, product);
                }
              });
              cartItems = Object.assign(Object.assign({}, cartItems), {
                productCheckouts: _toConsumableArray(updatedProducts)
              });
            } else {
              cartItems = Object.assign(Object.assign({}, cartItems), {
                productCheckouts: [].concat(_toConsumableArray(cartItems.productCheckouts), [newProduct])
              });
            }
          }

          return Object.assign({}, cartItems);
        } catch (ex) {
          throw ex;
        }
      };

      var removeProductFromCart = function removeProductFromCart(productId, cartItems) {
        var _a, _b;

        if (((_a = cartItems === null || cartItems === void 0 ? void 0 : cartItems.productCheckouts) === null || _a === void 0 ? void 0 : _a.length) > 0) {
          /**
           * Splice mutates the state of the array it's used on
           */
          var productCheckouts = (_b = cartItems === null || cartItems === void 0 ? void 0 : cartItems.productCheckouts) === null || _b === void 0 ? void 0 : _b.filter(function (product) {
            return product.productId !== productId;
          });
          return Object.assign(Object.assign({}, cartItems), {
            productCheckouts: _toConsumableArray(productCheckouts)
          });
        }

        return Object.assign({}, cartItems);
      };

      var groupUserOrders = function groupUserOrders(payload) {
        var _a, _b, _c, _d, _e;

        try {
          var verifiedOrders = payload === null || payload === void 0 ? void 0 : payload.filter(function (data) {
            return data.status === _app_user_store_model_user_model__WEBPACK_IMPORTED_MODULE_2__["ProductCartStatus"].VERIFIED;
          });
          var unVerifiedOrders = payload === null || payload === void 0 ? void 0 : payload.filter(function (data) {
            return data.status === _app_user_store_model_user_model__WEBPACK_IMPORTED_MODULE_2__["ProductCartStatus"].UNVERIFIED;
          });
          var cancelledOrders = payload === null || payload === void 0 ? void 0 : payload.filter(function (data) {
            return data.status === _app_user_store_model_user_model__WEBPACK_IMPORTED_MODULE_2__["ProductCartStatus"].CANCELLED;
          });
          var transitOrders = payload === null || payload === void 0 ? void 0 : payload.filter(function (data) {
            return data.status === _app_user_store_model_user_model__WEBPACK_IMPORTED_MODULE_2__["ProductCartStatus"].TRANSIT;
          });
          var completedOrders = payload === null || payload === void 0 ? void 0 : payload.filter(function (data) {
            return data.status === _app_user_store_model_user_model__WEBPACK_IMPORTED_MODULE_2__["ProductCartStatus"].COMPLETED;
          });
          return {
            cancelled: (_a = _toConsumableArray(cancelledOrders)) !== null && _a !== void 0 ? _a : [],
            completed: (_b = _toConsumableArray(completedOrders)) !== null && _b !== void 0 ? _b : [],
            transit: (_c = [].concat(_toConsumableArray(transitOrders), _toConsumableArray(verifiedOrders))) !== null && _c !== void 0 ? _c : [],
            unverified: (_d = _toConsumableArray(unVerifiedOrders)) !== null && _d !== void 0 ? _d : [],
            verified: (_e = _toConsumableArray(verifiedOrders)) !== null && _e !== void 0 ? _e : []
          };
        } catch (ex) {
          throw ex;
        }
      };

      var cancelCartDelivery = function cancelCartDelivery(cartId, allDeliveries, ongoingDeliveries) {
        try {
          var allDeliveriesRemap = allDeliveries.filter(function (data) {
            return (data === null || data === void 0 ? void 0 : data.cart.id) !== cartId;
          });
          var ongoingDeliveriesRemap = ongoingDeliveries.filter(function (data) {
            return (data === null || data === void 0 ? void 0 : data.cart.id) !== cartId;
          });
          return {
            All: _toConsumableArray(allDeliveriesRemap),
            Ongoing: _toConsumableArray(ongoingDeliveriesRemap)
          };
        } catch (ex) {
          throw ex;
        }
      };

      var markCartAsInTransit = function markCartAsInTransit(cartId, allDeliveries, ongoingDeliveries, status) {
        try {
          var ongoingDeliveriesRemap = ongoingDeliveries.map(function (data) {
            if (data.cart.id === cartId) {
              return Object.assign(Object.assign({}, data), {
                cart: Object.assign(Object.assign({}, data.cart), {
                  status: status
                })
              });
            } else {
              return Object.assign({}, data);
            }
          });
          var allDeliveriesRemap = allDeliveries.map(function (data) {
            if (data.cart.id === cartId) {
              return Object.assign(Object.assign({}, data), {
                cart: Object.assign(Object.assign({}, data.cart), {
                  status: status
                })
              });
            } else {
              return Object.assign({}, data);
            }
          });
          return {
            All: _toConsumableArray(allDeliveriesRemap),
            Ongoing: _toConsumableArray(ongoingDeliveriesRemap)
          };
        } catch (ex) {
          throw ex;
        }
      };

      var pushUniqueArrayValues = function pushUniqueArrayValues(newElement, existingElement) {
        var placeAt = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : "END";

        try {
          var returnedData;
          var elementExists = existingElement.find(function (data) {
            return data === newElement;
          });

          if (elementExists) {
            returnedData = _toConsumableArray(existingElement);
          } else {
            if (placeAt === "END") {
              returnedData = [].concat(_toConsumableArray(existingElement), [newElement]);
            } else {
              returnedData = [newElement].concat(_toConsumableArray(existingElement));
            }
          }

          return returnedData;
        } catch (ex) {
          throw ex;
        }
      };

      var filterMerchantByType = function filterMerchantByType(merchantModels, typeId) {
        try {
          return merchantModels.filter(function (merchant) {
            return merchant.merchantTypeModel.id === typeId;
          });
        } catch (ex) {
          throw ex;
        }
      };

      var clearObjectValues = function clearObjectValues(objToClear) {
        Object.keys(objToClear).forEach(function (param) {
          if (objToClear[param].toString() === "[object Object]") {
            clearObjectValues(objToClear[param]);
          } else {
            objToClear[param] = undefined;
          }
        });
        return objToClear;
      };

      var mountGoogleMapScript = function mountGoogleMapScript(apiKey) {
        var scriptId = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "googleMaps";

        // First check if the script already exists on the dom
        // by searching for an id
        if (navigator.onLine) {
          if (document.getElementById(scriptId) === null) {
            var script = document.createElement("script");
            script.setAttribute("src", "http://maps.googleapis.com/maps/api/js?key=".concat(apiKey));
            script.setAttribute("id", scriptId);
            document.body.appendChild(script); // now wait for it to load...

            script.onload = function () {
              // script has loaded, you can now use it safely
              console.log({
                message: "Google map loaded successfully"
              }); // ... do something with the newly loaded script
            };
          }
        }
      };

      var mountPlotlyScript = function mountPlotlyScript() {
        var scriptId = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "plotly";

        // First check if the script already exists on the dom
        // by searching for an id
        if (navigator.onLine) {
          if (document.getElementById(scriptId) === null) {
            var script = document.createElement("script");
            script.setAttribute("src", "https://cdn.plot.ly/plotly-latest.min.js");
            script.setAttribute("id", scriptId);
            document.body.appendChild(script); // now wait for it to load...

            script.onload = function () {
              // script has loaded, you can now use it safely
              console.log({
                message: "PlotlyJS CDN loaded successfully"
              }); // ... do something with the newly loaded script
            };
          }
        }
      };

      var isScriptImported = function isScriptImported(scriptId) {
        return document.querySelector("#".concat(scriptId)) === null ? false : true;
      };
      /***/

    },

    /***/
    "./src/utils/types/app.constant.ts":
    /*!*****************************************!*\
      !*** ./src/utils/types/app.constant.ts ***!
      \*****************************************/

    /*! exports provided: APIMessage, Zero30Role, LocalStorageKey, TransportType, PushNotificationCategory, PushNotificationEvent */

    /***/
    function srcUtilsTypesAppConstantTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "APIMessage", function () {
        return APIMessage;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Zero30Role", function () {
        return Zero30Role;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LocalStorageKey", function () {
        return LocalStorageKey;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TransportType", function () {
        return TransportType;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PushNotificationCategory", function () {
        return PushNotificationCategory;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PushNotificationEvent", function () {
        return PushNotificationEvent;
      });

      var APIMessage = {
        BAD_OPERATION: "This operation is not supported",
        RECORD_EXISTS: "Record already exists",
        SUCCESSFUL: "Successful",
        NOT_FOUND: "Record not found",
        UNAUTHORIZED_REQUEST: "You are not authorized to view this route",
        INACTIVE_ACCOUNT: "Your account is inactive",
        EXPIRED_TOKEN_MESSAGE: "Unauthorized access. You are using an expired token",
        UNAUTHORIZED_ERROR_MESSAGE: "Unauthorized Access",
        CREATED: "Created",
        DELETED: "Deleted",
        UPDATE: "Updated",
        ROLE_MISMATCH: "This feature does not work this role",
        SET_GROUP_LIMIT: "Specify how many user(s) can share this group plan",
        VALIDATION_SUCCESSFUL: "Validation successful"
      };
      var Zero30Role;

      (function (Zero30Role) {
        Zero30Role["CUSTOMER"] = "CUSTOMER";
        Zero30Role["RIDER"] = "RIDER";
      })(Zero30Role || (Zero30Role = {}));

      var LocalStorageKey;

      (function (LocalStorageKey) {
        LocalStorageKey["ZERO_30_USER"] = "Zero30User";
        LocalStorageKey["BOOKMARKS"] = "Bookmarks";
        LocalStorageKey["CART"] = "CART";
        LocalStorageKey["DEVICE_KEY"] = "DEVICE_KEY";
        LocalStorageKey["URL_HISTORY"] = "URL_HISTORY";
      })(LocalStorageKey || (LocalStorageKey = {}));

      var TransportType;

      (function (TransportType) {
        TransportType["DRIVING"] = "DRIVING";
        TransportType["WALKING"] = "WALKING";
        TransportType["TRANSIT"] = "TRANSIT";
        TransportType["BICYCLING"] = "BICYCLING";
      })(TransportType || (TransportType = {}));

      var PushNotificationCategory;

      (function (PushNotificationCategory) {
        PushNotificationCategory["CART_ASSIGNMENT"] = "0";
        PushNotificationCategory["CART_VERIFICATION"] = "1";
        PushNotificationCategory["PRODUCT_REMOVAL"] = "2";
      })(PushNotificationCategory || (PushNotificationCategory = {}));

      var PushNotificationEvent;

      (function (PushNotificationEvent) {
        PushNotificationEvent["PUSH_REGISTRATION"] = "registration";
        PushNotificationEvent["PUSH_REGISTRATION_ERROR"] = "registrationError";
        PushNotificationEvent["PUSH_NOTIFICATION_RECIEVED"] = "pushNotificationReceived";
        PushNotificationEvent["PUSH_NOTIFICATION_ACTION"] = "pushNotificationActionPerformed";
      })(PushNotificationEvent || (PushNotificationEvent = {}));
      /***/

    },

    /***/
    0:
    /*!***************************!*\
      !*** multi ./src/main.ts ***!
      \***************************/

    /*! no static exports found */

    /***/
    function _(module, exports, __webpack_require__) {
      module.exports = __webpack_require__(
      /*! /Users/mac/Documents/ionic_projects/zero-30-delivery/src/main.ts */
      "./src/main.ts");
      /***/
    }
  }, [[0, "runtime", "vendor"]]]);
})();
//# sourceMappingURL=main-es5.js.map