(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~order-info-order-info-module~orders-order-info-order-info-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/user/orders/order-info/order-info.page.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/user/orders/order-info/order-info.page.html ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n  <main id=\"order-information-page\" class=\"container\">\n    <!-- Wrapping everything in a form so it can be submitted to the database. -->\n    <form>\n      <section id=\"header\" class=\"constrain header-white\">\n        <a backButton class=\"back link\">\n          <img class=\"svg\" src=\"assets/images/icons/arrow-left.svg\" width=\"18px\" alt=\"Go back\">\n        </a>\n  \n        <div class=\"text\">\n          <h2 class=\"title\">Order Information</h2>\n  \n          <p class=\"id\">\n            <span>ID:</span> {{ cartId }}\n          </p>\n        </div>\n      </section>\n  \n      <section id=\"delivery-address\" class=\"constrain\">\n        <div class=\"header\">\n          <h5>Delivery to</h5>\n          <a [routerLink]=\"['/profile', 'add-address']\" \n            class=\"link link-primary\">\n            Add new address\n          </a>\n        </div>\n  \n        <div class=\"address\">\n          <!-- i think a map goes here. -->\n          <div class=\"map\">\n            <img src=\"assets/images/others/address-map.png\" height=\"100px\" alt=\"Map\">\n          </div>\n  \n          <div class=\"details\" *ngIf=\"(selectedCart$ | async) as selectedCart\">\n            <h5 class=\"location-address\">\n              <img class=\"svg\" src=\"assets/images/icons/map-pointer.svg\" height=\"9px\" alt=\"Store\">\n              <span>{{ selectedCart?.deliveryAddress | capitalize }}</span>\n            </h5>\n            <p class=\"name\">\n              <img class=\"svg\" src=\"assets/images/icons/person.svg\" height=\"9px\" alt=\"Store\">\n              <span *ngIf=\"(userData$ | async)?.person as person\">\n                {{ person.firstName | capitalize }} \n                {{ person.lastName | capitalize }}\n              </span>\n            </p>\n            <p class=\"phone\">\n              <img class=\"svg\" src=\"assets/images/icons/telephone.svg\" height=\"9px\" alt=\"Store\">\n              <span>{{ (userData$ | async)?.person.phoneNo }}</span>\n            </p>\n          </div>\n        </div>\n      </section>\n  \n      <section id=\"delivery-details\" >\n        <div class=\"heading constrain\">\n          <div class=\"v-grid\">\n            <h5>Delivery Time</h5>\n            <div class=\"h-grid\">\n              <p>{{ (selectedCart$ | async)?.dateCreated | date : 'shortTime' }}</p>\n              <p>{{ (selectedCart$ | async)?.dateCreated | date : 'mediumDate' }}</p>\n            </div>\n          </div>\n          <!-- <a href=\"#\" class=\"link link-primary\">Edit</a> -->\n        </div>\n  \n        <div class=\"cart-item constrain\">\n          <div class=\"image\" *ngIf=\"(selectedCart$ | async) as selectedCart\">\n            <img [src]=\"getProductImageToPreview(selectedCart.productCheckouts)\"\n              *ngIf=\"getProductImageToPreview(selectedCart.productCheckouts); else defaultTemplate\"\n              height=\"100px\" alt=\"Merchant Image\" />\n            <ng-template #defaultTemplate>\n              <img src=\"assets/images/restaurants/lion-square.png\" \n                height=\"100px\" \n                alt=\"Merchant Image\" />\n            </ng-template>\n          </div>\n          <div class=\"details\" *ngIf=\"(selectedCart$ | async) as selectedCart\">\n            <h5 class=\"name\">\n              {{ selectedCart.productCheckouts[0]?.productName | uppercase }}\n            </h5>\n            <p class=\"product\">{{ selectedCart.productCheckouts[0]?.productName | capitalize }}</p>\n          </div>\n        </div>\n  \n        <div class=\"cart-totals constrain\" *ngIf=\"(selectedCart$ | async) as selectedCart\">\n          <div class=\"h-grid\">\n            <p>Subtotal (1 item)</p>\n            <p>{{ selectedCart?.productCheckouts | productCostCalculator | currency }}</p>\n          </div>\n          <div class=\"h-grid\">\n            <p>VAT</p>\n            <p>{{ selectedCart.vat | currency }}</p>\n          </div>\n          <div class=\"h-grid\">\n            <p>Delivery Fee ({{ selectedCart.distance || 0 }} Km)</p>\n            <p>{{ selectedCart.deliveryCost | currency }}</p>\n          </div>\n          <div class=\"totals\">\n            <h4>Total</h4>\n            <h4>{{ selectedCart.total | currency }}</h4>\n          </div>\n        </div>\n      </section>\n  \n      <section id=\"note\">\n        <div class=\"title constrain\">\n          <h5>Note</h5>\n        </div>\n  \n        <div class=\"constrain\">\n          <!-- What does this do -->\n          <textarea name=\"note\" \n            placeholder=\"Please call me when you come. Thank you!\" \n            id=\"note\">\n          </textarea>\n        </div>\n        \n      </section>\n      \n      <div class=\"submit constrain\" *ngIf=\"(selectedCart$ | async) as selectedCart\">\n        <a class=\"btn btn-outline-black cursor-pointer\">Rating</a>\n        <button type=\"button\" \n          class=\"btn btn-primary\" \n          (click)=\"reorderOldCart(selectedCart.productCheckouts, selectedCart.deliveryAddress)\">\n          Re-Order\n        </button>\n      </div>\n    </form>\n\n    <app-footer-expander></app-footer-expander>\n\n    <app-user-bottom-navbar></app-user-bottom-navbar>\n\n    </main>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/user/orders/order-info/order-info-routing.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/user/orders/order-info/order-info-routing.module.ts ***!
  \*********************************************************************/
/*! exports provided: OrderInfoPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderInfoPageRoutingModule", function() { return OrderInfoPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _order_info_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./order-info.page */ "./src/app/user/orders/order-info/order-info.page.ts");




const routes = [
    {
        path: '',
        component: _order_info_page__WEBPACK_IMPORTED_MODULE_3__["OrderInfoPage"]
    }
];
let OrderInfoPageRoutingModule = class OrderInfoPageRoutingModule {
};
OrderInfoPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], OrderInfoPageRoutingModule);



/***/ }),

/***/ "./src/app/user/orders/order-info/order-info.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/user/orders/order-info/order-info.module.ts ***!
  \*************************************************************/
/*! exports provided: OrderInfoPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderInfoPageModule", function() { return OrderInfoPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _order_info_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./order-info-routing.module */ "./src/app/user/orders/order-info/order-info-routing.module.ts");
/* harmony import */ var _order_info_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./order-info.page */ "./src/app/user/orders/order-info/order-info.page.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../shared/shared.module */ "./src/app/shared/shared.module.ts");








let OrderInfoPageModule = class OrderInfoPageModule {
};
OrderInfoPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"],
            _order_info_routing_module__WEBPACK_IMPORTED_MODULE_5__["OrderInfoPageRoutingModule"]
        ],
        declarations: [_order_info_page__WEBPACK_IMPORTED_MODULE_6__["OrderInfoPage"]]
    })
], OrderInfoPageModule);



/***/ }),

/***/ "./src/app/user/orders/order-info/order-info.page.scss":
/*!*************************************************************!*\
  !*** ./src/app/user/orders/order-info/order-info.page.scss ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXIvb3JkZXJzL29yZGVyLWluZm8vb3JkZXItaW5mby5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/user/orders/order-info/order-info.page.ts":
/*!***********************************************************!*\
  !*** ./src/app/user/orders/order-info/order-info.page.ts ***!
  \***********************************************************/
/*! exports provided: OrderInfoPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderInfoPage", function() { return OrderInfoPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");
/* harmony import */ var subsink__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! subsink */ "./node_modules/subsink/dist/es2015/index.js");
/* harmony import */ var _user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../user/store/actions/user.action */ "./src/app/user/store/actions/user.action.ts");
/* harmony import */ var _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../utils/types/app.constant */ "./src/utils/types/app.constant.ts");
/* harmony import */ var _utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../utils/functions/app.functions */ "./src/utils/functions/app.functions.ts");








let OrderInfoPage = class OrderInfoPage {
    constructor(store, activatedRoute, router) {
        this.store = store;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.subSink = new subsink__WEBPACK_IMPORTED_MODULE_4__["SubSink"]();
    }
    ngOnInit() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const { userId } = yield Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_7__["getDataFromLocalStorage"])(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_6__["LocalStorageKey"].ZERO_30_USER);
            this.userId = userId;
            this.subSink.sink =
                this.activatedRoute.queryParams.subscribe((data) => {
                    if (!(data === null || data === void 0 ? void 0 : data.cartId)) {
                        this.router.navigate(['/user', 'orders']);
                    }
                    const { cartId, status } = data;
                    this.cartId = cartId;
                    this.cartStatus = status;
                    //? Get selected cart
                    this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetSelectedCartInitiatedAction({ payload: { cartId } }));
                    this.selectedCart$ = this.store.select((data) => data.User.SelectedCart);
                    //? Get userData
                    this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetUserDataInitiatedAction({ payload: { userId } }));
                    this.userData$ = this.store.select((data) => data.User.UserData);
                });
        });
    }
    reorderOldCart(products, deliveryAddress) {
        if (this.userId) {
            //? Delete "OrderId" from product object
            const productsRemapped = products.map((data) => {
                const { cost, productId, productName, quantity } = data;
                return {
                    cost,
                    productId,
                    productName,
                    quantity
                };
            });
            for (const product of productsRemapped) {
                this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].AddProductToCartInitiatedAction({ payload: { userId: this.userId, deliveryAddress, product } }));
            }
            this.router.navigate(["/user", "product-cart"]);
        }
    }
    getProductImageToPreview(products) {
        let imagePath;
        if (products[0].productImage) {
            imagePath = products[0].productImage;
        }
        else {
            const productsWithImages = products.filter((data) => data.productImage);
            if ((productsWithImages === null || productsWithImages === void 0 ? void 0 : productsWithImages.length) > 0) {
                imagePath = productsWithImages[0].productImage;
            }
        }
        return imagePath;
    }
    ngOnDestroy() {
        this.subSink.unsubscribe();
    }
};
OrderInfoPage.ctorParameters = () => [
    { type: _ngrx_store__WEBPACK_IMPORTED_MODULE_3__["Store"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
OrderInfoPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-order-info',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./order-info.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/user/orders/order-info/order-info.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./order-info.page.scss */ "./src/app/user/orders/order-info/order-info.page.scss")).default]
    })
], OrderInfoPage);



/***/ })

}]);
//# sourceMappingURL=default~order-info-order-info-module~orders-order-info-order-info-module-es2015.js.map