(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["product-detail-product-detail-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/user/product-detail/product-detail.page.html":
    /*!****************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/user/product-detail/product-detail.page.html ***!
      \****************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppUserProductDetailProductDetailPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n  <main id=\"nearby-page\" class=\"container with-bottom-menu bg-offwhite\">\n    <section class=\"header-area\">\n      <div class=\"header-section\">\n        <a backButton class=\"back link\">\n          <img class=\"svg\" src=\"assets/images/icons/arrow-left.svg\" width=\"18px\" alt=\"Go back\" />\n        </a>\n      </div>\n      <div class=\"header-section\" *ngIf=\"(product$ | async) as product\">\n        <div class=\"text\">\n          <h2 class=\"name\">{{ product.productModel.name | capitalize }}</h2>\n        </div>\n      </div>\n    </section>\n\n    <section class=\"product-detail-area\" *ngIf=\"(product$ | async) as product\">\n      <div *ngIf=\"product?.productImageModels.length > 0\">\n        <swiper id=\"productImage\"\n          *ngIf=\"product?.productImageModels?.length > 1; else defaultSingleProductImage\"\n          [slidesPerView]=\"'1'\"\n          [spaceBetween]=\"30\"\n          [centeredSlides]=\"false\"\n          [loop]=\"true\"\n          [autoplay]=\"{ delay: 2500, disableOnInteraction: true }\">\n          <ng-template swiperSlide *ngFor=\"let image of product.productImageModels\">\n            <ion-card>\n              <ion-card-header>\n                <button class=\"btn bookmark\" (click)=\"addProductToBookmark(product.productModel.id)\">\n                  <img class=\"svg\" src=\"assets/images/icons/bookmark.svg\" height=\"18px\" alt=\"Bookmark\">\n                </button>\n              </ion-card-header>\n              <ion-card-content>\n              <div class=\"img-area\">\n                <img [src]=\"image.path | filePathFormatter\" [alt]=\"product.productModel.name\" />\n              </div>\n              </ion-card-content>\n            </ion-card>\n          </ng-template>\n        </swiper>\n\n        <ng-template #defaultSingleProductImage>\n          <ion-card>\n            <ion-card-header>\n              <button class=\"btn bookmark\" (click)=\"addProductToBookmark(product.productModel.id)\">\n                <img class=\"svg\" src=\"assets/images/icons/bookmark.svg\" height=\"18px\" alt=\"Bookmark\">\n              </button>\n            </ion-card-header>\n            <ion-card-content>\n            <div class=\"img-area\">\n              <img [src]=\"product.productImageModels[0].path\" [alt]=\"product.productModel.name\" />\n            </div>\n            </ion-card-content>\n          </ion-card>\n        </ng-template>\n      </div>\n   \n       <article *ngIf=\"(product$ | async)?.productModel as productModel\">\n         <h1 class=\"title\">{{ productModel.name | capitalize }}</h1>\n         <span class=\"description\">\n           {{ productModel.description | capitalize }}\n         </span>\n       </article>\n   \n       <ion-item-group *ngIf=\"(product$ | async)?.productCostModel as productCostModel\">\n         <ion-item-divider>\n           <ion-label color=\"danger\">\n             Product details\n           </ion-label>\n         </ion-item-divider>\n         \n         <ion-item>\n           <ion-label>Price</ion-label>\n           <ion-badge color=\"warning\" slot=\"end\">\n             {{ productCostModel.unitPrice | currency }}\n           </ion-badge>\n         </ion-item>\n\n         <ion-item>\n          <ion-label>Rating</ion-label>\n          <ion-label class=\"text-right\">\n            <i *ngFor=\"let star of [0, 1, 2, 3, 4]; index as i\" \n              class=\"fa fa-star\" \n              [ngClass]=\"{ 'fa-checked': i < (product$ | async).productRating }\">\n            </i>\n          </ion-label>\n        </ion-item>\n\n         <ion-item lines=\"none\">\n          <ion-label>Total Orders</ion-label>\n          <!-- <ion-label>Total Rating</ion-label> -->\n          <ion-badge color=\"warning\" slot=\"end\">\n            {{ (product$ | async)['noOfOrders'] }}\n          </ion-badge>\n          </ion-item>\n   \n         <br />\n         <ion-grid>\n           <ion-row>\n             <ion-col size=\"12\" class=\"ion-align-self-start text-right\">\n               <button type=\"button\" class=\"btn btn-primary btn-block\" (click)=\"addProductToCart(product)\">\n                 Add to cart\n               </button>\n             </ion-col>\n           </ion-row>\n           </ion-grid>\n       </ion-item-group>\n   \n     </section>\n\n     <app-footer-expander></app-footer-expander>\n     <app-user-bottom-navbar></app-user-bottom-navbar>\n  </main>\n\n  <app-add-to-cart \n    *ngIf=\"openCartModal\"\n    [product]=\"productAddedToCart\"\n    [productImages]=\"productAddedToCartImages\"\n    [userId]=\"userId\"\n    [show]=\"openCartModal\"\n    (closeAddToCartModalEvent)=\"closeCartModal()\">\n  </app-add-to-cart>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/user/product-detail/product-detail-routing.module.ts":
    /*!**********************************************************************!*\
      !*** ./src/app/user/product-detail/product-detail-routing.module.ts ***!
      \**********************************************************************/

    /*! exports provided: ProductDetailPageRoutingModule */

    /***/
    function srcAppUserProductDetailProductDetailRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ProductDetailPageRoutingModule", function () {
        return ProductDetailPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _product_detail_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./product-detail.page */
      "./src/app/user/product-detail/product-detail.page.ts");

      var routes = [{
        path: '',
        component: _product_detail_page__WEBPACK_IMPORTED_MODULE_3__["ProductDetailPage"]
      }];

      var ProductDetailPageRoutingModule = function ProductDetailPageRoutingModule() {
        _classCallCheck(this, ProductDetailPageRoutingModule);
      };

      ProductDetailPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], ProductDetailPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/user/product-detail/product-detail.module.ts":
    /*!**************************************************************!*\
      !*** ./src/app/user/product-detail/product-detail.module.ts ***!
      \**************************************************************/

    /*! exports provided: ProductDetailPageModule */

    /***/
    function srcAppUserProductDetailProductDetailModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ProductDetailPageModule", function () {
        return ProductDetailPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _product_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./product-detail-routing.module */
      "./src/app/user/product-detail/product-detail-routing.module.ts");
      /* harmony import */


      var _product_detail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./product-detail.page */
      "./src/app/user/product-detail/product-detail.page.ts");
      /* harmony import */


      var _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../../shared/shared.module */
      "./src/app/shared/shared.module.ts");

      var ProductDetailPageModule = function ProductDetailPageModule() {
        _classCallCheck(this, ProductDetailPageModule);
      };

      ProductDetailPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"], _product_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__["ProductDetailPageRoutingModule"]],
        declarations: [_product_detail_page__WEBPACK_IMPORTED_MODULE_6__["ProductDetailPage"]]
      })], ProductDetailPageModule);
      /***/
    },

    /***/
    "./src/app/user/product-detail/product-detail.page.scss":
    /*!**************************************************************!*\
      !*** ./src/app/user/product-detail/product-detail.page.scss ***!
      \**************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppUserProductDetailProductDetailPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".product-detail-area {\n  padding: 10px 20px 30px 20px;\n  min-height: 100vh;\n}\n.product-detail-area ion-card {\n  box-shadow: var(--box-shadow-lg);\n}\n.product-detail-area .img-area {\n  width: 100%;\n  height: 350px;\n  background: whitesmoke;\n  text-align: center;\n}\n.product-detail-area .img-area img {\n  width: 100%;\n  height: 100%;\n  -o-object-fit: cover;\n  object-fit: cover;\n}\n.product-detail-area article {\n  width: 100%;\n  border-bottom: 1px solid #ddd;\n  padding: 0 0 15px 0;\n  font-family: \"Quicksand\", sans-serif;\n}\n.product-detail-area article .title {\n  font-size: 24px;\n  font-weight: 300;\n  margin: 5px 3px 3px 3px;\n  color: var(--bg-deep-black);\n  letter-spacing: 0.5px;\n}\n.product-detail-area article .description {\n  word-spacing: 1px;\n  letter-spacing: 0.5px;\n  font-weight: 100;\n  font-size: 14px;\n  padding: 10px 3px;\n}\n.product-detail-area .price {\n  margin: 10px 3px;\n  font-weight: 200;\n  font-size: 16px;\n  color: var(--bg-deep-black);\n}\n.product-detail-area ion-label {\n  font-size: 16px;\n}\n.product-detail-area ion-card ion-card-header {\n  text-align: right;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXNlci9wcm9kdWN0LWRldGFpbC9wcm9kdWN0LWRldGFpbC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSw0QkFBQTtFQUNBLGlCQUFBO0FBQ0o7QUFDSTtFQUNJLGdDQUFBO0FBQ1I7QUFFSTtFQUNJLFdBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxrQkFBQTtBQUFSO0FBRVE7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLG9CQUFBO0VBQ0EsaUJBQUE7QUFBWjtBQUlJO0VBQ0ksV0FBQTtFQUNBLDZCQUFBO0VBQ0EsbUJBQUE7RUFDQSxvQ0FBQTtBQUZSO0FBSVE7RUFDSSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSx1QkFBQTtFQUNBLDJCQUFBO0VBQ0EscUJBQUE7QUFGWjtBQUtRO0VBQ0ksaUJBQUE7RUFDQSxxQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0FBSFo7QUFXSTtFQUNJLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsMkJBQUE7QUFUUjtBQVlJO0VBQ0ksZUFBQTtBQVZSO0FBY1E7RUFDSSxpQkFBQTtBQVpaIiwiZmlsZSI6InNyYy9hcHAvdXNlci9wcm9kdWN0LWRldGFpbC9wcm9kdWN0LWRldGFpbC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIucHJvZHVjdC1kZXRhaWwtYXJlYSB7XG4gICAgcGFkZGluZzogMTBweCAyMHB4IDMwcHggMjBweDtcbiAgICBtaW4taGVpZ2h0OiAxMDB2aDtcblxuICAgIGlvbi1jYXJkIHtcbiAgICAgICAgYm94LXNoYWRvdzogdmFyKC0tYm94LXNoYWRvdy1sZyk7XG4gICAgfVxuXG4gICAgLmltZy1hcmVhIHtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIGhlaWdodDogMzUwcHg7XG4gICAgICAgIGJhY2tncm91bmQ6IHdoaXRlc21va2U7XG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcblxuICAgICAgICBpbWcge1xuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XG4gICAgICAgICAgICAtby1vYmplY3QtZml0OiBjb3ZlcjtcbiAgICAgICAgICAgIG9iamVjdC1maXQ6IGNvdmVyO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgYXJ0aWNsZSB7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2RkZDtcbiAgICAgICAgcGFkZGluZzogMCAwIDE1cHggMDtcbiAgICAgICAgZm9udC1mYW1pbHk6ICdRdWlja3NhbmQnLCBzYW5zLXNlcmlmO1xuXG4gICAgICAgIC50aXRsZSB7XG4gICAgICAgICAgICBmb250LXNpemU6IDI0cHg7XG4gICAgICAgICAgICBmb250LXdlaWdodDogMzAwO1xuICAgICAgICAgICAgbWFyZ2luOiA1cHggM3B4IDNweCAzcHg7XG4gICAgICAgICAgICBjb2xvcjogdmFyKC0tYmctZGVlcC1ibGFjayk7XG4gICAgICAgICAgICBsZXR0ZXItc3BhY2luZzogMC41cHg7XG4gICAgICAgIH1cblxuICAgICAgICAuZGVzY3JpcHRpb24ge1xuICAgICAgICAgICAgd29yZC1zcGFjaW5nOiAxcHg7XG4gICAgICAgICAgICBsZXR0ZXItc3BhY2luZzogMC41cHg7XG4gICAgICAgICAgICBmb250LXdlaWdodDogMTAwO1xuICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgcGFkZGluZzogMTBweCAzcHg7XG4gICAgICAgIH1cblxuICAgICAgICAvLyAuZGVzY3JpcHRpb246OmFmdGVyIHtcbiAgICAgICAgLy8gICAgIGNvbnRlbnQ6IFwi4oaSXCI7XG4gICAgICAgIC8vIH1cbiAgICB9XG5cbiAgICAucHJpY2Uge1xuICAgICAgICBtYXJnaW46IDEwcHggM3B4O1xuICAgICAgICBmb250LXdlaWdodDogMjAwO1xuICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICAgIGNvbG9yOiB2YXIoLS1iZy1kZWVwLWJsYWNrKTtcbiAgICB9XG5cbiAgICBpb24tbGFiZWwge1xuICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgfVxuXG4gICAgaW9uLWNhcmQge1xuICAgICAgICBpb24tY2FyZC1oZWFkZXIge1xuICAgICAgICAgICAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gICAgICAgIH1cbiAgICB9XG59Il19 */";
      /***/
    },

    /***/
    "./src/app/user/product-detail/product-detail.page.ts":
    /*!************************************************************!*\
      !*** ./src/app/user/product-detail/product-detail.page.ts ***!
      \************************************************************/

    /*! exports provided: ProductDetailPage */

    /***/
    function srcAppUserProductDetailProductDetailPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ProductDetailPage", function () {
        return ProductDetailPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! rxjs */
      "./node_modules/rxjs/_esm2015/index.js");
      /* harmony import */


      var _ngrx_store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ngrx/store */
      "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");
      /* harmony import */


      var subsink__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! subsink */
      "./node_modules/subsink/dist/es2015/index.js");
      /* harmony import */


      var swiper_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! swiper/core */
      "./node_modules/swiper/swiper.esm.js");
      /* harmony import */


      var _utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../../../utils/functions/app.functions */
      "./src/utils/functions/app.functions.ts");
      /* harmony import */


      var _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ../../../utils/types/app.constant */
      "./src/utils/types/app.constant.ts");
      /* harmony import */


      var _store_actions_user_action__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ../store/actions/user.action */
      "./src/app/user/store/actions/user.action.ts"); // install Swiper components


      swiper_core__WEBPACK_IMPORTED_MODULE_6__["default"].use([swiper_core__WEBPACK_IMPORTED_MODULE_6__["Navigation"], swiper_core__WEBPACK_IMPORTED_MODULE_6__["Pagination"], swiper_core__WEBPACK_IMPORTED_MODULE_6__["A11y"], swiper_core__WEBPACK_IMPORTED_MODULE_6__["Autoplay"]]);

      var ProductDetailPage = /*#__PURE__*/function () {
        function ProductDetailPage(activatedRoute, router, store) {
          _classCallCheck(this, ProductDetailPage);

          this.activatedRoute = activatedRoute;
          this.router = router;
          this.store = store;
          this.subSink = new subsink__WEBPACK_IMPORTED_MODULE_5__["SubSink"]();
          this.openCartModal = false;
        }

        _createClass(ProductDetailPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this = this;

            unBundleSVG();
            this.subSink.sink = this.activatedRoute.queryParams.subscribe(function (data) {
              if (data === null || data === void 0 ? void 0 : data.product) {
                _this.product$ = Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(JSON.parse(data.product));
              } else {
                _this.router.navigate(["/user"]);
              }
            });
          }
        }, {
          key: "addProductToBookmark",
          value: function addProductToBookmark(productId) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var _yield$Object, userId;

              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_7__["getDataFromLocalStorage"])(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_8__["LocalStorageKey"].ZERO_30_USER);

                    case 2:
                      _yield$Object = _context.sent;
                      userId = _yield$Object.userId;

                      if (userId) {
                        // ? Dispatch action
                        this.store.dispatch(_store_actions_user_action__WEBPACK_IMPORTED_MODULE_9__["actions"].AddProductToBookmarkInitiatedAction({
                          payload: {
                            userId: userId,
                            productId: productId
                          }
                        }));
                      }

                    case 5:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "addProductToCart",
          value: function addProductToCart(_ref) {
            var productImageModels = _ref.productImageModels,
                productModel = _ref.productModel,
                productCostModel = _ref.productCostModel;

            var _a;

            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var _yield$Object2, userId, id, name;

              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.next = 2;
                      return Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_7__["getDataFromLocalStorage"])(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_8__["LocalStorageKey"].ZERO_30_USER);

                    case 2:
                      _yield$Object2 = _context2.sent;
                      userId = _yield$Object2.userId;

                      if (userId) {
                        this.userId = userId;
                        id = productModel.id, name = productModel.name;
                        this.openCartModal = true;
                        this.productAddedToCart = {
                          cost: productCostModel.unitPrice,
                          quantity: 1,
                          productImage: ((_a = productImageModels[0]) === null || _a === void 0 ? void 0 : _a.path) || undefined,
                          productId: id,
                          productName: name
                        };
                        this.productAddedToCartImages = productImageModels;
                      }

                    case 5:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }, {
          key: "closeCartModal",
          value: function closeCartModal() {
            this.openCartModal = false;
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            this.subSink.unsubscribe();
          }
        }]);

        return ProductDetailPage;
      }();

      ProductDetailPage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: _ngrx_store__WEBPACK_IMPORTED_MODULE_4__["Store"]
        }];
      };

      ProductDetailPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-product-detail",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./product-detail.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/user/product-detail/product-detail.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./product-detail.page.scss */
        "./src/app/user/product-detail/product-detail.page.scss"))["default"]]
      })], ProductDetailPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=product-detail-product-detail-module-es5.js.map