(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["add-address-add-address-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/user/profile/add-address/add-address.page.html":
    /*!******************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/user/profile/add-address/add-address.page.html ***!
      \******************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppUserProfileAddAddressAddAddressPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n  <main class=\"bg-offwhite container\">\n\n    <section id=\"header\" class=\"constrain\">\n        <div class=\"navigation\">\n            <a [routerLink]=\"['/profile', 'address']\" class=\"back\">\n                <img class=\"svg\" src=\"assets/images/icons/arrow-left.svg\" height=\"5px\" alt=\"Go back\">\n            </a>\n        </div>\n        <h1 class=\"title\">Add New Address</h1>\n    </section>\n\n    <section id=\"add-new-address-form\" class=\"constrain\">\n        <form action=\"\">\n            <div class=\"form-row\">\n                <div>\n                    <input type=\"text\" placeholder=\"Address name\" name=\"address-name\" required>\n                </div>\n\n                <div>\n                    <div class=\"select\">\n                        <select name=\"location\" id=\"location\" disabled>\n                            <option value=\"home\" selected>Home</option>\n                            <option value=\"work\">Work</option>\n                        </select>\n                        <!-- This is that arrow icon that comes up on a select input -->\n                        <div class=\"chevron-icon\">\n                            <img class=\"svg\" src=\"assets/images/icons/chevron-down.svg\" height=\"7px\" alt=\"Select\">\n                        </div>\n                    </div>\n                </div>\n            </div>\n\n            <div class=\"form-row\">\n                <div>\n                    <input type=\"text\" placeholder=\"Your Location\" name=\"your-location\" required>\n                </div>\n\n                <!-- i think this button helps to locate the user and automatically fills in his address in the input field above this comment. -->\n                <div>\n                    <button class=\"locate btn btn-white\">\n                        <img class=\"svg\" src=\"assets/images/icons/locate.svg\" height=\"14px\" alt=\"Locate\">\n                    </button>\n                </div>\n            </div>\n\n            <div class=\"form-row\">\n                <div>\n                    <input type=\"text\" placeholder=\"Name of the Consignee\" name=\"consignee-name\" required>\n                </div>\n            </div>\n\n            <div class=\"form-row\">\n                <div>\n                    <input type=\"text\" placeholder=\"Phone number\" name=\"phone-number\" required>\n                </div>\n            </div>\n\n            <div class=\"form-row\">\n                <div>\n                    <input type=\"text\" placeholder=\"Delivery note\" name=\"delivery-note\" required>\n                </div>\n            </div>\n\n            <!-- This button submits this form. -->\n            <div class=\"form-row\">\n                <button type=\"submit\" class=\"btn btn-primary\" name=\"complete\">Complete</button>\n            </div>\n        </form>\n    </section>\n    </main>\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/user/profile/add-address/add-address-routing.module.ts":
    /*!************************************************************************!*\
      !*** ./src/app/user/profile/add-address/add-address-routing.module.ts ***!
      \************************************************************************/

    /*! exports provided: AddAddressPageRoutingModule */

    /***/
    function srcAppUserProfileAddAddressAddAddressRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AddAddressPageRoutingModule", function () {
        return AddAddressPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _add_address_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./add-address.page */
      "./src/app/user/profile/add-address/add-address.page.ts");

      var routes = [{
        path: '',
        component: _add_address_page__WEBPACK_IMPORTED_MODULE_3__["AddAddressPage"]
      }];

      var AddAddressPageRoutingModule = function AddAddressPageRoutingModule() {
        _classCallCheck(this, AddAddressPageRoutingModule);
      };

      AddAddressPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], AddAddressPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/user/profile/add-address/add-address.module.ts":
    /*!****************************************************************!*\
      !*** ./src/app/user/profile/add-address/add-address.module.ts ***!
      \****************************************************************/

    /*! exports provided: AddAddressPageModule */

    /***/
    function srcAppUserProfileAddAddressAddAddressModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AddAddressPageModule", function () {
        return AddAddressPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _add_address_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./add-address-routing.module */
      "./src/app/user/profile/add-address/add-address-routing.module.ts");
      /* harmony import */


      var _add_address_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./add-address.page */
      "./src/app/user/profile/add-address/add-address.page.ts");

      var AddAddressPageModule = function AddAddressPageModule() {
        _classCallCheck(this, AddAddressPageModule);
      };

      AddAddressPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _add_address_routing_module__WEBPACK_IMPORTED_MODULE_5__["AddAddressPageRoutingModule"]],
        declarations: [_add_address_page__WEBPACK_IMPORTED_MODULE_6__["AddAddressPage"]]
      })], AddAddressPageModule);
      /***/
    },

    /***/
    "./src/app/user/profile/add-address/add-address.page.scss":
    /*!****************************************************************!*\
      !*** ./src/app/user/profile/add-address/add-address.page.scss ***!
      \****************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppUserProfileAddAddressAddAddressPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "input {\n  height: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXNlci9wcm9maWxlL2FkZC1hZGRyZXNzL2FkZC1hZGRyZXNzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFlBQUE7QUFDSiIsImZpbGUiOiJzcmMvYXBwL3VzZXIvcHJvZmlsZS9hZGQtYWRkcmVzcy9hZGQtYWRkcmVzcy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpbnB1dCB7XG4gICAgaGVpZ2h0OiAxMDAlO1xufSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/user/profile/add-address/add-address.page.ts":
    /*!**************************************************************!*\
      !*** ./src/app/user/profile/add-address/add-address.page.ts ***!
      \**************************************************************/

    /*! exports provided: AddAddressPage */

    /***/
    function srcAppUserProfileAddAddressAddAddressPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AddAddressPage", function () {
        return AddAddressPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var AddAddressPage = /*#__PURE__*/function () {
        function AddAddressPage() {
          _classCallCheck(this, AddAddressPage);
        }

        _createClass(AddAddressPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return AddAddressPage;
      }();

      AddAddressPage.ctorParameters = function () {
        return [];
      };

      AddAddressPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-add-address',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./add-address.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/user/profile/add-address/add-address.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./add-address.page.scss */
        "./src/app/user/profile/add-address/add-address.page.scss"))["default"]]
      })], AddAddressPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=add-address-add-address-module-es5.js.map