(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["edit-profile-edit-profile-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/user/profile/edit-profile/edit-profile.page.html":
    /*!********************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/user/profile/edit-profile/edit-profile.page.html ***!
      \********************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppUserProfileEditProfileEditProfilePageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n\n<main id=\"edit-profile-page\" class=\"container with-bottom-menu bg-offwhite\">\n\t<form (ngSubmit)=\"onSubmit()\" [formGroup]=\"editProfileForm\">\n\t\t<section id=\"header\" class=\"constrain header-white\">\n\t\t\t<div class=\"header-actions\">\n\t\t\t\t<a [routerLink]=\"['/profile']\" class=\"back link\">\n\t\t\t\t\t<img class=\"svg\" src=\"assets/images/icons/arrow-left.svg\" width=\"18px\" alt=\"Go back\">\n\t\t\t\t</a>\n\t\t\t\t<button type=\"submit\" class=\"link save\" [disabled]=\"editProfileForm.invalid\">\n\t\t\t\t\tSave\n\t\t\t\t</button>\n\t\t\t</div>\n\t\t</section>\n\n\t\t<section id=\"display-picture\" class=\"constrain\">\n\t\t\t<div class=\"image\">\n\t\t\t\t<img [src]=\"(userData$ | async)?.person.passportUrl | filePathFormatter\"\n\t\t\t\t\theight=\"90px\" \n\t\t\t\t\t*ngIf=\"(userData$ | async)?.person.passportUrl\"\n\t\t\t\t\t[alt]=\"(userData$ | async)?.username\"\n\t\t\t\t\t#previewImage\n\t\t\t\t\tclass=\"img-preview\" />\n\t\t\t\t<img src=\"assets/images/humans/avatar.png\" \n\t\t\t\t\t\theight=\"90px\" \n\t\t\t\t\t\t*ngIf=\"!(userData$ | async)?.person.passportUrl\"\n\t\t\t\t\t\t#previewImage\n\t\t\t\t\t\t[alt]=\"(userData$ | async)?.username\"\n\t\t\t\t\t\tclass=\"img-preview\" />\n\n\t\t\t\t<!-- When the add-picture button is clicked, a file picker pops up and the image that is selected is supposed to replace the profile image.  -->\n\t\t\t\t<button type=\"button\" class=\"add-picture btn\">\n\t\t\t\t\t<label for=\"change-dp\" class=\"change-dp\">\n\t\t\t\t\t\t<!-- This is the image that should be replace when a user uploads a new one -->\n\t\t\t\t\t\t<img class=\"svg\" \n\t\t\t\t\t\t\tsrc=\"assets/images/icons/add1.svg\" \n\t\t\t\t\t\t\twidth=\"30px\" \n\t\t\t\t\t\t\talt=\"Add Picture\">\n\t\t\t\t\t</label>\n\t\t\t\t</button>\n\t\t\t\t<!-- This is the input that handles the image upload. It's hidden. -->\n\t\t\t\t<input id=\"change-dp\" \n\t\t\t\t\tname=\"change-dp\" \n\t\t\t\t\ttype=\"file\" \n\t\t\t\t\taccept=\"image/*\" \n\t\t\t\t\t(change)=\"onChange($event)\"/>\n\t\t\t</div>\n\t\t</section>\n\t\n\t\t<section id=\"profile-settings\">\n\t\t\t\t<div class=\"settings constrain\">\n\t\t\t\t\t<a class=\"setting\">\n\t\t\t\t\t\t<div class=\"details\">\n\t\t\t\t\t\t\t<p class=\"title\">First Name</p>\n\t\t\t\t\t\t\t<h5 class=\"name form-h5\">\n\t\t\t\t\t\t\t\t<input [formControlName]=\"'FirstName'\" \n\t\t\t\t\t\t\t\t\ttype=\"text\" \n\t\t\t\t\t\t\t\t\tclass=\"borderless-input\" \n\t\t\t\t\t\t\t\t\tplaceholder=\"Joy\"/>\n\t\t\t\t\t\t\t</h5>\n\t\t\t\t\t\t\t<p class=\"margin-xs\" \n                       \t\t\t*ngIf=\"editProfileForm.get('FirstName').invalid && editProfileForm.get('FirstName').touched\">\n\t\t\t\t\t\t\t\t<span class=\"error-message-block\" \n\t\t\t\t\t\t\t\t\t*ngIf=\"editProfileForm.get('FirstName').errors['required']\">\n\t\t\t\t\t\t\t\t\tThis field is required\n\t\t\t\t\t\t\t\t</span>\n                    \t\t</p>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<span class=\"chevron-icon\"><img class=\"svg\" src=\"assets/images/icons/chevron-right.svg\" height=\"20px\"></span>\n\t\t\t\t\t</a>\n\t\t\n\t\t\t\t\t<a class=\"setting\">\n\t\t\t\t\t\t<div class=\"details\">\n\t\t\t\t\t\t\t<p class=\"title\">Last Name</p>\n\t\t\t\t\t\t\t<h5 class=\"name form-h5\">\n\t\t\t\t\t\t\t\t<input type=\"text\" \n\t\t\t\t\t\t\t\t\t[formControlName]=\"'LastName'\" \n\t\t\t\t\t\t\t\t\tclass=\"borderless-input\" \n\t\t\t\t\t\t\t\t\tplaceholder=\"Obianaba\"/>\n\t\t\t\t\t\t\t</h5>\n\t\t\t\t\t\t\t<p class=\"margin-xs\" \n                       \t\t\t*ngIf=\"editProfileForm.get('LastName').invalid && editProfileForm.get('LastName').touched\">\n\t\t\t\t\t\t\t\t<span class=\"error-message-block\" \n\t\t\t\t\t\t\t\t\t*ngIf=\"editProfileForm.get('LastName').errors['required']\">\n\t\t\t\t\t\t\t\t\tThis field is required\n\t\t\t\t\t\t\t\t</span>\n                    \t\t</p>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<span class=\"chevron-icon\"><img class=\"svg\" src=\"assets/images/icons/chevron-right.svg\" height=\"20px\"></span>\n\t\t\t\t\t</a>\n\t\t\n\t\t\t\t\t<a class=\"setting\">\n\t\t\t\t\t\t<div class=\"details\">\n\t\t\t\t\t\t\t<p class=\"title\">Password</p>\n\t\t\t\t\t\t\t<!-- This password is just a placeholder, it shouldn't be dynamic. -->\n\t\t\t\t\t\t\t<h5 class=\"name password form-h5\">\n\t\t\t\t\t\t\t\t<input type=\"password\" \n\t\t\t\t\t\t\t\t\tclass=\"name borderless-input\" \n\t\t\t\t\t\t\t\t\tplaceholder=\"..........\"\n\t\t\t\t\t\t\t\t\t[formControlName]=\"'Password'\"  />\n\t\t\t\t\t\t\t</h5>\n\t\t\t\t\t\t\t<p class=\"margin-xs\" \n                       \t\t\t*ngIf=\"editProfileForm.get('Password').invalid && editProfileForm.get('Password').touched\">\n\t\t\t\t\t\t\t\t<span class=\"error-message-block\" \n\t\t\t\t\t\t\t\t\t*ngIf=\"editProfileForm.get('Password').errors['required']\">\n\t\t\t\t\t\t\t\t\tThis field is required\n\t\t\t\t\t\t\t\t</span>\n                    \t\t</p>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<span class=\"chevron-icon\"><img class=\"svg\" src=\"assets/images/icons/chevron-right.svg\" height=\"20px\"></span>\n\t\t\t\t\t</a>\n\t\t\n\t\t\t\t\t<a class=\"setting\">\n\t\t\t\t\t\t<div class=\"details\">\n\t\t\t\t\t\t\t<p class=\"title\">Phone Number</p>\n\t\t\t\t\t\t\t<h5 class=\"name form-h5\">\n\t\t\t\t\t\t\t\t<input type=\"tel\" \n\t\t\t\t\t\t\t\t\tplaceholder=\"+234 (803) 878 2933\"\n\t\t\t\t\t\t\t\t\t[formControlName]=\"'PhoneNumber'\" \n\t\t\t\t\t\t\t\t\tclass=\"name borderless-input\" />\n\t\t\t\t\t\t\t</h5>\n\t\t\t\t\t\t\t<p class=\"margin-xs\" \n                       \t\t\t*ngIf=\"editProfileForm.get('PhoneNumber').invalid && editProfileForm.get('PhoneNumber').touched\">\n\t\t\t\t\t\t\t\t<span class=\"error-message-block\" \n\t\t\t\t\t\t\t\t\t*ngIf=\"editProfileForm.get('PhoneNumber').errors['required']\">\n\t\t\t\t\t\t\t\t\tThis field is required\n\t\t\t\t\t\t\t\t</span>\n                    \t\t</p>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<span class=\"chevron-icon\"><img class=\"svg\" src=\"assets/images/icons/chevron-right.svg\" height=\"20px\"></span>\n\t\t\t\t\t</a>\n\n\t\t\t\t\t<input type=\"hidden\" \n\t\t\t\t\t\t\t[formControlName]=\"'Address'\" \n\t\t\t\t\t\t\tclass=\"name borderless-input\" />\n\n\t\t\t\t\t<a class=\"setting\">\n\t\t\t\t\t\t<div class=\"details\">\n\t\t\t\t\t\t\t<p class=\"title\">Email</p>\n\t\t\t\t\t\t\t<h5 class=\"name form-h5\">\n\t\t\t\t\t\t\t\t<input [formControlName]=\"'Email'\" \n\t\t\t\t\t\t\t\t\tplaceholder=\"joyobianaba@example.com\" \n\t\t\t\t\t\t\t\t\tclass=\"name borderless-input\" />\n\t\t\t\t\t\t\t</h5>\n\t\t\t\t\t\t\t<p class=\"margin-xs\" \n                       \t\t\t*ngIf=\"editProfileForm.get('Email').invalid && editProfileForm.get('Email').touched\">\n\t\t\t\t\t\t\t\t<span class=\"error-message-block\" \n\t\t\t\t\t\t\t\t\t*ngIf=\"editProfileForm.get('Email').errors['required']\">\n\t\t\t\t\t\t\t\t\tThis field is required\n\t\t\t\t\t\t\t\t</span><br/>\n\t\t\t\t\t\t\t\t<span class=\"error-message-block\" \n\t\t\t\t\t\t\t\t\t*ngIf=\"editProfileForm.get('Email').errors['required']\">\n\t\t\t\t\t\t\t\t\tThis field must be an email address\n\t\t\t\t\t\t\t\t</span>\n                    \t\t</p>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<span class=\"chevron-icon\">\n\t\t\t\t\t\t\t<img class=\"svg\" src=\"assets/images/icons/chevron-right.svg\" height=\"20px\">\n\t\t\t\t\t\t</span>\n\t\t\t\t\t</a>\n\t\t\n\t\t\t\t\t<a class=\"setting\">\n\t\t\t\t\t\t<div class=\"details\">\n\t\t\t\t\t\t\t<p class=\"title\">Date of Birth</p>\n\t\t\t\t\t\t\t<h5 class=\"name form-h5\">\n\t\t\t\t\t\t\t\t<input type=\"text\"\n\t\t\t\t\t\t\t\t\tclass=\"name borderless-input\"\n\t\t\t\t\t\t\t\t\tplaceholder=\"Oct 24, 2000\"\n\t\t\t\t\t\t\t\t\t[formControl]=\"editProfileForm.controls['DOB']\"\n\t\t\t\t\t\t\t\t\t[ngxDatePicker]=\"dateInstanceSingle\" \n\t\t\t\t\t\t\t\t\t(valueChange)=\"onChangeSingle($event)\" />\n\t\t\t\t\t\t\t\t<ngx-date-picker #dateInstanceSingle [options]=\"singleDatePickerOptions\"></ngx-date-picker>\n\t\t\t\t\t\t\t</h5>\n\t\t\t\t\t\t\t<p class=\"margin-xs\" \n                       \t\t\t*ngIf=\"editProfileForm.get('DOB').invalid && editProfileForm.get('DOB').touched\">\n\t\t\t\t\t\t\t\t<span class=\"error-message-block\" \n\t\t\t\t\t\t\t\t\t*ngIf=\"editProfileForm.get('DOB').errors['required']\">\n\t\t\t\t\t\t\t\t\tThis field is required\n\t\t\t\t\t\t\t\t</span>\n                    \t\t</p>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<span class=\"chevron-icon\"><img class=\"svg\" src=\"assets/images/icons/chevron-right.svg\" height=\"20px\"></span>\n\t\t\t\t\t</a>\n\t\t\t\t</div>                                                                                                                                                                                                                                                                                                                                                             \n\t\n\t\t\t<div class=\"footer-expander\"></div>\n\t\t</section>\n\t</form>\n\n\t<app-conditional-bottom-navbar\n\t\t*ngIf=\"userDataFromLocalStorage\"\n\t\t[userData]=\"userDataFromLocalStorage\">\n\t</app-conditional-bottom-navbar>\n  </main>\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/user/profile/edit-profile/edit-profile-routing.module.ts":
    /*!**************************************************************************!*\
      !*** ./src/app/user/profile/edit-profile/edit-profile-routing.module.ts ***!
      \**************************************************************************/

    /*! exports provided: EditProfilePageRoutingModule */

    /***/
    function srcAppUserProfileEditProfileEditProfileRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EditProfilePageRoutingModule", function () {
        return EditProfilePageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _edit_profile_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./edit-profile.page */
      "./src/app/user/profile/edit-profile/edit-profile.page.ts");

      var routes = [{
        path: '',
        component: _edit_profile_page__WEBPACK_IMPORTED_MODULE_3__["EditProfilePage"]
      }];

      var EditProfilePageRoutingModule = function EditProfilePageRoutingModule() {
        _classCallCheck(this, EditProfilePageRoutingModule);
      };

      EditProfilePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], EditProfilePageRoutingModule);
      /***/
    },

    /***/
    "./src/app/user/profile/edit-profile/edit-profile.module.ts":
    /*!******************************************************************!*\
      !*** ./src/app/user/profile/edit-profile/edit-profile.module.ts ***!
      \******************************************************************/

    /*! exports provided: EditProfilePageModule */

    /***/
    function srcAppUserProfileEditProfileEditProfileModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EditProfilePageModule", function () {
        return EditProfilePageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _edit_profile_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./edit-profile-routing.module */
      "./src/app/user/profile/edit-profile/edit-profile-routing.module.ts");
      /* harmony import */


      var _edit_profile_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./edit-profile.page */
      "./src/app/user/profile/edit-profile/edit-profile.page.ts");
      /* harmony import */


      var src_app_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/shared/shared.module */
      "./src/app/shared/shared.module.ts");

      var EditProfilePageModule = function EditProfilePageModule() {
        _classCallCheck(this, EditProfilePageModule);
      };

      EditProfilePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _edit_profile_routing_module__WEBPACK_IMPORTED_MODULE_5__["EditProfilePageRoutingModule"], src_app_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"]],
        declarations: [_edit_profile_page__WEBPACK_IMPORTED_MODULE_6__["EditProfilePage"]]
      })], EditProfilePageModule);
      /***/
    },

    /***/
    "./src/app/user/profile/edit-profile/edit-profile.page.scss":
    /*!******************************************************************!*\
      !*** ./src/app/user/profile/edit-profile/edit-profile.page.scss ***!
      \******************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppUserProfileEditProfileEditProfilePageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".form-h5 .borderless-input {\n  padding: 0;\n  width: 100%;\n  height: 20px;\n  border: none !important;\n  border-radius: 0;\n}\n\nbutton:disabled {\n  background: transparent;\n  color: #ccc !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXNlci9wcm9maWxlL2VkaXQtcHJvZmlsZS9lZGl0LXByb2ZpbGUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNJO0VBQ0ksVUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsdUJBQUE7RUFDQSxnQkFBQTtBQUFSOztBQUlBO0VBQ0ksdUJBQUE7RUFDQSxzQkFBQTtBQURKIiwiZmlsZSI6InNyYy9hcHAvdXNlci9wcm9maWxlL2VkaXQtcHJvZmlsZS9lZGl0LXByb2ZpbGUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmZvcm0taDUge1xuICAgIC5ib3JkZXJsZXNzLWlucHV0IHtcbiAgICAgICAgcGFkZGluZzogMDtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIGhlaWdodDogMjBweDtcbiAgICAgICAgYm9yZGVyOiBub25lICFpbXBvcnRhbnQ7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDA7XG4gICAgfVxufVxuXG5idXR0b246ZGlzYWJsZWQge1xuICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICAgIGNvbG9yOiAjY2NjICFpbXBvcnRhbnQ7XG59Il19 */";
      /***/
    },

    /***/
    "./src/app/user/profile/edit-profile/edit-profile.page.ts":
    /*!****************************************************************!*\
      !*** ./src/app/user/profile/edit-profile/edit-profile.page.ts ***!
      \****************************************************************/

    /*! exports provided: EditProfilePage */

    /***/
    function srcAppUserProfileEditProfileEditProfilePageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EditProfilePage", function () {
        return EditProfilePage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ngrx_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ngrx/store */
      "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! rxjs */
      "./node_modules/rxjs/_esm2015/index.js");
      /* harmony import */


      var subsink__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! subsink */
      "./node_modules/subsink/dist/es2015/index.js");
      /* harmony import */


      var _user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ../../../user/store/actions/user.action */
      "./src/app/user/store/actions/user.action.ts");
      /* harmony import */


      var _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../../../../utils/types/app.constant */
      "./src/utils/types/app.constant.ts");
      /* harmony import */


      var _utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ../../../../utils/functions/app.functions */
      "./src/utils/functions/app.functions.ts");

      var EditProfilePage = /*#__PURE__*/function () {
        function EditProfilePage(store) {
          _classCallCheck(this, EditProfilePage);

          this.store = store;
          this.subSink = new subsink__WEBPACK_IMPORTED_MODULE_5__["SubSink"]();
          this.singleDatePickerOptions = {
            enableMonthSelector: true,
            showMultipleYearsNavigation: true,
            showWeekNumbers: true
          };
        }

        _createClass(EditProfilePage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var _this = this;

              var userDataFromLocalStorage, userId;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      this.initForm(); // ? Bring in the user data

                      _context.next = 3;
                      return Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_8__["getDataFromLocalStorage"])(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_7__["LocalStorageKey"].ZERO_30_USER);

                    case 3:
                      userDataFromLocalStorage = _context.sent;
                      userId = userDataFromLocalStorage.userId;
                      this.userDataFromLocalStorage = userDataFromLocalStorage;
                      this.subSink.sink = this.store.select(function (data) {
                        return data.User.UserData;
                      }).subscribe(function (data) {
                        if (data === null || data === void 0 ? void 0 : data.userId) {
                          _this.userData$ = Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["of"])(data);

                          _this.initForm(data);
                        } else {
                          _this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_6__["actions"].GetUserDataInitiatedAction({
                            payload: {
                              userId: userId
                            }
                          }));
                        }
                      });

                    case 7:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "onChange",
          value: function onChange(event) {
            var _this2 = this;

            // Extract the <file> Object that was added
            var file = event.target.files[0]; // patch the form selector control to the formGroup

            this.editProfileForm.patchValue({
              ProfileImage: file
            }); // make sure that <postImage> is patched into the form behind the scenes and updates its validity

            this.editProfileForm.get("ProfileImage").updateValueAndValidity(); // get the ProfileImageUrl

            var fileReader = new FileReader(); // open the fileReader handler
            // set the imagePreviewUrl while file reading is happening

            fileReader.onload = function () {
              var _a;

              if ((_a = _this2.imagePreview) === null || _a === void 0 ? void 0 : _a.nativeElement) {
                _this2.imagePreview.nativeElement.src = fileReader.result;
              }
            }; // create a dataUrl for the uploaded <postImage>


            fileReader.readAsDataURL(file);
          }
        }, {
          key: "onSubmit",
          value: function onSubmit() {
            if (this.editProfileForm.invalid) {
              return;
            }

            var _this$editProfileForm = this.editProfileForm.value,
                firstName = _this$editProfileForm.FirstName,
                lastName = _this$editProfileForm.LastName,
                dob = _this$editProfileForm.DOB,
                email = _this$editProfileForm.Email,
                password = _this$editProfileForm.Password,
                phoneNo = _this$editProfileForm.PhoneNumber,
                address = _this$editProfileForm.Address,
                ProfileImage = _this$editProfileForm.ProfileImage;
            var payload = {
              userId: this.userId,
              firstName: firstName,
              lastName: lastName,
              email: email,
              phoneNo: phoneNo,
              dob: dob,
              address: address
            };

            if (password) {
              payload = Object.assign(Object.assign({}, payload), {
                password: password
              });
            }

            if (ProfileImage) {
              var file = ProfileImage; // ? Upload File

              this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_6__["actions"].UploadProfileImageInitiatedAction({
                payload: {
                  file: file,
                  userId: this.userId
                }
              }));
            }

            this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_6__["actions"].UpdateUserProfileInitiatedAction({
              payload: payload
            }));
          }
        }, {
          key: "onChangeSingle",
          value: function onChangeSingle(value) {}
        }, {
          key: "initForm",
          value: function initForm(userModel) {
            this.editProfileForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
              ProfileImage: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null),
              FirstName: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](userModel === null || userModel === void 0 ? void 0 : userModel.person.firstName, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])),
              LastName: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](userModel === null || userModel === void 0 ? void 0 : userModel.person.lastName, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])),
              Email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](userModel === null || userModel === void 0 ? void 0 : userModel.username, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email])),
              Password: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null),
              DOB: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](userModel ? new Date(userModel.person.dob) : null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])),
              PhoneNumber: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](userModel === null || userModel === void 0 ? void 0 : userModel.person.phoneNo, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])),
              Address: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](userModel === null || userModel === void 0 ? void 0 : userModel.person.address, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]))
            });
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            this.subSink.unsubscribe();
          }
        }]);

        return EditProfilePage;
      }();

      EditProfilePage.ctorParameters = function () {
        return [{
          type: _ngrx_store__WEBPACK_IMPORTED_MODULE_3__["Store"]
        }];
      };

      EditProfilePage.propDecorators = {
        imagePreview: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: ["previewImage", {
            read: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"]
          }]
        }]
      };
      EditProfilePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-edit-profile",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./edit-profile.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/user/profile/edit-profile/edit-profile.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./edit-profile.page.scss */
        "./src/app/user/profile/edit-profile/edit-profile.page.scss"))["default"]]
      })], EditProfilePage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=edit-profile-edit-profile-module-es5.js.map