(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["view-cart-detail-view-cart-detail-module"], {
    /***/
    "./src/app/rider/view-cart-detail/view-cart-detail-routing.module.ts":
    /*!***************************************************************************!*\
      !*** ./src/app/rider/view-cart-detail/view-cart-detail-routing.module.ts ***!
      \***************************************************************************/

    /*! exports provided: ViewCartDetailPageRoutingModule */

    /***/
    function srcAppRiderViewCartDetailViewCartDetailRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ViewCartDetailPageRoutingModule", function () {
        return ViewCartDetailPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _view_cart_detail_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./view-cart-detail.page */
      "./src/app/rider/view-cart-detail/view-cart-detail.page.ts");

      var routes = [{
        path: '',
        component: _view_cart_detail_page__WEBPACK_IMPORTED_MODULE_3__["ViewCartDetailPage"]
      }];

      var ViewCartDetailPageRoutingModule = function ViewCartDetailPageRoutingModule() {
        _classCallCheck(this, ViewCartDetailPageRoutingModule);
      };

      ViewCartDetailPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], ViewCartDetailPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/rider/view-cart-detail/view-cart-detail.module.ts":
    /*!*******************************************************************!*\
      !*** ./src/app/rider/view-cart-detail/view-cart-detail.module.ts ***!
      \*******************************************************************/

    /*! exports provided: ViewCartDetailPageModule */

    /***/
    function srcAppRiderViewCartDetailViewCartDetailModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ViewCartDetailPageModule", function () {
        return ViewCartDetailPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _view_cart_detail_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./view-cart-detail-routing.module */
      "./src/app/rider/view-cart-detail/view-cart-detail-routing.module.ts");
      /* harmony import */


      var _view_cart_detail_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./view-cart-detail.page */
      "./src/app/rider/view-cart-detail/view-cart-detail.page.ts");
      /* harmony import */


      var _shared_shared_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ../../shared/shared.module */
      "./src/app/shared/shared.module.ts");

      var ViewCartDetailPageModule = function ViewCartDetailPageModule() {
        _classCallCheck(this, ViewCartDetailPageModule);
      };

      ViewCartDetailPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _view_cart_detail_routing_module__WEBPACK_IMPORTED_MODULE_4__["ViewCartDetailPageRoutingModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_6__["SharedModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"]],
        declarations: [_view_cart_detail_page__WEBPACK_IMPORTED_MODULE_5__["ViewCartDetailPage"]]
      })], ViewCartDetailPageModule);
      /***/
    }
  }]);
})();
//# sourceMappingURL=view-cart-detail-view-cart-detail-module-es5.js.map