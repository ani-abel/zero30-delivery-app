(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["user-profile-profile-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/user/profile/profile.page.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/user/profile/profile.page.html ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n  <main id=\"profile-page\" class=\"container with-bottom-menu bg-offwhite\">\n    <section id=\"header\" class=\"constrain header-white\">\n      <div class=\"text\">\n        <h2 class=\"name\">Profile</h2>\n      </div>\n  \n      <a [routerLink]=\"['/profile', 'edit-profile']\" \n        class=\"user-details\" \n        *ngIf=\"(userData$ | async) as userData\">\n        <div class=\"image\">\n          <img [src]=\"userData.person.passportUrl | filePathFormatter\"\n             height=\"70px\" \n             *ngIf=\"userData?.person?.passportUrl; else defaultImage\"\n             [alt]=\"userData.username\" />\n          <ng-template #defaultImage>\n            <img src=\"assets/images/humans/1.png\" height=\"70px\" [alt]=\"userData.username\">\n          </ng-template>\n        </div>\n        \n        <div class=\"details\">\n          <h3 class=\"name\">\n            {{ userData.person.firstName | uppercase }}\n            {{ userData.person.lastName | uppercase }}\n          </h3>\n          <p>{{ userData?.person.phoneNo }}</p>\n        </div>\n        <img class=\"svg chevron-icon\" src=\"assets/images/icons/chevron-right.svg\" height=\"20px\">\n      </a>\n    </section>\n  \n    <app-conditional-bottom-navbar \n      *ngIf=\"userDataFromLocalStorage\"\n      [userData]=\"userDataFromLocalStorage\">\n    </app-conditional-bottom-navbar>\n  \n    <section id=\"settings\">\n      <div class=\"setting-group constrain\">\n        <a [routerLink]=\"['/profile', 'payment-methods']\" class=\"setting\">\n          <span class=\"icon\"><img class=\"svg\" src=\"assets/images/icons/profile/payment.svg\" width=\"16px\"></span>\n          <p class=\"name\">Payment Methods</p>\n          <span class=\"chevron-icon\"><img class=\"svg\" src=\"assets/images/icons/chevron-right.svg\" height=\"20px\"></span>\n        </a>\n  \n        <a [routerLink]=\"['/profile', 'address']\" class=\"setting\">\n          <span class=\"icon\"><img class=\"svg\" src=\"assets/images/icons/profile/address.svg\" width=\"16px\"></span>\n          <p class=\"name\">Address</p>\n          <span class=\"chevron-icon\"><img class=\"svg\" src=\"assets/images/icons/chevron-right.svg\" height=\"20px\"></span>\n        </a>\n  \n        <a class=\"setting\">\n          <span class=\"icon\"><img class=\"svg\" src=\"assets/images/icons/profile/vouchers.svg\" width=\"16px\"></span>\n          <p class=\"name\">My Vouchers</p>\n          <span class=\"chevron-icon\"><img class=\"svg\" src=\"assets/images/icons/chevron-right.svg\" height=\"20px\"></span>\n        </a>\n      </div>\n  \n      <div class=\"setting-group constrain\">\n        <a class=\"setting\">\n          <span class=\"icon\"><img class=\"svg\" src=\"assets/images/icons/profile/notifications.svg\" width=\"16px\"></span>\n          <p class=\"name\">Notifications</p>\n          <span class=\"chevron-icon\"><img class=\"svg\" src=\"assets/images/icons/chevron-right.svg\" height=\"20px\"></span>\n        </a>\n  \n        <a class=\"setting\">\n          <span class=\"icon\"><img class=\"svg\" src=\"assets/images/icons/profile/language.svg\" width=\"16px\"></span>\n          <p class=\"name\">Language</p>\n          <span class=\"chevron-icon\"><img class=\"svg\" src=\"assets/images/icons/chevron-right.svg\" height=\"20px\"></span>\n        </a>\n  \n        <a class=\"setting\">\n          <span class=\"icon\"><img class=\"svg\" src=\"assets/images/icons/profile/settings.svg\" width=\"16px\"></span>\n          <p class=\"name\">Settings</p>\n          <span class=\"chevron-icon\"><img class=\"svg\" src=\"assets/images/icons/chevron-right.svg\" height=\"20px\"></span>\n        </a>\n  \n        <a class=\"setting\">\n          <span class=\"icon\"><img class=\"svg\" src=\"assets/images/icons/profile/invite.svg\" width=\"16px\"></span>\n          <p class=\"name\">Invite Friends</p>\n          <span class=\"chevron-icon\"><img class=\"svg\" src=\"assets/images/icons/chevron-right.svg\" height=\"20px\"></span>\n        </a>\n  \n        <a class=\"setting\">\n          <span class=\"icon\"><img class=\"svg\" src=\"assets/images/icons/profile/support.svg\" width=\"16px\"></span>\n          <p class=\"name\">Support</p>\n          <span class=\"chevron-icon\"><img class=\"svg\" src=\"assets/images/icons/chevron-right.svg\" height=\"20px\"></span>\n        </a>\n      </div>\n  \n      <div class=\"setting-group constrain\">\n        <a (click)=\"logout()\" class=\"setting cursor-pointer\">\n          <span class=\"icon\"><img class=\"svg\" src=\"assets/images/icons/profile/logout.svg\" width=\"16px\"></span>\n          <p class=\"name\">Logout</p>\n          <span class=\"chevron-icon\"><img class=\"svg\" src=\"assets/images/icons/chevron-right.svg\" height=\"20px\"></span>\n        </a>\n      </div>\n      <div class=\"footer-expander\"></div>\n    </section>\n  </main>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/user/profile/profile-routing.module.ts":
/*!********************************************************!*\
  !*** ./src/app/user/profile/profile-routing.module.ts ***!
  \********************************************************/
/*! exports provided: ProfilePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfilePageRoutingModule", function() { return ProfilePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _profile_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./profile.page */ "./src/app/user/profile/profile.page.ts");




const routes = [
    {
        path: '',
        component: _profile_page__WEBPACK_IMPORTED_MODULE_3__["ProfilePage"]
    },
    {
        path: 'payment-method',
        loadChildren: () => __webpack_require__.e(/*! import() | payment-method-payment-method-module */ "payment-method-payment-method-module").then(__webpack_require__.bind(null, /*! ./payment-method/payment-method.module */ "./src/app/user/profile/payment-method/payment-method.module.ts")).then(m => m.PaymentMethodPageModule)
    },
    {
        path: 'edit-profile',
        loadChildren: () => __webpack_require__.e(/*! import() | edit-profile-edit-profile-module */ "edit-profile-edit-profile-module").then(__webpack_require__.bind(null, /*! ./edit-profile/edit-profile.module */ "./src/app/user/profile/edit-profile/edit-profile.module.ts")).then(m => m.EditProfilePageModule)
    },
    {
        path: 'address',
        loadChildren: () => __webpack_require__.e(/*! import() | address-address-module */ "address-address-module").then(__webpack_require__.bind(null, /*! ./address/address.module */ "./src/app/user/profile/address/address.module.ts")).then(m => m.AddressPageModule)
    },
    {
        path: 'add-address',
        loadChildren: () => __webpack_require__.e(/*! import() | add-address-add-address-module */ "add-address-add-address-module").then(__webpack_require__.bind(null, /*! ./add-address/add-address.module */ "./src/app/user/profile/add-address/add-address.module.ts")).then(m => m.AddAddressPageModule)
    }
];
let ProfilePageRoutingModule = class ProfilePageRoutingModule {
};
ProfilePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ProfilePageRoutingModule);



/***/ }),

/***/ "./src/app/user/profile/profile.module.ts":
/*!************************************************!*\
  !*** ./src/app/user/profile/profile.module.ts ***!
  \************************************************/
/*! exports provided: ProfilePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfilePageModule", function() { return ProfilePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _profile_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./profile-routing.module */ "./src/app/user/profile/profile-routing.module.ts");
/* harmony import */ var _profile_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./profile.page */ "./src/app/user/profile/profile.page.ts");
/* harmony import */ var src_app_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/shared/shared.module */ "./src/app/shared/shared.module.ts");








let ProfilePageModule = class ProfilePageModule {
};
ProfilePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _profile_routing_module__WEBPACK_IMPORTED_MODULE_5__["ProfilePageRoutingModule"],
            src_app_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"]
        ],
        declarations: [_profile_page__WEBPACK_IMPORTED_MODULE_6__["ProfilePage"]]
    })
], ProfilePageModule);



/***/ }),

/***/ "./src/app/user/profile/profile.page.scss":
/*!************************************************!*\
  !*** ./src/app/user/profile/profile.page.scss ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXIvcHJvZmlsZS9wcm9maWxlLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/user/profile/profile.page.ts":
/*!**********************************************!*\
  !*** ./src/app/user/profile/profile.page.ts ***!
  \**********************************************/
/*! exports provided: ProfilePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfilePage", function() { return ProfilePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");
/* harmony import */ var subsink__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! subsink */ "./node_modules/subsink/dist/es2015/index.js");
/* harmony import */ var _auth_store_actions_auth_action__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../auth/store/actions/auth.action */ "./src/app/auth/store/actions/auth.action.ts");
/* harmony import */ var _user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../user/store/actions/user.action */ "./src/app/user/store/actions/user.action.ts");
/* harmony import */ var _store_actions_app_action__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../store/actions/app.action */ "./src/app/store/actions/app.action.ts");
/* harmony import */ var _utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../utils/functions/app.functions */ "./src/utils/functions/app.functions.ts");
/* harmony import */ var _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../utils/types/app.constant */ "./src/utils/types/app.constant.ts");









let ProfilePage = class ProfilePage {
    constructor(store) {
        this.store = store;
        this.subSink = new subsink__WEBPACK_IMPORTED_MODULE_3__["SubSink"]();
    }
    ngOnInit() { }
    ionViewWillEnter() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            // ?  get the userId from localStorage
            const userDataFromLocalStorage = yield Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_7__["getDataFromLocalStorage"])(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_8__["LocalStorageKey"].ZERO_30_USER);
            this.userDataFromLocalStorage = userDataFromLocalStorage;
            if (userDataFromLocalStorage === null || userDataFromLocalStorage === void 0 ? void 0 : userDataFromLocalStorage.userId) {
                const { userId } = userDataFromLocalStorage;
                if (userId) {
                    this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetUserDataInitiatedAction({ payload: { userId } }));
                    this.userData$ = this.store.select((data) => data.User.UserData);
                }
            }
        });
    }
    logout() {
        try {
            this.store.dispatch(_store_actions_app_action__WEBPACK_IMPORTED_MODULE_6__["actions"].ClearAppStateAction());
            this.store.dispatch(_auth_store_actions_auth_action__WEBPACK_IMPORTED_MODULE_4__["actions"].LogoutInitiatedAction());
            // this.store.dispatch(UserActions.ClearUserStateAction());
            // this.store.dispatch(RiderActions.ClearRiderStateAction());
        }
        catch (ex) {
            throw ex;
        }
    }
    ngOnDestroy() {
        this.subSink.unsubscribe();
    }
};
ProfilePage.ctorParameters = () => [
    { type: _ngrx_store__WEBPACK_IMPORTED_MODULE_2__["Store"] }
];
ProfilePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-profile",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./profile.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/user/profile/profile.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./profile.page.scss */ "./src/app/user/profile/profile.page.scss")).default]
    })
], ProfilePage);



/***/ })

}]);
//# sourceMappingURL=user-profile-profile-module-es2015.js.map