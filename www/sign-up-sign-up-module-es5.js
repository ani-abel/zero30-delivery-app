(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["sign-up-sign-up-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/auth/sign-up/sign-up.page.html":
    /*!**************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/auth/sign-up/sign-up.page.html ***!
      \**************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppAuthSignUpSignUpPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n  <main id=\"create-new-account-page\" class=\"bg-offwhite container\">\n    <section id=\"header\" class=\"constrain\">\n        <div class=\"navigation\">\n            <a [routerLink]=\"['/auth', 'login']\" class=\"back\">\n                <img class=\"svg\" src=\"assets/images/icons/arrow-left.svg\" height=\"5px\" alt=\"Go back\">\n            </a>\n        </div>\n    </section>\n\n    <section id=\"heading-text\" class=\"constrain\">\n        <h1 class=\"title\">Create New Account</h1>\n    </section>\n\n    <section id=\"create-new-account-form\" class=\"constrain\">\n        <form [formGroup]=\"signUpForm\" (ngSubmit)=\"onSubmit()\">\n            <div class=\"form-row\">\n                <div>\n                    <input type=\"text\" \n                        placeholder=\"First name\" \n                        [formControlName]=\"'FirstName'\" />\n                        <p class=\"margin-xs\" \n                            *ngIf=\"signUpForm.get('FirstName').invalid && signUpForm.get('FirstName').touched\">\n                            <span class=\"error-message-block\" \n                                *ngIf=\"signUpForm.get('FirstName').errors['required']\">\n                                This field is required\n                            </span>\n                        </p>\n                </div>\n            </div>\n\n            <div class=\"form-row\">\n                <div>\n                    <input type=\"text\" \n                        placeholder=\"Last name\" \n                        [formControlName]=\"'LastName'\" />\n                        <p class=\"margin-xs\" \n                            *ngIf=\"signUpForm.get('LastName').invalid && signUpForm.get('LastName').touched\">\n                            <span class=\"error-message-block\" \n                                *ngIf=\"signUpForm.get('LastName').errors['required']\">\n                                This field is required\n                            </span>\n                        </p>\n                </div>\n            </div>\n\n            <div class=\"form-row\">\n                <div>\n                    <input type=\"email\" \n                        placeholder=\"Email\" \n                        name=\"email\" \n                        [formControlName]=\"'Email'\" />\n                    <p class=\"margin-xs\" \n                            *ngIf=\"signUpForm.get('Email').invalid && signUpForm.get('Email').touched\">\n                        <span class=\"error-message-block\" \n                            *ngIf=\"signUpForm.get('Email').errors['required']\">\n                            This field is required\n                        </span>\n                        <span class=\"error-message-block\" \n                            *ngIf=\"signUpForm.get('Email').errors['email']\">\n                            Must be a valid email\n                        </span>\n                    </p>\n                </div>\n            </div>\n\n            <div class=\"form-row\">\n                <div>\n                    <input type=\"password\" \n                        placeholder=\"Password\" \n                        [formControlName]=\"'Password'\" />\n                    <p class=\"margin-xs\" \n                        *ngIf=\"signUpForm.get('Password').invalid && signUpForm.get('Password').touched\">\n                        <span class=\"error-message-block\" \n                            *ngIf=\"signUpForm.get('Password').errors['required']\">\n                            This field is required\n                        </span>\n                    </p>\n                        \n                </div>\n            </div>\n\n            <div class=\"form-row\">\n                <div>\n                    <input type=\"tel\" \n                        placeholder=\"Phone Number\" \n                        [formControlName]=\"'PhoneNumber'\" />\n                    <p class=\"margin-xs\" \n                        *ngIf=\"signUpForm.get('PhoneNumber').invalid && signUpForm.get('PhoneNumber').touched\">\n                        <span class=\"error-message-block\" \n                            *ngIf=\"signUpForm.get('PhoneNumber').errors['required']\">\n                            This field is required\n                        </span>\n                    </p>\n                </div>\n            </div>\n\n            <div class=\"form-row\">\n                <div>\n                    <textarea \n                        class=\"resize-none\"\n                        [formControlName]=\"'Location'\" \n                        placeholder=\"Your Location\"></textarea>\n                    <p class=\"margin-xs\" \n                        *ngIf=\"signUpForm.get('Location').invalid && signUpForm.get('Location').touched\">\n                        <span class=\"error-message-block\" \n                            *ngIf=\"signUpForm.get('Location').errors['required']\">\n                            This field is required\n                        </span>\n                    </p>\n                </div>\n            </div>\n\n            <div class=\"form-row\">\n                <div>\n                    <input type=\"text\"\n                        placeholder=\"Date of Birth\" \n                        [ngxDatePicker]=\"dateInstanceSingle\" \n                        (valueChange)=\"onChangeSingle($event)\"\n                        [formControlName]=\"'DOB'\" />\n                        <ngx-date-picker #dateInstanceSingle [options]=\"singleDatePickerOptions\"></ngx-date-picker>\n                    <p class=\"margin-xs\" \n                        *ngIf=\"signUpForm.get('DOB').invalid && signUpForm.get('DOB').touched\">\n                        <span class=\"error-message-block\" \n                            *ngIf=\"signUpForm.get('DOB').errors['required']\">\n                            This field is required\n                        </span>\n                    </p>\n                </div>\n            </div>\n\n            <div></div>\n\n            <div class=\"form-row\">\n                <button type=\"submit\" \n                    class=\"btn btn-primary\" \n                    [disabled]=\"signUpForm.invalid\">\n                    Continue\n                </button>\n            </div>\n        </form>\n    </section>\n  </main>\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/auth/sign-up/sign-up-routing.module.ts":
    /*!********************************************************!*\
      !*** ./src/app/auth/sign-up/sign-up-routing.module.ts ***!
      \********************************************************/

    /*! exports provided: SignUpPageRoutingModule */

    /***/
    function srcAppAuthSignUpSignUpRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SignUpPageRoutingModule", function () {
        return SignUpPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _sign_up_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./sign-up.page */
      "./src/app/auth/sign-up/sign-up.page.ts");

      var routes = [{
        path: '',
        component: _sign_up_page__WEBPACK_IMPORTED_MODULE_3__["SignUpPage"]
      }];

      var SignUpPageRoutingModule = function SignUpPageRoutingModule() {
        _classCallCheck(this, SignUpPageRoutingModule);
      };

      SignUpPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], SignUpPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/auth/sign-up/sign-up.module.ts":
    /*!************************************************!*\
      !*** ./src/app/auth/sign-up/sign-up.module.ts ***!
      \************************************************/

    /*! exports provided: SignUpPageModule */

    /***/
    function srcAppAuthSignUpSignUpModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SignUpPageModule", function () {
        return SignUpPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _sign_up_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./sign-up-routing.module */
      "./src/app/auth/sign-up/sign-up-routing.module.ts");
      /* harmony import */


      var _sign_up_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./sign-up.page */
      "./src/app/auth/sign-up/sign-up.page.ts");
      /* harmony import */


      var _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../../shared/shared.module */
      "./src/app/shared/shared.module.ts");

      var SignUpPageModule = function SignUpPageModule() {
        _classCallCheck(this, SignUpPageModule);
      };

      SignUpPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"], _sign_up_routing_module__WEBPACK_IMPORTED_MODULE_5__["SignUpPageRoutingModule"]],
        declarations: [_sign_up_page__WEBPACK_IMPORTED_MODULE_6__["SignUpPage"]]
      })], SignUpPageModule);
      /***/
    },

    /***/
    "./src/app/auth/sign-up/sign-up.page.scss":
    /*!************************************************!*\
      !*** ./src/app/auth/sign-up/sign-up.page.scss ***!
      \************************************************/

    /*! exports provided: default */

    /***/
    function srcAppAuthSignUpSignUpPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2F1dGgvc2lnbi11cC9zaWduLXVwLnBhZ2Uuc2NzcyJ9 */";
      /***/
    },

    /***/
    "./src/app/auth/sign-up/sign-up.page.ts":
    /*!**********************************************!*\
      !*** ./src/app/auth/sign-up/sign-up.page.ts ***!
      \**********************************************/

    /*! exports provided: SignUpPage */

    /***/
    function srcAppAuthSignUpSignUpPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SignUpPage", function () {
        return SignUpPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ngrx_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ngrx/store */
      "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var subsink__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! subsink */
      "./node_modules/subsink/dist/es2015/index.js");
      /* harmony import */


      var _store_actions_auth_action__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ../store/actions/auth.action */
      "./src/app/auth/store/actions/auth.action.ts");

      var SignUpPage = /*#__PURE__*/function () {
        function SignUpPage(store, router) {
          _classCallCheck(this, SignUpPage);

          this.store = store;
          this.router = router;
          this.subSink = new subsink__WEBPACK_IMPORTED_MODULE_5__["SubSink"]();
          this.singleDatePickerOptions = {
            enableMonthSelector: true,
            showMultipleYearsNavigation: true,
            showWeekNumbers: true
          };
        }

        _createClass(SignUpPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.initForm();
          }
        }, {
          key: "initForm",
          value: function initForm() {
            this.signUpForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
              FirstName: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])),
              LastName: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])),
              Email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email])),
              Password: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])),
              PhoneNumber: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])),
              Location: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])),
              DOB: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]))
            });
          }
        }, {
          key: "onChangeSingle",
          value: function onChangeSingle(value) {
            console.log(value);
          }
        }, {
          key: "onSubmit",
          value: function onSubmit() {
            var _this = this;

            try {
              if (this.signUpForm.invalid) {
                return;
              }

              var _this$signUpForm$valu = this.signUpForm.value,
                  FirstName = _this$signUpForm$valu.FirstName,
                  LastName = _this$signUpForm$valu.LastName,
                  Email = _this$signUpForm$valu.Email,
                  DOB = _this$signUpForm$valu.DOB,
                  Password = _this$signUpForm$valu.Password,
                  PhoneNumber = _this$signUpForm$valu.PhoneNumber,
                  Location = _this$signUpForm$valu.Location;
              var payload = {
                isCustomer: true,
                password: Password,
                personModel: {
                  address: Location,
                  dob: DOB,
                  firstName: FirstName,
                  lastName: LastName,
                  phoneNo: PhoneNumber
                },
                userModel: {
                  username: Email
                }
              };
              this.store.dispatch(_store_actions_auth_action__WEBPACK_IMPORTED_MODULE_6__["actions"].SignUpInitiatedAction({
                payload: payload
              }));
              this.subSink.sink = this.store.select(function (data) {
                return data.Auth.ActiveMessage;
              }).subscribe(function (data) {
                if (data) {
                  _this.signUpForm.reset();

                  _this.router.navigate(["/auth", "sign-up-otp-validation", PhoneNumber]);
                }
              });
            } catch (ex) {
              throw ex;
            }
          }
        }]);

        return SignUpPage;
      }();

      SignUpPage.ctorParameters = function () {
        return [{
          type: _ngrx_store__WEBPACK_IMPORTED_MODULE_3__["Store"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
        }];
      };

      SignUpPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-sign-up',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./sign-up.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/auth/sign-up/sign-up.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./sign-up.page.scss */
        "./src/app/auth/sign-up/sign-up.page.scss"))["default"]]
      })], SignUpPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=sign-up-sign-up-module-es5.js.map