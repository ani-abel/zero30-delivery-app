(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<main class=\"bg-primary container\">\n  <!-- Top section of the Onboarding page (the white part) -->\n  \n  <swiper [slidesPerView]=\"1\" \n    id=\"onboardings\"\n    [spaceBetween]=\"30\" \n    [loop]=\"true\"\n    [autoplay]=\"{ delay: 2500, disableOnInteraction: false }\">\n    <ng-template swiperSlide>\n       <div class=\"onboarding\">\n            <div class=\"image constrain\">\n                <img src=\"assets/images/onboarding/1.svg\" width=\"200px\" alt=\"Onboarding Image\">\n            </div>\n\n            <div class=\"dots constrain\">\n                <span class=\"dot active\"></span>\n                <span class=\"dot\"></span>\n                <span class=\"dot\"></span>\n            </div>\n\n            <div class=\"text constrain\">\n                <h1>Search for favorite stores near you</h1>\n                <p>Discover items from over 3250 stores.</p>\n            </div>\n       </div>\n    </ng-template>\n    <ng-template swiperSlide>\n        <div class=\"onboarding\">\n            <div class=\"image constrain\">\n                <img src=\"assets/images/onboarding/2.svg\" width=\"200px\" alt=\"Onboarding Image\">\n            </div>\n    \n            <div class=\"dots constrain\">\n                <span class=\"dot\"></span>\n                <span class=\"dot active\"></span>\n                <span class=\"dot\"></span>\n            </div>\n    \n            <div class=\"text constrain\">\n                <h1>Fast delivery to your doorstep</h1>\n                <p>Fast delivery to your home, office and wherever you are.</p>\n            </div>\n        </div>\n    </ng-template>\n    <ng-template swiperSlide>\n        <div class=\"onboarding\">\n            <div class=\"image constrain\">\n                <img src=\"assets/images/onboarding/3.svg\" width=\"200px\" alt=\"Onboarding Image\">\n            </div>\n    \n            <div class=\"dots constrain\">\n                <span class=\"dot\"></span>\n                <span class=\"dot\"></span>\n                <span class=\"dot active\"></span>\n            </div>\n            \n            <div class=\"text constrain\">\n                <h1>Follow your order as it moves!</h1>\n                <p>Always know were r 3250 merchants</p>\n            </div>\n        </div>\n    </ng-template>\n  </swiper>\n\n  <!-- Bottom section of the Onboarding page (the orange part) -->\n  <section id=\"onboarding-cta\" class=\"constrain\">\n      <div class=\"row\">\n          <a [routerLink]=\"['/auth', 'sign-up']\" class=\"btn btn-outline-white\">Sign Up</a>\n          <a [routerLink]=\"['/auth', 'login']\" class=\"btn btn-white text-primary\">Sign In</a>\n      </div>\n  </section>\n  <!-- Bottom section of the Onboarding page (the orange part) -->\n</main>\n\n<!-- <ion-content></ion-content> -->");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/rider/home/home.page.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/rider/home/home.page.html ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n  <main id=\"home-page\" class=\"container with-bottom-menu bg-offwhite\">\n\n    <section id=\"header\" class=\"constrain header-white\" *ngIf=\"(userData$ | async) as userData\">\n      <h2>Hello {{ userData.person?.firstName?.trim() | capitalize }},</h2>\n      <p class=\"rider-reviews\" *ngIf=\"(riderRating$ | async) as riderRating\">\n        <!-- <img class=\"svg icon\" alt=\"Icon\" src=\"assets/images/icons/star.svg\"> -->\n        <i *ngFor=\"let star of [0, 1, 2, 3, 4]; index as i\" \n          class=\"fa fa-star\" \n          [ngClass]=\"{ 'fa-checked': i < riderRating?.rate }\"></i>\n          {{ riderRating?.rate || 0 | number: '1.0-2' }}\n        <span *ngIf=\"riderRating?.reviewerCount > 1; else defaultReviewCount\">\n          ({{ riderRating?.reviewerCount || 0 }} Reviews)\n        </span>\n        <ng-template #defaultReviewCount>\n          <span>\n            ({{ riderRating?.reviewerCount || 0 }} Review)\n          </span>\n        </ng-template>\n      </p>\n    </section>\n  \n    <section id=\"cards\" class=\"constrain\">\n      <a [routerLink]=\"['/rider', 'orders']\"\n        [queryParams]=\"{ type: productCartStatus.TRANSIT }\"  \n        class=\"card ongoing-orders\">\n        <img class=\"svg\" src=\"assets/images/icons/riders/ongoing-orders.svg\" height=\"80px\">\n        <p>Ongoing Orders</p>\n      </a>\n      <a [routerLink]=\"['/rider', 'orders']\" \n        [queryParams]=\"{ type: productCartStatus.COMPLETED }\"\n        class=\"card order-history\">\n        <img class=\"svg\" src=\"assets/images/icons/riders/order-history.svg\" height=\"80px\">\n        <p>Order History</p>\n      </a>\n    </section>\n  \n    <section id=\"delivery-address\" class=\"constrain\">\n      <app-no-content-found \n        *ngIf=\"!((riderDeliveryHistory$ | async)?.length > 0); else defaultTemplate\">\n      </app-no-content-found>\n\n      <ng-template #defaultTemplate>\n        <div class=\"section-title with-link\">\n          <h4>Recent Activities</h4>\n          <a [routerLink]=\"['/rider', 'orders']\" class=\"link link-primary\">\n            <span>View all</span>\n            <img class=\"svg\" src=\"assets/images/icons/double-chevron-right.svg\" height=\"7px\" alt=\"Arrow Right\">\n          </a>\n        </div>\n    \n        <div class=\"addresses constrain\" *ngIf=\"(riderDeliveryHistory$ | async) as riderDeliveryHistory\">\n          <a class=\"address cursor-pointer\" \n            [routerLink]=\"['/rider', fillInUrl(delivery.cart.status), delivery.cart?.id]\"\n            *ngFor=\"let delivery of riderDeliveryHistory  | sliceContent : 10\">\n            <div class=\"details\">\n              <!-- <h5>Home</h5> -->\n              <h5 class=\"location-address\">\n                <img class=\"svg\" src=\"assets/images/icons/map-pointer.svg\" height=\"9px\" alt=\"Store\">\n                <span>{{ delivery?.cart?.deliveryAddress | capitalize }}</span>\n              </h5>\n              <p class=\"name\">\n                <img class=\"svg\" src=\"assets/images/icons/person.svg\" height=\"9px\" alt=\"Store\">\n                <span>\n                  {{ delivery.person.firstName | uppercase }}\n                  {{ delivery.person.lastName | uppercase }}\n                </span>\n              </p>\n              <div class=\"bottom\">\n                <p class=\"phone\">\n                  <img class=\"svg\" src=\"assets/images/icons/telephone.svg\" height=\"9px\" alt=\"Store\">\n                  <span>{{ delivery.person.phoneNo }}</span>\n                </p>\n                <p class=\"status completed\" \n                  *ngIf=\"delivery?.cart?.status === productCartStatus.COMPLETED\">\n                  {{ delivery.cart.status | capitalize }}\n                </p>\n                <p class=\"status cancelled\" \n                  *ngIf=\"delivery?.cart?.status === productCartStatus.CANCELLED\">\n                  {{ delivery.cart.status | capitalize }}\n                </p>\n                <p class=\"status transit\" \n                  *ngIf=\"delivery?.cart?.status === productCartStatus.TRANSIT\">\n                  {{ delivery.cart.status | capitalize }}\n                </p>\n                <p class=\"status verified\" \n                  *ngIf=\"delivery?.cart?.status === productCartStatus.VERIFIED\">\n                  {{ delivery.cart.status | capitalize }}\n                </p>\n              </div>\n            </div>\n            <span class=\"chevron-icon\">\n              <img class=\"svg\" src=\"assets/images/icons/chevron-right.svg\" height=\"20px\" />\n            </span>\n            </a>\n                \n        </div>\n      </ng-template>\n    </section>\n    <app-footer-expander></app-footer-expander>\n\n    <app-rider-bottom-navbar></app-rider-bottom-navbar>\n  </main>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/home/home-routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/home/home-routing.module.ts ***!
  \*********************************************/
/*! exports provided: HomePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageRoutingModule", function() { return HomePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home.page */ "./src/app/home/home.page.ts");




const routes = [
    {
        path: '',
        component: _home_page__WEBPACK_IMPORTED_MODULE_3__["HomePage"],
    }
];
let HomePageRoutingModule = class HomePageRoutingModule {
};
HomePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], HomePageRoutingModule);



/***/ }),

/***/ "./src/app/home/home.module.ts":
/*!*************************************!*\
  !*** ./src/app/home/home.module.ts ***!
  \*************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home.page */ "./src/app/home/home.page.ts");
/* harmony import */ var _home_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home-routing.module */ "./src/app/home/home-routing.module.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../shared/shared.module */ "./src/app/shared/shared.module.ts");








let HomePageModule = class HomePageModule {
};
HomePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"],
            _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"],
            _home_routing_module__WEBPACK_IMPORTED_MODULE_6__["HomePageRoutingModule"],
            _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"]
        ],
        declarations: [_home_page__WEBPACK_IMPORTED_MODULE_5__["HomePage"]]
    })
], HomePageModule);



/***/ }),

/***/ "./src/app/home/home.page.scss":
/*!*************************************!*\
  !*** ./src/app/home/home.page.scss ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("#container {\n  text-align: center;\n  position: absolute;\n  left: 0;\n  right: 0;\n  top: 50%;\n  transform: translateY(-50%);\n}\n\n#container strong {\n  font-size: 20px;\n  line-height: 26px;\n}\n\n#container p {\n  font-size: 16px;\n  line-height: 22px;\n  color: #8c8c8c;\n  margin: 0;\n}\n\n#container a {\n  text-decoration: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS9ob21lLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGtCQUFBO0VBRUEsa0JBQUE7RUFDQSxPQUFBO0VBQ0EsUUFBQTtFQUNBLFFBQUE7RUFDQSwyQkFBQTtBQUFGOztBQUdBO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0FBQUY7O0FBR0E7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7RUFFQSxjQUFBO0VBRUEsU0FBQTtBQUZGOztBQUtBO0VBQ0UscUJBQUE7QUFGRiIsImZpbGUiOiJzcmMvYXBwL2hvbWUvaG9tZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjY29udGFpbmVyIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogMDtcbiAgcmlnaHQ6IDA7XG4gIHRvcDogNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTUwJSk7XG59XG5cbiNjb250YWluZXIgc3Ryb25nIHtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBsaW5lLWhlaWdodDogMjZweDtcbn1cblxuI2NvbnRhaW5lciBwIHtcbiAgZm9udC1zaXplOiAxNnB4O1xuICBsaW5lLWhlaWdodDogMjJweDtcblxuICBjb2xvcjogIzhjOGM4YztcblxuICBtYXJnaW46IDA7XG59XG5cbiNjb250YWluZXIgYSB7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/home/home.page.ts":
/*!***********************************!*\
  !*** ./src/app/home/home.page.ts ***!
  \***********************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _auth_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../auth/auth.service */ "./src/app/auth/auth.service.ts");
/* harmony import */ var swiper_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! swiper/core */ "./node_modules/swiper/swiper.esm.js");




// install Swiper components
swiper_core__WEBPACK_IMPORTED_MODULE_3__["default"].use([swiper_core__WEBPACK_IMPORTED_MODULE_3__["Navigation"], swiper_core__WEBPACK_IMPORTED_MODULE_3__["Pagination"], swiper_core__WEBPACK_IMPORTED_MODULE_3__["A11y"], swiper_core__WEBPACK_IMPORTED_MODULE_3__["Autoplay"]]);
let HomePage = class HomePage {
    constructor(authSrv) {
        this.authSrv = authSrv;
    }
    ionViewWillEnter() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield this.authSrv.loginAsync();
        });
    }
    ngOnInit() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield this.authSrv.loginAsync();
        });
    }
    onSwiper(swiper) {
        console.log(swiper);
    }
    onSlideChange() {
        console.log("slide change");
    }
};
HomePage.ctorParameters = () => [
    { type: _auth_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"] }
];
HomePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-home",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./home.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./home.page.scss */ "./src/app/home/home.page.scss")).default]
    })
], HomePage);



/***/ }),

/***/ "./src/app/rider/home/home-routing.module.ts":
/*!***************************************************!*\
  !*** ./src/app/rider/home/home-routing.module.ts ***!
  \***************************************************/
/*! exports provided: HomePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageRoutingModule", function() { return HomePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home.page */ "./src/app/rider/home/home.page.ts");




const routes = [
    {
        path: '',
        component: _home_page__WEBPACK_IMPORTED_MODULE_3__["HomePage"]
    }
];
let HomePageRoutingModule = class HomePageRoutingModule {
};
HomePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], HomePageRoutingModule);



/***/ }),

/***/ "./src/app/rider/home/home.module.ts":
/*!*******************************************!*\
  !*** ./src/app/rider/home/home.module.ts ***!
  \*******************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _home_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home-routing.module */ "./src/app/rider/home/home-routing.module.ts");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home.page */ "./src/app/rider/home/home.page.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../shared/shared.module */ "./src/app/shared/shared.module.ts");








let HomePageModule = class HomePageModule {
};
HomePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"],
            _home_routing_module__WEBPACK_IMPORTED_MODULE_5__["HomePageRoutingModule"]
        ],
        declarations: [_home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]]
    })
], HomePageModule);



/***/ }),

/***/ "./src/app/rider/home/home.page.scss":
/*!*******************************************!*\
  !*** ./src/app/rider/home/home.page.scss ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".rider-reviews {\n  font-size: 13px;\n}\n\n.transit {\n  color: purple;\n}\n\n.verified {\n  color: #0F52BA;\n}\n\n.address-section {\n  margin-bottom: 20px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmlkZXIvaG9tZS9ob21lLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGVBQUE7QUFDSjs7QUFFQTtFQUNJLGFBQUE7QUFDSjs7QUFFQTtFQUNJLGNBQUE7QUFDSjs7QUFFQTtFQUNJLG1CQUFBO0FBQ0oiLCJmaWxlIjoic3JjL2FwcC9yaWRlci9ob21lL2hvbWUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnJpZGVyLXJldmlld3Mge1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbn1cblxuLnRyYW5zaXQge1xuICAgIGNvbG9yOiBwdXJwbGU7XG59XG5cbi52ZXJpZmllZCB7XG4gICAgY29sb3I6ICMwRjUyQkE7XG59XG5cbi5hZGRyZXNzLXNlY3Rpb24ge1xuICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XG59Il19 */");

/***/ }),

/***/ "./src/app/rider/home/home.page.ts":
/*!*****************************************!*\
  !*** ./src/app/rider/home/home.page.ts ***!
  \*****************************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");
/* harmony import */ var subSink__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! subSink */ "./node_modules/subsink/dist/es2015/index.js");
/* harmony import */ var _store_actions_rider_action__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../store/actions/rider.action */ "./src/app/rider/store/actions/rider.action.ts");
/* harmony import */ var _user_store_model_user_model__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../user/store/model/user.model */ "./src/app/user/store/model/user.model.ts");
/* harmony import */ var _utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../utils/functions/app.functions */ "./src/utils/functions/app.functions.ts");
/* harmony import */ var _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../utils/types/app.constant */ "./src/utils/types/app.constant.ts");
/* harmony import */ var _user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../user/store/actions/user.action */ "./src/app/user/store/actions/user.action.ts");
/* harmony import */ var _services_signalr_websocket_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../services/signalr-websocket.service */ "./src/app/services/signalr-websocket.service.ts");
/* harmony import */ var _services_location_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../services/location.service */ "./src/app/services/location.service.ts");











let HomePage = class HomePage {
    constructor(store, _ngZone, signalRSrv, locationSrv) {
        this.store = store;
        this._ngZone = _ngZone;
        this.signalRSrv = signalRSrv;
        this.locationSrv = locationSrv;
        this.productCartStatus = _user_store_model_user_model__WEBPACK_IMPORTED_MODULE_5__["ProductCartStatus"];
        this.subSink = new subSink__WEBPACK_IMPORTED_MODULE_3__["SubSink"]();
        // ? Subscribe and listen to websocket events related to the rider
        this.subscribeToEvents();
    }
    ngOnInit() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            unBundleSVG();
            const { userId } = yield Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_6__["getDataFromLocalStorage"])(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_7__["LocalStorageKey"].ZERO_30_USER);
            if (userId) {
                this.userId = userId;
                // ? Start the geolocation beacon and emit the rider location data, if they have any carts marked as "IN_TRANSIT"
                yield this.locationSrv.startGeolocationRemotely();
                this.userId = userId;
                this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_8__["actions"].GetUserDataInitiatedAction({ payload: { userId } }));
                this.userData$ = this.store.select((data) => data.User.UserData);
                // ? Get the list of carts that the rider has delivered in recently
                this.store.dispatch(_store_actions_rider_action__WEBPACK_IMPORTED_MODULE_4__["actions"].GetRiderActivityHistoryInitiatedAction({ payload: { userId } }));
                this.riderDeliveryHistory$ = this.store.select((data) => data.Rider.DeliveryActivities);
                // ? Get all the rider ratings
                this.store.dispatch(_store_actions_rider_action__WEBPACK_IMPORTED_MODULE_4__["actions"].GetRiderRatingInitiatedAction({ payload: { userId } }));
                this.riderRating$ = this.store.select((data) => data.Rider.RiderRating);
                // ? Connect the rider to a group allowing them to recieve personal notifications
                this.signalRSrv.addRiderToGroup(userId);
                this.signalRSrv.cartAssignedToCartRider();
            }
        });
    }
    subscribeToEvents() {
        this.subSink.sink =
            this.signalRSrv.cartAssignedToRider.subscribe((payload) => {
                this._ngZone.run(() => {
                    console.log({ payload });
                    if (payload === null || payload === void 0 ? void 0 : payload.person) {
                        this.store.dispatch(_store_actions_rider_action__WEBPACK_IMPORTED_MODULE_4__["actions"].NewCartAssignedToRiderAction({ payload: { cart: payload } }));
                    }
                });
            });
    }
    fillInUrl(status) {
        return status === _user_store_model_user_model__WEBPACK_IMPORTED_MODULE_5__["ProductCartStatus"].TRANSIT || this.productCartStatus.COMPLETED
            ? "order-info"
            : "new-order-detail";
    }
    ngOnDestroy() {
        this.subSink.unsubscribe();
    }
};
HomePage.ctorParameters = () => [
    { type: _ngrx_store__WEBPACK_IMPORTED_MODULE_2__["Store"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"] },
    { type: _services_signalr_websocket_service__WEBPACK_IMPORTED_MODULE_9__["SignalrWebsocketService"] },
    { type: _services_location_service__WEBPACK_IMPORTED_MODULE_10__["LocationService"] }
];
HomePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-home",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./home.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/rider/home/home.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./home.page.scss */ "./src/app/rider/home/home.page.scss")).default]
    })
], HomePage);



/***/ })

}]);
//# sourceMappingURL=home-home-module-es2015.js.map