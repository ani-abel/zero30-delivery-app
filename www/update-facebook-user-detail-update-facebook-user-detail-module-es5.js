(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["update-facebook-user-detail-update-facebook-user-detail-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/auth/update-facebook-user-detail/update-facebook-user-detail.page.html":
    /*!******************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/auth/update-facebook-user-detail/update-facebook-user-detail.page.html ***!
      \******************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppAuthUpdateFacebookUserDetailUpdateFacebookUserDetailPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n  <main id=\"create-new-account-page\" class=\"bg-offwhite container\">\n    <section id=\"header\" class=\"constrain\">\n        <div class=\"navigation\">\n            <a [routerLink]=\"['/auth', 'login']\" class=\"back\">\n                <img class=\"svg\" src=\"assets/images/icons/arrow-left.svg\" height=\"5px\" alt=\"Go back\">\n            </a>\n        </div>\n    </section>\n\n    <section id=\"heading-text\" class=\"constrain\">\n        <h1 class=\"title\">Update Account Details</h1>\n    </section>\n\n    <section id=\"create-new-account-form\" class=\"constrain\">\n        <form [formGroup]=\"updateFacebookDataForm\" (ngSubmit)=\"onSubmit()\">\n            <div class=\"form-row\">\n                <div>\n                    <input type=\"email\" \n                        placeholder=\"Email\" \n                        name=\"email\" \n                        [formControlName]=\"'Email'\" />\n                    <p class=\"margin-xs\" \n                            *ngIf=\"updateFacebookDataForm.get('Email').invalid && updateFacebookDataForm.get('Email').touched\">\n                        <span class=\"error-message-block\" \n                            *ngIf=\"updateFacebookDataForm.get('Email').errors['required']\">\n                            This field is required\n                        </span>\n                        <span class=\"error-message-block\" \n                            *ngIf=\"updateFacebookDataForm.get('Email').errors['email']\">\n                            Must be a valid email\n                        </span>\n                    </p>\n                </div>\n            </div>\n\n            <div class=\"form-row\">\n                <div>\n                    <input type=\"tel\" \n                        placeholder=\"Phone Number\" \n                        [formControlName]=\"'PhoneNumber'\" />\n                    <p class=\"margin-xs\" \n                        *ngIf=\"updateFacebookDataForm.get('PhoneNumber').invalid && updateFacebookDataForm.get('PhoneNumber').touched\">\n                        <span class=\"error-message-block\" \n                            *ngIf=\"updateFacebookDataForm.get('PhoneNumber').errors['required']\">\n                            This field is required\n                        </span>\n                    </p>\n                </div>\n            </div>\n\n            <div class=\"form-row\">\n                <div>\n                    <textarea \n                        class=\"resize-none\"\n                        [formControlName]=\"'Location'\" \n                        placeholder=\"Your Location\"></textarea>\n                    <p class=\"margin-xs\" \n                        *ngIf=\"updateFacebookDataForm.get('Location').invalid && updateFacebookDataForm.get('Location').touched\">\n                        <span class=\"error-message-block\" \n                            *ngIf=\"updateFacebookDataForm.get('Location').errors['required']\">\n                            This field is required\n                        </span>\n                    </p>\n                </div>\n            </div>\n\n            <div></div>\n\n            <div class=\"form-row\">\n                <button type=\"submit\" \n                    class=\"btn btn-primary\" \n                    [disabled]=\"updateFacebookDataForm.invalid\">\n                    Submit\n                </button>\n            </div>\n        </form>\n    </section>\n  </main>\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/auth/update-facebook-user-detail/update-facebook-user-detail-routing.module.ts":
    /*!************************************************************************************************!*\
      !*** ./src/app/auth/update-facebook-user-detail/update-facebook-user-detail-routing.module.ts ***!
      \************************************************************************************************/

    /*! exports provided: UpdateFacebookUserDetailPageRoutingModule */

    /***/
    function srcAppAuthUpdateFacebookUserDetailUpdateFacebookUserDetailRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UpdateFacebookUserDetailPageRoutingModule", function () {
        return UpdateFacebookUserDetailPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _update_facebook_user_detail_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./update-facebook-user-detail.page */
      "./src/app/auth/update-facebook-user-detail/update-facebook-user-detail.page.ts");

      var routes = [{
        path: '',
        component: _update_facebook_user_detail_page__WEBPACK_IMPORTED_MODULE_3__["UpdateFacebookUserDetailPage"]
      }];

      var UpdateFacebookUserDetailPageRoutingModule = function UpdateFacebookUserDetailPageRoutingModule() {
        _classCallCheck(this, UpdateFacebookUserDetailPageRoutingModule);
      };

      UpdateFacebookUserDetailPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], UpdateFacebookUserDetailPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/auth/update-facebook-user-detail/update-facebook-user-detail.module.ts":
    /*!****************************************************************************************!*\
      !*** ./src/app/auth/update-facebook-user-detail/update-facebook-user-detail.module.ts ***!
      \****************************************************************************************/

    /*! exports provided: UpdateFacebookUserDetailPageModule */

    /***/
    function srcAppAuthUpdateFacebookUserDetailUpdateFacebookUserDetailModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UpdateFacebookUserDetailPageModule", function () {
        return UpdateFacebookUserDetailPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _update_facebook_user_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./update-facebook-user-detail-routing.module */
      "./src/app/auth/update-facebook-user-detail/update-facebook-user-detail-routing.module.ts");
      /* harmony import */


      var _update_facebook_user_detail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./update-facebook-user-detail.page */
      "./src/app/auth/update-facebook-user-detail/update-facebook-user-detail.page.ts");
      /* harmony import */


      var _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../../shared/shared.module */
      "./src/app/shared/shared.module.ts");

      var UpdateFacebookUserDetailPageModule = function UpdateFacebookUserDetailPageModule() {
        _classCallCheck(this, UpdateFacebookUserDetailPageModule);
      };

      UpdateFacebookUserDetailPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"], _update_facebook_user_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__["UpdateFacebookUserDetailPageRoutingModule"]],
        declarations: [_update_facebook_user_detail_page__WEBPACK_IMPORTED_MODULE_6__["UpdateFacebookUserDetailPage"]]
      })], UpdateFacebookUserDetailPageModule);
      /***/
    },

    /***/
    "./src/app/auth/update-facebook-user-detail/update-facebook-user-detail.page.scss":
    /*!****************************************************************************************!*\
      !*** ./src/app/auth/update-facebook-user-detail/update-facebook-user-detail.page.scss ***!
      \****************************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppAuthUpdateFacebookUserDetailUpdateFacebookUserDetailPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2F1dGgvdXBkYXRlLWZhY2Vib29rLXVzZXItZGV0YWlsL3VwZGF0ZS1mYWNlYm9vay11c2VyLWRldGFpbC5wYWdlLnNjc3MifQ== */";
      /***/
    },

    /***/
    "./src/app/auth/update-facebook-user-detail/update-facebook-user-detail.page.ts":
    /*!**************************************************************************************!*\
      !*** ./src/app/auth/update-facebook-user-detail/update-facebook-user-detail.page.ts ***!
      \**************************************************************************************/

    /*! exports provided: UpdateFacebookUserDetailPage */

    /***/
    function srcAppAuthUpdateFacebookUserDetailUpdateFacebookUserDetailPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UpdateFacebookUserDetailPage", function () {
        return UpdateFacebookUserDetailPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ngrx_store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ngrx/store */
      "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");
      /* harmony import */


      var subsink__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! subsink */
      "./node_modules/subsink/dist/es2015/index.js");
      /* harmony import */


      var _store_actions_auth_action__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ../store/actions/auth.action */
      "./src/app/auth/store/actions/auth.action.ts");

      var UpdateFacebookUserDetailPage = /*#__PURE__*/function () {
        function UpdateFacebookUserDetailPage(activatedRoute, store) {
          _classCallCheck(this, UpdateFacebookUserDetailPage);

          this.activatedRoute = activatedRoute;
          this.store = store;
          this.subSink = new subsink__WEBPACK_IMPORTED_MODULE_5__["SubSink"]();
        }

        _createClass(UpdateFacebookUserDetailPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this = this;

            this.initForm();
            this.subSink.sink = this.activatedRoute.params.subscribe(function (data) {
              _this.facebookId = data.facebookId;
            });
          }
        }, {
          key: "initForm",
          value: function initForm() {
            this.updateFacebookDataForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
              Location: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])),
              Email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])),
              PhoneNumber: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]))
            });
          }
        }, {
          key: "onSubmit",
          value: function onSubmit() {
            var _this2 = this;

            if (this.updateFacebookDataForm.invalid) {
              return;
            }

            if (this.facebookId) {
              var _this$updateFacebookD = this.updateFacebookDataForm.value,
                  address = _this$updateFacebookD.Location,
                  email = _this$updateFacebookD.Email,
                  phoneNo = _this$updateFacebookD.PhoneNumber;
              this.store.dispatch(_store_actions_auth_action__WEBPACK_IMPORTED_MODULE_6__["actions"].UpdateFacebookUserDetailsInitiatedAction({
                payload: {
                  address: address,
                  email: email,
                  phoneNo: phoneNo,
                  facebookId: this.facebookId
                }
              }));
              this.subSink.sink = this.store.select(function (data) {
                return data.User.ActiveMessage;
              }).subscribe(function (data) {
                if (data) {
                  _this2.updateFacebookDataForm.reset();
                }
              });
            }
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            this.subSink.unsubscribe();
          }
        }]);

        return UpdateFacebookUserDetailPage;
      }();

      UpdateFacebookUserDetailPage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]
        }, {
          type: _ngrx_store__WEBPACK_IMPORTED_MODULE_4__["Store"]
        }];
      };

      UpdateFacebookUserDetailPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-update-facebook-user-detail',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./update-facebook-user-detail.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/auth/update-facebook-user-detail/update-facebook-user-detail.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./update-facebook-user-detail.page.scss */
        "./src/app/auth/update-facebook-user-detail/update-facebook-user-detail.page.scss"))["default"]]
      })], UpdateFacebookUserDetailPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=update-facebook-user-detail-update-facebook-user-detail-module-es5.js.map