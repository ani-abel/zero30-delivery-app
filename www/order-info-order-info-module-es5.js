(function () {
  function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

  function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

  function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

  function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

  function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

  function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["order-info-order-info-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/rider/order-info/order-info.page.html":
    /*!*********************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/rider/order-info/order-info.page.html ***!
      \*********************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppRiderOrderInfoOrderInfoPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n  <main id=\"order-information-page\" class=\"container\">\n    <!-- Wrapping everything in a form so it can be submitted to the database. -->\n    <form>\n      <section id=\"header\" class=\"constrain header-white\">\n        <a backButton class=\"back link\">\n          <img class=\"svg\" src=\"assets/images/icons/arrow-left.svg\" width=\"18px\" alt=\"Go back\" />\n        </a>\n  \n        <div class=\"text\">\n          <h2 class=\"title mb-2\">Order Information</h2>\n  \n          <!-- <p class=\"id\">\n            <span>ID:</span> {{ cartId }}\n          </p> -->\n        </div>\n\n        <div class=\"text\">\n          <p class=\"id\">\n            <span>ID:</span> {{ cartId }}\n          </p>\n        </div>\n        <p class=\"link\">\n          <a (click)=\"openCartCheckListModal()\">\n            <i class=\"fa fa-angle-double-right\"></i> View item Checklist\n          </a>\n        </p>\n      </section>\n  \n      <section id=\"delivery-address\" class=\"constrain\">\n        <div class=\"header\">\n          <h5>Delivery to</h5>\n        </div>\n  \n        <div class=\"address\" *ngIf=\"(orderInfo$ | async)?.cart.user as userInfo\">\n          <!-- i think a map goes here. -->\n          <div class=\"map\">\n            <img src=\"assets/images/others/address-map.png\" height=\"100px\" alt=\"Map\">\n          </div>\n  \n          <div class=\"details\">\n            <h5 class=\"location-address\">\n              <img class=\"svg\" src=\"assets/images/icons/map-pointer.svg\" height=\"9px\" alt=\"Store\">\n              <span>{{ (orderInfo$ | async)?.cart.deliveryAddress | capitalize }}</span>\n            </h5>\n            <p class=\"name\">\n              <img class=\"svg\" src=\"assets/images/icons/person.svg\" height=\"9px\" alt=\"Store\">\n              <span>\n                {{ userInfo.person.firstName | capitalize }}\n                {{ userInfo.person.lastName | capitalize }}\n              </span>\n            </p>\n            <p class=\"phone\">\n              <img class=\"svg\" src=\"assets/images/icons/telephone.svg\" height=\"9px\" alt=\"Store\">\n              <span>{{ userInfo.person.phoneNo }}</span>\n            </p>\n          </div>\n        </div>\n      </section>\n      <!-- TODO: should I list at all the merchant in a cart? -->\n  \n      <section id=\"delivery-details\">\n        <div class=\"heading constrain\" *ngIf=\"(orderInfo$ | async) as orderInfo\">\n          <div class=\"v-grid\">\n            <h5>Delivery Time</h5>\n            <div class=\"h-grid\">\n              <p>{{ orderInfo?.dateCreated | date : 'shortTime' }}</p>\n              <p>{{ orderInfo?.dateCreated | date : 'mediumDate' }}</p>\n            </div>\n          </div>\n          <!-- <a class=\"link link-primary\">Edit</a> -->\n        </div>\n  \n        <div class=\"cart-item constrain\" *ngFor=\"let merchant of (merchantsRelatedToCart$ | async)\">\n          <div class=\"image\">\n            <img [src]=\"merchant.merchantImage | filePathFormatter\"\n              height=\"100px\" \n              *ngIf=\"merchant.merchantImage; else defaultImage\"\n              [alt]=\"merchant.merchantName\" />\n            <ng-template #defaultImage>\n              <img src=\"assets/images/restaurants/lion-square.png\" height=\"100px\" [alt]=\"merchant.merchantName\" />\n            </ng-template>\n          </div>\n          <div class=\"details\">\n            <h5 class=\"name\">{{ merchant.merchantName | capitalize }}</h5>\n            <!-- <p class=\"product\">Lemon Fresh Juice</p> -->\n            <p class=\"product\">{{ merchant.merchantAddress | capitalize }}</p>\n          </div>\n        </div>\n        <div class=\"cart-totals constrain\" *ngIf=\"(orderInfo$ | async) as orderInfo\">\n          <div class=\"h-grid\">\n            <p>Subtotal (1 item)</p>\n            <p>{{ orderInfo.cart.amount | currency }}</p>\n          </div>\n          <div class=\"h-grid\">\n            <p>VAT</p>\n            <p>{{ orderInfo.cart.vat | currency }}</p>\n          </div>\n          <div class=\"h-grid\">\n            <p>DeliveryFee ({{ orderInfo.cart.distance }}Km)</p>\n            <p>{{ orderInfo.cart.deliveryCost | currency }}</p>\n          </div>\n          <div class=\"totals\">\n            <h4>Total</h4>\n            <h4>{{ orderInfo.cart.total | currency }}</h4>\n          </div>\n          <div class=\"h-grid payment-status-line\">\n            <p>Payment Status &nbsp;&nbsp;&nbsp;<span class=\"hightlight\">{{ orderInfo.paid ? \"Yes\" : \"No\" }}</span></p>\n          </div>\n        </div>\n      </section>\n      <section id=\"note\">\n        <div class=\"title constrain\">\n          <h5>Note</h5>\n        </div>\n        <div class=\"constrain\">\n          <textarea name=\"note\" placeholder=\"Please call me when you come. Thank you!\" id=\"note\"></textarea>\n        </div>\n      </section>\n      \n      <div *ngIf=\"(orderInfo$ | async) as orderInfo\">\n        <!-- <div class=\"submit constrain\">\n          <button type=\"button\"  \n            class=\"btn btn-primary cursor-pointer\"\n            (click)=\"completeDelivery(orderInfo.cartId)\">\n            Completed\n          </button>\n        </div> -->\n\n        <div class=\"submit constrain\" \n            *ngIf=\"orderInfo.cart.status === productStatus.VERIFIED; else defaultButtons\"\n            (click)=\"emitRiderLocation(orderInfo.cartId)\">\n            <button type=\"button\" \n            class=\"btn btn-primary cursor-pointer\">\n            Start\n          </button>\n        </div>\n\n        <ng-template #defaultButtons>\n          <div *ngIf=\"orderInfo.cart.status === productStatus.TRANSIT\">\n            <div class=\"submit constrain\" \n              *ngIf=\"!orderInfo.paid; else completedButton\">\n              <a [routerLink]=\"['/rider', 'accept-manual-payment', orderInfo.cartId]\"\n                class=\"btn btn-primary cursor-pointer\">\n                Accept Payment\n              </a>\n            </div>\n\n            <ng-template #completedButton>\n              <div class=\"submit constrain\" *ngIf=\"orderInfo.paid\">\n                <button type=\"button\"\n                  class=\"btn btn-primary cursor-pointer\"\n                  (click)=\"completeDelivery(orderInfo.cartId)\">\n                  Completed\n                </button>\n              </div>\n            </ng-template>\n          </div>\n        </ng-template>\n      </div>\n\n    </form>\n  </main>\n\n  <!-- <app-footer-expander></app-footer-expander>\n  <app-rider-bottom-navbar></app-rider-bottom-navbar> -->\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/rider/order-info/order-info-routing.module.ts":
    /*!***************************************************************!*\
      !*** ./src/app/rider/order-info/order-info-routing.module.ts ***!
      \***************************************************************/

    /*! exports provided: OrderInfoPageRoutingModule */

    /***/
    function srcAppRiderOrderInfoOrderInfoRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "OrderInfoPageRoutingModule", function () {
        return OrderInfoPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _order_info_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./order-info.page */
      "./src/app/rider/order-info/order-info.page.ts");

      var routes = [{
        path: '',
        component: _order_info_page__WEBPACK_IMPORTED_MODULE_3__["OrderInfoPage"]
      }];

      var OrderInfoPageRoutingModule = function OrderInfoPageRoutingModule() {
        _classCallCheck(this, OrderInfoPageRoutingModule);
      };

      OrderInfoPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], OrderInfoPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/rider/order-info/order-info.module.ts":
    /*!*******************************************************!*\
      !*** ./src/app/rider/order-info/order-info.module.ts ***!
      \*******************************************************/

    /*! exports provided: OrderInfoPageModule */

    /***/
    function srcAppRiderOrderInfoOrderInfoModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "OrderInfoPageModule", function () {
        return OrderInfoPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _order_info_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./order-info-routing.module */
      "./src/app/rider/order-info/order-info-routing.module.ts");
      /* harmony import */


      var _order_info_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./order-info.page */
      "./src/app/rider/order-info/order-info.page.ts");
      /* harmony import */


      var src_app_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/shared/shared.module */
      "./src/app/shared/shared.module.ts");

      var OrderInfoPageModule = function OrderInfoPageModule() {
        _classCallCheck(this, OrderInfoPageModule);
      };

      OrderInfoPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], src_app_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"], _order_info_routing_module__WEBPACK_IMPORTED_MODULE_5__["OrderInfoPageRoutingModule"]],
        declarations: [_order_info_page__WEBPACK_IMPORTED_MODULE_6__["OrderInfoPage"]]
      })], OrderInfoPageModule);
      /***/
    },

    /***/
    "./src/app/rider/order-info/order-info.page.scss":
    /*!*******************************************************!*\
      !*** ./src/app/rider/order-info/order-info.page.scss ***!
      \*******************************************************/

    /*! exports provided: default */

    /***/
    function srcAppRiderOrderInfoOrderInfoPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".payment-status-line p {\n  font-size: 18px;\n}\n.payment-status-line p .hightlight {\n  color: #CBCBCD;\n}\n#order-information-page #header {\n  grid-gap: 10px;\n}\n.link a:hover,\n.link a:focus {\n  color: #fa6400;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmlkZXIvb3JkZXItaW5mby9vcmRlci1pbmZvLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDSTtFQUNJLGVBQUE7QUFBUjtBQUVRO0VBQ0ksY0FBQTtBQUFaO0FBS0E7RUFDSSxjQUFBO0FBRko7QUFNSTs7RUFFSSxjQUFBO0FBSFIiLCJmaWxlIjoic3JjL2FwcC9yaWRlci9vcmRlci1pbmZvL29yZGVyLWluZm8ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnBheW1lbnQtc3RhdHVzLWxpbmUge1xuICAgIHAge1xuICAgICAgICBmb250LXNpemU6IDE4cHg7XG5cbiAgICAgICAgLmhpZ2h0bGlnaHQge1xuICAgICAgICAgICAgY29sb3I6ICNDQkNCQ0Q7XG4gICAgICAgIH1cbiAgICB9XG59XG5cbiNvcmRlci1pbmZvcm1hdGlvbi1wYWdlICNoZWFkZXIge1xuICAgIGdyaWQtZ2FwOiAxMHB4O1xufVxuXG4ubGluayB7XG4gICAgYTpob3ZlcixcbiAgICBhOmZvY3VzIHtcbiAgICAgICAgY29sb3I6ICNmYTY0MDA7XG4gICAgfVxufSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/rider/order-info/order-info.page.ts":
    /*!*****************************************************!*\
      !*** ./src/app/rider/order-info/order-info.page.ts ***!
      \*****************************************************/

    /*! exports provided: OrderInfoPage */

    /***/
    function srcAppRiderOrderInfoOrderInfoPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "OrderInfoPage", function () {
        return OrderInfoPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _ngrx_store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ngrx/store */
      "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");
      /* harmony import */


      var subsink__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! subsink */
      "./node_modules/subsink/dist/es2015/index.js");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! rxjs */
      "./node_modules/rxjs/_esm2015/index.js");
      /* harmony import */


      var _store_actions_rider_action__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../store/actions/rider.action */
      "./src/app/rider/store/actions/rider.action.ts");
      /* harmony import */


      var _user_store_model_user_model__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ../../user/store/model/user.model */
      "./src/app/user/store/model/user.model.ts");
      /* harmony import */


      var _utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ../../../utils/functions/app.functions */
      "./src/utils/functions/app.functions.ts");
      /* harmony import */


      var _user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! ../../user/store/actions/user.action */
      "./src/app/user/store/actions/user.action.ts");
      /* harmony import */


      var _services_location_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! ../../services/location.service */
      "./src/app/services/location.service.ts");
      /* harmony import */


      var _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
      /*! ../../../utils/types/app.constant */
      "./src/utils/types/app.constant.ts");
      /* harmony import */


      var _view_cart_detail_view_cart_detail_page__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
      /*! ../view-cart-detail/view-cart-detail.page */
      "./src/app/rider/view-cart-detail/view-cart-detail.page.ts");

      var OrderInfoPage = /*#__PURE__*/function () {
        function OrderInfoPage(store, activatedRoute, locationSrv, modalController) {
          _classCallCheck(this, OrderInfoPage);

          this.store = store;
          this.activatedRoute = activatedRoute;
          this.locationSrv = locationSrv;
          this.modalController = modalController;
          this.productStatus = _user_store_model_user_model__WEBPACK_IMPORTED_MODULE_8__["ProductCartStatus"];
          this.subSink = new subsink__WEBPACK_IMPORTED_MODULE_5__["SubSink"]();
        }

        _createClass(OrderInfoPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var _this = this;

              var userData;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      this.subSink.sink = this.activatedRoute.params.subscribe(function (data) {
                        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                          return regeneratorRuntime.wrap(function _callee$(_context) {
                            while (1) {
                              switch (_context.prev = _context.next) {
                                case 0:
                                  _context.next = 2;
                                  return this.locationSrv.startGeolocationRemotely();

                                case 2:
                                  this.cartId = data.cartId;
                                  this.store.dispatch(_store_actions_rider_action__WEBPACK_IMPORTED_MODULE_7__["actions"].GetOrderInfoInitiatedAction({
                                    payload: {
                                      cartId: data.cartId
                                    }
                                  }));
                                  this.orderInfo$ = this.store.select(function (storeData) {
                                    return storeData.Rider.SelectedOrderInfo;
                                  });
                                  this.store.dispatch(_store_actions_rider_action__WEBPACK_IMPORTED_MODULE_7__["actions"].GetMerchantsRelatedToCartInitiatedAction({
                                    payload: {
                                      cartId: data.cartId
                                    }
                                  }));
                                  this.merchantsRelatedToCart$ = this.store.select(function (storeData) {
                                    return storeData.Rider.MerchantsRelatedToCart;
                                  }); // ? Get the content of all items in the cart

                                  this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_10__["actions"].GetSelectedCartInitiatedAction({
                                    payload: {
                                      cartId: data.cartId
                                    }
                                  }));
                                  this.selectedCart$ = this.store.select(function (storeData) {
                                    return storeData.User.SelectedCart;
                                  });

                                case 9:
                                case "end":
                                  return _context.stop();
                              }
                            }
                          }, _callee, this);
                        }));
                      });
                      _context2.next = 3;
                      return Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_9__["getDataFromLocalStorage"])(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_12__["LocalStorageKey"].ZERO_30_USER);

                    case 3:
                      userData = _context2.sent;

                      if (userData.userId) {
                        this.userId = userData.userId;
                      }

                    case 5:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }, {
          key: "emitRiderLocation",
          value: function emitRiderLocation(cartId) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
              var _this2 = this;

              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                      if (!this.userId) {
                        _context3.next = 5;
                        break;
                      }

                      this.store.dispatch(_store_actions_rider_action__WEBPACK_IMPORTED_MODULE_7__["actions"].AcceptCartDeliveryInitiatedAction({
                        payload: {
                          cartId: cartId,
                          userId: this.userId
                        }
                      }));
                      _context3.next = 4;
                      return this.locationSrv.startGeolocationRemotely();

                    case 4:
                      // TODO Update the status of selectedCart, once the user selects it
                      // this.store.dispatch(RiderActions.GetOrderInfoInitiatedAction({ payload: { cartId: this.cartId } }));
                      // this.orderInfo$ = this.store.select((data) => data.Rider.SelectedOrderInfo);
                      this.subSink.sink = this.store.select(function (data) {
                        return data.Rider.ActiveMessage;
                      }).subscribe(function (data) {
                        if (data) {
                          _this2.subSink.sink = _this2.orderInfo$.subscribe(function (currentData) {
                            _this2.orderInfo$ = Object(rxjs__WEBPACK_IMPORTED_MODULE_6__["of"])(_this2.updateCartState(currentData));
                          });
                        }
                      });

                    case 5:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3, this);
            }));
          }
        }, {
          key: "openCartCheckListModal",
          value: function openCartCheckListModal() {
            var _this3 = this;

            this.store.dispatch(_store_actions_rider_action__WEBPACK_IMPORTED_MODULE_7__["actions"].GetCartCheckListInitiatedAction({
              payload: {
                cartId: this.cartId
              }
            }));
            var cartCheckList$ = this.store.select(function (data) {
              return data.Rider.SelectedCartCheckList;
            });
            this.subSink.sink = cartCheckList$.subscribe(function (data) {
              return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this3, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
                var modal;
                return regeneratorRuntime.wrap(function _callee4$(_context4) {
                  while (1) {
                    switch (_context4.prev = _context4.next) {
                      case 0:
                        if (!((data === null || data === void 0 ? void 0 : data.length) > 0)) {
                          _context4.next = 6;
                          break;
                        }

                        _context4.next = 3;
                        return this.modalController.create({
                          component: _view_cart_detail_view_cart_detail_page__WEBPACK_IMPORTED_MODULE_13__["ViewCartDetailPage"],
                          componentProps: {
                            cartCheckLists: _toConsumableArray(data)
                          }
                        });

                      case 3:
                        modal = _context4.sent;
                        _context4.next = 6;
                        return modal.present();

                      case 6:
                      case "end":
                        return _context4.stop();
                    }
                  }
                }, _callee4, this);
              }));
            });
          }
        }, {
          key: "completeDelivery",
          value: function completeDelivery(cartId) {
            if (cartId) {
              this.store.dispatch(_store_actions_rider_action__WEBPACK_IMPORTED_MODULE_7__["actions"].MarkDeliveryAsCompleteInitiatedAction({
                payload: {
                  cartId: cartId
                }
              }));
            }
          }
        }, {
          key: "updateCartState",
          value: function updateCartState(payload) {
            try {
              return Object.assign(Object.assign({}, payload), {
                cart: Object.assign(Object.assign({}, payload.cart), {
                  status: _user_store_model_user_model__WEBPACK_IMPORTED_MODULE_8__["ProductCartStatus"].TRANSIT
                })
              });
            } catch (ex) {
              throw ex;
            }
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            this.subSink.unsubscribe();
          }
        }]);

        return OrderInfoPage;
      }();

      OrderInfoPage.ctorParameters = function () {
        return [{
          type: _ngrx_store__WEBPACK_IMPORTED_MODULE_4__["Store"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
        }, {
          type: _services_location_service__WEBPACK_IMPORTED_MODULE_11__["LocationService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"]
        }];
      };

      OrderInfoPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-order-info",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./order-info.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/rider/order-info/order-info.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./order-info.page.scss */
        "./src/app/rider/order-info/order-info.page.scss"))["default"]]
      })], OrderInfoPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=order-info-order-info-module-es5.js.map