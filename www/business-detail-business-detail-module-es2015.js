(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["business-detail-business-detail-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/user/nearby/business-detail/business-detail.page.html":
/*!*************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/user/nearby/business-detail/business-detail.page.html ***!
  \*************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n  <main id=\"business-details-page\" class=\"bg-offwhite container with-bottom-menu bookmarked\">\n\t\n    <!-- So this <section id=\"header\"> has a backgroud image, I don't know if it's meant to be dynamic but I've left a link to the image in the inline style of the section.  -->\n    <section id=\"header\" class=\"constrain header-transparent-image\"\n      style=\"background: linear-gradient(0deg, rgba(34, 43, 69, 0.7), rgba(34, 43, 69, 0.7)), url(assets/images/banner/banner2.png);\">\n      \n      <div class=\"header-actions\">\n        <a [routerLink]=\"['/user']\" class=\"back link\">\n          <img class=\"svg\" src=\"assets/images/icons/arrow-left.svg\" width=\"18px\" alt=\"Go back\">\n        </a>\n        <!-- Bookmark icon -->\n        <button type=\"button\" class=\"btn bookmark\" (click)=\"addMerchantToBookmark()\">\n          <img class=\"svg\" \n            src=\"assets/images/icons/bookmark.svg\" \n            height=\"18px\" alt=\"Bookmark\" />\n        </button>\n        <button type=\"button\" class=\"link options\">\n          <img class=\"svg\" src=\"assets/images/icons/options.svg\" width=\"18px\" alt=\"Options\">\n        </button>\n      </div>\n  \n      <!-- This shows the name and address of the restaurant -->\n      <div class=\"text\" *ngIf=\"(selectedMerchant$ | async) as merchant\">\n        <h2 class=\"name\">{{ merchant?.companyName | uppercase }}</h2>\n        <p class=\"address\">\n          <img class=\"svg\" src=\"assets/images/icons/map-pointer.svg\" height=\"10px\" alt=\"Store\">\n          <span>{{ merchant?.address | uppercase }}</span>\n        </p>\n        <p class=\"reviews\">\n          <img class=\"svg icon\" alt=\"\" src=\"assets/images/icons/star.svg\">\n          <b>{{ merchant?.rate | number : '1.1-3' }}</b>\n          <span>({{ merchant?.ratingCount || 0 }} Reviews)</span>\n        </p>\n      </div>\n    </section>\n  \n    <!-- This is that menu at the bottom of every page -->\n    <app-user-bottom-navbar></app-user-bottom-navbar>\n\n    <!-- \n      This is the tab buttons.\n      When you click the buttons they activate the selected tab, clicking it also adds a \".active\" class to both the button of the tab and the tab content.\n      I have already implemented it using javascript.\n    -->\n    <section id=\"tabs\" class=\"constrain\">\n      <button class=\"tab\" \n        [ngClass]=\"{ 'active': activeTab === 'PRODUCT' }\" \n        (click)=\"switchTab('PRODUCT')\"><span>Products</span>\n      </button>\n      <button class=\"tab\" \n        [ngClass]=\"{ 'active': activeTab === 'REVIEW' }\"\n        (click)=\"switchTab('REVIEW')\"><span>Reviews</span>\n      </button>\n      <button class=\"tab\"\n        [ngClass]=\"{ 'active': activeTab === 'INFORMATION' }\"\n        (click)=\"switchTab('INFORMATION')\"><span>Information</span></button>\n    </section>\n  \n    <!-- Tab content -->\n    <!-- This is the Products tab -->\n    <section id=\"products\" class=\"tab-content\" [ngClass]=\"{ 'active': activeTab === 'PRODUCT' }\">\n      <div class=\"section-title with-link constrain\">\n        <h4>Popular Items</h4>\n        <a [routerLink]=\"['/user', 'merchant-products', merchantId]\" class=\"link link-primary\">\n          <span>View all</span>\n          <img class=\"svg\" src=\"assets/images/icons/double-chevron-right.svg\" height=\"7px\" alt=\"Arrow Right\">\n        </a>\n      </div>\n\n      <swiper\n        *ngIf=\"(selectedMerchantHotSaleProducts$ | async) as selectedMerchantHotSaleProducts\"\n        [ngClass]=\"['products', 'constrain']\"\n        [slidesPerView]=\"'auto'\"\n        [spaceBetween]=\"15\"\n        [centeredSlides]=\"false\"\n        [loop]=\"true\"\n        [autoplay]=\"{ delay: 2500, disableOnInteraction: true }\">\n        <ng-template swiperSlide *ngFor=\"let product of selectedMerchantHotSaleProducts | sliceContent : 8\">\n          <app-merchant-hot-sale-product \n            (viewProductDetailEvent)=\"openProductDetail($event, product)\"\n            (addProductToBookmarkEvent)=\"addProductToBoomkark($event)\"\n            (addToCartEvent)=\"addProductToCart($event)\"\n            [product]=\"product\">\n        </app-merchant-hot-sale-product>\n        </ng-template>\n      </swiper>\n  \n      <div class=\"product-categories-container\" *ngIf=\"(productCategoryGroup$ | async) as productCategoryGroups\">\n        <div id=\"product-categories\" *ngFor=\"let productCategoryGroup of productCategoryGroups\">\n          <div class=\"section-title constrain\">\n            <h5>{{ productCategoryGroup.productCategoryModel?.name | uppercase }} \n              <span>{{ productCategoryGroup?.productDetailModel.length || 0 }} items</span>\n            </h5>\n          </div>\n    \n          <!-- \n            There's a \".bookmarked\" class on the products that are bookmarked by the user, Once the .bookmarked tag is added as a class it styles the bookmark icon.\n          -->\n         <app-product-category-widget\n          (viewProductDetailEvent)=\"openProductDetail($event, product)\"\n          (addToCartEvent)=\"addProductToCart($event)\"\n          (addProductToBookmarkEvent)=\"addProductToBoomkark($event)\"\n          [product]=\"product\" \n          *ngFor=\"let product of productCategoryGroup?.productDetailModel | sliceContent : 8\">\n        </app-product-category-widget>\n        </div>\n      </div>\n      \n    </section>\n  \n    <app-add-to-cart \n      *ngIf=\"openCartModal\"\n      [product]=\"productAddedToCart\"\n      [productImages]=\"productAddedToCartImages\"\n      [userId]=\"userId\"\n      [show]=\"openCartModal\"\n      (closeAddToCartModalEvent)=\"closeCartModal()\">\n    </app-add-to-cart>\n  \n    <!-- Tab content -->\n    <!-- This is the Reviews tab -->\n    <section id=\"reviews\" class=\"tab-content constrain\" [ngClass]=\"{ 'active': activeTab === 'REVIEW' }\">\n  \n      <!-- <div class=\"section-title with-link\">\n        <h4>Photos & Videos</h4>\n        <a href=\"#\" class=\"link link-primary\">\n          <span>View all</span>\n          <img class=\"svg\" src=\"assets/images/icons/double-chevron-right.svg\" height=\"7px\" alt=\"Arrow Right\">\n        </a>\n      </div> -->\n      \n      <!-- These are the photos from ratings. -->\n      <!-- <div class=\"review-media\">\n        <a href=\"#\" class=\"media\"><img src=\"assets/images/products/14.png\" height=\"70px\" alt=\"Review Media\"></a>\n        <a href=\"#\" class=\"media\"><img src=\"assets/images/products/15.png\" height=\"70px\" alt=\"Review Media\"></a>\n        <a href=\"#\" class=\"media\"><img src=\"assets/images/products/16.png\" height=\"70px\" alt=\"Review Media\"></a>\n        <a href=\"#\" class=\"media\"><img src=\"assets/images/products/17.png\" height=\"70px\" alt=\"Review Media\"></a>\n        <a href=\"#\" class=\"media\"><img src=\"assets/images/products/18.png\" height=\"70px\" alt=\"Review Media\"></a>\n      </div> -->\n  \n      <!-- Star Ratings -->\n      <div class=\"star-ratings\">\n        <h4>Rate</h4>\n        <div class=\"ratings\">\n          <div class=\"stars\">\n            <i *ngFor=\"let star of [0, 1, 2, 3, 4]; index as i\" \n              class=\"fa fa-star add-rating\" \n              [ngClass]=\"{ 'fa-checked': i < noOfStarsSelected }\"\n              (click)=\"addStarRating(i + 1)\"></i>\n          </div>\n        </div>\n      </div>\n      <div class=\"ratings caption-form constrain\" *ngIf=\"openCaptionForm\">\n        <form [formGroup]=\"ratingForm\" (ngSubmit)=\"onSubmit()\">\n         <div class=\"form-row\">\n              <div>\n                <textarea [formControlName]=\"'Caption'\"\n                  class=\"resize-none\"\n                  rows=\"5\"\n                  placeholder=\"Caption (Optional) 350 character limit\"\n                  maxlength=\"350\">\n                </textarea>\n              </div>\n          </div>\n\n          <div></div>\n\n          <div class=\"form-row\">\n              <button type=\"submit\" \n                [disabled]=\"ratingForm.invalid\" \n                class=\"btn btn-primary\" name=\"sign-in\">\n                Submit\n              </button>\n          </div>\n        </form>\n      </div>\n\n      <!-- <div class=\"star-ratings\">\n        <h4>Rate</h4>\n        <div class=\"ratings\">\n          <div class=\"rating star-5\"> -->\n            <!-- This is the number of reviews with 5 stars -->\n            <!-- <span>99+</span>\n            <div class=\"stars\">\n              <i class=\"fa fa-star fa-checked\"></i>\n              <i class=\"fa fa-star\"></i>\n              <i class=\"fa fa-star checked\"></i>\n              <i class=\"fa fa-star\"></i>\n              <i class=\"fa fa-star checked\"></i>\n            </div>\n          </div> -->\n          \n          <!-- <div class=\"rating star-4\">\n            <span>56</span>\n            <div class=\"stars\">\n              <img class=\"svg\" src=\"assets/images/icons/star.svg\" height=\"12px\" alt=\"Star\">\n              <img class=\"svg\" src=\"assets/images/icons/star.svg\" height=\"12px\" alt=\"Star\">\n              <img class=\"svg\" src=\"assets/images/icons/star.svg\" height=\"12px\" alt=\"Star\">\n              <img class=\"svg\" src=\"assets/images/icons/star.svg\" height=\"12px\" alt=\"Star\">\n              <img class=\"svg\" src=\"assets/images/icons/star.svg\" height=\"12px\" alt=\"Star\">\n            </div>\n          </div>\n          \n          <div class=\"rating star-3\">\n            <span>45</span>\n            <div class=\"stars\">\n              <img class=\"svg\" src=\"assets/images/icons/star.svg\" height=\"12px\" alt=\"Star\">\n              <img class=\"svg\" src=\"assets/images/icons/star.svg\" height=\"12px\" alt=\"Star\">\n              <img class=\"svg\" src=\"assets/images/icons/star.svg\" height=\"12px\" alt=\"Star\">\n              <img class=\"svg\" src=\"assets/images/icons/star.svg\" height=\"12px\" alt=\"Star\">\n              <img class=\"svg\" src=\"assets/images/icons/star.svg\" height=\"12px\" alt=\"Star\">\n            </div>\n          </div> -->\n          \n          <!-- <div class=\"rating star-2\">\n            <span>12</span>\n            <div class=\"stars\">\n              <img class=\"svg\" src=\"assets/images/icons/star.svg\" height=\"12px\" alt=\"Star\">\n              <img class=\"svg\" src=\"assets/images/icons/star.svg\" height=\"12px\" alt=\"Star\">\n              <img class=\"svg\" src=\"assets/images/icons/star.svg\" height=\"12px\" alt=\"Star\">\n              <img class=\"svg\" src=\"assets/images/icons/star.svg\" height=\"12px\" alt=\"Star\">\n              <img class=\"svg\" src=\"assets/images/icons/star.svg\" height=\"12px\" alt=\"Star\">\n            </div>\n          </div> -->\n        \n          <!-- <div class=\"rating star-1\">\n            <span>5</span>\n            <div class=\"stars\">\n              <img class=\"svg\" src=\"assets/images/icons/star.svg\" height=\"12px\" alt=\"Star\">\n              <img class=\"svg\" src=\"assets/images/icons/star.svg\" height=\"12px\" alt=\"Star\">\n              <img class=\"svg\" src=\"assets/images/icons/star.svg\" height=\"12px\" alt=\"Star\">\n              <img class=\"svg\" src=\"assets/images/icons/star.svg\" height=\"12px\" alt=\"Star\">\n              <img class=\"svg\" src=\"assets/images/icons/star.svg\" height=\"12px\" alt=\"Star\">\n            </div>\n          </div> -->\n        <!-- </div>\n      </div> -->\n  \n      <!-- Ratings -->\n      <div class=\"comment-container\" *ngIf=\"(selectedMerchantReviews$ | async) as selectedMerchantReviews\">\n        <div class=\"comments\" *ngIf=\"selectedMerchantReviews?.length > 0\">\n          <app-rating-comment \n            [merchantReview]=\"review\"\n            *ngFor=\"let review of selectedMerchantReviews\">\n          </app-rating-comment>\n        </div>\n      </div>\n    </section>\n    \n    <!-- Tab content -->\n    <section id=\"information\" class=\"tab-content\" [ngClass]=\"{ 'active': activeTab === 'INFORMATION' }\">\n      <div class=\"v-grid constrain\" *ngIf=\"(selectedMerchant$ | async)?.description\">\n        <h5 class=\"title\">Information</h5>\n        <p>{{ (selectedMerchant$ | async)?.description | capitalize }}</p>\n      </div>\n  \n      <div class=\"v-grid constrain\">\n        <h5 class=\"title\">Address</h5>\n        <div class=\"map\" #mapCanvas></div>\n      </div>\n    </section>\n\n    <app-footer-expander></app-footer-expander>\n    </main>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/user/nearby/business-detail/business-detail-routing.module.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/user/nearby/business-detail/business-detail-routing.module.ts ***!
  \*******************************************************************************/
/*! exports provided: BusinessDetailPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BusinessDetailPageRoutingModule", function() { return BusinessDetailPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _business_detail_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./business-detail.page */ "./src/app/user/nearby/business-detail/business-detail.page.ts");




const routes = [
    {
        path: '',
        component: _business_detail_page__WEBPACK_IMPORTED_MODULE_3__["BusinessDetailPage"]
    }
];
let BusinessDetailPageRoutingModule = class BusinessDetailPageRoutingModule {
};
BusinessDetailPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], BusinessDetailPageRoutingModule);



/***/ }),

/***/ "./src/app/user/nearby/business-detail/business-detail.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/user/nearby/business-detail/business-detail.module.ts ***!
  \***********************************************************************/
/*! exports provided: BusinessDetailPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BusinessDetailPageModule", function() { return BusinessDetailPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _business_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./business-detail-routing.module */ "./src/app/user/nearby/business-detail/business-detail-routing.module.ts");
/* harmony import */ var _business_detail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./business-detail.page */ "./src/app/user/nearby/business-detail/business-detail.page.ts");
/* harmony import */ var src_app_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/shared/shared.module */ "./src/app/shared/shared.module.ts");








let BusinessDetailPageModule = class BusinessDetailPageModule {
};
BusinessDetailPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            src_app_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"],
            _business_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__["BusinessDetailPageRoutingModule"]
        ],
        declarations: [_business_detail_page__WEBPACK_IMPORTED_MODULE_6__["BusinessDetailPage"]]
    })
], BusinessDetailPageModule);



/***/ }),

/***/ "./src/app/user/nearby/business-detail/business-detail.page.scss":
/*!***********************************************************************!*\
  !*** ./src/app/user/nearby/business-detail/business-detail.page.scss ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".add-rating:hover,\n.add-rating:active {\n  font-size: 22px;\n  cursor: pointer;\n}\n\n.caption-form {\n  padding: 10px 25px 50px 25px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXNlci9uZWFyYnkvYnVzaW5lc3MtZGV0YWlsL2J1c2luZXNzLWRldGFpbC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7O0VBRUksZUFBQTtFQUNBLGVBQUE7QUFDSjs7QUFFQTtFQUNJLDRCQUFBO0FBQ0oiLCJmaWxlIjoic3JjL2FwcC91c2VyL25lYXJieS9idXNpbmVzcy1kZXRhaWwvYnVzaW5lc3MtZGV0YWlsLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5hZGQtcmF0aW5nOmhvdmVyLFxuLmFkZC1yYXRpbmc6YWN0aXZlIHtcbiAgICBmb250LXNpemU6IDIycHg7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xufVxuXG4uY2FwdGlvbi1mb3JtIHtcbiAgICBwYWRkaW5nOiAxMHB4IDI1cHggNTBweCAyNXB4O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/user/nearby/business-detail/business-detail.page.ts":
/*!*********************************************************************!*\
  !*** ./src/app/user/nearby/business-detail/business-detail.page.ts ***!
  \*********************************************************************/
/*! exports provided: TabType, BusinessDetailPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabType", function() { return TabType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BusinessDetailPage", function() { return BusinessDetailPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");
/* harmony import */ var subsink__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! subsink */ "./node_modules/subsink/dist/es2015/index.js");
/* harmony import */ var _user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../user/store/actions/user.action */ "./src/app/user/store/actions/user.action.ts");
/* harmony import */ var _utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../utils/functions/app.functions */ "./src/utils/functions/app.functions.ts");
/* harmony import */ var _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../../utils/types/app.constant */ "./src/utils/types/app.constant.ts");
/* harmony import */ var swiper_core__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! swiper/core */ "./node_modules/swiper/swiper.esm.js");











// install Swiper components
swiper_core__WEBPACK_IMPORTED_MODULE_10__["default"].use([swiper_core__WEBPACK_IMPORTED_MODULE_10__["Navigation"], swiper_core__WEBPACK_IMPORTED_MODULE_10__["Pagination"], swiper_core__WEBPACK_IMPORTED_MODULE_10__["A11y"], swiper_core__WEBPACK_IMPORTED_MODULE_10__["Autoplay"]]);
var TabType;
(function (TabType) {
    TabType["PRODUCT"] = "PRODUCT";
    TabType["REVIEW"] = "REVIEW";
    TabType["INFORMATION"] = "INFORMATION";
})(TabType || (TabType = {}));
let BusinessDetailPage = class BusinessDetailPage {
    constructor(activatedRoute, router, store, modalController) {
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.store = store;
        this.modalController = modalController;
        this.subSink = new subsink__WEBPACK_IMPORTED_MODULE_6__["SubSink"]();
        this.activeTab = TabType.PRODUCT;
        this.openCaptionForm = false;
        this.noOfStarsSelected = 0;
        this.openCartModal = false;
    }
    ngOnInit() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.noOfStarsSelected = 0;
            this.initForm();
            //? Get and set userId from locastorage
            const { userId } = yield Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_8__["getDataFromLocalStorage"])(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_9__["LocalStorageKey"].ZERO_30_USER);
            this.userId = userId;
            this.subSink.sink =
                this.activatedRoute.params.subscribe((data) => {
                    this.merchantId = data.merchantId;
                    if (!(data === null || data === void 0 ? void 0 : data.merchantId)) {
                        //? Navigate away if merchantId is not found
                        this.router.navigate(["/user", "nearby"]);
                    }
                });
            this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_7__["actions"].GetMerchantDetailInitiatedAction({ payload: { typeId: this.merchantId } }));
            this.selectedMerchant$ = this.store.select((data) => data.User.SelectedMerchant);
            this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_7__["actions"].GetMerchantReviewsInitiatedAction({ payload: { merchantId: this.merchantId } }));
            this.selectedMerchantReviews$ = this.store.select((data) => data.User.SelectedMerchantReviews);
            //? Get hot sale items
            this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_7__["actions"].GetHotSaleProductForMerchantInitiatedAction({ payload: { merchantId: this.merchantId } }));
            this.selectedMerchantHotSaleProducts$ = this.store.select((data) => data.User.SelectedMerchantHotSaleProducts);
            //? Get merchant's products grouped by categories
            this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_7__["actions"].GetMechantProductsGroupedByCategoryInitiatedAction({ payload: { merchantId: this.merchantId } }));
            this.productCategoryGroup$ = this.store.select((data) => data.User.ProductCategoryGroups);
            this.subSink.sink =
                this.selectedMerchantReviews$.subscribe((data) => {
                    if ((data === null || data === void 0 ? void 0 : data.length) > 0) {
                        //? Find user review if any
                        data.find((review) => {
                            const match = review.userId === this.userId;
                            if (match) {
                                this.noOfStarsSelected = review.rating;
                            }
                            return match;
                        });
                    }
                });
            setTimeout(() => {
                this.subSink.sink =
                    this.selectedMerchant$.subscribe(data => {
                        if (data === null || data === void 0 ? void 0 : data.id) {
                            this.selectedMerchantDataSync = data;
                        }
                        else {
                            //? Navigate away if merchant is not found
                            this.router.navigate(["/user", "nearby"]);
                        }
                    });
            }, 2000);
        });
    }
    ngAfterViewInit() {
        setTimeout(() => {
            var _a;
            if ((_a = this.selectedMerchantDataSync) === null || _a === void 0 ? void 0 : _a.longitude) {
                const { longitude, latitude, address } = this.selectedMerchantDataSync;
                this.loadMap({
                    Latitude: latitude,
                    Longitude: longitude,
                    Element: this.mapCanvas,
                    Tooltip: address
                });
            }
        }, 3000);
    }
    // async openProductDetail(event: any, product: ProductDetailType): Promise<void> {
    //   if(product?.productModel) {
    //     const modal = await this.modalController.create({
    //       component: ProductDetailModalComponent,
    //       componentProps: {
    //         "product": product
    //       }
    //     });
    //     return await modal.present();
    //   }
    // }
    openProductDetail(event, product) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (product === null || product === void 0 ? void 0 : product.productModel) {
                this.router.navigateByUrl(`/user/product-detail?product=${JSON.stringify(product)}`);
            }
        });
    }
    initForm() {
        this.ratingForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            Caption: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null)
        });
    }
    addMerchantToBookmark() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.subSink.sink =
                this.selectedMerchant$.subscribe((merchant) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                    //? Get the user Id
                    if (this.userId) {
                        //? Dispatch action
                        this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_7__["actions"].AddMerchantToUserBookmarksInitiatedAction({ payload: {
                                MerchantId: merchant.id,
                                UserId: this.userId,
                                Merchant: merchant
                            }
                        }));
                    }
                }));
        });
    }
    switchTab(tab) {
        this.activeTab = tab;
    }
    addStarRating(index) {
        this.openCaptionForm = true;
        this.noOfStarsSelected = index;
    }
    addProductToBoomkark(event) {
        if (this.userId) {
            this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_7__["actions"].AddProductToBookmarkInitiatedAction({ payload: { productId: event, userId: this.userId } }));
        }
    }
    onSubmit() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (this.ratingForm.invalid) {
                return;
            }
            this.openCaptionForm = false;
            if (this.userId) {
                const { Caption } = this.ratingForm.value;
                this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_7__["actions"].AddMerchantRatingInitiatedAction({ payload: {
                        UserId: this.userId,
                        Rate: this.noOfStarsSelected,
                        MerchantId: this.merchantId,
                        Review: Caption || undefined
                    }
                }));
                this.subSink.sink =
                    this.store
                        .select((data) => data.User.ActiveMessage)
                        .subscribe((data) => {
                        if (data) {
                            this.ratingForm.reset();
                            this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_7__["actions"].GetMerchantReviewsInitiatedAction({ payload: { merchantId: this.merchantId } }));
                        }
                    });
            }
        });
    }
    loadMap(payload) {
        const { Longitude, Latitude, Element, Tooltip } = payload;
        const myLatLng = { lat: Latitude, lng: Longitude };
        const map = new google.maps.Map(Element.nativeElement, {
            zoom: 15,
            center: myLatLng,
        });
        new google.maps.Marker({
            position: myLatLng,
            map,
            title: Tooltip,
        });
    }
    // loadMap(payload: MapDetail): void {
    //   const { Longitude, Latitude, Element, Tooltip } = payload;
    //   L.mapquest.key = env.mapQuestAPIKey;
    //   const map = L.mapquest.map(Element.nativeElement, {
    //     center: [Longitude, Latitude],
    //     layers: L.mapquest.tileLayer("map"),
    //     zoom: 15
    //   });
    //   L.marker([Longitude, Latitude], {
    //     icon: L.mapquest.icons.marker(),
    //     draggable: false
    //   }).bindPopup(Tooltip).addTo(map);
    //   L.circle([Longitude, Latitude], { radius: 20000 }).addTo(map);
    // }
    addProductToCart({ payload, productImages }) {
        var _a;
        /**
         * payload carries details about the product
         */
        if (payload) {
            this.openCartModal = true;
            const { cost, productId, productName, quantity } = payload;
            this.productAddedToCart = {
                cost,
                productId,
                productName,
                productImage: ((_a = productImages[0]) === null || _a === void 0 ? void 0 : _a.path) || undefined,
                quantity,
            };
            this.productAddedToCartImages = productImages;
        }
    }
    closeCartModal() {
        this.openCartModal = false;
    }
    ngOnDestroy() {
        this.subSink.unsubscribe();
    }
};
BusinessDetailPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _ngrx_store__WEBPACK_IMPORTED_MODULE_5__["Store"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"] }
];
BusinessDetailPage.propDecorators = {
    mapCanvas: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"], args: ["mapCanvas", { read: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] },] }]
};
BusinessDetailPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-business-detail',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./business-detail.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/user/nearby/business-detail/business-detail.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./business-detail.page.scss */ "./src/app/user/nearby/business-detail/business-detail.page.scss")).default]
    })
], BusinessDetailPage);



/***/ })

}]);
//# sourceMappingURL=business-detail-business-detail-module-es2015.js.map