(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["track-order-track-order-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/user/orders/track-order/track-order.page.html":
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/user/orders/track-order/track-order.page.html ***!
  \*****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n  <main id=\"track-order-page\" class=\"container bg-offwhite\">\n    <section id=\"header\" class=\"header-white\">\n      <div class=\"header-actions constrain\">\n        <a [routerLink]=\"['/user', 'orders']\" class=\"back link\">\n          <img class=\"svg\" src=\"assets/images/icons/arrow-left.svg\" width=\"18px\" alt=\"Go back\">\n        </a>\n        <h4>Tracking on Map</h4>\n      </div>\n    </section>\n  \n    <section id=\"map\" class=\"constrain\" #mapSection>\n      <!-- <img src=\"assets/images/others/rider-map.png\" height=\"100px\" alt=\"Restaurant Image\" /> -->\n    </section>\n  \n    <section id=\"track-order-popup\" class=\"popup\" [ngClass]=\"{ 'active': isPopupOpen }\">\n      <!-- This popup has 2 screens. The second one comes up immediately the user rates the driver.\n      The class responsible for the switching is \".rated\" You can switch by adding a \".rated\" class to the popup-content section. It'll look like this: \"popup-content rated\" -->\n      <form [formGroup]=\"addRiderRatingForm\"\n        (ngSubmit)=\"onSubmit()\"\n        class=\"popup-content\" \n        [ngClass]=\"{ 'rated': noOfStarsSelected > 0 }\">\n        <!-- This button closes the popup, it has been implemented. -->\n        <button type=\"button\" class=\"close-popup\" (click)=\"togglePopup()\">\n          <img class=\"svg\" src=\"assets/images/icons/close-popup.svg\" height=\"10px\" alt=\"Close Popup\">\n        </button>\n  \n        <div class=\"tracking-details\">\n          <h5 class=\"title constrain\">Your order will arrive in 5:35</h5>\n  \n          <div class=\"order constrain\" *ngIf=\"(selectedCartById$ | async) as selectedCartById\">\n            <div class=\"image\">\n              <img [src]=\"selectedCartById?.productCheckouts[0]?.productImage | filePathFormatter\" \n                height=\"95px\" \n                *ngIf=\"selectedCartById?.productCheckouts[0]?.productImage\"\n                [alt]=\"selectedCartById?.productCheckouts[0]?.productName\"  />\n                <ng-template #defaultProductImage>\n                  <img src=\"assets/images/restaurants/kfc.png\" \n                    height=\"95px\" \n                    [alt]=\"selectedCartById?.productCheckouts[0].productName\"  />\n                </ng-template>\n            </div>\n            <div class=\"details\">\n              <a class=\"name cursor-pointer\">\n                <h5>{{ selectedCartById?.productCheckouts[0].productName }}</h5>\n              </a>\n              <p class=\"id\">\n                <span>ID:</span> {{ selectedCartById.cartId }}\n              </p>\n              <p class=\"items\">\n                {{ selectedCartById?.productCheckouts?.length || 0 }} Item\n              </p>\n              <p class=\"status waiting\">Waiting</p>\n            </div>\n          </div>\n  \n          <div class=\"rider constrain\" *ngIf=\"(selectedCartRider$  | async) as selectedCartRider\">\n            <div class=\"image\">\n              <img [src]=\"selectedCartRider.person.passportUrl | filePathFormatter\" \n                *ngIf=\"selectedCartRider.person.passportUrl; else defaultRiderImage\"\n                height=\"50px\" \n                alt=\"Rider's Picture\" />\n                <ng-template #defaultRiderImage>\n                  <img src=\"assets/images/humans/rider.png\" \n                    height=\"50px\" \n                    alt=\"Rider's Picture\" />\n                </ng-template>\n            </div>\n            <div class=\"details\">\n              <a class=\"name cursor-pointer\">\n                <h5>\n                  {{ selectedCartRider.person.firstName | capitalize }}\n                  {{ selectedCartRider.person.lastName | capitalize }}\n                </h5>\n              </a>\n              <p class=\"reviews\" *ngIf=\"(riderRating$ | async) as riderRating\">\n                <img class=\"svg icon\" alt=\"star rating icon\" src=\"assets/images/icons/star.svg\">\n                {{ riderRating.rate }}\n                <span>({{ riderRating.reviewerCount }})</span>\n              </p>\n              <div class=\"contact\">\n                <!-- <a href=\"sms:+234{{ selectedCartRider.person.phoneNo }}&body={{ encodeTextMessageData(selectedCartRider.person.firstName) }}\" class=\"sms btn\">\n                  <img class=\"svg icon\" alt=\"sms icon\" src=\"assets/images/icons/message.svg\">\n                </a> -->\n  \n                <a href=\"tel:+234{{ selectedCartRider.person.phoneNo }}\" class=\"call btn\">\n                  <img class=\"svg icon\" alt=\"phone icon\" src=\"assets/images/icons/call.svg\">\n                </a>\n              </div>\n            </div>\n          </div>\n  \n          <div class=\"trip constrain\">\n            <h5 class=\"title\">TRIP</h5>\n            <div class=\"order-timeline\">\n              <div class=\"timeline active\">\n                <div class=\"icon\"></div>\n                <h5>Order confirmation</h5>\n                <h5 class=\"time\">9:15 AM</h5>\n              </div>\n  \n              <div class=\"timeline active\">\n                <div class=\"icon\"></div>\n                <h5>Rider at Restaurant</h5>\n                <h5 class=\"time\">10:05 AM</h5>\n              </div>\n  \n              <div class=\"timeline inactive\">\n                <div class=\"icon\"></div>\n                <h5>Shipping</h5>\n                <h5 class=\"time\">12:35 PM</h5>\n              </div>\n  \n              <div class=\"timeline end\">\n                <div class=\"icon\"></div>\n                <h5>Delivery to your address</h5>\n                <h5 class=\"time\">1:25 PM</h5>\n              </div>\n            </div>\n          </div>\n  \n          <div class=\"rate-rider constrain\" *ngIf=\"(selectedCartRider$  | async)?.person as person\">\n            <h4>Rate your rider</h4>\n            <p>Your feedback will help us improve shipping experience better.</p>\n            <div class=\"rating-stars\">\n              <i (click)=\"rateRider(i, person.firstName)\" \n                *ngFor=\"let star of [0, 1, 2, 3, 4]; index as i\" \n                class=\"fa fa-star cursor-pointer\"\n                [ngClass]=\"{ 'fa-checked': i < noOfStarsSelected }\">\n              </i>\n            </div>\n          </div>\n        </div>\n  \n        <div class=\"rider-rated constrain\">\n          <a (click)=\"resetRiderRating()\" class=\"back link cursor-pointer\">\n            <img class=\"svg\" src=\"assets/images/icons/arrow-left.svg\" width=\"18px\" alt=\"Go back\" />\n          </a>\n          <div class=\"text\">\n            <h4>AWESOME!</h4>\n            <p>You rated {{ riderFirstName }} {{ noOfStarsSelected }} stars</p>\n          </div>\n          <div class=\"rating-stars\">\n            <i *ngFor=\"let star of [0, 1, 2, 3, 4]; index as i\" \n                class=\"fa fa-star cursor-pointer\"\n                [ngClass]=\"{ 'fa-checked': i < noOfStarsSelected }\">\n              </i>\n          </div>\n          \n          <div class=\"compliments\">\n            <div class=\"checkbox\" *ngFor=\"let quality of riderQualities\">\n              <input [id]=\"quality.id\" \n                type=\"checkbox\" \n                [value]=\"quality.key\"\n                [checked]=\"quality.value\"\n                (change)=\"onCheckboxChange($event)\" />\n              <span class=\"check\"></span>\n              <label [for]=\"quality.id\">{{ quality.label }}</label>\n            </div>\n          </div>\n          \n          <textarea [formControlName]=\"'review'\"\n            placeholder=\"Do you want to say something about {{ riderFirstName }}?\" \n            id=\"note\"></textarea>\n          <button type=\"submit\" \n            [disabled]=\"addRiderRatingForm.invalid || (existingCartDeliveryFeedback$ | async)\"\n            class=\"btn btn-primary\" \n            name=\"add-address\">\n            Complete\n          </button>\n        </div>\n      </form>\n    </section>\n\n    <!-- <app-footer-expander></app-footer-expander>\n    <app-rider-bottom-navbar></app-rider-bottom-navbar> -->\n  </main>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/user/orders/track-order/track-order-routing.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/user/orders/track-order/track-order-routing.module.ts ***!
  \***********************************************************************/
/*! exports provided: TrackOrderPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TrackOrderPageRoutingModule", function() { return TrackOrderPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _track_order_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./track-order.page */ "./src/app/user/orders/track-order/track-order.page.ts");




const routes = [
    {
        path: '',
        component: _track_order_page__WEBPACK_IMPORTED_MODULE_3__["TrackOrderPage"]
    }
];
let TrackOrderPageRoutingModule = class TrackOrderPageRoutingModule {
};
TrackOrderPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], TrackOrderPageRoutingModule);



/***/ }),

/***/ "./src/app/user/orders/track-order/track-order.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/user/orders/track-order/track-order.module.ts ***!
  \***************************************************************/
/*! exports provided: TrackOrderPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TrackOrderPageModule", function() { return TrackOrderPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _track_order_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./track-order-routing.module */ "./src/app/user/orders/track-order/track-order-routing.module.ts");
/* harmony import */ var _track_order_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./track-order.page */ "./src/app/user/orders/track-order/track-order.page.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../shared/shared.module */ "./src/app/shared/shared.module.ts");








let TrackOrderPageModule = class TrackOrderPageModule {
};
TrackOrderPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _track_order_routing_module__WEBPACK_IMPORTED_MODULE_5__["TrackOrderPageRoutingModule"],
            _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"]
        ],
        declarations: [_track_order_page__WEBPACK_IMPORTED_MODULE_6__["TrackOrderPage"]]
    })
], TrackOrderPageModule);



/***/ }),

/***/ "./src/app/user/orders/track-order/track-order.page.scss":
/*!***************************************************************!*\
  !*** ./src/app/user/orders/track-order/track-order.page.scss ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".rating-stars i {\n  margin-right: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXNlci9vcmRlcnMvdHJhY2stb3JkZXIvdHJhY2stb3JkZXIucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNJO0VBQ0ksa0JBQUE7QUFBUiIsImZpbGUiOiJzcmMvYXBwL3VzZXIvb3JkZXJzL3RyYWNrLW9yZGVyL3RyYWNrLW9yZGVyLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5yYXRpbmctc3RhcnMge1xuICAgIGkge1xuICAgICAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG4gICAgfVxufSJdfQ== */");

/***/ }),

/***/ "./src/app/user/orders/track-order/track-order.page.ts":
/*!*************************************************************!*\
  !*** ./src/app/user/orders/track-order/track-order.page.ts ***!
  \*************************************************************/
/*! exports provided: RiderQuality, TrackOrderPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RiderQuality", function() { return RiderQuality; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TrackOrderPage", function() { return TrackOrderPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var subsink__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! subsink */ "./node_modules/subsink/dist/es2015/index.js");
/* harmony import */ var _user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../user/store/actions/user.action */ "./src/app/user/store/actions/user.action.ts");
/* harmony import */ var _services_location_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../services/location.service */ "./src/app/services/location.service.ts");
/* harmony import */ var _services_signalr_websocket_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../services/signalr-websocket.service */ "./src/app/services/signalr-websocket.service.ts");
/* harmony import */ var _store_model_user_model__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../store/model/user.model */ "./src/app/user/store/model/user.model.ts");
/* harmony import */ var _utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../../utils/functions/app.functions */ "./src/utils/functions/app.functions.ts");
/* harmony import */ var _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../../../utils/types/app.constant */ "./src/utils/types/app.constant.ts");
var TrackOrderPage_1;













var RiderQuality;
(function (RiderQuality) {
    RiderQuality["IsFast"] = "IsFast";
    RiderQuality["IsEnthusiastic"] = "IsEnthusiastic";
    RiderQuality["IsFriendly"] = "IsFriendly";
})(RiderQuality || (RiderQuality = {}));
let TrackOrderPage = TrackOrderPage_1 = class TrackOrderPage {
    constructor(activatedRoute, router, signalRSrv, locationSrv, 
    // tslint:disable-next-line: variable-name
    _ngZone, store) {
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.signalRSrv = signalRSrv;
        this.locationSrv = locationSrv;
        this._ngZone = _ngZone;
        this.store = store;
        this.isPopupOpen = false;
        this.subSink = new subsink__WEBPACK_IMPORTED_MODULE_6__["SubSink"]();
        this.noOfStarsSelected = 0;
        this.riderQualities = [
            {
                value: false,
                key: RiderQuality.IsEnthusiastic,
                id: "enthusiastic",
                label: "Enthusiastic"
            },
            {
                value: false,
                key: RiderQuality.IsFast,
                id: "fast",
                label: "Fast"
            },
            {
                value: false,
                key: RiderQuality.IsFriendly,
                id: "friendly",
                label: "Friendly"
            }
        ];
        this.subscribeToEvents();
    }
    ngOnInit() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.initForm();
            const { userId } = yield Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_11__["getDataFromLocalStorage"])(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_12__["LocalStorageKey"].ZERO_30_USER);
            this.userId = userId;
            this.subSink.sink =
                this.activatedRoute.data.subscribe((data) => {
                    const { cartLocation: { locationMarker, cart } } = data;
                    if (cart.id) {
                        if (cart.status !== _store_model_user_model__WEBPACK_IMPORTED_MODULE_10__["ProductCartStatus"].TRANSIT) {
                            this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_7__["actions"].CartIsNotInTransitAction());
                            // ? navigate the user away
                            this.router.navigate(["/user", "orders"]);
                        }
                        this.cartId = cart.id;
                        this.selectedCart$ = Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["of"])(cart);
                        if ((locationMarker === null || locationMarker === void 0 ? void 0 : locationMarker.lat) && (locationMarker === null || locationMarker === void 0 ? void 0 : locationMarker.lng)) {
                            this.customerLocation = {
                                Tag: "CUSTOMER",
                                Coordinates: locationMarker,
                                ToolTip: cart.deliveryAddress
                            };
                        }
                    }
                });
            if (this.cartId) {
                this.signalRSrv.joinGroupToViewCartLocation(this.cartId);
                // ? Subscribe to and get the current location of the rider
                this.signalRSrv.findCurrentLocationOfCart();
            }
            // ? Get prior review data to prefill the addRatingForm
            this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_7__["actions"].GetRiderDeliveryRatingInitiatedAction({ payload: { cartId: this.cartId } }));
            this.existingCartDeliveryFeedback$ = this.store.select((data) => data.User.SelectedCartDeliveryFeedback);
            this.subSink.sink =
                this.existingCartDeliveryFeedback$
                    .subscribe((deliveryData) => {
                    if (deliveryData === null || deliveryData === void 0 ? void 0 : deliveryData.cartId) {
                        this.initForm(deliveryData);
                        this.riderQualities = this.remapRiderQualities(deliveryData, this.riderQualities);
                        this.noOfStarsSelected = deliveryData.rating;
                    }
                });
            // ? Get the data for a cart rider
            this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_7__["actions"].GetCartRiderDataInitiatedAction({ payload: { cartId: this.cartId } }));
            this.selectedCartRider$ = this.store.select((data) => data.User.SelectedCartRiderData);
            setTimeout(() => {
                this.loadMap(this.mapSection, this.riderLocation, this.customerLocation);
            }, TrackOrderPage_1.intervalToWaitBeforeLoadingMap);
        });
    }
    // async ngOnInit(): Promise<void> {
    //   this.initForm();
    //   const { userId } = await getDataFromLocalStorage<AuthResponseType>(LocalStorageKey.ZERO_30_USER);
    //   this.userId = userId;
    //   this.subSink.sink =
    //   this.activatedRoute.data.subscribe((data) => {
    //     console.log({ data });
    //     const { cartLocation: { locationMarker, cart } } = data;
    //     if (locationMarker?.lat && locationMarker?.lng) {
    //       this.customerLocation = {
    //         Tag: "CUSTOMER",
    //         Coordinates: locationMarker,
    //         ToolTip: cart.deliveryAddress
    //       };
    //     }
    //   });
    //   this.subSink.sink =
    //   this.activatedRoute
    //       .queryParams
    //       .subscribe((data) => {
    //         this.cartId = data.cartId;
    //       });
    //   if (this.cartId) {
    //     this.signalRSrv.joinGroupToViewCartLocation(this.cartId);
    //     // ? Subscribe to and get the current location of the rider
    //     this.signalRSrv.findCurrentLocationOfCart();
    //   }
    //   // ? Get prior review data to prefill the addRatingForm
    //   this.store.dispatch(UserActions.GetRiderDeliveryRatingInitiatedAction({ payload: { cartId: this.cartId } }));
    //   this.existingCartDeliveryFeedback$ = this.store.select((data) => data.User.SelectedCartDeliveryFeedback);
    //   this.subSink.sink =
    //   this.existingCartDeliveryFeedback$
    //       .subscribe((deliveryData) => {
    //         if (deliveryData?.cartId) {
    //           this.initForm(deliveryData);
    //           this.riderQualities = this.remapRiderQualities(deliveryData, this.riderQualities);
    //           this.noOfStarsSelected = deliveryData.rating;
    //         }
    //       });
    //   // ? Get cart by id
    //   this.store.dispatch(UserActions.GetSelectedCartInitiatedAction({ payload: { cartId: this.cartId } }));
    //   this.selectedCartById$ = this.store.select((data) => data.User.SelectedCart);
    //   this.store.dispatch(RiderActions.GetSelectedCartInitiatedAction({ payload: { cartId: this.cartId } }));
    //   this.selectedCart$ = this.store.select((data) => data.Rider.SelectedCart);
    //   this.selectedCartRider$ = this.store.select((data) => data.User.SelectedCartRiderData);
    //   // ? Get all the rider ratings
    //   this.store.dispatch(RiderActions.GetRiderRatingInitiatedAction({ payload: { userId: this.userId } }));
    //   this.riderRating$ = this.store.select((data) => data.Rider.RiderRating);
    //   // ? Get the cart delivery location
    //   this.subSink.sink =
    //   this.selectedCart$.subscribe((cartData) => {
    //     if (cartData?.id) {
    //       if (cartData?.status !== ProductCartStatus.TRANSIT) {
    //         this.store.dispatch(UserActions.CartIsNotInTransitAction());
    //         // ? navigate the user away
    //         this.router.navigate(["/user", "orders"]);
    //       }
    //       this.store.dispatch(RiderActions.ReverseGeocodingForAddressInitiatedAction({ payload: { address: cartData.deliveryAddress } }));
    //       this.deliveryDestination$ = this.store.select((data) => data.Rider.SelectedAddressToLocation);
    //       // ? Get the data for a cart rider
    //       this.store.dispatch(UserActions.GetCartRiderDataInitiatedAction({ payload: { cartId: this.cartId } }));
    //       this.deliveryDestination$.subscribe((data) => {
    //         if (data?.lat && data?.lng) {
    //           this.customerLocation = {
    //             Tag: "CUSTOMER",
    //             Coordinates: data,
    //             ToolTip: cartData.deliveryAddress
    //           };
    //         }
    //       });
    //       setTimeout(() => {
    //         this.loadMap(this.mapSection, this.riderLocation, this.customerLocation);
    //       }, TrackOrderPage.intervalToWaitBeforeLoadingMap);
    //     }
    //   });
    // }
    loadMap(element, riderLocation, cartLocation) {
        var _a, _b;
        // ? suppressMarkers is optional
        const directionsRenderer = new google.maps.DirectionsRenderer({
            suppressMarkers: true,
        });
        const directionsService = new google.maps.DirectionsService();
        if ((riderLocation === null || riderLocation === void 0 ? void 0 : riderLocation.Coordinates) && (cartLocation === null || cartLocation === void 0 ? void 0 : cartLocation.Coordinates)) {
            this.map = new google.maps.Map(element.nativeElement, {
                center: new google.maps.LatLng((_a = riderLocation === null || riderLocation === void 0 ? void 0 : riderLocation.Coordinates) === null || _a === void 0 ? void 0 : _a.lat, (_b = riderLocation === null || riderLocation === void 0 ? void 0 : riderLocation.Coordinates) === null || _b === void 0 ? void 0 : _b.lng),
                zoom: 10,
            });
            directionsRenderer.setMap(this.map);
            this.locationSrv.calculateAndDisplayRoute(directionsService, directionsRenderer, riderLocation.Coordinates, cartLocation.Coordinates);
            // ? Add map markers for rider
            const riderLocationInfo = `<h3>Rider position</h3>`;
            this.locationSrv.addMapMarker(this.map, riderLocationInfo, riderLocationInfo, riderLocation.Coordinates, "RIDER");
            // ? Add map markers for rider
            const customerLocationInfo = `<h3>My location</h3>`;
            this.locationSrv.addMapMarker(this.map, customerLocationInfo, customerLocationInfo, cartLocation.Coordinates, "CUSTOMER");
        }
        else {
            // TODO try to load the map if the initial load fails
        }
    }
    encodeTextMessageData(riderFirstName) {
        return encodeURI(`Hello ${riderFirstName}, I wanted to confirm that my delivery is in route`);
    }
    onCheckboxChange(e) {
        const checkArray = this.addRiderRatingForm.get("checkArray");
        if (e.target.checked) {
            checkArray.push(new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](e.target.value));
        }
        else {
            let i = 0;
            checkArray.controls.forEach((item) => {
                if (item.value === e.target.value) {
                    checkArray.removeAt(i);
                    return;
                }
                i++;
            });
        }
    }
    onSubmit() {
        if (this.addRiderRatingForm.invalid) {
            return;
        }
        const { review, checkArray } = this.addRiderRatingForm.value;
        const riderQualities = this.setRiderQualities(checkArray);
        if (this.userId) {
            this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_7__["actions"].RateRiderInitiatedAction({ payload: {
                    rating: this.noOfStarsSelected,
                    review,
                    cartId: this.cartId,
                    riderQualityReview: riderQualities,
                }
            }));
        }
    }
    togglePopup() {
        this.isPopupOpen = !this.isPopupOpen;
    }
    resetRiderRating() {
        this.noOfStarsSelected = 0;
    }
    rateRider(index, riderFirstName) {
        this.noOfStarsSelected = index + 1;
        this.riderFirstName = riderFirstName;
    }
    remapRiderQualities(payload, oldData) {
        return oldData.map((data) => {
            if (data.key === RiderQuality.IsFast) {
                return Object.assign(Object.assign({}, data), { value: payload.fast });
            }
            if (data.key === RiderQuality.IsEnthusiastic) {
                return Object.assign(Object.assign({}, data), { value: payload.enthusiastic });
            }
            if (data.key === RiderQuality.IsFriendly) {
                return Object.assign(Object.assign({}, data), { value: payload.friendly });
            }
        });
    }
    subscribeToEvents() {
        // ? Get the current location of the cart
        this.subSink.sink =
            this.signalRSrv.currentCartLocation.subscribe((data) => {
                this._ngZone.run(() => {
                    console.log({ riderCurrentLocation: data });
                    if (data === null || data === void 0 ? void 0 : data.cartId) {
                        const { latitude, longitude } = data;
                        this.riderLocation = {
                            Coordinates: { lat: latitude, lng: longitude },
                            Tag: "RIDER",
                            ToolTip: "Rider's location"
                        };
                        // ? Wait for map to load for applying markers
                        setTimeout(() => {
                            var _a;
                            // ? Update the map marker for rider once they get emitted
                            if ((_a = this.riderLocation) === null || _a === void 0 ? void 0 : _a.Coordinates) {
                                // ? Add map markers for rider
                                const riderLocationInfo = `<h3>Rider position</h3>`;
                                this.locationSrv.addMapMarker(this.map, riderLocationInfo, riderLocationInfo, this.riderLocation.Coordinates, "RIDER");
                            }
                        }, TrackOrderPage_1.intervalToWaitBeforeLoadingMap);
                    }
                });
            });
    }
    initForm(payload) {
        if (payload) {
            this.addRiderRatingForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
                review: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](payload.review),
                checkArray: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormArray"]([]),
            });
        }
        else {
            this.addRiderRatingForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
                review: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null),
                checkArray: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormArray"]([]),
            });
        }
    }
    setRiderQualities(payload) {
        const returnedData = {
            Fast: false,
            Friendly: false,
            Enthusiastic: false
        };
        if (payload.includes(RiderQuality.IsEnthusiastic)) {
            returnedData.Enthusiastic = true;
        }
        if (payload.includes(RiderQuality.IsFast)) {
            returnedData.Fast = true;
        }
        if (payload.includes(RiderQuality.IsFriendly)) {
            returnedData.Friendly = true;
        }
        return returnedData;
    }
    ngOnDestroy() {
        this.subSink.unsubscribe();
    }
};
TrackOrderPage.intervalToWaitBeforeLoadingMap = 3000; // 3 Seconds
TrackOrderPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _services_signalr_websocket_service__WEBPACK_IMPORTED_MODULE_9__["SignalrWebsocketService"] },
    { type: _services_location_service__WEBPACK_IMPORTED_MODULE_8__["LocationService"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"] },
    { type: _ngrx_store__WEBPACK_IMPORTED_MODULE_4__["Store"] }
];
TrackOrderPage.propDecorators = {
    mapSection: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"], args: ["mapSection", { read: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] },] }]
};
TrackOrderPage = TrackOrderPage_1 = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-track-order",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./track-order.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/user/orders/track-order/track-order.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./track-order.page.scss */ "./src/app/user/orders/track-order/track-order.page.scss")).default]
    })
], TrackOrderPage);



/***/ })

}]);
//# sourceMappingURL=track-order-track-order-module-es2015.js.map