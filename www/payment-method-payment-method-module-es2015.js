(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["payment-method-payment-method-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/user/profile/payment-method/payment-method.page.html":
/*!************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/user/profile/payment-method/payment-method.page.html ***!
  \************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n  <main id=\"payment-methods-page\" class=\"container with-bottom-menu bg-offwhite\">\n    <section id=\"header\" class=\"constrain header-white\">\n      <div class=\"header-actions\">\n        <a [routerLink]=\"['/profile', 'edit-profile']\" class=\"back link\">\n          <img class=\"svg\" src=\"assets/images/icons/arrow-left.svg\" width=\"18px\" alt=\"Go back\" />\n        </a>\n        <button class=\"link edit\">Edit</button>\n      </div>\n\n      <h2>Payment Methods</h2>\n    </section>\n\n    <section id=\"payment-methods\">\n      <form action=\"\" class=\"cards constrain\">\n        <!-- If the Visa Card option is selected, add a \".visa\" class to the div below.\n          If it's a Mastercard, add a \".mastercard\" class instead.\n          The classes are in charge of switching the image. (There's no \"src\" attribute. The classes are changing the source from the CSS.)  -->\n        <div class=\"card visa\">\n          <div class=\"image\">\n            <!-- This is the card image that changes when the class changes. -->\n            <img class=\"svg\" width=\"40px\" alt=\"Card Image\">\n          </div>\n          <div class=\"details\">\n            <h5 class=\"number\">**** **** *325</h5>\n            <p class=\"date\">Jun 10, 2020</p>\n          </div>\n          <!-- This is the radio button -->\n          <label class=\"radio\">\n            <input name=\"card\" id=\"mastercard\" type=\"radio\" checked>\n            <span class=\"check\"></span>\n          </label>\n        </div>\n\n        <div class=\"card mastercard\">\n          <div class=\"image\">\n            <img class=\"svg\" width=\"40px\" alt=\"Card Image\">\n          </div>\n          <div class=\"details\">\n            <h5 class=\"number\">**** **** *153</h5>\n            <p class=\"date\">Jun 10, 2020</p>\n          </div>\n          <label class=\"radio\">\n            <input name=\"card\" id=\"mastercard\" type=\"radio\">\n            <span class=\"check\"></span>\n          </label>\n        </div>\n\n        <div class=\"card visa\">\n          <div class=\"image\">\n            <img class=\"svg\" width=\"40px\" alt=\"Card Image\">\n          </div>\n          <div class=\"details\">\n            <h5 class=\"number\">**** **** *894</h5>\n            <p class=\"date\">Jun 10, 2020</p>\n          </div>\n          <label class=\"radio\">\n            <input name=\"card\" id=\"mastercard\" type=\"radio\">\n            <span class=\"check\"></span>\n          </label>\n        </div>\n      </form>\n    </section>\n\n    <section id=\"new-payment-method\">\n\n      <h2 class=\"constrain\">Add Payment Methods</h2>\n      \n      <form action=\"\" id=\"credit-card-info\" class=\"constrain\">\n        <div class=\"form-row\">\n          <!-- If the Visa Card option is selected, add a \".visa\" class to the div below.\n          If it's a Mastercard, add a \".mastercard\" class instead.\n          The classes are in charge of switching the image. (There's no \"src\" attribute. The classes are changing the source from the CSS.)  -->\n          <div class=\"select visa\">\n            <div class=\"image\">\n              <!-- This is the card image that changes when the class changes. -->\n              <img class=\"svg\" height=\"15px\">\n            </div>\n            <select name=\"card-type\" id=\"card-type\">\n              <option value=\"visa\">Visa Card</option>\n              <option value=\"mastercard\">Mastercard Card</option>\n            </select>\n            <div class=\"chevron-icon\">\n              <img class=\"svg\" src=\"assets/images/icons/chevron-down.svg\" height=\"7px\" alt=\"Select\">\n            </div>\n          </div>\n        </div>\n\n        <div class=\"form-row\">\n          <div>\n            <input class=\"card-number\" maxlength=\"19\" name=\"credit-number\" pattern=\"\\d*\" placeholder=\"Card Number\" type=\"tel\" />\n          </div>\n        </div>\n\n        <div class=\"form-row\">\n          <div>\n            <input class=\"card-expiry-date\" maxlength=\"7\" name=\"credit-expires\" pattern=\"\\d*\" placeholder=\"MM / YY\" type=\"tel\" />\n          </div>\n\n          <div>\n            <input class=\"cvv\" maxlength=\"4\" name=\"credit-cvc\" pattern=\"\\d*\" placeholder=\"CVV\" type=\"tel\" />\n          </div>\n        </div>\n\n        <div class=\"form-row\">\n          <button type=\"submit\" class=\"btn btn-primary\" name=\"complete\">Complete</button>\n        </div>\n      </form>\n    </section>\n\n    <app-footer-expander></app-footer-expander>\n    \n    <app-conditional-bottom-navbar\n      *ngIf=\"userDataFromLocalStorage\"\n      [userData]=\"userDataFromLocalStorage\">\n\t  </app-conditional-bottom-navbar>\n\n    <script>\n      // FOR THE TABS\n      function openTab(evt, tabTitle) {\n        // Declare all variables\n        var i, tabcontent, tab;\n\n        // Get all elements with class=\"tabcontent\" and hide them\n        tabcontent = document.getElementsByClassName(\"tab-content\");\n        for (i = 0; i < tabcontent.length; i++) {\n          tabcontent[i].style.display = \"none\";\n        }\n\n        // // Get all elements with class=\"tab\" and remove the class \"active\"\n        tab = document.getElementsByClassName(\"tab\");\n        for (i = 0; i < tab.length; i++) {\n          tab[i].className = tab[i].className.replace(\" active\", \"\");\n        }\n\n        // Show the current tab, and add an \"active\" class to the button that opened the tab\n        document.getElementById(tabTitle).style.display = \"block\";\n        evt.currentTarget.className += \" active\";\n      }\n\n      $(document).ready(function () {\n\n        // Set up formatting for Credit Card fields\n        $('#credit-card-info .card-number').formatCardNumber();\n        $('#credit-card-info .card-expiry-date').formatCardExpiry();\n        $('#credit-card-info .cvv').formatCardCVC();\n\n        $('body.with-bottom-menu').css({\n          'padding-bottom': $('#bottom-menu').height() + \"px\"\n\n        });\n      });\n    </script>\n  </main>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/user/profile/payment-method/payment-method-routing.module.ts":
/*!******************************************************************************!*\
  !*** ./src/app/user/profile/payment-method/payment-method-routing.module.ts ***!
  \******************************************************************************/
/*! exports provided: PaymentMethodPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentMethodPageRoutingModule", function() { return PaymentMethodPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _payment_method_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./payment-method.page */ "./src/app/user/profile/payment-method/payment-method.page.ts");




const routes = [
    {
        path: '',
        component: _payment_method_page__WEBPACK_IMPORTED_MODULE_3__["PaymentMethodPage"]
    }
];
let PaymentMethodPageRoutingModule = class PaymentMethodPageRoutingModule {
};
PaymentMethodPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], PaymentMethodPageRoutingModule);



/***/ }),

/***/ "./src/app/user/profile/payment-method/payment-method.module.ts":
/*!**********************************************************************!*\
  !*** ./src/app/user/profile/payment-method/payment-method.module.ts ***!
  \**********************************************************************/
/*! exports provided: PaymentMethodPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentMethodPageModule", function() { return PaymentMethodPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _payment_method_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./payment-method-routing.module */ "./src/app/user/profile/payment-method/payment-method-routing.module.ts");
/* harmony import */ var _payment_method_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./payment-method.page */ "./src/app/user/profile/payment-method/payment-method.page.ts");
/* harmony import */ var src_app_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/shared/shared.module */ "./src/app/shared/shared.module.ts");








let PaymentMethodPageModule = class PaymentMethodPageModule {
};
PaymentMethodPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _payment_method_routing_module__WEBPACK_IMPORTED_MODULE_5__["PaymentMethodPageRoutingModule"],
            src_app_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"]
        ],
        declarations: [_payment_method_page__WEBPACK_IMPORTED_MODULE_6__["PaymentMethodPage"]]
    })
], PaymentMethodPageModule);



/***/ }),

/***/ "./src/app/user/profile/payment-method/payment-method.page.scss":
/*!**********************************************************************!*\
  !*** ./src/app/user/profile/payment-method/payment-method.page.scss ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXIvcHJvZmlsZS9wYXltZW50LW1ldGhvZC9wYXltZW50LW1ldGhvZC5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/user/profile/payment-method/payment-method.page.ts":
/*!********************************************************************!*\
  !*** ./src/app/user/profile/payment-method/payment-method.page.ts ***!
  \********************************************************************/
/*! exports provided: PaymentMethodPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentMethodPage", function() { return PaymentMethodPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../utils/functions/app.functions */ "./src/utils/functions/app.functions.ts");
/* harmony import */ var _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../utils/types/app.constant */ "./src/utils/types/app.constant.ts");




let PaymentMethodPage = class PaymentMethodPage {
    constructor() { }
    ngOnInit() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            // ? Bring in the user data
            const userDataFromLocalStorage = yield Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_2__["getDataFromLocalStorage"])(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_3__["LocalStorageKey"].ZERO_30_USER);
            const { userId } = userDataFromLocalStorage;
            this.userDataFromLocalStorage = userDataFromLocalStorage;
        });
    }
};
PaymentMethodPage.ctorParameters = () => [];
PaymentMethodPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-payment-method",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./payment-method.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/user/profile/payment-method/payment-method.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./payment-method.page.scss */ "./src/app/user/profile/payment-method/payment-method.page.scss")).default]
    })
], PaymentMethodPage);



/***/ })

}]);
//# sourceMappingURL=payment-method-payment-method-module-es2015.js.map