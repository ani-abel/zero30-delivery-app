(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["make-payment-make-payment-module"], {
    /***/
    "./node_modules/angular4-paystack/__ivy_ngcc__/fesm2015/angular4-paystack.js":
    /*!***********************************************************************************!*\
      !*** ./node_modules/angular4-paystack/__ivy_ngcc__/fesm2015/angular4-paystack.js ***!
      \***********************************************************************************/

    /*! exports provided: Angular4PaystackComponent, Angular4PaystackDirective, Angular4PaystackEmbedComponent, Angular4PaystackModule, ɵa, ɵb */

    /***/
    function node_modulesAngular4Paystack__ivy_ngcc__Fesm2015Angular4PaystackJs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Angular4PaystackComponent", function () {
        return Angular4PaystackComponent;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Angular4PaystackDirective", function () {
        return Angular4PaystackDirective;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Angular4PaystackEmbedComponent", function () {
        return Angular4PaystackEmbedComponent;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Angular4PaystackModule", function () {
        return Angular4PaystackModule;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ɵa", function () {
        return Angular4PaystackService;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ɵb", function () {
        return PUBLIC_KEY_TOKEN;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "../../../node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /**
       * @fileoverview added by tsickle
       * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
       */

      /** @type {?} */


      var _c0 = ["*"];
      var PUBLIC_KEY_TOKEN = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["InjectionToken"]('paystack.publickey');
      /**
       * @fileoverview added by tsickle
       * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
       */

      /**
       * @record
       */

      function MyWindow() {}

      if (false) {}

      var Angular4PaystackService = /*#__PURE__*/function () {
        /**
         * @param {?} token
         */
        function Angular4PaystackService(token) {
          _classCallCheck(this, Angular4PaystackService);

          this.token = token;
        }
        /**
         * @return {?}
         */


        _createClass(Angular4PaystackService, [{
          key: "loadScript",
          value: function loadScript() {
            return new Promise(
            /**
            * @param {?} resolve
            * @return {?}
            */
            function (resolve) {
              if (window.PaystackPop && typeof window.PaystackPop.setup === 'function') {
                resolve();
                return;
              }
              /** @type {?} */


              var script = window.document.createElement('script');
              window.document.head.appendChild(script);
              /** @type {?} */

              var onLoadFunc =
              /**
              * @return {?}
              */
              function onLoadFunc() {
                script.removeEventListener('load', onLoadFunc);
                resolve();
              };

              script.addEventListener('load', onLoadFunc);
              script.setAttribute('src', 'https://js.paystack.co/v1/inline.js');
            });
          }
          /**
           * @param {?} obj
           * @return {?}
           */

        }, {
          key: "checkInput",
          value: function checkInput(obj) {
            if (!obj.key && !this.token) {
              return 'ANGULAR-PAYSTACK: Please insert a your public key';
            }

            if (!obj.email) {
              return 'ANGULAR-PAYSTACK: Paystack email cannot be empty';
            }

            if (!obj.amount) {
              return 'ANGULAR-PAYSTACK: Paystack amount cannot be empty';
            }

            if (!obj.ref) {
              return 'ANGULAR-PAYSTACK: Paystack ref cannot be empty';
            }

            return '';
          }
          /**
           * @param {?} obj
           * @return {?}
           */

        }, {
          key: "getPaystackOptions",
          value: function getPaystackOptions(obj) {
            /** @type {?} */
            var paystackOptions = {
              key: obj.key || this.token,
              email: obj.email,
              amount: obj.amount,
              ref: obj.ref,
              metadata: obj.metadata || {},
              currency: obj.currency || 'NGN',
              plan: obj.plan || '',
              channels: obj.channels,
              quantity: obj.quantity || '',
              subaccount: obj.subaccount || '',
              transaction_charge: obj.transaction_charge || 0,
              // tslint:disable-line
              bearer: obj.bearer || ''
            };
            return paystackOptions;
          }
        }]);

        return Angular4PaystackService;
      }();

      Angular4PaystackService.ɵfac = function Angular4PaystackService_Factory(t) {
        return new (t || Angular4PaystackService)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](PUBLIC_KEY_TOKEN));
      };

      Angular4PaystackService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({
        token: Angular4PaystackService,
        factory: Angular4PaystackService.ɵfac,
        providedIn: 'root'
      });
      /** @nocollapse */

      Angular4PaystackService.ctorParameters = function () {
        return [{
          type: String,
          decorators: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"],
            args: [PUBLIC_KEY_TOKEN]
          }]
        }];
      };
      /** @nocollapse */


      Angular4PaystackService.ngInjectableDef = Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"])({
        factory: function Angular4PaystackService_Factory() {
          return new Angular4PaystackService(Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"])(PUBLIC_KEY_TOKEN));
        },
        token: Angular4PaystackService,
        providedIn: "root"
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](Angular4PaystackService, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"],
          args: [{
            providedIn: 'root'
          }]
        }], function () {
          return [{
            type: String,
            decorators: [{
              type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"],
              args: [PUBLIC_KEY_TOKEN]
            }]
          }];
        }, null);
      })();

      if (false) {}
      /**
       * @fileoverview added by tsickle
       * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
       */

      /**
       * @record
       */


      function MyWindow$1() {}

      if (false) {}

      var Angular4PaystackComponent = /*#__PURE__*/function () {
        /**
         * @param {?} paystackService
         */
        function Angular4PaystackComponent(paystackService) {
          _classCallCheck(this, Angular4PaystackComponent);

          this.paystackService = paystackService;
          this.paymentInit = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
          this.onClose = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"](); // tslint:disable-line
          // tslint:disable-line

          this.callback = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"](); // tslint:disable-line

          this.isPaying = false;
        }
        /**
         * @return {?}
         */


        _createClass(Angular4PaystackComponent, [{
          key: "pay",
          value: function pay() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var errorText, payment;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      /** @type {?} */
                      errorText = '';

                      if (this.paystackOptions && Object.keys(this.paystackOptions).length >= 2) {
                        errorText = this.valdateInput(this.paystackOptions);
                        this.generateOptions(this.paystackOptions);
                      } else {
                        errorText = this.valdateInput(this);
                        this.generateOptions(this);
                      }

                      if (!errorText) {
                        _context.next = 5;
                        break;
                      }

                      console.error(errorText);
                      return _context.abrupt("return", errorText);

                    case 5:
                      _context.next = 7;
                      return this.paystackService.loadScript();

                    case 7:
                      if (!this.isPaying) {
                        _context.next = 9;
                        break;
                      }

                      return _context.abrupt("return");

                    case 9:
                      if (this.paymentInit.observers.length) {
                        this.paymentInit.emit();
                      }
                      /** @type {?} */


                      payment = window.PaystackPop.setup(this._paystackOptions);
                      payment.openIframe();
                      this.isPaying = true;

                    case 13:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
          /**
           * @param {?} obj
           * @return {?}
           */

        }, {
          key: "valdateInput",
          value: function valdateInput(obj) {
            if (!this.callback.observers.length) {
              return 'ANGULAR-PAYSTACK: Insert a callback output like so (callback)=\'PaymentComplete($event)\' to check payment status';
            }

            return this.paystackService.checkInput(obj);
          }
          /**
           * @param {?} obj
           * @return {?}
           */

        }, {
          key: "generateOptions",
          value: function generateOptions(obj) {
            var _this = this;

            this._paystackOptions = this.paystackService.getPaystackOptions(obj);

            this._paystackOptions.onClose =
            /**
            * @return {?}
            */
            function () {
              if (_this.onClose.observers.length) {
                _this.onClose.emit();
              }
            };

            this._paystackOptions.callback =
            /**
            * @param {...?} response
            * @return {?}
            */
            function () {
              var _this$callback;

              (_this$callback = _this.callback).emit.apply(_this$callback, arguments);
            };
          }
        }]);

        return Angular4PaystackComponent;
      }();

      Angular4PaystackComponent.ɵfac = function Angular4PaystackComponent_Factory(t) {
        return new (t || Angular4PaystackComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](Angular4PaystackService));
      };

      Angular4PaystackComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
        type: Angular4PaystackComponent,
        selectors: [["angular4-paystack"]],
        inputs: {
          key: "key",
          email: "email",
          amount: "amount",
          metadata: "metadata",
          ref: "ref",
          currency: "currency",
          plan: "plan",
          quantity: "quantity",
          channels: "channels",
          subaccount: "subaccount",
          transaction_charge: "transaction_charge",
          bearer: "bearer",
          "class": "class",
          style: "style",
          paystackOptions: "paystackOptions"
        },
        outputs: {
          paymentInit: "paymentInit",
          onClose: "onClose",
          callback: "callback"
        },
        ngContentSelectors: _c0,
        decls: 2,
        vars: 2,
        consts: [[3, "ngClass", "ngStyle", "click"]],
        template: function Angular4PaystackComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵprojectionDef"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "button", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function Angular4PaystackComponent_Template_button_click_0_listener() {
              return ctx.pay();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵprojection"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", ctx["class"])("ngStyle", ctx.style);
          }
        },
        directives: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["NgClass"], _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgStyle"]],
        encapsulation: 2
      });
      /** @nocollapse */

      Angular4PaystackComponent.ctorParameters = function () {
        return [{
          type: Angular4PaystackService
        }];
      };

      Angular4PaystackComponent.propDecorators = {
        key: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        email: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        amount: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        metadata: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        ref: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        currency: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        plan: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        quantity: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        channels: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        subaccount: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        transaction_charge: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        bearer: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        "class": [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        style: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        paystackOptions: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        paymentInit: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
        }],
        onClose: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
        }],
        callback: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
        }]
      };
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](Angular4PaystackComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"],
          args: [{
            selector: 'angular4-paystack',
            template: "<button [ngClass]=\"class\" [ngStyle]=\"style\" (click)=\"pay()\"><ng-content></ng-content></button>"
          }]
        }], function () {
          return [{
            type: Angular4PaystackService
          }];
        }, {
          paymentInit: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
          }],
          onClose: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
          }],
          callback: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
          }],
          key: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          email: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          amount: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          metadata: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          ref: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          currency: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          plan: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          quantity: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          channels: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          subaccount: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          transaction_charge: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          bearer: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          "class": [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          style: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          paystackOptions: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }]
        });
      })();

      if (false) {}
      /**
       * @fileoverview added by tsickle
       * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
       */

      /**
       * @record
       */


      function MyWindow$2() {}

      if (false) {}

      var Angular4PaystackDirective = /*#__PURE__*/function () {
        /**
         * @param {?} paystackService
         */
        function Angular4PaystackDirective(paystackService) {
          _classCallCheck(this, Angular4PaystackDirective);

          this.paystackService = paystackService;
          this.paymentInit = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"](); // tslint:disable-line
          // tslint:disable-line

          this.onClose = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"](); // tslint:disable-line
          // tslint:disable-line

          this.callback = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"](); // tslint:disable-line

          this.isPaying = false;
        }
        /**
         * @return {?}
         */


        _createClass(Angular4PaystackDirective, [{
          key: "pay",
          value: function pay() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var errorText, payment;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      /** @type {?} */
                      errorText = '';

                      if (this.paystackOptions && Object.keys(this.paystackOptions).length >= 2) {
                        errorText = this.valdateInput(this.paystackOptions);
                        this.generateOptions(this.paystackOptions);
                      } else {
                        errorText = this.valdateInput(this);
                        this.generateOptions(this);
                      }

                      if (!errorText) {
                        _context2.next = 5;
                        break;
                      }

                      console.error(errorText);
                      return _context2.abrupt("return", errorText);

                    case 5:
                      _context2.next = 7;
                      return this.paystackService.loadScript();

                    case 7:
                      if (!this.isPaying) {
                        _context2.next = 9;
                        break;
                      }

                      return _context2.abrupt("return");

                    case 9:
                      if (this.paymentInit.observers.length) {
                        this.paymentInit.emit();
                      }
                      /** @type {?} */


                      payment = window.PaystackPop.setup(this._paystackOptions);
                      payment.openIframe();
                      this.isPaying = true;

                    case 13:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
          /**
           * @param {?} obj
           * @return {?}
           */

        }, {
          key: "valdateInput",
          value: function valdateInput(obj) {
            if (!this.callback.observers.length) {
              return 'ANGULAR-PAYSTACK: Insert a callback output like so (callback)=\'PaymentComplete($event)\' to check payment status';
            }

            return this.paystackService.checkInput(obj);
          }
          /**
           * @param {?} obj
           * @return {?}
           */

        }, {
          key: "generateOptions",
          value: function generateOptions(obj) {
            var _this2 = this;

            this._paystackOptions = this.paystackService.getPaystackOptions(obj);

            this._paystackOptions.onClose =
            /**
            * @return {?}
            */
            function () {
              if (_this2.onClose.observers.length) {
                _this2.onClose.emit();
              }
            };

            this._paystackOptions.callback =
            /**
            * @param {...?} response
            * @return {?}
            */
            function () {
              var _this2$callback;

              (_this2$callback = _this2.callback).emit.apply(_this2$callback, arguments);
            };
          }
          /**
           * @return {?}
           */

        }, {
          key: "buttonClick",
          value: function buttonClick() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                      this.pay();

                    case 1:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3, this);
            }));
          }
        }]);

        return Angular4PaystackDirective;
      }();

      Angular4PaystackDirective.ɵfac = function Angular4PaystackDirective_Factory(t) {
        return new (t || Angular4PaystackDirective)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](Angular4PaystackService));
      };

      Angular4PaystackDirective.ɵdir = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineDirective"]({
        type: Angular4PaystackDirective,
        selectors: [["", "angular4-paystack", ""]],
        hostBindings: function Angular4PaystackDirective_HostBindings(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function Angular4PaystackDirective_click_HostBindingHandler() {
              return ctx.buttonClick();
            });
          }
        },
        inputs: {
          key: "key",
          email: "email",
          amount: "amount",
          metadata: "metadata",
          ref: "ref",
          currency: "currency",
          plan: "plan",
          quantity: "quantity",
          subaccount: "subaccount",
          channels: "channels",
          transaction_charge: "transaction_charge",
          bearer: "bearer",
          "class": "class",
          style: "style",
          paystackOptions: "paystackOptions"
        },
        outputs: {
          paymentInit: "paymentInit",
          onClose: "onClose",
          callback: "callback"
        }
      });
      /** @nocollapse */

      Angular4PaystackDirective.ctorParameters = function () {
        return [{
          type: Angular4PaystackService
        }];
      };

      Angular4PaystackDirective.propDecorators = {
        key: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        email: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        amount: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        metadata: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        ref: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        currency: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        plan: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        quantity: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        subaccount: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        channels: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        transaction_charge: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        bearer: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        "class": [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        style: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        paystackOptions: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        paymentInit: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
        }],
        onClose: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
        }],
        callback: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
        }],
        buttonClick: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"],
          args: ['click']
        }]
      };
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](Angular4PaystackDirective, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"],
          args: [{
            selector: '[angular4-paystack]'
          }]
        }], function () {
          return [{
            type: Angular4PaystackService
          }];
        }, {
          paymentInit: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
          }],
          onClose: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
          }],
          callback: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
          }],

          /**
           * @return {?}
           */
          buttonClick: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"],
            args: ['click']
          }],
          key: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          email: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          amount: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          metadata: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          ref: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          currency: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          plan: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          quantity: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          subaccount: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          channels: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          transaction_charge: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          bearer: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          "class": [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          style: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          paystackOptions: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }]
        });
      })();

      if (false) {}
      /**
       * @fileoverview added by tsickle
       * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
       */

      /**
       * @record
       */


      function MyWindow$3() {}

      if (false) {}

      var Angular4PaystackEmbedComponent = /*#__PURE__*/function () {
        // tslint:disable-line

        /**
         * @param {?} paystackService
         */
        function Angular4PaystackEmbedComponent(paystackService) {
          _classCallCheck(this, Angular4PaystackEmbedComponent);

          this.paystackService = paystackService;
          this.paymentInit = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
          this.onClose = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"](); // tslint:disable-line
          // tslint:disable-line

          this.callback = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        }
        /**
         * @return {?}
         */


        _createClass(Angular4PaystackEmbedComponent, [{
          key: "pay",
          value: function pay() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
              var errorText, payment;
              return regeneratorRuntime.wrap(function _callee4$(_context4) {
                while (1) {
                  switch (_context4.prev = _context4.next) {
                    case 0:
                      /** @type {?} */
                      errorText = '';

                      if (this.paystackOptions && Object.keys(this.paystackOptions).length >= 2) {
                        errorText = this.valdateInput(this.paystackOptions);
                        this.generateOptions(this.paystackOptions);
                      } else {
                        errorText = this.valdateInput(this);
                        this.generateOptions(this);
                      }

                      if (!errorText) {
                        _context4.next = 5;
                        break;
                      }

                      console.error(errorText);
                      return _context4.abrupt("return", errorText);

                    case 5:
                      _context4.next = 7;
                      return this.paystackService.loadScript();

                    case 7:
                      if (this.paymentInit.observers.length) {
                        this.paymentInit.emit();
                      }
                      /** @type {?} */


                      payment = window.PaystackPop.setup(this._paystackOptions);
                      payment.openIframe();

                    case 10:
                    case "end":
                      return _context4.stop();
                  }
                }
              }, _callee4, this);
            }));
          }
          /**
           * @param {?} obj
           * @return {?}
           */

        }, {
          key: "valdateInput",
          value: function valdateInput(obj) {
            if (!this.callback.observers.length) {
              return 'ANGULAR-PAYSTACK: Insert a callback output like so (callback)=\'PaymentComplete($event)\' to check payment status';
            }

            return this.paystackService.checkInput(obj);
          }
          /**
           * @param {?} obj
           * @return {?}
           */

        }, {
          key: "generateOptions",
          value: function generateOptions(obj) {
            var _this3 = this;

            this._paystackOptions = this.paystackService.getPaystackOptions(obj);

            this._paystackOptions.onClose =
            /**
            * @return {?}
            */
            function () {
              if (_this3.onClose.observers.length) {
                _this3.onClose.emit();
              }
            };

            this._paystackOptions.callback =
            /**
            * @param {...?} response
            * @return {?}
            */
            function () {
              var _this3$callback;

              (_this3$callback = _this3.callback).emit.apply(_this3$callback, arguments);
            };
          }
          /**
           * @return {?}
           */

        }, {
          key: "ngOnInit",
          value: function ngOnInit() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
              return regeneratorRuntime.wrap(function _callee5$(_context5) {
                while (1) {
                  switch (_context5.prev = _context5.next) {
                    case 0:
                      console.error('ANGULAR-PAYSTACK: The paystack embed option is deprecated. Please use the paystack component or directive');
                      this.pay();

                    case 2:
                    case "end":
                      return _context5.stop();
                  }
                }
              }, _callee5, this);
            }));
          }
        }]);

        return Angular4PaystackEmbedComponent;
      }();

      Angular4PaystackEmbedComponent.ɵfac = function Angular4PaystackEmbedComponent_Factory(t) {
        return new (t || Angular4PaystackEmbedComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](Angular4PaystackService));
      };

      Angular4PaystackEmbedComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
        type: Angular4PaystackEmbedComponent,
        selectors: [["angular4-paystack-embed"]],
        inputs: {
          key: "key",
          email: "email",
          amount: "amount",
          metadata: "metadata",
          channels: "channels",
          ref: "ref",
          currency: "currency",
          plan: "plan",
          quantity: "quantity",
          subaccount: "subaccount",
          transaction_charge: "transaction_charge",
          bearer: "bearer",
          paystackOptions: "paystackOptions"
        },
        outputs: {
          paymentInit: "paymentInit",
          onClose: "onClose",
          callback: "callback"
        },
        decls: 1,
        vars: 0,
        consts: [["id", "paystackEmbedContainer"]],
        template: function Angular4PaystackEmbedComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "div", 0);
          }
        },
        encapsulation: 2,
        changeDetection: 0
      });
      /** @nocollapse */

      Angular4PaystackEmbedComponent.ctorParameters = function () {
        return [{
          type: Angular4PaystackService
        }];
      };

      Angular4PaystackEmbedComponent.propDecorators = {
        key: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        email: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        amount: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        metadata: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        channels: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        ref: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        currency: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        plan: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        quantity: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        subaccount: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        transaction_charge: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        bearer: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        paystackOptions: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
        }],
        paymentInit: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
        }],
        onClose: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
        }],
        callback: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
        }]
      };
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](Angular4PaystackEmbedComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"],
          args: [{
            selector: 'angular4-paystack-embed',
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectionStrategy"].OnPush,
            template: "<div id=\"paystackEmbedContainer\"></div>"
          }]
        }], function () {
          return [{
            type: Angular4PaystackService
          }];
        }, {
          paymentInit: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
          }],
          onClose: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
          }],
          callback: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
          }],
          key: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          email: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          amount: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          metadata: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          channels: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          ref: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          currency: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          plan: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          quantity: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          subaccount: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          transaction_charge: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          bearer: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          paystackOptions: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }]
        });
      })();

      if (false) {}
      /**
       * @fileoverview added by tsickle
       * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
       */


      var Angular4PaystackModule = /*#__PURE__*/function () {
        function Angular4PaystackModule() {
          _classCallCheck(this, Angular4PaystackModule);
        }

        _createClass(Angular4PaystackModule, null, [{
          key: "forRoot",

          /**
           * @param {?} token
           * @return {?}
           */
          value: function forRoot(token) {
            return {
              ngModule: Angular4PaystackModule,
              providers: [Angular4PaystackService, {
                provide: PUBLIC_KEY_TOKEN,
                useValue: token
              }]
            };
          }
        }]);

        return Angular4PaystackModule;
      }();

      Angular4PaystackModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineNgModule"]({
        type: Angular4PaystackModule
      });
      Angular4PaystackModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjector"]({
        factory: function Angular4PaystackModule_Factory(t) {
          return new (t || Angular4PaystackModule)();
        },
        providers: [],
        imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsetNgModuleScope"](Angular4PaystackModule, {
          declarations: function declarations() {
            return [Angular4PaystackComponent, Angular4PaystackDirective, Angular4PaystackEmbedComponent];
          },
          imports: function imports() {
            return [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]];
          },
          exports: function exports() {
            return [Angular4PaystackComponent, Angular4PaystackDirective, Angular4PaystackEmbedComponent];
          }
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](Angular4PaystackModule, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"],
          args: [{
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]],
            exports: [Angular4PaystackComponent, Angular4PaystackDirective, Angular4PaystackEmbedComponent],
            declarations: [Angular4PaystackComponent, Angular4PaystackDirective, Angular4PaystackEmbedComponent],
            providers: []
          }]
        }], null, null);
      })();
      /**
       * @fileoverview added by tsickle
       * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
       */

      /**
       * @fileoverview added by tsickle
       * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
       */
      //# sourceMappingURL=angular4-paystack.js.map

      /***/

    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/user/make-payment/make-payment.page.html":
    /*!************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/user/make-payment/make-payment.page.html ***!
      \************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppUserMakePaymentMakePaymentPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n  <main id=\"nearby-page\" class=\"container with-bottom-menu bg-offwhite\">\n    <section class=\"header-area\">\n      <div class=\"header-section\">\n         <a backButton class=\"back link cursor-pointer\">\n          <img class=\"svg\" src=\"assets/images/icons/arrow-left.svg\" width=\"18px\" alt=\"Go back\">\n        </a>\n      </div>\n      <div class=\"header-section\">\n        <div class=\"text\">\n          <h2 class=\"name\">Make Payment</h2>\n        </div>\n      </div>\n    </section>\n\n    <div class=\"display-flex\">\n      <div class=\"flex-item\">\n        <div class=\"shadow-item\" (click)=\"makePayment(paymentMethodType.CASH)\">\n         <i class=\"far fa-money-bill-alt\"></i>\n         <h5>Cash Payment</h5>\n        </div>\n      </div>\n\n      <div class=\"flex-item\" (click)=\"makePayment(paymentMethodType.POS)\">\n        <div class=\"shadow-item\">\n         <i class=\"far fa-money-bill-alt\"></i>\n         <h5>POS Payment</h5>\n        </div>\n      </div>\n\n      <div class=\"flex-item\">\n        <div class=\"shadow-item\" (click)=\"makePayment(paymentMethodType.TRANSFER)\">\n         <i class=\"far fa-money-bill-alt\"></i>\n         <h5>Transfer Payment</h5>\n        </div>\n      </div>\n\n      <div class=\"flex-item\" *ngIf=\"(processedCart$ | async)?.status === productCartStatus.VERIFIED\">\n        <div class=\"shadow-item\" \n          angular4-paystack\n          [email]=\"email\"\n          [amount]=\"((processedCart$ | async).total * 100)\"\n          [ref]=\"(processedCart$ | async)?.paymentInvoice\"\n          [channels]=\"['bank', 'card', 'ussd']\"\n          [class]=\"'btn btn-primary'\"\n          (close)=\"paymentCancel()\"\n          (callback)=\"paymentDone($event)\">\n         <i class=\"fas fa-credit-card\"></i>\n         <h5>Paystack Payment</h5>\n        </div>\n      </div>\n    </div>\n  </main>\n\n  <app-user-bottom-navbar></app-user-bottom-navbar>\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/user/make-payment/make-payment-routing.module.ts":
    /*!******************************************************************!*\
      !*** ./src/app/user/make-payment/make-payment-routing.module.ts ***!
      \******************************************************************/

    /*! exports provided: MakePaymentPageRoutingModule */

    /***/
    function srcAppUserMakePaymentMakePaymentRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MakePaymentPageRoutingModule", function () {
        return MakePaymentPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _make_payment_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./make-payment.page */
      "./src/app/user/make-payment/make-payment.page.ts");

      var routes = [{
        path: '',
        component: _make_payment_page__WEBPACK_IMPORTED_MODULE_3__["MakePaymentPage"]
      }];

      var MakePaymentPageRoutingModule = function MakePaymentPageRoutingModule() {
        _classCallCheck(this, MakePaymentPageRoutingModule);
      };

      MakePaymentPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], MakePaymentPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/user/make-payment/make-payment.module.ts":
    /*!**********************************************************!*\
      !*** ./src/app/user/make-payment/make-payment.module.ts ***!
      \**********************************************************/

    /*! exports provided: MakePaymentPageModule */

    /***/
    function srcAppUserMakePaymentMakePaymentModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MakePaymentPageModule", function () {
        return MakePaymentPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var angular4_paystack__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! angular4-paystack */
      "./node_modules/angular4-paystack/__ivy_ngcc__/fesm2015/angular4-paystack.js");
      /* harmony import */


      var _environments_environment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ../../../environments/environment */
      "./src/environments/environment.ts");
      /* harmony import */


      var _make_payment_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./make-payment-routing.module */
      "./src/app/user/make-payment/make-payment-routing.module.ts");
      /* harmony import */


      var _make_payment_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ./make-payment.page */
      "./src/app/user/make-payment/make-payment.page.ts");
      /* harmony import */


      var _shared_shared_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ../../shared/shared.module */
      "./src/app/shared/shared.module.ts");

      var MakePaymentPageModule = function MakePaymentPageModule() {
        _classCallCheck(this, MakePaymentPageModule);
      };

      MakePaymentPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_9__["SharedModule"], angular4_paystack__WEBPACK_IMPORTED_MODULE_5__["Angular4PaystackModule"].forRoot(_environments_environment__WEBPACK_IMPORTED_MODULE_6__["environment"].paystackPublicTestKey), _make_payment_routing_module__WEBPACK_IMPORTED_MODULE_7__["MakePaymentPageRoutingModule"]],
        declarations: [_make_payment_page__WEBPACK_IMPORTED_MODULE_8__["MakePaymentPage"]]
      })], MakePaymentPageModule);
      /***/
    },

    /***/
    "./src/app/user/make-payment/make-payment.page.scss":
    /*!**********************************************************!*\
      !*** ./src/app/user/make-payment/make-payment.page.scss ***!
      \**********************************************************/

    /*! exports provided: default */

    /***/
    function srcAppUserMakePaymentMakePaymentPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".display-flex {\n  width: 100%;\n  display: block;\n}\n.display-flex .flex-item {\n  width: 50%;\n  display: inline-block;\n  padding: 30px 10px 20px 10px;\n  text-align: center;\n}\n.display-flex .flex-item .shadow-item {\n  cursor: pointer;\n  box-shadow: 0px 2px 10px #e6e8ef;\n  background: #fff;\n  padding: 20px 15px;\n}\n.display-flex .flex-item .shadow-item h5 {\n  margin-top: 15px;\n  font-weight: 400;\n  font-size: 16px;\n  letter-spacing: 2px;\n}\n.display-flex .flex-item .shadow-item:hover,\n.display-flex .flex-item .shadow-item:active {\n  background: #fa6400;\n  color: #fff;\n}\n.display-flex .flex-item .shadow-item:hover i, .display-flex .flex-item .shadow-item:hover h5,\n.display-flex .flex-item .shadow-item:active i,\n.display-flex .flex-item .shadow-item:active h5 {\n  color: #fff;\n}\n.display-flex .flex-item:nth-child(3),\n.display-flex .flex-item:nth-child(4) {\n  padding-top: 0;\n}\ni {\n  font-size: 30px;\n  color: #000;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXNlci9tYWtlLXBheW1lbnQvbWFrZS1wYXltZW50LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFdBQUE7RUFDQSxjQUFBO0FBQ0o7QUFDSTtFQUNJLFVBQUE7RUFDQSxxQkFBQTtFQUNBLDRCQUFBO0VBQ0Esa0JBQUE7QUFDUjtBQUNRO0VBQ0ksZUFBQTtFQUNBLGdDQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtBQUNaO0FBQ1k7RUFDSSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0FBQ2hCO0FBR1E7O0VBRUksbUJBQUE7RUFDQSxXQUFBO0FBRFo7QUFHWTs7O0VBQ0ksV0FBQTtBQUNoQjtBQUlJOztFQUVJLGNBQUE7QUFGUjtBQU1BO0VBQ0ksZUFBQTtFQUNBLFdBQUE7QUFISiIsImZpbGUiOiJzcmMvYXBwL3VzZXIvbWFrZS1wYXltZW50L21ha2UtcGF5bWVudC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZGlzcGxheS1mbGV4IHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBkaXNwbGF5OiBibG9jaztcblxuICAgIC5mbGV4LWl0ZW0ge1xuICAgICAgICB3aWR0aDogNTAlO1xuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgICAgIHBhZGRpbmc6IDMwcHggMTBweCAyMHB4IDEwcHg7XG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcblxuICAgICAgICAuc2hhZG93LWl0ZW0ge1xuICAgICAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xuICAgICAgICAgICAgYm94LXNoYWRvdzogMHB4IDJweCAxMHB4ICNlNmU4ZWY7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiAjZmZmO1xuICAgICAgICAgICAgcGFkZGluZzogMjBweCAxNXB4O1xuXG4gICAgICAgICAgICBoNSB7XG4gICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogMTVweDtcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogNDAwO1xuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgICAgICAgICAgICBsZXR0ZXItc3BhY2luZzogMnB4O1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgLnNoYWRvdy1pdGVtOmhvdmVyLFxuICAgICAgICAuc2hhZG93LWl0ZW06YWN0aXZlIHtcbiAgICAgICAgICAgIGJhY2tncm91bmQ6ICNmYTY0MDA7XG4gICAgICAgICAgICBjb2xvcjogI2ZmZjtcblxuICAgICAgICAgICAgaSxoNSB7XG4gICAgICAgICAgICAgICAgY29sb3I6ICNmZmY7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAuZmxleC1pdGVtOm50aC1jaGlsZCgzKSxcbiAgICAuZmxleC1pdGVtOm50aC1jaGlsZCg0KSB7XG4gICAgICAgIHBhZGRpbmctdG9wOiAwO1xuICAgIH1cbn1cblxuaSB7XG4gICAgZm9udC1zaXplOiAzMHB4O1xuICAgIGNvbG9yOiAjMDAwO1xufSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/user/make-payment/make-payment.page.ts":
    /*!********************************************************!*\
      !*** ./src/app/user/make-payment/make-payment.page.ts ***!
      \********************************************************/

    /*! exports provided: MakePaymentPage */

    /***/
    function srcAppUserMakePaymentMakePaymentPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MakePaymentPage", function () {
        return MakePaymentPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ngrx_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ngrx/store */
      "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");
      /* harmony import */


      var subsink__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! subsink */
      "./node_modules/subsink/dist/es2015/index.js");
      /* harmony import */


      var _store_model_user_model__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../store/model/user.model */
      "./src/app/user/store/model/user.model.ts");
      /* harmony import */


      var _user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ../../user/store/actions/user.action */
      "./src/app/user/store/actions/user.action.ts");
      /* harmony import */


      var _utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../../../utils/functions/app.functions */
      "./src/utils/functions/app.functions.ts");
      /* harmony import */


      var _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ../../../utils/types/app.constant */
      "./src/utils/types/app.constant.ts");

      var MakePaymentPage = /*#__PURE__*/function () {
        function MakePaymentPage(store, activatedRoute, router) {
          _classCallCheck(this, MakePaymentPage);

          this.store = store;
          this.activatedRoute = activatedRoute;
          this.router = router;
          /**
           * Pass Enums to be used on the template
           * http://localhost:8100/#/user/make-payment/1e9ae2f7-d4c8-46d7-baa9-f08e608a9a9c
           */

          this.paymentMethodType = _store_model_user_model__WEBPACK_IMPORTED_MODULE_5__["PaymentMethodType"];
          this.subSink = new subsink__WEBPACK_IMPORTED_MODULE_4__["SubSink"]();
          this.productCartStatus = _store_model_user_model__WEBPACK_IMPORTED_MODULE_5__["ProductCartStatus"];
        }

        _createClass(MakePaymentPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
              var _this4 = this;

              var _yield$Object, username;

              return regeneratorRuntime.wrap(function _callee6$(_context6) {
                while (1) {
                  switch (_context6.prev = _context6.next) {
                    case 0:
                      this.subSink.sink = this.activatedRoute.params.subscribe(function (data) {
                        _this4.cartId = data.cartId;

                        _this4.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_6__["actions"].GetSelectedCartInitiatedAction({
                          payload: {
                            cartId: _this4.cartId
                          }
                        }));
                      }); // this.reference = `ref-${Math.ceil(Math.random() * 10e13)}`;

                      _context6.next = 3;
                      return Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_7__["getDataFromLocalStorage"])(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_8__["LocalStorageKey"].ZERO_30_USER);

                    case 3:
                      _yield$Object = _context6.sent;
                      username = _yield$Object.username;
                      this.email = username;
                      this.processedCart$ = this.store.select(function (data) {
                        return data.User.ProcessedProductCart;
                      });
                      this.subSink.sink = this.processedCart$.subscribe(function (data) {
                        if (!((data === null || data === void 0 ? void 0 : data.cartId) && (data === null || data === void 0 ? void 0 : data.total))) {
                          _this4.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_6__["actions"].GetSelectedCartInitiatedAction({
                            payload: {
                              cartId: _this4.cartId
                            }
                          }));

                          _this4.processedCart$ = _this4.store.select(function (selectedCartData) {
                            return selectedCartData.User.SelectedCart;
                          });
                        }
                      });

                    case 8:
                    case "end":
                      return _context6.stop();
                  }
                }
              }, _callee6, this);
            }));
          }
        }, {
          key: "paymentInit",
          value: function paymentInit() {
            console.log("Payment initialized");
          }
        }, {
          key: "paymentDone",
          value: function paymentDone(ref) {
            var _this5 = this;

            if (this.cartId) {
              this.makePayment(_store_model_user_model__WEBPACK_IMPORTED_MODULE_5__["PaymentMethodType"].PAYSTACK);

              if (ref) {
                var status = ref.status,
                    reference = ref.reference;

                if (status === "success") {
                  // ? Wait for 5 seconds before confirming paystack payment
                  setTimeout(function () {
                    _this5.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_6__["actions"].ConfirmPaystackPaymentInitiatedAction({
                      payload: {
                        refrenceNo: reference
                      }
                    }));

                    _this5.navigateToHome();
                  }, 5000);
                }
              }
            }
          }
        }, {
          key: "paymentCancel",
          value: function paymentCancel() {
            console.log("payment failed");
          }
        }, {
          key: "makePayment",
          value: function makePayment(paymentOption) {
            if (this.cartId) {
              this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_6__["actions"].MakePaymentInitiatedAction({
                payload: {
                  cartId: this.cartId,
                  paymentOption: paymentOption
                }
              }));

              if (paymentOption !== _store_model_user_model__WEBPACK_IMPORTED_MODULE_5__["PaymentMethodType"].PAYSTACK) {
                this.navigateToHome();
              }
            }
          }
        }, {
          key: "navigateToHome",
          value: function navigateToHome() {
            var _this6 = this;

            this.subSink.sink = this.store.select(function (data) {
              return data.User.ActiveMessage;
            }).subscribe(function (data) {
              if (data) {
                _this6.router.navigate(["/user", "discover-items"]);
              }
            });
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            this.subSink.unsubscribe();
          }
        }]);

        return MakePaymentPage;
      }();

      MakePaymentPage.ctorParameters = function () {
        return [{
          type: _ngrx_store__WEBPACK_IMPORTED_MODULE_3__["Store"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }];
      };

      MakePaymentPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-make-payment",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./make-payment.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/user/make-payment/make-payment.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./make-payment.page.scss */
        "./src/app/user/make-payment/make-payment.page.scss"))["default"]]
      })], MakePaymentPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=make-payment-make-payment-module-es5.js.map