(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["rider-live-order-tracking-live-order-tracking-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/rider/live-order-tracking/live-order-tracking.page.html":
/*!***************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/rider/live-order-tracking/live-order-tracking.page.html ***!
  \***************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-title>live-order-tracking</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>\n");

/***/ }),

/***/ "./src/app/rider/live-order-tracking/live-order-tracking-routing.module.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/rider/live-order-tracking/live-order-tracking-routing.module.ts ***!
  \*********************************************************************************/
/*! exports provided: LiveOrderTrackingPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LiveOrderTrackingPageRoutingModule", function() { return LiveOrderTrackingPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _live_order_tracking_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./live-order-tracking.page */ "./src/app/rider/live-order-tracking/live-order-tracking.page.ts");




const routes = [
    {
        path: '',
        component: _live_order_tracking_page__WEBPACK_IMPORTED_MODULE_3__["LiveOrderTrackingPage"]
    }
];
let LiveOrderTrackingPageRoutingModule = class LiveOrderTrackingPageRoutingModule {
};
LiveOrderTrackingPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], LiveOrderTrackingPageRoutingModule);



/***/ }),

/***/ "./src/app/rider/live-order-tracking/live-order-tracking.module.ts":
/*!*************************************************************************!*\
  !*** ./src/app/rider/live-order-tracking/live-order-tracking.module.ts ***!
  \*************************************************************************/
/*! exports provided: LiveOrderTrackingPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LiveOrderTrackingPageModule", function() { return LiveOrderTrackingPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _live_order_tracking_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./live-order-tracking-routing.module */ "./src/app/rider/live-order-tracking/live-order-tracking-routing.module.ts");
/* harmony import */ var _live_order_tracking_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./live-order-tracking.page */ "./src/app/rider/live-order-tracking/live-order-tracking.page.ts");







let LiveOrderTrackingPageModule = class LiveOrderTrackingPageModule {
};
LiveOrderTrackingPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _live_order_tracking_routing_module__WEBPACK_IMPORTED_MODULE_5__["LiveOrderTrackingPageRoutingModule"]
        ],
        declarations: [_live_order_tracking_page__WEBPACK_IMPORTED_MODULE_6__["LiveOrderTrackingPage"]]
    })
], LiveOrderTrackingPageModule);



/***/ }),

/***/ "./src/app/rider/live-order-tracking/live-order-tracking.page.scss":
/*!*************************************************************************!*\
  !*** ./src/app/rider/live-order-tracking/live-order-tracking.page.scss ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3JpZGVyL2xpdmUtb3JkZXItdHJhY2tpbmcvbGl2ZS1vcmRlci10cmFja2luZy5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/rider/live-order-tracking/live-order-tracking.page.ts":
/*!***********************************************************************!*\
  !*** ./src/app/rider/live-order-tracking/live-order-tracking.page.ts ***!
  \***********************************************************************/
/*! exports provided: LiveOrderTrackingPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LiveOrderTrackingPage", function() { return LiveOrderTrackingPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let LiveOrderTrackingPage = class LiveOrderTrackingPage {
    constructor() { }
    ngOnInit() {
    }
};
LiveOrderTrackingPage.ctorParameters = () => [];
LiveOrderTrackingPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-live-order-tracking',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./live-order-tracking.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/rider/live-order-tracking/live-order-tracking.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./live-order-tracking.page.scss */ "./src/app/rider/live-order-tracking/live-order-tracking.page.scss")).default]
    })
], LiveOrderTrackingPage);



/***/ })

}]);
//# sourceMappingURL=rider-live-order-tracking-live-order-tracking-module-es2015.js.map