(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["login-login-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/auth/login/login.page.html":
/*!**********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/auth/login/login.page.html ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n    <main id=\"login-page\" class=\"bg-offwhite container\">\n        <section id=\"login-logo\" class=\"constrain\">\n            <img class=\"svg\" src=\"assets/images/logo.png\" height=\"150px\" alt=\"Zero-30 Logo\">\n        </section>\n        <section id=\"login-form\" class=\"constrain\">\n            <form [formGroup]=\"loginForm\" (ngSubmit)=\"onSubmit()\">\n                <!-- This is the error text, it displays form errors. -->\n                <p class=\"error\" *ngIf=\"(activeError$ | async)\">\n                    Oh no! Your account or password is incorrect, please check again.\n                </p>\n                \n                <div class=\"form-row\">\n                    <div>\n                        <input type=\"text\" \n                            placeholder=\"Username\" \n                            [formControlName]=\"'Username'\" />\n                        <p class=\"margin-xs\" \n                            *ngIf=\"loginForm.get('Username').invalid && loginForm.get('Username').touched\">\n                            <span class=\"error-message-block\" \n                                *ngIf=\"loginForm.get('Username').errors['required']\">\n                                This field is required\n                            </span>\n                        </p>\n                    </div>\n                </div>\n      \n                <div class=\"form-row\">\n                    <div class=\"input-password\">\n                        <input type=\"password\" \n                            placeholder=\"Password\"\n                            #passwordField\n                            [formControlName]=\"'Password'\" />\n                            <i class=\"far fa-eye\" \n                                id=\"togglePassword\" \n                                (click)=\"revealPassword(passwordField)\"></i>\n                        <!-- <p class=\"margin-xs\" \n                            *ngIf=\"loginForm.get('Password').invalid && loginForm.get('Password').touched\">\n                            <span class=\"error-message-block\" \n                                *ngIf=\"loginForm.get('Password').errors['required']\">\n                                This field is required\n                            </span>\n                        </p> -->\n                    </div>\n                </div>\n                <p class=\"margin-xs mt-0\" \n                    style=\"width: 100%; display:block;\"\n                        *ngIf=\"loginForm.get('Password').invalid && loginForm.get('Password').touched\">\n                        <span class=\"error-message-block\" \n                            *ngIf=\"loginForm.get('Password').errors['required']\">\n                                This field is required\n                        </span>\n                        </p>\n      \n                <div class=\"form-row reset-password-link\">\n                    <a [routerLink]=\"['/auth', 'forgot-password']\" class=\"link link-secondary\">Forgot password!</a>\n                </div>\n      \n                <div></div>\n      \n                <div class=\"form-row\">\n                    <button type=\"submit\" class=\"btn btn-primary\" name=\"sign-in\">Sign In</button>\n                </div>\n      \n                <!-- Facebook sign in button -->\n                <div class=\"form-row\">\n                    <button class=\"btn btn-facebook\" (click)=\"facebookLogin()\">Sign In with Facebook</button>\n                </div>\n            </form>\n        </section>\n      \n        <section id=\"create-account-link\" class=\"constrain\">\n            <p>You don’t have an account? <a [routerLink]=\"['/auth', 'sign-up']\" class=\"link link-primary\">Sign up</a></p>\n        </section>\n      </main>\n</ion-content>");

/***/ }),

/***/ "./src/app/auth/login/login-routing.module.ts":
/*!****************************************************!*\
  !*** ./src/app/auth/login/login-routing.module.ts ***!
  \****************************************************/
/*! exports provided: LoginPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageRoutingModule", function() { return LoginPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./login.page */ "./src/app/auth/login/login.page.ts");




const routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_3__["LoginPage"]
    }
];
let LoginPageRoutingModule = class LoginPageRoutingModule {
};
LoginPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], LoginPageRoutingModule);



/***/ }),

/***/ "./src/app/auth/login/login.module.ts":
/*!********************************************!*\
  !*** ./src/app/auth/login/login.module.ts ***!
  \********************************************/
/*! exports provided: LoginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _login_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./login-routing.module */ "./src/app/auth/login/login-routing.module.ts");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login.page */ "./src/app/auth/login/login.page.ts");
/* harmony import */ var src_app_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/shared/shared.module */ "./src/app/shared/shared.module.ts");








let LoginPageModule = class LoginPageModule {
};
LoginPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            src_app_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"],
            _login_routing_module__WEBPACK_IMPORTED_MODULE_5__["LoginPageRoutingModule"]
        ],
        declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]]
    })
], LoginPageModule);



/***/ }),

/***/ "./src/app/auth/login/login.page.scss":
/*!********************************************!*\
  !*** ./src/app/auth/login/login.page.scss ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("i {\n  position: absolute;\n  bottom: 7px;\n  right: 5px;\n  width: 24px;\n  height: 24px;\n  color: #000;\n}\n\n.input-password {\n  position: relative;\n  margin: 0;\n  direction: ltr;\n  width: 100%;\n}\n\n.input-password input[type=password]:focus i {\n  position: absolute;\n  bottom: 7px;\n  right: 5px;\n  width: 24px;\n  height: 24px;\n}\n\n.mt-0 {\n  margin-top: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXV0aC9sb2dpbi9sb2dpbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxjQUFBO0VBQ0EsV0FBQTtBQUNKOztBQUdJO0VBQ0ksa0JBQUE7RUFDQSxXQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FBQVI7O0FBSUE7RUFDSSxhQUFBO0FBREoiLCJmaWxlIjoic3JjL2FwcC9hdXRoL2xvZ2luL2xvZ2luLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImkge1xuICAgIHBvc2l0aW9uOmFic29sdXRlO1xuICAgIGJvdHRvbTo3cHg7XG4gICAgcmlnaHQ6NXB4O1xuICAgIHdpZHRoOjI0cHg7XG4gICAgaGVpZ2h0OjI0cHg7XG4gICAgY29sb3I6ICMwMDA7XG59XG5cbi5pbnB1dC1wYXNzd29yZCB7XG4gICAgcG9zaXRpb246cmVsYXRpdmU7XG4gICAgbWFyZ2luOiAwO1xuICAgIGRpcmVjdGlvbjogbHRyO1xuICAgIHdpZHRoOiAxMDAlO1xufVxuXG4uaW5wdXQtcGFzc3dvcmQge1xuICAgIGlucHV0W3R5cGU9XCJwYXNzd29yZFwiXTpmb2N1cyBpIHtcbiAgICAgICAgcG9zaXRpb246YWJzb2x1dGU7XG4gICAgICAgIGJvdHRvbTo3cHg7XG4gICAgICAgIHJpZ2h0OjVweDtcbiAgICAgICAgd2lkdGg6MjRweDtcbiAgICAgICAgaGVpZ2h0OjI0cHg7XG4gICAgfVxufVxuXG4ubXQtMCB7XG4gICAgbWFyZ2luLXRvcDogMDtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/auth/login/login.page.ts":
/*!******************************************!*\
  !*** ./src/app/auth/login/login.page.ts ***!
  \******************************************/
/*! exports provided: LoginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPage", function() { return LoginPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");
/* harmony import */ var _store_actions_auth_action__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../store/actions/auth.action */ "./src/app/auth/store/actions/auth.action.ts");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../auth.service */ "./src/app/auth/auth.service.ts");
/* harmony import */ var _user_store_effects_user_http_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../user/store/effects/user-http.service */ "./src/app/user/store/effects/user-http.service.ts");







let LoginPage = class LoginPage {
    constructor(store, authSrv, userSrv) {
        this.store = store;
        this.authSrv = authSrv;
        this.userSrv = userSrv;
        this.authSrv.setupFbLogin();
    }
    facebookLogin() {
        var _a;
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const facebookData = yield this.authSrv.facebookLogin();
            if (facebookData === null || facebookData === void 0 ? void 0 : facebookData.id) {
                const [firstName, lastName] = facebookData.name.split(" ");
                const { id: facebookId, birthday: dob } = facebookData;
                const imageUrl = (_a = facebookData.picture.data) === null || _a === void 0 ? void 0 : _a.url;
                const requestPayload = {
                    facebookId,
                    firstName,
                    lastName,
                    dob: new Date(),
                    imageUrl
                };
                this.store.dispatch(_store_actions_auth_action__WEBPACK_IMPORTED_MODULE_4__["actions"].FacebookLoginInitiatedAction({ payload: requestPayload }));
            }
        });
    }
    ionViewWillEnter() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield this.authSrv.loginAsync();
        });
    }
    ngOnInit() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.initForm();
            this.activeError$ = this.store.select((data) => data.Auth.ActiveError);
            yield this.authSrv.loginAsync();
        });
    }
    initForm() {
        this.loginForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            Username: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email
            ])),
            Password: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required
            ]))
        });
    }
    revealPassword(passwordField) {
        const passwordHtmlField = passwordField;
        if (passwordHtmlField.type === "password") {
            passwordHtmlField.type = "text";
        }
        else {
            passwordHtmlField.type = "password";
        }
    }
    onSubmit() {
        if (this.loginForm.invalid) {
            return;
        }
        const { Username: username, Password: password } = this.loginForm.value;
        this.store.dispatch(_store_actions_auth_action__WEBPACK_IMPORTED_MODULE_4__["actions"].LoginInitiatedAction({ payload: { username, password } }));
    }
};
LoginPage.ctorParameters = () => [
    { type: _ngrx_store__WEBPACK_IMPORTED_MODULE_3__["Store"] },
    { type: _auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"] },
    { type: _user_store_effects_user_http_service__WEBPACK_IMPORTED_MODULE_6__["UserHttpService"] }
];
LoginPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./login.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/auth/login/login.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./login.page.scss */ "./src/app/auth/login/login.page.scss")).default]
    })
], LoginPage);



/***/ })

}]);
//# sourceMappingURL=login-login-module-es2015.js.map