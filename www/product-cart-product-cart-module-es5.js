(function () {
  function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

  function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

  function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["product-cart-product-cart-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/user/product-cart/product-cart.page.html":
    /*!************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/user/product-cart/product-cart.page.html ***!
      \************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppUserProductCartProductCartPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n  <main id=\"business-details-page\" class=\"bg-offwhite container with-bottom-menu bookmarked\">\n    <section id=\"header\" class=\"constrain header-transparent-image\"\n    style=\"background: linear-gradient(0deg, rgba(34, 43, 69, 0.7), rgba(34, 43, 69, 0.7)), url(assets/images/banner/banner2.png);\">\n\n    \n    <div class=\"header-actions\">\n      <a backButton class=\"back link cursor-pointer\">\n        <img class=\"svg\" src=\"assets/images/icons/arrow-left.svg\" width=\"18px\" alt=\"Go back\">\n      </a>\n    </div>\n\n    <!-- This shows the name and address of the restaurant -->\n      <div class=\"text\">\n        <h2 class=\"name\">My Cart</h2>\n        <p class=\"address\" *ngIf=\"(productCartCount$ | async) as productCartCount\">\n          <i class=\"fa fa-shopping-cart\"></i>\n          <span *ngIf=\"productCartCount > 1; else defaultProductCount\">\n            {{ productCartCount }} Items\n          </span>\n          <ng-template #defaultProductCount>\n            <span>{{ productCartCount }} Item</span>\n          </ng-template>\n        </p>\n      </div>\n    </section>\n\n    <!-- Tab content -->\n    <!-- This is the Products tab -->\n    <section id=\"products\" class=\"tab-content active\" \n      *ngIf=\"(productCart$ | async) as productCart\">\n      <div id=\"product-categories\">\n        <div class=\"section-title constrain\">\n          <h5>My Cart <span>{{ (productCartCount$ | async) || 0 }} items</span></h5>\n        </div>\n  \n        <!-- \n          There's a \".bookmarked\" class on the products that are bookmarked by the user, Once the .bookmarked tag is added as a class it styles the bookmark icon.\n        -->\n        <div class=\"product constrain bookmarked\" \n          *ngFor=\"let product of productCart.productCheckouts; index as i\">\n          <a class=\"image cursor-pointer\">`\n            <img [src]=\"product?.productImage | filePathFormatter\" \n              [alt]=\"product?.productName\"\n              height=\"120px\"\n              *ngIf=\"product?.productImage; else defaultImage\" />\n              <ng-template #defaultImage>\n                <img src=\"assets/images/products/4.png\" \n                [alt]=\"product?.productName\"\n                  height=\"120px\" />\n              </ng-template>\n          </a>\n  \n          <div class=\"details\">\n            <a class=\"name cursor-pointer\">\n              <h5>{{ product.productName | capitalize }}</h5>\n            </a>\n            <!-- <button class=\"btn bookmark\">\n              <img class=\"svg\" src=\"assets/images/icons/bookmark.svg\" height=\"18px\" alt=\"Bookmark\">\n            </button> -->\n  \n            <p class=\"description\" *ngIf=\"(cartItemDetails$ | async) as cartItemDetails\">\n              {{ cartItemDetails[i]?.productModel?.description | capitalize }}\n            </p>\n  \n            <h5 class=\"price\">{{ product.cost | currency }}</h5>\n  \n            <p class=\"bag\">\n              <img class=\"svg icon\" alt=\"Bag Icon\" src=\"assets/images/icons/bag.svg\">\n              <span>99+</span>\n            </p>\n  \n            <span class=\"line\"></span>\n  \n            <p class=\"like\">\n              <img class=\"svg icon\" alt=\"\" src=\"assets/images/icons/like.svg\">\n              <span>26</span>\n            </p>\n  \n            <button class=\"btn add-to-cart\" (click)=\"removeProductFromCart(product.productId)\">\n              <img class=\"svg\" src=\"assets/images/icons/minus.svg\" height=\"18px\" alt=\"Add to Cart\">\n            </button>\n          </div>\n        </div>\n      </div>\n    </section>\n\n    <section *ngIf=\"!isCheckoutBtnHidden\">\n      <div class=\"constrain\" \n      *ngIf=\"(productCart$ | async) && !(processedCart$ | async)\">\n      <button type=\"button\" \n        *ngIf=\"(productCart$ | async) as productCart\"\n        (click)=\"cartCheckout(productCart)\" \n        class=\"btn btn-primary btn-block\">\n        <i class=\"fa fa-check\" aria-hidden=\"true\"></i>\n        Checkout\n      </button>\n      </div>\n    </section>\n\n    <div class=\"constrain\" \n      *ngIf=\"(processedCart$ | async) as processedCart\">\n      <ion-item-group>\n        <ion-item-divider>\n          <ion-label color=\"danger\">\n            Payment Summary\n          </ion-label>\n        </ion-item-divider>\n      \n        <ion-item>\n          <ion-label>Cart Status</ion-label>\n          <ion-badge color=\"medium\" slot=\"end\">\n            {{ processedCart.status }}\n          </ion-badge>\n        </ion-item>\n        <ion-item>\n          <ion-label>VAT</ion-label>\n          <ion-badge color=\"medium\" slot=\"end\">\n            {{ processedCart.vat || 0 | currency }}\n          </ion-badge>\n        </ion-item>\n        <ion-item>\n          <ion-label>Delivery Fee</ion-label>\n          <ion-badge color=\"medium\" slot=\"end\">\n            {{ processedCart?.deliveryCost || 0 | currency }}\n          </ion-badge>\n        </ion-item>\n        <ion-item lines=\"none\">\n          <ion-label>Total</ion-label>\n          <ion-badge color=\"warning\" slot=\"end\">\n            {{ processedCart?.total || 0 | currency }}\n          </ion-badge>\n        </ion-item>\n    \n        <br />\n        <ion-grid>\n          <ion-row>\n            <ion-col size=\"12\" class=\"ion-align-self-start text-right\">\n              <button class=\"btn btn-primary\" \n                [disabled]=\"processedCart.status !== productCartStatus.VERIFIED || processedCart.status === productCartStatus.CANCELLED\"\n                (click)=\"makePayment(processedCart.cartId)\">\n                <i class=\"fa fa-credit-card\"></i>\n                Pay\n              </button>\n            </ion-col>\n          </ion-row>\n          </ion-grid>\n      </ion-item-group>\n    </div>\n  </main>\n\n  <app-footer-expander></app-footer-expander>\n\n  <app-user-bottom-navbar></app-user-bottom-navbar>\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/user/product-cart/product-cart-routing.module.ts":
    /*!******************************************************************!*\
      !*** ./src/app/user/product-cart/product-cart-routing.module.ts ***!
      \******************************************************************/

    /*! exports provided: ProductCartPageRoutingModule */

    /***/
    function srcAppUserProductCartProductCartRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ProductCartPageRoutingModule", function () {
        return ProductCartPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _product_cart_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./product-cart.page */
      "./src/app/user/product-cart/product-cart.page.ts");

      var routes = [{
        path: '',
        component: _product_cart_page__WEBPACK_IMPORTED_MODULE_3__["ProductCartPage"]
      }];

      var ProductCartPageRoutingModule = function ProductCartPageRoutingModule() {
        _classCallCheck(this, ProductCartPageRoutingModule);
      };

      ProductCartPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], ProductCartPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/user/product-cart/product-cart.module.ts":
    /*!**********************************************************!*\
      !*** ./src/app/user/product-cart/product-cart.module.ts ***!
      \**********************************************************/

    /*! exports provided: ProductCartPageModule */

    /***/
    function srcAppUserProductCartProductCartModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ProductCartPageModule", function () {
        return ProductCartPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _product_cart_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./product-cart-routing.module */
      "./src/app/user/product-cart/product-cart-routing.module.ts");
      /* harmony import */


      var _product_cart_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./product-cart.page */
      "./src/app/user/product-cart/product-cart.page.ts");
      /* harmony import */


      var _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../../shared/shared.module */
      "./src/app/shared/shared.module.ts");

      var ProductCartPageModule = function ProductCartPageModule() {
        _classCallCheck(this, ProductCartPageModule);
      };

      ProductCartPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"], _product_cart_routing_module__WEBPACK_IMPORTED_MODULE_5__["ProductCartPageRoutingModule"]],
        declarations: [_product_cart_page__WEBPACK_IMPORTED_MODULE_6__["ProductCartPage"]]
      })], ProductCartPageModule);
      /***/
    },

    /***/
    "./src/app/user/product-cart/product-cart.page.scss":
    /*!**********************************************************!*\
      !*** ./src/app/user/product-cart/product-cart.page.scss ***!
      \**********************************************************/

    /*! exports provided: default */

    /***/
    function srcAppUserProductCartProductCartPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "#business-details-page #products {\n  grid-gap: 5px;\n  padding: 0;\n  margin: 30px 0;\n  width: 100%;\n  overflow: hidden;\n}\n\n#business-details-page #products .products {\n  padding: 0;\n  margin-bottom: 30px;\n}\n\n#business-details-page #products .products .product {\n  display: grid;\n  text-align: left;\n  text-decoration: none;\n  padding: 15px 7.5px;\n}\n\n#business-details-page #products .products .product:first-child {\n  padding-left: 15px;\n}\n\n#business-details-page #products .products .product:last-child {\n  padding-right: 15px;\n}\n\n#products .products .product .details {\n  display: grid;\n  grid-gap: 5px;\n  text-align: left;\n  text-decoration: none;\n  background: #ffffff;\n  box-shadow: 0px 2px 10px #e6e8ef;\n  border-radius: 4px;\n}\n\n#products .products .product .details .image {\n  display: grid;\n  width: 120px;\n  height: 120px;\n  border-radius: 4px 4px 0 0;\n  overflow: hidden;\n  position: relative;\n}\n\n#products .products .product .details .image img {\n  width: 100%;\n  height: 100%;\n  -o-object-fit: cover;\n  object-fit: cover;\n}\n\n#products .products .product .details .image .bookmark {\n  position: absolute;\n  top: 0px;\n  right: 0px;\n  padding: 10px;\n  border-radius: 0;\n}\n\n#products .products .product .details .image .bookmark img,\n#products .products .product .details .image .bookmark svg {\n  width: auto;\n  height: 15px;\n  border-radius: 0;\n}\n\n#products .products .product .details .image .bookmark img path,\n#products .products .product .details .image .bookmark svg path {\n  stroke: #ffffff;\n}\n\n#products .products .product .details .image .bookmark:active {\n  transform: scale(0.95);\n}\n\n#products .products .product .details .text {\n  padding: 10px;\n  max-width: 120px;\n  display: grid;\n  grid-gap: 10px;\n}\n\n#products .products .product .details .text .name {\n  color: #222b45;\n  font-weight: 600;\n  font-size: 15px;\n  line-height: 18px;\n  margin-bottom: 0;\n}\n\n#products .products .product .details .text p {\n  display: grid;\n  grid-auto-flow: column;\n  grid-gap: 5px;\n  grid-template-columns: 1fr -webkit-max-content;\n  grid-template-columns: 1fr max-content;\n  align-items: center;\n}\n\n#products .products .product .details .text p span {\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: nowrap;\n  font-size: 15px;\n  line-height: 15px;\n  font-weight: 600;\n  color: #fa6400;\n}\n\n#business-details-page #products .products .product .details .text p .add-to-cart {\n  padding: 0;\n}\n\n#products .products .product .details .text p .add-to-cart img,\n#products .products .product .details .text p .add-to-cart svg {\n  width: 18px;\n  height: auto;\n}\n\n#products .products .product .details .text p .add-to-cart img path,\n#products .products .product .details .text p .add-to-cart svg path {\n  fill: #fa6400;\n}\n\n#products .products .product.bookmarked .bookmark svg path {\n  fill: #fa6400;\n  stroke: #fa6400 !important;\n}\n\n#products #product-categories {\n  background: #ffffff;\n  box-shadow: 0px 2px 10px #e6e8ef;\n  display: grid;\n  margin-bottom: 30px;\n}\n\n#products #product-categories:last-child {\n  margin-bottom: 0;\n}\n\n#products #product-categories .section-title {\n  padding: 15px;\n}\n\n#products #product-categories .section-title h5 {\n  font-size: 15px;\n  line-height: 20px;\n  font-weight: 600;\n  color: #222b45;\n}\n\n#products #product-categories .section-title h5 span {\n  color: #8f9bb3;\n  font-weight: normal;\n}\n\n.constrain {\n  margin: 0 auto;\n  max-width: 1000px;\n  width: 100%;\n  padding: 15px;\n}\n\n.big-cart-btn .items {\n  grid-row: 3;\n  grid-column: 1;\n  grid-area: items;\n  font-size: 12px;\n  line-height: 12px;\n  color: #ffffff;\n  text-transform: uppercase;\n}\n\n.big-cart-btn .price {\n  grid-row: 2;\n  grid-column: 1;\n  grid-area: price;\n  font-size: 17px;\n  line-height: 17px;\n  font-weight: 600;\n  color: #ffffff;\n}\n\n.big-cart-btn .text {\n  grid-row: 1;\n  -ms-grid-row-span: 2;\n  grid-column: 2;\n  grid-area: text;\n  font-size: 17px;\n  line-height: 17px;\n  color: #ffffff;\n  font-weight: 600;\n  justify-self: end;\n}\n\n.big-cart-btn {\n  display: grid;\n  grid-template-columns: auto 1fr;\n  grid-template-rows: auto;\n  grid-gap: 15px;\n  align-items: center;\n  width: 100%;\n  padding: 20px 30px;\n  grid-template-areas: \"items text\" \"price text\";\n}\n\ni.fa {\n  color: #fff;\n}\n\n.add-to-cart img path,\n.add-to-cart svg path {\n  fill: #fa6400;\n  stroke: #fa6400 !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXNlci9wcm9kdWN0LWNhcnQvcHJvZHVjdC1jYXJ0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGFBQUE7RUFDQSxVQUFBO0VBQ0EsY0FBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtBQUNKOztBQUVFO0VBQ0UsVUFBQTtFQUNBLG1CQUFBO0FBQ0o7O0FBRUU7RUFFRSxhQUFBO0VBQ0EsZ0JBQUE7RUFDQSxxQkFBQTtFQUNBLG1CQUFBO0FBQ0o7O0FBT0U7RUFDRSxrQkFBQTtBQUpKOztBQU9FO0VBQ0UsbUJBQUE7QUFKSjs7QUFPRTtFQUVFLGFBQUE7RUFDQSxhQUFBO0VBQ0EsZ0JBQUE7RUFDQSxxQkFBQTtFQUNBLG1CQUFBO0VBRVEsZ0NBQUE7RUFDUixrQkFBQTtBQUpKOztBQU9FO0VBRUUsYUFBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0VBQ0EsMEJBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FBSko7O0FBT0U7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLG9CQUFBO0VBQ0csaUJBQUE7QUFKUDs7QUFPRTtFQUNFLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFVBQUE7RUFDQSxhQUFBO0VBQ0EsZ0JBQUE7QUFKSjs7QUFPRTs7RUFFRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0FBSko7O0FBT0U7O0VBRUUsZUFBQTtBQUpKOztBQU9FO0VBRVUsc0JBQUE7QUFKWjs7QUFPRTtFQUNFLGFBQUE7RUFDQSxnQkFBQTtFQUVBLGFBQUE7RUFDQSxjQUFBO0FBSko7O0FBT0U7RUFDRSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtBQUpKOztBQU9BO0VBRUksYUFBQTtFQUNBLHNCQUFBO0VBQ0EsYUFBQTtFQUVJLDhDQUFBO0VBQ0Esc0NBQUE7RUFHSSxtQkFBQTtBQUpaOztBQU9FO0VBQ0UsZ0JBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0FBSko7O0FBT0U7RUFDRSxVQUFBO0FBSko7O0FBT0U7O0VBRUUsV0FBQTtFQUNBLFlBQUE7QUFKSjs7QUFPRTs7RUFFRSxhQUFBO0FBSko7O0FBT0U7RUFDRSxhQUFBO0VBQ0EsMEJBQUE7QUFKSjs7QUFPRTtFQUNFLG1CQUFBO0VBRVEsZ0NBQUE7RUFFUixhQUFBO0VBQ0EsbUJBQUE7QUFKSjs7QUFPRTtFQUNFLGdCQUFBO0FBSko7O0FBT0U7RUFDRSxhQUFBO0FBSko7O0FBT0U7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7QUFKSjs7QUFPRTtFQUNFLGNBQUE7RUFDQSxtQkFBQTtBQUpKOztBQU9BO0VBQ0ksY0FBQTtFQUNBLGlCQUFBO0VBQ0EsV0FBQTtFQUNBLGFBQUE7QUFKSjs7QUFPQTtFQUVFLFdBQUE7RUFFQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0VBQ0EseUJBQUE7QUFKRjs7QUFPQTtFQUVFLFdBQUE7RUFFQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7QUFKRjs7QUFPQTtFQUVFLFdBQUE7RUFDQSxvQkFBQTtFQUVBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0VBRUksaUJBQUE7QUFKTjs7QUFPQTtFQUNJLGFBQUE7RUFDQSwrQkFBQTtFQUNBLHdCQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsOENBQ0k7QUFMUjs7QUFTQTtFQUNFLFdBQUE7QUFORjs7QUFTQTs7RUFFRSxhQUFBO0VBQ0EsMEJBQUE7QUFORiIsImZpbGUiOiJzcmMvYXBwL3VzZXIvcHJvZHVjdC1jYXJ0L3Byb2R1Y3QtY2FydC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjYnVzaW5lc3MtZGV0YWlscy1wYWdlICNwcm9kdWN0cyB7XG4gICAgZ3JpZC1nYXA6IDVweDtcbiAgICBwYWRkaW5nOiAwO1xuICAgIG1hcmdpbjogMzBweCAwO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG4gIH1cbiAgXG4gICNidXNpbmVzcy1kZXRhaWxzLXBhZ2UgI3Byb2R1Y3RzIC5wcm9kdWN0cyB7XG4gICAgcGFkZGluZzogMDtcbiAgICBtYXJnaW4tYm90dG9tOiAzMHB4O1xuICB9XG4gIFxuICAjYnVzaW5lc3MtZGV0YWlscy1wYWdlICNwcm9kdWN0cyAucHJvZHVjdHMgLnByb2R1Y3Qge1xuICAgIGRpc3BsYXk6IC1tcy1ncmlkO1xuICAgIGRpc3BsYXk6IGdyaWQ7XG4gICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gICAgcGFkZGluZzogMTVweCA3LjVweDtcbiAgICAvLyBjb2x1bW4tZ2FwOiAxMnB4O1xuICAgIC8vIHJvdy1nYXA6IDExMHB4O1xuICAgIC8vIGdyaWQtYXV0by1yb3dzOiA1MHB4O1xuICAgIC8vIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KGF1dG8tZmlsbCwgbWlubWF4KDE1MHB4LCAxZnIpKTtcbiAgXG4gIH1cbiAgXG4gICNidXNpbmVzcy1kZXRhaWxzLXBhZ2UgI3Byb2R1Y3RzIC5wcm9kdWN0cyAucHJvZHVjdDpmaXJzdC1jaGlsZCB7XG4gICAgcGFkZGluZy1sZWZ0OiAxNXB4O1xuICB9XG4gIFxuICAjYnVzaW5lc3MtZGV0YWlscy1wYWdlICNwcm9kdWN0cyAucHJvZHVjdHMgLnByb2R1Y3Q6bGFzdC1jaGlsZCB7XG4gICAgcGFkZGluZy1yaWdodDogMTVweDtcbiAgfVxuICBcbiAgI3Byb2R1Y3RzIC5wcm9kdWN0cyAucHJvZHVjdCAuZGV0YWlscyB7XG4gICAgZGlzcGxheTogLW1zLWdyaWQ7XG4gICAgZGlzcGxheTogZ3JpZDtcbiAgICBncmlkLWdhcDogNXB4O1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICAgIGJhY2tncm91bmQ6ICNmZmZmZmY7XG4gICAgLXdlYmtpdC1ib3gtc2hhZG93OiAwcHggMnB4IDEwcHggI2U2ZThlZjtcbiAgICAgICAgICAgIGJveC1zaGFkb3c6IDBweCAycHggMTBweCAjZTZlOGVmO1xuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgfVxuICBcbiAgI3Byb2R1Y3RzIC5wcm9kdWN0cyAucHJvZHVjdCAuZGV0YWlscyAuaW1hZ2Uge1xuICAgIGRpc3BsYXk6IC1tcy1ncmlkO1xuICAgIGRpc3BsYXk6IGdyaWQ7XG4gICAgd2lkdGg6IDEyMHB4O1xuICAgIGhlaWdodDogMTIwcHg7XG4gICAgYm9yZGVyLXJhZGl1czogNHB4IDRweCAwIDA7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIH1cbiAgXG4gICNwcm9kdWN0cyAucHJvZHVjdHMgLnByb2R1Y3QgLmRldGFpbHMgLmltYWdlIGltZyB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIC1vLW9iamVjdC1maXQ6IGNvdmVyO1xuICAgICAgIG9iamVjdC1maXQ6IGNvdmVyO1xuICB9XG5cbiAgI3Byb2R1Y3RzIC5wcm9kdWN0cyAucHJvZHVjdCAuZGV0YWlscyAuaW1hZ2UgLmJvb2ttYXJrIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAwcHg7XG4gICAgcmlnaHQ6IDBweDtcbiAgICBwYWRkaW5nOiAxMHB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDA7XG4gIH1cbiAgXG4gICNwcm9kdWN0cyAucHJvZHVjdHMgLnByb2R1Y3QgLmRldGFpbHMgLmltYWdlIC5ib29rbWFyayBpbWcsXG4gICNwcm9kdWN0cyAucHJvZHVjdHMgLnByb2R1Y3QgLmRldGFpbHMgLmltYWdlIC5ib29rbWFyayBzdmcge1xuICAgIHdpZHRoOiBhdXRvO1xuICAgIGhlaWdodDogMTVweDtcbiAgICBib3JkZXItcmFkaXVzOiAwO1xuICB9XG4gIFxuICAjcHJvZHVjdHMgLnByb2R1Y3RzIC5wcm9kdWN0IC5kZXRhaWxzIC5pbWFnZSAuYm9va21hcmsgaW1nIHBhdGgsXG4gICNwcm9kdWN0cyAucHJvZHVjdHMgLnByb2R1Y3QgLmRldGFpbHMgLmltYWdlIC5ib29rbWFyayBzdmcgcGF0aCB7XG4gICAgc3Ryb2tlOiAjZmZmZmZmO1xuICB9XG4gIFxuICAjcHJvZHVjdHMgLnByb2R1Y3RzIC5wcm9kdWN0IC5kZXRhaWxzIC5pbWFnZSAuYm9va21hcms6YWN0aXZlIHtcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogc2NhbGUoMC45NSk7XG4gICAgICAgICAgICB0cmFuc2Zvcm06IHNjYWxlKDAuOTUpO1xuICB9XG4gIFxuICAjcHJvZHVjdHMgLnByb2R1Y3RzIC5wcm9kdWN0IC5kZXRhaWxzIC50ZXh0IHtcbiAgICBwYWRkaW5nOiAxMHB4O1xuICAgIG1heC13aWR0aDogMTIwcHg7XG4gICAgZGlzcGxheTogLW1zLWdyaWQ7XG4gICAgZGlzcGxheTogZ3JpZDtcbiAgICBncmlkLWdhcDogMTBweDtcbiAgfVxuICBcbiAgI3Byb2R1Y3RzIC5wcm9kdWN0cyAucHJvZHVjdCAuZGV0YWlscyAudGV4dCAubmFtZSB7XG4gICAgY29sb3I6ICMyMjJiNDU7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBmb250LXNpemU6IDE1cHg7XG4gICAgbGluZS1oZWlnaHQ6IDE4cHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMDtcbiAgfVxuICBcbiNwcm9kdWN0cyAucHJvZHVjdHMgLnByb2R1Y3QgLmRldGFpbHMgLnRleHQgcCB7XG4gICAgZGlzcGxheTogLW1zLWdyaWQ7XG4gICAgZGlzcGxheTogZ3JpZDtcbiAgICBncmlkLWF1dG8tZmxvdzogY29sdW1uO1xuICAgIGdyaWQtZ2FwOiA1cHg7XG4gICAgLW1zLWdyaWQtY29sdW1uczogMWZyIG1heC1jb250ZW50O1xuICAgICAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IDFmciAtd2Via2l0LW1heC1jb250ZW50O1xuICAgICAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IDFmciBtYXgtY29udGVudDtcbiAgICAtd2Via2l0LWJveC1hbGlnbjogY2VudGVyO1xuICAgICAgICAtbXMtZmxleC1hbGlnbjogY2VudGVyO1xuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgfVxuICBcbiAgI3Byb2R1Y3RzIC5wcm9kdWN0cyAucHJvZHVjdCAuZGV0YWlscyAudGV4dCBwIHNwYW4ge1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XG4gICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgICBmb250LXNpemU6IDE1cHg7XG4gICAgbGluZS1oZWlnaHQ6IDE1cHg7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBjb2xvcjogI2ZhNjQwMDtcbiAgfVxuXG4gICNidXNpbmVzcy1kZXRhaWxzLXBhZ2UgI3Byb2R1Y3RzIC5wcm9kdWN0cyAucHJvZHVjdCAuZGV0YWlscyAudGV4dCBwIC5hZGQtdG8tY2FydCB7XG4gICAgcGFkZGluZzogMDtcbiAgfVxuICBcbiAgI3Byb2R1Y3RzIC5wcm9kdWN0cyAucHJvZHVjdCAuZGV0YWlscyAudGV4dCBwIC5hZGQtdG8tY2FydCBpbWcsXG4gICNwcm9kdWN0cyAucHJvZHVjdHMgLnByb2R1Y3QgLmRldGFpbHMgLnRleHQgcCAuYWRkLXRvLWNhcnQgc3ZnIHtcbiAgICB3aWR0aDogMThweDtcbiAgICBoZWlnaHQ6IGF1dG87XG4gIH1cbiAgXG4gICNwcm9kdWN0cyAucHJvZHVjdHMgLnByb2R1Y3QgLmRldGFpbHMgLnRleHQgcCAuYWRkLXRvLWNhcnQgaW1nIHBhdGgsXG4gICNwcm9kdWN0cyAucHJvZHVjdHMgLnByb2R1Y3QgLmRldGFpbHMgLnRleHQgcCAuYWRkLXRvLWNhcnQgc3ZnIHBhdGgge1xuICAgIGZpbGw6ICNmYTY0MDA7XG4gIH1cbiAgXG4gICNwcm9kdWN0cyAucHJvZHVjdHMgLnByb2R1Y3QuYm9va21hcmtlZCAuYm9va21hcmsgc3ZnIHBhdGgge1xuICAgIGZpbGw6ICNmYTY0MDA7XG4gICAgc3Ryb2tlOiAjZmE2NDAwICFpbXBvcnRhbnQ7XG4gIH1cbiAgXG4gICNwcm9kdWN0cyAjcHJvZHVjdC1jYXRlZ29yaWVzIHtcbiAgICBiYWNrZ3JvdW5kOiAjZmZmZmZmO1xuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDJweCAxMHB4ICNlNmU4ZWY7XG4gICAgICAgICAgICBib3gtc2hhZG93OiAwcHggMnB4IDEwcHggI2U2ZThlZjtcbiAgICBkaXNwbGF5OiAtbXMtZ3JpZDtcbiAgICBkaXNwbGF5OiBncmlkO1xuICAgIG1hcmdpbi1ib3R0b206IDMwcHg7XG4gIH1cbiAgXG4gICNwcm9kdWN0cyAjcHJvZHVjdC1jYXRlZ29yaWVzOmxhc3QtY2hpbGQge1xuICAgIG1hcmdpbi1ib3R0b206IDA7XG4gIH1cbiAgXG4gICNwcm9kdWN0cyAjcHJvZHVjdC1jYXRlZ29yaWVzIC5zZWN0aW9uLXRpdGxlIHtcbiAgICBwYWRkaW5nOiAxNXB4O1xuICB9XG4gIFxuICAjcHJvZHVjdHMgI3Byb2R1Y3QtY2F0ZWdvcmllcyAuc2VjdGlvbi10aXRsZSBoNSB7XG4gICAgZm9udC1zaXplOiAxNXB4O1xuICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgY29sb3I6ICMyMjJiNDU7XG4gIH1cbiAgXG4gICNwcm9kdWN0cyAjcHJvZHVjdC1jYXRlZ29yaWVzIC5zZWN0aW9uLXRpdGxlIGg1IHNwYW4ge1xuICAgIGNvbG9yOiAjOGY5YmIzO1xuICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gIH1cblxuLmNvbnN0cmFpbiB7XG4gICAgbWFyZ2luOiAwIGF1dG87XG4gICAgbWF4LXdpZHRoOiAxMDAwcHg7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgcGFkZGluZzogMTVweDtcbn1cblxuLmJpZy1jYXJ0LWJ0biAuaXRlbXMge1xuICAtbXMtZ3JpZC1yb3c6IDM7XG4gIGdyaWQtcm93OiAzO1xuICAtbXMtZ3JpZC1jb2x1bW46IDE7XG4gIGdyaWQtY29sdW1uOiAxO1xuICBncmlkLWFyZWE6IGl0ZW1zO1xuICBmb250LXNpemU6IDEycHg7XG4gIGxpbmUtaGVpZ2h0OiAxMnB4O1xuICBjb2xvcjogI2ZmZmZmZjtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbn1cblxuLmJpZy1jYXJ0LWJ0biAucHJpY2Uge1xuICAtbXMtZ3JpZC1yb3c6IDI7XG4gIGdyaWQtcm93OiAyO1xuICAtbXMtZ3JpZC1jb2x1bW46IDE7XG4gIGdyaWQtY29sdW1uOiAxO1xuICBncmlkLWFyZWE6IHByaWNlO1xuICBmb250LXNpemU6IDE3cHg7XG4gIGxpbmUtaGVpZ2h0OiAxN3B4O1xuICBmb250LXdlaWdodDogNjAwO1xuICBjb2xvcjogI2ZmZmZmZjtcbn1cblxuLmJpZy1jYXJ0LWJ0biAudGV4dCB7XG4gIC1tcy1ncmlkLXJvdzogMTtcbiAgZ3JpZC1yb3c6IDE7XG4gIC1tcy1ncmlkLXJvdy1zcGFuOiAyO1xuICAtbXMtZ3JpZC1jb2x1bW46IDI7XG4gIGdyaWQtY29sdW1uOiAyO1xuICBncmlkLWFyZWE6IHRleHQ7XG4gIGZvbnQtc2l6ZTogMTdweDtcbiAgbGluZS1oZWlnaHQ6IDE3cHg7XG4gIGNvbG9yOiAjZmZmZmZmO1xuICBmb250LXdlaWdodDogNjAwO1xuICAtbXMtZ3JpZC1jb2x1bW4tYWxpZ246IGVuZDtcbiAgICAgIGp1c3RpZnktc2VsZjogZW5kO1xufVxuXG4uYmlnLWNhcnQtYnRuIHtcbiAgICBkaXNwbGF5OiBncmlkO1xuICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogYXV0byAxZnI7XG4gICAgZ3JpZC10ZW1wbGF0ZS1yb3dzOiBhdXRvO1xuICAgIGdyaWQtZ2FwOiAxNXB4O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgcGFkZGluZzogMjBweCAzMHB4O1xuICAgIGdyaWQtdGVtcGxhdGUtYXJlYXM6XG4gICAgICAgIFwiaXRlbXMgdGV4dFwiXG4gICAgICAgIFwicHJpY2UgdGV4dFwiO1xufVxuXG5pLmZhIHtcbiAgY29sb3I6ICNmZmY7XG59XG5cbi5hZGQtdG8tY2FydCBpbWcgcGF0aCxcbi5hZGQtdG8tY2FydCBzdmcgcGF0aCB7XG4gIGZpbGw6ICNmYTY0MDA7XG4gIHN0cm9rZTogI2ZhNjQwMCAhaW1wb3J0YW50O1xufSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/user/product-cart/product-cart.page.ts":
    /*!********************************************************!*\
      !*** ./src/app/user/product-cart/product-cart.page.ts ***!
      \********************************************************/

    /*! exports provided: ProductCartPage */

    /***/
    function srcAppUserProductCartProductCartPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ProductCartPage", function () {
        return ProductCartPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ngrx_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ngrx/store */
      "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");
      /* harmony import */


      var subsink__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! subsink */
      "./node_modules/subsink/dist/es2015/index.js");
      /* harmony import */


      var _user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../../user/store/actions/user.action */
      "./src/app/user/store/actions/user.action.ts");
      /* harmony import */


      var _store_model_user_model__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ../store/model/user.model */
      "./src/app/user/store/model/user.model.ts");
      /* harmony import */


      var _services_signalr_websocket_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../../services/signalr-websocket.service */
      "./src/app/services/signalr-websocket.service.ts");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");

      var ProductCartPage = /*#__PURE__*/function () {
        function ProductCartPage(store, signalRSrv, router, _ngZone, platform) {
          var _this = this;

          _classCallCheck(this, ProductCartPage);

          this.store = store;
          this.signalRSrv = signalRSrv;
          this.router = router;
          this._ngZone = _ngZone;
          this.platform = platform;
          this.productCartStatus = _store_model_user_model__WEBPACK_IMPORTED_MODULE_6__["ProductCartStatus"];
          this.subSink = new subsink__WEBPACK_IMPORTED_MODULE_4__["SubSink"]();
          this.isCheckoutBtnHidden = false;
          this.productCartVertificationStatus = _store_model_user_model__WEBPACK_IMPORTED_MODULE_6__["ProductCartStatus"].UNVERIFIED;
          this.subscribeToEvents();
          /**
           * When the app gets activated from backgriund mode,
           * then display the details from the cart
           */

          this.subSink.sink = this.platform.resume.subscribe(function () {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      this.getProcessedCartAsync();

                    case 1:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          });
        }

        _createClass(ProductCartPage, [{
          key: "ionViewWillEnter",
          value: function ionViewWillEnter() {
            this.getProcessedCartAsync();
          }
        }, {
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this2 = this;

            var _a;

            this.productCart$ = this.store.select(function (data) {
              return data.User.CartItems;
            });
            this.productCartCount$ = this.store.select(function (data) {
              return data.User.CartItemCount;
            });
            this.subSink.sink = this.productCartCount$.subscribe(function (data) {
              if (data < 1) {
                _this2.router.navigate(["/user", "discover-items"]);
              }
            });
            this.subSink.sink = this.productCart$.subscribe(function (data) {
              return _this2.productCart = data;
            });

            var _iterator = _createForOfIteratorHelper((_a = this.productCart) === null || _a === void 0 ? void 0 : _a.productCheckouts),
                _step;

            try {
              for (_iterator.s(); !(_step = _iterator.n()).done;) {
                var product = _step.value;
                this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].GetSingleProductInitiatedAction({
                  payload: {
                    productId: product.productId
                  }
                }));
              }
            } catch (err) {
              _iterator.e(err);
            } finally {
              _iterator.f();
            }

            this.cartItemDetails$ = this.store.select(function (data) {
              return data.User.SelectedCartProductsList;
            });
          }
        }, {
          key: "removeProductFromCart",
          value: function removeProductFromCart(productId) {
            // ? Dispatch an action that removes the product from store
            this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].RemoveProductFromCartInitiatedAction({
              payload: {
                productId: productId
              }
            }));
          }
        }, {
          key: "cartCheckout",
          value: function cartCheckout(cart) {
            var _this3 = this;

            this.isCheckoutBtnHidden = true;
            this.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].CartCheckoutInitiatedAction({
              payload: cart
            }));
            this.getProcessedCartAsync();
            this.subSink.sink = this.processedCart$.subscribe(function (data) {
              if (data === null || data === void 0 ? void 0 : data.cartId) {
                /**
                 * Make a websocket connection to make known that Cart verification is needed
                 */
                _this3.signalRSrv.makeCartVerificationRequest(data.cartId);
              }
            });
          }
        }, {
          key: "makePayment",
          value: function makePayment(cartId) {
            /**
             * Route to payment page
             */
            this.router.navigate(["/user", "make-payment", cartId]);
          }
        }, {
          key: "subscribeToEvents",
          value: function subscribeToEvents() {
            var _this4 = this;

            this.subSink.sink = this.signalRSrv.cartVerified.subscribe(function (payload) {
              _this4._ngZone.run(function () {
                if (payload === null || payload === void 0 ? void 0 : payload.status) {
                  _this4.store.dispatch(_user_store_actions_user_action__WEBPACK_IMPORTED_MODULE_5__["actions"].VerifyProcessedCartAction({
                    payload: {
                      status: payload,
                      processedCart: _this4.processedCart
                    }
                  }));

                  _this4.productCartVertificationStatus = payload.status;
                }
              });
            });
          }
        }, {
          key: "getProcessedCartAsync",
          value: function getProcessedCartAsync() {
            var _this5 = this;

            this.processedCart$ = this.store.select(function (data) {
              return data.User.ProcessedProductCart;
            });
            this.subSink.sink = this.processedCart$.subscribe(function (data) {
              if (data === null || data === void 0 ? void 0 : data.cartId) {
                _this5.processedCart = data;
              }
            });
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            this.signalRSrv.disconnect();
            this.subSink.unsubscribe();
          }
        }]);

        return ProductCartPage;
      }();

      ProductCartPage.ctorParameters = function () {
        return [{
          type: _ngrx_store__WEBPACK_IMPORTED_MODULE_3__["Store"]
        }, {
          type: _services_signalr_websocket_service__WEBPACK_IMPORTED_MODULE_7__["SignalrWebsocketService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_8__["Platform"]
        }];
      };

      ProductCartPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-product-cart",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./product-cart.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/user/product-cart/product-cart.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./product-cart.page.scss */
        "./src/app/user/product-cart/product-cart.page.scss"))["default"]]
      })], ProductCartPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=product-cart-product-cart-module-es5.js.map