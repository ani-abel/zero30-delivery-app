(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["validate-otp-validate-otp-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/auth/validate-otp/validate-otp.page.html":
    /*!************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/auth/validate-otp/validate-otp.page.html ***!
      \************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppAuthValidateOtpValidateOtpPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n    <main id=\"verification-page\" class=\"bg-offwhite container\">\n\n        <section id=\"header\" class=\"constrain\">\n            <div class=\"navigation\">\n                <a [routerLink]=\"['/auth', 'forgot-password']\" class=\"back\">\n                    <img class=\"svg\" src=\"assets/images/icons/arrow-left.svg\" height=\"5px\" alt=\"Go back\">\n                </a>\n            </div>\n        </section>\n      \n        <section id=\"heading-text\" class=\"constrain\">\n            <h1 class=\"title\">Verification</h1>\n            <p>Enter the OTP code from the phone we just sent you.</p>\n        </section>\n      \n        <section id=\"verification-form\" class=\"constrain\">\n            <form [formGroup]=\"validateOTPForm\" \n                (ngSubmit)=\"onSubmit()\"\n                class=\"otp\" \n                autocomplete=\"off\">\n                <div class=\"form-row\">\n                    <input type=\"text\" \n                        maxlength=\"1\" \n                        id=\"digit-1\" \n                        [formControlName]=\"'Digit1'\"/>\n                    <input type=\"text\" \n                        maxlength=\"1\" \n                        id=\"digit-2\" \n                        [formControlName]=\"'Digit2'\"/>\n                    <input type=\"text\" \n                        maxlength=\"1\" \n                        id=\"digit-3\" \n                        [formControlName]=\"'Digit3'\"/>\n                    <input type=\"text\" \n                        maxlength=\"1\" \n                        id=\"digit-4\" \n                        [formControlName]=\"'Digit4'\"/>\n                </div>\n      \n                <p class=\"error\" \n                    *ngIf=\"validateOTPForm.get('Digit1').touched && validateOTPForm.get('Digit1').invalid\">\n                    <span *ngIf=\"validateOTPForm.get('Digit1').errors['required']\">\n                        Oh no! This field is required\n                    </span><br />\n                    <span *ngIf=\"validateOTPForm.get('Digit1').errors['notNumber']\">\n                        Oh no! This must be a number\n                    </span>\n                </p>\n\n                <p class=\"error\" \n                    *ngIf=\"validateOTPForm.get('Digit2').touched && validateOTPForm.get('Digit2').invalid\">\n                    <span *ngIf=\"validateOTPForm.get('Digit2').errors['required']\">\n                        Oh no! This field is required\n                    </span><br />\n                    <span *ngIf=\"validateOTPForm.get('Digit2').errors['notNumber']\">\n                        Oh no! This must be a number\n                    </span>\n                </p>\n\n                <p class=\"error\" \n                    *ngIf=\"validateOTPForm.get('Digit3').touched && validateOTPForm.get('Digit3').invalid\">\n                    <span *ngIf=\"validateOTPForm.get('Digit3').errors['required']\">\n                        Oh no! This field is required\n                    </span><br />\n                    <span *ngIf=\"validateOTPForm.get('Digit3').errors['notNumber']\">\n                        Oh no! This must be a number\n                    </span>\n                </p>\n\n                <p class=\"error\" \n                    *ngIf=\"validateOTPForm.get('Digit4').touched && validateOTPForm.get('Digit4').invalid\">\n                    <span *ngIf=\"validateOTPForm.get('Digit4').errors['required']\">\n                        Oh no! This field is required\n                    </span><br />\n                    <span *ngIf=\"validateOTPForm.get('Digit4').errors['notNumber']\">\n                        Oh no! This must be a number\n                    </span>\n                </p>\n      \n                <div></div>\n      \n                <div class=\"form-row\">\n                    <p>Didn’t receive OTP code! <a (click)=\"resendCode()\" class=\"link link-dark\">Resend</a></p>\n                </div>\n      \n                <div class=\"form-row\">\n                    <button type=\"submit\" \n                        class=\"btn btn-primary\" \n                        [disabled]=\"validateOTPForm.invalid\">\n                        Next\n                    </button>\n                </div>\n            </form>\n        </section>\n      </main>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/auth/validate-otp/validate-otp-routing.module.ts":
    /*!******************************************************************!*\
      !*** ./src/app/auth/validate-otp/validate-otp-routing.module.ts ***!
      \******************************************************************/

    /*! exports provided: ValidateOtpPageRoutingModule */

    /***/
    function srcAppAuthValidateOtpValidateOtpRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ValidateOtpPageRoutingModule", function () {
        return ValidateOtpPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _validate_otp_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./validate-otp.page */
      "./src/app/auth/validate-otp/validate-otp.page.ts");

      var routes = [{
        path: '',
        component: _validate_otp_page__WEBPACK_IMPORTED_MODULE_3__["ValidateOtpPage"]
      }];

      var ValidateOtpPageRoutingModule = function ValidateOtpPageRoutingModule() {
        _classCallCheck(this, ValidateOtpPageRoutingModule);
      };

      ValidateOtpPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], ValidateOtpPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/auth/validate-otp/validate-otp.module.ts":
    /*!**********************************************************!*\
      !*** ./src/app/auth/validate-otp/validate-otp.module.ts ***!
      \**********************************************************/

    /*! exports provided: ValidateOtpPageModule */

    /***/
    function srcAppAuthValidateOtpValidateOtpModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ValidateOtpPageModule", function () {
        return ValidateOtpPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _validate_otp_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./validate-otp-routing.module */
      "./src/app/auth/validate-otp/validate-otp-routing.module.ts");
      /* harmony import */


      var _validate_otp_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./validate-otp.page */
      "./src/app/auth/validate-otp/validate-otp.page.ts");
      /* harmony import */


      var _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../../shared/shared.module */
      "./src/app/shared/shared.module.ts");

      var ValidateOtpPageModule = function ValidateOtpPageModule() {
        _classCallCheck(this, ValidateOtpPageModule);
      };

      ValidateOtpPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"], _validate_otp_routing_module__WEBPACK_IMPORTED_MODULE_5__["ValidateOtpPageRoutingModule"]],
        declarations: [_validate_otp_page__WEBPACK_IMPORTED_MODULE_6__["ValidateOtpPage"]]
      })], ValidateOtpPageModule);
      /***/
    },

    /***/
    "./src/app/auth/validate-otp/validate-otp.page.scss":
    /*!**********************************************************!*\
      !*** ./src/app/auth/validate-otp/validate-otp.page.scss ***!
      \**********************************************************/

    /*! exports provided: default */

    /***/
    function srcAppAuthValidateOtpValidateOtpPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2F1dGgvdmFsaWRhdGUtb3RwL3ZhbGlkYXRlLW90cC5wYWdlLnNjc3MifQ== */";
      /***/
    },

    /***/
    "./src/app/auth/validate-otp/validate-otp.page.ts":
    /*!********************************************************!*\
      !*** ./src/app/auth/validate-otp/validate-otp.page.ts ***!
      \********************************************************/

    /*! exports provided: ValidateOtpPage */

    /***/
    function srcAppAuthValidateOtpValidateOtpPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ValidateOtpPage", function () {
        return ValidateOtpPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ngrx_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ngrx/store */
      "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var subsink__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! subsink */
      "./node_modules/subsink/dist/es2015/index.js");
      /* harmony import */


      var _store_actions_auth_action__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ../store/actions/auth.action */
      "./src/app/auth/store/actions/auth.action.ts");
      /* harmony import */


      var _utils_validators_form_validators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../../../utils/validators/form.validators */
      "./src/utils/validators/form.validators.ts");

      var ValidateOtpPage = /*#__PURE__*/function () {
        function ValidateOtpPage(store, activatedRoute, router) {
          _classCallCheck(this, ValidateOtpPage);

          this.store = store;
          this.activatedRoute = activatedRoute;
          this.router = router;
          this.subSink = new subsink__WEBPACK_IMPORTED_MODULE_5__["SubSink"]();
        }

        _createClass(ValidateOtpPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this = this;

            this.initForm();
            this.subSink.sink = this.activatedRoute.params.subscribe(function (data) {
              _this.phoneNumber = data.phoneNumber;
            });
          }
        }, {
          key: "resendCode",
          value: function resendCode() {
            if (this.phoneNumber) {
              this.store.dispatch(_store_actions_auth_action__WEBPACK_IMPORTED_MODULE_6__["actions"].PasswordRecoveryInitiatedAction({
                payload: this.phoneNumber
              }));
            }
          }
        }, {
          key: "onSubmit",
          value: function onSubmit() {
            var _this2 = this;

            if (this.validateOTPForm.invalid) {
              return;
            }

            var _this$validateOTPForm = this.validateOTPForm.value,
                Digit1 = _this$validateOTPForm.Digit1,
                Digit2 = _this$validateOTPForm.Digit2,
                Digit3 = _this$validateOTPForm.Digit3,
                Digit4 = _this$validateOTPForm.Digit4;
            var fullOTPCode = "".concat(Digit1).concat(Digit2).concat(Digit3).concat(Digit4);
            this.store.dispatch(_store_actions_auth_action__WEBPACK_IMPORTED_MODULE_6__["actions"].VerifyOTPTokenInitiatedAction({
              payload: {
                token: fullOTPCode,
                PhoneNumber: this.phoneNumber
              }
            }));
            this.subSink.sink = this.store.select(function (data) {
              return data.Auth.ActiveMessage;
            }).subscribe(function (data) {
              if (data) {
                _this2.router.navigate(["/auth", "reset-password", _this2.phoneNumber]);

                _this2.validateOTPForm.reset();
              }
            });
          }
        }, {
          key: "initForm",
          value: function initForm() {
            this.validateOTPForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
              Digit1: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _utils_validators_form_validators__WEBPACK_IMPORTED_MODULE_7__["NumberTypeValidator"]])),
              Digit2: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _utils_validators_form_validators__WEBPACK_IMPORTED_MODULE_7__["NumberTypeValidator"]])),
              Digit3: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _utils_validators_form_validators__WEBPACK_IMPORTED_MODULE_7__["NumberTypeValidator"]])),
              Digit4: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _utils_validators_form_validators__WEBPACK_IMPORTED_MODULE_7__["NumberTypeValidator"]]))
            });
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            this.subSink.unsubscribe();
          }
        }]);

        return ValidateOtpPage;
      }();

      ValidateOtpPage.ctorParameters = function () {
        return [{
          type: _ngrx_store__WEBPACK_IMPORTED_MODULE_3__["Store"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
        }];
      };

      ValidateOtpPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-validate-otp',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./validate-otp.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/auth/validate-otp/validate-otp.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./validate-otp.page.scss */
        "./src/app/auth/validate-otp/validate-otp.page.scss"))["default"]]
      })], ValidateOtpPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=validate-otp-validate-otp-module-es5.js.map