(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["sign-up-otp-verification-sign-up-otp-verification-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/auth/sign-up-otp-verification/sign-up-otp-verification.page.html":
/*!************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/auth/sign-up-otp-verification/sign-up-otp-verification.page.html ***!
  \************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n  <main id=\"verification-page\" class=\"bg-offwhite container\">\n\n      <section id=\"header\" class=\"constrain\">\n          <div class=\"navigation\">\n              <a [routerLink]=\"['/auth', 'sign-up']\" class=\"back\">\n                  <img class=\"svg\" src=\"assets/images/icons/arrow-left.svg\" height=\"5px\" alt=\"Go back\">\n              </a>\n          </div>\n      </section>\n    \n      <section id=\"heading-text\" class=\"constrain\">\n          <h1 class=\"title\">Verification</h1>\n          <p>Enter the OTP code from the phone we just sent you.</p>\n      </section>\n    \n      <section id=\"verification-form\" class=\"constrain\">\n          <form [formGroup]=\"validateOTPForm\" \n              (ngSubmit)=\"onSubmit()\"\n              class=\"otp\" \n              autocomplete=\"off\">\n              <div class=\"form-row\">\n                  <input type=\"text\" \n                      maxlength=\"1\" \n                      id=\"digit-1\" \n                      (input)=\"hightlightNextField(digit2)\"\n                      [formControlName]=\"'Digit1'\"/>\n                  <input type=\"text\" \n                      maxlength=\"1\" \n                      #digit2\n                      (input)=\"hightlightNextField(digit3)\"\n                      id=\"digit-2\" \n                      [formControlName]=\"'Digit2'\"/>\n                  <input type=\"text\" \n                      maxlength=\"1\" \n                      #digit3\n                      (input)=\"hightlightNextField(digit4)\"\n                      id=\"digit-3\" \n                      [formControlName]=\"'Digit3'\"/>\n                  <input type=\"text\" \n                      maxlength=\"1\" \n                      #digit4\n                      id=\"digit-4\" \n                      [formControlName]=\"'Digit4'\"/>\n              </div>\n    \n              <p class=\"error\" \n                  *ngIf=\"validateOTPForm.get('Digit1').touched && validateOTPForm.get('Digit1').invalid\">\n                  <span *ngIf=\"validateOTPForm.get('Digit1').errors['required']\">\n                      Oh no! This field is required\n                  </span><br />\n                  <span *ngIf=\"validateOTPForm.get('Digit1').errors['notNumber']\">\n                      Oh no! This must be a number\n                  </span>\n              </p>\n\n              <p class=\"error\" \n                  *ngIf=\"validateOTPForm.get('Digit2').touched && validateOTPForm.get('Digit2').invalid\">\n                  <span *ngIf=\"validateOTPForm.get('Digit2').errors['required']\">\n                      Oh no! This field is required\n                  </span><br />\n                  <span *ngIf=\"validateOTPForm.get('Digit2').errors['notNumber']\">\n                      Oh no! This must be a number\n                  </span>\n              </p>\n\n              <p class=\"error\" \n                  *ngIf=\"validateOTPForm.get('Digit3').touched && validateOTPForm.get('Digit3').invalid\">\n                  <span *ngIf=\"validateOTPForm.get('Digit3').errors['required']\">\n                      Oh no! This field is required\n                  </span><br />\n                  <span *ngIf=\"validateOTPForm.get('Digit3').errors['notNumber']\">\n                      Oh no! This must be a number\n                  </span>\n              </p>\n\n              <p class=\"error\" \n                  *ngIf=\"validateOTPForm.get('Digit4').touched && validateOTPForm.get('Digit4').invalid\">\n                  <span *ngIf=\"validateOTPForm.get('Digit4').errors['required']\">\n                      Oh no! This field is required\n                  </span><br />\n                  <span *ngIf=\"validateOTPForm.get('Digit4').errors['notNumber']\">\n                      Oh no! This must be a number\n                  </span>\n              </p>\n    \n              <div></div>\n    \n              <div class=\"form-row\">\n                    <p>\n                      Didn’t receive OTP code! \n                      <a (click)=\"resendOTP()\"  \n                        class=\"link link-dark cursor-pointer\">Resend\n                      </a>\n                    </p>\n              </div>\n    \n              <div class=\"form-row\">\n                  <button type=\"submit\" \n                      class=\"btn btn-primary\" \n                      [disabled]=\"validateOTPForm.invalid\">\n                      Next\n                  </button>\n              </div>\n          </form>\n      </section>\n    </main>\n</ion-content>");

/***/ }),

/***/ "./src/app/auth/sign-up-otp-verification/sign-up-otp-verification-routing.module.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/auth/sign-up-otp-verification/sign-up-otp-verification-routing.module.ts ***!
  \******************************************************************************************/
/*! exports provided: SignUpOtpVerificationPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignUpOtpVerificationPageRoutingModule", function() { return SignUpOtpVerificationPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _sign_up_otp_verification_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./sign-up-otp-verification.page */ "./src/app/auth/sign-up-otp-verification/sign-up-otp-verification.page.ts");




const routes = [
    {
        path: '',
        component: _sign_up_otp_verification_page__WEBPACK_IMPORTED_MODULE_3__["SignUpOtpVerificationPage"]
    }
];
let SignUpOtpVerificationPageRoutingModule = class SignUpOtpVerificationPageRoutingModule {
};
SignUpOtpVerificationPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SignUpOtpVerificationPageRoutingModule);



/***/ }),

/***/ "./src/app/auth/sign-up-otp-verification/sign-up-otp-verification.module.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/auth/sign-up-otp-verification/sign-up-otp-verification.module.ts ***!
  \**********************************************************************************/
/*! exports provided: SignUpOtpVerificationPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignUpOtpVerificationPageModule", function() { return SignUpOtpVerificationPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _sign_up_otp_verification_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./sign-up-otp-verification-routing.module */ "./src/app/auth/sign-up-otp-verification/sign-up-otp-verification-routing.module.ts");
/* harmony import */ var _sign_up_otp_verification_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./sign-up-otp-verification.page */ "./src/app/auth/sign-up-otp-verification/sign-up-otp-verification.page.ts");
/* harmony import */ var src_app_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/shared/shared.module */ "./src/app/shared/shared.module.ts");








let SignUpOtpVerificationPageModule = class SignUpOtpVerificationPageModule {
};
SignUpOtpVerificationPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _sign_up_otp_verification_routing_module__WEBPACK_IMPORTED_MODULE_5__["SignUpOtpVerificationPageRoutingModule"],
            src_app_shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"],
        ],
        declarations: [_sign_up_otp_verification_page__WEBPACK_IMPORTED_MODULE_6__["SignUpOtpVerificationPage"]]
    })
], SignUpOtpVerificationPageModule);



/***/ }),

/***/ "./src/app/auth/sign-up-otp-verification/sign-up-otp-verification.page.scss":
/*!**********************************************************************************!*\
  !*** ./src/app/auth/sign-up-otp-verification/sign-up-otp-verification.page.scss ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2F1dGgvc2lnbi11cC1vdHAtdmVyaWZpY2F0aW9uL3NpZ24tdXAtb3RwLXZlcmlmaWNhdGlvbi5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/auth/sign-up-otp-verification/sign-up-otp-verification.page.ts":
/*!********************************************************************************!*\
  !*** ./src/app/auth/sign-up-otp-verification/sign-up-otp-verification.page.ts ***!
  \********************************************************************************/
/*! exports provided: SignUpOtpVerificationPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignUpOtpVerificationPage", function() { return SignUpOtpVerificationPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");
/* harmony import */ var subsink__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! subsink */ "./node_modules/subsink/dist/es2015/index.js");
/* harmony import */ var _utils_validators_form_validators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../utils/validators/form.validators */ "./src/utils/validators/form.validators.ts");
/* harmony import */ var _store_actions_auth_action__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../store/actions/auth.action */ "./src/app/auth/store/actions/auth.action.ts");








let SignUpOtpVerificationPage = class SignUpOtpVerificationPage {
    constructor(store, activatedRoute, router) {
        this.store = store;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.subSink = new subsink__WEBPACK_IMPORTED_MODULE_5__["SubSink"]();
    }
    ngOnInit() {
        this.initForm();
        this.subSink.sink =
            this.activatedRoute
                .params
                .subscribe((data) => {
                this.phoneNumber = data === null || data === void 0 ? void 0 : data.phoneNumber;
            });
    }
    resendOTP() {
        if (this.phoneNumber) {
            this.store.dispatch(_store_actions_auth_action__WEBPACK_IMPORTED_MODULE_7__["actions"].PasswordRecoveryInitiatedAction({ payload: this.phoneNumber }));
        }
    }
    onSubmit() {
        if (this.validateOTPForm.invalid) {
            return;
        }
        const { Digit1, Digit2, Digit3, Digit4 } = this.validateOTPForm.value;
        const fullOTPCode = `${Digit1}${Digit2}${Digit3}${Digit4}`;
        this.store.dispatch(_store_actions_auth_action__WEBPACK_IMPORTED_MODULE_7__["actions"].VerifyOTPAfterSignUpInitiatedAction({ payload: { token: fullOTPCode, PhoneNumber: this.phoneNumber } }));
        this.subSink.sink =
            this.store.select((data) => data.Auth.ActiveMessage).subscribe((data) => {
                if (data) {
                    this.router.navigate(["/auth", "login"]);
                    this.validateOTPForm.reset();
                }
            });
    }
    initForm() {
        this.validateOTPForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            Digit1: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                _utils_validators_form_validators__WEBPACK_IMPORTED_MODULE_6__["NumberTypeValidator"]
            ])),
            Digit2: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                _utils_validators_form_validators__WEBPACK_IMPORTED_MODULE_6__["NumberTypeValidator"]
            ])),
            Digit3: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                _utils_validators_form_validators__WEBPACK_IMPORTED_MODULE_6__["NumberTypeValidator"]
            ])),
            Digit4: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                _utils_validators_form_validators__WEBPACK_IMPORTED_MODULE_6__["NumberTypeValidator"]
            ]))
        });
    }
    hightlightNextField(nextField) {
        nextField.focus();
    }
    ngOnDestroy() {
        this.subSink.unsubscribe();
    }
};
SignUpOtpVerificationPage.ctorParameters = () => [
    { type: _ngrx_store__WEBPACK_IMPORTED_MODULE_4__["Store"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] }
];
SignUpOtpVerificationPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-sign-up-otp-verification',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./sign-up-otp-verification.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/auth/sign-up-otp-verification/sign-up-otp-verification.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./sign-up-otp-verification.page.scss */ "./src/app/auth/sign-up-otp-verification/sign-up-otp-verification.page.scss")).default]
    })
], SignUpOtpVerificationPage);



/***/ })

}]);
//# sourceMappingURL=sign-up-otp-verification-sign-up-otp-verification-module-es2015.js.map