(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["new-order-detail-new-order-detail-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/rider/new-order-detail/new-order-detail.page.html":
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/rider/new-order-detail/new-order-detail.page.html ***!
  \*********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("  <main id=\"new-order-page\" class=\"container bg-offwhite\">\n    <section id=\"header\" class=\"header-white\">\n      <div class=\"header-actions constrain\">\n        <a backButton class=\"back link\">\n          <img class=\"svg\" src=\"assets/images/icons/arrow-left.svg\" width=\"18px\" alt=\"Go back\" />\n        </a>\n        <h4>New Order</h4>\n      </div>\n    </section>\n  \n    <section id=\"map\" class=\"constrain\" #mapSection></section>\n  \n    <ion-content>\n      <section id=\"new-order-popup\" class=\"popup\" [ngClass]=\"{ 'active': isModelOpen }\">\n        <!-- This popup has 2 screens. The second one comes up immediately the user rates the driver.\n        The class responsible for the switching is \".rated\" You can switch by adding a \".rated\" class to the popup-content section. It'll look like this: \"popup-content rated\" -->\n        <form class=\"popup-content\">\n          <!-- This button closes the popup, it has been implemented. -->\n          <button type=\"button\" class=\"close-popup\" (click)=\"toggleModal()\">\n            <img class=\"svg\" src=\"assets/images/icons/close-popup.svg\" height=\"10px\" alt=\"Close Popup\" />\n          </button>\n    \n          <div class=\"tracking-details\">\n            <div class=\"trip constrain\">\n              <div class=\"order-timeline\">\n                <section *ngIf=\"(merchantInRiderProximity$ | async) as merchantInRiderProximity\">\n                  <div class=\"timeline active\" *ngFor=\"let merchant of merchantInRiderProximity; index as i\">\n                    <div class=\"icon\"></div>\n                    <p class=\"header\">\n                      <img class=\"svg\" src=\"assets/images/icons/map-pointer.svg\" height=\"10px\" alt=\"Store\" />\n                      <span>Pickup Location {{ i + 1 }}</span>\n                    </p>\n                    <div class=\"user\">\n                      <div class=\"image\">\n                        <img [src]=\"merchant.merchantImage | filePathFormatter\" \n                          height=\"95px\" \n                          [alt]=\"merchant.merchantName\" />\n                        <ng-template #defaultImage>\n                          <img src=\"assets/images/restaurants/lion-square.png\" \n                            height=\"95px\" \n                            [alt]=\"merchant.merchantName\" />\n                        </ng-template>\n                      </div>\n                      <div class=\"details\">\n                        <a class=\"name cursor-pointer\">\n                          <h5>{{ merchant.merchantName | capitalize }}</h5>\n                        </a>\n                        <p class=\"address\">\n                          <img class=\"svg\" src=\"assets/images/icons/map-pointer.svg\" height=\"10px\" alt=\"Store\" />\n                          <span>{{ merchant.merchantAddress | capitalize }}</span>\n                        </p>\n                      </div>\n                    </div>\n                  </div>\n                </section>\n    \n                <div class=\"timeline inactive\">\n                  <div class=\"icon\"></div>\n                  <p class=\"header\">\n                    <img class=\"svg\" src=\"assets/images/icons/map-pointer.svg\" height=\"10px\" alt=\"Store\" />\n                    <span>Drop-off Location</span>\n                  </p>\n                  <div class=\"user\" *ngIf=\"(selectedCart$ | async) as selectedCart;\">\n                    <div class=\"image\">\n                      <img *ngIf=\"selectedCart?.user?.person?.passportUrl; else defaultUserImageTemplate\"\n                        height=\"50px\" \n                        alt=\"User's Picture\"\n                        [src]=\"selectedCart.user.person.passportUrl | filePathFormatter\" />\n                      <ng-template #defaultUserImageTemplate>\n                        <img src=\"assets/images/icons/riders/user.png\" height=\"50px\" alt=\"User's Picture\" />\n                      </ng-template>\n                    </div>\n                    <div class=\"details\">\n                      <a class=\"name cursor-pointer\">\n                        <h5>\n                          {{ selectedCart?.user?.person.firstName | capitalize }}\n                          {{ selectedCart?.user?.person.lastName | capitalize }}\n                        </h5>\n                      </a>\n                      <p class=\"address\">\n                        <img class=\"svg\" src=\"assets/images/icons/map-pointer.svg\" height=\"10px\" alt=\"Store\" />\n                        <span>{{ selectedCart?.deliveryAddress | capitalize }}</span>\n                      </p>\n                      <p class=\"phone\">\n                        <img class=\"svg\" src=\"assets/images/icons/telephone.svg\" height=\"9px\" alt=\"Store\" />\n                        <span>{{ selectedCart?.user?.person.phoneNo }}</span>\n                      </p>\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n    \n            <div class=\"submit constrain\" *ngIf=\"(selectedCart$ | async) as selectedCart;\">\n              <button type=\"button\" \n                class=\"btn btn-green cursor-pointer\" \n                (click)=\"acceptDelivery(selectedCart.id)\"\n                [disabled]=\"selectedCart.status === productStatus.TRANSIT\">\n                Accept\n              </button>\n              <button type=\"button\" \n                class=\"btn btn-white cursor-pointer\" \n                (click)=\"cancelDelivery(selectedCart.id)\"\n                [disabled]=\"selectedCart.status === productStatus.TRANSIT\">\n                Cancel\n              </button>\n            </div>\n          </div>\n        </form>\n        <!-- <div class=\"popup-overlay\"></div> -->\n      </section>\n    </ion-content>\n  \n  </main>\n");

/***/ }),

/***/ "./src/app/rider/new-order-detail/new-order-detail-routing.module.ts":
/*!***************************************************************************!*\
  !*** ./src/app/rider/new-order-detail/new-order-detail-routing.module.ts ***!
  \***************************************************************************/
/*! exports provided: NewOrderDetailPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewOrderDetailPageRoutingModule", function() { return NewOrderDetailPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _new_order_detail_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./new-order-detail.page */ "./src/app/rider/new-order-detail/new-order-detail.page.ts");




const routes = [
    {
        path: '',
        component: _new_order_detail_page__WEBPACK_IMPORTED_MODULE_3__["NewOrderDetailPage"]
    }
];
let NewOrderDetailPageRoutingModule = class NewOrderDetailPageRoutingModule {
};
NewOrderDetailPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], NewOrderDetailPageRoutingModule);



/***/ }),

/***/ "./src/app/rider/new-order-detail/new-order-detail.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/rider/new-order-detail/new-order-detail.module.ts ***!
  \*******************************************************************/
/*! exports provided: NewOrderDetailPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewOrderDetailPageModule", function() { return NewOrderDetailPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _new_order_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./new-order-detail-routing.module */ "./src/app/rider/new-order-detail/new-order-detail-routing.module.ts");
/* harmony import */ var _new_order_detail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./new-order-detail.page */ "./src/app/rider/new-order-detail/new-order-detail.page.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../shared/shared.module */ "./src/app/shared/shared.module.ts");








let NewOrderDetailPageModule = class NewOrderDetailPageModule {
};
NewOrderDetailPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"],
            _new_order_detail_routing_module__WEBPACK_IMPORTED_MODULE_5__["NewOrderDetailPageRoutingModule"]
        ],
        declarations: [_new_order_detail_page__WEBPACK_IMPORTED_MODULE_6__["NewOrderDetailPage"]]
    })
], NewOrderDetailPageModule);



/***/ }),

/***/ "./src/app/rider/new-order-detail/new-order-detail.page.scss":
/*!*******************************************************************!*\
  !*** ./src/app/rider/new-order-detail/new-order-detail.page.scss ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3JpZGVyL25ldy1vcmRlci1kZXRhaWwvbmV3LW9yZGVyLWRldGFpbC5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/rider/new-order-detail/new-order-detail.page.ts":
/*!*****************************************************************!*\
  !*** ./src/app/rider/new-order-detail/new-order-detail.page.ts ***!
  \*****************************************************************/
/*! exports provided: NewOrderDetailPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewOrderDetailPage", function() { return NewOrderDetailPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");
/* harmony import */ var subsink__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! subsink */ "./node_modules/subsink/dist/es2015/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _store_actions_rider_action__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../store/actions/rider.action */ "./src/app/rider/store/actions/rider.action.ts");
/* harmony import */ var _user_store_model_user_model__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../user/store/model/user.model */ "./src/app/user/store/model/user.model.ts");
/* harmony import */ var _services_location_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../services/location.service */ "./src/app/services/location.service.ts");









let NewOrderDetailPage = class NewOrderDetailPage {
    constructor(activatedRoute, router, store, locationSrv) {
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.store = store;
        this.locationSrv = locationSrv;
        this.isModelOpen = true;
        this.subSink = new subsink__WEBPACK_IMPORTED_MODULE_4__["SubSink"]();
        this.productStatus = _user_store_model_user_model__WEBPACK_IMPORTED_MODULE_7__["ProductCartStatus"];
    }
    // ? http://localhost:8100/#/rider/new-order-detail/294f6bf4-a2ab-4a2f-b567-62ef73216411
    ngOnInit() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.subSink.sink =
                this.activatedRoute.data.subscribe((data) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                    const { cartLocation: { locationMarker, cart } } = data;
                    if (cart === null || cart === void 0 ? void 0 : cart.id) {
                        this.cartId = cart.id;
                        this.selectedCart$ = Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["of"])(cart);
                        if ((locationMarker === null || locationMarker === void 0 ? void 0 : locationMarker.lat) && (locationMarker === null || locationMarker === void 0 ? void 0 : locationMarker.lng)) {
                            this.cartDeliveryLocation = {
                                Tag: "CUSTOMER",
                                Coordinates: locationMarker,
                                ToolTip: cart.deliveryAddress
                            };
                        }
                        // ? Get the user's Current location
                        const riderGeoPosition = yield this.locationSrv.getUserCurrentPosition();
                        if (riderGeoPosition === null || riderGeoPosition === void 0 ? void 0 : riderGeoPosition.lat) {
                            this.store.dispatch(_store_actions_rider_action__WEBPACK_IMPORTED_MODULE_6__["actions"].GetMerchantProximityToRiderInitiatedAction({
                                payload: {
                                    cartId: cart.id,
                                    latitude: riderGeoPosition.lat,
                                    longitude: riderGeoPosition.lng
                                }
                            }));
                            this.merchantInRiderProximity$ = this.store.select((riderData) => riderData.Rider.MerchantsInProximityToRider);
                            // ? Save the rider's location
                            this.riderLocation = {
                                Tag: "RIDER",
                                Coordinates: Object.assign({}, riderGeoPosition),
                                ToolTip: "My current location"
                            };
                        }
                    }
                    else {
                        this.router.navigate(["/rider", "orders"]);
                    }
                }));
            // ? Load map on the screen
            this.checkConnectionThenLoadMap();
        });
    }
    checkConnectionThenLoadMap() {
        // ? Make sure that the Rider's current location is loaded before rendering the map
        setTimeout(() => {
            var _a, _b;
            if (((_a = this.cartDeliveryLocation) === null || _a === void 0 ? void 0 : _a.Coordinates) && ((_b = this.riderLocation) === null || _b === void 0 ? void 0 : _b.Coordinates)) {
                // ? Load map to screen
                this.loadMap(this.mapSection, this.riderLocation, this.cartDeliveryLocation);
            }
            else {
                this.thenAble = this.locationSrv.getUserCurrentPosition();
                this.thenAble.then((data) => {
                    this.riderLocation = {
                        Tag: "RIDER",
                        Coordinates: Object.assign({}, data),
                        ToolTip: "My current location"
                    };
                    this.loadMap(this.mapSection, this.riderLocation, this.cartDeliveryLocation);
                });
            }
        }, 3000);
    }
    loadMap(element, riderLocation, cartLocation) {
        // ? suppressMarkers is optional
        const directionsRenderer = new google.maps.DirectionsRenderer({
            suppressMarkers: true
        });
        const directionsService = new google.maps.DirectionsService();
        if ((riderLocation === null || riderLocation === void 0 ? void 0 : riderLocation.Coordinates) && (cartLocation === null || cartLocation === void 0 ? void 0 : cartLocation.Coordinates)) {
            const map = new google.maps.Map(element.nativeElement, {
                center: new google.maps.LatLng(riderLocation.Coordinates.lat, riderLocation.Coordinates.lng),
                zoom: 10,
            });
            directionsRenderer.setMap(map);
            this.locationSrv.calculateAndDisplayRoute(directionsService, directionsRenderer, riderLocation.Coordinates, cartLocation.Coordinates);
            // ? Add map markers for rider
            const riderLocationInfo = `<h3>My current position</h3>`;
            this.locationSrv.addMapMarker(map, riderLocationInfo, riderLocationInfo, riderLocation.Coordinates, "RIDER");
            // ? Add map markers for rider
            const customerLocationInfo = `<h3>Cart position</h3>`;
            this.locationSrv.addMapMarker(map, customerLocationInfo, customerLocationInfo, cartLocation.Coordinates, "CUSTOMER");
        }
        else {
            // ? Try to reload the map of the initial load for google map fails
        }
    }
    toggleModal() {
        this.isModelOpen = !this.isModelOpen;
    }
    // acceptDelivery(cartId: string): void {
    //   if(this.userId) {
    //     this.store.dispatch(RiderActions.AcceptCartDeliveryInitiatedAction({ payload: { cartId, userId: this.userId } }));
    //     //? Navigate to live-tracking page
    //     this.subSink.sink =
    //     this.store.select((data) => data.Rider.ActiveMessage).subscribe((data) => {
    //       if(data && typeof data !== undefined) {
    //         this.router.navigate(["/rider", "orders"]);
    //       }
    //     })
    //   }
    // }
    acceptDelivery(cartId) {
        if (this.userId) {
            // ? Navigate to live-tracking page
            this.router.navigate(["/rider", "order-info", cartId]);
        }
    }
    cancelDelivery(cartId) {
        if (this.userId) {
            this.store.dispatch(_store_actions_rider_action__WEBPACK_IMPORTED_MODULE_6__["actions"].CancelCartDeliveryInitiatedAction({ payload: { cartId, userId: this.userId } }));
            // ? Navigate to live-tracking page
            this.subSink.sink =
                this.store.select((data) => data.Rider.ActiveMessage).subscribe((data) => {
                    if (data && typeof data !== undefined) {
                        this.router.navigate(["/rider", "orders"]);
                    }
                });
        }
    }
    ngOnDestroy() {
        this.subSink.unsubscribe();
    }
};
NewOrderDetailPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ngrx_store__WEBPACK_IMPORTED_MODULE_3__["Store"] },
    { type: _services_location_service__WEBPACK_IMPORTED_MODULE_8__["LocationService"] }
];
NewOrderDetailPage.propDecorators = {
    mapSection: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"], args: ["mapSection", { read: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"] },] }]
};
NewOrderDetailPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-new-order-detail",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./new-order-detail.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/rider/new-order-detail/new-order-detail.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./new-order-detail.page.scss */ "./src/app/rider/new-order-detail/new-order-detail.page.scss")).default]
    })
], NewOrderDetailPage);



/***/ })

}]);
//# sourceMappingURL=new-order-detail-new-order-detail-module-es2015.js.map