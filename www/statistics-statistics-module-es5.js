(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["statistics-statistics-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/rider/statistics/statistics.page.html":
    /*!*********************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/rider/statistics/statistics.page.html ***!
      \*********************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppRiderStatisticsStatisticsPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n  <main id=\"statistics-page\" class=\"container with-bottom-menu bg-offwhite\">\n\n    <section id=\"header\" class=\"constrain header-white\">\n      <h2>Statistics</h2>\n      <!-- <a class=\"link link-primary cursor-pointer\">\n        <span>Filter</span>\n        <img class=\"svg icon\" alt=\"filter icon\" src=\"assets/images/icons/filter.svg\">\n      </a> -->\n    </section>\n  \n    <section id=\"statistics\" class=\"constrain\" *ngFor=\"let stat of (riderDeliveryGraphData$ | async); index as i\">\n      <app-graph-widget \n        [graphData]=\"stat\" \n        [index]=\"i\">\n      </app-graph-widget>\n    </section>\n  \n    <app-footer-expander></app-footer-expander>\n    <app-rider-bottom-navbar></app-rider-bottom-navbar>\n  </main>\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/rider/statistics/statistics-routing.module.ts":
    /*!***************************************************************!*\
      !*** ./src/app/rider/statistics/statistics-routing.module.ts ***!
      \***************************************************************/

    /*! exports provided: StatisticsPageRoutingModule */

    /***/
    function srcAppRiderStatisticsStatisticsRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "StatisticsPageRoutingModule", function () {
        return StatisticsPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _statistics_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./statistics.page */
      "./src/app/rider/statistics/statistics.page.ts");

      var routes = [{
        path: '',
        component: _statistics_page__WEBPACK_IMPORTED_MODULE_3__["StatisticsPage"]
      }];

      var StatisticsPageRoutingModule = function StatisticsPageRoutingModule() {
        _classCallCheck(this, StatisticsPageRoutingModule);
      };

      StatisticsPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], StatisticsPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/rider/statistics/statistics.module.ts":
    /*!*******************************************************!*\
      !*** ./src/app/rider/statistics/statistics.module.ts ***!
      \*******************************************************/

    /*! exports provided: StatisticsPageModule */

    /***/
    function srcAppRiderStatisticsStatisticsModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "StatisticsPageModule", function () {
        return StatisticsPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _statistics_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./statistics-routing.module */
      "./src/app/rider/statistics/statistics-routing.module.ts");
      /* harmony import */


      var _statistics_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./statistics.page */
      "./src/app/rider/statistics/statistics.page.ts");
      /* harmony import */


      var _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../../shared/shared.module */
      "./src/app/shared/shared.module.ts");

      var StatisticsPageModule = function StatisticsPageModule() {
        _classCallCheck(this, StatisticsPageModule);
      };

      StatisticsPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"], _statistics_routing_module__WEBPACK_IMPORTED_MODULE_5__["StatisticsPageRoutingModule"]],
        declarations: [_statistics_page__WEBPACK_IMPORTED_MODULE_6__["StatisticsPage"]]
      })], StatisticsPageModule);
      /***/
    },

    /***/
    "./src/app/rider/statistics/statistics.page.scss":
    /*!*******************************************************!*\
      !*** ./src/app/rider/statistics/statistics.page.scss ***!
      \*******************************************************/

    /*! exports provided: default */

    /***/
    function srcAppRiderStatisticsStatisticsPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3JpZGVyL3N0YXRpc3RpY3Mvc3RhdGlzdGljcy5wYWdlLnNjc3MifQ== */";
      /***/
    },

    /***/
    "./src/app/rider/statistics/statistics.page.ts":
    /*!*****************************************************!*\
      !*** ./src/app/rider/statistics/statistics.page.ts ***!
      \*****************************************************/

    /*! exports provided: StatisticsPage */

    /***/
    function srcAppRiderStatisticsStatisticsPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "StatisticsPage", function () {
        return StatisticsPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ngrx_store__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ngrx/store */
      "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");
      /* harmony import */


      var _store_actions_rider_action__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../store/actions/rider.action */
      "./src/app/rider/store/actions/rider.action.ts");
      /* harmony import */


      var _services_location_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../../services/location.service */
      "./src/app/services/location.service.ts");
      /* harmony import */


      var _utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../../../utils/functions/app.functions */
      "./src/utils/functions/app.functions.ts");
      /* harmony import */


      var _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ../../../utils/types/app.constant */
      "./src/utils/types/app.constant.ts");

      var StatisticsPage = /*#__PURE__*/function () {
        function StatisticsPage(locationSrv, store) {
          _classCallCheck(this, StatisticsPage);

          this.locationSrv = locationSrv;
          this.store = store;
        }

        _createClass(StatisticsPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this = this;

            Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_5__["mountPlotlyScript"])();
            setTimeout(function () {
              return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                var _yield$Object, userId;

                return regeneratorRuntime.wrap(function _callee$(_context) {
                  while (1) {
                    switch (_context.prev = _context.next) {
                      case 0:
                        _context.next = 2;
                        return this.locationSrv.startGeolocationRemotely();

                      case 2:
                        _context.next = 4;
                        return Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_5__["getDataFromLocalStorage"])(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_6__["LocalStorageKey"].ZERO_30_USER);

                      case 4:
                        _yield$Object = _context.sent;
                        userId = _yield$Object.userId;

                        if (userId) {
                          this.store.dispatch(_store_actions_rider_action__WEBPACK_IMPORTED_MODULE_3__["actions"].GetRiderDeliveryGraphSummaryInitiatedAction({
                            payload: {
                              riderId: userId
                            }
                          }));
                          this.riderDeliveryGraphData$ = this.store.select(function (data) {
                            return data.Rider.RiderDeliveryGraphData;
                          });
                        }

                      case 7:
                      case "end":
                        return _context.stop();
                    }
                  }
                }, _callee, this);
              }));
            }, 2000);
          }
        }]);

        return StatisticsPage;
      }();

      StatisticsPage.ctorParameters = function () {
        return [{
          type: _services_location_service__WEBPACK_IMPORTED_MODULE_4__["LocationService"]
        }, {
          type: _ngrx_store__WEBPACK_IMPORTED_MODULE_2__["Store"]
        }];
      };

      StatisticsPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-statistics",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./statistics.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/rider/statistics/statistics.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./statistics.page.scss */
        "./src/app/rider/statistics/statistics.page.scss"))["default"]]
      })], StatisticsPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=statistics-statistics-module-es5.js.map