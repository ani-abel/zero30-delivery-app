(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["accept-manual-payment-accept-manual-payment-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/rider/accept-manual-payment/accept-manual-payment.page.html":
/*!*******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/rider/accept-manual-payment/accept-manual-payment.page.html ***!
  \*******************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n  <main id=\"order-information-page\" class=\"container\">\n      <!-- Wrapping everything in a form so it can be submitted to the database. -->\n        <section id=\"header\" class=\"constrain header-white\">\n          <a [routerLink]=\"['/rider', 'order-info', cartId]\" class=\"back link\">\n            <img class=\"svg\" src=\"assets/images/icons/arrow-left.svg\" width=\"18px\" alt=\"Go back\" />\n          </a>\n          <div class=\"text\">\n            <h2 class=\"title mb-2\">Confirm Payment</h2>\n          </div>\n        </section>\n\n        <section id=\"login-form\" class=\"constrain\">\n          <form [formGroup]=\"paymentConfirmationForm\" (ngSubmit)=\"onSubmit()\">\n              <div class=\"form-row\">\n                  <div class=\"input-password\">\n                    <select class=\"hide-shadow\" [formControlName]=\"'paymentMethod'\">\n                      <option value=\"\" selected>Select Payment method</option>\n                      <option\n                        [value]=\"selection.value\" \n                        *ngFor=\"let selection of paymentOptionSelection; index as i\">\n                        {{ selection.key }}\n                      </option>\n                    </select>\n                     <p class=\"margin-xs\" \n                          *ngIf=\"paymentConfirmationForm.get('paymentMethod').invalid && paymentConfirmationForm.get('paymentMethod').touched\">\n                          <span class=\"error-message-block\" \n                              *ngIf=\"paymentConfirmationForm.get('paymentMethod').errors['required']\">\n                              This field is required\n                          </span>\n                      </p>\n                  </div>\n              </div>\n\n              <p class=\"margin-xs mt-0\" style=\"width: 100%; display:block;\"></p>\n              <div class=\"form-row\">\n                  <button type=\"submit\" \n                    [disabled]=\"paymentConfirmationForm.invalid\" \n                    class=\"btn btn-primary\">\n                    Confirm\n                  </button>\n              </div>\n          </form>\n      </section>\n  </main>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/rider/accept-manual-payment/accept-manual-payment-routing.module.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/rider/accept-manual-payment/accept-manual-payment-routing.module.ts ***!
  \*************************************************************************************/
/*! exports provided: AcceptManualPaymentPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AcceptManualPaymentPageRoutingModule", function() { return AcceptManualPaymentPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _accept_manual_payment_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./accept-manual-payment.page */ "./src/app/rider/accept-manual-payment/accept-manual-payment.page.ts");




const routes = [
    {
        path: '',
        component: _accept_manual_payment_page__WEBPACK_IMPORTED_MODULE_3__["AcceptManualPaymentPage"]
    }
];
let AcceptManualPaymentPageRoutingModule = class AcceptManualPaymentPageRoutingModule {
};
AcceptManualPaymentPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], AcceptManualPaymentPageRoutingModule);



/***/ }),

/***/ "./src/app/rider/accept-manual-payment/accept-manual-payment.module.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/rider/accept-manual-payment/accept-manual-payment.module.ts ***!
  \*****************************************************************************/
/*! exports provided: AcceptManualPaymentPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AcceptManualPaymentPageModule", function() { return AcceptManualPaymentPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _accept_manual_payment_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./accept-manual-payment-routing.module */ "./src/app/rider/accept-manual-payment/accept-manual-payment-routing.module.ts");
/* harmony import */ var _accept_manual_payment_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./accept-manual-payment.page */ "./src/app/rider/accept-manual-payment/accept-manual-payment.page.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../shared/shared.module */ "./src/app/shared/shared.module.ts");







let AcceptManualPaymentPageModule = class AcceptManualPaymentPageModule {
};
AcceptManualPaymentPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"],
            _shared_shared_module__WEBPACK_IMPORTED_MODULE_6__["SharedModule"],
            _accept_manual_payment_routing_module__WEBPACK_IMPORTED_MODULE_4__["AcceptManualPaymentPageRoutingModule"]
        ],
        declarations: [_accept_manual_payment_page__WEBPACK_IMPORTED_MODULE_5__["AcceptManualPaymentPage"]]
    })
], AcceptManualPaymentPageModule);



/***/ }),

/***/ "./src/app/rider/accept-manual-payment/accept-manual-payment.page.scss":
/*!*****************************************************************************!*\
  !*** ./src/app/rider/accept-manual-payment/accept-manual-payment.page.scss ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("#order-information-page #header {\n  grid-gap: 10px;\n}\n\nselect {\n  background: #fff;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmlkZXIvYWNjZXB0LW1hbnVhbC1wYXltZW50L2FjY2VwdC1tYW51YWwtcGF5bWVudC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxjQUFBO0FBQ0o7O0FBRUE7RUFDSSxnQkFBQTtBQUNKIiwiZmlsZSI6InNyYy9hcHAvcmlkZXIvYWNjZXB0LW1hbnVhbC1wYXltZW50L2FjY2VwdC1tYW51YWwtcGF5bWVudC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjb3JkZXItaW5mb3JtYXRpb24tcGFnZSAjaGVhZGVyIHtcbiAgICBncmlkLWdhcDogMTBweDtcbn1cblxuc2VsZWN0IHtcbiAgICBiYWNrZ3JvdW5kOiAjZmZmO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/rider/accept-manual-payment/accept-manual-payment.page.ts":
/*!***************************************************************************!*\
  !*** ./src/app/rider/accept-manual-payment/accept-manual-payment.page.ts ***!
  \***************************************************************************/
/*! exports provided: AcceptManualPaymentPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AcceptManualPaymentPage", function() { return AcceptManualPaymentPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ngrx_store__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngrx/store */ "./node_modules/@ngrx/store/__ivy_ngcc__/fesm2015/ngrx-store.js");
/* harmony import */ var subsink__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! subsink */ "./node_modules/subsink/dist/es2015/index.js");
/* harmony import */ var _store_actions_rider_action__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../store/actions/rider.action */ "./src/app/rider/store/actions/rider.action.ts");
/* harmony import */ var _utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../utils/functions/app.functions */ "./src/utils/functions/app.functions.ts");
/* harmony import */ var _utils_types_app_constant__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../utils/types/app.constant */ "./src/utils/types/app.constant.ts");
/* harmony import */ var _user_store_model_user_model__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../user/store/model/user.model */ "./src/app/user/store/model/user.model.ts");










let AcceptManualPaymentPage = class AcceptManualPaymentPage {
    constructor(activatedRoute, router, store) {
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.store = store;
        this.subSink = new subsink__WEBPACK_IMPORTED_MODULE_5__["SubSink"]();
    }
    ngOnInit() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.initForm();
            const keyValuePair = Object.keys(_user_store_model_user_model__WEBPACK_IMPORTED_MODULE_9__["PaymentMethodType"])
                .filter((data) => _user_store_model_user_model__WEBPACK_IMPORTED_MODULE_9__["PaymentMethodType"][data] !== _user_store_model_user_model__WEBPACK_IMPORTED_MODULE_9__["PaymentMethodType"].PAYSTACK)
                .map((data) => {
                return {
                    key: data,
                    value: _user_store_model_user_model__WEBPACK_IMPORTED_MODULE_9__["PaymentMethodType"][data]
                };
            });
            this.paymentOptionSelection = keyValuePair.slice(Math.ceil(keyValuePair.length / 2));
            const userData = yield Object(_utils_functions_app_functions__WEBPACK_IMPORTED_MODULE_7__["getDataFromLocalStorage"])(_utils_types_app_constant__WEBPACK_IMPORTED_MODULE_8__["LocalStorageKey"].ZERO_30_USER);
            if (userData) {
                this.userId = userData.userId;
            }
            this.subSink.sink =
                this.activatedRoute
                    .params
                    .subscribe((data) => {
                    this.cartId = data.cartId;
                });
        });
    }
    initForm() {
        this.paymentConfirmationForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            paymentMethod: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]("", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
        });
    }
    onSubmit() {
        if (this.paymentConfirmationForm.invalid) {
            return;
        }
        const { paymentMethod } = this.paymentConfirmationForm.value;
        if (this.userId && this.cartId) {
            this.store.dispatch(_store_actions_rider_action__WEBPACK_IMPORTED_MODULE_6__["actions"].ConfirmManualPaymentInitiatedAction({
                payload: {
                    cartId: this.cartId,
                    riderId: this.userId,
                    paymentOption: paymentMethod
                }
            }));
            // ? Mark the delivery as complete
            this.store.dispatch(_store_actions_rider_action__WEBPACK_IMPORTED_MODULE_6__["actions"].MarkDeliveryAsCompleteInitiatedAction({ payload: { cartId: this.cartId } }));
            this.subSink.sink =
                this.store
                    .select((data) => data.Rider.ActiveMessage)
                    .subscribe((data) => {
                    if (data) {
                        this.router.navigate(["/rider", "order-info", this.cartId]);
                    }
                });
        }
    }
    ngOnDestroy() {
        this.subSink.unsubscribe();
    }
};
AcceptManualPaymentPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _ngrx_store__WEBPACK_IMPORTED_MODULE_4__["Store"] }
];
AcceptManualPaymentPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-accept-manual-payment",
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./accept-manual-payment.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/rider/accept-manual-payment/accept-manual-payment.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./accept-manual-payment.page.scss */ "./src/app/rider/accept-manual-payment/accept-manual-payment.page.scss")).default]
    })
], AcceptManualPaymentPage);



/***/ })

}]);
//# sourceMappingURL=accept-manual-payment-accept-manual-payment-module-es2015.js.map