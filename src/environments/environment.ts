// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiRoot: "http://udeolisa-001-site1.gtempurl.com",
  apiRoot2: "https://b9d26a5093eb.ngrok.io",
  websocketRoot: "http://udeolisa-001-site1.gtempurl.com/chat",
  googleAPIKey: "AIzaSyDGi8jX0dEEC8C7M6dWXdc6Rjo94fLaMGU",
  mapQuestAPIKey: "lYrP4vF3Uk5zgTiGGuEzQGwGIVDGuy24",
  paystackPublicTestKey: "pk_test_deb472537dfaa30e50cb8e612fca9e3ff333f341",
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
