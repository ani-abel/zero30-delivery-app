import { Plugins } from "@capacitor/core";
import { RiderCartType } from "../../app/rider/store/model/rider.model";
import {
    CheckedOutProduct,
    MerchantModel,
    ProductCartStatus,
    ProductCheckout,
    ProductOrdersGroupType,
    ProductOrdersResultType,
    UpdateUserPayloadType,
    UserBookmark,
    UserModel
} from "../../app/user/store/model/user.model";
import { LocalStorageKey } from "../types/app.constant";

const { Storage } = Plugins;

export const saveDataToLocalStorage = async <T>(payload: T, key: string): Promise<void> => {
    await Storage.set({
        key,
        value: JSON.stringify(payload)
      });
};

export const getDataFromLocalStorage = async <T>(key: string): Promise<T> => {
    const ret = await Storage.get({ key });
    return JSON.parse(ret.value);
};

export const deleteDataFromLocalStorage = async (key: string): Promise<void> => await Storage.remove({ key });

export const findBookmarkInLocalStorage = (id: string, allBookmarks: UserBookmark[]): UserBookmark => {
    return allBookmarks.find((data) => data.Id === id);
};

export const addBookmarkToLocalStorage = async (payload: UserBookmark): Promise<void> => {
    const bookmarks: UserBookmark[] = await getDataFromLocalStorage(LocalStorageKey.BOOKMARKS) || [];
    if (!findBookmarkInLocalStorage(payload.Id, bookmarks)) {
        bookmarks.push(payload);
        await saveDataToLocalStorage(bookmarks, LocalStorageKey.BOOKMARKS);
    }
};

/**
 * Get the bookmark from localstorage
 * @param id
 */
export const findUserBookmark = async (id: string): Promise<UserBookmark> => {
    const bookmarks: UserBookmark[] = await getDataFromLocalStorage(LocalStorageKey.BOOKMARKS) || [];
    return findBookmarkInLocalStorage(id, bookmarks);
};


/**
 * Update the user details that were changed by a user in state before,
 * updating online
 * @param updatedUserData
 * @param userData
 */
export const swapUserDataAfterUpdate = (
    updatedUserData: UpdateUserPayloadType, 
    userData: UserModel
): UserModel => {
    const { address, dob, email, firstName, lastName } = updatedUserData;
    const newUserData: UserModel = {
        ...userData,
        username: email,
        person: {
            ...userData.person,
            address,
            firstName,
            lastName,
            dob
        },
    };
    return newUserData;
};

export const capitalizeFirstCharacter = (s: string): string => {
    if (typeof s !== "string") {
        return "";
    }
    return `${s.charAt(0).toUpperCase()}${s.slice(1)?.toLowerCase()}`;
};

export const addProductToCart = (
    newProduct: ProductCheckout,
    cartItems: CheckedOutProduct,
    userId: string,
    deliveryAddress: string
): CheckedOutProduct => {
    try {
        // ? Check if this value is undefined
        // ? Initialize the store
        if (typeof cartItems === "undefined" || !cartItems) {
            cartItems = {
                userId,
                deliveryAddress,
                productCheckouts: [{ ...newProduct }],
            };
        }
        else {
            // ? Add a new product to store
            // ? Check if that product already exists
            // ? If (true) Increase the quantity and save
            const productExistIndex: number = cartItems.productCheckouts.findIndex((product) => product.productId === newProduct.productId);
            if (productExistIndex !== -1) {
                const updatedProducts: ProductCheckout[] = cartItems?.productCheckouts?.map((product, index) => {
                    if (productExistIndex === index) {
                        return {
                            ...product,
                            quantity: newProduct.quantity,
                            cost: newProduct.cost
                        };
                    }
                    else {
                        return { ...product };
                    }
                });
                cartItems = {
                    ...cartItems,
                    productCheckouts: [...updatedProducts]
                };
            }
            else {
                cartItems = {
                    ...cartItems,
                    productCheckouts: [...cartItems.productCheckouts, newProduct]
                };
            }
        }
        return { ...cartItems };
    }
    catch (ex) {
        throw ex;
    }
};

export const removeProductFromCart = (
    productId: string,
    cartItems: CheckedOutProduct
): CheckedOutProduct => {
    if (cartItems?.productCheckouts?.length > 0) {
        /**
         * Splice mutates the state of the array it's used on
         */
        const productCheckouts = cartItems?.productCheckouts?.filter((product) => product.productId !== productId);
        return {
            ...cartItems,
            productCheckouts: [...productCheckouts]
        };
    }
    return { ...cartItems };
};

export const groupUserOrders = (payload: ProductOrdersResultType[]): ProductOrdersGroupType => {
    try {
        const verifiedOrders: ProductOrdersResultType[] = payload?.filter((data) => data.status ===  ProductCartStatus.VERIFIED);
        const unVerifiedOrders: ProductOrdersResultType[] = payload?.filter((data) => data.status ===  ProductCartStatus.UNVERIFIED);
        const cancelledOrders: ProductOrdersResultType[] = payload?.filter((data) => data.status ===  ProductCartStatus.CANCELLED);
        const transitOrders: ProductOrdersResultType[] = payload?.filter((data) => data.status ===  ProductCartStatus.TRANSIT);
        const completedOrders: ProductOrdersResultType[] = payload?.filter((data) => data.status ===  ProductCartStatus.COMPLETED);

        return {
            cancelled: [...cancelledOrders] ?? [],
            completed: [...completedOrders] ?? [],
            transit: [...transitOrders, ...verifiedOrders] ?? [], // New
            unverified: [...unVerifiedOrders] ?? [],
            verified: [...verifiedOrders] ?? []
        };
    }
    catch (ex) {
        throw ex;
    }
};

export const cancelCartDelivery = (
    cartId: string,
    allDeliveries: RiderCartType[],
    ongoingDeliveries: RiderCartType[]
): { Ongoing: RiderCartType[], All: RiderCartType[] } => {
    try {
        const allDeliveriesRemap = allDeliveries.filter((data) => data?.cart.id !== cartId);
        const ongoingDeliveriesRemap = ongoingDeliveries.filter((data) => data?.cart.id !== cartId);

        return {
            All: [...allDeliveriesRemap],
            Ongoing: [...ongoingDeliveriesRemap]
        };
    }
    catch (ex) {
        throw ex;
    }
};

export const markCartAsInTransit = (
    cartId: string,
    allDeliveries: RiderCartType[],
    ongoingDeliveries: RiderCartType[],
    status: ProductCartStatus,
): { Ongoing: RiderCartType[], All: RiderCartType[] } => {
    try {
        const ongoingDeliveriesRemap: RiderCartType[] = ongoingDeliveries.map((data) => {
            if (data.cart.id === cartId) {
                return {
                    ...data,
                    cart: {
                        ...data.cart,
                        status,
                    }
                };
            }
            else {
                return { ...data };
            }
        });

        const allDeliveriesRemap: RiderCartType[] = allDeliveries.map((data) => {
            if (data.cart.id === cartId) {
                return {
                    ...data,
                    cart: {
                        ...data.cart,
                        status,
                    }
                };
            }
            else {
                return { ...data };
            }
        });

        return {
            All: [...allDeliveriesRemap],
            Ongoing: [...ongoingDeliveriesRemap]
        };
    }
    catch (ex) {
        throw ex;
    }
};

export const pushUniqueArrayValues = <T>(
    newElement: T,
    existingElement: T[],
    placeAt: "END" | "START" = "END"
): T[] =>  {
    try {
        let returnedData: T[];
        const elementExists: T = existingElement.find((data) => data === newElement);
        if (elementExists) {
            returnedData = [...existingElement];
        }
        else {
            if (placeAt === "END") {
                returnedData = [...existingElement, newElement];
            }
            else {
                returnedData = [newElement, ...existingElement];
            }
        }
        return returnedData;
    }
    catch (ex) {
        throw ex;
    }
};

export const filterMerchantByType = (
    merchantModels: MerchantModel[],
    typeId: number
) : MerchantModel[] => {
    try {
        return merchantModels.filter((merchant) => merchant.merchantTypeModel.id === typeId);
    }
    catch (ex) {
        throw ex;
    }
}

export const clearObjectValues = <T>(objToClear: T): T => {
    Object.keys(objToClear).forEach((param) => {
        if ( (objToClear[param]).toString() === "[object Object]" ) {
            clearObjectValues(objToClear[param]);
        } else {
            objToClear[param] = undefined;
        }
    });
    return objToClear;
};

export const mountGoogleMapScript = (apiKey: string, scriptId: string = "googleMaps"): void => {
    // First check if the script already exists on the dom
    // by searching for an id
    if (navigator.onLine) {
        if (document.getElementById(scriptId) === null) {
            const script: HTMLScriptElement = document.createElement("script");
            script.setAttribute("src", `http://maps.googleapis.com/maps/api/js?key=${apiKey}`);
            script.setAttribute("id", scriptId);
            document.body.appendChild(script);

            // now wait for it to load...
            script.onload = () => {
                // script has loaded, you can now use it safely
                console.log({ message: "Google map loaded successfully" });
                // ... do something with the newly loaded script
            };
        }
    }
};

export const mountPlotlyScript = (scriptId: string = "plotly"): void => {
    // First check if the script already exists on the dom
    // by searching for an id
    if (navigator.onLine) {
        if (document.getElementById(scriptId) === null) {
            const script: HTMLScriptElement = document.createElement("script");
            script.setAttribute("src", `https://cdn.plot.ly/plotly-latest.min.js`);
            script.setAttribute("id", scriptId);
            document.body.appendChild(script);

            // now wait for it to load...
            script.onload = () => {
                // script has loaded, you can now use it safely
                console.log({ message: "PlotlyJS CDN loaded successfully" });
                // ... do something with the newly loaded script
            };
        }
    }
};

export const isScriptImported = (scriptId: "plotly" | "googleMaps"): boolean => {
    return (document.querySelector(`#${scriptId}`) as HTMLScriptElement) === null ? false : true;
};
