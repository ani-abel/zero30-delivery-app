import { RiderState } from '../../app/rider/store/model/rider.model';
import { UserState } from '../../app/user/store/model/user.model';
import { AuthState } from '../../app/auth/store/model/auth.model';

export interface AppState {
    readonly Auth: AuthState;
    readonly User: UserState;
    readonly Rider: RiderState;
}