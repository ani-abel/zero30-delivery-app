export const APIMessage = {
  BAD_OPERATION: "This operation is not supported",
  RECORD_EXISTS: "Record already exists",
  SUCCESSFUL: "Successful",
  NOT_FOUND: "Record not found",
  UNAUTHORIZED_REQUEST: "You are not authorized to view this route",
  INACTIVE_ACCOUNT: "Your account is inactive",
  EXPIRED_TOKEN_MESSAGE: "Unauthorized access. You are using an expired token",
  UNAUTHORIZED_ERROR_MESSAGE: "Unauthorized Access",
  CREATED: "Created",
  DELETED: "Deleted",
  UPDATE: "Updated",
  ROLE_MISMATCH: "This feature does not work this role",
  SET_GROUP_LIMIT: "Specify how many user(s) can share this group plan",
  VALIDATION_SUCCESSFUL: "Validation successful",
};

export enum Zero30Role {
  CUSTOMER = "CUSTOMER",
  RIDER = "RIDER"
}

export enum LocalStorageKey {
  ZERO_30_USER = "Zero30User",
  BOOKMARKS = "Bookmarks",
  CART = "CART",
  DEVICE_KEY = "DEVICE_KEY",
  URL_HISTORY = "URL_HISTORY",
}

export enum TransportType {
  DRIVING = "DRIVING",
  WALKING = "WALKING",
  TRANSIT = "TRANSIT",
  BICYCLING = "BICYCLING",
}

export enum PushNotificationCategory {
  CART_ASSIGNMENT = "0",
  CART_VERIFICATION = "1",
  PRODUCT_REMOVAL = "2",
}

export interface NotificationPayloadType {
  Id: number; // 1 - 3
  Title: string;
  Body: string;
  ExtraData: ExtraNotficationData;
}

export interface ExtraNotficationData {
  Id: string;
  SecondaryId?: string;
  PushNotificationType: PushNotificationCategory;
}

export enum PushNotificationEvent {
  PUSH_REGISTRATION = "registration",
  PUSH_REGISTRATION_ERROR = "registrationError",
  PUSH_NOTIFICATION_RECIEVED = "pushNotificationReceived",
  PUSH_NOTIFICATION_ACTION = "pushNotificationActionPerformed",
}
