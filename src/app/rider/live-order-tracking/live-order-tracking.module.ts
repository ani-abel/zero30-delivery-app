import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LiveOrderTrackingPageRoutingModule } from './live-order-tracking-routing.module';

import { LiveOrderTrackingPage } from './live-order-tracking.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LiveOrderTrackingPageRoutingModule
  ],
  declarations: [LiveOrderTrackingPage]
})
export class LiveOrderTrackingPageModule {}
