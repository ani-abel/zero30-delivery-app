import { Component, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Store } from "@ngrx/store";
import { Observable } from "rxjs";
import { SubSink } from "subsink";
import { actions as RiderAction } from "../store/actions/rider.action";
import { ProductCartStatus } from "../../user/store/model/user.model";
import { RiderCartType } from "../store/model/rider.model";
import { AppState } from "../../../utils/types/app.model";
import { getDataFromLocalStorage } from "../../../utils/functions/app.functions";
import { AuthResponseType } from "../../../app/auth/store/model/auth.model";
import { LocalStorageKey } from "../../../utils/types/app.constant";
import { NavigationService } from "../../../app/services/navigation.service";

declare function unBundleSVG();

@Component({
  selector: "app-orders",
  templateUrl: "./orders.page.html",
  styleUrls: ["./orders.page.scss"],
})
export class OrdersPage implements
OnInit,
OnDestroy {
  activeTabType: ProductCartStatus = ProductCartStatus.TRANSIT;
  productCartStatus = ProductCartStatus;
  subSink: SubSink = new SubSink();
  completedDeliveries$: Observable<RiderCartType[]>;
  ongoingDeliveries$: Observable<RiderCartType[]>;
  allDeliveries$: Observable<RiderCartType[]>;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly store: Store<AppState>,
    private readonly navigationSrv: NavigationService,
  ) { }

  async ngOnInit(): Promise<void> {
    const { userId } = await getDataFromLocalStorage<AuthResponseType>(LocalStorageKey.ZERO_30_USER);
    unBundleSVG();
    this.subSink.sink =
    this.activatedRoute
        .queryParams
        .subscribe((data) => {
          if (data?.type) {
            this.activeTabType = data.type;
          }
        });

    if (userId) {
      this.store.dispatch(RiderAction.GetRiderActivityHistoryInitiatedAction({ payload: { userId } }));
      this.completedDeliveries$ = this.store.select((data) => data.Rider.CompletedDeliveries);
      this.ongoingDeliveries$ = this.store.select((data) => data.Rider.OngoingDeliveries);
      this.allDeliveries$ = this.store.select((data) => data.Rider.DeliveryActivities);
    }
  }

  switchTabs(tab: ProductCartStatus): void {
    this.activeTabType = tab;
  }

  goBackHistory(): void {
    this.navigationSrv.goBackOnNative();
  }

  ngOnDestroy(): void {
    this.subSink.unsubscribe();
  }
}
