import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { CartLocationResolver } from "../resolvers/cart-location.resolver";

const routes: Routes = [
    { path: "", children: [
        { path: "", pathMatch: "full", redirectTo: "home" },
        {
          path: "home",
          loadChildren: () => import("./home/home.module").then( m => m.HomePageModule)
        },
        {
          path: "statistics",
          loadChildren: () => import("./statistics/statistics.module").then( m => m.StatisticsPageModule)
        },
        {
          path: "orders",
          loadChildren: () => import("./orders/orders.module").then( m => m.OrdersPageModule)
        },
        {
          path: "order-info/:cartId",
          loadChildren: () => import("./order-info/order-info.module").then( m => m.OrderInfoPageModule)
        },
        {
          path: "new-order-detail/:cartId",
          resolve: {
            cartLocation: CartLocationResolver
        },
          loadChildren: () => import("./new-order-detail/new-order-detail.module").then( m => m.NewOrderDetailPageModule)
        },
        {
          path: "accept-manual-payment/:cartId",
          loadChildren: () => import("./accept-manual-payment/accept-manual-payment.module").then( m => m.AcceptManualPaymentPageModule)
        },
        {
          path: "view-cart-detail/:cartId",
          loadChildren: () => import("./view-cart-detail/view-cart-detail.module").then( m => m.ViewCartDetailPageModule)
        },
      ]},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RiderRoutingModule { }
