import { Component, OnInit, OnDestroy } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { Store } from "@ngrx/store";
import { SubSink } from "subsink";
import { actions as RiderActions } from "../store/actions/rider.action";
import { AuthResponseType } from "../../../app/auth/store/model/auth.model";
import { getDataFromLocalStorage } from "../../../utils/functions/app.functions";
import { LocalStorageKey } from "../../../utils/types/app.constant";
import { AppState } from "../../../utils/types/app.model";
import { PaymentMethodType } from "../../user/store/model/user.model";

type KeyValueType = {
  key: string,
  value: number
};

@Component({
  selector: "app-accept-manual-payment",
  templateUrl: "./accept-manual-payment.page.html",
  styleUrls: ["./accept-manual-payment.page.scss"],
})
export class AcceptManualPaymentPage implements
OnInit,
OnDestroy {
  paymentConfirmationForm: FormGroup;
  paymentOptionSelection: KeyValueType[];
  subSink: SubSink = new SubSink();
  userId: string;
  cartId: string;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly store: Store<AppState>,
  ) { }

  async ngOnInit(): Promise<void> {
    this.initForm();
    const keyValuePair = Object.keys(PaymentMethodType)
      .filter((data) => PaymentMethodType[data] !== PaymentMethodType.PAYSTACK)
      .map((data: string) => {
        return {
          key: data,
          value: PaymentMethodType[data]
        };
    });
    this.paymentOptionSelection = keyValuePair.slice(Math.ceil(keyValuePair.length / 2));

    const userData: AuthResponseType = await getDataFromLocalStorage<AuthResponseType>(LocalStorageKey.ZERO_30_USER);
    if (userData) {
      this.userId = userData.userId;
    }

    this.subSink.sink =
    this.activatedRoute
        .params
        .subscribe((data) => {
          this.cartId = data.cartId;
      });
  }

  initForm(): void {
    this.paymentConfirmationForm = new FormGroup({
      paymentMethod: new FormControl("", Validators.required),
    });
  }

  onSubmit(): void {
    if (this.paymentConfirmationForm.invalid) {
      return;
    }
    const { paymentMethod } = this.paymentConfirmationForm.value;
    if (this.userId && this.cartId) {
      this.store.dispatch(RiderActions.ConfirmManualPaymentInitiatedAction({
        payload: {
          cartId: this.cartId,
          riderId: this.userId,
          paymentOption: paymentMethod
        }
      }));

      // ? Mark the delivery as complete
      this.store.dispatch(RiderActions.MarkDeliveryAsCompleteInitiatedAction({ payload: { cartId: this.cartId } }));

      this.subSink.sink =
      this.store
          .select((data) => data.Rider.ActiveMessage)
          .subscribe((data) => {
            if (data) {
              this.router.navigate(["/rider", "order-info", this.cartId]);
            }
          });
    }
  }

  ngOnDestroy(): void {
    this.subSink.unsubscribe();
  }

}
