import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { AcceptManualPaymentPageRoutingModule } from "./accept-manual-payment-routing.module";

import { AcceptManualPaymentPage } from "./accept-manual-payment.page";
import { SharedModule } from "../../shared/shared.module";

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    SharedModule,
    AcceptManualPaymentPageRoutingModule
  ],
  declarations: [AcceptManualPaymentPage]
})
export class AcceptManualPaymentPageModule {}
