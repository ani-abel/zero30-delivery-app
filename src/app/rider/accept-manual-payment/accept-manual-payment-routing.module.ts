import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AcceptManualPaymentPage } from './accept-manual-payment.page';

const routes: Routes = [
  {
    path: '',
    component: AcceptManualPaymentPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AcceptManualPaymentPageRoutingModule {}
