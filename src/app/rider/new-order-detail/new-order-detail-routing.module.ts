import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewOrderDetailPage } from './new-order-detail.page';

const routes: Routes = [
  {
    path: '',
    component: NewOrderDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewOrderDetailPageRoutingModule {}
