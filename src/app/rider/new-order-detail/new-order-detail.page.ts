import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  OnDestroy
} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Store } from "@ngrx/store";
import {} from "googlemaps";
import { SubSink } from "subsink";
import { Observable, of } from "rxjs";
import { actions as RiderActions } from "../store/actions/rider.action";
import { AppState } from "../../../utils/types/app.model";
import {
  GeoPointType,
  LocationAbbrType,
  MerchantProximityType,
  SingleCartType
} from "../store/model/rider.model";
import { ProductCartStatus } from "../../user/store/model/user.model";
import { LocationService } from "../../services/location.service";

export type MapListDetail = {
  Locations: LocationAbbrType[];
  Element: ElementRef,
  Tooltip: string,
};

@Component({
  selector: "app-new-order-detail",
  templateUrl: "./new-order-detail.page.html",
  styleUrls: ["./new-order-detail.page.scss"],
})
export class NewOrderDetailPage implements
OnInit,
OnDestroy {
  @ViewChild("mapSection", { read: ElementRef }) mapSection: ElementRef;
  thenAble: Promise<{ lat: number, lng: number }>;
  isModelOpen = true;
  cartId: string;
  subSink: SubSink = new SubSink();
  selectedCart$: Observable<SingleCartType>;
  merchantInRiderProximity$: Observable<MerchantProximityType[]>;
  deliveryDestination$: Observable<LocationAbbrType>;
  deliveryDestination: LocationAbbrType;
  userId: string;
  productStatus = ProductCartStatus;
  riderLocation: GeoPointType;
  cartDeliveryLocation: GeoPointType;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly store: Store<AppState>,
    private readonly locationSrv: LocationService
  ) { }

  // ? http://localhost:8100/#/rider/new-order-detail/294f6bf4-a2ab-4a2f-b567-62ef73216411

  async ngOnInit(): Promise<void> {
    this.subSink.sink =
    this.activatedRoute.data.subscribe(async (data) => {
      const { cartLocation: { locationMarker, cart } } = data;
      if (cart?.id) {
        this.cartId = cart.id;
        this.selectedCart$ = of(cart);

        if (locationMarker?.lat && locationMarker?.lng) {
          this.cartDeliveryLocation = {
            Tag: "CUSTOMER",
            Coordinates: locationMarker,
            ToolTip: cart.deliveryAddress
          };
        }

        // ? Get the user's Current location
        const riderGeoPosition: LocationAbbrType = await this.locationSrv.getUserCurrentPosition();
        if (riderGeoPosition?.lat) {
          this.store.dispatch(RiderActions.GetMerchantProximityToRiderInitiatedAction({
            payload: {
              cartId: cart.id,
              latitude: riderGeoPosition.lat,
              longitude: riderGeoPosition.lng
            }
          }));
          this.merchantInRiderProximity$ = this.store.select((riderData) => riderData.Rider.MerchantsInProximityToRider);
          // ? Save the rider's location
          this.riderLocation = {
            Tag: "RIDER",
            Coordinates: { ...riderGeoPosition },
            ToolTip: "My current location"
          };
        }
      } else {
        this.router.navigate(["/rider", "orders"]);
      }
    });

    // ? Load map on the screen
    this.checkConnectionThenLoadMap();
  }

  private checkConnectionThenLoadMap(): void {
    // ? Make sure that the Rider's current location is loaded before rendering the map
    setTimeout(() => {
      if (
        this.cartDeliveryLocation?.Coordinates
        && this.riderLocation?.Coordinates
      ) {
        // ? Load map to screen
        this.loadMap(this.mapSection, this.riderLocation, this.cartDeliveryLocation);
      }
      else {
        this.thenAble = this.locationSrv.getUserCurrentPosition();
        this.thenAble.then((data) => {
          this.riderLocation = {
            Tag: "RIDER",
            Coordinates: { ...data },
            ToolTip: "My current location"
          };
          this.loadMap(this.mapSection, this.riderLocation, this.cartDeliveryLocation);
        });
      }
    }, 3000);
  }

  private loadMap(
    element: ElementRef,
    riderLocation: GeoPointType,
    cartLocation: GeoPointType
  ): void {
    // ? suppressMarkers is optional
    const directionsRenderer = new google.maps.DirectionsRenderer({
      suppressMarkers: true
    });
    const directionsService = new google.maps.DirectionsService();

    if (riderLocation?.Coordinates && cartLocation?.Coordinates) {
      const map = new google.maps.Map(element.nativeElement, {
        center: new google.maps.LatLng(riderLocation.Coordinates.lat, riderLocation.Coordinates.lng),
        zoom: 10,
      });
      directionsRenderer.setMap(map);
      this.locationSrv.calculateAndDisplayRoute(
        directionsService,
        directionsRenderer,
        riderLocation.Coordinates,
        cartLocation.Coordinates
      );

      // ? Add map markers for rider
      const riderLocationInfo = `<h3>My current position</h3>`;
      this.locationSrv.addMapMarker(map, riderLocationInfo, riderLocationInfo, riderLocation.Coordinates, "RIDER");

      // ? Add map markers for rider
      const customerLocationInfo = `<h3>Cart position</h3>`;
      this.locationSrv.addMapMarker(map, customerLocationInfo, customerLocationInfo, cartLocation.Coordinates, "CUSTOMER");
    }
    else {
      // ? Try to reload the map of the initial load for google map fails
    }
  }

  toggleModal(): void {
    this.isModelOpen = !this.isModelOpen;
  }

  // acceptDelivery(cartId: string): void {
  //   if(this.userId) {
  //     this.store.dispatch(RiderActions.AcceptCartDeliveryInitiatedAction({ payload: { cartId, userId: this.userId } }));
  //     //? Navigate to live-tracking page
  //     this.subSink.sink =
  //     this.store.select((data) => data.Rider.ActiveMessage).subscribe((data) => {
  //       if(data && typeof data !== undefined) {
  //         this.router.navigate(["/rider", "orders"]);
  //       }
  //     })
  //   }
  // }

  acceptDelivery(cartId: string): void {
    if (this.userId) {
      // ? Navigate to live-tracking page
      this.router.navigate(["/rider", "order-info", cartId]);
    }
  }

  cancelDelivery(cartId: string): void {
    if (this.userId) {
      this.store.dispatch(RiderActions.CancelCartDeliveryInitiatedAction({ payload: { cartId, userId: this.userId } }));
      // ? Navigate to live-tracking page
      this.subSink.sink =
      this.store.select((data) => data.Rider.ActiveMessage).subscribe((data) => {
        if (data && typeof data !== undefined) {
          this.router.navigate(["/rider", "orders"]);
        }
      });
    }
  }

  ngOnDestroy(): void {
    this.subSink.unsubscribe();
  }

}
