import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NewOrderDetailPageRoutingModule } from './new-order-detail-routing.module';

import { NewOrderDetailPage } from './new-order-detail.page';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    NewOrderDetailPageRoutingModule
  ],
  declarations: [NewOrderDetailPage]
})
export class NewOrderDetailPageModule {}
