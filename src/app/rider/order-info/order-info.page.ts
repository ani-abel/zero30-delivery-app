import { Component, OnInit, OnDestroy } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { ModalController } from "@ionic/angular";
import { Store } from "@ngrx/store";
import { SubSink } from "subsink";
import { Observable, of } from "rxjs";
import { actions as RiderActions } from "../store/actions/rider.action";
import { AppState } from "../../../utils/types/app.model";
import { ProductCartStatus, ProductOrdersResultType } from "../../user/store/model/user.model";
import { CartCheckListType, MerchantProximityType, OrderInfoType } from "../store/model/rider.model";
import { getDataFromLocalStorage } from "../../../utils/functions/app.functions";
import { actions as UserActions } from "../../user/store/actions/user.action";
import { AuthResponseType } from "../../auth/store/model/auth.model";
import { LocationService } from "../../services/location.service";
import { LocalStorageKey } from "../../../utils/types/app.constant";
import { ViewCartDetailPage } from "../view-cart-detail/view-cart-detail.page";
import { tick } from "@angular/core/testing";

@Component({
  selector: "app-order-info",
  templateUrl: "./order-info.page.html",
  styleUrls: ["./order-info.page.scss"],
})
export class OrderInfoPage implements
OnInit,
OnDestroy {
  productStatus = ProductCartStatus;
  orderInfo$: Observable<OrderInfoType>;
  merchantsRelatedToCart$: Observable<MerchantProximityType[]>;
  selectedCart$: Observable<ProductOrdersResultType>;
  subSink: SubSink = new SubSink();
  cartId: string;
  userId: string;

  constructor(
    private readonly store: Store<AppState>,
    private readonly activatedRoute: ActivatedRoute,
    private readonly locationSrv: LocationService,
    public modalController: ModalController,
  ) { }

  async ngOnInit(): Promise<void> {
    this.subSink.sink =
    this.activatedRoute.params.subscribe(async (data) => {
      // ? Start the geolocation beacon and emit the rider location data, if they have any carts marked as "IN_TRANSIT"
      await this.locationSrv.startGeolocationRemotely();

      this.cartId = data.cartId;
      this.store.dispatch(RiderActions.GetOrderInfoInitiatedAction({ payload: { cartId: data.cartId } }));
      this.orderInfo$ = this.store.select((storeData) => storeData.Rider.SelectedOrderInfo);

      this.store.dispatch(RiderActions.GetMerchantsRelatedToCartInitiatedAction({ payload: { cartId: data.cartId } }));
      this.merchantsRelatedToCart$ = this.store.select((storeData) => storeData.Rider.MerchantsRelatedToCart);

      // ? Get the content of all items in the cart
      this.store.dispatch(UserActions.GetSelectedCartInitiatedAction({ payload: { cartId: data.cartId } }));
      this.selectedCart$ = this.store.select((storeData) => storeData.User.SelectedCart);
    });

    const userData: AuthResponseType = await getDataFromLocalStorage<AuthResponseType>(LocalStorageKey.ZERO_30_USER);
    if (userData.userId) {
      this.userId = userData.userId;
    }
  }

  async emitRiderLocation(cartId: string): Promise<void> {
    if (this.userId) {
      this.store.dispatch(RiderActions.AcceptCartDeliveryInitiatedAction({ payload: { cartId, userId: this.userId } }));
      await this.locationSrv.startGeolocationRemotely();

      // TODO Update the status of selectedCart, once the user selects it
      // this.store.dispatch(RiderActions.GetOrderInfoInitiatedAction({ payload: { cartId: this.cartId } }));
      // this.orderInfo$ = this.store.select((data) => data.Rider.SelectedOrderInfo);

      this.subSink.sink =
      this.store
          .select((data) => data.Rider.ActiveMessage)
          .subscribe((data) => {
            if (data) {
              this.subSink.sink =
                this.orderInfo$.subscribe((currentData) => {
                  this.orderInfo$ = of(this.updateCartState(currentData));
                });
            }
          });
    }
  }

  openCartCheckListModal(): void {
    this.store.dispatch(RiderActions.GetCartCheckListInitiatedAction({ payload: { cartId: this.cartId } }));
    const cartCheckList$: Observable<CartCheckListType[]> = this.store.select((data) => data.Rider.SelectedCartCheckList);

    this.subSink.sink =
    cartCheckList$.subscribe(async (data) => {
      if (data?.length > 0) {
        const modal = await this.modalController.create({
          component: ViewCartDetailPage,
          componentProps: {
            cartCheckLists: [...data],
          },
        });
        // ? Open
        await modal.present();
      }
    });
  }

  completeDelivery(cartId: string): void {
    if (cartId) {
      this.store.dispatch(RiderActions.MarkDeliveryAsCompleteInitiatedAction({ payload: { cartId } }));
    }
  }

  private updateCartState(payload: OrderInfoType): OrderInfoType {
    try {
      return {
        ...payload,
        cart: {
          ...payload.cart,
          status: ProductCartStatus.TRANSIT
        }
      };
    }
    catch (ex) {
      throw ex;
    }
  }

  ngOnDestroy(): void {
    this.subSink.unsubscribe();
  }
}
