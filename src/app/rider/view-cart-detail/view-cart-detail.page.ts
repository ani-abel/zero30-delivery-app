import { Component, OnInit, Input } from "@angular/core";
import { ModalController } from "@ionic/angular";
import { CartCheckListType } from "../store/model/rider.model";

@Component({
  selector: "app-view-cart-detail",
  templateUrl: "./view-cart-detail.page.html",
  styleUrls: ["./view-cart-detail.page.scss"],
})
export class ViewCartDetailPage implements OnInit {
  @Input() cartCheckLists: CartCheckListType[];

  constructor(
    public modalController: ModalController,
  ) {}

  ngOnInit() { }

  async closeModal(): Promise<void> {
    await this.modalController.dismiss();
  }

}
