import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewCartDetailPage } from './view-cart-detail.page';

const routes: Routes = [
  {
    path: '',
    component: ViewCartDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewCartDetailPageRoutingModule {}
