import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { IonicModule } from "@ionic/angular";
import { ViewCartDetailPageRoutingModule } from "./view-cart-detail-routing.module";

import { ViewCartDetailPage } from "./view-cart-detail.page";
import { SharedModule } from "../../shared/shared.module";

@NgModule({
  imports: [
    CommonModule,
    ViewCartDetailPageRoutingModule,
    SharedModule,
    IonicModule,
  ],
  declarations: [ViewCartDetailPage]
})
export class ViewCartDetailPageModule {}
