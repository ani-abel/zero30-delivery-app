import {
  Component,
  NgZone,
  OnInit,
  OnDestroy
} from "@angular/core";
import { Store } from "@ngrx/store";
import { Observable } from "rxjs";
import { SubSink } from "subSink";
import { actions as RiderAction } from "../store/actions/rider.action";
import { AuthResponseType } from "../../auth/store/model/auth.model";
import {
  ProductCartStatus,
  UserModel
} from "../../user/store/model/user.model";
import { getDataFromLocalStorage } from "../../../utils/functions/app.functions";
import { LocalStorageKey } from "../../../utils/types/app.constant";
import { AppState } from "../../../utils/types/app.model";
import { actions as UserAction } from "../../user/store/actions/user.action";
import { RiderCartType, RiderRatingType } from "../store/model/rider.model";
import { SignalrWebsocketService } from "../../services/signalr-websocket.service";
import { LocationService } from "../../services/location.service";

declare function unBundleSVG();

@Component({
  selector: "app-home",
  templateUrl: "./home.page.html",
  styleUrls: ["./home.page.scss"],
})
export class HomePage implements
OnInit,
OnDestroy {
  userData$: Observable<UserModel>;
  riderDeliveryHistory$: Observable<RiderCartType[]>;
  riderRating$: Observable<RiderRatingType>;
  productCartStatus = ProductCartStatus;
  subSink: SubSink = new SubSink();
  userId: string;

  constructor(
    private readonly store: Store<AppState>,
    private readonly _ngZone: NgZone,
    private readonly signalRSrv: SignalrWebsocketService,
    private readonly locationSrv: LocationService,
  ) {
    // ? Subscribe and listen to websocket events related to the rider
    this.subscribeToEvents();
  }

  async ngOnInit() {
    unBundleSVG();
    const { userId } = await getDataFromLocalStorage<AuthResponseType>(LocalStorageKey.ZERO_30_USER);
    if (userId) {
      this.userId = userId;
      // ? Start the geolocation beacon and emit the rider location data, if they have any carts marked as "IN_TRANSIT"
      await this.locationSrv.startGeolocationRemotely();

      this.userId = userId;
      this.store.dispatch(UserAction.GetUserDataInitiatedAction({ payload: { userId } }));
      this.userData$ = this.store.select((data) => data.User.UserData);

      // ? Get the list of carts that the rider has delivered in recently
      this.store.dispatch(RiderAction.GetRiderActivityHistoryInitiatedAction({ payload: { userId } }));
      this.riderDeliveryHistory$ = this.store.select((data) => data.Rider.DeliveryActivities);

      // ? Get all the rider ratings
      this.store.dispatch(RiderAction.GetRiderRatingInitiatedAction({ payload: { userId } }));
      this.riderRating$ = this.store.select((data) => data.Rider.RiderRating);

      // ? Connect the rider to a group allowing them to recieve personal notifications
      this.signalRSrv.addRiderToGroup(userId);
      this.signalRSrv.cartAssignedToCartRider();
    }
  }

  private subscribeToEvents(): void {
    this.subSink.sink =
    this.signalRSrv.cartAssignedToRider.subscribe((payload: RiderCartType) => {
      this._ngZone.run(() => {
        console.log({ payload });
        if (payload?.person) {
          this.store.dispatch(RiderAction.NewCartAssignedToRiderAction({ payload: { cart: payload } }));
        }
      });
    });
  }

  fillInUrl(status: ProductCartStatus): string {
    return status === ProductCartStatus.TRANSIT || this.productCartStatus.COMPLETED
    ? "order-info"
    : "new-order-detail";
  }

  ngOnDestroy(): void {
    this.subSink.unsubscribe();
  }

}
