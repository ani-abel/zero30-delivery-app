import {
  Component,
  OnInit,
} from "@angular/core";
import { Store } from "@ngrx/store";
import { Observable } from "rxjs";
import { AppState } from "../../../utils/types/app.model";
import { actions as RiderAction } from "../store/actions/rider.action";
import { LocationService } from "../../services/location.service";
import { DeliveryGraphSummaryType } from "../store/model/rider.model";
import { getDataFromLocalStorage, mountPlotlyScript } from "../../../utils/functions/app.functions";
import { LocalStorageKey } from "../../../utils/types/app.constant";
import { AuthResponseType } from "../../auth/store/model/auth.model";

@Component({
  selector: "app-statistics",
  templateUrl: "./statistics.page.html",
  styleUrls: ["./statistics.page.scss"],
})
export class StatisticsPage implements OnInit {
  riderDeliveryGraphData$: Observable<DeliveryGraphSummaryType[]>;

  constructor(
    private readonly locationSrv: LocationService,
    private readonly store: Store<AppState>,
  ) { }

  ngOnInit(): void {
    mountPlotlyScript();
    setTimeout(async () => {
      // ? Start the geolocation beacon and emit the rider location data, if they have any carts marked as "IN_TRANSIT"
      await this.locationSrv.startGeolocationRemotely();

      const { userId } = await getDataFromLocalStorage<AuthResponseType>(LocalStorageKey.ZERO_30_USER);
      if (userId) {
        this.store.dispatch(RiderAction.GetRiderDeliveryGraphSummaryInitiatedAction({ payload: { riderId: userId } }));
        this.riderDeliveryGraphData$ = this.store.select((data) => data.Rider.RiderDeliveryGraphData);
      }
    }, 2000);
  }

}
