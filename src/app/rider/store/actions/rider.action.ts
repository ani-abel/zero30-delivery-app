import {
    createAction,
    props
} from "@ngrx/store";
import {
    CartCheckListType,
    DeliveryGraphSummaryType,
    LocationAbbrType,
    LocationType,
    MerchantProximityType,
    OrderInfoType,
    RiderCartType,
    RiderRatingType,
    SingleCartType
} from "../model/rider.model";

export enum RiderActionType {
    CLEAR_ACTIVE_ERROR = "[ERROR_RIDER] CLEAR_ACTIVE_ERROR",
    CLEAR_ACTIVE_MESSAGE = "[ERROR_RIDER] CLEAR_ACTIVE_MESSAGE",
    CLEAR_ACTIVE_ACTION_MESSAGE = "[ERROR_RIDER] CLEAR_ACTIVE_ACTION_MESSAGE",

    GET_RIDER_ACTIVITY_HISTORY_INITIATED = "[RIDER] GET_RIDER_ACTIVITY_HISTORY_INITIATED",
    GET_RIDER_ACTIVITY_HISTORY_SUCCESSFUL = "[RIDER] GET_RIDER_ACTIVITY_HISTORY_SUCCESSFUL",
    GET_RIDER_ACTIVITY_HISTORY_FAILED = "[RIDER] GET_RIDER_ACTIVITY_HISTORY_FAILED",

    GET_RIDER_RATING_INITIATED = "[RIDER_RATING] GET_RIDER_RATING_INITIATED",
    GET_RIDER_RATING_FAILED = "[RIDER_RATING] GET_RIDER_RATING_FAILED",
    GET_RIDER_RATING_SUCCESSFUL = "[RIDER_RATING] GET_RIDER_RATING_SUCCESSFUL",

    SAVE_RIDER_GEOLOCATION_INITIATED = "[REALTIME_TRACKING] SAVE_RIDER_GEOLOCATION_INITIATED",
    SAVE_RIDER_GEOLOCATION_FAILED = "[REALTIME_TRACVKING] SAVE_RIDER_GEOLOCATION_FAILED",
    SAVE_RIDER_GEOLOCATION_SUCCESSFUL = " [REALTIME_TRACKING] SAVE_RIDER_GEOLOCATION_SUCCESSFUL",

    GET_RIDER_GEOLOCATION_HISTORY_FOR_DELIVERY_INITIATED = "[REALTIME TRACKING] GET_RIDER_GEOLOCATION_HISTORY_FOR_DELIVERY_INITIATED",
    GET_RIDER_GEOLOCATION_HISTORY_FOR_DELIVERY_FAILED = "[REALTIME_TRACKING] GET_RIDER_GEOLOCATION_HISTORY_FOR_DELIVERY_FAILED",
    GET_RIDER_GEOLOCATION_HISTORY_FOR_DELIVERY_SUCCESFUL = "[REALTIME_TRACKING] GET_RIDER_GEOLOCATION_HISTORY_FOR_DELIVERY_SUCCESSFUL",

    GET_SELECTED_CART_INITIATED = "[ORDERS] GET_SELECTED_CART_INITIATED",
    GET_SELECTED_CART_FAILED = "[ORDERS] GET_SELECTED_CART_FAILED",
    GET_SELECTED_CART_SUCCESSFUL = "[ORDERS] GET_SELECTED_CART_SUCCESSFUL",

    REVERSE_GEOCODING_FOR_ADDRESS_INITIATED = "[LOCATION_TRACKING] REVERSE_GEOCODING_FOR_ADDRESS_INITIATED",
    REVERSE_GEOCODING_FOR_ADDRESS_FAILED = "[LOCATION_TRACKING] REVERSE_GEOCODING_FOR_ADDRESS_FAILED",
    REVERSE_GEOCODING_FOR_ADDRESS_SUCCESSFUL = "[LOCATION_TRACKING] REVERSE_GEOCODING_FOR_ADDRESS_SUCCESSFUL",

    ACCEPT_CART_DELIVERY_INITIATED = "[RIDER_DELIVERY] ACCEPT_CART_DELIVERY_INITIATED",
    ACCEPT_CART_DELIVERY_FAILED = "[RIDER_DELIVERY] ACCEPT_CART_DELIVERY_FAILED",
    ACCEPT_CART_DELIVERY_SUCCESSFUL = "[RIDER_DELIVERY] ACCEPT_CART_DELIVERY_SUCCESSFUL",

    CANCEL_CART_DELIVERY_INITIATED = "[RIDER_DELIVERY] CANCEL_CART_DELIVERY_INITIATED",
    CANCEL_CART_DELIVERY_FAILED = "[RIDER_DELIVERY] CANCEL_CART_DELIVERY_FAILED",
    CANCEL_CART_DELIVERY_SUCCESSFUL = "[RIDER_DELIVERY] CANCEL_CART_DELIVERY_SUCCESSFUL",

    GET_MERCHANT_PROXIMITY_TO_RIDER_INITIATED = "[RIDER_DELIVERY] GET_MERCHANT_PROXIMITY_TO_RIDER_INITIATED",
    GET_MERCHANT_PROXIMITY_TO_RIDER_FAILED = "[RIDER_DELIVERY] GET_MERCHANT_PROXIMITY_TO_RIDER_FAILED",
    GET_MERCHANT_PROXIMITY_TO_RIDER_SUCCESSFUL = "[RIDER_DELIVERY] GET_MERCHANT_PROXIMITY_TO_RIDER_SUCCESSFUL",

    GET_ORDER_INFO_INITIATED = "[DELIVERY_ORDER] GET_ORDER_INFO_INITIATED",
    GET_ORDER_INFO_FAILED = "[DELIVERY_ORDER] GET_ORDER_INFO_FAILED",
    GET_ORDER_INFO_SUCCESSFUL = "[DELIVERY_ORDER] GET_ORDER_INFO_SUCCESSFUL",

    GET_MERCHANTS_RELATED_TO_CART_INITIATED = "[DELIVERY_ORDER] GET_MERCHANTS_RELATED_TO_CART_INITIATED",
    GET_MERCHANTS_RELATED_TO_CART_FAILED = "[DELIVERY_ORDER] GET_MERCHANTS_RELATED_TO_CART_FAILED",
    GET_MERCHANTS_RELATED_TO_CART_SUCCESSFUL = "[DELIVERY_ORDER] GET_MERCHANTS_RELATED_TO_CART_SUCCESSFUL",

    GET_RIDER_DELIVERY_GRAPH_SUMMARY_INITIATED = "[GRAPHICAL_SUMMARY] GET_RIDER_DELIVERY_GRAPH_SUMMARY_INITIATED",
    GET_RIDER_DELIVERY_GRAPH_SUMMARY_FAILED = "[GRAPHICAL_SUMMARY] GET_RIDER_DELIVERY_GRAPH_SUMMARY_FAILED",
    GET_RIDER_DELIVERY_GRAPH_SUMMARY_SUCCESSFUL = "[GRAPHICAL_SUMMARY] GET_RIDER_DELIVERY_GRAPH_SUMMARY_SUCCESSFUL",

    MARK_DELIVERY_AS_COMPLETE_INITIATED = "[RIDER_DELIVERY] MARK_DELIVERY_AS_COMPLETE_INITIATED",
    MARK_DELIVERY_AS_COMPLETE_FAILED = "[RIDER_DELIVERY] MARK_DELIVERY_AS_COMPLETE_FAILED",
    MARK_DELIVERY_AS_COMPLETE_SUCCESSFUL = "[RIDER_DELIVERY] MARK_DELIVERY_AS_COMPLETE_SUCCESSFUL",

    CONFIRM_MANUAL_PAYMENT_INITIATED = "[RIDER_DELIVERY] CONFIRM_MANUAL_PAYMENT_INITIATED",
    CONFIRM_MANUAL_PAYMENT_FAILED = "[RIDER_DELIVERY] CONFIRM_MANUAL_PAYMENT_FAILED",
    CONFIRM_MANUAL_PAYMENT_SUCCESSFUL = "[RIDER_DELIVERY] CONFIRM_MANUAL_PAYMENT_SUCCESSFUL",

    GET_CART_CHECKLIST_INITIATED = "[RIDER_DELIVERY] GET_CART_CHECKLIST_INITIATED",
    GET_CART_CHECKLIST_FAILED = "[RIDER_DELIVERY] GET_CART_CHECKLIST_FAILED",
    GET_CART_CHECKLIST_SUCCESSFUL = "[RIDER_DELIVERY] GET_CART_CHECKLIST_SUCCESSFUL",

    CLEAR_RIDER_STATE = "[AUTH] CLEAR_RIDER_STATE",
    NEW_CART_ASSIGNED_TO_RIDER = "[RIDER] NEW_CART_ASSIGNED_TO_RIDER",
}

export const actions = {
    ClearActiveErrorAction: createAction(
        RiderActionType.CLEAR_ACTIVE_ERROR
    ),
    ClearActiveMessageAction: createAction(
        RiderActionType.CLEAR_ACTIVE_MESSAGE
    ),
    ClearActiveActionMessageAction: createAction(
        RiderActionType.CLEAR_ACTIVE_ACTION_MESSAGE
    ),
    ClearRiderStateAction: createAction(
        RiderActionType.CLEAR_RIDER_STATE,
    ),
    GetRiderActivityHistoryInitiatedAction: createAction(
        RiderActionType.GET_RIDER_ACTIVITY_HISTORY_INITIATED,
        props<{ payload: { userId: string } }>()
    ),
    GetRiderActivityHistoryFailedAction: createAction(
        RiderActionType.GET_RIDER_ACTIVITY_HISTORY_FAILED,
        props<{ payload: Error }>()
    ),
    GetRiderActivityHistorySuccessfulAction: createAction(
        RiderActionType.GET_RIDER_ACTIVITY_HISTORY_SUCCESSFUL,
        props<{ payload: RiderCartType[] }>()
    ),
    NewCartAssignedToRiderAction: createAction(
        RiderActionType.NEW_CART_ASSIGNED_TO_RIDER,
        props<{ payload: { cart: RiderCartType } }>()
    ),
    GetRiderRatingInitiatedAction: createAction(
        RiderActionType.GET_RIDER_RATING_INITIATED,
        props<{ payload: { userId: string } }>()
    ),
    GetRiderRatingFailedAction: createAction(
        RiderActionType.GET_RIDER_RATING_FAILED,
        props<{ payload: Error }>()
    ),
    GetRiderRatingSuccessfulAction: createAction(
        RiderActionType.GET_RIDER_RATING_SUCCESSFUL,
        props<{ payload: RiderRatingType }>()
    ),
    SaveRiderGeolocationInitiatedAction: createAction(
        RiderActionType.SAVE_RIDER_GEOLOCATION_INITIATED,
        props<{ payload: { userId: string, longitude: number, latitude: number } }>(),
    ),
    SaveRiderGeolocationFailedAction: createAction(
        RiderActionType.SAVE_RIDER_GEOLOCATION_FAILED,
        props<{ payload: Error }>(),
    ),
    SaveRiderGeolocationSuccessfulAction: createAction(
        RiderActionType.SAVE_RIDER_GEOLOCATION_SUCCESSFUL,
        props<{ payload: boolean }>(),
    ),
    GetRiderGeolocationHistoryForDeliveryInitiatedAction: createAction(
        RiderActionType.GET_RIDER_GEOLOCATION_HISTORY_FOR_DELIVERY_INITIATED,
        props<{ payload: { cartId: string } }>()
    ),
    GetRiderGeolocationHistoryForDeliveryFailedAction: createAction(
        RiderActionType.GET_RIDER_GEOLOCATION_HISTORY_FOR_DELIVERY_FAILED,
        props<{ payload: Error }>()
    ),
    GetRiderGeolocationHistoryForDeliverySuccessfulAction: createAction(
        RiderActionType.GET_RIDER_GEOLOCATION_HISTORY_FOR_DELIVERY_SUCCESFUL,
        props<{ payload: LocationType[] }>()
    ),
    GetSelectedCartInitiatedAction: createAction(
        RiderActionType.GET_SELECTED_CART_INITIATED,
        props<{ payload: { cartId: string } }>()
    ),
    GetSelectedCartFailedAction: createAction(
        RiderActionType.GET_SELECTED_CART_FAILED,
        props<{ payload: Error }>()
    ),
    GetSelectedCartSuccessfulAction: createAction(
        RiderActionType.GET_SELECTED_CART_SUCCESSFUL,
        props<{ payload: SingleCartType }>()
    ),
    ReverseGeocodingForAddressInitiatedAction: createAction(
        RiderActionType.REVERSE_GEOCODING_FOR_ADDRESS_INITIATED,
        props<{ payload: { address: string } }>()
    ),
    ReverseGeocodingForAddressFailedAction: createAction(
        RiderActionType.REVERSE_GEOCODING_FOR_ADDRESS_FAILED,
        props<{ payload: Error }>()
    ),
    ReverseGeocodingForAddressSuccessfulAction: createAction(
        RiderActionType.REVERSE_GEOCODING_FOR_ADDRESS_SUCCESSFUL,
        props<{ payload: LocationAbbrType }>()
    ),
    AcceptCartDeliveryInitiatedAction: createAction(
        RiderActionType.ACCEPT_CART_DELIVERY_INITIATED,
        props<{ payload: { cartId: string, userId: string } }>()
    ),
    AcceptCartDeliveryFailedAction: createAction(
        RiderActionType.ACCEPT_CART_DELIVERY_FAILED,
        props<{ payload: Error }>()
    ),
    AcceptCartDeliverySuccessfulAction: createAction(
        RiderActionType.ACCEPT_CART_DELIVERY_SUCCESSFUL,
        props<{ payload: boolean }>()
    ),
    CancelCartDeliveryInitiatedAction: createAction(
        RiderActionType.CANCEL_CART_DELIVERY_INITIATED,
        props<{ payload: { cartId: string, userId: string } }>()
    ),
    CancelCartDeliveryFailedAction: createAction(
        RiderActionType.CANCEL_CART_DELIVERY_INITIATED,
        props<{ payload: Error }>()
    ),
    CancelCartDeliverySuccessfulAction: createAction(
        RiderActionType.CANCEL_CART_DELIVERY_SUCCESSFUL,
        props<{ payload: boolean }>()
    ),
    GetMerchantProximityToRiderInitiatedAction: createAction(
        RiderActionType.GET_MERCHANT_PROXIMITY_TO_RIDER_INITIATED,
        props<{ payload: { cartId: string, latitude: number, longitude: number } }>()
    ),
    GetMerchantProximityToRiderFailedAction: createAction(
        RiderActionType.GET_MERCHANT_PROXIMITY_TO_RIDER_FAILED,
        props<{ payload: Error }>()
    ),
    GetMerchantProximityToRiderSuccessfulAction: createAction(
        RiderActionType.GET_MERCHANT_PROXIMITY_TO_RIDER_SUCCESSFUL,
        props<{ payload: MerchantProximityType[] }>()
    ),
    GetOrderInfoInitiatedAction: createAction(
        RiderActionType.GET_ORDER_INFO_INITIATED,
        props<{ payload: { cartId: string } }>()
    ),
    GetOrderInfoFailedAction: createAction(
        RiderActionType.GET_ORDER_INFO_FAILED,
        props<{ payload: Error }>()
    ),
    GetOrderInfoSuccessfulAction: createAction(
        RiderActionType.GET_ORDER_INFO_SUCCESSFUL,
        props<{ payload: OrderInfoType }>()
    ),
    GetMerchantsRelatedToCartInitiatedAction: createAction(
        RiderActionType.GET_MERCHANTS_RELATED_TO_CART_INITIATED,
        props<{ payload: { cartId: string } }>()
    ),
    GetMerchantsRelatedToCartFailedAction: createAction(
        RiderActionType.GET_MERCHANTS_RELATED_TO_CART_FAILED,
        props<{ payload: Error}>()
    ),
    GetMerchantsRelatedToCartSuccessfulAction: createAction(
        RiderActionType.GET_MERCHANTS_RELATED_TO_CART_SUCCESSFUL,
        props<{ payload: MerchantProximityType[] }>()
    ),
    GetRiderDeliveryGraphSummaryInitiatedAction: createAction(
        RiderActionType.GET_RIDER_DELIVERY_GRAPH_SUMMARY_INITIATED,
        props<{ payload: { riderId: string } }>()
    ),
    GetRiderDeliveryGraphSummaryFailedAction: createAction(
        RiderActionType.GET_RIDER_DELIVERY_GRAPH_SUMMARY_FAILED,
        props<{ payload: Error }>()
    ),
    GetRiderDeliveryGraphSummarySuccessfulAction: createAction(
        RiderActionType.GET_RIDER_DELIVERY_GRAPH_SUMMARY_SUCCESSFUL,
        props<{ payload: DeliveryGraphSummaryType[] }>()
    ),
    MarkDeliveryAsCompleteInitiatedAction: createAction(
        RiderActionType.MARK_DELIVERY_AS_COMPLETE_INITIATED,
        props<{ payload: { cartId: string,  } }>()
    ),
    MarkDeliveryAsCompleteFailedAction: createAction(
        RiderActionType.MARK_DELIVERY_AS_COMPLETE_FAILED,
        props<{ payload: Error }>()
    ),
    MarkDeliveryAsCompleteSuccessfulAction: createAction(
        RiderActionType.MARK_DELIVERY_AS_COMPLETE_SUCCESSFUL,
        props<{ payload: boolean }>()
    ),
    ConfirmManualPaymentInitiatedAction: createAction(
        RiderActionType.CONFIRM_MANUAL_PAYMENT_INITIATED,
        props<{ payload: { cartId: string, riderId: string, paymentOption: number } }>()
    ),
    ConfirmManualPaymentFailedAction: createAction(
        RiderActionType.CONFIRM_MANUAL_PAYMENT_FAILED,
        props<{ payload: Error }>()
    ),
    ConfirmManualPaymentSuccessfulAction: createAction(
        RiderActionType.CONFIRM_MANUAL_PAYMENT_SUCCESSFUL,
        props<{ payload: boolean }>()
    ),
    GetCartCheckListInitiatedAction: createAction(
        RiderActionType.GET_CART_CHECKLIST_INITIATED,
        props<{ payload: { cartId } }>()
    ),
    GetCartCheckListFailedAction: createAction(
        RiderActionType.GET_CART_CHECKLIST_FAILED,
        props<{ payload: Error }>()
    ),
    GetCartCheckListSuccessfulAction: createAction(
        RiderActionType.GET_CART_CHECKLIST_SUCCESSFUL,
        props<{ payload: CartCheckListType[] }>()
    ),
};
