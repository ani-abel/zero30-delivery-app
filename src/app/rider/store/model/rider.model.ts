import {} from 'googlemaps';
import { PersonModel } from '../../../auth/store/model/auth.model'
import { 
    MessageAction,
    PaymentMethodTextType,
    PaymentMethodType, 
    ProductCartStatus, 
    ProductModel, 
    UserModel 
} from '../../../user/store/model/user.model';

export interface RiderRatingType {
    rate: number;
    reviewerCount: number;
}

export type MapIcon = {
    label: "RIDER" | "CUSTOMER",
    metadata: {
        url: string,
        scaledSize: google.maps.Size,
        origin: google.maps.Point,
        anchor: google.maps.Point  
    }
};

export interface LocationAbbrType {
    lat: number;
    lng: number;
}

export interface LocationType {
    latitude: number;
    longitude: number;
}

export interface RiderGeolocationData {
    UserId: string;
    Payload: number;
}

export interface CartCurrentLocationData {
    cartId: string;
    latitude: number;
    longitude: number;
}

export interface WeekOrderCountType {
    weekDay: string;
    count: number;
    label?: string;
}

export interface DeliveryGraphSummaryType {
    startDate: string;
    weekOrderCounts: WeekOrderCountType[];
}  

export interface GeoPointType {
    Tag: "RIDER" | "CUSTOMER", 
    Coordinates: LocationAbbrType,
    ToolTip: string,
}

export interface PaymentType {
    id: string;
    cartId: string;
    paymentReference: string;
    modeOfPayment: PaymentMethodType;
    paid: boolean;
    amount: number;
    dateCreated: Date;
    cart: CartType;
}

export interface CartType {
    id: string;
    userId: string;
    user: UserModel;
    dateCreated: Date;
    active: boolean;
    vat: number;
    deliveryCost: number;
    total: number;
    amount: number;
    status: ProductCartStatus;
    deliveryAddress: string;
    distance: number;
}

export interface OrderType {
    id: string;
    productId: string;
    dateCreated: Date;
    quantity: number;
    userId: string;
    active: boolean;
    product: ProductModel;
    user: UserModel;
    cartId: string;
    cart: CartType;
}

export interface SingleCartType {
    id: string;
    userId: string;
    user: UserModel;
    dateCreated: Date;
    active: boolean;
    vat: number;
    deliveryCost: number;
    total: number;
    amount: number;
    status: ProductCartStatus;
    distance: number;
    deliveryAddress: string;
}

export interface RiderCartType {
    payment: PaymentType;
    cart: CartType;
    orders: OrderType[];
    person: PersonModel;
}

export interface MerchantProximityType {
    merchantName: string;
    merchantTrade: string;
    merchantImage: string;
    merchantDistance: number;
    merchantId: string;
    merchantAddress: string;
    cartId: string;
    latitude: number;
    longitude: number;
}  

export interface OrderInfoType {
    id: string;
    cartId: string;
    paymentReference: string;
    modeOfPayment: PaymentMethodTextType;
    paid: boolean;
    amount: number;
    dateCreated: string;
    cart: Cart;
}

export interface Cart {
    id: string;
    userId: string;
    user: UserModel;
    dateCreated: string;
    active: boolean;
    vat: number;
    deliveryCost: number;
    total: number;
    amount: number;
    status: string;
    deliveryAddress: string;
    distance: number;
}

export interface CartCheckListType {
    mechantTradeName: string;
    mechantName: string;
    mechantAddress: string;
    mechantLogo: string;
    mechantPhone: string;
    mechantEmail: string;
    productOrderDetails?: ProductCheckListType[];
}

export interface ProductCheckListType {
    productName: string;
    productDescription: string;
    productLogo: string;
    quantity: number;
    unitPrice: number;
    totalPrice: number;
}

export interface RiderState {
    ActiveMessage: string;
    ActiveActionMessage: MessageAction;
    RiderRating: RiderRatingType;
    ActiveError: Error;
    IsLoading: boolean;
    DeliveryActivities: RiderCartType[];
    CompletedDeliveries: RiderCartType[];
    OngoingDeliveries: RiderCartType[];
    RiderLocationGeoPoints: LocationType[];
    SelectedCart: SingleCartType;
    SelectedAddressToLocation: LocationAbbrType;
    MerchantsInProximityToRider: MerchantProximityType[];
    MerchantsRelatedToCart: MerchantProximityType[];
    SelectedOrderInfo: OrderInfoType;
    RiderDeliveryGraphData: DeliveryGraphSummaryType[];
    SelectedCartCheckList: CartCheckListType[];
}

export const InitialRiderState: RiderState = {
    DeliveryActivities: [],
    CompletedDeliveries: [],
    OngoingDeliveries: [],
    RiderRating: undefined,
    ActiveActionMessage: undefined,
    ActiveError: undefined,
    ActiveMessage: undefined,
    RiderLocationGeoPoints: [],
    IsLoading: false,
    SelectedCart: undefined,
    SelectedAddressToLocation: undefined,
    MerchantsInProximityToRider: [],
    MerchantsRelatedToCart: [],
    SelectedOrderInfo: undefined,
    RiderDeliveryGraphData: [],
    SelectedCartCheckList: [],
};
