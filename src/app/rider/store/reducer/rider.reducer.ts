import { createReducer, on } from "@ngrx/store";
import { InitialRiderState, RiderCartType } from "../model/rider.model";
import { actions as RiderActions } from "../actions/rider.action";
import { MessageActionType, ProductCartStatus } from "../../../user/store/model/user.model";
import {
    cancelCartDelivery,
    markCartAsInTransit,
    pushUniqueArrayValues
} from "../../../../utils/functions/app.functions";

export const RiderReducer = createReducer(
    InitialRiderState,
    on(RiderActions.ClearActiveErrorAction, (state) => {
        return { ...state, ActiveError: undefined, ActiveMessage: undefined, IsLoading: false };
    }),
    on(RiderActions.ClearActiveMessageAction, (state) => {
        return { ...state, ActiveError: undefined, ActiveMessage: undefined, IsLoading: false };
    }),
    on(RiderActions.ClearActiveActionMessageAction, (state) => {
        return { ...state, ActiveActionMessage: undefined, ActiveMessage: undefined, IsLoading: false };
    }),
    on(RiderActions.ClearRiderStateAction, (state) => {
        return { ...InitialRiderState };
    }),
    on(RiderActions.GetRiderActivityHistoryInitiatedAction, (state, { payload }) => {
        return { ...state, IsLoading: true };
    }),
    on(RiderActions.GetRiderActivityHistoryFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(RiderActions.GetRiderActivityHistorySuccessfulAction, (state, { payload }) => {
        const completedActivities: RiderCartType[] = payload?.filter((data) => data.cart.status === ProductCartStatus.COMPLETED);
        const ongoingActivities: RiderCartType[] = payload?.filter((data) => data.cart.status === ProductCartStatus.TRANSIT);
        // const verifiedActivities: RiderCartType[] = payload?.filter((data) => data.cart.status === ProductCartStatus.VERIFIED); // New
        return {
            ...state,
            IsLoading: false,
            DeliveryActivities: [...payload],
            // OngoingDeliveries: [...ongoingActivities, ...verifiedActivities],
            OngoingDeliveries: [...ongoingActivities],
            CompletedDeliveries: [...completedActivities],
        };
    }),
    on(RiderActions.NewCartAssignedToRiderAction, (state, { payload: { cart } }) => {
        if ((cart.cart.status === ProductCartStatus.COMPLETED)) {
            return {
                ...state,
                ActiveActionMessage: {
                    Message: "New cart assigned to you",
                    Type: MessageActionType.RIDER_ASSIGNED_TO_CART,
                    Label: "Open",
                },
                DeliveryActivities: pushUniqueArrayValues(cart, state.DeliveryActivities, "START"),
                CompletedDeliveries: pushUniqueArrayValues(cart, state.CompletedDeliveries, "START"),
            };
        }
        if ((cart.cart.status === ProductCartStatus.TRANSIT)) {
            return {
                ...state,
                ActiveActionMessage: {
                    Message: "New cart assigned to you",
                    Type: MessageActionType.RIDER_ASSIGNED_TO_CART,
                    Label: "Open",
                },
                DeliveryActivities: pushUniqueArrayValues(cart, state.DeliveryActivities, "START"),
                OngoingDeliveries: pushUniqueArrayValues(cart, state.OngoingDeliveries, "START"),
            };
        }
        else {
            return {
                ...state,
                ActiveActionMessage: {
                    Message: "New cart assigned to you",
                    Type: MessageActionType.RIDER_ASSIGNED_TO_CART,
                    Label: "Open",
                },
                DeliveryActivities: pushUniqueArrayValues(cart, state.DeliveryActivities, "START"),
            };
        }
    }),
    on(RiderActions.GetRiderRatingInitiatedAction, (state, { payload }) => {
        return { ...state, IsLoading: true };
    }),
    on(RiderActions.GetRiderRatingFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(RiderActions.GetRiderRatingSuccessfulAction, (state, { payload }) => {
        return { ...state, IsLoading: false, RiderRating: { ...payload } };
    }),
    on(RiderActions.SaveRiderGeolocationInitiatedAction, (state, { payload }) => {
        return {
            ...state,
            // IsLoading: true
        };
    }),
    on(RiderActions.SaveRiderGeolocationFailedAction, (state, { payload }) => {
        return {
            ...state,
            // IsLoading: false,
            ActiveError: payload
        };
    }),
    on(RiderActions.SaveRiderGeolocationSuccessfulAction, (state, { payload }) => {
        return {
            ...state,
            // IsLoading: false,
        };
    }),
    on(RiderActions.GetRiderGeolocationHistoryForDeliveryInitiatedAction, (state, { payload }) => {
        return { ...state, IsLoading: true, };
    }),
    on(RiderActions.GetRiderGeolocationHistoryForDeliveryFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(RiderActions.GetRiderGeolocationHistoryForDeliverySuccessfulAction, (state, { payload }) => {
        return { ...state, IsLoading: false, RiderLocationGeoPoints: [...payload] };
    }),
    on(RiderActions.GetSelectedCartInitiatedAction, (state, { payload }) => {
        return { ...state, IsLoading: true };
    }),
    on(RiderActions.GetSelectedCartFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(RiderActions.GetSelectedCartSuccessfulAction, (state, { payload }) => {
        return { ...state, IsLoading: false, SelectedCart: { ...payload } };
    }),
    on(RiderActions.ReverseGeocodingForAddressInitiatedAction, (state, { payload }) => {
        return { ...state, IsLoading: true };
    }),
    on(RiderActions.ReverseGeocodingForAddressFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(RiderActions.ReverseGeocodingForAddressSuccessfulAction, (state, { payload }) => {
        return { ...state, IsLoading: false, SelectedAddressToLocation: payload };
    }),
    on(RiderActions.AcceptCartDeliveryInitiatedAction, (state, { payload: { cartId } }) => {
        const { All, Ongoing } = markCartAsInTransit(
            cartId,
            state.DeliveryActivities,
            state.OngoingDeliveries,
            ProductCartStatus.TRANSIT
        );
        return {
            ...state,
            IsLoading: true,
            DeliveryActivities: [...All] ?? [...state.DeliveryActivities],
            OngoingDeliveries: [...Ongoing] ?? [...state.OngoingDeliveries]
        };
    }),
    on(RiderActions.AcceptCartDeliveryFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(RiderActions.AcceptCartDeliverySuccessfulAction, (state, { payload }) => {
        return {
            ...state,
            IsLoading: false,
            ActiveMessage: payload === true ?
            "You just accepted delivery" :
            undefined,
        };
    }),
    on(RiderActions.CancelCartDeliveryInitiatedAction, (state, { payload: { cartId } }) => {
        const { All, Ongoing } = cancelCartDelivery(cartId, state.DeliveryActivities, state.OngoingDeliveries);
        return {
            ...state,
            IsLoading: true,
            DeliveryActivities: [...All] ?? [...state.DeliveryActivities],
            OngoingDeliveries: [...Ongoing] ?? [...state.OngoingDeliveries],
        };
    }),
    on(RiderActions.CancelCartDeliveryFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(RiderActions.CancelCartDeliverySuccessfulAction, (state, { payload }) => {
        return {
            ...state,
            IsLoading: false,
            ActiveMessage: payload === true ?
            "You just canceled a delivery" :
            undefined,
        };
    }),
    on(RiderActions.GetMerchantProximityToRiderInitiatedAction, (state, { payload }) => {
        return { ...state, IsLoading: true, };
    }),
    on(RiderActions.GetMerchantProximityToRiderFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(RiderActions.GetMerchantProximityToRiderSuccessfulAction, (state, { payload }) => {
        return { ...state, IsLoading: false, MerchantsInProximityToRider: payload };
    }),
    on(RiderActions.GetOrderInfoInitiatedAction, (state, { payload }) => {
        return { ...state, IsLoading: true };
    }),
    on(RiderActions.GetOrderInfoFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveMessage: payload };
    }),
    on(RiderActions.GetOrderInfoSuccessfulAction, (state, { payload }) => {
        return { ...state, IsLoading: false, SelectedOrderInfo: payload };
    }),
    on(RiderActions.GetMerchantsRelatedToCartInitiatedAction, (state, { payload }) => {
        return { ...state, IsLoading: true, };
    }),
    on(RiderActions.GetMerchantsRelatedToCartFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(RiderActions.GetMerchantsRelatedToCartSuccessfulAction, (state, { payload }) => {
        return { ...state, IsLoading: false, MerchantsRelatedToCart: payload };
    }),
    on(RiderActions.GetRiderDeliveryGraphSummaryInitiatedAction, (state, { payload }) => {
        return { ...state, IsLoading: true };
    }),
    on(RiderActions.GetRiderDeliveryGraphSummaryFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(RiderActions.GetRiderDeliveryGraphSummarySuccessfulAction, (state, { payload }) => {
        return {
            ...state,
            IsLoading: false,
            RiderDeliveryGraphData: [...payload],
        };
    }),
    on(RiderActions.MarkDeliveryAsCompleteInitiatedAction, (state, { payload: { cartId } }) => {
        const { All, Ongoing } = markCartAsInTransit(
            cartId,
            state.DeliveryActivities,
            state.OngoingDeliveries,
            ProductCartStatus.COMPLETED
        );
        return {
            ...state,
            IsLoading: true,
            DeliveryActivities: [...All] ?? [...state.DeliveryActivities],
            OngoingDeliveries: [...Ongoing] ?? [...state.OngoingDeliveries]
        };
    }),
    on(RiderActions.MarkDeliveryAsCompleteFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(RiderActions.MarkDeliveryAsCompleteSuccessfulAction, (state, { payload }) => {
        return { ...state, IsLoading: false, };
    }),
    on(RiderActions.ConfirmManualPaymentInitiatedAction, (state, { payload }) => {
        return { ...state, IsLoading: true, };
    }),
    on(RiderActions.ConfirmManualPaymentFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(RiderActions.ConfirmManualPaymentSuccessfulAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveMessage: payload ? "Payment confirmed" : undefined };
    }),
    on(RiderActions.GetCartCheckListInitiatedAction, (state, { payload }) => {
        return { ...state, IsLoading: true, };
    }),
    on(RiderActions.GetCartCheckListFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(RiderActions.GetCartCheckListSuccessfulAction, (state, { payload }) => {
        return { ...state, IsLoading: false,  SelectedCartCheckList: [...payload] };
    }),
);
