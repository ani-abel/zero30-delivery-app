import { Injectable } from "@angular/core";
import { createEffect, Actions, ofType } from "@ngrx/effects";
import { of } from "rxjs";
import { switchMap, catchError, map } from "rxjs/operators";
import { actions as RiderActions } from "../actions/rider.action";
import { RiderHttpService } from "./rider-http.service";

@Injectable({
  providedIn: "root"
})
export class RiderEffectService {

  constructor(
    private readonly riderHttpSrv: RiderHttpService,
    private readonly actions$: Actions,
  ) { }

  getRiderDeliveryHistory$ = createEffect(() =>
      this.actions$.pipe(
        ofType(RiderActions.GetRiderActivityHistoryInitiatedAction),
        switchMap((action) => {
          return this.riderHttpSrv.getRiderDeliveryHistory(action.payload.userId).pipe(
            map((data) => RiderActions.GetRiderActivityHistorySuccessfulAction({ payload: data })),
            catchError((error: Error) => of(RiderActions.GetRiderActivityHistoryFailedAction({ payload: error })))
          );
        })
      ));

  getRiderRating$ = createEffect(() =>
      this.actions$.pipe(
        ofType(RiderActions.GetRiderRatingInitiatedAction),
        switchMap((action) => {
          return this.riderHttpSrv.getRiderRating(action.payload.userId).pipe(
            map((data) => RiderActions.GetRiderRatingSuccessfulAction({ payload: data })),
            catchError((error: Error) => of(RiderActions.GetRiderRatingFailedAction({ payload: error })))
          );
        })
      ));


  saveRiderCurrentGeolocation$ = createEffect(() =>
      this.actions$.pipe(
        ofType(RiderActions.SaveRiderGeolocationInitiatedAction),
        switchMap((action) => {
          const { payload: { userId, longitude, latitude } } = action;
          return this.riderHttpSrv.saveRiderCurrentGeolocation(userId, longitude, latitude).pipe(
            map((data) => RiderActions.SaveRiderGeolocationSuccessfulAction({ payload: data })),
            catchError((error: Error) => of(RiderActions.SaveRiderGeolocationFailedAction({ payload: error })))
          );
        })
      ));

  getRiderDeliveryGeoPoints$ = createEffect(() =>
      this.actions$.pipe(
        ofType(RiderActions.GetRiderGeolocationHistoryForDeliveryInitiatedAction),
        switchMap((action) => {
          return this.riderHttpSrv.getRiderLocationTrackingForCartDelivery(action.payload.cartId).pipe(
            map((data) => RiderActions.GetRiderGeolocationHistoryForDeliverySuccessfulAction({ payload: data })),
            catchError((error: Error) => of(RiderActions.GetRiderGeolocationHistoryForDeliveryFailedAction({ payload: error })))
          );
        })
      ));

    getSelectedCartById$ = createEffect(() =>
        this.actions$.pipe(
          ofType(RiderActions.GetSelectedCartInitiatedAction),
          switchMap((action) => {
            return this.riderHttpSrv.getSelectedCart(action.payload.cartId).pipe(
              map((data) => RiderActions.GetSelectedCartSuccessfulAction({ payload: data })),
              catchError((error: Error) => of(RiderActions.GetSelectedCartFailedAction({ payload: error })))
            );
          })
        ));

    getReverseGeocodingForAddress$ = createEffect(() =>
        this.actions$.pipe(
          ofType(RiderActions.ReverseGeocodingForAddressInitiatedAction),
          switchMap((action) => {
            return this.riderHttpSrv.reverseGeocodingForAddress(action.payload.address).pipe(
              map((data) => RiderActions.ReverseGeocodingForAddressSuccessfulAction({ payload: data })),
              catchError((error: Error) => of(RiderActions.ReverseGeocodingForAddressFailedAction({ payload: error })))
          );
        })
        ));

    acceptCartDelivery$ = createEffect(() =>
        this.actions$.pipe(
          ofType(RiderActions.AcceptCartDeliveryInitiatedAction),
          switchMap((action) => {
            const { payload: { cartId, userId } } = action;
            return this.riderHttpSrv.startRiderDelivery(cartId, userId).pipe(
              map((data) => RiderActions.AcceptCartDeliverySuccessfulAction({ payload: data })),
              catchError((error: Error) => of(RiderActions.AcceptCartDeliveryFailedAction({ payload: error })))
            );
          })
        ));

    cancelCartDelivery$ = createEffect(() =>
      this.actions$.pipe(
        ofType(RiderActions.CancelCartDeliveryInitiatedAction),
        switchMap((action) => {
          const { payload: { cartId, userId } } = action;
          return this.riderHttpSrv.cancelRiderDelivery(cartId, userId).pipe(
            map((data) => RiderActions.CancelCartDeliverySuccessfulAction({ payload: data })),
            catchError((error: Error) => of(RiderActions.CancelCartDeliveryFailedAction({ payload: error })))
          );
        })
      ));

    getMerchantInProximityToRider$ = createEffect(() =>
        this.actions$.pipe(
          ofType(RiderActions.GetMerchantProximityToRiderInitiatedAction),
          switchMap((action) => {
            const { payload: { cartId, longitude, latitude } } = action;
            return this.riderHttpSrv.getMerchantInProximityToRider(cartId, latitude, longitude).pipe(
              map((data) => RiderActions.GetMerchantProximityToRiderSuccessfulAction({ payload: data })),
              catchError((error: Error) => of(RiderActions.GetMerchantProximityToRiderFailedAction({ payload: error })))
            );
          })
        ));

    getOrderInfo$ = createEffect(() =>
      this.actions$.pipe(
        ofType(RiderActions.GetOrderInfoInitiatedAction),
        switchMap((action) => {
          return this.riderHttpSrv.getOrderInfo(action.payload.cartId).pipe(
            map((data) => RiderActions.GetOrderInfoSuccessfulAction({ payload: data })),
            catchError((error: Error) => of(RiderActions.GetOrderInfoFailedAction({ payload: error })))
          );
        })
      ));

    getMerchantsRelatedToCart$ = createEffect(() =>
        this.actions$.pipe(
          ofType(RiderActions.GetMerchantsRelatedToCartInitiatedAction),
          switchMap((action) => {
            return this.riderHttpSrv.getMerchantsRelatedToCart(action.payload.cartId).pipe(
              map((data) => RiderActions.GetMerchantsRelatedToCartSuccessfulAction({ payload: data })),
              catchError((error: Error) => of(RiderActions.GetMerchantsRelatedToCartFailedAction({ payload: error })))
            );
          })
        ));

  getRiderDeliveryGraphSummary$ = createEffect(() =>
      this.actions$.pipe(
        ofType(RiderActions.GetRiderDeliveryGraphSummaryInitiatedAction),
        switchMap((action) => {
          return this.riderHttpSrv.getRiderDeliveryGraphSummary(action.payload.riderId).pipe(
            map((data) => RiderActions.GetRiderDeliveryGraphSummarySuccessfulAction({ payload: [...data] })),
            catchError((error: Error) => of(RiderActions.GetRiderDeliveryGraphSummaryFailedAction({ payload: error })))
          );
        })
      ));

  markDeliveryAsComplete$ = createEffect(() =>
    this.actions$.pipe(
      ofType(RiderActions.MarkDeliveryAsCompleteInitiatedAction),
      switchMap((action) => {
        return this.riderHttpSrv.markDeliveryAsComplete(action.payload.cartId).pipe(
          map((data) => RiderActions.MarkDeliveryAsCompleteSuccessfulAction({ payload: data })),
          catchError((error: Error) => of(RiderActions.MarkDeliveryAsCompleteFailedAction({ payload: error })))
        );
      })
    ));

  confirmManualPayment$ = createEffect(() =>
    this.actions$.pipe(
      ofType(RiderActions.ConfirmManualPaymentInitiatedAction),
      switchMap((action) => {
        const {  payload: { cartId, riderId, paymentOption } } = action;
        return this.riderHttpSrv.confirmManualPayment(cartId, riderId, paymentOption).pipe(
          map((data) => RiderActions.ConfirmManualPaymentSuccessfulAction({ payload: data })),
          catchError((error: Error) => of(RiderActions.ConfirmManualPaymentFailedAction({ payload: error })))
        );
      })
    ));

  getCartCheckList$ = createEffect(() =>
    this.actions$.pipe(
      ofType(RiderActions.GetCartCheckListInitiatedAction),
      switchMap((action) => {
        return this.riderHttpSrv.getCartCheckList(action.payload.cartId).pipe(
          map((data) => RiderActions.GetCartCheckListSuccessfulAction({ payload: data })),
          catchError((error: Error) => of(RiderActions.GetCartCheckListFailedAction({ payload: error })))
        );
      })
    ));

}
