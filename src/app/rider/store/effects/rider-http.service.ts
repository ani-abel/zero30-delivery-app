import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { retry, throttleTime } from "rxjs/operators";
import { environment as env } from "../../../../environments/environment";
import {
  CartCheckListType,
  DeliveryGraphSummaryType,
  LocationAbbrType,
  LocationType,
  MerchantProximityType,
  OrderInfoType,
  RiderCartType,
  RiderRatingType,
  SingleCartType
} from "../model/rider.model";

@Injectable({
  providedIn: "root"
})
export class RiderHttpService {

  constructor(private readonly httpClient: HttpClient) { }

  getRiderDeliveryHistory(userId: string)
  : Observable<RiderCartType[]> {
    try {
      return this.httpClient
                .get<RiderCartType[]>(`${env.apiRoot}/api/Shopping/GetCartByRiderId?RiderId=${userId}&IncludePayment=true&IncludeOrder=true&CustomerDetail=true&IsActive=true`)
                .pipe(
                  throttleTime(500),
                  retry(3)
                );
    }
    catch (ex) {
      throw ex;
    }
  }

  getRiderRating(userId: string)
  : Observable<RiderRatingType> {
    return this.httpClient
                .get<RiderRatingType>(`${env.apiRoot}/api/Rider/GetRiderRatingModel?RiderId=${userId}`)
                .pipe(
                  throttleTime(500),
                  retry(3)
                );
  }

  saveRiderCurrentGeolocation(userId: string, longitude: number, latitude: number)
  : Observable<boolean> {
    try {
      return this.httpClient
                  .post<boolean>(`${env.apiRoot}/api/Rider/SaveRiderCartGeolocation?longitude=${longitude}&latitude=${latitude}&UserId=${userId}`, {})
                  .pipe(
                    throttleTime(500),
                    retry(3)
                  );
    }
    catch (ex) {
      throw ex;
    }
  }

  getRiderLocationTrackingForCartDelivery(cartId: string)
  : Observable<LocationType[]> {
    try {
      return this.httpClient
                .get<LocationType[]>(`${env.apiRoot}/api/Rider/GetCartRiderLocation?CartId=${cartId}`)
                .pipe(
                  throttleTime(500),
                  retry(3)
                );
    }
    catch (ex) {
      throw ex;
    }
  }

  getSelectedCart(cartId: string)
  : Observable<SingleCartType> {
    try {
      return this.httpClient
                .get<SingleCartType>(`${env.apiRoot}/api/Shopping/GetCartById?CartId=${cartId}`)
                .pipe(
                  throttleTime(500),
                  retry(3)
                );
    }
    catch (ex) {
      throw ex;
    }
  }

  reverseGeocodingForAddress(address: string)
  : Observable<LocationAbbrType> {
    try {
      return this.httpClient
                .post<LocationAbbrType>(`${env.apiRoot}/api/Account/GetLocation?street=${address}`, {})
                .pipe(
                  throttleTime(500),
                  retry(3)
                );
    }
    catch (ex) {
      throw ex;
    }
  }

  startRiderDelivery(cartId: string, userId: string)
  : Observable<boolean> {
    try {
      return this.httpClient
                .post<boolean>(`${env.apiRoot}/api/Rider/RiderStartDelivery?UserId=${userId}&CartId=${cartId}`, {})
                .pipe(
                  throttleTime(500),
                  retry(3)
                );
    }
    catch (ex) {
      throw ex;
    }
  }

  cancelRiderDelivery(cartId: string, userId: string)
  : Observable<boolean> {
    try {
      return this.httpClient
                .post<boolean>(`${env.apiRoot}/api/Rider/RiderCancelDelivery?CartId=${cartId}&UserId=${userId}`, {})
                .pipe(
                  throttleTime(500),
                  retry(3)
                );
    }
    catch (ex) {
      throw ex;
    }
  }

  getMerchantInProximityToRider(cartId: string, latitude: number, longitude: number)
  : Observable<MerchantProximityType[]> {
    return this.httpClient
              .get<MerchantProximityType[]>(`${env.apiRoot}/api/Shopping/GetMerchantProximityToRider?CartId=${cartId}&RiderLatitude=${latitude}&RiderLongitude=${longitude}`)
              .pipe(
                throttleTime(500),
                retry(3)
              );
  }

  getOrderInfo(cartId: string)
  : Observable<OrderInfoType> {
    return this.httpClient
              .get<OrderInfoType>(`${env.apiRoot}/api/Payment/GetPaymentByCartId?CartId=${cartId}&IncludeCart=true`)
              .pipe(
                throttleTime(500),
                retry(3)
              );
  }

  getMerchantsRelatedToCart(cartId: string)
  : Observable<MerchantProximityType[]> {
    try {
      return this.httpClient
                .get<MerchantProximityType[]>(`${env.apiRoot}/api/Shopping/GetMerchantByCartId?CartId=${cartId}`)
                .pipe(
                  throttleTime(500),
                  retry(3)
                );
    }
    catch (ex) {
      throw ex;
    }
  }

  getRiderDeliveryGraphSummary(riderId: string)
  : Observable<DeliveryGraphSummaryType[]> {
    try {
      return this.httpClient
                 .get<DeliveryGraphSummaryType[]>(`${env.apiRoot}/api/Shopping/RiderDelievrySummary?RiderId=${riderId}`)
                 .pipe(
                  throttleTime(500),
                  retry(3)
                );
    }
    catch (ex) {
      throw ex;
    }
  }

  markDeliveryAsComplete(cartId: string)
  : Observable<boolean> {
    try {
      return this.httpClient
                  .post<boolean>(`${env.apiRoot}/api/Shopping/CompleteDelivery?CartId=${cartId}`, {})
                  .pipe(
                    throttleTime(500),
                    retry(3)
                  );
    }
    catch (ex) {
      throw ex;
    }
  }

  confirmManualPayment(cartId: string, riderId: string, paymentOption: number)
  : Observable<boolean> {
    try {
      return this.httpClient
                 .post<boolean>(`${env.apiRoot}/api/Payment/ConfirmManualPayment?CartId=${cartId}&UserId=${riderId}&PaymentOption=${paymentOption}`, {})
                 .pipe(
                  throttleTime(500),
                  retry(3)
                );
    }
    catch (ex) {
      throw ex;
    }
  }

  getCartCheckList(cartId: string): Observable<CartCheckListType[]> {
    try {
      return this.httpClient
                .get<CartCheckListType[]>(`${env.apiRoot}/api/Shopping/GetCartProductDetail?CartId=${cartId}`)
                .pipe(
                  throttleTime(500),
                  retry(3)
                );
    }
    catch (ex) {
      throw ex;
    }
  }
}
