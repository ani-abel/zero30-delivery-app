import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { EffectsModule } from "@ngrx/effects";
import { StoreModule } from "@ngrx/store";
import { SharedModule } from "../shared/shared.module";
import { RiderRoutingModule } from "./rider-routing.module";
import { RiderEffectService } from "./store/effects/rider-effect.service";
import { RiderReducer } from "./store/reducer/rider.reducer";

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SharedModule,
    RiderRoutingModule,
    EffectsModule.forFeature([RiderEffectService]),
    StoreModule.forFeature("Rider", RiderReducer)
  ],
  exports: [
    RiderRoutingModule,
  ]
})
export class RiderModule { }
