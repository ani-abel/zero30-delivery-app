import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { EffectsModule } from "@ngrx/effects";
import { StoreModule } from "@ngrx/store";
import { SharedModule } from "../shared/shared.module";
import { UserRoutingModule } from "./user-routing.module";
import { UserEffectService } from "./store/effects/user-effect.service";
import { UserReducer } from "./store/reducer/user.reducer";

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SharedModule,
    UserRoutingModule,
    EffectsModule.forFeature([UserEffectService]),
    StoreModule.forFeature("User", UserReducer)
  ],
  exports: [
    UserRoutingModule,
  ]
})
export class UserModule { }
