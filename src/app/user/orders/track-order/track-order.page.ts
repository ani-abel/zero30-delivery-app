import {
  Component,
  NgZone,
  OnDestroy,
  OnInit,
  ElementRef,
  ViewChild
} from "@angular/core";
import { FormGroup, FormControl, FormArray } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { Store } from "@ngrx/store";
import { Observable, of } from "rxjs";
import { SubSink } from "subsink";
import {} from "googlemaps";
import { actions as UserActions } from "../../../user/store/actions/user.action";
import {
  CartCurrentLocationData,
  GeoPointType,
  LocationAbbrType,
  RiderRatingType,
  SingleCartType
} from "../../../rider/store/model/rider.model";
import { LocationService } from "../../../services/location.service";
import { SignalrWebsocketService } from "../../../services/signalr-websocket.service";
import { AppState } from "../../../../utils/types/app.model";
import {
  ProductCartStatus,
  ProductOrdersResultType,
  RiderDeliveryFeedbackType,
  RiderQualitiesList
} from "../../store/model/user.model";
import { UserModel } from "../../../user/store/model/user.model";
import { getDataFromLocalStorage } from "../../../../utils/functions/app.functions";
import { AuthResponseType } from "../../../auth/store/model/auth.model";
import { LocalStorageKey } from "../../../../utils/types/app.constant";

export enum RiderQuality {
  IsFast = "IsFast",
  IsEnthusiastic = "IsEnthusiastic",
  IsFriendly = "IsFriendly",
}

export interface RiderQualityType {
  value: boolean;
  key: RiderQuality;
  id: string;
  label: string;
}

@Component({
  selector: "app-track-order",
  templateUrl: "./track-order.page.html",
  styleUrls: ["./track-order.page.scss"],
})
export class TrackOrderPage implements
OnInit,
OnDestroy {
  static intervalToWaitBeforeLoadingMap = 3000; // 3 Seconds
  @ViewChild("mapSection", { read: ElementRef }) mapSection: ElementRef;
  isPopupOpen = false;
  cartId: string;
  subSink: SubSink = new SubSink();
  riderLocation: GeoPointType;
  customerLocation: GeoPointType;
  selectedCart$: Observable<SingleCartType>;
  deliveryDestination$: Observable<LocationAbbrType>;
  selectedCartRider$: Observable<UserModel>;
  selectedCartById$: Observable<ProductOrdersResultType>;
  riderRating$: Observable<RiderRatingType>;
  existingCartDeliveryFeedback$: Observable<RiderDeliveryFeedbackType>;
  userId: string;
  noOfStarsSelected = 0;
  riderFirstName: string;
  addRiderRatingForm: FormGroup;
  map: google.maps.Map;
  riderQualities: RiderQualityType[] = [
    {
      value: false,
      key: RiderQuality.IsEnthusiastic,
      id: "enthusiastic",
      label: "Enthusiastic"
    },
    {
      value: false,
      key: RiderQuality.IsFast,
      id: "fast",
      label: "Fast"
    },
    {
      value: false,
      key: RiderQuality.IsFriendly,
      id: "friendly",
      label: "Friendly"
    }
  ];

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly signalRSrv: SignalrWebsocketService,
    private readonly locationSrv: LocationService,
    // tslint:disable-next-line: variable-name
    private readonly _ngZone: NgZone,
    private readonly store: Store<AppState>,
  ) {
    this.subscribeToEvents();
  }

  async ngOnInit(): Promise<void> {
    this.initForm();
    const { userId } = await getDataFromLocalStorage<AuthResponseType>(LocalStorageKey.ZERO_30_USER);
    this.userId = userId;

    this.subSink.sink =
    this.activatedRoute.data.subscribe((data) => {
      const { cartLocation: { locationMarker, cart } } = data;
      if (cart.id) {
        if (cart.status !== ProductCartStatus.TRANSIT) {
          this.store.dispatch(UserActions.CartIsNotInTransitAction());
          // ? navigate the user away
          this.router.navigate(["/user", "orders"]);
        }
        this.cartId = cart.id;
        this.selectedCart$ = of(cart);
        if (locationMarker?.lat && locationMarker?.lng) {
          this.customerLocation = {
            Tag: "CUSTOMER",
            Coordinates: locationMarker,
            ToolTip: cart.deliveryAddress
          };
        }
      }
    });

    if (this.cartId) {
      this.signalRSrv.joinGroupToViewCartLocation(this.cartId);
      // ? Subscribe to and get the current location of the rider
      this.signalRSrv.findCurrentLocationOfCart();
    }

    // ? Get prior review data to prefill the addRatingForm
    this.store.dispatch(UserActions.GetRiderDeliveryRatingInitiatedAction({ payload: { cartId: this.cartId } }));
    this.existingCartDeliveryFeedback$ = this.store.select((data) => data.User.SelectedCartDeliveryFeedback);
    this.subSink.sink =
    this.existingCartDeliveryFeedback$
        .subscribe((deliveryData) => {
          if (deliveryData?.cartId) {
            this.initForm(deliveryData);
            this.riderQualities = this.remapRiderQualities(deliveryData, this.riderQualities);
            this.noOfStarsSelected = deliveryData.rating;
          }
        });
    // ? Get the data for a cart rider
    this.store.dispatch(UserActions.GetCartRiderDataInitiatedAction({ payload: { cartId: this.cartId } }));
    this.selectedCartRider$ = this.store.select((data) => data.User.SelectedCartRiderData);

    setTimeout(() => {
      this.loadMap(this.mapSection, this.riderLocation, this.customerLocation);
    }, TrackOrderPage.intervalToWaitBeforeLoadingMap);
  }

  // async ngOnInit(): Promise<void> {
  //   this.initForm();
  //   const { userId } = await getDataFromLocalStorage<AuthResponseType>(LocalStorageKey.ZERO_30_USER);
  //   this.userId = userId;

  //   this.subSink.sink =
  //   this.activatedRoute.data.subscribe((data) => {
  //     console.log({ data });
  //     const { cartLocation: { locationMarker, cart } } = data;
  //     if (locationMarker?.lat && locationMarker?.lng) {
  //       this.customerLocation = {
  //         Tag: "CUSTOMER",
  //         Coordinates: locationMarker,
  //         ToolTip: cart.deliveryAddress
  //       };
  //     }
  //   });

  //   this.subSink.sink =
  //   this.activatedRoute
  //       .queryParams
  //       .subscribe((data) => {
  //         this.cartId = data.cartId;
  //       });

  //   if (this.cartId) {
  //     this.signalRSrv.joinGroupToViewCartLocation(this.cartId);
  //     // ? Subscribe to and get the current location of the rider
  //     this.signalRSrv.findCurrentLocationOfCart();
  //   }

  //   // ? Get prior review data to prefill the addRatingForm
  //   this.store.dispatch(UserActions.GetRiderDeliveryRatingInitiatedAction({ payload: { cartId: this.cartId } }));
  //   this.existingCartDeliveryFeedback$ = this.store.select((data) => data.User.SelectedCartDeliveryFeedback);
  //   this.subSink.sink =
  //   this.existingCartDeliveryFeedback$
  //       .subscribe((deliveryData) => {
  //         if (deliveryData?.cartId) {
  //           this.initForm(deliveryData);
  //           this.riderQualities = this.remapRiderQualities(deliveryData, this.riderQualities);
  //           this.noOfStarsSelected = deliveryData.rating;
  //         }
  //       });

  //   // ? Get cart by id
  //   this.store.dispatch(UserActions.GetSelectedCartInitiatedAction({ payload: { cartId: this.cartId } }));
  //   this.selectedCartById$ = this.store.select((data) => data.User.SelectedCart);

  //   this.store.dispatch(RiderActions.GetSelectedCartInitiatedAction({ payload: { cartId: this.cartId } }));
  //   this.selectedCart$ = this.store.select((data) => data.Rider.SelectedCart);
  //   this.selectedCartRider$ = this.store.select((data) => data.User.SelectedCartRiderData);

  //   // ? Get all the rider ratings
  //   this.store.dispatch(RiderActions.GetRiderRatingInitiatedAction({ payload: { userId: this.userId } }));
  //   this.riderRating$ = this.store.select((data) => data.Rider.RiderRating);

  //   // ? Get the cart delivery location
  //   this.subSink.sink =
  //   this.selectedCart$.subscribe((cartData) => {
  //     if (cartData?.id) {
  //       if (cartData?.status !== ProductCartStatus.TRANSIT) {
  //         this.store.dispatch(UserActions.CartIsNotInTransitAction());
  //         // ? navigate the user away
  //         this.router.navigate(["/user", "orders"]);
  //       }

  //       this.store.dispatch(RiderActions.ReverseGeocodingForAddressInitiatedAction({ payload: { address: cartData.deliveryAddress } }));
  //       this.deliveryDestination$ = this.store.select((data) => data.Rider.SelectedAddressToLocation);

  //       // ? Get the data for a cart rider
  //       this.store.dispatch(UserActions.GetCartRiderDataInitiatedAction({ payload: { cartId: this.cartId } }));

  //       this.deliveryDestination$.subscribe((data) => {
  //         if (data?.lat && data?.lng) {
  //           this.customerLocation = {
  //             Tag: "CUSTOMER",
  //             Coordinates: data,
  //             ToolTip: cartData.deliveryAddress
  //           };
  //         }
  //       });

  //       setTimeout(() => {
  //         this.loadMap(this.mapSection, this.riderLocation, this.customerLocation);
  //       }, TrackOrderPage.intervalToWaitBeforeLoadingMap);
  //     }
  //   });
  // }

  private loadMap(
    element: ElementRef,
    riderLocation: GeoPointType,
    cartLocation: GeoPointType
  ): void {
    // ? suppressMarkers is optional
    const directionsRenderer = new google.maps.DirectionsRenderer({
      suppressMarkers: true,
    });
    const directionsService = new google.maps.DirectionsService();

    if (riderLocation?.Coordinates && cartLocation?.Coordinates) {
      this.map = new google.maps.Map(element.nativeElement, {
        center: new google.maps.LatLng(riderLocation?.Coordinates?.lat, riderLocation?.Coordinates?.lng),
        zoom: 10,
      });
      directionsRenderer.setMap(this.map);
      this.locationSrv.calculateAndDisplayRoute(
        directionsService,
        directionsRenderer,
        riderLocation.Coordinates,
        cartLocation.Coordinates
      );

      // ? Add map markers for rider
      const riderLocationInfo = `<h3>Rider position</h3>`;
      this.locationSrv.addMapMarker(this.map, riderLocationInfo, riderLocationInfo, riderLocation.Coordinates, "RIDER");

      // ? Add map markers for rider
      const customerLocationInfo = `<h3>My location</h3>`;
      this.locationSrv.addMapMarker(this.map, customerLocationInfo, customerLocationInfo, cartLocation.Coordinates, "CUSTOMER");
    }
    else {
      // TODO try to load the map if the initial load fails
    }
  }

  encodeTextMessageData(riderFirstName: string): string {
    return encodeURI(`Hello ${riderFirstName}, I wanted to confirm that my delivery is in route`);
  }

  onCheckboxChange(e: any): void {
    const checkArray: FormArray = this.addRiderRatingForm.get("checkArray") as FormArray;

    if (e.target.checked) {
      checkArray.push(new FormControl(e.target.value));
    } else {
      let i = 0;
      checkArray.controls.forEach((item: FormControl) => {
        if (item.value === e.target.value) {
          checkArray.removeAt(i);
          return;
        }
        i++;
      });
    }
  }

  onSubmit(): void {
    if (this.addRiderRatingForm.invalid) {
      return;
    }
    const { review, checkArray } = this.addRiderRatingForm.value;
    const riderQualities = this.setRiderQualities(checkArray);

    if (this.userId) {
        this.store.dispatch(UserActions.RateRiderInitiatedAction({ payload: {
          rating: this.noOfStarsSelected,
          review,
          cartId: this.cartId,
          riderQualityReview: riderQualities,
        }
      }));
    }
  }

  togglePopup(): void {
    this.isPopupOpen = !this.isPopupOpen;
  }

  resetRiderRating(): void {
    this.noOfStarsSelected = 0;
  }

  rateRider(index: number, riderFirstName: string): void {
    this.noOfStarsSelected = index + 1;
    this.riderFirstName = riderFirstName;
  }

  private remapRiderQualities(
    payload: RiderDeliveryFeedbackType,
    oldData: RiderQualityType[]
  )
  : RiderQualityType[] {
    return oldData.map((data) => {
      if (data.key === RiderQuality.IsFast) {
        return {
          ...data,
          value: payload.fast
        };
      }
      if (data.key === RiderQuality.IsEnthusiastic) {
        return {
          ...data,
          value: payload.enthusiastic
        };
      }
      if (data.key === RiderQuality.IsFriendly) {
        return {
          ...data,
          value: payload.friendly
        };
      }
    });
  }

  private subscribeToEvents(): void {
    // ? Get the current location of the cart
    this.subSink.sink =
    this.signalRSrv.currentCartLocation.subscribe((data: CartCurrentLocationData) => {
      this._ngZone.run(() => {
        console.log({ riderCurrentLocation: data });
        if (data?.cartId) {
          const { latitude, longitude } = data;
          this.riderLocation = {
            Coordinates: { lat: latitude, lng: longitude },
            Tag: "RIDER",
            ToolTip: "Rider's location"
          };

          // ? Wait for map to load for applying markers
          setTimeout(() => {
            // ? Update the map marker for rider once they get emitted
            if (this.riderLocation?.Coordinates) {
              // ? Add map markers for rider
              const riderLocationInfo = `<h3>Rider position</h3>`;
              this.locationSrv.addMapMarker(this.map, riderLocationInfo, riderLocationInfo, this.riderLocation.Coordinates, "RIDER");
            }
          }, TrackOrderPage.intervalToWaitBeforeLoadingMap);
        }
      });
    });
  }

  private initForm(payload?: RiderDeliveryFeedbackType): void {
    if (payload) {
      this.addRiderRatingForm = new FormGroup({
        review: new FormControl(payload.review),
        checkArray: new FormArray([]),
      });
    }
    else {
      this.addRiderRatingForm = new FormGroup({
        review: new FormControl(null),
        checkArray: new FormArray([]),
      });
    }
  }

  private setRiderQualities(payload: string[])
  : RiderQualitiesList {
    const returnedData: RiderQualitiesList = {
      Fast: false,
      Friendly: false,
      Enthusiastic: false
    };

    if (payload.includes(RiderQuality.IsEnthusiastic)) {
      returnedData.Enthusiastic = true;
    }
    if (payload.includes(RiderQuality.IsFast)) {
      returnedData.Fast = true;
    }
    if (payload.includes(RiderQuality.IsFriendly)) {
      returnedData.Friendly = true;
    }
    return returnedData;
  }

  ngOnDestroy(): void {
    this.subSink.unsubscribe();
  }
}
