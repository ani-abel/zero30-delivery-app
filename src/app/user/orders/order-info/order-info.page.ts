import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { Store } from "@ngrx/store";
import { Observable } from 'rxjs';
import { SubSink } from 'subsink';
import { ProductCheckout, UserModel } from '../../../user/store/model/user.model';
import { AppState } from '../../../../utils/types/app.model';
import { actions as UserAction } from "../../../user/store/actions/user.action";
import { ProductCartStatus, ProductOrdersResultType } from '../../store/model/user.model';
import { LocalStorageKey } from '../../../../utils/types/app.constant';
import { getDataFromLocalStorage } from '../../../../utils/functions/app.functions';
import { AuthResponseType } from '../../../auth/store/model/auth.model';

@Component({
  selector: 'app-order-info',
  templateUrl: './order-info.page.html',
  styleUrls: ['./order-info.page.scss'],
})
export class OrderInfoPage implements 
OnInit,
OnDestroy {
  cartId: string;
  cartStatus: ProductCartStatus;
  subSink: SubSink = new SubSink();
  selectedCart$: Observable<ProductOrdersResultType>;
  userData$: Observable<UserModel>;
  userId: string;

  constructor(
    private readonly store: Store<AppState>,
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router
  ) { }

  async ngOnInit(): Promise<void> {
    const { userId } = await getDataFromLocalStorage<AuthResponseType>(LocalStorageKey.ZERO_30_USER);
    this.userId = userId;
    this.subSink.sink = 
    this.activatedRoute.queryParams.subscribe((data) => {
      if(!data?.cartId) {
        this.router.navigate(['/user', 'orders']);
      }
      const { cartId, status } = data;
      this.cartId = cartId;
      this.cartStatus = status;

      //? Get selected cart
      this.store.dispatch(UserAction.GetSelectedCartInitiatedAction({ payload: { cartId } }));
      this.selectedCart$ = this.store.select((data) => data.User.SelectedCart);

      //? Get userData
      this.store.dispatch(UserAction.GetUserDataInitiatedAction({ payload: { userId } }))
      this.userData$ = this.store.select((data) => data.User.UserData);
    });
  }

  reorderOldCart(products: ProductCheckout[], deliveryAddress: string): void {
    if(this.userId) {
      //? Delete "OrderId" from product object
      const productsRemapped = products.map((data: ProductCheckout) => {
        const { cost, productId, productName, quantity } = data;
        return {
          cost,
          productId,
          productName,
          quantity
        }
      });
      for(const product of productsRemapped) {
        this.store.dispatch(UserAction.AddProductToCartInitiatedAction({ payload: { userId: this.userId, deliveryAddress, product } }));
      }
      this.router.navigate(["/user", "product-cart"]);
    }
  }

  getProductImageToPreview(products: ProductCheckout[]): string {
    let imagePath: string;
    if(products[0].productImage) {
      imagePath = products[0].productImage;
    }
    else {
      const productsWithImages: ProductCheckout[] = products.filter((data) => data.productImage);
      if(productsWithImages?.length > 0) {
        imagePath = productsWithImages[0].productImage;
      }
    }
    return imagePath;
  }

  ngOnDestroy(): void {
    this.subSink.unsubscribe();
  }

}
