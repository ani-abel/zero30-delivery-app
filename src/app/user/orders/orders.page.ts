import { Component, OnInit } from "@angular/core";
import { Store } from "@ngrx/store";
import { Observable } from "rxjs";
import { AppState } from "../../../utils/types/app.model";
import { ProductCartStatus, ProductOrdersResultType } from "../store/model/user.model";
import { actions as UserAction } from "../../user/store/actions/user.action";
import { getDataFromLocalStorage } from "../../../utils/functions/app.functions";
import { LocalStorageKey } from "../../../utils/types/app.constant";
import { AuthResponseType } from "../../../app/auth/store/model/auth.model";

declare function unBundleSVG();

@Component({
  selector: "app-orders",
  templateUrl: "./orders.page.html",
  styleUrls: ["./orders.page.scss"],
})
export class OrdersPage implements OnInit {
  activeTabType: ProductCartStatus = ProductCartStatus.TRANSIT;
  productCartStatus = ProductCartStatus;
  completedOrders$: Observable<ProductOrdersResultType[]>;
  cancelledOrders$: Observable<ProductOrdersResultType[]>;
  transitOrders$: Observable<ProductOrdersResultType[]>;
  unVerifiedOrders$: Observable<ProductOrdersResultType[]>;

  constructor(
    private readonly store: Store<AppState>
  ) { }

  async ngOnInit(): Promise<void> {
    const { userId } = await getDataFromLocalStorage<AuthResponseType>(LocalStorageKey.ZERO_30_USER);
    if (userId) {
      unBundleSVG();
      this.store.dispatch(UserAction.GetUserOrderInitiatedAction({ payload: { userId } }));
      this.completedOrders$ = this.store.select((data) => data.User.CompletedOrders);
      this.transitOrders$ = this.store.select((data) => data.User.TransitOrders);
      this.cancelledOrders$ = this.store.select((data) => data.User.CancelledOrders);
      this.unVerifiedOrders$ = this.store.select((data) => data.User.UnverifiedOrders);
    }
  }

  switchTabs(tab: ProductCartStatus): void {
    this.activeTabType = tab;
  }
}
