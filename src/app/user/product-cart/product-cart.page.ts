import { Component, NgZone, OnDestroy, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Store } from "@ngrx/store";
import { SubSink } from "subsink";
import { Observable } from "rxjs";
import { actions as UserAction } from "../../user/store/actions/user.action";
import { AppState } from "../../../utils/types/app.model";
import {
  CartConfirmationType,
  CheckedOutProduct,
  ProductCartStatus,
  ProductDetailType
} from "../store/model/user.model";
import { SignalrWebsocketService } from "../../services/signalr-websocket.service";
import { Platform } from "@ionic/angular";

@Component({
  selector: "app-product-cart",
  templateUrl: "./product-cart.page.html",
  styleUrls: ["./product-cart.page.scss"],
})
export class ProductCartPage implements
OnInit,
OnDestroy {
  productCart$: Observable<CheckedOutProduct>;
  productCart: CheckedOutProduct;
  productCartCount$: Observable<number>;
  cartItemDetails$: Observable<ProductDetailType[]>;
  productCartStatus = ProductCartStatus;
  subSink: SubSink = new SubSink();
  processedCart$: Observable<CheckedOutProduct>;
  processedCart: CheckedOutProduct; // Sync
  isProcessedCartVerified$: Observable<ProductCartStatus>;
  isCheckoutBtnHidden = false;
  productCartVertificationStatus: ProductCartStatus = ProductCartStatus.UNVERIFIED;

  constructor(
    private readonly store: Store<AppState>,
    private readonly signalRSrv: SignalrWebsocketService,
    private readonly router: Router,
    private readonly _ngZone: NgZone,
    private readonly platform: Platform,
  ) {
    this.subscribeToEvents();
    /**
     * When the app gets activated from backgriund mode, 
     * then display the details from the cart
     */
    this.subSink.sink =
    this.platform.resume.subscribe(async () => {
      this.getProcessedCartAsync();
    });
  }

  ionViewWillEnter(): void {
    this.getProcessedCartAsync();
  }

  ngOnInit(): void {
    this.productCart$ = this.store.select((data) => data.User.CartItems);
    this.productCartCount$ = this.store.select((data) => data.User.CartItemCount);

    this.subSink.sink =
    this.productCartCount$.subscribe((data) => {
      if (data < 1) {
        this.router.navigate(["/user", "discover-items"]);
      }
    });

    this.subSink.sink =
    this.productCart$.subscribe((data) => this.productCart = data);

    for (const product of this.productCart?.productCheckouts) {
      this.store.dispatch(UserAction.GetSingleProductInitiatedAction({ payload: { productId: product.productId } }));
    }
    this.cartItemDetails$ = this.store.select((data) => data.User.SelectedCartProductsList);
  }

  removeProductFromCart(productId: string): void {
    // ? Dispatch an action that removes the product from store
    this.store.dispatch(UserAction.RemoveProductFromCartInitiatedAction({ payload: { productId } }));
  }

  cartCheckout(cart: CheckedOutProduct): void {
    this.isCheckoutBtnHidden = true;
    this.store.dispatch(UserAction.CartCheckoutInitiatedAction({ payload: cart }));
    this.getProcessedCartAsync();

    this.subSink.sink =
    this.processedCart$.subscribe((data) => {
      if (data?.cartId) {
        /**
         * Make a websocket connection to make known that Cart verification is needed
         */
        this.signalRSrv.makeCartVerificationRequest(data.cartId);
      }
    });
  }

  makePayment(cartId: string): void {
    /**
     * Route to payment page
     */
    this.router.navigate(["/user", "make-payment", cartId]);
  }

  private subscribeToEvents(): void {
    this.subSink.sink =
    this.signalRSrv.cartVerified.subscribe((payload: CartConfirmationType) => {
      this._ngZone.run(() => {
       if (payload?.status) {
          this.store.dispatch(UserAction.VerifyProcessedCartAction({ payload: { status: payload, processedCart: this.processedCart } }));
          this.productCartVertificationStatus = payload.status;
        }
      });
    });
  }

  private getProcessedCartAsync(): void {
    this.processedCart$ = this.store.select((data) => data.User.ProcessedProductCart);
      this.subSink.sink =
      this.processedCart$.subscribe((data) => {
        if (data?.cartId) {
          this.processedCart = data;
        }
      });
  }

  ngOnDestroy(): void {
    this.signalRSrv.disconnect();
    this.subSink.unsubscribe();
  }
}
