import { Cart } from "../../../rider/store/model/rider.model";

export enum PaymentMethodType {
    CASH = 0,
    POS = 1,
    PAYSTACK = 2,
    TRANSFER = 3
}

export enum PaymentMethodTextType {
    CASH = "CASH",
    POS = "POS",
    PAYSTACK = "PAYSTACK",
    TRANSFER = "TRANSFER"
}

export enum ProductCartStatus {
    VERIFIED = "VERIFIED",
    UNVERIFIED = "UNVERIFIED",
    TRANSIT = "TRANSIT",
    COMPLETED = "COMPLETED",
    CANCELLED = "CANCELLED"
}

export enum OrderStatus {
    UNVERIFIED = 0,
    VERIFIED = 1,
    TRANSIT = 2,
    COMPLETED = 3,
    CANCELLED = 4
}

export enum MessageActionType {
    CART_VERIFIED = "CART_VERIFIED",
    RIDER_ASSIGNED_TO_CART = "RIDER_ASSIGNED_TO_CART"
}

export interface CartAssignedResponseType {
    id: string;
    message: string;
    status: string;
}
  
export interface MessageAction {
    Message: string;
    Type: MessageActionType;
    Label: string;
}

export interface ProductCostModel {
    id: string;
    productId: string;
    dateCreated: Date;
    unitPrice: number;
    active: boolean;
}

export interface ProductCategoryModel {
    id: string;
    name: string;
    active: boolean;
}

export interface ProductModel {
    id: string;
    name: string;
    description: string;
    dateCreated: Date;
    product_CategoryId: number;
    merchantId: string;
    active: boolean;
    productCategory: ProductCategoryModel;
}

export interface MerchantType {
    id: number;
    name: string;
    active: boolean;
}

export interface MerchantModel {
    id: string;
    companyName: string;
    tradeMark: string;
    logo: string;
    dateCreated: Date;
    active: boolean;
    email: string;
    address: string;
    description: string;
    phoneNo: string;
    merchant_TypeId: number;
    merchantTypeModel: MerchantType;
    rate: number;
    ratingCount: number;
    distance: number;
    latitude: number;
    longitude: number;
    reviews?: string[];
}

export interface ProductImage {
    id: string;
    productId: string;
    path: string;
    active: boolean;
    description: string;
    dateCreated: Date;
}

export interface ProductCategoryType {
    productCategory: ProductCategoryModel;
    productDetails: ProductDetailType[];
}

export interface ProductDiscoveryType {
    productProductCategoryIndexModels?: [{
        productDetails: [
            {
                productCostModel: ProductCostModel,
                productModel: ProductModel,
                merchantModel: MerchantModel,
                productImageModels: ProductImage[],
                productRating: number,
            }
        ]
    }];
    
    productMerchantIndexModels?: [{
        productDetails: [
            {
                productCostModel: ProductCostModel,
                productModel: ProductModel,
                merchantModel: MerchantModel,
                productImageModels: ProductImage[],
                productRating: number,
            }
        ]
    }];
}

export interface ProductSearchResultType {
    productCostModel: ProductCostModel;
    productModel: ProductModel;
    productImageModels: ProductImage[];
    productRating: number;
    ratingCount: number;
    reviews: string[];
}

// tslint:disable-next-line: no-empty-interface
export interface ProductOrdersResultType extends CheckedOutProduct  { }

export interface ProductOrdersGroupType {
    unverified: ProductOrdersResultType[];
    verified: ProductOrdersResultType[];
    transit: ProductOrdersResultType[];
    completed: ProductOrdersResultType[];
    cancelled: ProductOrdersResultType[];
}

export interface ProductDetailType {
    productCostModel: ProductCostModel;
    productModel: ProductModel;
    merchantModel: MerchantModel;
    productImageModels: ProductImage[];
    productRating: number;
    ratingCount: number;
    reviews: string[];
}

export interface ProductMerchantType {
    merchant: MerchantModel;
    productDetails: ProductDetailType[];
}

export interface FavouriteItemModel {
    merchants: MerchantModel[];
    productDetails: ProductDetailType[];
}

export enum BookmarkType {
    PRODUCT = "PRODUCT",
    MERCHANT = "MERCHANT"
}

export interface UserBookmark {
    /**
     * this refers to I.E product.Id
     */
    Id: string;

    /**
     * This refers to the context of the id. I.E PRODUCT
     */
    Type: BookmarkType;
}

export interface CartConfirmationType {
    id: string;
    message: string;
    status: ProductCartStatus;
  }

export interface PersonModel {
    id: string;
    firstName: string;
    lastName: string;
    phoneNo: string;
    address: string;
    dob: Date;
    created: Date;
    active: boolean;
    passportUrl: string;
}

export interface UserModel {
    userId: string;
    username: string;
    passwordHash: string;
    passwordSalt: string;
    lastLogin: Date;
    isVerified: boolean;
    signUpDate: Date;
    personId: string;
    person: PersonModel;
    role: number;
    active: boolean;
    latitude: number;
    longitude: number;
}

export interface UpdateUserPayloadType {
    userId: string;
    password?: string;
    phoneNo?: string;
    email?: string;
    firstName?: string
    lastName?: string;
    address?: string;
    dob?: Date;
}

export interface AddMerchantRatingPayloadType {
    MerchantId: string;
    UserId: string;
    Rate: number;
    Review: string;
}

export interface ProductCategoryGroupType {
    productDetailModel: ProductDetailType[];
    productCategoryModel: ProductCategoryModel;

}

export interface MerchantReviewType {
    id: string;
    merchantId: string;
    dateCreated: Date;
    rating: number;
    userId: string;
    active: boolean;
    merchant: MerchantModel;
    user: UserModel;
    review: string;
}

export interface RiderDeliveryFeedbackType {
    id: string;
    userRiderId: string;
    dateCreated: Date;
    rating: number;
    review: string;
    cartId: string;
    active: boolean;
    enthusiastic: boolean;
    fast: boolean;
    friendly: boolean;
    cart: Cart;
    userRider: UserModel;
}

/**
 * Represents a product selected by a user to cart
 */
export interface ProductCheckout {
    productName: string;
    cost: number;
    productId: string;
    productImage?: string;
    quantity: number;
    orderId?: string;
}

/**
 * Represents a user's product cart
 */
export interface CheckedOutProduct {
    productCheckouts: ProductCheckout[];
    userId: string;
    deliveryAddress?: string;
    distance?: number;
    cartId?: string;
    deliveryCost?: number;
    vat?: number;
    total?: number;
    dateCreated?: Date;
    paymentInvoice?: string;
    status?: ProductCartStatus;
}

export interface RiderQualitiesList {
    Fast: boolean;
    Friendly: boolean;
    Enthusiastic: boolean;
  }

export interface UserState {
    IsLoading: boolean;
    ActiveError: Error;
    ActiveMessage: string;
    ProductsByMerchants: ProductDiscoveryType;
    ProductSearchResults: ProductSearchResultType[],
    FavouriteMerchants: MerchantModel[];
    FavouriteProducts: ProductDetailType[];
    ProductsOnHotSale: ProductDetailType[];
    SelectedCartProductsList: ProductDetailType[];
    ProductOrderHistory: ProductDetailType[];
    MerchantOrderHistory: MerchantModel[];
    MerchantType: MerchantType[];
    MerchantOrderedByType: MerchantModel[];
    SelectedMerchant: MerchantModel;
    SelectedMerchantReviews: MerchantReviewType[];
    SelectedMerchantHotSaleProducts: ProductDetailType[];
    ProductCategoryGroups: ProductCategoryGroupType[];
    ProductsGroupedByCategory: ProductSearchResultType[];
    UserData: UserModel;
    CartItemCount: number;
    CartItems: CheckedOutProduct;
    /**
     * This refers to the cart once it's been validated by the backend
     */
    ProcessedProductCart: CheckedOutProduct;
    UnverifiedOrders: ProductOrdersResultType[];
    VerifiedOrders: ProductOrdersResultType[];
    TransitOrders: ProductOrdersResultType[];
    CompletedOrders: ProductOrdersResultType[];
    CancelledOrders: ProductOrdersResultType[];
    SelectedCart: ProductOrdersResultType;
    IsProcessedCartVerified: ProductCartStatus;
    ActiveActionMessage: MessageAction;
    SelectedCartRiderData: UserModel;
    SelectedCartDeliveryFeedback: RiderDeliveryFeedbackType;
    NearbyMerchantsByMerchantType: MerchantModel[];
}

export const InitialUserState: UserState =  {
    IsLoading: false,
    ActiveError: undefined,
    ActiveMessage: undefined,
    ProductsByMerchants: undefined,
    ProductSearchResults: [],
    FavouriteMerchants:[],
    FavouriteProducts: [],
    ProductsOnHotSale: [],
    ProductOrderHistory: [],
    MerchantOrderHistory: [],
    MerchantOrderedByType: [],
    MerchantType: [],
    SelectedMerchant: undefined,
    SelectedMerchantReviews: [],
    SelectedMerchantHotSaleProducts: [],
    ProductCategoryGroups: [],
    ProductsGroupedByCategory: [],
    SelectedCartProductsList: [],
    UserData: undefined,
    CartItems: undefined,
    ProcessedProductCart: undefined,
    CartItemCount: 0,
    UnverifiedOrders: [],
    VerifiedOrders: [],
    TransitOrders: [],
    CompletedOrders: [],
    CancelledOrders: [],
    NearbyMerchantsByMerchantType: [],
    SelectedCart: undefined,
    IsProcessedCartVerified: undefined,
    ActiveActionMessage: undefined,
    SelectedCartRiderData: undefined,
    SelectedCartDeliveryFeedback: undefined,
}