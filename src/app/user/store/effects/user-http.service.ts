import { Injectable } from "@angular/core";
import { HttpClient} from "@angular/common/http";
import { Observable } from "rxjs";
import { retry, throttleTime } from "rxjs/operators";
import { environment as env } from "../../../../environments/environment";
import {
    AddMerchantRatingPayloadType,
    CheckedOutProduct,
    FavouriteItemModel,
    MerchantModel,
    MerchantReviewType,
    MerchantType,
    PaymentMethodType,
    ProductCategoryGroupType,
    ProductDetailType,
    ProductDiscoveryType,
    ProductOrdersResultType,
    ProductSearchResultType,
    RiderDeliveryFeedbackType,
    RiderQualitiesList,
    UpdateUserPayloadType
} from "../model/user.model";
import { UserModel } from "../../../auth/store/model/auth.model";

@Injectable({
    providedIn: "root"
})
export class UserHttpService {
    constructor(private readonly httpClient: HttpClient) {}

    getProductsByMerchant()
    : Observable<ProductDiscoveryType> {
        try {
            return this.httpClient
                    .get<ProductDiscoveryType>(`${env.apiRoot}/api/Product/GetProductIndex?includeMerchant=true&includeCategory=true`)
                    .pipe(
                        throttleTime(500),
                        retry(3)
                    );
        }
        catch(ex) {
            throw ex;
        }
    }

    getProductsByNearestMerchant(userId: string)
    : Observable<ProductDiscoveryType> {
        try {
            return this.httpClient
                    .get<ProductDiscoveryType>(`${env.apiRoot}/api/Product/GetProductIndexCloserToUser?UserId=${userId}&includeMerchant=true&includeCategory=true`)
                    .pipe(
                        throttleTime(500),
                        retry(3)
                    );
        }
        catch (ex) {
            throw ex;
        }
    }

    searchForProducts(searchTerm: string)
    : Observable<ProductSearchResultType[]> {
        return this.httpClient
                    .get<ProductSearchResultType[]>(`${env.apiRoot}/api/Product/SearchProductsByName?Name=${searchTerm}&includeRating=true&includeImages=true&includeCost=true`)
                    .pipe(
                        throttleTime(500),
                        retry(3)
                    );
    }

    getUserFavourites(userId: string): Observable<FavouriteItemModel> {
        try {
            return this.httpClient
                    .get<FavouriteItemModel>(`${env.apiRoot}/api/Product/GetFavouriteItem?UserId=${userId}&includeMerchant=true&includeProduct=true`)
                    .pipe(
                        throttleTime(500),
                        retry(3)
                    );
        }
        catch(ex) {
            throw ex;
        }
    }

    addMerchantToBookmarks(userId: string, merchantId: string)
    : Observable<boolean> {
        try {
            return this.httpClient
            .post<boolean>(`${env.apiRoot}/api/Merchant/AddFavouriteProduct?UserId=${userId}&MerchantId=${merchantId}`, {})
            .pipe(
                throttleTime(500),
                retry(3)
            );
        }
        catch(ex) {
            throw ex;
        }
    }

    getProductsOnHotSaleList(): Observable<ProductDetailType[]> {
        try {
            return this.httpClient
                        .get<ProductDetailType[]>(`${env.apiRoot}/api/Product/GetHotSaleProducts?includeRating=true&includeImages=true&includeCost=true`)
                        .pipe(
                            throttleTime(500),
                            retry(3)
                        );
        }
        catch(ex) {
            throw ex;
        }
    }

    getUserOrderHistory(userId: string): Observable<ProductSearchResultType[]> {
        try {
            return this.httpClient
                        .get<ProductSearchResultType[]>(`${env.apiRoot}/api/Shopping/GetUserOrderHistory?User=${userId}&includeRating=true&includeImages=true&includeCost=true`)
                        .pipe(
                            throttleTime(500),
                            retry(3)
                        );
        }
        catch(ex) {
            throw ex;
        }
    }

    addProductToBookmark(productId: string, userId: string): Observable<boolean> {
        try {
            return this.httpClient
                    .post<boolean>(`${env.apiRoot}/api/Product/AddFavouriteProduct?UserId=${userId}&ProductId=${productId}`, {}).pipe(
                        throttleTime(500),
                        retry(3)
                    );
        }
        catch(ex) {
            throw ex;
        }
    }

    getUserData(userId: string): Observable<UserModel> {
        try {
            return this.httpClient
                        .get<UserModel>(`${env.apiRoot}/api/Customer/Get?userId=${userId}&includeperson=true&includeOrder=false&includeActiveCart=false&includeAllCart=false&completedCart=false`)
                        .pipe(
                            throttleTime(500),
                            retry(3)
                        );
        }
        catch(ex) {
            throw ex;
        }
    }

    getMerchantType(): Observable<MerchantType[]> {
        try {
            return this.httpClient
                        .get<MerchantType[]>(`${env.apiRoot}/api/MerchantType`)
                        .pipe(
                            throttleTime(500),
                            retry(3)
                        );
        }
        catch(ex) {
            throw ex;
        }
    }

    updateUserProfile(payload: UpdateUserPayloadType): Observable<boolean> {
        try {
            return this.httpClient
                    .post<boolean>(`${env.apiRoot}/api/Customer/EditCustomerProfile`, payload)
                    .pipe(
                        throttleTime(500),
                        retry(3)
                    );
        }
        catch(ex) {
            throw ex;
        }
    }

    changeUserPassport(file: File, userId: string): Observable<string> {
        try {
            const formData: FormData = new FormData();
            formData.append("UserId", userId);
            formData.append("File", file, file.name);
            
            /**
             * { responseType: "text" as "json" }
             * This is done to specify that this endpoint returns a string(filePath)
             * It stop httpClient from expecting a Json request payload
             */
            return this.httpClient
                    .post<string>(`${env.apiRoot}/api/Account/AddPassport`, formData, { responseType: "text" as "json" })
                    .pipe(
                        throttleTime(500),
                        retry(3)
                    );
        }
        catch(ex) {
            throw ex;
        }
    }

    getMerchantByType(merchantTypeId: number)
    : Observable<MerchantModel[]> {
        try {
            return this.httpClient
                        .get<MerchantModel[]>(`${env.apiRoot}/api/Merchant/FindMerchantByType?typeId=${merchantTypeId}&all=true&isActive=true`)
                        .pipe(
                            throttleTime(500),
                            retry(3)
                        );
        }
        catch(ex){
            throw ex;
        }
    }

    getMerchantById(id: string): Observable<MerchantModel> {
        try {
            return this.httpClient
                        .get<MerchantModel>(`${env.apiRoot}/api/Merchant/GetById?id=${id}`)
                        .pipe(
                            throttleTime(500),
                            retry(3)
                        );
        }
        catch(ex) {
            throw ex;
        }
    }

    addMerchantRating(payload: AddMerchantRatingPayloadType)
    : Observable<boolean> {
        try {
            const { MerchantId, Rate, Review, UserId } = payload;
            return this.httpClient
                        .post<boolean>(`${env.apiRoot}/api/Merchant/RateMerchant?UserId=${UserId}&MerchantId=${MerchantId}&Rate=${Rate}&Review=${Review}`, {})
                        .pipe(
                            throttleTime(500),
                            retry(3)
                        );

        }
        catch(ex) {
            throw ex;
        }
    }

    getMerchantReviews(merchantId: string)
    : Observable<MerchantReviewType[]> {
        try {
            return this.httpClient
                        .get<MerchantReviewType[]>(`${env.apiRoot}/api/Merchant/GetMerchantReviews?MerchantId=${merchantId}`)
                        .pipe(
                            throttleTime(500),
                            retry(3)
                        );
        }
        catch(ex) {
            throw ex;
        }
    }

    getHotSaleProductsForMerchant(merchantId: string)
    : Observable<ProductDetailType[]> {
        try {
            return this.httpClient
                        .get<ProductDetailType[]>(`${env.apiRoot}/api/Product/GetMerchantHotSaleProducts?MerchantId=${merchantId}&includeRating=true&includeImages=true&includeCost=true`)
                        .pipe(
                            throttleTime(500),
                            retry(3)
                        );
        }
        catch(ex) {
            throw ex;
        }
    }

    getMerchantGroupedProducts(merchantId: string)
    : Observable<ProductCategoryGroupType[]> {
        try {
            return this.httpClient
                        .get<ProductCategoryGroupType[]>(`${env.apiRoot}/api/Product/GetMerchantGroupedProducts?MerchantId=${merchantId}&includeRating=true&includeImages=true&includeCost=true`)
                        .pipe(
                            throttleTime(500),
                            retry(3)
                        );
        }
        catch(ex) {
            throw ex;
        }
    }

    getCategoryGroupedProducts(categoryId: string)
    : Observable<ProductSearchResultType[]> {
        try {
            return this.httpClient
                        .get<ProductSearchResultType[]>(`${env.apiRoot}/api/Product/GetProductsByCategory?CategoryId=${categoryId}&includeRating=true&includeImages=true&includeCost=true`)
                        .pipe(
                            throttleTime(500),
                            retry(3)
                        );
        }
        catch(ex) {
            throw ex;
        }
    }

    getSelectedProduct(productId: string)
    : Observable<ProductDetailType> {
        return this.httpClient
                .get<ProductDetailType>(`${env.apiRoot}/api/Product/GetProduct?ProductId=${productId}&includeRating=true&includeImages=true&includeCost=true&includeOrdercount=true`)
                .pipe(
                    throttleTime(500),
                    retry(3)
                );
    }

    productCheckout(payload: CheckedOutProduct)
    : Observable<CheckedOutProduct> {
        try {
            return this.httpClient
                        .post<CheckedOutProduct>(`${env.apiRoot}/api/Shopping/CheckOut`, payload)
                        .pipe(
                            throttleTime(500),
                            retry(3)
                        );
        }
        catch(ex) {
            throw ex;
        }
    }

    makePayment(cartId: string, paymentOption: PaymentMethodType)
    : Observable<boolean> {
        return this.httpClient
                .post<boolean>(`${env.apiRoot}/api/Payment/MakePayment?CartId=${cartId}&PaymentOption=${paymentOption}`, {})
                .pipe(
                    throttleTime(500),
                    retry(3)
                );

    }

    confirmPaystackPayment(referenceNo: string)
    : Observable<boolean> {
        try {
            return this.httpClient
                    .post<boolean>(`${env.apiRoot}/api/Payment/ConfirmPaystackPayment?referenceNo=${referenceNo}`, {})
                    .pipe(
                        throttleTime(500),
                        retry(3)
                    );
        }
        catch(ex) {
            throw ex;
        }
    }

    getUserProductOrders(userId: string)
    : Observable<ProductOrdersResultType[]> {
        try {
            return this.httpClient
                        .get<ProductOrdersResultType[]>(`${env.apiRoot}/api/Shopping/GetCartByUserId?UserId=${userId}&includeOrder=true&includeActiveCart=true&includeAllCart=true&completedCart=true`)
                        .pipe(
                            throttleTime(500),
                            retry(3)
                        );
        }
        catch(ex) {
            throw ex;
        }
    }

    getCartById(cartId: string): Observable<ProductOrdersResultType> {
        try {
            return this.httpClient
                        .get<ProductOrdersResultType>(`${env.apiRoot}/api/Shopping/GetCartByCartId?CartId=${cartId}&includeOrder=true`)
                        .pipe(
                            throttleTime(500),
                            retry(3)
                        );
        }
        catch(ex) {
            throw ex;
        } 
    }

    rateRider(
        cartId: string,
        rate: number,
        review: string,
        riderQualityReview: RiderQualitiesList
    ): Observable<boolean> {
        const url: string = `${env.apiRoot}/api/Rider/RateRider?CartId=${cartId}&Rate=${rate}&review=${review}&IsEnthusiastic=${riderQualityReview.Enthusiastic}&IsFast=${riderQualityReview.Fast}&IsFriendly=${riderQualityReview.Friendly}`;
        return this.httpClient
                    .post<boolean>(url, {})
                    .pipe(
                        throttleTime(500),
                        retry(3)
                    );
    }

    getRiderDeliveryFeedback(cartId: string)
    : Observable<RiderDeliveryFeedbackType> {
        try {
            return this.httpClient
                       .get<RiderDeliveryFeedbackType>(`${env.apiRoot}/api/Rider/GetRiderCartRatingModel?CartId=${cartId}`)
                       .pipe(
                        throttleTime(500),
                        retry(3)
                    );
        }
        catch(ex) {
            throw ex;
        }
    }

    getCartRiderData(cartId: string)
    : Observable<UserModel> {
        try {
            return this.httpClient
                        .get<UserModel>(`${env.apiRoot}/api/Shopping/GetRiderBy?CartId=${cartId}`)
                        .pipe(
                            throttleTime(500),
                            retry(3)
                        );
        }
        catch(ex) {
            throw ex;
        }
    }

    getNearbyMerchants(userId: string)
    : Observable<MerchantModel[]> {
        try {
            return this.httpClient
                        .get<MerchantModel[]>(`${env.apiRoot}/api/Merchant/GetCloseByMerchant?UserId=${userId}`)
                        .pipe(
                            throttleTime(500),
                            retry(3)
                        );
        }
        catch (ex) {
            throw ex;
        }
    }

}