import { Injectable } from "@angular/core";
import { 
    createEffect,
    Actions,
    ofType
} from "@ngrx/effects";
import { of } from "rxjs";
import { 
    tap,
    switchMap,
    catchError,
    map
} from "rxjs/operators";
import { actions as UserActions } from "../actions/user.action";
import { UserHttpService } from "./user-http.service";
import { addBookmarkToLocalStorage } from "../../../../utils/functions/app.functions";
import { BookmarkType } from "../model/user.model";

@Injectable({
    providedIn: "root"
})
export class UserEffectService {
    constructor(
        private readonly userHttpSrv: UserHttpService,
        private readonly actions$: Actions,
    ) {}

    getProductsByMerchants$ = createEffect(() => 
        this.actions$.pipe(
            ofType(UserActions.GetProductsByMerchantInitiatedAction),
            switchMap((action) => {
                return this.userHttpSrv.getProductsByNearestMerchant(action.payload?.userId).pipe(
                    map((data) => UserActions.GetProductsByMerchantSuccessfulAction({ payload: data })),
                    catchError((error: Error) => of(UserActions.GetProductsByMerchantFailedAction({ payload: error })))
                );
            })
        ));

    searchForProducts$ = createEffect(() => 
        this.actions$.pipe(
            ofType(UserActions.SearchForProductsInitiatedAction),
            switchMap((action) => {
                return this.userHttpSrv.searchForProducts(action.payload.searchTerm).pipe(
                    map((data) => UserActions.SearchForProductsSuccessfulAction({ payload: data })),
                    catchError((error: Error) => of(UserActions.SearchForProductsFailedAction({ payload: error })))
                )
            })
        )); 

    getUserFavourites$ = createEffect(() => 
        this.actions$.pipe(
            ofType(UserActions.GetUserFavouritesInitiatedAction),
            switchMap((action) => {
                return this.userHttpSrv.getUserFavourites(action.payload.userId).pipe(
                    map((data) => UserActions.GetUserFavouritesSuccessfulAction({ payload: data })),
                    catchError((error: Error) => of(UserActions.GetUserFavouritesFailedAction({ payload: error })))
                )
            })
    ));

    addMerchantToBookmark$ = createEffect(() => 
        this.actions$.pipe(
            ofType(UserActions.AddMerchantToUserBookmarksInitiatedAction),
            switchMap((action) => {
                const { Merchant, MerchantId, UserId } = action.payload;
                return this.userHttpSrv.addMerchantToBookmarks(UserId, MerchantId).pipe(
                    tap(async () => {
                        // ? Add book mark to localstorage
                        await addBookmarkToLocalStorage({ Id: MerchantId, Type: BookmarkType.MERCHANT });
                    }),
                    map((data) => UserActions.AddMerchantToUserBookmarksSuccessfulAction({ payload: { result: data, Merchant } })),
                    catchError((error: Error) => of(UserActions.AddMerchantToUserBookmarksFailedAction({ payload: error })))
                )
            })
        ));

        getProductsOnHotSaleList$ = createEffect(() => 
            this.actions$.pipe(
                ofType(UserActions.GetProductsOnHotSaleInitiatedAction),
                switchMap(() => {;
                    return this.userHttpSrv.getProductsOnHotSaleList().pipe(
                        map((data) => UserActions.GetProductsOnHotSaleSuccessfulAction({ payload: data })),
                        catchError((error: Error) => of(UserActions.GetProductsByMerchantFailedAction({ payload: error })))
                    )
                })
        ));

    getUserOrderHistory$ = createEffect(() => 
        this.actions$.pipe(
            ofType(UserActions.GetUserOrderHistoryInitiatedAction),
            switchMap((action) => {
                return this.userHttpSrv.getUserOrderHistory(action.payload.userId).pipe(
                    map((data) => UserActions.GetUserOrderHistorySuccessfulAction({ payload: data })),
                    catchError((error: Error) => of(UserActions.GetUserOrderHistoryFailedAction({ payload: error })))
                )
            })
        ));

    addProductToBookmarks$ = createEffect(() => 
        this.actions$.pipe(
            ofType(UserActions.AddProductToBookmarkInitiatedAction),
            switchMap((action) => {
                const { userId, productId } = action.payload;
                return this.userHttpSrv.addProductToBookmark(productId, userId).pipe(
                    tap(async () => {
                        //? Add book mark to localstorage
                        await addBookmarkToLocalStorage({ Id: productId, Type: BookmarkType.PRODUCT });
                    }),
                    map((data) => UserActions.AddProductToBookmarkSuccessfulAction({ payload: data })),
                    catchError((error: Error) => of(UserActions.AddProductToBookmarkFailedAction({ payload: error })))
                )
            })
        ));

    getUserData$ = createEffect(() => 
        this.actions$.pipe(
            ofType(UserActions.GetUserDataInitiatedAction),
            switchMap((action) => {
                return this.userHttpSrv.getUserData(action.payload.userId).pipe(
                    map((data) => UserActions.GetUserDataSuccessfulAction({ payload: data })),
                    catchError((error: Error) => of(UserActions.GetUserDataFailedAction({ payload: error })))
                )
            })
        ));

    updateUserProfile$ = createEffect(() => 
        this.actions$.pipe(
            ofType(UserActions.UpdateUserProfileInitiatedAction),
            switchMap((action) => {
                const { payload } = action;
                return this.userHttpSrv.updateUserProfile(payload).pipe(
                    map((data) => UserActions.UpdateUserProfileSuccessfulAction({ payload: { result: data, updatedUserData: payload } })),
                    catchError((error: Error) => of(UserActions.UpdateUserProfileFailedAction({ payload: error })))
                );
            })
        ));

    changeProfileImage$ = createEffect(() => 
        this.actions$.pipe(
            ofType(UserActions.UploadProfileImageInitiatedAction),
            switchMap((action) => {
                const { file, userId } = action.payload;
                return this.userHttpSrv.changeUserPassport(file, userId).pipe(
                    map((data) => UserActions.UploadProfileImageSuccessfulAction({ payload: data })),
                    catchError((error: Error) => of(UserActions.UploadProfileImageFailedAction({ payload: error })))
                )
            })
        ));

    getMerchantTypes$ = createEffect(() => 
        this.actions$.pipe(
            ofType(UserActions.GetMerchantTypeInitiatedAction),
            switchMap(() => {
                return this.userHttpSrv.getMerchantType().pipe(
                    map((data) => UserActions.GetMerchantTypeSuccessfulAction({ payload: data })),
                    catchError((error: Error) => of(UserActions.GetMerchantTypeFailedAction({ payload: error })))
                )
            })
        ));

    getMerchantsByType$ = createEffect(() => 
        this.actions$.pipe(
            ofType(UserActions.GetMerchantByTypeInitiatedAction),
            switchMap((action) => {
                return this.userHttpSrv.getMerchantByType(action.payload.typeId).pipe(
                    map((data) => UserActions.GetMerchantByTypeSuccessfulAction({ payload: data })),
                    catchError((error: Error) => of(UserActions.GetMerchantByTypeFailedAction({ payload: error })))
                )
            })
        ));

    getSelectedMerchant$ = createEffect(() => 
        this.actions$.pipe(
            ofType(UserActions.GetMerchantDetailInitiatedAction),
            switchMap((action) => {
                return this.userHttpSrv.getMerchantById(action.payload.typeId).pipe(
                    map((data) => UserActions.GetMerchantDetailSuccessfulAction({ payload: data })),
                    catchError((error: Error) => of(UserActions.GetMerchantDetailFailedAction({ payload: error })))
                )
            })
        ));


    addMerchantRating$ = createEffect(() => 
        this.actions$.pipe(
            ofType(UserActions.AddMerchantRatingInitiatedAction),
            switchMap((action) => {
                return this.userHttpSrv.addMerchantRating(action.payload).pipe(
                    map((data) => UserActions.AddMerchantRatingSuccessfulAction({ payload: data })),
                    catchError((error: Error) => of(UserActions.AddMerchantRatingFailedAction({ payload: error })))
                )
            })
        ));

    getMerchantReviews$ = createEffect(() => 
        this.actions$.pipe(
            ofType(UserActions.GetMerchantReviewsInitiatedAction),
            switchMap((action) => {
                return this.userHttpSrv.getMerchantReviews(action.payload.merchantId).pipe(
                    map((data) => UserActions.GetMerchantReviewsSuccessfulAction({ payload: data })),
                    catchError((error: Error) => of(UserActions.GetMerchantReviewsFailedAction({ payload: error })))
                )
            })
        ));

    getMerchantHotSaleProducts$ = createEffect(() => 
        this.actions$.pipe(
            ofType(UserActions.GetHotSaleProductForMerchantInitiatedAction),
            switchMap((action) => {
                return this.userHttpSrv.getHotSaleProductsForMerchant(action.payload.merchantId).pipe(
                    map((data) => UserActions.GetHotSaleProductForMerchantSuccessfulAction({ payload: data })),
                    catchError((error: Error) => of(UserActions.GetHotSaleProductForMerchantFailedAction({ payload: error })))
                )
            })
        ));

    getMerchantGroupedProducts$ = createEffect(() => 
        this.actions$.pipe(
            ofType(UserActions.GetMechantProductsGroupedByCategoryInitiatedAction),
            switchMap((action) => {
                return this.userHttpSrv.getMerchantGroupedProducts(action.payload.merchantId).pipe(
                    map((data) => UserActions.GetMechantProductsGroupedByCategorySuccessfulAction({ payload: data })),
                    catchError((error: Error) => of(UserActions.GetMechantProductsGroupedByCategoryFailedAction({ payload: error })))
                )
            })
        ));

    getCategoryGroupedProducts$ = createEffect(() => 
        this.actions$.pipe(
            ofType(UserActions.GetProductsGroupedByCategoryInitiatedAction),
            switchMap((action) => {
                return this.userHttpSrv.getCategoryGroupedProducts(action.payload.categoryId).pipe(
                    map((data) => UserActions.GetProductsGroupedByCategorySuccessfulAction({ payload: data })),
                    catchError((error: Error) => of(UserActions.GetProductsGroupedByCategoryFailedAction({ payload: error })))
                )
            })
        ));
    
    getSingleProductById$ = createEffect(() =>
        this.actions$.pipe(
            ofType(UserActions.GetSingleProductInitiatedAction),
            switchMap((action) => {
                return this.userHttpSrv.getSelectedProduct(action.payload.productId).pipe(
                    map((data) => UserActions.GetSingleProductSuccessfulAction({ payload: data })),
                    catchError((error: Error) => of(UserActions.GetSingleProductFailedAction({ payload: error })))
                )
            })
        ));

    addProductToCart$ = createEffect(() => 
        this.actions$.pipe(
            ofType(UserActions.AddProductToCartInitiatedAction),
            map(() => UserActions.AddProductToCartSuccessfulAction()),
            catchError((error: Error) => of(UserActions.AddProductToCartFailedAction({ payload: error })))
        ));

    removeProductToCart$ = createEffect(() => 
        this.actions$.pipe(
            ofType(UserActions.RemoveProductFromCartInitiatedAction),
            map(() => UserActions.RemoveProductFromCartSuccessfulAction()),
            catchError((error: Error) => of(UserActions.RemoveProductFromCartFailedAction({ payload: error })))
        ));

    processCart$ = createEffect(() =>
        this.actions$.pipe(
            ofType(UserActions.CartCheckoutInitiatedAction),
            switchMap((action) => {
                return this.userHttpSrv.productCheckout(action.payload).pipe(
                    map((data) => UserActions.CartCheckoutSuccessfulAction({ payload: data })),
                    catchError((error: Error) => of(UserActions.CartCheckoutFailedAction({ payload: error })))
                )
            })
        ));

    makePayment$ = createEffect(() => 
        this.actions$.pipe(
            ofType(UserActions.MakePaymentInitiatedAction),
            switchMap((action) => {
                const { payload: { cartId, paymentOption } } = action;
                return this.userHttpSrv.makePayment(cartId, paymentOption).pipe(
                    map((data) => UserActions.MakePaymentSuccessfulAction({ payload: data })),
                    catchError((error: Error) => of(UserActions.MakePaymentFailedAction({ payload: error })))
                )
            })
        ));

    confirmPaystackPayment$ = createEffect(() => 
        this.actions$.pipe(
            ofType(UserActions.ConfirmPaystackPaymentInitiatedAction),
            switchMap((action) => {
                return this.userHttpSrv.confirmPaystackPayment(action.payload.refrenceNo).pipe(
                    map((data) => UserActions.ConfirmPaystackPaymentSuccessfulAction({ payload: data })),
                    catchError((error: Error) => of(UserActions.ConfirmPaystackPaymentFailedAction({ payload: error })))
                )
            })
        ));

    getUserOrders$ = createEffect(() => 
        this.actions$.pipe(
            ofType(UserActions.GetUserOrderInitiatedAction),
            switchMap((action) => {
                return this.userHttpSrv.getUserProductOrders(action.payload.userId).pipe(
                    map((data) => UserActions.GetUserOrderSuccessfulAction({ payload: data })),
                    catchError((error: Error) => of(UserActions.GetUserOrderFailedAction({ payload: error })))
                )
            })
        ));

    getSelectedCartById$ = createEffect(() => 
        this.actions$.pipe(
            ofType(UserActions.GetSelectedCartInitiatedAction),
            switchMap((action) => { 
                return this.userHttpSrv.getCartById(action.payload.cartId).pipe(
                    map((data) => UserActions.GetSelectedCartSuccessfulAction({ payload: data })),
                    catchError((error: Error) => of(UserActions.GetSelectedCartFailedAction({ payload: error })))
                )
            })
        ));

    rateRider$ = createEffect(() => 
        this.actions$.pipe(
            ofType(UserActions.RateRiderInitiatedAction),
            switchMap((action) => {
                const { payload: { review, rating, cartId, riderQualityReview } } = action;
                return this.userHttpSrv.rateRider(cartId, rating, review, riderQualityReview).pipe(
                    map((data) => UserActions.RateRiderSuccessfulAction({ payload: data })),
                    catchError((error: Error) => of(UserActions.RateRiderFailedAction({ payload: error })))
                )
            })
        ));  
        

    getCartRiderData$ = createEffect(() => 
        this.actions$.pipe(
            ofType(UserActions.GetCartRiderDataInitiatedAction),
            switchMap((action) => {
                return this.userHttpSrv.getCartRiderData(action.payload.cartId).pipe(
                    map((data) => UserActions.GetCartRiderDataSuccessfulAction({ payload: data })),
                    catchError((error: Error) => of(UserActions.GetCartRiderDataFailedAction({ payload: error })))
                )
            })
        ));

    getRiderCartDeliveryFeedback$ = createEffect(() => 
        this.actions$.pipe(
            ofType(UserActions.GetRiderDeliveryRatingInitiatedAction),
            switchMap((action) => {
                return this.userHttpSrv.getRiderDeliveryFeedback(action.payload.cartId).pipe(
                    map((data) => UserActions.GetRiderDeliveryRatingSuccessfulAction({ payload: data })),
                    catchError((error: Error) => of(UserActions.GetRiderDeliveryRatingFailedAction({ payload: error })))
                )
            })
        ));

    getNearbyMerchants$ = createEffect(() =>
        this.actions$.pipe(
            ofType(UserActions.GetNearbyMerchantsInitiatedAction),
            switchMap((action) => {
                const { payload: { userId, typeId } } = action;
                return this.userHttpSrv.getNearbyMerchants(userId).pipe(
                    map((data) => UserActions.GetNearbyMerchantsSuccessfulAction({ payload: { data, typeId } })),
                    catchError((error: Error) => of(UserActions.GetNearbyMerchantsFailedAction({ payload: error })))
                )
            })
        ));
}