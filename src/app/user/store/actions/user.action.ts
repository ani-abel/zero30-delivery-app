import { createAction, props } from "@ngrx/store";
import { UserModel } from "../../../auth/store/model/auth.model";
import {
    AddMerchantRatingPayloadType,
    CartConfirmationType,
    CheckedOutProduct,
    FavouriteItemModel,
    MerchantModel,
    MerchantReviewType,
    MerchantType,
    PaymentMethodType,
    ProductCategoryGroupType,
    ProductCheckout,
    ProductDetailType,
    ProductDiscoveryType,
    ProductOrdersResultType,
    ProductSearchResultType,
    RiderDeliveryFeedbackType,
    RiderQualitiesList,
    UpdateUserPayloadType
} from "../model/user.model";

export enum UserActionType {
    CLEAR_ACTIVE_ERROR = "[ERROR_USER] CLEAR_ACTIVE_ERROR",
    CLEAR_ACTIVE_MESSAGE = "[ERROR_USER] CLEAR_ACTIVE_MESSAGE",
    CLEAR_ACTIVE_ACTION_MESSAGE = "[ERROR_USER] CLEAR_ACTIVE_ACTION_MESSAGE",

    GET_PRODUCTS_BY_MERCHANT_INITIATED = "[PRODUCT DISCOVERY] GET_PRODUCTS_BY_MERCHANT_INITIATED",
    GET_PRODUCTS_BY_MERCHANT_FAILED = "[PRODUCT DISCOVERY] GET_PRODUCTS_BY_MERCHANT_FAILED",
    GET_PRODUCTS_BY_MERCHANT_SUCCESSFUL = "[PRODUCT DISCOVERY] GET_PRODUCTS_BY_MERCHANT_SUCCESSFUL",

    SEARCH_FOR_PRODUCTS_INITIATED = "[VIEW_SEARCH_RESULTS] SEARCH_FOR_PRODUCTS_INITIATED",
    SEARCH_FOR_PRODUCTS_FAILED = "[VIEW_SEARCH_RESULTS] SEARCH_FOR_PRODUCTS_FAILED",
    SEARCH_FOR_PRODUCTS_SUCCESSFUL = "[VIEW_SEARCH_RESULTS] SEARCH_FOR_PRODUCTS_SUCCESSFUL",

    GET_USER_FAVOURITES_INITIATED = "[PRODUCT_DISCOVERY] GET_USER_FAVOURITES_INITIATED",
    GET_USER_FAVOURITES_FAILED = "[PRODUCT_DISCOVERY] GET_USER_FAVOURITES_FAILED",
    GET_USER_FAVOURITES_SUCCESSFUL = "[PRODUCT_DISCOVERY] GET_USER_FAVOURITES_SUCCESSFUL",

    ADD_MERCHANT_TO_USER_BOOKMARKS_INITIATED = "[BOOKMARKS] ADD_MERCHANT_TO_USER_BOOKMARKS_INITIATED",
    ADD_MERCHANT_TO_USER_BOOKMARKS_FAILED = "[BOOKMARKS] ADD_MERCHANT_TO_USER_BOOKMARKS_FAILED",
    ADD_MERCHANT_TO_USER_BOOKMARKS_SUCCESSFUL = "[BOOKMARKS] ADD_MERCHANT_TO_USER_BOOKMARKS_SUCCESSFUL",

    GET_PRODUCTS_ON_HOT_SALE_INITIATED = "[HOT_SALE] GET_PRODUCTS_ON_HOT_SALE_INITIATED",
    GET_PRODUCTS_ON_HOT_SALE_FAILED = "[HOT_SALE] GET_PRODUCTS_ON_HOT_SALE_FAILED",
    GET_PRODUCTS_ON_HOT_SALE_SUCCESSFUL = "[HOT_SALE] GET_PRODUCTS_ON_HOT_SALE_SUCCESSFUL",

    GET_USER_ORDER_HISTORY_INITIATED = "[ORDER_HISTORY] GET_USER_ORDER_HISTORY_INITIATED",
    GET_USER_ORDER_HISTORY_FAILED = "[ORDER_HISTORY] GET_USER_ORDER_HISTORY_FAILED",
    GET_USER_ORDER_HISTORY_SUCCESSFUL = "[ORDER_HISTORY] GET_USER_ORDER_HISTORY_SUCCESSFUL",

    ADD_PRODUCT_TO_BOOKMARK_INITIATED = "[BOOKMARK_PRODUCT] ADD_PRODUCT_TO_BOOKMARK_INITIATED",
    ADD_PRODUCT_TO_BOOKMARK_FAILED = "[BOOKMARK_PRODUCT] ADD_PRODUCT_TO_BOOKMARK_FAILED",
    ADD_PRODUCT_TO_BOOKMARK_SUCCESSFUL = "[BOOKMARK_PRODUCT] ADD_PRODUCT_TO_BOOKMARK_SUCCESSFUL",

    GET_USER_INFO_INITIATED = "[USER_INFO] GET_USER_INFO_INITIATED",
    GET_USER_INFO_FAILED = "[USER_INFO] GET_USER_INFO_FAILED",
    GET_USER_INFO_SUCCESSFUL = "[USER_INFO] GET_USER_INFO_SUCCESSFUL",

    UPDATE_USER_PROFILE_INITIATED = "[USER_PROFILE] UPDATE_USER_PROFILE_INITIATED",
    UPDATE_USER_PROFILE_FAILED = "[USER_PROFILE] UPDATE_USER_PROFILE_FAILED",
    UPDATE_USER_PROFILE_SUCCESSFUL = "[USER_PROFILE] UPDATE_USER_PROFILE_SUCCESSFUL",

    UPLOAD_PROFILE_IMAGE_INITIATED = "[USER_PROFILE] UPLOAD_PROFILE_IMAGE_INITIATED",
    UPLOAD_PROFILE_IMAGE_FAILED = "[USER_PROFILE] UPLOAD_PROFILE_IMAGE_FAILED",
    UPLOAD_PROFILE_IMAGE_SUCCESSFUL = "[USER_PROFILE] UPLOAD_PROFILE_IMAGE_SUCCESSFUL",

    GET_MERCHANT_TYPE_INITIATED = "[MERCHANT_TYPE] GET_MERCHANT_TYPE_INITIATED",
    GET_MERCHANT_TYPE_FAILED = "[MERCHANT_TYPE] GET_MERCHANT_TYPE_FAILED",
    GET_MERCHANT_TYPE_SUCCESSFUL = "[MERCHANT_TYPE] GET_MERCHANT_TYPE_SUCCESSFUL",

    GET_MERCHANT_BY_TYPE_INITIATED = "[MERCHANTS] GET_MERCHANT_BY_TYPE_INITIATED",
    GET_MERCHANT_BY_TYPE_FAILED = "[MERCHANTS] GET_MERCHANT_BY_TYPE_FAILED",
    GET_MERCHANT_BY_TYPE_SUCCESSFUL = "[MERCHANTS] GET_MERCHANT_BY_TYPE_SUCCESSFUL",

    GET_MERCHANT_DETAIL_INITIATED = "[BUSINESS_DETAIL] GET_BUSINESS_DETAIL_INITIATED",
    GET_MERCHANT_DETAIL_FAILED = "[BUSINESS_DETAIL] GET_BUSINESS_DETAIL_FAILED",
    GET_MERCHANT_DETAIL_SUCCESSFUL = "[BUSINESS_DETAIL] GET_BUSINESS_DETAIL_SUCCESSFUL",

    ADD_MERCHANT_RATING_INITIATED = "[RATING] ADD_MERCHANT_RATING_INITIATED",
    ADD_MERCHANT_RATING_FAILED = "[RATING] ADD_RATE_MERCHANT_FAILEDADD_MERCHANT_RATING_FAILED",
    ADD_MERCHANT_RATING_SUCCESSFUL = "[RATING] ADD_MERCHANT_RATING_SUCCESSFUL",

    GET_MERCHANT_REVIEWS_INTIIATED = "[RATING] GET_MERCHANT_REVIEWS_INTIIATED",
    GET_MERCHANT_REVIEWS_FAILED = "[RATING] GET_MERCHANT_REVIEWS_FAILED",
    GET_MERCHANT_REVIEWS_SUCCESSFUL = "[RATING] GET_MERCHANT_REVIEWS_SUCCESSFUL",

    GET_HOT_SALE_PRODUCT_FOR_MERCHANT_INITIATED = "[HOT_SALE] GET_HOT_SALE_PRODUCT_FOR_MERCHANT_INITIATED",
    GET_HOT_SALE_PRODUCT_FOR_MERCHANT_FAILED = "[HOT_SALE] GET_HOT_SALE_PRODUCT_FOR_MERCHANT_FAILED",
    GET_HOT_SALE_PRODUCT_FOR_MERCHANT_SUCCESSFUL = "[HOT_SALE] GET_HOT_SALE_PRODUCT_FOR_MERCHANT_SUCCESSFUL",

    GET_MERCHANT_PRODUCTS_GROUPED_BY_CATEGORY_INITIATED = "[HOT SALE] GET_MERCHANT_PRODUCTS_GROUPED_BY_CATEGORY_INITIATED",
    GET_MERCHANT_PRODUCTS_GROUPED_BY_CATEGORY_FAILED = "[HOT SALE] GET_MERCHANT_PRODUCTS_GROUPED_BY_CATEGORY_FAILED",
    GET_MERCHANT_PRODUCTS_GROUPED_BY_CATEGORY_SUCCESSFUL = "[HOT SALE] GET_MERCHANT_PRODUCTS_GROUPED_BY_CATEGORY_SUCCESSFUL",

    GET_PRODUCTS_GROUPED_BY_CATEGORY_INITIATED = "[PRODUCTS] GET_PRODUCTS_GROUPED_BY_CATEGORY_INITIATED",
    GET_PRODUCTS_GROUPED_BY_CATEGORY_FAILED = "[PRODUCTS] GET_PRODUCTS_GROUPED_BY_CATEGORY_FAILES",
    GET_PRODUCTS_GROUPED_BY_CATEGORY_SUCCESSFUL = "[PRODUCTS] GET_PRODUCTS_GROUPED_BY_CATEGORY_SUCCESSFUL",

    ADD_PRODUCT_TO_CART_INITIATED = "[CART] ADD_PRODUCT_TO_CART_INITIATED",
    ADD_PRODUCT_TO_CART_FAILED = "[CART] ADD_PRODUCT_TO_CART_FAILED",
    ADD_PRODUCT_TO_CART_SUCCESSFUL = "[CART] ADD_PRODUCT_TO_CART_SUCCESSFUL",

    CART_CHECKOUT_INITIATED = "[CART] CART_CHECKOUT_INITIATED",
    CART_CHECKOUT_FAILED = "[CART] CART_CHECKOUT_FAILED",
    CART_CHECKOUT_SUCCESSFUL = "[CART] CART_CHECKOUT_SUCCESSFUL",

    REMOVE_PRODUCT_FROM_CART_INITIATED = "[CART] REMOVE_PRODUCT_FROM_CART_INITIATED",
    REMOVE_PRODUCT_FROM_CART_FAILED = "[CART] REMOVE_PRODUCT_FROM_CART_FAILED",
    REMOVE_PRODUCT_FROM_CART_SUCCESSFUL = "[CART] REMOVE_PRODUCT_FROM_CART_SUCCESSFUL",

    GET_SINGLE_PRODUCT_INITIATED = "[CART] GET_SINGLE_PRODUCT_INITIATED",
    GET_SINGLE_PRODUCT_FAILED = "[CART] GET_SINGLE_PRODUCT_FAILED",
    GET_SINGLE_PRODUCT_SUCCESSFUL = "[CART] GET_SINGLE_PRODUCT_SUCCESSFUL",

    MAKE_PAYMENT_INITIATED = "[PAYMENT] MAKE_PAYMENT_INITIATED",
    MAKE_PAYMENT_FAILED = "[PAYMENT] MAKE_PAYMENT_FAILED",
    MAKE_PAYMENT_SUCCESSFUL = "[PAYMENT] MAKE_PAYMENT_SUCCESSFUL",

    CONFIRM_PAYSTACK_PAYMENT_INITIATED = "[PAYMENT] CONFIRM_PAYSTACK_PAYMENT_INITIATED",
    CONFIRM_PAYSTACK_PAYMENT_FAILED = "[PAYMENT] CONFIRM_PAYSTACK_PAYMENT_FAILED",
    CONFIRM_PAYSTACK_PAYMENT_SUCCESSFUL = "[PAYMENT] CONFIRM_PAYSTACK_PAYMENT_SUCCESSFUL",

    GET_USER_ORDERS_INITIATED = "[USER_ORDERS] GET_ORDERS_INITIATED",
    GET_USER_ORDERS_FAILED = "[USER_ORDERS] GET_ORDERS_FAILED",
    GET_USER_ORDERS_SUCCESSFUL = "[USER_ORDERS] GET_ORDERS_SUCCESSFUL",

    GET_SELECTED_CART_INITIATED = "[ORDER] GET_SELECTED_CART_INITIATED",
    GET_SELECTED_CART_FAILED = "[ORDER] GET_SELECTED_CART_FAILED",
    GET_SELECTED_CART_SUCCESSFUL = "[ORDER] GET_SELECTED_CART_SUCCESSFUL",

    RATE_RIDER_INITIATED = "[RIDER_RATING] RATE_RIDER_INITIATED",
    RATE_RIDER_FAILED = "[RIDER_RATING] RATE_RIDER_FAILED",
    RATE_RIDER_SUCCESSFUL = "[RIDER_RATING] RATE_RIDER_SUCCESSFUL",

    GET_CART_RIDER_DATA_INITIATED = "[RIDER] GET_CART_RIDER_DATA_INITIATED",
    GET_CART_RIDER_DATA_FAILED = "[RIDER] GET_CART_RIDER_DATA_FAILED",
    GET_CART_RIDER_DATA_SUCCESSFUL = "[RIDER] GET_CART_RIDER_DATA_SUCCESSFUL",

    GET_RIDER_DELIVERY_RATING_INITIATED = "[CART_DELIVERY] GET_RIDER_DELIVERY_RATING_INITIATED",
    GET_RIDER_DELIVERY_RATING_FAILED = "[CART_DELIVERY] GET_RIDER_DELIVERY_RATING_FAILED",
    GET_RIDER_DELIVERY_RATING_SUCCESSFUL = "[CART_DELIVERY] GET_RIDER_DELIVERY_RATING_SUCCESSFUL",

    VERIFY_PROCESSED_CART = "[CART] VERIFY_PROCESSED_CART",
    CLEAR_PROCESSED_CART = "[CART] VERIFY_PROCESSED_CART",
    CART_IS_NOT_IN_TRANSIT = "[CART] CART_IS_NOT_IN_TRANSIT",

    GET_NEARBY_MERCHANTS_INITIATED = "[MERCHANT_LISTING] GET_NEARBY_MERCHANTS_INITIATED",
    GET_NEARBY_MERCHANTS_FAILED = "[MERCHANT_LISTING] GET_NEARBY_MERCHANTS_FAILED",
    GET_NEARBY_MERCHANTS_SUCCESSFUL = "[MERCHANT_LISTING] GET_NEARBY_MERCHANTS_SUCCESSFUL",

    START_LOADER_REMOTELY = "[USER_LOADER] START_LOADER_REMOTELY",
    END_LOADER_REMOTELY = "[USER_LOADER] END_LOADER_REMOTELY",
    CLEAR_USER_STATE = "[LOGOUT] CLEAR_USER_STATE",
}

export const actions = {
    ClearActiveErrorAction: createAction(
        UserActionType.CLEAR_ACTIVE_ERROR
    ),
    ClearActiveMessageAction: createAction(
        UserActionType.CLEAR_ACTIVE_MESSAGE
    ),
    ClearActiveActionMessageAction: createAction(
        UserActionType.CLEAR_ACTIVE_ACTION_MESSAGE
    ),
    GetProductsByMerchantInitiatedAction: createAction(
        UserActionType.GET_PRODUCTS_BY_MERCHANT_INITIATED,
        props<{ payload: { userId: string } }>()
    ),
    GetProductsByMerchantFailedAction: createAction(
        UserActionType.GET_PRODUCTS_BY_MERCHANT_FAILED,
        props<{ payload: Error }>()
    ),
    GetProductsByMerchantSuccessfulAction: createAction(
        UserActionType.GET_PRODUCTS_BY_MERCHANT_SUCCESSFUL,
        props<{ payload: ProductDiscoveryType }>()
    ),
    SearchForProductsInitiatedAction: createAction(
        UserActionType.SEARCH_FOR_PRODUCTS_INITIATED,
        props<{ payload: { searchTerm: string } }>()
    ),
    SearchForProductsFailedAction: createAction(
        UserActionType.SEARCH_FOR_PRODUCTS_FAILED,
        props<{ payload: Error }>()
    ),
    SearchForProductsSuccessfulAction: createAction(
        UserActionType.SEARCH_FOR_PRODUCTS_SUCCESSFUL,
        props<{ payload: ProductSearchResultType[] }>()
    ),
    GetUserFavouritesInitiatedAction: createAction(
        UserActionType.GET_USER_FAVOURITES_INITIATED,
        props<{ payload: { userId: string } }>()
    ),
    GetUserFavouritesFailedAction: createAction(
        UserActionType.GET_USER_FAVOURITES_FAILED,
        props<{ payload: Error }>()
    ),
    GetUserFavouritesSuccessfulAction: createAction(
        UserActionType.GET_USER_FAVOURITES_SUCCESSFUL,
        props<{ payload: FavouriteItemModel }>()
    ),
    AddMerchantToUserBookmarksInitiatedAction: createAction(
        UserActionType.ADD_MERCHANT_TO_USER_BOOKMARKS_INITIATED,
        props<{ payload: { UserId: string, MerchantId: string, Merchant: MerchantModel }}>()
    ),
    AddMerchantToUserBookmarksFailedAction: createAction(
        UserActionType.ADD_MERCHANT_TO_USER_BOOKMARKS_FAILED,
        props<{ payload: Error }>()
    ),
    AddMerchantToUserBookmarksSuccessfulAction: createAction(
        UserActionType.ADD_MERCHANT_TO_USER_BOOKMARKS_SUCCESSFUL,
        props<{ payload: { result: boolean, Merchant: MerchantModel } }>()
    ),
    GetProductsOnHotSaleInitiatedAction: createAction(
        UserActionType.GET_PRODUCTS_BY_MERCHANT_INITIATED
    ),
    GetProductsOnHotSaleFailedAction: createAction(
        UserActionType.GET_PRODUCTS_BY_MERCHANT_FAILED,
        props<{ payload: Error }>()
    ),
    GetProductsOnHotSaleSuccessfulAction: createAction(
        UserActionType.GET_PRODUCTS_ON_HOT_SALE_SUCCESSFUL,
        props<{ payload: ProductDetailType[] }>()
    ),
    GetUserOrderHistoryInitiatedAction: createAction(
        UserActionType.GET_USER_ORDER_HISTORY_INITIATED,
        props<{ payload: { userId: string } }>()
    ),
    GetUserOrderHistoryFailedAction: createAction(
        UserActionType.GET_USER_ORDER_HISTORY_FAILED,
        props<{ payload: Error }>()
    ),
    GetUserOrderHistorySuccessfulAction: createAction(
        UserActionType.GET_USER_ORDER_HISTORY_SUCCESSFUL,
        props<{ payload: ProductSearchResultType[] }>()
    ),
    AddProductToBookmarkInitiatedAction: createAction(
        UserActionType.ADD_PRODUCT_TO_BOOKMARK_INITIATED,
        props<{ payload: { productId: string, userId: string } }>()
    ),
    AddProductToBookmarkFailedAction: createAction(
        UserActionType.ADD_PRODUCT_TO_BOOKMARK_FAILED,
        props<{ payload: Error }>()
    ),
    AddProductToBookmarkSuccessfulAction: createAction(
        UserActionType.ADD_PRODUCT_TO_BOOKMARK_SUCCESSFUL,
        props<{ payload: boolean }>()
    ),
    GetUserDataInitiatedAction: createAction(
        UserActionType.GET_USER_INFO_INITIATED,
        props<{ payload: { userId: string } }>()
    ),
    GetUserDataFailedAction: createAction(
        UserActionType.GET_USER_INFO_FAILED,
        props<{ payload: Error }>()
    ),
    GetUserDataSuccessfulAction: createAction(
        UserActionType.GET_USER_INFO_SUCCESSFUL,
        props<{ payload: UserModel }>()
    ),
    UpdateUserProfileInitiatedAction: createAction(
        UserActionType.UPDATE_USER_PROFILE_INITIATED,
        props<{ payload: UpdateUserPayloadType }>()
    ),
    UpdateUserProfileFailedAction: createAction(
        UserActionType.UPDATE_USER_PROFILE_FAILED,
        props<{ payload: Error }>()
    ),
    UpdateUserProfileSuccessfulAction: createAction(
        UserActionType.UPDATE_USER_PROFILE_SUCCESSFUL,
        props<{ payload: {
                result: boolean,
                updatedUserData: UpdateUserPayloadType
            } }>()
    ),
    UploadProfileImageInitiatedAction: createAction(
        UserActionType.UPLOAD_PROFILE_IMAGE_INITIATED,
        props<{ payload: { userId: string, file: File } }>()
    ),
    UploadProfileImageFailedAction: createAction(
        UserActionType.UPLOAD_PROFILE_IMAGE_FAILED,
        props<{ payload: Error }>()
    ),
    UploadProfileImageSuccessfulAction: createAction(
        UserActionType.UPLOAD_PROFILE_IMAGE_SUCCESSFUL,
        props<{ payload: string }>()
    ),
    ClearUserStateAction: createAction(
        UserActionType.CLEAR_USER_STATE,
    ),
    GetMerchantTypeInitiatedAction: createAction(
        UserActionType.GET_MERCHANT_TYPE_INITIATED,
    ),
    GetMerchantTypeFailedAction: createAction(
        UserActionType.GET_MERCHANT_TYPE_FAILED,
        props<{ payload: Error }>()
    ),
    GetMerchantTypeSuccessfulAction: createAction(
        UserActionType.GET_MERCHANT_TYPE_SUCCESSFUL,
        props<{ payload: MerchantType[] }>()
    ),
    GetMerchantByTypeInitiatedAction: createAction(
        UserActionType.GET_MERCHANT_BY_TYPE_INITIATED,
        props<{ payload: { typeId: number } }>()
    ),
    GetMerchantByTypeFailedAction: createAction(
        UserActionType.GET_MERCHANT_BY_TYPE_FAILED,
        props<{ payload: Error }>()
    ),
    GetMerchantByTypeSuccessfulAction: createAction(
        UserActionType.GET_MERCHANT_BY_TYPE_SUCCESSFUL,
        props<{ payload: MerchantModel[] }>()
    ),
    GetMerchantDetailInitiatedAction: createAction(
        UserActionType.GET_MERCHANT_DETAIL_INITIATED,
        props<{ payload: { typeId: string } }>()
    ),
    GetMerchantDetailFailedAction: createAction(
        UserActionType.GET_MERCHANT_DETAIL_FAILED,
        props<{ payload: Error }>()
    ),
    GetMerchantDetailSuccessfulAction: createAction(
        UserActionType.GET_MERCHANT_DETAIL_SUCCESSFUL,
        props<{ payload: MerchantModel }>()
    ),
    AddMerchantRatingInitiatedAction: createAction(
        UserActionType.ADD_MERCHANT_RATING_INITIATED,
        props<{ payload: AddMerchantRatingPayloadType }>()
    ),
    AddMerchantRatingFailedAction: createAction(
        UserActionType.ADD_MERCHANT_RATING_FAILED,
        props<{ payload: Error }>()
    ),
    AddMerchantRatingSuccessfulAction: createAction(
        UserActionType.ADD_MERCHANT_RATING_SUCCESSFUL,
        props<{ payload: boolean }>()
    ),
    GetMerchantReviewsInitiatedAction: createAction(
        UserActionType.GET_MERCHANT_REVIEWS_INTIIATED,
        props<{ payload: { merchantId: string } }>()
    ),
    GetMerchantReviewsFailedAction: createAction(
        UserActionType.GET_MERCHANT_REVIEWS_FAILED,
        props<{ payload: Error }>()
    ),
    GetMerchantReviewsSuccessfulAction: createAction(
        UserActionType.GET_MERCHANT_REVIEWS_SUCCESSFUL,
        props<{ payload: MerchantReviewType[] }>()
    ),
    GetHotSaleProductForMerchantInitiatedAction: createAction(
        UserActionType.GET_HOT_SALE_PRODUCT_FOR_MERCHANT_INITIATED,
        props<{ payload: { merchantId: string } }>()
    ),
    GetHotSaleProductForMerchantSuccessfulAction: createAction(
        UserActionType.GET_HOT_SALE_PRODUCT_FOR_MERCHANT_SUCCESSFUL,
        props<{ payload: ProductDetailType[] }>()
    ),
    GetHotSaleProductForMerchantFailedAction: createAction(
        UserActionType.GET_HOT_SALE_PRODUCT_FOR_MERCHANT_FAILED,
        props<{ payload: Error }>()
    ),
    GetMechantProductsGroupedByCategoryInitiatedAction: createAction(
        UserActionType.GET_MERCHANT_PRODUCTS_GROUPED_BY_CATEGORY_INITIATED,
        props<{ payload: { merchantId: string } }>()
    ),
    GetMechantProductsGroupedByCategoryFailedAction: createAction(
        UserActionType.GET_MERCHANT_PRODUCTS_GROUPED_BY_CATEGORY_FAILED,
        props<{ payload: Error }>()
    ),
    GetMechantProductsGroupedByCategorySuccessfulAction: createAction(
        UserActionType.GET_MERCHANT_PRODUCTS_GROUPED_BY_CATEGORY_SUCCESSFUL,
        props<{ payload: ProductCategoryGroupType[] }>()
    ),
    GetProductsGroupedByCategoryInitiatedAction: createAction(
        UserActionType.GET_PRODUCTS_GROUPED_BY_CATEGORY_INITIATED,
        props<{ payload: { categoryId: string } }>()
    ),
    GetProductsGroupedByCategoryFailedAction: createAction(
        UserActionType.GET_PRODUCTS_GROUPED_BY_CATEGORY_FAILED,
        props<{ payload: Error }>()
    ),
    GetProductsGroupedByCategorySuccessfulAction: createAction(
        UserActionType.GET_PRODUCTS_GROUPED_BY_CATEGORY_SUCCESSFUL,
        props<{ payload: ProductSearchResultType[] }>()
    ),
    AddProductToCartInitiatedAction: createAction(
        UserActionType.ADD_PRODUCT_TO_CART_INITIATED,
        props<{ payload: {
            product: ProductCheckout,
            userId: string,
            deliveryAddress: string
        } }>()
    ),
    AddProductToCartFailedAction: createAction(
        UserActionType.ADD_PRODUCT_TO_CART_FAILED,
        props<{ payload: Error }>()
    ),
    AddProductToCartSuccessfulAction: createAction(
        UserActionType.ADD_PRODUCT_TO_CART_SUCCESSFUL
    ),
    CartCheckoutInitiatedAction: createAction(
        UserActionType.CART_CHECKOUT_INITIATED,
        props<{ payload: CheckedOutProduct }>()
    ),
    CartCheckoutFailedAction: createAction(
        UserActionType.CART_CHECKOUT_FAILED,
        props<{ payload: Error }>()
    ),
    CartCheckoutSuccessfulAction: createAction(
        UserActionType.CART_CHECKOUT_SUCCESSFUL,
        props<{ payload: CheckedOutProduct }>()
    ),
    RemoveProductFromCartInitiatedAction: createAction(
        UserActionType.REMOVE_PRODUCT_FROM_CART_INITIATED,
        props<{ payload: { productId: string } }>()
    ),
    RemoveProductFromCartFailedAction: createAction(
        UserActionType.REMOVE_PRODUCT_FROM_CART_FAILED,
        props<{ payload: Error }>()
    ),
    RemoveProductFromCartSuccessfulAction: createAction(
        UserActionType.REMOVE_PRODUCT_FROM_CART_SUCCESSFUL
    ),
    GetSingleProductInitiatedAction: createAction(
        UserActionType.GET_SINGLE_PRODUCT_INITIATED,
        props<{ payload: { productId: string } }>()
    ),
    GetSingleProductFailedAction: createAction(
        UserActionType.GET_SINGLE_PRODUCT_FAILED,
        props<{ payload: Error }>()
    ),
    GetSingleProductSuccessfulAction: createAction(
        UserActionType.GET_SINGLE_PRODUCT_SUCCESSFUL,
        props<{ payload: ProductDetailType }>()
    ),
    MakePaymentInitiatedAction: createAction(
        UserActionType.MAKE_PAYMENT_INITIATED,
        props<{ payload: { cartId: string, paymentOption: PaymentMethodType } }>()
    ),
    MakePaymentFailedAction: createAction(
        UserActionType.MAKE_PAYMENT_FAILED,
        props<{ payload: Error }>()
    ),
    MakePaymentSuccessfulAction: createAction(
        UserActionType.MAKE_PAYMENT_SUCCESSFUL,
        props<{ payload: boolean }>()
    ),
    ConfirmPaystackPaymentInitiatedAction: createAction(
        UserActionType.CONFIRM_PAYSTACK_PAYMENT_INITIATED,
        props<{ payload: { refrenceNo: string } }>()
    ),
    ConfirmPaystackPaymentFailedAction: createAction(
        UserActionType.CONFIRM_PAYSTACK_PAYMENT_FAILED,
        props<{ payload: Error }>()
    ),
    ConfirmPaystackPaymentSuccessfulAction: createAction(
        UserActionType.CONFIRM_PAYSTACK_PAYMENT_SUCCESSFUL,
        props<{ payload: boolean}>()
    ),
    GetUserOrderInitiatedAction: createAction(
        UserActionType.GET_USER_ORDERS_INITIATED,
        props<{ payload: { userId: string } }>()
    ),
    GetUserOrderFailedAction: createAction(
        UserActionType.GET_USER_ORDERS_FAILED,
        props<{ payload: Error }>()
    ),
    GetUserOrderSuccessfulAction: createAction(
        UserActionType.GET_USER_ORDERS_SUCCESSFUL,
        props<{ payload: ProductOrdersResultType[] }>()
    ),
    GetSelectedCartInitiatedAction: createAction(
        UserActionType.GET_SELECTED_CART_INITIATED,
        props<{ payload: { cartId: string } }>()
    ),
    GetSelectedCartFailedAction: createAction(
        UserActionType.GET_SELECTED_CART_FAILED,
        props<{ payload: Error }>()
    ),
    GetSelectedCartSuccessfulAction: createAction(
        UserActionType.GET_SELECTED_CART_SUCCESSFUL,
        props<{ payload: ProductOrdersResultType }>()
    ),
    VerifyProcessedCartAction: createAction(
        UserActionType.VERIFY_PROCESSED_CART,
        props<{ payload: { status: CartConfirmationType, processedCart: CheckedOutProduct } }>()
    ),
    ClearProcessedCartAction: createAction(
        UserActionType.CLEAR_PROCESSED_CART
    ),
    CartIsNotInTransitAction: createAction(
        UserActionType.CART_IS_NOT_IN_TRANSIT,
    ),
    RateRiderInitiatedAction: createAction(
        UserActionType.RATE_RIDER_INITIATED,
        props<{ payload:
            {
                review: string,
                rating: number,
                cartId: string,
                riderQualityReview: RiderQualitiesList
            }
        }>()
    ),
    RateRiderFailedAction: createAction(
        UserActionType.RATE_RIDER_FAILED,
        props<{ payload: Error }>()
    ),
    RateRiderSuccessfulAction: createAction(
        UserActionType.RATE_RIDER_SUCCESSFUL,
        props<{ payload: boolean }>()
    ),
    GetCartRiderDataInitiatedAction: createAction(
        UserActionType.GET_CART_RIDER_DATA_INITIATED,
        props<{ payload: { cartId: string } }>()
    ),
    GetCartRiderDataFailedAction: createAction(
        UserActionType.GET_CART_RIDER_DATA_FAILED,
        props<{ payload: Error }>()
    ),
    GetCartRiderDataSuccessfulAction: createAction(
        UserActionType.GET_CART_RIDER_DATA_SUCCESSFUL,
        props<{ payload: UserModel }>()
    ),
    GetRiderDeliveryRatingInitiatedAction: createAction(
        UserActionType.GET_RIDER_DELIVERY_RATING_INITIATED,
        props<{ payload: { cartId: string } }>()
    ),
    GetRiderDeliveryRatingFailedAction: createAction(
        UserActionType.GET_RIDER_DELIVERY_RATING_FAILED,
        props<{ payload: Error }>()
    ),
    GetRiderDeliveryRatingSuccessfulAction: createAction(
        UserActionType.GET_RIDER_DELIVERY_RATING_SUCCESSFUL,
        props<{ payload: RiderDeliveryFeedbackType }>()
    ),
    StartLoaderRemotelyAction: createAction(
        UserActionType.START_LOADER_REMOTELY,
    ),
    EndLoaderRemotelyAction: createAction(
        UserActionType.END_LOADER_REMOTELY,
    ),
    GetNearbyMerchantsInitiatedAction: createAction(
        UserActionType.GET_NEARBY_MERCHANTS_INITIATED,
        props<{ payload: { userId: string, typeId: number } }>()
    ),
    GetNearbyMerchantsFailedAction: createAction(
        UserActionType.GET_NEARBY_MERCHANTS_FAILED,
        props<{ payload: Error }>()
    ),
    GetNearbyMerchantsSuccessfulAction: createAction(
        UserActionType.GET_NEARBY_MERCHANTS_SUCCESSFUL,
        props<{ payload: { data: MerchantModel[], typeId: number } }>()
    ),
};
