import { createReducer, on } from "@ngrx/store";
import {
    CheckedOutProduct,
    InitialUserState,
    MerchantModel,
    MessageActionType,
    ProductCartStatus,
    ProductOrdersGroupType
} from "../model/user.model";
import { actions as UserActions } from "../actions/user.action";
import { APIMessage, LocalStorageKey } from "../../../../utils/types/app.constant";
import {
    addProductToCart,
    deleteDataFromLocalStorage,
    filterMerchantByType,
    groupUserOrders,
    removeProductFromCart,
    saveDataToLocalStorage,
    swapUserDataAfterUpdate
} from "../../../../utils/functions/app.functions";
import { UserModel } from "../../../user/store/model/user.model";

export const UserReducer = createReducer(
    InitialUserState,
    on(UserActions.ClearActiveErrorAction, (state) => {
        return { ...state, ActiveError: undefined, ActiveMessage: undefined, IsLoading: false };
    }),
    on(UserActions.ClearActiveMessageAction, (state) => {
        return { ...state, ActiveError: undefined, ActiveMessage: undefined, IsLoading: false };
    }),
    on(UserActions.ClearActiveActionMessageAction, (state) => {
        return { ...state, ActiveActionMessage: undefined, ActiveMessage: undefined, IsLoading: false };
    }),
    on(UserActions.GetProductsByMerchantInitiatedAction, (state, { payload }) => {
        return { ...state, ActiveError: undefined, IsLoading: true };
    }),
    on(UserActions.GetProductsByMerchantFailedAction, (state, { payload }) => {
        return { ...state, ActiveError: payload, IsLoading: false };
    }),
    on(UserActions.GetProductsByMerchantSuccessfulAction, (state, { payload }) => {
        return { ...state, ProductsByMerchants: payload, IsLoading: false };
    }),
    on(UserActions.SearchForProductsInitiatedAction, (state, { payload }) => {
        return { ...state, IsLoading: true };
    }),
    on(UserActions.SearchForProductsFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(UserActions.SearchForProductsSuccessfulAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ProductSearchResults: [...payload] };
    }),
    on(UserActions.GetUserFavouritesInitiatedAction, (state, { payload }) => {
        return { ...state, IsLoading: true };
    }),
    on(UserActions.GetUserFavouritesFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(UserActions.GetUserFavouritesSuccessfulAction, (state, { payload }) => {
        return {
            ...state,
            IsLoading: false,
            FavouriteMerchants: [...payload?.merchants],
            FavouriteProducts: [...payload?.productDetails]
        };
    }),
    on(UserActions.GetProductsOnHotSaleFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(UserActions.GetProductsOnHotSaleSuccessfulAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ProductsOnHotSale: [...payload] };
    }),
    on(UserActions.GetUserOrderHistoryInitiatedAction, (state, { payload }) => {
        return { ...state, IsLoading: true };
    }),
    on(UserActions.GetUserOrderHistoryFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(UserActions.GetUserOrderHistorySuccessfulAction, (state, { payload }) => {
        return {
            ...state,
            IsLoading: false,
            ProductOrderHistory: [...payload]
        };
    }),
    on(UserActions.AddProductToBookmarkInitiatedAction, (state, { payload }) => {
        return { ...state, IsLoading: true };
    }),
    on(UserActions.AddProductToBookmarkFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(UserActions.AddProductToBookmarkSuccessfulAction, (state, { payload }) => {
        return { 
            ...state, 
            IsLoading: false, 
            // ActiveMessage: "Bookmark added" 
        };
    }),
    on(UserActions.AddMerchantToUserBookmarksSuccessfulAction, (state, { payload }) => {
        return { ...state, IsLoading: true };
    }),
    on(UserActions.AddMerchantToUserBookmarksInitiatedAction, (state, { payload }) => {
        return { ...state, IsLoading: true };
    }),
    on(UserActions.AddMerchantToUserBookmarksFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(UserActions.AddMerchantToUserBookmarksSuccessfulAction, (state, { payload}) => {
        return {
            ...state,
            IsLoading: false,
            // ActiveMessage: "Bookmark added",
            FavouriteMerchants: [...state.FavouriteMerchants, { ...payload.Merchant } ],
        };
    }),
    on(UserActions.GetUserDataInitiatedAction, (state, { payload }) => {
        return { ...state, IsLoading: true };
    }),
    on(UserActions.GetUserDataFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(UserActions.GetUserDataSuccessfulAction, (state, { payload }) => {
        // tslint:disable-next-line: no-string-literal
        return { ...state, IsLoading: false, UserData: payload["user"] };
    }),
    on(UserActions.UpdateUserProfileInitiatedAction, (state, { payload }) => {
        return { ...state, IsLoading: true };
    }),
    on(UserActions.UpdateUserProfileFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(UserActions.UpdateUserProfileSuccessfulAction, (state, { payload }) => {
        if (state?.UserData) {
            const newUserData: UserModel = swapUserDataAfterUpdate(payload.updatedUserData, state.UserData);
            return {
                ...state,
                IsLoading: false,
                ActiveMessage: APIMessage.UPDATE,
                UserData: newUserData
            };
        }
        else {
            return {
                ...state,
                IsLoading: false,
                ActiveMessage: APIMessage.UPDATE
            };
        }
    }),
    on(UserActions.UploadProfileImageInitiatedAction, (state, { payload }) => {
        return { ...state, IsLoading: true };
    }),
    on(UserActions.UploadProfileImageFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(UserActions.UploadProfileImageSuccessfulAction, (state, { payload }) => {
        return  { ...state, IsLoading: false, ActiveMessage: APIMessage.UPDATE };
    }),
    on(UserActions.ClearUserStateAction, (state) => {
        // console.log({ ...InitialUserState });
        // !!! This is just a hack, got to build a root reducer ans modify data there
        return { ...state, UserData: undefined };
    }),
    on(UserActions.GetMerchantTypeInitiatedAction, (state) => {
        return { ...state, IsLoading: true };
    }),
    on(UserActions.GetMerchantTypeFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(UserActions.GetMerchantTypeSuccessfulAction, (state, { payload }) => {
        return { ...state, IsLoading: false, MerchantType: payload };
    }),
    on(UserActions.GetMerchantByTypeInitiatedAction, (state, { payload }) => {
        return { ...state, IsLoading: true };
    }),
    on(UserActions.GetMerchantByTypeFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(UserActions.GetMerchantByTypeSuccessfulAction, (state, { payload }) => {
        return { ...state, IsLoading: false, MerchantOrderedByType: payload };
    }),
    on(UserActions.GetMerchantDetailInitiatedAction, (state, { payload }) => {
        return { ...state, IsLoading: true };
    }),
    on(UserActions.GetMerchantDetailFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(UserActions.GetMerchantDetailSuccessfulAction, (state, { payload }) => {
        return { ...state, IsLoading: false, SelectedMerchant: payload };
    }),
    on(UserActions.AddMerchantRatingInitiatedAction, (state, { payload }) => {
        return { ...state, IsLoading: true };
    }),
    on(UserActions.AddMerchantRatingFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(UserActions.AddMerchantRatingSuccessfulAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveMessage: "Rating added" };
    }),
    on(UserActions.GetMerchantReviewsInitiatedAction, (state, { payload }) => {
        return { ...state, IsLoading: true };
    }),
    on(UserActions.GetMerchantReviewsFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(UserActions.GetMerchantReviewsSuccessfulAction, (state, { payload }) => {
        return { ...state, IsLoading: false, SelectedMerchantReviews: [...payload] };
    }),
    on(UserActions.GetHotSaleProductForMerchantInitiatedAction, (state, { payload }) => {
        return { ...state, IsLoading: true };
    }),
    on(UserActions.GetHotSaleProductForMerchantSuccessfulAction, (state, { payload }) => {
        return { ...state, IsLoading: false, SelectedMerchantHotSaleProducts: [...payload] };
    }),
    on(UserActions.GetHotSaleProductForMerchantFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(UserActions.GetMechantProductsGroupedByCategoryInitiatedAction, (state, { payload }) => {
        return { ...state, IsLoading: true };
    }),
    on(UserActions.GetMechantProductsGroupedByCategoryFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(UserActions.GetMechantProductsGroupedByCategorySuccessfulAction, (state, { payload }) => {
        return {
            ...state,
            IsLoading: false,
            ProductCategoryGroups: payload.length > 0 ? [...payload] : []
        };
    }),
    on(UserActions.GetProductsGroupedByCategoryInitiatedAction, (state, { payload }) => {
        return {...state, IsLoading: true };
    }),
    on(UserActions.GetProductsGroupedByCategoryFailedAction, (state, { payload }) => {
        return {...state, IsLoading: false, ActiveError: payload };
    }),
    on(UserActions.GetProductsGroupedByCategorySuccessfulAction, (state, { payload }) => {
        return {...state, IsLoading: false, ProductsGroupedByCategory: payload };
    }),
    on(UserActions.AddProductToCartInitiatedAction, (state, { payload: { product, deliveryAddress, userId } }) => {
        const currentCart: CheckedOutProduct = addProductToCart(product, state.CartItems, userId, deliveryAddress);
        /**
         * Save cart to localStorage
         */
        saveDataToLocalStorage(currentCart, LocalStorageKey.CART);
        return {
            ...state,
            IsLoading: true,
            CartItems: currentCart,
            CartItemCount: currentCart?.productCheckouts?.length ?? 0,
            // // TODO Confirm new Change
            // ProcessedProductCart: typeof state.ProcessedProductCart === undefined ?
            //     undefined :
            //     {
            //         ...state.ProcessedProductCart,
            //         productCheckouts: [...currentCart.productCheckouts],
            //     }
        };
    }),
    on(UserActions.AddProductToCartFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(UserActions.AddProductToCartSuccessfulAction, (state) => {
        return { 
            ...state, 
            IsLoading: false, 
            //ActiveMessage: "Added to cart" 
        };
    }),
    on(UserActions.CartCheckoutInitiatedAction, (state, { payload }) => {
        return { ...state, IsLoading: true };
    }),
    on(UserActions.CartCheckoutFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(UserActions.CartCheckoutSuccessfulAction, (state, { payload }) => {
        return {
            ...state,
            IsLoading: false,
            ActiveMessage: payload.status === ProductCartStatus.UNVERIFIED ?
             "Checkout completed. Please wait while your cart is verified"
             : undefined,
            ProcessedProductCart: { ...payload }
        };
    }),
    on(UserActions.RemoveProductFromCartInitiatedAction, (state, { payload }) => {
        // ? Remove from state
        const currentCart: CheckedOutProduct = removeProductFromCart(payload.productId, state.CartItems);
        saveDataToLocalStorage(currentCart, LocalStorageKey.CART);
        return {
            ...state,
            IsLoading: true,
            CartItems: currentCart,
            CartItemCount: currentCart?.productCheckouts?.length ?? 0
        };
    }),
    on(UserActions.RemoveProductFromCartFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(UserActions.RemoveProductFromCartSuccessfulAction, (state) => {
        return { ...state, IsLoading: false, ActiveMessage: "Product Removed" };
    }),
    on(UserActions.GetSingleProductInitiatedAction, (state, { payload }) => {
        return { ...state, IsLoading: true };
    }),
    on(UserActions.GetSingleProductFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(UserActions.GetSingleProductSuccessfulAction, (state, { payload }) => {
        return {
            ...state,
            IsLoading: false,
            SelectedCartProductsList: [...state.SelectedCartProductsList, payload]
        };
    }),
    on(UserActions.MakePaymentInitiatedAction, (state, { payload }) => {
        return { ...state, IsLoading: true };
    }),
    on(UserActions.MakePaymentFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(UserActions.MakePaymentSuccessfulAction, (state, { payload }) => {
        if (payload) {
            deleteDataFromLocalStorage(LocalStorageKey.CART);
            return {
                ...state,
                ProcessedProductCart: undefined,
                CartItems: undefined,
                CartItemCount: undefined,
                IsLoading: false,
                ActiveMessage: payload ?
                 "Payment completed" :
                 undefined
            };
        }
        else {
            return {
                ...state,
                IsLoading: false,
            };
        }
    }),
    on(UserActions.ConfirmPaystackPaymentInitiatedAction, (state, { payload }) => {
        return { ...state, IsLoading: true };
    }),
    on(UserActions.ConfirmPaystackPaymentFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(UserActions.ConfirmPaystackPaymentSuccessfulAction, (state, { payload }) => {
        return {
            ...state,
            IsLoading: false,
            ProcessedProductCart: undefined,
            CartItems: undefined, // ? Clear Cart
            ActiveMessage: payload === true ? "Payment Completed" : undefined
        };
    }),
    on(UserActions.GetUserOrderInitiatedAction, (state, { payload }) => {
        return { ...state, IsLoading: true };
    }),
    on(UserActions.GetUserOrderFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(UserActions.GetUserOrderSuccessfulAction, (state, { payload }) => {
        const groupedResult: ProductOrdersGroupType = groupUserOrders(payload);
        return {
            ...state,
            IsLoading: false,
            UnverifiedOrders: groupedResult.unverified,
            VerifiedOrders: groupedResult.verified,
            TransitOrders: groupedResult.transit,
            CancelledOrders: groupedResult.cancelled,
            CompletedOrders: groupedResult.completed
        };
    }),
    on(UserActions.GetSelectedCartInitiatedAction, (state, { payload }) => {
        return { ...state, IsLoading: true };
    }),
    on(UserActions.GetSelectedCartFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(UserActions.GetSelectedCartSuccessfulAction, (state, { payload }) => {
        return { ...state, IsLoading: false, SelectedCart: payload };
    }),
    on(UserActions.ClearProcessedCartAction, (state) => {
        return {
            ...state,
            IsProcessedCartVerified: undefined,
            ActiveMessage: undefined,
            ProcessedProductCart: undefined
        };
    }),
    on(UserActions.VerifyProcessedCartAction, (state, { payload: { status: { status, message }, processedCart } }) => {
        const currentState = {
            ...state,
            IsProcessedCartVerified: status,
            ActiveActionMessage: {
                Message: message,
                Type: MessageActionType.CART_VERIFIED,
                Label: "Pay"
            },
            ProcessedProductCart:  {
                ...processedCart,
                status
            },
        };
        return currentState;
    }),
    on(UserActions.CartIsNotInTransitAction, (state) => {
        return { ...state, ActiveMessage: "This cart is not in transit" };
    }),
    on(UserActions.RateRiderInitiatedAction, (state, { payload }) => {
        return { ...state, IsLoading: true };
    }),
    on(UserActions.RateRiderFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(UserActions.RateRiderSuccessfulAction, (state, { payload }) => {
        return {
            ...state,
            IsLoading: false,
            ActiveMessage: payload ? "Rating successful" : undefined
        };
    }),
    on(UserActions.GetCartRiderDataInitiatedAction, (state, { payload }) => {
        return { ...state, IsLoading: true, };
    }),
    on(UserActions.GetCartRiderDataFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(UserActions.GetCartRiderDataSuccessfulAction, (state, { payload }) => {
        return { ...state, IsLoading: false, SelectedCartRiderData: { ...payload } };
    }),
    on(UserActions.GetRiderDeliveryRatingInitiatedAction, (state, { payload }) => {
        return { ...state, IsLoading: true };
    }),
    on(UserActions.GetRiderDeliveryRatingFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(UserActions.GetRiderDeliveryRatingSuccessfulAction, (state, { payload }) => {
        return { ...state, IsLoading: false, SelectedCartDeliveryFeedback: payload };
    }),
    on(UserActions.StartLoaderRemotelyAction, (state) => {
        return { ...state, IsLoading: true };
    }),
    on(UserActions.EndLoaderRemotelyAction, (state) => {
        return { ...state, IsLoading: false };
    }),
    on(UserActions.GetNearbyMerchantsInitiatedAction, (state, { payload }) => {
        return { ...state, IsLoading: true, };
    }),
    on(UserActions.GetNearbyMerchantsFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(UserActions.GetNearbyMerchantsSuccessfulAction, (state, { payload: { data, typeId } }) => {
        const merchants: MerchantModel[] = filterMerchantByType(data, typeId);
        return { ...state, IsLoading: false, NearbyMerchantsByMerchantType: [...merchants] };
    }),
);
