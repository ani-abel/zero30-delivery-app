import { Component, OnDestroy, OnInit } from "@angular/core";
import { Store } from "@ngrx/store";
import { SubSink } from "subsink";
import { Observable, of } from "rxjs";
import { AuthResponseType } from "../../../auth/store/model/auth.model";
import { getDataFromLocalStorage } from "../../../../utils/functions/app.functions";
import { LocalStorageKey } from "../../../../utils/types/app.constant";
import { actions as UserActions } from "../../../user/store/actions/user.action";
import { AppState } from "../../../../utils/types/app.model";
import { UserModel } from "../../store/model/user.model";

@Component({
  selector: "app-address",
  templateUrl: "./address.page.html",
  styleUrls: ["./address.page.scss"],
})
export class AddressPage implements OnInit, OnDestroy {
  userData$: Observable<UserModel>;
  subSink: SubSink = new SubSink();
  userDataFromLocalStorage: AuthResponseType;

  constructor(
    private readonly store: Store<AppState>
  ) { }

  async ngOnInit(): Promise<void> {
    // ? get the userId from localStorage
    const userDataFromLocalStorage = await getDataFromLocalStorage<AuthResponseType>(LocalStorageKey.ZERO_30_USER);
    const { userId } = userDataFromLocalStorage;
    this.userDataFromLocalStorage = userDataFromLocalStorage;

    this.subSink.sink =
    this.store
          .select((data) => data.User.UserData)
          .subscribe((data) => {
            if (data?.userId) {
              this.userData$ = of(data);
            }
            else {
              this.store.dispatch(UserActions.GetUserDataInitiatedAction({ payload: { userId } }));
            }
        });
  }

  ngOnDestroy(): void {
    this.subSink.unsubscribe();
  }

}
