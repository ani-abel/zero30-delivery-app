import { Component, OnInit } from "@angular/core";
import { AuthResponseType } from "../../../../app/auth/store/model/auth.model";
import { getDataFromLocalStorage } from "../../../../utils/functions/app.functions";
import { LocalStorageKey } from "../../../../utils/types/app.constant";

@Component({
  selector: "app-payment-method",
  templateUrl: "./payment-method.page.html",
  styleUrls: ["./payment-method.page.scss"],
})
export class PaymentMethodPage implements OnInit {
  userDataFromLocalStorage: AuthResponseType;

  constructor() { }

  async ngOnInit(): Promise<void> {
    // ? Bring in the user data
    const userDataFromLocalStorage = await getDataFromLocalStorage<AuthResponseType>(LocalStorageKey.ZERO_30_USER);
    const { userId } = userDataFromLocalStorage;
    this.userDataFromLocalStorage = userDataFromLocalStorage;
  }

}
