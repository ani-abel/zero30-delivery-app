import { Component, ElementRef, OnInit, ViewChild, OnDestroy } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Store } from "@ngrx/store";
import { Observable, of } from "rxjs";
import { SubSink } from "subsink";
import { actions as UserActions } from "../../../user/store/actions/user.action";
import { UpdateUserPayloadType, UserModel } from "../../../user/store/model/user.model";
import { AppState } from "../../../../utils/types/app.model";
import { AuthResponseType } from "../../../auth/store/model/auth.model";
import { LocalStorageKey } from "../../../../utils/types/app.constant";
import { getDataFromLocalStorage } from "../../../../utils/functions/app.functions";

@Component({
  selector: "app-edit-profile",
  templateUrl: "./edit-profile.page.html",
  styleUrls: ["./edit-profile.page.scss"],
})
export class EditProfilePage implements OnInit, OnDestroy {
  @ViewChild("previewImage", { read: ElementRef }) imagePreview: ElementRef;
  editProfileForm: FormGroup;
  subSink: SubSink = new SubSink();
  singleDatePickerOptions: any = {
    enableMonthSelector: true,
    showMultipleYearsNavigation: true,
    showWeekNumbers: true
  };
  userId: string;
  userData$: Observable<UserModel>;
  userDataFromLocalStorage: AuthResponseType;

  constructor(
    private readonly store: Store<AppState>
  ) { }

  async ngOnInit(): Promise<void> {
    this.initForm();
    // ? Bring in the user data
    const userDataFromLocalStorage = await getDataFromLocalStorage<AuthResponseType>(LocalStorageKey.ZERO_30_USER);
    const { userId } = userDataFromLocalStorage;
    this.userDataFromLocalStorage = userDataFromLocalStorage;

    this.subSink.sink =
    this.store
          .select((data) => data.User.UserData)
          .subscribe((data) => {
            if(data?.userId) {
              this.userData$ = of(data);
              this.initForm(data);
            }
            else {
              this.store.dispatch(UserActions.GetUserDataInitiatedAction({ payload: { userId } }))
            }
        });
  }

  onChange(event: Event): void {
    // Extract the <file> Object that was added
    const file: File = (event.target as HTMLInputElement).files[0];

    // patch the form selector control to the formGroup
    this.editProfileForm.patchValue({
      ProfileImage: file
    });
    // make sure that <postImage> is patched into the form behind the scenes and updates its validity
    this.editProfileForm.get("ProfileImage").updateValueAndValidity();

    // get the ProfileImageUrl
    const fileReader: FileReader = new FileReader(); // open the fileReader handler
    // set the imagePreviewUrl while file reading is happening
    fileReader.onload = () => {
      if(this.imagePreview?.nativeElement) {
        (this.imagePreview.nativeElement as HTMLImageElement).src = (fileReader.result as string);
      }
    };
    // create a dataUrl for the uploaded <postImage>
    fileReader.readAsDataURL(file);
  }

  onSubmit(): void {
    if(this.editProfileForm.invalid) {
      return;
    }
    const { 
      FirstName: firstName,
      LastName: lastName,
      DOB: dob,
      Email: email,
      Password: password,
      PhoneNumber: phoneNo,
      Address: address,
      ProfileImage
    } = this.editProfileForm.value;

    let payload: UpdateUserPayloadType = {
      userId: this.userId,
      firstName,
      lastName,
      email,
      phoneNo,
      dob,
      address
    };

    if(password) {
      payload = {
        ...payload,
        password
      };
    }

    if (ProfileImage) {
      const file: File = (ProfileImage as File);
      // ? Upload File
      this.store.dispatch(UserActions.UploadProfileImageInitiatedAction({ payload: {
          file,
          userId: this.userId
        }
      }));
    }
    this.store.dispatch(UserActions.UpdateUserProfileInitiatedAction({ payload }));
  }

  onChangeSingle(value: Date) {  }

  initForm(userModel?: UserModel): void {
    this.editProfileForm = new FormGroup({
      ProfileImage: new FormControl(null),
      FirstName: new FormControl(userModel?.person.firstName,
        Validators.compose([
          Validators.required
      ])),
      LastName: new FormControl(userModel?.person.lastName,
        Validators.compose([
          Validators.required
      ])),
      Email: new FormControl(userModel?.username,
        Validators.compose([
          Validators.required,
          Validators.email
      ])),
      Password: new FormControl(null),
      DOB: new FormControl(userModel ? new Date(userModel.person.dob) : null,
      Validators.compose([
        Validators.required
      ])),
      PhoneNumber: new FormControl(userModel?.person.phoneNo,
        Validators.compose([
          Validators.required
      ])),
      Address: new FormControl(userModel?.person.address,
        Validators.compose([
          Validators.required
      ])),
    });
  }

  ngOnDestroy(): void {
    this.subSink.unsubscribe();
  }
}
