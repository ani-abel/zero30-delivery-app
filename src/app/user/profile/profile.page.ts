import { Component, OnDestroy, OnInit } from "@angular/core";
import { Store } from "@ngrx/store";
import { Observable } from "rxjs";
import { SubSink } from "subsink";
import { AuthResponseType } from "../../auth/store/model/auth.model";
import { AppState } from "../../../utils/types/app.model";
import { actions as AuthActions } from "../../auth/store/actions/auth.action";
import { actions as UserActions } from "../../user/store/actions/user.action";
import { actions as AppActions } from "../../store/actions/app.action";
import { getDataFromLocalStorage } from "../../../utils/functions/app.functions";
import { LocalStorageKey } from "../../../utils/types/app.constant";
import { UserModel } from "../store/model/user.model";

@Component({
  selector: "app-profile",
  templateUrl: "./profile.page.html",
  styleUrls: ["./profile.page.scss"],
})
export class ProfilePage implements
OnInit,
OnDestroy {
  userData$: Observable<UserModel>;
  subSink: SubSink = new SubSink();
  userDataFromLocalStorage: AuthResponseType;

  constructor(private readonly store: Store<AppState>) { }

  ngOnInit(): void { }

  async ionViewWillEnter(): Promise<void> {
    // ?  get the userId from localStorage
    const userDataFromLocalStorage: AuthResponseType = await getDataFromLocalStorage<AuthResponseType>(LocalStorageKey.ZERO_30_USER);
    this.userDataFromLocalStorage = userDataFromLocalStorage;
    if (userDataFromLocalStorage?.userId) {
      const { userId } = userDataFromLocalStorage;
      if (userId) {
        this.store.dispatch(UserActions.GetUserDataInitiatedAction({ payload: { userId } }));
        this.userData$ = this.store.select((data) => data.User.UserData);
      }
    }
  }

  logout(): void {
    try {
      this.store.dispatch(AppActions.ClearAppStateAction());
      this.store.dispatch(AuthActions.LogoutInitiatedAction());
      // this.store.dispatch(UserActions.ClearUserStateAction());
      // this.store.dispatch(RiderActions.ClearRiderStateAction());
    }
    catch (ex) {
      throw ex;
    }
  }

  ngOnDestroy(): void {
    this.subSink.unsubscribe();
  }

}
