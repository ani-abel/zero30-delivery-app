import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Store } from '@ngrx/store';
import { SubSink } from 'subsink';
import { Observable } from 'rxjs';
import { AppState } from '../../../utils/types/app.model';
import { actions as UserAction } from "../../user/store/actions/user.action";
import { ProductDetailType, ProductSearchResultType } from '../store/model/user.model';
import { ModalController } from '@ionic/angular';
import { AuthResponseType } from 'src/app/auth/store/model/auth.model';
import { getDataFromLocalStorage } from 'src/utils/functions/app.functions';
import { LocalStorageKey } from 'src/utils/types/app.constant';

@Component({
  selector: 'app-view-search-results',
  templateUrl: './view-search-results.page.html',
  styleUrls: ['./view-search-results.page.scss'],
})
export class ViewSearchResultsPage implements OnInit, OnDestroy {
  subSink: SubSink = new SubSink();
  searchTerm: string;
  searchForm: FormGroup;
  searchResults$: Observable<ProductSearchResultType[]>;
  @Output() viewProductDetailEvent: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly store: Store<AppState>,
    private readonly router: Router
  ) { }

  ngOnInit() {
    //? The get search-term from query params
    this.subSink.sink =
    this.activatedRoute.queryParams.subscribe((data) => {
      if(data?.searchTerm) {
        this.searchTerm = data.searchTerm;
        this.store.dispatch(UserAction.SearchForProductsInitiatedAction({ payload: { searchTerm: data.searchTerm } }));
      }
      this.initForm();
    });

    this.searchResults$ = this.store.select((data) => data.User.ProductSearchResults);
  }

  initForm(): void {
    this.searchForm = new FormGroup({
      SearchTerm: new FormControl(this.searchTerm, Validators.compose([
        Validators.required
      ]))
    });
  }

  onSubmit(): void {
    if(this.searchForm.invalid) {
      return;
    }
    const { SearchTerm } =  this.searchForm.value;
    this.store.dispatch(UserAction.SearchForProductsInitiatedAction({ payload: { searchTerm: SearchTerm } }));
    return;
  }

  async openProductDetail(event: any, product: ProductDetailType): Promise<void> {
    if (product?.productModel) {
      this.router.navigateByUrl(`/user/product-detail?product=${JSON.stringify(product)}`);
    }
  }

  async addProductToBookmark(event: any, product: ProductDetailType ): Promise<void> {
    const { userId } = await getDataFromLocalStorage<AuthResponseType>(LocalStorageKey.ZERO_30_USER);
    if (userId) {
      // ? Dispatch action
      this.store.dispatch(UserAction.AddProductToBookmarkInitiatedAction({ payload: { productId: product.productModel.id, userId } }));
    }
  }

  clearSearchTerm(): void {
    this.searchForm.reset();
  }

  ngOnDestroy(): void {
    this.subSink.unsubscribe();
  }
}
