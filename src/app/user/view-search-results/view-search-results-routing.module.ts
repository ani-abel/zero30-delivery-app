import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewSearchResultsPage } from './view-search-results.page';

const routes: Routes = [
  {
    path: '',
    component: ViewSearchResultsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewSearchResultsPageRoutingModule {}
