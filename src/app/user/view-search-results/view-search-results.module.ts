import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { ViewSearchResultsPageRoutingModule } from './view-search-results-routing.module';
import { ViewSearchResultsPage } from './view-search-results.page';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViewSearchResultsPageRoutingModule,
    SharedModule
  ],
  declarations: [ViewSearchResultsPage]
})
export class ViewSearchResultsPageModule {}
