import { Component, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Store } from "@ngrx/store";
import { SubSink } from "subsink";
import { Observable } from "rxjs";
import { AppState } from "../../../utils/types/app.model";
import {
  CheckedOutProduct,
  PaymentMethodType,
  ProductCartStatus,
  ProductOrdersResultType
} from "../store/model/user.model";
import { actions as UserAction } from "../../user/store/actions/user.action";
import { getDataFromLocalStorage } from "../../../utils/functions/app.functions";
import { AuthResponseType } from "../../auth/store/model/auth.model";
import { LocalStorageKey } from "../../../utils/types/app.constant";

@Component({
  selector: "app-make-payment",
  templateUrl: "./make-payment.page.html",
  styleUrls: ["./make-payment.page.scss"],
})
export class MakePaymentPage implements
OnInit,
OnDestroy {
  /**
   * Pass Enums to be used on the template
   * http://localhost:8100/#/user/make-payment/1e9ae2f7-d4c8-46d7-baa9-f08e608a9a9c
   */
  paymentMethodType = PaymentMethodType;
  processedCart$: Observable<CheckedOutProduct>;
  selectedCart$: Observable<ProductOrdersResultType>;
  subSink: SubSink = new SubSink();
  productCartStatus = ProductCartStatus;
  cartId: string;
  email: string;

  constructor(
    private readonly store: Store<AppState>,
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
  ) { }

  async ngOnInit(): Promise<void> {
    this.subSink.sink =
    this.activatedRoute.params.subscribe((data) => {
      this.cartId = data.cartId;
      this.store.dispatch(UserAction.GetSelectedCartInitiatedAction({ payload: { cartId: this.cartId } }));
    });
    // this.reference = `ref-${Math.ceil(Math.random() * 10e13)}`;
    const { username } = await getDataFromLocalStorage<AuthResponseType>(LocalStorageKey.ZERO_30_USER);
    this.email = username;
    this.processedCart$ = this.store.select((data) => data.User.ProcessedProductCart);
    this.subSink.sink =
    this.processedCart$.subscribe((data) => {
      if (!(data?.cartId && data?.total)) {
        this.store.dispatch(UserAction.GetSelectedCartInitiatedAction({ payload: { cartId: this.cartId } }));
        this.processedCart$ = this.store.select((selectedCartData) => selectedCartData.User.SelectedCart);
      }
    });
  }

  paymentInit(): void {
    console.log("Payment initialized");
  }

  paymentDone(ref: any): void {
    if (this.cartId) {
      this.makePayment(PaymentMethodType.PAYSTACK);
      if (ref) {
        const { status, reference } = ref;
        if (status === "success") {
          // ? Wait for 5 seconds before confirming paystack payment
          setTimeout(() => {
            this.store.dispatch(UserAction.ConfirmPaystackPaymentInitiatedAction({ payload: { refrenceNo: reference } }));
            this.navigateToHome();
          }, 5000);
        }
      }
    }
  }

  paymentCancel(): void {
    console.log("payment failed");
  }

  makePayment(paymentOption: PaymentMethodType): void {
    if (this.cartId) {
      this.store.dispatch(UserAction.MakePaymentInitiatedAction({ payload: { cartId: this.cartId, paymentOption } }));
      if (paymentOption !== PaymentMethodType.PAYSTACK) {
        this.navigateToHome();
      }
    }
  }

  private navigateToHome(): void {
    this.subSink.sink =
      this.store
          .select((data) => data.User.ActiveMessage)
          .subscribe((data) => {
            if (data) {
              this.router.navigate(["/user", "discover-items"]);
            }
          });
  }

  ngOnDestroy(): void {
    this.subSink.unsubscribe();
  }
}
