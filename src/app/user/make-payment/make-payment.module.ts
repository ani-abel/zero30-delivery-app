import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { Angular4PaystackModule } from 'angular4-paystack';

import { environment as env } from "../../../environments/environment";

import { MakePaymentPageRoutingModule } from './make-payment-routing.module';

import { MakePaymentPage } from './make-payment.page';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    Angular4PaystackModule.forRoot(env.paystackPublicTestKey),
    MakePaymentPageRoutingModule
  ],
  declarations: [MakePaymentPage]
})
export class MakePaymentPageModule {}
