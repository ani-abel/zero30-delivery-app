import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NearbyPage } from './nearby.page';

const routes: Routes = [
  {
    path: '',
    component: NearbyPage
  },
  {
    path: 'business-detail',
    loadChildren: () => import('./business-detail/business-detail.module').then( m => m.BusinessDetailPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NearbyPageRoutingModule {}
