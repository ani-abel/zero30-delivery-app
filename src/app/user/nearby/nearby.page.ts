import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { SubSink } from 'subsink';
import { Observable } from 'rxjs';
import { actions as UserAction } from "../../user/store/actions/user.action";
import { AppState } from '../../../utils/types/app.model';
import { MerchantModel, MerchantType } from '../store/model/user.model';
import { AuthResponseType } from '../../auth/store/model/auth.model';
import { getDataFromLocalStorage } from '../../../utils/functions/app.functions';
import { LocalStorageKey } from '../../../utils/types/app.constant';

@Component({
  selector: 'app-nearby',
  templateUrl: './nearby.page.html',
  styleUrls: ['./nearby.page.scss'],
})
export class NearbyPage implements OnInit, OnDestroy {
  subSink: SubSink = new SubSink();
  merchantTypeExists: boolean = false;
  activeTabType: MerchantType;
  merchantTypes$: Observable<MerchantType[]>;
  merchantTypes: MerchantType[];
  merchantsOrderedByType$: Observable<MerchantModel[]>;
  userId: string;

  constructor(private readonly store: Store<AppState>) { }

  async ngOnInit(): Promise<void> {
    const userData: AuthResponseType = await getDataFromLocalStorage<AuthResponseType>(LocalStorageKey.ZERO_30_USER);
    if (userData.userId) {
      this.userId = userData.userId;

      this.merchantTypes$ = this.store.select((data) => data.User.MerchantType);
      //? Check if merchant Types exist in cache
      this.subSink.sink =
      this.merchantTypes$.subscribe((data) => {
        if(data?.length > 0) {
          this.merchantTypeExists = true;
        }
      });

      //? If the merchant Types does not exist, call data from API
      if(!this.merchantTypeExists) {
        this.store.dispatch(UserAction.GetMerchantTypeInitiatedAction());
      }
      this.subSink.sink = 
      this.merchantTypes$.subscribe((data) => {
        this.merchantTypes = data;
        this.activeTabType = data.find((dt) => dt.id === 1);
        if(this.activeTabType?.id) {
          this.store.dispatch(UserAction.GetNearbyMerchantsInitiatedAction({ payload: { typeId: this.activeTabType.id, userId: this.userId } }));
        }
      });
      this.merchantsOrderedByType$ = this.store.select((data) => data.User.NearbyMerchantsByMerchantType);
    }
  }

  switchTabs(tab: MerchantType): void {
    this.activeTabType = tab;
    this.store.dispatch(UserAction.GetNearbyMerchantsInitiatedAction({ payload: { typeId: tab.id, userId: this.userId } }));
  }

  addMerchantToBookmark(event: MerchantModel): void {
    if(this.userId) {
      //? Dispatch action
      this.store.dispatch(UserAction.AddMerchantToUserBookmarksInitiatedAction({ payload: { 
        MerchantId: event.id, 
        UserId: this.userId, 
        Merchant: event } 
      }));
    }
  }

  ngOnDestroy(): void {
    this.subSink.unsubscribe();
  }
}
