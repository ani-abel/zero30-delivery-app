import { 
  Component, 
  OnDestroy, 
  OnInit, 
  ElementRef, 
  ViewChild, 
  AfterViewInit 
} from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from "@angular/router";
import { ModalController } from '@ionic/angular';
import { Store } from "@ngrx/store";
import { Observable } from 'rxjs';
import { SubSink } from "subsink";
import {} from 'googlemaps';
import { actions as UserAction } from "../../../user/store/actions/user.action";
import { AppState } from '../../../../utils/types/app.model';
import { 
  MerchantModel, 
  MerchantReviewType, 
  ProductCategoryGroupType, 
  ProductCheckout, 
  ProductDetailType, 
  ProductImage
} from '../../store/model/user.model';
import { AuthResponseType } from '../../../auth/store/model/auth.model';
import { getDataFromLocalStorage } from '../../../../utils/functions/app.functions';
import { LocalStorageKey } from '../../../../utils/types/app.constant';
import { ProductDetailModalComponent } from '../../../shared/product-detail-modal/product-detail-modal.component';
import SwiperCore, { Navigation, Pagination, A11y, Autoplay } from 'swiper/core';
// install Swiper components
SwiperCore.use([Navigation, Pagination, A11y, Autoplay]);

// declare var L : any;

export type MapDetail = { 
  Latitude: number, 
  Longitude: number, 
  Element: ElementRef, 
  Tooltip: string 
};

export enum TabType {
  PRODUCT = "PRODUCT",
  REVIEW = "REVIEW",
  INFORMATION = "INFORMATION",
}

@Component({
  selector: 'app-business-detail',
  templateUrl: './business-detail.page.html',
  styleUrls: ['./business-detail.page.scss'],
})
export class BusinessDetailPage implements 
OnInit, 
OnDestroy, 
AfterViewInit {
  @ViewChild("mapCanvas", { read: ElementRef }) mapCanvas: ElementRef;
  selectedMerchantDataSync: MerchantModel;
  ratingForm: FormGroup;
  merchantId: string;
  subSink: SubSink = new SubSink();
  selectedMerchant$: Observable<MerchantModel>;
  selectedMerchantReviews$: Observable<MerchantReviewType[]>;
  selectedMerchantHotSaleProducts$: Observable<ProductDetailType[]>;
  productCategoryGroup$: Observable<ProductCategoryGroupType[]>;
  activeTab: TabType = TabType.PRODUCT;
  openCaptionForm: boolean = false;
  noOfStarsSelected: number = 0;
  openCartModal: boolean = false;
  userId: string;
  productAddedToCart: ProductCheckout;
  productAddedToCartImages: ProductImage[];

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly store: Store<AppState>,
    public readonly modalController: ModalController
  ) { }

  async ngOnInit() {
    this.noOfStarsSelected = 0;
    this.initForm();

    //? Get and set userId from locastorage
    const { userId } = await getDataFromLocalStorage<AuthResponseType>(LocalStorageKey.ZERO_30_USER);
    this.userId = userId;

    this.subSink.sink =
    this.activatedRoute.params.subscribe((data) => {
      this.merchantId = data.merchantId;

      if(!data?.merchantId) {
        //? Navigate away if merchantId is not found
        this.router.navigate(["/user", "nearby"]);
      }
    });
    
    this.store.dispatch(UserAction.GetMerchantDetailInitiatedAction({ payload: { typeId: this.merchantId } }));
    this.selectedMerchant$ = this.store.select((data) => data.User.SelectedMerchant);

    this.store.dispatch(UserAction.GetMerchantReviewsInitiatedAction({ payload: { merchantId: this.merchantId } }));
    this.selectedMerchantReviews$ = this.store.select((data) => data.User.SelectedMerchantReviews);

    //? Get hot sale items
    this.store.dispatch(UserAction.GetHotSaleProductForMerchantInitiatedAction({ payload: { merchantId: this.merchantId } }));
    this.selectedMerchantHotSaleProducts$ = this.store.select((data) => data.User.SelectedMerchantHotSaleProducts);

    //? Get merchant's products grouped by categories
    this.store.dispatch(UserAction.GetMechantProductsGroupedByCategoryInitiatedAction({ payload: { merchantId: this.merchantId } }));
    this.productCategoryGroup$ = this.store.select((data) => data.User.ProductCategoryGroups);
    
    this.subSink.sink =
    this.selectedMerchantReviews$.subscribe((data: MerchantReviewType[]) => {
      if(data?.length > 0) {
        //? Find user review if any
        data.find((review: MerchantReviewType) => {
          const match: boolean = review.userId === this.userId;
          if(match) {
            this.noOfStarsSelected = review.rating;
          }
          return match;
        });
      }
    });

    setTimeout(() => {
      this.subSink.sink =
      this.selectedMerchant$.subscribe(data => {
        if(data?.id) {
          this.selectedMerchantDataSync = data;
        }
        else {
          //? Navigate away if merchant is not found
          this.router.navigate(["/user", "nearby"]);
        }
      })
    }, 2000);
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      if(this.selectedMerchantDataSync?.longitude) {
        const { longitude, latitude, address } = this.selectedMerchantDataSync;
        this.loadMap({
          Latitude: latitude, 
          Longitude: longitude, 
          Element: this.mapCanvas,
          Tooltip: address
        });
      }
    }, 3000);
  }

  // async openProductDetail(event: any, product: ProductDetailType): Promise<void> {
  //   if(product?.productModel) {
  //     const modal = await this.modalController.create({
  //       component: ProductDetailModalComponent,
  //       componentProps: {
  //         "product": product
  //       }
  //     });
  //     return await modal.present();
  //   }
  // }

  async openProductDetail(event: any, product: ProductDetailType): Promise<void> {
    if (product?.productModel) {
      this.router.navigateByUrl(`/user/product-detail?product=${JSON.stringify(product)}`);
    }
  }

  initForm(): void {
    this.ratingForm = new FormGroup({
      Caption: new FormControl(null)
    });
  }

  async addMerchantToBookmark(): Promise<void> {
    this.subSink.sink =
    this.selectedMerchant$.subscribe(async (merchant) => {
      //? Get the user Id
      if(this.userId) {
        //? Dispatch action
        this.store.dispatch(UserAction.AddMerchantToUserBookmarksInitiatedAction({ payload: { 
          MerchantId: merchant.id, 
          UserId: this.userId, 
          Merchant: merchant } 
        }));
      }
    });
  }

  switchTab(tab: TabType): void {
    this.activeTab = tab;
  }

  addStarRating(index: number): void {
    this.openCaptionForm = true;
    this.noOfStarsSelected = index;
  }

  addProductToBoomkark(event: string): void {
    if(this.userId) {
      this.store.dispatch(UserAction.AddProductToBookmarkInitiatedAction({ payload: { productId: event, userId: this.userId } }));
    }
  }

  async onSubmit(): Promise<void> {
    if(this.ratingForm.invalid) {
      return;
    }
    this.openCaptionForm = false;
    if(this.userId) {
      const { Caption } = this.ratingForm.value;
        this.store.dispatch(UserAction.AddMerchantRatingInitiatedAction({ payload: { 
          UserId: this.userId, 
          Rate: this.noOfStarsSelected, 
          MerchantId: this.merchantId,
          Review: Caption || undefined
        } 
      }));

      this.subSink.sink =
      this.store
          .select((data) => data.User.ActiveMessage)
          .subscribe((data) => {
            if(data) {
              this.ratingForm.reset();
              this.store.dispatch(UserAction.GetMerchantReviewsInitiatedAction({ payload: { merchantId: this.merchantId } }));
            }
          });
    }
  }

  loadMap(payload: MapDetail): void {
    const { Longitude, Latitude, Element, Tooltip } = payload;
    const myLatLng = { lat: Latitude, lng: Longitude };
    const map = new google.maps.Map(Element.nativeElement, {
        zoom: 15,
        center: myLatLng,
        // styles: [
        //   { elementType: "geometry", stylers: [{ color: "#242f3e" }] },
        //   { elementType: "labels.text.stroke", stylers: [{ color: "#242f3e" }] },
        //   { elementType: "labels.text.fill", stylers: [{ color: "#746855" }] },
        //   {
        //     featureType: "administrative.locality",
        //     elementType: "labels.text.fill",
        //     stylers: [{ color: "#d59563" }],
        //   },
        //   {
        //     featureType: "poi",
        //     elementType: "labels.text.fill",
        //     stylers: [{ color: "#d59563" }],
        //   },
        //   {
        //     featureType: "poi.park",
        //     elementType: "geometry",
        //     stylers: [{ color: "#263c3f" }],
        //   },
        //   {
        //     featureType: "poi.park",
        //     elementType: "labels.text.fill",
        //     stylers: [{ color: "#6b9a76" }],
        //   },
        //   {
        //     featureType: "road",
        //     elementType: "geometry",
        //     stylers: [{ color: "#38414e" }],
        //   },
        //   {
        //     featureType: "road",
        //     elementType: "geometry.stroke",
        //     stylers: [{ color: "#212a37" }],
        //   },
        //   {
        //     featureType: "road",
        //     elementType: "labels.text.fill",
        //     stylers: [{ color: "#9ca5b3" }],
        //   },
        //   {
        //     featureType: "road.highway",
        //     elementType: "geometry",
        //     stylers: [{ color: "#746855" }],
        //   },
        //   {
        //     featureType: "road.highway",
        //     elementType: "geometry.stroke",
        //     stylers: [{ color: "#1f2835" }],
        //   },
        //   {
        //     featureType: "road.highway",
        //     elementType: "labels.text.fill",
        //     stylers: [{ color: "#f3d19c" }],
        //   },
        //   {
        //     featureType: "transit",
        //     elementType: "geometry",
        //     stylers: [{ color: "#2f3948" }],
        //   },
        //   {
        //     featureType: "transit.station",
        //     elementType: "labels.text.fill",
        //     stylers: [{ color: "#d59563" }],
        //   },
        //   {
        //     featureType: "water",
        //     elementType: "geometry",
        //     stylers: [{ color: "#17263c" }],
        //   },
        //   {
        //     featureType: "water",
        //     elementType: "labels.text.fill",
        //     stylers: [{ color: "#515c6d" }],
        //   },
        //   {
        //     featureType: "water",
        //     elementType: "labels.text.stroke",
        //     stylers: [{ color: "#17263c" }],
        //   },
        // ],
    });
    new google.maps.Marker({
        position: myLatLng,
        map,
        title: Tooltip,
    });
  }

  // loadMap(payload: MapDetail): void {
  //   const { Longitude, Latitude, Element, Tooltip } = payload;
  //   L.mapquest.key = env.mapQuestAPIKey;

  //   const map = L.mapquest.map(Element.nativeElement, {
  //     center: [Longitude, Latitude],
  //     layers: L.mapquest.tileLayer("map"),
  //     zoom: 15
  //   });

  //   L.marker([Longitude, Latitude], {
  //     icon: L.mapquest.icons.marker(),
  //     draggable: false
  //   }).bindPopup(Tooltip).addTo(map);

  //   L.circle([Longitude, Latitude], { radius: 20000 }).addTo(map);
  // }

  addProductToCart({ payload, productImages }): void {
    /**
     * payload carries details about the product
     */
    if(payload) {
      this.openCartModal = true;
      const { cost, productId, productName, quantity } = payload;
      this.productAddedToCart = {
        cost,
        productId,
        productName,
        productImage: productImages[0]?.path || undefined,
        quantity,
      }
      this.productAddedToCartImages = productImages;
    }
  }

  closeCartModal(): void {
    this.openCartModal = false;
  }

  ngOnDestroy(): void {
    this.subSink.unsubscribe();
  }
}