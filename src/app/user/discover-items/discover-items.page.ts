import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { SubSink } from 'subsink';
import { AppState } from '../../../utils/types/app.model';
import { 
  MerchantModel, 
  ProductDetailType, 
  ProductDiscoveryType,
  UserModel
} from '../store/model/user.model';
import { actions as UserAction } from "../../user/store/actions/user.action";
import { AuthResponseType } from '../../auth/store/model/auth.model';
import { getDataFromLocalStorage } from '../../../utils/functions/app.functions';
import { LocalStorageKey } from '../../../utils/types/app.constant';
import { BookmarkFavouriteMerchantType } from '../../shared/favourite-merchant-hightlight-widget/favourite-merchant-hightlight-widget.component';
import SwiperCore, { Navigation, Pagination, A11y, SwiperOptions, Autoplay } from 'swiper/core';
// install Swiper components
SwiperCore.use([Navigation, Pagination, A11y, Autoplay]);

@Component({
  selector: 'app-discover-items',
  templateUrl: './discover-items.page.html',
  styleUrls: ['./discover-items.page.scss'],
})
export class DiscoverItemsPage implements OnInit, OnDestroy {
  productsByMerchant$: Observable<ProductDiscoveryType>;
  searchForm: FormGroup;
  favouriteMerchants$: Observable<MerchantModel[]>;
  productsOnHotSale$: Observable<ProductDetailType[]>;
  productOrderHistory$: Observable<ProductDetailType[]>;
  userData$: Observable<UserModel>;
  subSink: SubSink = new SubSink();
  userId: string;
  config: SwiperOptions = {
    speed: 500,
    spaceBetween: 30,
    slidesPerView: 2,
    loop: true,
      autoplay: {
        delay: 2500,
        disableOnInteraction: true,
      },
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
  }

  constructor(
    private readonly store: Store<AppState>,
    private readonly router: Router,
  ) { }

  async ngOnInit(): Promise<void> {
    this.initForm();
    const authData: AuthResponseType = await getDataFromLocalStorage<AuthResponseType>(LocalStorageKey.ZERO_30_USER);
    if(authData?.userId) {
      this.userId = authData.userId;
      setTimeout(() => {
        this.store.dispatch(UserAction.GetProductsByMerchantInitiatedAction({ payload: { userId: authData.userId } }));
        this.productsByMerchant$ = this.store.select((data) => data.User.ProductsByMerchants);
      }, 500);

      //? Get the list of favourites merchants
      this.store.dispatch(UserAction.GetUserFavouritesInitiatedAction({ payload: { userId: authData?.userId } }));
      this.favouriteMerchants$ = this.store.select((data) => data.User.FavouriteMerchants);

      //? Get Order history for the user
      this.productOrderHistory$ = this.store.select((data) => data.User.ProductOrderHistory);
      this.store.dispatch(UserAction.GetUserOrderHistoryInitiatedAction({ payload: { userId: authData.userId } }));

      //? Get user's personal data
      this.userData$ = this.store.select((data) => data.User.UserData);
      this.subSink.sink = 
      this.userData$.subscribe((data) => {
        if(!data?.userId) {
          this.store.dispatch(UserAction.GetUserDataInitiatedAction({ payload: { userId: authData.userId } }));
        }
      });
    }
    this.store.dispatch(UserAction.GetProductsOnHotSaleInitiatedAction());
    this.productsOnHotSale$ = this.store.select((data) => data.User.ProductsOnHotSale);
  }

  initForm(): void {
    this.searchForm = new FormGroup({
      SearchTerm: new FormControl(null, Validators.compose([
        Validators.required
      ]))
    });
  }

  onSwiper(swiper): void {
    console.log(swiper)
  }

  onSlideChange(): void {
    console.log('slide change')
  }

  addResturantToBookmark(
    event: BookmarkFavouriteMerchantType,
    product: ProductDetailType
  ): void {
    if (this.userId) {
      // ? Dispatch action
      this.store.dispatch(UserAction.AddMerchantToUserBookmarksInitiatedAction({ 
        payload: {
          MerchantId: event.MerchantId,
          UserId: this.userId, 
          Merchant: product["merchant"]
        } }));
    }
  }
  
  onSubmit(): void {
    if(this.searchForm.invalid) {
      return;
    }
    const { SearchTerm } =  this.searchForm.value;
    this.router.navigate(["/user", "view-search-results"], { queryParams: { searchTerm: SearchTerm }});
  }

  clearSearchTerm(): void {
    this.searchForm.reset();
  }

  async openProductDetail(
    event: any,
    product: ProductDetailType
  ): Promise<void> {
    if (product?.productModel) {
      // this.router.navigate(['/user', 'product-detail', { queryParams: { product: JSON.stringify(product) } }]);
      this.router.navigateByUrl(`/user/product-detail?product=${JSON.stringify(product)}`);
    }
  }

  ngOnDestroy(): void {
    this.subSink.unsubscribe();
  }

}
