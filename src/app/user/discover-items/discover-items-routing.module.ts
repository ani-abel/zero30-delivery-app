import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DiscoverItemsPage } from './discover-items.page';

const routes: Routes = [
  {
    path: '',
    component: DiscoverItemsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DiscoverItemsPageRoutingModule {}
