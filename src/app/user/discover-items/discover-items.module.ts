import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DiscoverItemsPageRoutingModule } from './discover-items-routing.module';

import { DiscoverItemsPage } from './discover-items.page';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DiscoverItemsPageRoutingModule,
    SharedModule
  ],
  declarations: [DiscoverItemsPage]
})
export class DiscoverItemsPageModule {}
