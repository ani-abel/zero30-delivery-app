import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MerchantProductsPageRoutingModule } from './merchant-products-routing.module';

import { MerchantProductsPage } from './merchant-products.page';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    MerchantProductsPageRoutingModule
  ],
  declarations: [MerchantProductsPage]
})
export class MerchantProductsPageModule {}
