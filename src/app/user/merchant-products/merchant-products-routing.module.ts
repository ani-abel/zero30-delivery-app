import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MerchantProductsPage } from './merchant-products.page';

const routes: Routes = [
  {
    path: '',
    component: MerchantProductsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MerchantProductsPageRoutingModule {}
