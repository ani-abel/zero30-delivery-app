import { 
  Component, 
  OnDestroy, 
  OnInit
} from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { ModalController } from '@ionic/angular';
import { Store } from "@ngrx/store";
import { Observable } from 'rxjs';
import { SubSink } from "subsink";
import { actions as UserAction } from "../../user/store/actions/user.action";
import { AppState } from '../../../utils/types/app.model';
import { 
  MerchantModel, 
  ProductCategoryGroupType, 
  ProductDetailType 
} from '../store/model/user.model';
import { AuthResponseType } from '../../auth/store/model/auth.model';
import { getDataFromLocalStorage } from '../../../utils/functions/app.functions';
import { LocalStorageKey } from '../../../utils/types/app.constant';
import { ProductDetailModalComponent } from '../../shared/product-detail-modal/product-detail-modal.component';

@Component({
  selector: 'app-merchant-products',
  templateUrl: './merchant-products.page.html',
  styleUrls: ['./merchant-products.page.scss'],
})
export class MerchantProductsPage implements 
OnInit, 
OnDestroy {
  selectedMerchantDataSync: MerchantModel;
  merchantId: string;
  subSink: SubSink = new SubSink();
  selectedMerchant$: Observable<MerchantModel>;
  productCategoryGroup$: Observable<ProductCategoryGroupType[]>;
  userId: string;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly store: Store<AppState>,
    public readonly modalController: ModalController
  ) { }

  async ngOnInit() {
    //? Get and set userId from locastorage
    const { userId } = await getDataFromLocalStorage<AuthResponseType>(LocalStorageKey.ZERO_30_USER);
    this.userId = userId;

    this.subSink.sink =
    this.activatedRoute.params.subscribe((data) => {
      this.merchantId = data.merchantId;

      if(!data?.merchantId) {
        //? Navigate away if merchantId is not found
        this.router.navigate(["/user", "nearby"]);
      }
    });
    
    this.store.dispatch(UserAction.GetMerchantDetailInitiatedAction({ payload: { typeId: this.merchantId } }));
    this.selectedMerchant$ = this.store.select((data) => data.User.SelectedMerchant);

    //? Get merchant's products grouped by categories
    this.store.dispatch(UserAction.GetMechantProductsGroupedByCategoryInitiatedAction({ payload: { merchantId: this.merchantId } }));
    this.productCategoryGroup$ = this.store.select((data) => data.User.ProductCategoryGroups);

    setTimeout(() => {
      this.subSink.sink =
      this.selectedMerchant$.subscribe(data => {
        if(data?.id) {
          this.selectedMerchantDataSync = data;
        }
        else {
          //? Navigate away if merchant is not found
          this.router.navigate(["/user", "nearby"]);
        }
      })
    }, 2000);
  }

  // async openProductDetail(event: any, product: ProductDetailType): Promise<void> {
  //   if(product?.productModel) {
  //     const modal = await this.modalController.create({
  //       component: ProductDetailModalComponent,
  //       componentProps: {
  //         "product": product
  //       }
  //     });
  //     return await modal.present();
  //   }
  // }

  async openProductDetail(event: any, product: ProductDetailType): Promise<void> {
    if (product?.productModel) {
      this.router.navigateByUrl(`/user/product-detail?product=${JSON.stringify(product)}`);
    }
  }

  async addMerchantToBookmark(): Promise<void> {
    this.subSink.sink =
    this.selectedMerchant$.subscribe(async (merchant) => {
      //? Get the user Id
      if(this.userId) {
        //? Dispatch action
        this.store.dispatch(UserAction.AddMerchantToUserBookmarksInitiatedAction({ payload: { 
          MerchantId: merchant.id, 
          UserId: this.userId, 
          Merchant: merchant } 
        }));
      }
    });
  }

  addProductToBoomkark(event: string): void {
    if(this.userId) {
      this.store.dispatch(UserAction.AddProductToBookmarkInitiatedAction({ payload: { 
          productId: event, 
          userId: this.userId 
        } 
      }));
    }
  }

  ngOnDestroy(): void {
    this.subSink.unsubscribe();
  }
}
