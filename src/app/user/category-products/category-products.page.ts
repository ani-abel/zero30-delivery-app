import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AuthResponseType } from '../../auth/store/model/auth.model';
import { SubSink } from 'subsink';
import { getDataFromLocalStorage } from '../../../utils/functions/app.functions';
import { LocalStorageKey } from '../../../utils/types/app.constant';
import { AppState } from '../../../utils/types/app.model';
import { actions as UserAction } from "../../user/store/actions/user.action";
import { ProductDetailType, ProductSearchResultType } from '../store/model/user.model';

@Component({
  selector: 'app-category-products',
  templateUrl: './category-products.page.html',
  styleUrls: ['./category-products.page.scss'],
})
export class CategoryProductsPage implements OnInit, OnDestroy {
  categoryId: string;
  categoryName: string;
  productsGroupedByCategory$: Observable<ProductSearchResultType[]>;
  subSink: SubSink = new SubSink();

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly store: Store<AppState>,
    private readonly router: Router,
  ) { }

  ngOnInit() {
    //? Get the catrory Id
    this.subSink.sink = 
    this.activatedRoute.params.subscribe((data) => {
      this.categoryId = data.categoryId;
      this.categoryName = data.categoryName;
    });

    //? Get all products related to this category
    this.store.dispatch(UserAction.GetProductsGroupedByCategoryInitiatedAction({ payload: { categoryId: this.categoryId } }));
    this.productsGroupedByCategory$ = this.store.select((data) => data.User.ProductsGroupedByCategory);
  }

  async openProductDetail(event: any, product: ProductDetailType): Promise<void> {
    if (product?.productModel) {
      this.router.navigateByUrl(`/user/product-detail?product=${JSON.stringify(product)}`);
    }
  }

  async addProductToBookmark(event: any, product: ProductDetailType ): Promise<void> {
    const { userId } = await getDataFromLocalStorage<AuthResponseType>(LocalStorageKey.ZERO_30_USER);
    if (userId) {
      // ? Dispatch action
      this.store.dispatch(UserAction.AddProductToBookmarkInitiatedAction({ payload: { productId: product.productModel.id, userId } }));
    }
  }

  ngOnDestroy(): void {
    this.subSink.unsubscribe();
  }

}
