import { Component, OnInit, OnDestroy } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Observable, of } from "rxjs";
import { Store } from "@ngrx/store";
import { SubSink } from "subsink";
import SwiperCore, { Navigation, Pagination, A11y, Autoplay } from "swiper/core";
import { ProductCheckout, ProductDetailType, ProductImage } from "../store/model/user.model";
import { AuthResponseType } from "../../auth/store/model/auth.model";
import { getDataFromLocalStorage } from "../../../utils/functions/app.functions";
import { LocalStorageKey } from "../../../utils/types/app.constant";
import { actions as UserAction } from "../store/actions/user.action";
import { AppState } from "../../../utils/types/app.model";
// install Swiper components
SwiperCore.use([Navigation, Pagination, A11y, Autoplay]);

declare function unBundleSVG();

@Component({
  selector: "app-product-detail",
  templateUrl: "./product-detail.page.html",
  styleUrls: ["./product-detail.page.scss"],
})
export class ProductDetailPage implements
OnInit,
OnDestroy {
  product$: Observable<ProductDetailType>;
  subSink: SubSink = new SubSink();
  openCartModal = false;
  productAddedToCart: ProductCheckout;
  productAddedToCartImages: ProductImage[];
  userId: string;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router,
    private readonly store: Store<AppState>,
  ) { }

  ngOnInit(): void {
    unBundleSVG();
    this.subSink.sink =
    this.activatedRoute.queryParams.subscribe((data) => {
      if (data?.product) {
        this.product$ = of(JSON.parse(data.product));
      } else {
        this.router.navigate(["/user"]);
      }
    });
  }

  async addProductToBookmark(productId: string): Promise<void> {
    const { userId } = await getDataFromLocalStorage<AuthResponseType>(LocalStorageKey.ZERO_30_USER);
    if (userId) {
      // ? Dispatch action
      this.store.dispatch(UserAction.AddProductToBookmarkInitiatedAction({ payload: { userId, productId } }));
    }
  }

  async addProductToCart({ productImageModels, productModel, productCostModel }): Promise<void> {
    const { userId } = await getDataFromLocalStorage<AuthResponseType>(LocalStorageKey.ZERO_30_USER);
    if (userId) {
      this.userId = userId;
      const { id, name } = productModel;
      this.openCartModal = true;
      this.productAddedToCart = {
        cost: productCostModel.unitPrice,
        quantity: 1,
        productImage: productImageModels[0]?.path || undefined,
        productId: id,
        productName: name
      };
      this.productAddedToCartImages = productImageModels;
    }
  }

  closeCartModal(): void {
    this.openCartModal = false;
  }

  ngOnDestroy(): void {
    this.subSink.unsubscribe();
  }
}
