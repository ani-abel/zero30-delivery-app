import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { CartLocationResolver } from "../resolvers/cart-location.resolver";

const routes: Routes = [
{
    path: "",
    children: [
        { path: "", pathMatch: "full", redirectTo: "discover-items" },
        {
            path: "discover-items",
            loadChildren: () => import("./discover-items/discover-items.module").then(m => m.DiscoverItemsPageModule)
        },
        {
            path: "favourites",
            loadChildren: () => import("./favourites/favourites.module").then( m => m.FavouritesPageModule),
        },
        {
            path: "view-search-results",
            loadChildren: () => import("./view-search-results/view-search-results.module").then(m => m.ViewSearchResultsPageModule)
        },
        {
            path: "merchant-products/:merchantId",
            loadChildren: () => import("./merchant-products/merchant-products.module").then( m => m.MerchantProductsPageModule)
        },
        {
            path: "category-products/:categoryId/:categoryName",
            loadChildren: () => import("./category-products/category-products.module").then( m => m.CategoryProductsPageModule)
        },
        {
            path: "category-list",
            loadChildren: () => import("./category-list/category-list.module").then( m => m.CategoryListPageModule)
        },
        {
            path: "hot-sales-list",
            loadChildren: () => import("./hot-sales-list/hot-sales-list.module").then( m => m.HotSalesListPageModule)
        },
        {
            path: "product-cart",
            loadChildren: () => import("./product-cart/product-cart.module").then( m => m.ProductCartPageModule)
        },
        {
            path: "make-payment/:cartId",
            loadChildren: () => import("./make-payment/make-payment.module").then( m => m.MakePaymentPageModule)
        },
        {
            path: "product-detail",
            loadChildren: () => import("./product-detail/product-detail.module").then( m => m.ProductDetailPageModule)
        },
        {
            path: "nearby",
            children: [
                {
                    path: "",
                    pathMatch: "full",
                    loadChildren: () => import("./nearby/nearby.module").then( m => m.NearbyPageModule)
                    },
                    {
                    path: "business-detail/:merchantId",
                    loadChildren: () => import("./nearby/business-detail/business-detail.module").then( m => m.BusinessDetailPageModule)
                    }
                ]
            },
            {
                path: "orders",
                children: [
                    {
                    path: "",
                    pathMatch: "full",
                    loadChildren: () => import("./orders/orders.module").then( m => m.OrdersPageModule)
                    },
                    {
                    path: "track-order",
                    resolve: {
                        cartLocation: CartLocationResolver
                    },
                    loadChildren: () => import("./orders/track-order/track-order.module").then(m => m.TrackOrderPageModule)
                    },
                    {
                    path: "order-info",
                    loadChildren: () => import("./orders/order-info/order-info.module").then(m => m.OrderInfoPageModule)
                    }
                ]
            }
        ]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UserRoutingModule { }
