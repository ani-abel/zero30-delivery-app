import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { actions as UserAction } from "../../user/store/actions/user.action";
import { AuthResponseType } from '../../auth/store/model/auth.model';
import { getDataFromLocalStorage } from '../../../utils/functions/app.functions';
import { LocalStorageKey } from '../../../utils/types/app.constant';
import { AppState } from '../../../utils/types/app.model';
import { ProductDetailType, ProductSearchResultType } from '../store/model/user.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-hot-sales-list',
  templateUrl: './hot-sales-list.page.html',
  styleUrls: ['./hot-sales-list.page.scss'],
})
export class HotSalesListPage implements OnInit {
  productsOnHotSale$: Observable<ProductSearchResultType[]>;

  constructor(
    private readonly store: Store<AppState>,
    private readonly router: Router,
    private readonly modalController: ModalController
  ) { }

  ngOnInit() {
    this.store.dispatch(UserAction.GetProductsOnHotSaleInitiatedAction());
    this.productsOnHotSale$ = this.store.select((data) => data.User.ProductsOnHotSale);
  }

  async openProductDetail(event: any, product: ProductDetailType): Promise<void> {
    if (product?.productModel) {
      this.router.navigateByUrl(`/user/product-detail?product=${JSON.stringify(product)}`);
    }
  }

  async addProductToBookmark(event: any, product: ProductDetailType ): Promise<void> {
    const { userId } = await getDataFromLocalStorage<AuthResponseType>(LocalStorageKey.ZERO_30_USER);
    if (userId) {
      // ? Dispatch action
      this.store.dispatch(UserAction.AddProductToBookmarkInitiatedAction({ payload: { productId: product.productModel.id, userId } }));
    }
  }

}
