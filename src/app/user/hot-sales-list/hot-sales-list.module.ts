import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HotSalesListPageRoutingModule } from './hot-sales-list-routing.module';

import { HotSalesListPage } from './hot-sales-list.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    HotSalesListPageRoutingModule
  ],
  declarations: [HotSalesListPage]
})
export class HotSalesListPageModule {}
