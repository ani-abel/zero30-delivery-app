import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HotSalesListPage } from './hot-sales-list.page';

const routes: Routes = [
  {
    path: '',
    component: HotSalesListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HotSalesListPageRoutingModule {}
