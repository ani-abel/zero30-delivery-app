import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AuthResponseType } from 'src/app/auth/store/model/auth.model';
import { getDataFromLocalStorage } from 'src/utils/functions/app.functions';
import { LocalStorageKey } from 'src/utils/types/app.constant';
import { AppState } from '../../../utils/types/app.model';
import { actions as UserAction } from "../../user/store/actions/user.action";
import { ProductDiscoveryType } from '../store/model/user.model';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.page.html',
  styleUrls: ['./category-list.page.scss'],
})
export class CategoryListPage implements OnInit {
  productsByMerchant$: Observable<ProductDiscoveryType>;

  constructor(
    private readonly store: Store<AppState>,
  ) { }

  async ngOnInit(): Promise<void> {
    const authData: AuthResponseType = await getDataFromLocalStorage<AuthResponseType>(LocalStorageKey.ZERO_30_USER);
    if (authData?.userId) {
      this.store.dispatch(UserAction.GetProductsByMerchantInitiatedAction({ payload: { userId: authData.userId } }));
      this.productsByMerchant$ = this.store.select((data) => data.User.ProductsByMerchants);
    }
  }

}
