
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from "@ngrx/store";
import { Observable } from "rxjs";
import { SubSink } from 'subsink';
import { actions as UserAction } from "../../user/store/actions/user.action";
import { AuthResponseType } from '../../auth/store/model/auth.model';
import { getDataFromLocalStorage } from '../../../utils/functions/app.functions';
import { LocalStorageKey } from '../../../utils/types/app.constant';
import { AppState } from '../../../utils/types/app.model';
import { MerchantModel, ProductDetailType } from '../store/model/user.model';
import { ProductDetailModalComponent } from '../../shared/product-detail-modal/product-detail-modal.component';
import { Router } from '@angular/router';

export enum TabType {
  PRODUCT = "PRODUCT",
  MERCHANT = "MERCHANT"
}

@Component({
  selector: 'app-favourites',
  templateUrl: './favourites.page.html',
  styleUrls: ['./favourites.page.scss'],
})
export class FavouritesPage implements OnInit, OnDestroy {
  activeTabType: TabType = TabType.PRODUCT;
  favouriteMerchants$: Observable<MerchantModel[]>;
  favouriteProducts$: Observable<ProductDetailType[]>;
  subSink: SubSink = new SubSink();
  isInAppStore: boolean;

  constructor(
    private readonly store: Store<AppState>,
    private readonly router: Router
  ) { }

  async ngOnInit() {
    // ? Check for favourites
    this.favouriteMerchants$ = this.store.select((data) => data.User.FavouriteMerchants);
    this.favouriteProducts$ = this.store.select((data) => data.User.FavouriteProducts);
    this.subSink.sink = 
    this.favouriteMerchants$.subscribe(async (data) => {
      if(data?.length < 1) {
        this.isInAppStore = true;
      }
    });

    if (this.isInAppStore) {
      const { userId } = await getDataFromLocalStorage<AuthResponseType>(LocalStorageKey.ZERO_30_USER);
      if (userId) {
        this.store.dispatch(UserAction.GetUserFavouritesInitiatedAction({ payload: { userId } }));
      }
    }
  }

  async openProductDetail(event: any, product: ProductDetailType): Promise<void> {
    if (product?.productModel) {
      this.router.navigateByUrl(`/user/product-detail?product=${JSON.stringify(product)}`);
    }
  }

  switchTabs(tab: TabType): void {
    this.activeTabType = tab;
  }

  async addMerchantToBookmark(event: MerchantModel): Promise<void> {
      //? Get the user Id
    const { userId } = await getDataFromLocalStorage<AuthResponseType>(LocalStorageKey.ZERO_30_USER);
    if(userId) {
      //? Dispatch action
      this.store.dispatch(UserAction.AddMerchantToUserBookmarksInitiatedAction({ payload: { MerchantId: event.id, UserId: userId, Merchant: event } }));
    }
  }

  ngOnDestroy(): void {
    this.subSink.unsubscribe();
  }
}