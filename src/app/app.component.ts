import {
  Component,
  OnDestroy,
  OnInit,
  NgZone
} from "@angular/core";
import { Platform } from "@ionic/angular";
import { Plugins } from "@capacitor/core";
import { Store } from "@ngrx/store";
import { HttpErrorResponse } from "@angular/common/http";
import { Observable, timer } from "rxjs";
import { SubSink } from "subsink";
import { actions as AuthActions } from "./auth/store/actions/auth.action";
import { actions as UserActions } from "./user/store/actions/user.action";
import { actions as RiderActions } from "./rider/store/actions/rider.action";
import { AppState } from "../utils/types/app.model";
import { MessageAction } from "./user/store/model/user.model";
import { SignalrWebsocketService } from "./services/signalr-websocket.service";
import {
  LocationAbbrType,
  RiderCartType
} from "./rider/store/model/rider.model";
import { FcmService } from "./services/fcm.service";
import { LocationService } from "./services/location.service";
import { NavigationService } from "./services/navigation.service";
const { SplashScreen } = Plugins;

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["app.component.scss"]
})
export class AppComponent implements
OnInit,
OnDestroy {
  message$: Observable<string>;
  userMessage$: Observable<string>;
  error$: Observable<Error | HttpErrorResponse>;
  riderError$: Observable<Error | HttpErrorResponse>;
  userError$: Observable<Error | HttpErrorResponse>;
  riderMessage$: Observable<string>;
  isLoading$: Observable<boolean>;
  isUserLoading$: Observable<boolean>;
  isRiderLoading$: Observable<boolean>;
  cartItemsCount$: Observable<number>;
  activeActionMessage$: Observable<MessageAction>;
  riderActiveActionMessage$: Observable<MessageAction>;
  subSink: SubSink = new SubSink();

  constructor(
    private readonly platform: Platform,
    private readonly store: Store<AppState>,
    private readonly locationSrv: LocationService,
    private readonly signalRSrv: SignalrWebsocketService,
    // tslint:disable-next-line: variable-name
    private readonly _ngZone: NgZone,
    private readonly navigationSrv: NavigationService,
    private readonly fcmSrv: FcmService,
  ) {
    this.initializeApp();
  }

  ngOnInit(): void {
    this.riderMessage$ = this.store.select((data) =>  data.Rider.ActiveMessage);
    this.riderError$  = this.store.select((data) => data.Rider.ActiveError);
    this.message$ = this.store.select((data) => data.Auth.ActiveMessage);
    this.userMessage$ = this.store.select((data) => data.User.ActiveMessage);
    this.error$ = this.store.select((data) => data.Auth.ActiveError);
    this.userError$ = this.store.select((data) => data.User.ActiveError);
    this.isLoading$ = this.store.select((data) => data.Auth.IsLoading);
    this.isUserLoading$ = this.store.select((data) => data.User.IsLoading);
    this.isRiderLoading$ = this.store.select((data) => data.Rider.IsLoading);
    this.cartItemsCount$ = this.store.select((data) => data.User.CartItemCount);
    this.activeActionMessage$ = this.store.select((data) => data.User.ActiveActionMessage);
    this.riderActiveActionMessage$ = this.store.select((data) => data.Rider.ActiveActionMessage);
    // ? Open websocket connection
    this.signalRSrv.startConnection();
    // ? Listen for cart confirmation
    this.signalRSrv.confirmCartVerificationRequest();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      timer(5000).subscribe(() => {
        SplashScreen.hide();
      });

      /**
       * Subscribe to Socket events to make them global
       */
      this.subscribeToEvents();
    });
  }

  clearError(): void {
    this.store.dispatch(AuthActions.ClearActiveErrorAction());
    this.store.dispatch(UserActions.ClearActiveErrorAction());
    this.store.dispatch(RiderActions.ClearActiveErrorAction());
  }

  clearMessage(): void {
    this.store.dispatch(AuthActions.ClearActiveMessageAction());
    this.store.dispatch(UserActions.ClearActiveMessageAction());
    this.store.dispatch(UserActions.ClearActiveActionMessageAction());
    this.store.dispatch(RiderActions.ClearActiveMessageAction());
    this.store.dispatch(RiderActions.ClearActiveActionMessageAction());
  }

  private subscribeToEvents(): void {
    this.subSink.sink =
    this.signalRSrv.cartAssignedToRider.subscribe((payload: RiderCartType) => {
      this._ngZone.run(() => {
        console.log({ payload });
        if (payload?.person) {
          // ? Open local push notifications
          // this.localNotificationSrv.initPush({
          //   Id: 1,
          //   Title: "Cart Assigned",
          //   Body: "New cart assigned to you",
          //   ExtraData: {
          //     Id: payload.cart.id,
          //     PushNotificationType: PushNotificationCategory.CART_ASSIGNMENT
          //   }
          // });
          this.store.dispatch(RiderActions.NewCartAssignedToRiderAction({ payload: { cart: payload } }));
        }
      });
    });

    // ? Emit the rider current location every x seconds
    this.subSink.sink =
    this.locationSrv
        .getRiderCurrentGeoPosition
        .subscribe(({ Payload, UserId }) => {
          this._ngZone.run(() => {
            let runningInterval;
            if (Payload && Payload > 0) {
              runningInterval = setInterval(() => {
                const thenAble: Promise<LocationAbbrType> = this.locationSrv.getUserCurrentPosition();
                thenAble.then(({ lat, lng }) => {
                  console.log({ riderLocation: { lat, lng } });
                  this.store.dispatch(RiderActions.SaveRiderGeolocationInitiatedAction({
                    payload: {
                      latitude: lat,
                      longitude: lng,
                      userId: UserId
                    }}));
                });
              }, (60000 * 2));
            }
            else {
              clearInterval(runningInterval);
            }
          });
    });
  }

  ngOnDestroy(): void {
    this.subSink.unsubscribe();
  }
}
