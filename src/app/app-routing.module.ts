import { NgModule } from "@angular/core";
import { PreloadAllModules, RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "./auth/guards/auth.guard";
import { CustomerGuard } from "./auth/guards/customer.guard";
import { RiderGuard } from "./auth/guards/rider.guard";

const routes: Routes = [
  {
    path: "home",
    loadChildren: () => import("./home/home.module").then( m => m.HomePageModule)
  },
  {
    path: "",
    redirectTo: "home",
    pathMatch: "full"
  },
  {
    path: "profile",
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    children: [
      {
        path: "",
        pathMatch: "full",
        loadChildren: () => import("./user/profile/profile.module").then( m => m.ProfilePageModule)
      },
      {
        path: "edit-profile",
        loadChildren: () => import("./user/profile/edit-profile/edit-profile.module").then(m => m.EditProfilePageModule)
      },
      {
        path: "payment-methods",
        loadChildren: () => import("./user/profile/payment-method/payment-method.module").then(m => m.PaymentMethodPageModule)
      },
      {
        path: "address",
        loadChildren: () => import("./user/profile/address/address.module").then( m => m.AddressPageModule)
      },
      {
        path: "add-address",
        loadChildren: () => import("./user/profile/add-address/add-address.module").then(m => m.AddAddressPageModule)
      }
    ]
  },
  {
    path: "sign-up-otp-verification",
    loadChildren: () =>
    import("./auth/sign-up-otp-verification/sign-up-otp-verification.module").then( m => m.SignUpOtpVerificationPageModule)
  },
  {
    path: "auth",
    loadChildren: () => import("./auth/auth.module").then((m) => m.AuthModule),
  },
  {
    path: "user",
    canActivate: [CustomerGuard],
    canActivateChild: [CustomerGuard],
    loadChildren: () => import("./user/user.module").then((m) => m.UserModule),
  },
  {
    path: "rider",
    canActivate: [RiderGuard],
    canActivateChild: [RiderGuard],
    loadChildren: () => import("./rider/rider.module").then((m) => m.RiderModule),
  },
  {
    path: "live-order-tracking",
    loadChildren: () => import("./rider/live-order-tracking/live-order-tracking.module").then( m => m.LiveOrderTrackingPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules, useHash: true })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
