import { createAction } from "@ngrx/store";

export enum AppActionType {
    CLEAR_APP_STATE = "[APP_ROOT] CLEAR_APP_STATE",
}

export const actions = {
    ClearAppStateAction: createAction(
        AppActionType.CLEAR_APP_STATE
    ),
};
