import { createReducer, on } from "@ngrx/store";
import { InitialAppState } from "../model/app.model";
import { actions as AppActions } from "../actions/app.action";

export const AppReducer = createReducer(
    InitialAppState,
    on(AppActions.ClearAppStateAction, (state) => {
        return { User: undefined, Auth: undefined, Rider: undefined };
    }),
);
