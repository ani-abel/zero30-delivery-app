import { SingleCartType, LocationAbbrType } from "../../rider/store/model/rider.model";
import { AppState } from "../../../utils/types/app.model";

export interface CartLocationType {
    cart: SingleCartType;
    locationMarker: LocationAbbrType;
}

export const InitialAppState: AppState =  {
    User: undefined,
    Rider: undefined,
    Auth: undefined,
}
