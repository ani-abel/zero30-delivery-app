import { Component, Input, OnInit } from '@angular/core';
import { RiderCartType } from '../../rider/store/model/rider.model';

@Component({
  selector: 'app-rider-delivery-history-widget',
  templateUrl: './rider-delivery-history-widget.component.html',
  styleUrls: ['./rider-delivery-history-widget.component.scss'],
})
export class RiderDeliveryHistoryWidgetComponent implements OnInit {
  @Input() cart: RiderCartType;

  constructor() { }

  ngOnInit() {}

}
