import { Component, OnInit, Input } from '@angular/core';
import { MerchantModel } from '../../user/store/model/user.model';

export type BookmarkFavouriteMerchantType = { MerchantId: string, IsBookmarked: boolean };

@Component({
  selector: 'app-favourite-merchant-hightlight-widget',
  templateUrl: './favourite-merchant-hightlight-widget.component.html',
  styleUrls: ['./favourite-merchant-hightlight-widget.component.scss'],
})
export class FavouriteMerchantHightlightWidgetComponent implements OnInit {
  @Input() favouriteMerchant: MerchantModel;
  
  constructor() { }

  ngOnInit() { }

}
