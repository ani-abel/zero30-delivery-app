import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-no-orders-found-widget',
  templateUrl: './no-orders-found-widget.component.html',
  styleUrls: ['./no-orders-found-widget.component.scss'],
})
export class NoOrdersFoundWidgetComponent implements OnInit {
  @Input() purpose: string;
  @Input() showButton: boolean;

  constructor() { }

  ngOnInit() {}

}
