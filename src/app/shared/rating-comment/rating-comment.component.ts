import { Component, OnInit, Input } from '@angular/core';
import { MerchantReviewType } from '../../user/store/model/user.model';

@Component({
  selector: 'app-rating-comment',
  templateUrl: './rating-comment.component.html',
  styleUrls: ['./rating-comment.component.scss'],
})
export class RatingCommentComponent implements OnInit {
  @Input() merchantReview: MerchantReviewType;
  fullName: string;
  ariaLabel: string;
  styleString: string;

  constructor() { }

  ngOnInit() {
    const { user: { person: { firstName, lastName } }, rating } = this.merchantReview;

    this.fullName = `${firstName} ${lastName}`;
    this.styleString = `--rating: ${rating}`;
    this.ariaLabel = `Rating of this product is ${rating} out of 5.`
  }

}
