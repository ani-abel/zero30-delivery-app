import { Component, OnInit } from "@angular/core";

declare function unBundleSVG();

@Component({
  selector: "app-rider-bottom-navbar",
  templateUrl: "./rider-bottom-navbar.component.html",
  styleUrls: ["./rider-bottom-navbar.component.scss"],
})
export class RiderBottomNavbarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    unBundleSVG();
  }

}
