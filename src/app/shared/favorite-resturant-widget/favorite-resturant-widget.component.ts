import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { MerchantModel } from '../../user/store/model/user.model';

declare function unBundleSVG();

@Component({
  selector: 'app-favorite-resturant-widget',
  templateUrl: './favorite-resturant-widget.component.html',
  styleUrls: ['./favorite-resturant-widget.component.scss'],
})
export class FavoriteResturantWidgetComponent implements OnInit {
  @Input() merchant: MerchantModel;
  @Output() addToBookmark: EventEmitter<MerchantModel> = new EventEmitter<MerchantModel>();

  constructor() { }

  ngOnInit() { 
    unBundleSVG();
  }

  addBookmark(merchant: MerchantModel): void {
    this.addToBookmark.emit(merchant);
  }

}
