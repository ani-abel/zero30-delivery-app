import { Component, OnInit, Input } from "@angular/core";
import { CartCheckListType } from "../../rider/store/model/rider.model";

@Component({
  selector: "app-accordian",
  templateUrl: "./accordian.component.html",
  styleUrls: ["./accordian.component.scss"],
})
export class AccordianComponent implements OnInit {
  @Input() cartCheckList: CartCheckListType;
  isAccordianExpanded = true;

  constructor() { }

  ngOnInit(): void { }

  toggleAccordian(): void {
    this.isAccordianExpanded = !this.isAccordianExpanded;
  }

}
