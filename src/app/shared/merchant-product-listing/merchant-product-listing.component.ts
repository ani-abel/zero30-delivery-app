import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { ProductDetailType, ProductMerchantType } from '../../user/store/model/user.model';
import { BookmarkFavouriteMerchantType } from '../favourite-merchant-hightlight-widget/favourite-merchant-hightlight-widget.component';

declare function unBundleSVG();

@Component({
  selector: 'app-merchant-product-listing',
  templateUrl: './merchant-product-listing.component.html',
  styleUrls: ['./merchant-product-listing.component.scss'],
})
export class MerchantProductListingComponent implements OnInit {
  @Output() addToBookmark: EventEmitter<BookmarkFavouriteMerchantType> = new EventEmitter<BookmarkFavouriteMerchantType>();
  @Input() isBookmarked: boolean;
  @Input() merchant: ProductMerchantType;
  @Input() currentIteration: number;
  @Input() arraySize: number;

  constructor(
    private readonly router: Router,
  ) { }

  ngOnInit() { 
    unBundleSVG();
  }

  addBookmark(merchantId: string): void {
    this.addToBookmark.emit({ IsBookmarked: true, MerchantId: merchantId });
  }

  async openProductDetail(event: any, product: ProductDetailType): Promise<void> {
    if (product?.productModel) {
      this.router.navigateByUrl(`/user/product-detail?product=${JSON.stringify(product)}`);
    }
  }

}
