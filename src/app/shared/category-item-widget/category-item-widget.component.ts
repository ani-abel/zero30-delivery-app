import { Component, Input, OnInit } from '@angular/core';
import { ProductCategoryType } from '../../user/store/model/user.model';

@Component({
  selector: 'app-category-item-widget',
  templateUrl: './category-item-widget.component.html',
  styleUrls: ['./category-item-widget.component.scss'],
})
export class CategoryItemWidgetComponent implements OnInit {
  @Input() productCategory: ProductCategoryType;
  
  constructor() { }

  ngOnInit() { }

}
