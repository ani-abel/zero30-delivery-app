import { Component, OnInit, Input } from "@angular/core";
import { AuthResponseType } from "../../auth/store/model/auth.model";
import { Zero30Role } from "../../../utils/types/app.constant";

declare function unBundleSVG();

@Component({
  selector: "app-conditional-bottom-navbar",
  templateUrl: "./conditional-bottom-navbar.component.html",
  styleUrls: ["./conditional-bottom-navbar.component.scss"],
})
export class ConditionalBottomNavbarComponent implements OnInit {
  @Input() userData: AuthResponseType;
  appRole = Zero30Role;

  constructor() { }

  ngOnInit(): void {
    unBundleSVG();
  }

}
