import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { ProductSearchResultType } from '../../user/store/model/user.model';

@Component({
  selector: 'app-search-results-widget',
  templateUrl: './search-results-widget.component.html',
  styleUrls: ['./search-results-widget.component.scss'],
})
export class SearchResultsWidgetComponent implements OnInit {
  @Output() viewProductDetailEvent: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() addProductToBookmarkType: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Input() searchResult: ProductSearchResultType;
  @Input() isBookmarked: boolean;

  viewProductDetail(): void {
    this.viewProductDetailEvent.emit(true);
  }

  constructor() { }

  ngOnInit() { }

  addProductToBookmark(): void {
    this.addProductToBookmarkType.emit(true);
  }

}
