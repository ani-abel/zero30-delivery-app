import { Pipe, PipeTransform } from '@angular/core';
import { ProductCheckout } from '../user/store/model/user.model';

@Pipe({
  name: 'productCostCalculator'
})
export class ProductCostCalculatorPipe implements PipeTransform {

  transform(value: ProductCheckout[]): number {
    const allPrices: number[] = value.map((data) => data.cost) || [];
    return allPrices.reduce((a, b) => a + b, 0);
  }
}
