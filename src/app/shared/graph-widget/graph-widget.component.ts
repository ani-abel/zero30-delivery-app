import {
  Component,
  OnInit,
  ViewChild,
  Input,
  AfterViewInit,
  ElementRef
} from "@angular/core";
import { endOfWeek } from "date-fns";
import { DeliveryGraphSummaryType } from "../../rider/store/model/rider.model";

declare var Plotly: any;

@Component({
  selector: "app-graph-widget",
  templateUrl: "./graph-widget.component.html",
  styleUrls: ["./graph-widget.component.scss"],
})
export class GraphWidgetComponent implements
OnInit,
AfterViewInit {
  @Input() graphData: DeliveryGraphSummaryType;
  @Input() index: number;
  @ViewChild("thisWeekStats", { read: ElementRef }) thisWeekStatsEl: ElementRef;
  graphLabel: string;
  isDataFull: boolean;
  xValue: string[];
  yValue: number[];

  constructor() { }

  ngOnInit() {
    this.graphData = {
      ...this.graphData,
      weekOrderCounts: this.graphData.weekOrderCounts.map((data) => {
        return {
          ...data,
          labels: `${data.count} deliveries`,
        };
      })
    };

    this.xValue = this.graphData.weekOrderCounts.map((data) => {
      return data.weekDay?.slice(0, 3);
    });

    this.yValue = this.graphData.weekOrderCounts.map((data) => data.count);
    this.graphLabel = this.setGraphLabel(this.index);
  }

  ngAfterViewInit(): void {
    const statsForThisWeek = [
      {
        text: this.yValue.map(String),
        textposition: "auto",
        hoverinfo: "none",
        x: [...this.xValue],
        y: [...this.yValue],
        autosize: false,
        height: 500,
        margin: {
          l: 50,
          r: 50,
          b: 100,
          t: 100,
          pad: 4
        },
        marker: {
          color: [
            "rgba(0,126,204, 0.9)",
            "rgba(0, 135, 176, 0.8)",
            "rgba(222,45,38,0.8)",
            "rgba(0,126,204, 0.9)",
            "rgba(0, 135, 176, 0.8)",
            "rgba(222,45,38,0.8)",
            "rgba(0,126,204, 0.9)",
          ]
        },
        name: `Deliveries for ${this.graphLabel}`,
        type: "bar"
      }
    ];
    Plotly.newPlot(this.thisWeekStatsEl.nativeElement, statsForThisWeek);
  }

  endOfWeek(startDate: string): Date {
    return endOfWeek(new Date(startDate));
  }

  private setGraphLabel(index: number): string {
    let returnedData = `${index + 1} weeks ago`;
    if (index === 0) {
      returnedData = "This Week";
    }
    if (index === 1) {
      returnedData = "Last Week";
    }
    return returnedData;
  }

}
