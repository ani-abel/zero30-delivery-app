import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ProductDetailType } from '../../user/store/model/user.model';

declare function unBundleSVG();

@Component({
  selector: 'app-favorite-food-widget',
  templateUrl: './favorite-food-widget.component.html',
  styleUrls: ['./favorite-food-widget.component.scss'],
})
export class FavoriteFoodWidgetComponent implements OnInit {
  @Input() product: ProductDetailType;
  @Output() viewProductDetailEvent: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit() { 
    unBundleSVG();
  }

  openProductDetail(): void {
    this.viewProductDetailEvent.emit(true);
  }

}
