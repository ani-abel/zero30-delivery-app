import { Component, OnInit, Input } from '@angular/core';
import { ProductCartStatus, ProductCheckout, ProductOrdersResultType } from '../../user/store/model/user.model';

declare function unBundleSVG();

@Component({
  selector: 'app-order-selected-widget-history',
  templateUrl: './order-selected-widget-history.component.html',
  styleUrls: ['./order-selected-widget-history.component.scss'],
})
export class OrderSelectedWidgetHistoryComponent implements OnInit {
  @Input() cart: ProductOrdersResultType;
  productCartStatus = ProductCartStatus;

  constructor() { }

  ngOnInit() {
    unBundleSVG();
  }

  getProductImageToPreview(products: ProductCheckout[]): string {
    let imagePath: string;
    if(products[0].productImage) {
      imagePath = products[0].productImage;
    }
    else {
      const productsWithImages: ProductCheckout[] = products.filter((data) => data.productImage);
      if(productsWithImages?.length > 0) {
        imagePath = productsWithImages[0].productImage;
      }
    }
    return imagePath;
  }

}
