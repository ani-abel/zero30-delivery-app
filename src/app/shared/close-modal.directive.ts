import { Directive, HostListener } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Directive({
  selector: '[appCloseModal]'
})
export class CloseModalDirective {

  constructor(public readonly modalCtrl: ModalController) { }

  @HostListener("click")
  onClick(): void {
   // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

}
