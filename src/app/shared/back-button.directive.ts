import { Directive, HostListener } from "@angular/core";
import { NavigationService } from "../services/navigation.service";

@Directive({
  selector: "[backButton]",
})
export class BackButtonDirective {
  constructor(private navigation: NavigationService) {}

  @HostListener("click")
  async onClick(): Promise<void> {
    await this.navigation.goBackOnNative();
  }
}
