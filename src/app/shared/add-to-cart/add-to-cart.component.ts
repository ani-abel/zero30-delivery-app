import { Component, OnInit, Input, Output,OnDestroy, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators} from "@angular/forms";
import { Observable } from 'rxjs';
import { Store } from "@ngrx/store";
import { SubSink } from 'subsink';
import { UserModel } from '../../user/store/model/user.model';
import { CheckedOutProduct, ProductCheckout, ProductImage } from '../../user/store/model/user.model';
import { AppState } from '../../../utils/types/app.model';
import { actions as UserAction } from "../../user/store/actions/user.action";

@Component({
  selector: 'app-add-to-cart',
  templateUrl: './add-to-cart.component.html',
  styleUrls: ['./add-to-cart.component.scss'],
})
export class AddToCartComponent implements 
OnInit,
OnDestroy {
  @Output() closeAddToCartModalEvent: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Input() show: boolean;
  @Input() product: ProductCheckout;
  @Input() productImages: ProductImage[];
  @Input() userId: string;
  addDeliveryAddressForm: FormGroup;
  subSink: SubSink = new SubSink();
  userData$: Observable<UserModel>;
  cartItems$: Observable<CheckedOutProduct>;
  currentPrice: number;

  constructor(private readonly store: Store<AppState>) { }

  ngOnInit(): void {
    if(this.userId) {
      this.initForm();
      this.currentPrice = this.product.cost || 0;

      //? Pull the current items in cart to be displayed
      this.cartItems$ = this.store.select((data) => data.User.CartItems);

      //? Get user data from the store
      this.store.dispatch(UserAction.GetUserDataInitiatedAction({ payload: { userId: this.userId } }));
      this.userData$ = this.store.select((data) => data.User.UserData);

      this.subSink.sink = 
      this.userData$
          .subscribe((data) => {
            if(data?.userId) {
              this.initForm(data);
            }
          });
    }
  }

  closeAddToCartModal(): void {
    this.closeAddToCartModalEvent.emit(true);
  }

  subtractItemCount(): void {
    this.product.quantity = this.product.quantity > 1 ? this.product.quantity -= 1 : 1;
    this.product.cost = this.product.cost > this.currentPrice ? this.product.cost -= this.currentPrice : this.product.cost;
  }

  addItemCount(): void {
    this.product.quantity += 1;
    this.product.cost = this.currentPrice * this.product.quantity;
  }

  initForm(userData?: UserModel) {
    this.addDeliveryAddressForm = new FormGroup({
      Address: new FormControl(userData?.person?.address || null, Validators.compose([
        Validators.required
      ]))
    });
  }

  onSubmit(): void {
    if(this.addDeliveryAddressForm.invalid) {
      return;
    }
    const { Address } = this.addDeliveryAddressForm.value;
    this.store.dispatch(UserAction.AddProductToCartInitiatedAction({ payload: { 
      deliveryAddress: Address, 
      userId: this.userId, 
      product: { ...this.product } 
    } }));
  }

  ngOnDestroy(): void {
    this.subSink.unsubscribe();
  }

}
