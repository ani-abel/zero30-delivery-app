import { Component, OnInit } from "@angular/core";

declare function unBundleSVG();

@Component({
  selector: "app-user-bottom-navbar",
  templateUrl: "./user-bottom-navbar.component.html",
  styleUrls: ["./user-bottom-navbar.component.scss"],
})
export class UserBottomNavbarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    unBundleSVG();
  }
}
