import { Component, Input, OnInit } from '@angular/core';
import { ProductCategoryType } from '../../user/store/model/user.model';

@Component({
  selector: 'app-category-hightlight-widget',
  templateUrl: './category-hightlight-widget.component.html',
  styleUrls: ['./category-hightlight-widget.component.scss'],
})
export class CategoryHightlightWidgetComponent implements OnInit {
  @Input() productCategory: ProductCategoryType;

  constructor() { }

  ngOnInit() { }

}
