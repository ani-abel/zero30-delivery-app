import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ProductCheckout, ProductDetailType } from '../../user/store/model/user.model';
import { ProductCartValue } from '../merchant-hot-sale-product/merchant-hot-sale-product.component';

@Component({
  selector: 'app-product-category-widget',
  templateUrl: './product-category-widget.component.html',
  styleUrls: ['./product-category-widget.component.scss'],
})
export class ProductCategoryWidgetComponent implements OnInit {
  @Output() viewProductDetailEvent: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() addProductToBookmarkEvent: EventEmitter<string> = new EventEmitter<string>();
  @Output() addToCartEvent: EventEmitter<ProductCartValue> = new EventEmitter<ProductCartValue>();
  @Input() product: ProductDetailType;

  constructor() { }

  ngOnInit() { }

  viewProductDetail(): void {
    this.viewProductDetailEvent.emit(true);
  }

  addProductToBookmark(productId): void {
    this.addProductToBookmarkEvent.emit(productId);
  }

  addToCart(product: ProductDetailType): void {
    const { productModel: { id, name }, productCostModel: { unitPrice }, productImageModels } = product;
    this.addToCartEvent.emit({ status: true, payload: { 
      productId: id, 
      productName: name, 
      productImage: product.productImageModels[0]?.path || undefined,
      cost: unitPrice, 
      quantity: 1 
    }, 
    productImages: productImageModels });
  }

}
