import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-no-internet-modal',
  templateUrl: './no-internet-modal.component.html',
  styleUrls: ['./no-internet-modal.component.scss'],
})
export class NoInternetModalComponent implements OnInit {

  constructor() { }

  ngOnInit() {}

}
