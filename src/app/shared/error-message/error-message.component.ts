import { HttpErrorResponse } from '@angular/common/http';
import { 
  Component, 
  EventEmitter, 
  Input, 
  OnInit, 
  Output 
} from '@angular/core';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-error-message',
  template: ``,
  styleUrls: ['./error-message.component.scss'],
})
export class ErrorMessageComponent implements OnInit {
  @Input() error: Error | HttpErrorResponse;
  @Output() closeAlert: EventEmitter<boolean> = new EventEmitter<boolean>();
  
  constructor(public readonly toastController: ToastController) {}

  async ngOnInit() {
    if(!this.error?.message) {
      if(this.error["error"]) {
        this.error = this.error["error"];
      }
      else {
        this.error = new Error("An error occured");
      }
    }
    await this.presentToastWithOptions();
  }
  
  async presentToast() {
    const toast = await this.toastController.create({
      message: this.error?.message,
      duration: 5000
    });
    toast.present();
  }

  async presentToastWithOptions() {
    const toast = await this.toastController.create({
      message: this.error?.message,
      duration: 5000,
      position: 'top',
      color: "danger",
      buttons: [
        {
          text: 'Close',
          role: 'cancel',
          handler: () => {
            this.closeAlert.emit(true);
          }
        }
      ]
    });
    toast.present();
  }

}
