import { Component, Output, OnInit, Input, EventEmitter } from '@angular/core';
import { ProductCheckout, ProductDetailType, ProductImage } from '../../user/store/model/user.model';

declare function unBundleSVG();

export interface ProductCartValue {
  productImages: ProductImage[];
  status: boolean;
  payload: ProductCheckout;
}

@Component({
  selector: 'app-merchant-hot-sale-product',
  templateUrl: './merchant-hot-sale-product.component.html',
  styleUrls: ['./merchant-hot-sale-product.component.scss'],
})
export class MerchantHotSaleProductComponent implements OnInit {
  @Output() viewProductDetailEvent: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() addProductToBookmarkEvent: EventEmitter<string> = new EventEmitter<string>();
  @Output() addToCartEvent: EventEmitter<ProductCartValue> = new EventEmitter<ProductCartValue>();
  @Input() product: ProductDetailType;

  constructor() { }

  ngOnInit() { 
    unBundleSVG();
  }

  viewProductDetail(): void {
    this.viewProductDetailEvent.emit(true);
  }

  addProductToBookmark(productId): void {
    this.addProductToBookmarkEvent.emit(productId);
  }

  addToCart(product: ProductDetailType): void {
    const { productModel: { id, name }, productCostModel: { unitPrice }, productImageModels } = product;
    this.addToCartEvent.emit({ status: true, payload: { 
      productId: id, 
      productName: name, 
      cost: unitPrice, 
      quantity: 1 
    }, 
    productImages: productImageModels });
  }
}
