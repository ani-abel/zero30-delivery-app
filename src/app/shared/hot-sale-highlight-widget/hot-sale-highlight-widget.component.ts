import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ProductDetailType } from '../../user/store/model/user.model';

@Component({
  selector: 'app-hot-sale-highlight-widget',
  templateUrl: './hot-sale-highlight-widget.component.html',
  styleUrls: ['./hot-sale-highlight-widget.component.scss'],
})
export class HotSaleHighlightWidgetComponent implements OnInit {
  @Input() product: ProductDetailType;
  @Output() viewProductDetailEvent: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit() { }

  viewProductDetail(): void {
    this.viewProductDetailEvent.emit(true);
  }

}
