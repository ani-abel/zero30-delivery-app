import { Component, OnInit, Input } from '@angular/core';
import { ProductCartStatus, ProductCheckout, ProductOrdersResultType } from '../../user/store/model/user.model';

declare function unBundleSVG();

@Component({
  selector: 'app-order-selected-widget-ongoing',
  templateUrl: './order-selected-widget-ongoing.component.html',
  styleUrls: ['./order-selected-widget-ongoing.component.scss'],
})
export class OrderSelectedWidgetOngoingComponent implements OnInit {
  @Input() cart: ProductOrdersResultType;
  productCartStatus = ProductCartStatus;

  constructor() { }

  ngOnInit() {
    unBundleSVG();
  }

  getProductImageToPreview(products: ProductCheckout[]): string {
    let imagePath: string;
    if(products[0].productImage) {
      imagePath = products[0].productImage;
    }
    else {
      const productsWithImages: ProductCheckout[] = products.filter((data) => data.productImage);
      if(productsWithImages?.length > 0) {
        imagePath = productsWithImages[0].productImage;
      }
    }
    return imagePath;
  }

}
