import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { ProductDetailType } from '../../user/store/model/user.model';

@Component({
  selector: 'app-single-product-widget',
  templateUrl: './single-product-widget.component.html',
  styleUrls: ['./single-product-widget.component.scss'],
})
export class SingleProductWidgetComponent implements OnInit {
  @Input() product: ProductDetailType;
  @Output() viewProductDetailEvent: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit() { }

  viewProductDetail(): void {
    this.viewProductDetailEvent.emit(true);
  }

}
