import { Component, OnDestroy, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-loader',
  template: `
    <div class="modal-container">
      <div class="loader"></div>
    </div>
  `,
  styleUrls: ['./loader.component.scss'],
})
export class LoaderComponent implements 
OnInit, 
OnDestroy {

  ngOnInit(): void { }

  // async ngOnInit(): Promise<void> {
  //   // const el: HTMLIonLoadingElement = await this.loadingController.create({
  //   //   message: 'Please wait...',
  //   //   spinner: "bubbles",
  //   //   keyboardClose: true,
  //   //   backdropDismiss: false
  //   // });
  //   // await el.present();
  // }

  constructor(private readonly loadingController: LoadingController) { }

  ngOnDestroy(): void {
    // try {
    //   setTimeout(async () => {
    //     //? Wait for 1ms, then dismiss the loader
    //     await this.loadingController.dismiss();
    //   }, 1000);
    // }
    // catch(ex) {
    //   throw ex;
    // }
  }
  
}