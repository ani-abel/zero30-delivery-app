import { Pipe, PipeTransform } from '@angular/core';
import { environment as env } from "../../environments/environment";

@Pipe({
  name: 'filePathFormatter'
})
export class FilePathFormatterPipe implements PipeTransform {

  transform(value: string): string {
    if(value) {
      //? Get the apis root url
      const regExp = /https?:\/\/udevalentine-001-site1\.itempurl\.com\//ig;

      //? Search for the url at the start of the image
      if(value?.match(regExp)?.length > 1) {
        const returnValue: string = value.replace(regExp, "");
        return `${env.apiRoot}/${returnValue}`;
      }
      if(!value?.startsWith(env.apiRoot)) {
        return `${env.apiRoot}/${value}`;
      }
      return value;
    }
  }

}
