import { Component, Input, OnInit } from '@angular/core';
import { ToastController } from "@ionic/angular";
import { Router } from "@angular/router";
import { MessageAction, MessageActionType } from '../../user/store/model/user.model';

@Component({
  selector: 'app-clickable-mesage',
  template: ``,
  styleUrls: ['./clickable-mesage.component.scss'],
})
export class ClickableMesageComponent implements OnInit {
  @Input() message: MessageAction;

  constructor(
    public readonly toastController: ToastController,
    private readonly router: Router
  ) {}

  async ngOnInit(): Promise<void> {
    await this.presentToastWithOptions();
  }

  async presentToastWithOptions() {
    const toast = await this.toastController.create({
      message: this.message.Message,
      position:  'top',
      duration: 15000, //5 Seconds limit
      buttons: [
        {
          text: this.message?.Label,
          role: 'cancel',
          handler: () => {
            if(this.message.Type === MessageActionType.CART_VERIFIED) {
              this.router.navigate(["/user", "product-cart"]);
            }
            if(this.message.Type === MessageActionType.RIDER_ASSIGNED_TO_CART) {
              this.router.navigate(["/rider", "orders"]);
            }
          }
        }
      ]
    });
    toast.present();
  }

}
