import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ProductSearchResultType } from '../../user/store/model/user.model';

@Component({
  selector: 'app-ordered-product-highlight-widget',
  templateUrl: './ordered-product-highlight-widget.component.html',
  styleUrls: ['./ordered-product-highlight-widget.component.scss'],
})
export class OrderedProductHighlightWidgetComponent implements OnInit {
  @Input() product: ProductSearchResultType;
  @Output() viewProductDetailEvent: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit() { }

  viewProductDetail(): void {
    this.viewProductDetailEvent.emit(true);
  }

}
