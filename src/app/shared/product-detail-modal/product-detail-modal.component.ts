import { Component, OnInit, Input } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../../utils/types/app.model';
import { actions as UserAction } from "../../user/store/actions/user.action";
import { AuthResponseType } from '../../../app/auth/store/model/auth.model';
import { getDataFromLocalStorage } from '../../../utils/functions/app.functions';
import { LocalStorageKey } from '../../../utils/types/app.constant';
import { ProductCheckout, ProductDetailType, ProductImage } from '../../user/store/model/user.model';
import SwiperCore, { Navigation, Pagination, A11y, Autoplay } from 'swiper/core';
// install Swiper components
SwiperCore.use([Navigation, Pagination, A11y, Autoplay]);

declare function unBundleSVG();

@Component({
  selector: 'app-product-detail-modal',
  templateUrl: './product-detail-modal.component.html',
  styleUrls: ['./product-detail-modal.component.scss'],
})
export class ProductDetailModalComponent implements OnInit {
  @Input() product: ProductDetailType;
  openCartModal: boolean = false;
  productAddedToCart: ProductCheckout;
  productAddedToCartImages: ProductImage[];
  userId: string;

  constructor(private readonly store: Store<AppState>) { }

  ngOnInit() { 
    unBundleSVG();
  }

  async addProductToCart({ productImageModels, productModel, productCostModel }): Promise<void> {
    const { userId } = await getDataFromLocalStorage<AuthResponseType>(LocalStorageKey.ZERO_30_USER);
    if(userId) {
      this.userId = userId
      const { id, name } = productModel;
      this.openCartModal = true;
      this.productAddedToCart = {
        cost: productCostModel.unitPrice,
        quantity: 1,
        productImage: productImageModels[0]?.path || undefined,
        productId: id,
        productName: name
      };
      this.productAddedToCartImages = productImageModels;
    }
  }

  closeCartModal(): void {
    this.openCartModal = false;
  }

  async addProductToBookmark(productId: string): Promise<void> {
    const { userId } = await getDataFromLocalStorage<AuthResponseType>(LocalStorageKey.ZERO_30_USER);
    if (userId) {
      // ? Dispatch action
      this.store.dispatch(UserAction.AddProductToBookmarkInitiatedAction({ payload: { userId, productId } }));
    }
  }

}
