import { Component, OnInit, Input } from "@angular/core";
import { RiderCartType } from "../../rider/store/model/rider.model";

@Component({
  selector: "app-rider-delivery-ongoing-widget",
  templateUrl: "./rider-delivery-ongoing-widget.component.html",
  styleUrls: ["./rider-delivery-ongoing-widget.component.scss"],
})
export class RiderDeliveryOngoingWidgetComponent implements OnInit {
  @Input() cart: RiderCartType;

  constructor() { }

  ngOnInit() {}

}
