import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgxDatePickerModule } from "@ngx-tiny/date-picker";
import { SwiperModule } from "swiper/angular";
import { UserBottomNavbarComponent } from "./user-bottom-navbar/user-bottom-navbar.component";
import { OrderSelectedWidgetHistoryComponent } from "./order-selected-widget-history/order-selected-widget-history.component";
import { OrderSelectedWidgetDraftComponent } from "./order-selected-widget-draft/order-selected-widget-draft.component";
import { OrderSelectedWidgetOngoingComponent } from "./order-selected-widget-ongoing/order-selected-widget-ongoing.component";
import { NoOrdersFoundWidgetComponent } from "./no-orders-found-widget/no-orders-found-widget.component";
import { FavoriteFoodWidgetComponent } from "./favorite-food-widget/favorite-food-widget.component";
import { FavoriteResturantWidgetComponent } from "./favorite-resturant-widget/favorite-resturant-widget.component";
import { NearbyItemWidgetComponent } from "./nearby-item-widget/nearby-item-widget.component";
import { LoaderComponent } from "./loader/loader.component";
import { SuccessMessageComponent } from "./success-message/success-message.component";
import { ErrorMessageComponent } from "./error-message/error-message.component";
import { NoInternetModalComponent } from "./no-internet-modal/no-internet-modal.component";
import { MerchantProductListingComponent } from "./merchant-product-listing/merchant-product-listing.component";
import { SearchResultsWidgetComponent } from "./search-results-widget/search-results-widget.component";
import { SingleProductWidgetComponent } from "./single-product-widget/single-product-widget.component";
import { CategoryHightlightWidgetComponent } from "./category-hightlight-widget/category-hightlight-widget.component";
import { FavouriteMerchantHightlightWidgetComponent } from "./favourite-merchant-hightlight-widget/favourite-merchant-hightlight-widget.component";
import { FilePathFormatterPipe } from "./file-path-formatter.pipe";
import { HotSaleHighlightWidgetComponent } from "./hot-sale-highlight-widget/hot-sale-highlight-widget.component";
import { OrderedProductHighlightWidgetComponent } from "./ordered-product-highlight-widget/ordered-product-highlight-widget.component";
import { CloseModalDirective } from "./close-modal.directive";
import { ProductDetailModalComponent } from "./product-detail-modal/product-detail-modal.component";
import { IonicModule } from "@ionic/angular";
import { SliceContentPipe } from "./slice-content.pipe";
import { CapitalizePipe } from "./capitalize.pipe";
import { ProductCategoryWidgetComponent } from "./product-category-widget/product-category-widget.component";
import { FooterExpanderComponent } from "./footer-expander/footer-expander.component";
import { RatingCommentComponent } from "./rating-comment/rating-comment.component";
import { MerchantHotSaleProductComponent } from "./merchant-hot-sale-product/merchant-hot-sale-product.component";
import { CategoryItemWidgetComponent } from "./category-item-widget/category-item-widget.component";
import { ProductItemWidgetComponent } from "./product-item-widget/product-item-widget.component";
import { CartBadgeComponent } from "./cart-badge/cart-badge.component";
import { AddToCartComponent } from "./add-to-cart/add-to-cart.component";
import { ProductCostCalculatorPipe } from "./product-cost-calculator.pipe";
import { BackButtonDirective } from "./back-button.directive";
import { NoContentFoundComponent } from "./no-content-found/no-content-found.component";
import { ClickableMesageComponent } from "./clickable-mesage/clickable-mesage.component";
import { RiderBottomNavbarComponent } from "./rider-bottom-navbar/rider-bottom-navbar.component";
import { ConditionalBottomNavbarComponent } from "./conditional-bottom-navbar/conditional-bottom-navbar.component";
import { RiderDeliveryHistoryWidgetComponent } from "./rider-delivery-history-widget/rider-delivery-history-widget.component";
import { RiderDeliveryOngoingWidgetComponent } from "./rider-delivery-ongoing-widget/rider-delivery-ongoing-widget.component";
import { GraphWidgetComponent } from "./graph-widget/graph-widget.component";
import { AccordianComponent } from "./accordian/accordian.component";

@NgModule({
  declarations: [
    UserBottomNavbarComponent,
    ClickableMesageComponent,
    OrderSelectedWidgetHistoryComponent,
    OrderSelectedWidgetDraftComponent,
    OrderSelectedWidgetOngoingComponent,
    NoOrdersFoundWidgetComponent,
    FavoriteFoodWidgetComponent,
    FavoriteResturantWidgetComponent,
    NearbyItemWidgetComponent,
    LoaderComponent,
    SuccessMessageComponent,
    ErrorMessageComponent,
    NoInternetModalComponent,
    MerchantProductListingComponent,
    SearchResultsWidgetComponent,
    SingleProductWidgetComponent,
    CategoryHightlightWidgetComponent,
    FavouriteMerchantHightlightWidgetComponent,
    HotSaleHighlightWidgetComponent,
    OrderedProductHighlightWidgetComponent,
    ProductDetailModalComponent,
    ProductCategoryWidgetComponent,
    ProductItemWidgetComponent,
    FooterExpanderComponent,
    RatingCommentComponent,
    MerchantHotSaleProductComponent,
    CategoryItemWidgetComponent,
    CartBadgeComponent,
    AddToCartComponent,
    NoContentFoundComponent,
    RiderBottomNavbarComponent,
    ConditionalBottomNavbarComponent,
    RiderDeliveryHistoryWidgetComponent,
    RiderDeliveryOngoingWidgetComponent,
    AccordianComponent,
    GraphWidgetComponent,
    FilePathFormatterPipe,
    CloseModalDirective,
    SliceContentPipe,
    CapitalizePipe,
    ProductCostCalculatorPipe,
    BackButtonDirective
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RouterModule,
    HttpClientModule,
    NgxDatePickerModule,
    SwiperModule
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    UserBottomNavbarComponent,
    OrderSelectedWidgetHistoryComponent,
    OrderSelectedWidgetDraftComponent,
    OrderSelectedWidgetOngoingComponent,
    NoOrdersFoundWidgetComponent,
    FavoriteFoodWidgetComponent,
    FavoriteResturantWidgetComponent,
    NearbyItemWidgetComponent,
    LoaderComponent,
    SuccessMessageComponent,
    ErrorMessageComponent,
    NoInternetModalComponent,
    MerchantProductListingComponent,
    SearchResultsWidgetComponent,
    SingleProductWidgetComponent,
    CategoryHightlightWidgetComponent,
    FavouriteMerchantHightlightWidgetComponent,
    HotSaleHighlightWidgetComponent,
    OrderedProductHighlightWidgetComponent,
    ProductDetailModalComponent,
    ProductCategoryWidgetComponent,
    FooterExpanderComponent,
    RatingCommentComponent,
    MerchantHotSaleProductComponent,
    CategoryItemWidgetComponent,
    ProductItemWidgetComponent,
    CartBadgeComponent,
    AddToCartComponent,
    NoContentFoundComponent,
    ClickableMesageComponent,
    RiderBottomNavbarComponent,
    ConditionalBottomNavbarComponent,
    RiderDeliveryHistoryWidgetComponent,
    RiderDeliveryOngoingWidgetComponent,
    GraphWidgetComponent,
    AccordianComponent,
    FilePathFormatterPipe,
    SliceContentPipe,
    CapitalizePipe,
    ProductCostCalculatorPipe,
    CloseModalDirective,
    BackButtonDirective,
    RouterModule,
    HttpClientModule,
    NgxDatePickerModule,
    SwiperModule
  ],
})
export class SharedModule { }
