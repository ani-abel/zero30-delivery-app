import { 
  Component, 
  OnInit, 
  Input, 
  EventEmitter, 
  Output 
} from '@angular/core';
import { findUserBookmark } from '../../../utils/functions/app.functions';
import { MerchantModel, UserBookmark } from '../../user/store/model/user.model';

declare function unBundleSVG();

@Component({
  selector: 'app-nearby-item-widget',
  templateUrl: './nearby-item-widget.component.html',
  styleUrls: ['./nearby-item-widget.component.scss'],
})
export class NearbyItemWidgetComponent implements OnInit {
  @Input() merchant: MerchantModel;
  @Input() currentIndex: number;
  @Input() arrayLength: number;
  @Output() addToBookmark: EventEmitter<MerchantModel> = new EventEmitter<MerchantModel>();
  isBookmarked: boolean = false;

  constructor() { }

  async ngOnInit() {
    const bookmark: UserBookmark = await findUserBookmark(this.merchant.id);
    if(bookmark?.Id) {
      this.isBookmarked = true;
    }
    unBundleSVG();
  }

  addBookmark(merchant: MerchantModel): void {
    this.addToBookmark.emit(merchant);
  }

}
