import { 
  Component, 
  EventEmitter, 
  Input, 
  OnInit, 
  Output 
} from '@angular/core';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-success-message',
  template: ``,
  styleUrls: ['./success-message.component.scss'],
})
export class SuccessMessageComponent implements OnInit {
  @Input() message: string;
  @Output() closeAlert: EventEmitter<boolean> = new EventEmitter<boolean>();;

  constructor(public readonly toastController: ToastController) {}

  async ngOnInit() {
    await this.presentToastWithOptions();
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: this.message,
      duration: 5000,
    });
    toast.present();
  }

  async presentToastWithOptions() {
    const toast = await this.toastController.create({
      message: this.message,
      duration: 5000,
      position: 'top',
      keyboardClose: true,
      color: "dark",
      buttons: [
        {
          text: 'Close',
          role: 'cancel',
          handler: () => {
            this.closeAlert.emit(true);
          }
        }
      ]
    });
    toast.present();
  }

}
