import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer-expander',
  template: `<div class="footer-expander"></div>`,
  styles: [],
})
export class FooterExpanderComponent implements OnInit {

  constructor() { }

  ngOnInit() {}

}
