import { Injectable } from "@angular/core";
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from "@angular/router";
import { Store } from "@ngrx/store";
import { AppState } from "../../utils/types/app.model";
import { RiderHttpService } from "../rider/store/effects/rider-http.service";
import { LocationAbbrType, SingleCartType } from "../rider/store/model/rider.model";
import { CartLocationType } from "../store/model/app.model";
import { actions as UserActions} from "../user/store/actions/user.action";

@Injectable({
  providedIn: "root"
})
export class CartLocationResolver implements Resolve<CartLocationType> {
  constructor(
    private readonly riderHttpSrv: RiderHttpService,
    private readonly store: Store<AppState>,
  ) {}

  async resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<CartLocationType> {
    const cartId: string = route.paramMap.get("cartId") ?? route.queryParamMap.get("cartId");
    // ? Visually add a loader
    this.store.dispatch(UserActions.StartLoaderRemotelyAction());
    const selectedCart: SingleCartType = await this.riderHttpSrv.getSelectedCart(cartId).toPromise();
    if (selectedCart?.id) {
      const geoTag: LocationAbbrType = await this.riderHttpSrv.reverseGeocodingForAddress(selectedCart.deliveryAddress).toPromise();
      this.store.dispatch(UserActions.EndLoaderRemotelyAction());
      return { cart: selectedCart, locationMarker: geoTag };
    }
  }
}
