import { Injectable, EventEmitter } from "@angular/core";
import { Store } from "@ngrx/store";
import { Plugins } from "@capacitor/core";
import { Observable } from "rxjs";
import {} from "googlemaps";
import { AppState } from "../../utils/types/app.model";
import { actions as RiderActions } from "../rider/store/actions/rider.action";
import {
  LocalStorageKey,
  TransportType,
  Zero30Role
} from "../../utils/types/app.constant";
import {
  LocationAbbrType,
  MapIcon,
  RiderCartType,
  RiderGeolocationData
} from "../rider/store/model/rider.model";
import {
  getDataFromLocalStorage,
  isScriptImported,
  mountGoogleMapScript
} from "../../utils/functions/app.functions";
import { AuthResponseType } from "../auth/store/model/auth.model";
import { environment as env } from "../../environments/environment";

const { Geolocation } = Plugins;

@Injectable({
  providedIn: "root"
})
export class LocationService {
  getRiderCurrentGeoPosition: EventEmitter<RiderGeolocationData> = new EventEmitter<RiderGeolocationData>();
  riderIcon: MapIcon;
  customerIcon: MapIcon;

  constructor(
    private readonly store: Store<AppState>,
  ) {
    mountGoogleMapScript(env.googleAPIKey);
    if (isScriptImported("googleMaps")) {
      // ? Setting the timeout makes sure that google maps loads first before setting the map icons
      setTimeout(() => {
        this.customerIcon = {
          label: "CUSTOMER",
          metadata: {
              url: "assets/images/map-icons/user-map-icon.png",
              // This marker is 20 pixels wide by 32 pixels high.
              // size: new google.maps.Size(20, 32),
              scaledSize: new google.maps.Size(70, 70),
              // The origin for this image is (0, 0).
              origin: new google.maps.Point(0, 0),
              // The anchor for this image is the base of the flagpole at (0, 32).
              anchor: new google.maps.Point(0, 32),
            }
        };
        this.riderIcon = {
          label: "RIDER",
          metadata: {
            url: "assets/images/map-icons/motocycle-icon-3.png",
            // This marker is 20 pixels wide by 32 pixels high.
            // size: new google.maps.Size(20, 32),
            scaledSize: new google.maps.Size(70, 70),
            // The origin for this image is (0, 0).
            origin: new google.maps.Point(0, 0),
            // The anchor for this image is the base of the flagpole at (0, 32).
            anchor: new google.maps.Point(0, 32),
          }
        };
      }, 5000);
    }
  }

  sendRiderLocationData(userId: string, latitude: number, longitude: number): void {
    this.store.dispatch(RiderActions.SaveRiderGeolocationInitiatedAction({ payload: { latitude, longitude, userId } }));
  }

  async getUserCurrentPosition()
  : Promise<LocationAbbrType> {
    // Geolocation.requestPermissions().then((permission) => {
    //   if (permission.results.length > 0) { }
    // });
    const coordinates = await Geolocation.getCurrentPosition({ enableHighAccuracy: true, timeout: 10000, maximumAge: 0 });
    const { coords: { latitude, longitude } } = coordinates;
    if (latitude && longitude) {
      return { lat: latitude, lng: longitude };
    }
  }

  calculateAndDisplayRoute(
    directionsService: google.maps.DirectionsService,
    directionsRenderer: google.maps.DirectionsRenderer,
    pointOfOrigin: LocationAbbrType,
    pointOfDestination: LocationAbbrType,
    transportMode: TransportType = TransportType.DRIVING
    ) {
    directionsService.route(
      {
        origin: { ...pointOfOrigin },
        destination: { ...pointOfDestination },
        // Note that Javascript allows us to access the constant
        // using square brackets and a string value as its
        // "property."
        travelMode: google.maps.TravelMode[transportMode],
        drivingOptions: {
          departureTime: new Date()
        }
      },
      (response, status) => {
        if (status === "OK") {
          console.log({ mapResponse: response });
          directionsRenderer.setDirections(response);
        } else {
          console.error("Directions request failed due to " + status);
        }
      }
    );
  }

  // ? Start geolocation remotely
  async startGeolocationRemotely(): Promise<void> {
    try {
      const userData = await getDataFromLocalStorage<AuthResponseType>(LocalStorageKey.ZERO_30_USER);
      if (userData?.userId && userData?.role === Zero30Role.RIDER) {
        this.store.dispatch(RiderActions.GetRiderActivityHistoryInitiatedAction({ payload: { userId: userData.userId } }));
        const ongoingDeliveries$: Observable<RiderCartType[]> = this.store.select((data) => data.Rider.OngoingDeliveries);
        ongoingDeliveries$.subscribe((deliverydata) => {
          if (deliverydata.length > 0) {
            // ? Pass in the number of carts that are in transit, this data will be used to Push rider's location to server
            this.getRiderCurrentGeoPosition.emit({ Payload: deliverydata.length, UserId: userData.userId });
          }
          else {
            // ? Emit null if no deliveries are left for the rider
            this.getRiderCurrentGeoPosition.emit({ Payload: null, UserId: userData.userId });
          }
        });
      }
    }
    catch (ex) {
      throw ex;
    }
  }

  addMapMarker(
    map: google.maps.Map,
    locationInfo: string,
    title: string,
    position: LocationAbbrType,
    iconType: "RIDER" | "CUSTOMER"
  ): void {
    const infoWindow = new google.maps.InfoWindow({
      content: locationInfo,
    });

    const marker = new google.maps.Marker({
        position,
        icon: iconType === "RIDER"
        ? this.riderIcon.metadata
        : this.customerIcon.metadata,
        map,
        title,
    });

    marker.addListener("click", () => {
      infoWindow.open(map, marker);
    });
  }
}
