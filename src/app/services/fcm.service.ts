import { Injectable, NgZone } from "@angular/core";
import {
  Plugins,
  PushNotification,
  PushNotificationToken,
  PushNotificationActionPerformed,
  Capacitor,
  NotificationPermissionResponse
} from "@capacitor/core";
import { Router } from "@angular/router";
import { saveDataToLocalStorage } from "../../utils/functions/app.functions";
import {
  ExtraNotficationData,
  PushNotificationEvent,
  LocalStorageKey,
  PushNotificationCategory
} from "../../utils/types/app.constant";

const { PushNotifications } = Plugins;

@Injectable({
  providedIn: "root"
})
export class FcmService {

  constructor(
    private readonly router: Router,
    // tslint:disable-next-line: variable-name
    private readonly _ngZone: NgZone,
  ) {
    this.initPush();
  }

  private async initPush(): Promise<void> {
    if (Capacitor.platform !== "web") {
      await this.registerPush();
      console.log({  message: "FCM push notification was initialized successfully" });
    }
  }

  private async registerPush(): Promise<void> {
    const permission: NotificationPermissionResponse = await PushNotifications.requestPermission();
    if (permission.granted) {
      // Register with Apple / Google to receive push via APNS/FCM
      PushNotifications.register();
    } else {
      // No permission for push granted
    }

    // On success, we should be able to receive notifications
    PushNotifications.addListener(PushNotificationEvent.PUSH_REGISTRATION, (notification: PushNotificationToken) => {
      this.notificationRegistrationHandler(notification);
    });

    // Some issue with our setup and push will not work
    PushNotifications.addListener(PushNotificationEvent.PUSH_REGISTRATION_ERROR, (notification: Error) => {
      this.notificationErrorHandler(notification);
    });

    // Show us the notification payload if the app is open on our device
    PushNotifications.addListener(PushNotificationEvent.PUSH_NOTIFICATION_RECIEVED, (notification: PushNotification) => {
      this.notificationRecievedHandler(notification);
    });

    // Method called when tapping on a notification
    PushNotifications.addListener(PushNotificationEvent.PUSH_NOTIFICATION_ACTION, (notification: PushNotificationActionPerformed) => {
      this.notificationActionPerformedHandler(notification);
    });
  }

  private async notificationRegistrationHandler(token: PushNotificationToken)
  : Promise<void> {
    // ? Save the token to localStorage
    await saveDataToLocalStorage(token.value, LocalStorageKey.DEVICE_KEY);
  }

  private notificationErrorHandler(error: Error): void {
    console.log(`Error: ${JSON.stringify(error)}`);
  }

  private notificationRecievedHandler(notification: PushNotification): void {
    console.log(`Push received: ${JSON.stringify(notification)}`);
  }

  private notificationActionPerformedHandler(notification: PushNotificationActionPerformed): void {
    console.log(`Push action performed: ${JSON.stringify(notification.notification)}`);
    const { notification: { data: extraData } } = notification;
    this.navigationOptions(extraData);
  }

  private navigationOptions(data: ExtraNotficationData): void {
    if (data?.Id) {
      const { Id, PushNotificationType, SecondaryId } = data;
      if (PushNotificationType === PushNotificationCategory.CART_ASSIGNMENT) {
        this._ngZone.run(() => {
          this.router.navigate(["/rider", "order-info", Id]);
        });
      }
      if (PushNotificationType === PushNotificationCategory.CART_VERIFICATION) {
        this._ngZone.run(() => {
          this.router.navigate(["/user", "make-payment", Id]);
        });
      }
      if (PushNotificationType === PushNotificationCategory.PRODUCT_REMOVAL) {
        // ? Actions when a product is removed from the cart
      }
    }
  }

  resetBadgeCount() {
    PushNotifications.removeAllDeliveredNotifications();
  }
}
