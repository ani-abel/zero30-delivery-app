import { Injectable } from "@angular/core";
import { Location } from "@angular/common";
import { NavigationEnd, Router } from "@angular/router";
import { getDataFromLocalStorage, saveDataToLocalStorage } from "../../utils/functions/app.functions";
import { LocalStorageKey } from "../../utils/types/app.constant";

@Injectable({
  providedIn: "root"
})
export class NavigationService {
  private history: string[];

  constructor(
    private readonly location: Location,
    private readonly router: Router,
  ) {
    // setTimeout(async () => {
    //   this.history = await getDataFromLocalStorage<string[]>(LocalStorageKey.URL_HISTORY) || [];
    //   this.router.events.subscribe((event) => {
    //     if (event instanceof NavigationEnd) {
    //       // ? Clear the localStorage if the number os cached URLS exceed 10 (Avoid clutter in localStorage)
    //       this.history = this.clearNavigationHistory(this.history);
    //       this.history.push(event.urlAfterRedirects);
    //       saveDataToLocalStorage(this.history, LocalStorageKey.URL_HISTORY);
    //     }
    //   });
    // }, 1000);

    setTimeout(async () => {
      this.history = await getDataFromLocalStorage<string[]>(LocalStorageKey.URL_HISTORY) || [];
      this.router.events.subscribe((event) => {
        if (event instanceof NavigationEnd) {
          // ? Clear the localStorage if the number os cached URLS exceed 10 (Avoid clutter in localStorage)
          this.history = this.clearNavigationHistory(this.history);
          if (this.appendURLIfNotPopped(this.history, event.urlAfterRedirects)) {
            this.history.push(event.urlAfterRedirects);
            saveDataToLocalStorage(this.history, LocalStorageKey.URL_HISTORY);
          }
        }
      });
    }, 1000);
  }

  appendURLIfNotPopped(history: string[], newURL: string): boolean {
    let returnData = true;
    if (history?.length > 0) {
      if (history[history.length - 1] === newURL) {
        returnData = false;
      }
    }
    return returnData;
  }

  // ? This version works on the WEB by using the browser history & location APIs
  goBackOnWeb(): void {
    this.history.pop();
    if (this.history?.length > 0) {
      this.location.back();
    } else {
      this.router.navigate(["/"]);
    }
  }

  // ? This version works both on Web and native apps by using the localStorage API
  async goBackOnNative(): Promise<void> {
    const urlHistory: string[] = await getDataFromLocalStorage<string[]>(LocalStorageKey.URL_HISTORY);
    if (urlHistory?.length > 0) {
      const previousUrl: string = urlHistory[urlHistory.length - 2];
      console.log({ previousUrl });
      this.router.navigateByUrl(previousUrl);
    }
    else {
      this.router.navigate(["/"]);
    }
  }

  private clearNavigationHistory(history: string[]): string[] {
    if (history?.length > 10) {
      return [];
    } else {
      return [...history];
    }
  }
}
