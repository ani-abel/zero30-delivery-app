import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import {
  Plugins,
  LocalNotificationActionPerformed,
  LocalNotification,
  Capacitor,
  LocalNotificationScheduleResult,
} from "@capacitor/core";
import {
  ExtraNotficationData,
  PushNotificationCategory,
  NotificationPayloadType,
} from "../../utils/types/app.constant";

const { LocalNotifications } = Plugins;

@Injectable({
  providedIn: "root"
})
export class LocalNotificationService {

  constructor(
    private readonly router: Router,
  ) {
    this.initNotificationActions();
  }

  async initPush(data: NotificationPayloadType): Promise<void>{
    if (Capacitor.platform !== "web") {
      await this.sendLocalNotification(data);
    }
  }

  private async sendLocalNotification(payload: NotificationPayloadType)
  : Promise<void> {
    try {
      const { Title: title, Body: body, ExtraData: extra, Id: id } = payload;
      const notificationObject: LocalNotificationScheduleResult = await LocalNotifications.schedule({
        notifications: [
        {
          title,
          body,
          id,
          extra,
          iconColor: "#0000FF",
          sound: "ringer.wav",
          actionTypeId: "CONTROL_BUTTONS",
          schedule: {
            at: new Date(Date.now()),
          },
        }]
      });

      console.log({ message: `Local advanced notification was schduled for ${new Date().toDateString()}`, notificationObject });

      LocalNotifications.addListener("localNotificationReceived", (notification: LocalNotification) => {
        console.log({ mode: "Recieved", notification });
      });

      LocalNotifications.addListener("localNotificationActionPerformed", (notification: LocalNotificationActionPerformed) => {
        console.log({ mode: "Action Performed", notification });
        // tslint:disable-next-line: no-shadowed-variable
        const { actionId, notification: { extra } } = notification;
        if (actionId === "view") {
          this.navigationOptions(extra);
        }
      });
    }
    catch (ex) {
      throw ex;
    }
  }

  private initNotificationActions(): void {
    if (Capacitor.platform !== "web") {
      // ? Register local notificaion actions buttons
      LocalNotifications.registerActionTypes({
        types: [
          {
            id: "CONTROL_BUTTONS",
            actions: [
              {
                id: "view",
                title: "Open",
              },
              {
                id: "remove",
                title: "Dismiss",
                destructive: true
              },
            ]
          }]
        });
    }
  }

  private navigationOptions(data: ExtraNotficationData): void {
    if (data?.Id) {
      const { Id, PushNotificationType, SecondaryId } = data;
      if (PushNotificationType === PushNotificationCategory.CART_ASSIGNMENT) {
        this.router.navigate(["/rider", "order-info", Id]);
      }
      if (PushNotificationType === PushNotificationCategory.CART_VERIFICATION) {
        console.log({ purpose: PushNotificationCategory.CART_VERIFICATION });
        this.router.navigate(["/user", "make-payment", Id]);
      }
      if (PushNotificationType === PushNotificationCategory.PRODUCT_REMOVAL) {
        // ? Actions when a product is removed from the cart
      }
    }
  }
}
