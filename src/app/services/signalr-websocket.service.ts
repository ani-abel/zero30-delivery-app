import { Injectable, EventEmitter } from "@angular/core";
import * as signalR from "@microsoft/signalr";
import { CartConfirmationType } from "../user/store/model/user.model";
import { environment as env } from "../../environments/environment";
import { CartCurrentLocationData, RiderCartType } from "../rider/store/model/rider.model";

@Injectable({
  providedIn: "root"
})
export class SignalrWebsocketService {
  private thenAble: Promise<void>;
  private hubConnection: signalR.HubConnection;
  currentCartLocation: EventEmitter<CartCurrentLocationData> = new EventEmitter<CartCurrentLocationData>();
  cartAssignedToRider: EventEmitter<RiderCartType> = new EventEmitter<RiderCartType>();
  cartVerified: EventEmitter<CartConfirmationType> = new EventEmitter<CartConfirmationType>();

  constructor() { }

  public startConnection = () => {
    this.hubConnection = new signalR.HubConnectionBuilder()
                            .withUrl(env.websocketRoot)
                            .withAutomaticReconnect()
                            .configureLogging(signalR.LogLevel.Debug)
                            .build();

    this.hubConnection.keepAliveIntervalInMilliseconds = (1000 * 60) * 5; // 5 minutes
    this.hubConnection.serverTimeoutInMilliseconds = (1000 * 60) * 10; // 10 minutes
    this.thenAble = this.hubConnection.start();
    this.thenAble
        .then(() => {
          console.log("%cWebsocket connection started", "color: #2FA823; font-size: 13px;");
        })
        .catch((error) => {
          console.log(`%cError while starting connection: ${error}`, "color: red; font-size: 13px;");
        });
  }

  public confirmCartVerificationRequest = (): void => {
    try {
      this.hubConnection
        .on("ConfirmedCartVerification",
        (payload: CartConfirmationType) => {
          this.cartVerified.emit(payload);
        });
    }
    catch (ex) {
      console.error(ex);
    }
  }

  public cartAssignedToCartRider = (): void => {
    try {
      this.hubConnection.on("AssignedCart",
      (payload: RiderCartType) => {
        this.cartAssignedToRider.emit(payload);
      });
    }
    catch (ex) {
      throw ex;
    }
  }

  public makeCartVerificationRequest = (cartId: string): void => {
    this.thenAble.then(() => {
      this.hubConnection
        .invoke("CartVerificationRequest", cartId)
        .then(() => {
          console.log({ message: "Verification request was sent successfully" });
        });
    });
  }

  public findCurrentLocationOfCart = (): void => {
    try {
      this.hubConnection
          .on("CartLocation",
          (payload: CartCurrentLocationData) => {
            this.currentCartLocation.emit(payload);
          });
    }
    catch (ex) {
      throw ex;
    }
  }

  public joinGroupToViewCartLocation = (cartId: string): void => {
    try {
      this.thenAble.then(() => {
        this.hubConnection
            .invoke("CustomerAddToCartGroup", cartId)
            .then((payload: CartCurrentLocationData) => {
              console.log({ message: "Geolocation payload for cart can be gotten" });
              this.currentCartLocation.emit(payload);
            });
      });
    }
    catch (ex) {
      throw ex;
    }
  }

  public addRiderToGroup = (userId: string): void => {
    this.thenAble.then(() => {
      this.hubConnection
        .invoke("AddRiderToGroup", userId)
        .then(() => {
          console.log({ message: "Rider joined group connection" });
        });
    });
  }

  public reconnectToWebSocket = () => {
    this.hubConnection.onclose(() => {
      setTimeout(() => {
        this.startConnection();
      }, 5000);
    });
  }

  public disconnect() {
    try {
      this.hubConnection.stop();
    }
    catch (ex) {
      console.error(ex);
    }
  }

}
