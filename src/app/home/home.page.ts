import { Component, OnInit } from "@angular/core";
import { AuthService } from "../auth/auth.service";

import SwiperCore, { Navigation, Pagination, A11y, Autoplay } from "swiper/core";

// install Swiper components
SwiperCore.use([Navigation, Pagination, A11y, Autoplay]);

@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"],
})
export class HomePage implements OnInit {

  constructor(
    private readonly authSrv: AuthService,
  ) {}

  async ionViewWillEnter(): Promise<void> { 
    await this.authSrv.loginAsync();
  }

  async ngOnInit(): Promise<void> {
    await this.authSrv.loginAsync();
  }

  onSwiper(swiper) {
    console.log(swiper);
  }

  onSlideChange() {
    console.log("slide change");
  }

}
