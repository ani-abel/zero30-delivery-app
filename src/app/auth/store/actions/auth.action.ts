import {
    createAction,
    props
} from "@ngrx/store";
import { 
    SignUpType, 
    SignUpResponseType, 
    LoginType,
    AuthResponseType,
    SignUpOTPVerificationType,
    ChangePasswordType,
    FacebookAuthPayloadType,
    FacebookAuthResponseType,
    FacebookAccountUpdateType
} from '../model/auth.model';

export enum AuthActionType {
    CLEAR_ACTIVE_ERROR = "[ERROR_AUTH] CLEAR_ACTIVE_ERROR",
    CLEAR_ACTIVE_MESSAGE = "[ERROR_AUTH] CLEAR_ACTIVE_MESSAGE",

    LOGIN_INITIATED = "[AUTH] LOGIN_INITIATED",
    LOGIN_FAILED = "[AUTH] LOGIN_FAILED",
    LOGIN_SUCCESSFUL = "[AUTH] LOGIN_SUCCESSFUL",

    LOGOUT_INITIATED = "[AUTH] LOGOUT_INTIATED",
    LOGOUT_FAILED = "[AUTH] LOGOUT_FAILED",
    LOGOUT_SUCCESSFUL = "[AUTH] LOGOUT_SUCCESSFUL",

    SIGN_UP_INITIATED = "[AUTH] SIGN_UP_INITIATED",
    SIGN_UP_FAILED = "[AUTH] SIGN_UP_FAILED",
    SIGN_UP_SUCCESSFUL = "[AUTH] SIGN_UP_SUCCESSFUL",

    VERIFY_OTP_TOKEN_INITIATED = "[VERIFY_OTP] VERIFY_TOP_TOKEN_INTIATED",
    VERIFY_OTP_TOKEN_FAILED = "[VERIFY_OTP] VERIFY_OTP_TOKEN_FAILED",
    VERIFY_OTP_TOKEN_SUCCESSFUL = "[VERIFY_OTP] VERIFY_OTP_TOKEN_SUCCESSFUL",

    VERIFY_OTP_AFTER_SIGN_UP_INITIATED = "[SIGN_UP_VERIFICATION] VERIFY_OTP_AFTER_SIGN_UP_INITIATED",
    VERIFY_OTP_AFTER_SIGN_UP_FAILED = "[SIGN_UP_VERIFICATION] VERIFY_OTP_AFTER_SIGN_UP_FAILED",
    VERIFY_OTP_AFTER_SIGN_UP_SUCCESSFUL = "[SIGN_UP_VERIFICATION] VERIFY_OTP_AFTER_SIGN_UP_SUCCESSFUL",

    PASSWORD_RECOVERY_INITIATED = "[FORGOT_PASSWORD] PASSWORD_RECOVERY_INITIATED",
    PASSWORD_RECOVERY_FAILED = "[FORGOT_PASSWORD] PASSWORD_RECOVERY_FAILED",
    PASSWORD_RECOVERY_SUCCESSFUL = "[FORGOT_PASSWORD] PASSWORD_RECOVERY_SUCCESSFUL",

    CHANGE_PASSWORD_INTITIATED = "[CHANGE_PASSWORD] CHANGE_PASSWORD_INTITIATED",
    CHANGE_PASSWORD_FAILED = "[CHANGE_PASSWORD] CHANGE_PASSWORD_FAILED",
    CHANGE_PASSWORD_SUCCESSFUL = "[CHANGE_PASSWORD] CHANGE_PASSWORD_SUCCESSFUL",

    RESEND_OTP_TOKEN_INITIATED = "[RESEND_OTP] RESEND_OTP_TOKEN_INITIATED",
    RESEND_OTP_TOKEN_FAILED = "[RESEND_OTP] RESEND_OTP_TOKEN_FAILED",
    RESEND_OTP_TOKEN_SUCCESSFUL = "[RESEND_OTP] RESEND_OTP_TOKEN_SUCCESSFUL",

    FACEBOOK_LOGIN_INITIATED = "[FACEBOOK_LOGIN] FACEBOOK_LOGIN_INITIATED",
    FACEBOOK_LOGIN_FAILED = "[FACEBOOK_LOGIN] FACEBOOK_LOGIN_FAILED",
    FACEBOOK_LOGIN_SUCCESSFUL = "[FACEBOOK_LOGIN] FACEBOOK_LOGIN_SUCCESSFUL",

    UPDATE_FACEBOOK_USER_DETAILS_INITIATED = "[FACEBOOK_LOGIN] UPDATE_FACEBOOK_USER_DETAILS_INITIATED",
    UPDATE_FACEBOOK_USER_DETAILS_FAILED = "[FACEBOOK_LOGIN] UPDATE_FACEBOOK_USER_DETAILS_FAILED",
    UPDATE_FACEBOOK_USER_DETAILS_SUCCESSFUL = "[FACEBOOK_LOGIN] UPDATE_FACEBOOK_USER_DETAILS_SUCCESSFUL",

    SAVE_DEVICE_TOKEN_INITIATED = "[DEVICE_NOTIFICATION] SAVE_DEVICE_TOKEN_INITIATED",
    SAVE_DEVICE_TOKEN_FAILED = "[DEVICE_NOTIFICATION] SAVE_DEVICE_TOKEN_FAILED",
    SAVE_DEVICE_TOKEN_SUCCESSFUL = "[DEVICE_NOTIFICATION] SAVE_DEVICE_TOKEN_SUCCESSFUL",
}

export const actions = { 
    ClearActiveErrorAction: createAction(
        AuthActionType.CLEAR_ACTIVE_ERROR
    ),
    ClearActiveMessageAction: createAction(
        AuthActionType.CLEAR_ACTIVE_MESSAGE
    ),
    SignUpInitiatedAction: createAction(
        AuthActionType.SIGN_UP_INITIATED,
        props<{ payload: SignUpType }>()
    ),
    SignUpFailedAction: createAction(
        AuthActionType.SIGN_UP_FAILED,
        props<{ payload: Error }>()
    ),
    SignUpSuccessfulAction: createAction(
        AuthActionType.SIGN_UP_SUCCESSFUL,
        props<{ payload: SignUpResponseType }>()
    ),
    VerifyOTPAfterSignUpInitiatedAction: createAction(
        AuthActionType.VERIFY_OTP_AFTER_SIGN_UP_INITIATED,
        props<{ payload: SignUpOTPVerificationType  }>()
    ),
    VerifyOTPAfterSignUpFailedAction: createAction(
        AuthActionType.VERIFY_OTP_AFTER_SIGN_UP_FAILED,
        props<{ payload: Error }>()
    ),
    VerifyOTPAfterSignUpSuccessfulAction: createAction(
        AuthActionType.VERIFY_OTP_AFTER_SIGN_UP_SUCCESSFUL,
        props<{ payload: boolean }>()
    ),
    LoginInitiatedAction: createAction(
        AuthActionType.LOGIN_INITIATED,
        props<{ payload: LoginType }>()
    ),
    LoginFailedAction: createAction(
        AuthActionType.LOGIN_FAILED,
        props<{ payload: Error }>()
    ),
    LoginSuccessfulAction: createAction(
        AuthActionType.LOGIN_SUCCESSFUL,
        props<{ payload: AuthResponseType }>()
    ),
    LogoutInitiatedAction: createAction(
        AuthActionType.LOGOUT_INITIATED
    ),
    LogoutFailedAction: createAction(
        AuthActionType.LOGOUT_FAILED,
        props<{ payload: Error }>()
    ),
    LogoutSuccessfulAction: createAction(
        AuthActionType.LOGOUT_SUCCESSFUL
    ),
    VerifyOTPTokenInitiatedAction: createAction(
        AuthActionType.VERIFY_OTP_TOKEN_INITIATED,
        props<{ payload: SignUpOTPVerificationType }>()
    ),
    VerifyOTPTokenFailedAction: createAction(
        AuthActionType.VERIFY_OTP_TOKEN_FAILED,
        props<{ payload: Error }>()
    ),
    VerifyOTPTokenSuccessfulAction: createAction(
        AuthActionType.VERIFY_OTP_TOKEN_SUCCESSFUL,
        props<{ payload: boolean }>()
    ),
    PasswordRecoveryInitiatedAction: createAction(
        AuthActionType.PASSWORD_RECOVERY_INITIATED,
        props<{ payload: string }>()
    ),
    PasswordRecoveryFailedAction: createAction(
        AuthActionType.PASSWORD_RECOVERY_FAILED,
        props<{ payload: Error }>()
    ),
    PasswordRecoverySuccessfulAction: createAction(
        AuthActionType.PASSWORD_RECOVERY_SUCCESSFUL,
        props<{ payload: boolean }>()
    ),
    ChangePasswordInitiatedAction: createAction(
        AuthActionType.CHANGE_PASSWORD_INTITIATED,
        props<{ payload: ChangePasswordType }>()
    ),
    ChangePasswordFailedAction: createAction(
        AuthActionType.CHANGE_PASSWORD_FAILED,
        props<{ payload: Error }>()
    ),
    ChangePasswordSuccessfulAction: createAction(
        AuthActionType.CHANGE_PASSWORD_SUCCESSFUL,
        props<{ payload: AuthResponseType }>()
    ),
    ResendOTPInitiatedAction: createAction(
        AuthActionType.RESEND_OTP_TOKEN_INITIATED,
        props<{ payload: string }>()
    ),
    ResendOTPFailedAction: createAction(
        AuthActionType.RESEND_OTP_TOKEN_FAILED,
        props<{ payload: Error }>()
    ),
    ResendOTPSuccessfulAction: createAction(
        AuthActionType.RESEND_OTP_TOKEN_SUCCESSFUL,
        props<{ payload: boolean }>()
    ),
    FacebookLoginInitiatedAction: createAction(
        AuthActionType.FACEBOOK_LOGIN_INITIATED,
        props<{ payload: FacebookAuthPayloadType }>()
    ),
    FacebookLoginFailedAction: createAction(
        AuthActionType.FACEBOOK_LOGIN_FAILED,
        props<{ payload: Error }>()
    ),
    FacebookLoginSuccessfulAction: createAction(
        AuthActionType.FACEBOOK_LOGIN_SUCCESSFUL,
        props<{ payload: FacebookAuthResponseType }>()
    ),
    UpdateFacebookUserDetailsInitiatedAction: createAction(
        AuthActionType.UPDATE_FACEBOOK_USER_DETAILS_INITIATED,
        props<{ payload: FacebookAccountUpdateType }>()
    ),
    UpdateFacebookUserDetailsFailedAction: createAction(
        AuthActionType.UPDATE_FACEBOOK_USER_DETAILS_FAILED,
        props<{ payload: Error }>()
    ),
    UpdateFacebookUserDetailsSuccessfulAction: createAction(
        AuthActionType.UPDATE_FACEBOOK_USER_DETAILS_SUCCESSFUL,
        props<{ payload: FacebookAuthResponseType }>()
    ),
    SaveDeviceTokenInitiatedAction: createAction(
        AuthActionType.SAVE_DEVICE_TOKEN_INITIATED,
        props<{ payload: { userId: string, token: string } }>()
    ),
    SaveDeviceTokenFailedAction: createAction(
        AuthActionType.SAVE_DEVICE_TOKEN_FAILED,
        props<{ payload: Error }>()
    ),
    SaveDeviceTokenSuccessfulAction: createAction(
        AuthActionType.SAVE_DEVICE_TOKEN_SUCCESSFUL,
        props<{ payload: boolean }>()
    ),
}