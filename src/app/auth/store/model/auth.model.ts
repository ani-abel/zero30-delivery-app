export interface AuthState {
    ActiveError: Error;
    ActiveMessage: string;
    IsLoading: boolean;
    AuthenticatedUser: AuthResponseType;
    UserOTP: string;
    PhoneNumber: string;
}

export const InitialAuthState: AuthState = {
    ActiveError: undefined,
    ActiveMessage: undefined,
    IsLoading: false,
    AuthenticatedUser: undefined,
    UserOTP: undefined,
    PhoneNumber: undefined,
}

export interface SignUpResponseType {
    userId: string;
    username: string;
    role: number;
    firstName: string;
    lastName: string;
    phoneNo: string;
    address: string;
    dob: Date;
}

export interface SignUpType {
    personModel: PersonModel;
    userModel: UserModel;
    isCustomer: boolean;
    password: string;
}

export interface PersonModel {
    firstName: string;
    lastName: string;
    phoneNo: string;
    address: string;
    dob: Date;
}

export interface UserModel {
    username: string;//Email
}

export interface LoginType {
    username: string;
    password: string;
}

export interface AuthResponseType {
    userId: string;
    username: string;
    role: string;
    authToken: string;
}

export interface FacebookAuthPayloadType {
    facebookId: string;
    firstName: string;
    lastName?: string;
    imageUrl?: string;
    dob?: Date;
}

export interface FacebookAccountUpdateType {
    facebookId: string;
    email: string;
    phoneNo: string;
    address: string;
}

export interface FacebookAuthResponseType extends AuthResponseType {
    notExist: boolean;
}

export interface SignUpOTPVerificationType {
    PhoneNumber: string;
    token: string;
}

export interface ChangePasswordType {
    PhoneNo: string;
    Password: string;
}