import { createReducer, on } from '@ngrx/store';
import { APIMessage } from '../../../../utils/types/app.constant';
import { actions as AuthActions } from "../actions/auth.action";
import { InitialAuthState } from '../model/auth.model';

export const AuthReducer = createReducer(
    InitialAuthState,
    on(AuthActions.ClearActiveErrorAction, (state) => {
        return { ...state, ActiveError: undefined, ActiveMessage: undefined, IsLoading: false };
    }),
    on(AuthActions.ClearActiveMessageAction, (state) => {
        return { ...state, ActiveMessage: undefined, ActiveError: undefined, IsLoading: false };
    }),
    on(AuthActions.SignUpInitiatedAction, (state) => {
        return { ...state, IsLoading: true };
    }),
    on(AuthActions.SignUpFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(AuthActions.SignUpSuccessfulAction, (state, { payload }) => {
        return { 
            ...state, 
            IsLoading: false, 
            ActiveMessage: `${APIMessage.CREATED} successfully. Account needs to be verified` 
        };
    }),
    on(AuthActions.VerifyOTPAfterSignUpInitiatedAction, (state, { payload }) => {
        return { ...state, IsLoading: true };
    }),
    on(AuthActions.VerifyOTPAfterSignUpFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(AuthActions.VerifyOTPAfterSignUpSuccessfulAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveMessage: payload === true ? APIMessage.SUCCESSFUL : APIMessage.BAD_OPERATION };
    }),
    on(AuthActions.LoginInitiatedAction, (state, { payload }) => {
        return { ...state, IsLoading: true };
    }),
    on(AuthActions.LoginFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload }; 
    }),
    on(AuthActions.LoginSuccessfulAction, (state, { payload }) => {
        return { ...state, IsLoading: false, AuthenticatedUser: payload };
    }),
    on(AuthActions.LogoutInitiatedAction, (state) => {
        return { ...state, IsLoading: true };
    }),
    on(AuthActions.LogoutSuccessfulAction, (state) => {
        return { ...InitialAuthState };
    }),
    on(AuthActions.VerifyOTPTokenInitiatedAction, (state, { payload }) => {
        return { ...state, IsLoading: true, UserOTP: payload.token }; 
    }),
    on(AuthActions.VerifyOTPTokenFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(AuthActions.VerifyOTPTokenSuccessfulAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveMessage: payload ? APIMessage.VALIDATION_SUCCESSFUL : undefined };
    }),
    on(AuthActions.PasswordRecoveryInitiatedAction, (state, { payload }) => {
        return { ...state, IsLoading: true };
    }),
    on(AuthActions.PasswordRecoveryFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(AuthActions.PasswordRecoverySuccessfulAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveMessage: payload === true ? APIMessage.CREATED : undefined };
    }),
    on(AuthActions.ChangePasswordInitiatedAction, (state, { payload }) => {
        return { ...state, IsLoading: true };
    }),
    on(AuthActions.ChangePasswordFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(AuthActions.ChangePasswordSuccessfulAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveMessage: APIMessage.UPDATE };
    }),
    on(AuthActions.FacebookLoginInitiatedAction, (state, { payload }) => {
        return { ...state, IsLoading: true };
    }),
    on(AuthActions.FacebookLoginFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(AuthActions.FacebookLoginSuccessfulAction, (state, { payload }) => {
        return { 
            ...state, 
            IsLoading: false, 
            ActiveMessage: payload.notExist === true ? APIMessage.CREATED : undefined,
        };
    }),
    on(AuthActions.UpdateFacebookUserDetailsInitiatedAction, (state, { payload }) => {
        return { ...state, IsLoading: true };
    }),
    on(AuthActions.UpdateFacebookUserDetailsFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(AuthActions.UpdateFacebookUserDetailsSuccessfulAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveMessage: APIMessage.UPDATE };
    }),
    on(AuthActions.SaveDeviceTokenInitiatedAction, (state, { payload }) => {
        return { ...state, IsLoading: true, };
    }),
    on(AuthActions.SaveDeviceTokenFailedAction, (state, { payload }) => {
        return { ...state, IsLoading: false, ActiveError: payload };
    }),
    on(AuthActions.SaveDeviceTokenSuccessfulAction, (state, { payload }) => {
        return { ...state, IsLoading: false, };
    }),
);