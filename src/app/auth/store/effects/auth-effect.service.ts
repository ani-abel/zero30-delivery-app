import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import { getDataFromLocalStorage } from 'src/utils/functions/app.functions';
import { LocalStorageKey } from '../../../../utils/types/app.constant';
import { AuthService } from '../../auth.service';
import { actions as AuthActions } from "../actions/auth.action";
import { AuthHttpService } from './auth-http.service';

@Injectable({
  providedIn: 'root'
})
export class AuthEffectService {

  constructor(
    private readonly authHttpSrv: AuthHttpService,
    private readonly actions$: Actions,
    private readonly authSrv: AuthService,
    private readonly router: Router,
  ) { }

  signUpCustomer$ = createEffect(() => 
    this.actions$.pipe(
      ofType(AuthActions.SignUpInitiatedAction),
      switchMap((action) => {
        return this.authHttpSrv.signUpCustomer(action.payload).pipe(
          map((data) => AuthActions.SignUpSuccessfulAction({ payload: data })),
          catchError((error: Error) => of(AuthActions.SignUpFailedAction({ payload: error })))
        )
      })
  ));

  login$ = createEffect(() => 
    this.actions$.pipe(
      ofType(AuthActions.LoginInitiatedAction),
      switchMap((action) => {
        return this.authHttpSrv.login(action.payload).pipe(
          map((data) => AuthActions.LoginSuccessfulAction({ payload: data })),
          tap(async (data) => {
            if ("payload" in data) {
              //? Save the device token after successful login
              const deviceToken: string = await getDataFromLocalStorage(LocalStorageKey.DEVICE_KEY);
              if(deviceToken) {
                this.authSrv.registerDeviceToken(
                  deviceToken, 
                  data.payload.userId
                );
              }
              //? Navigate in authorized routes
              await this.authSrv.afterLogin(data.payload);
            }
          }),
          catchError((error: Error) => of(AuthActions.LoginFailedAction({ payload: error })))
        )
      })
  ));

  /**
   * Gets run after the user signs up and an OTP is sent to them,
   * this effects validates the OTP
   */
  verifyOTPAfterSignUp$ = createEffect(() => 
    this.actions$.pipe(
      ofType(AuthActions.VerifyOTPAfterSignUpInitiatedAction),
      switchMap((action) => {
        return this.authHttpSrv.verifyPhoneNumberAfterSignUp(action.payload).pipe(
          map((data) => AuthActions.VerifyOTPAfterSignUpSuccessfulAction({ payload: data })),
          tap((data) => {
            if(data.payload === true) {
              //? Run side-effect
              this.authSrv.afterSignUpOTPValidation();
            }
          }),
          catchError((error: Error) => of(AuthActions.VerifyOTPAfterSignUpFailedAction({ payload: error })))
        )
      })
  ));

  getVefiricationCodeAfterForgetPassword$ = createEffect(() => 
    this.actions$.pipe(
      ofType(AuthActions.PasswordRecoveryInitiatedAction),
      switchMap((action) => {
        return this.authHttpSrv.getVefiricationCodeAfterForgetPassword(action.payload).pipe(
          map((data) => AuthActions.PasswordRecoverySuccessfulAction({ payload: data })),
          tap((data) => {
            if(data.payload === true) {

            }
          }),
          catchError((error: Error) => of(AuthActions.PasswordRecoveryFailedAction({ payload: error })))
        )
      })
  ));

  logout$ = createEffect(() => 
    this.actions$.pipe(
      ofType(AuthActions.LogoutInitiatedAction),
      map(() => AuthActions.LogoutSuccessfulAction()),
      tap(async () => await this.authSrv.afterLogout())
  ));

  verifyOTP$ = createEffect(() => 
    this.actions$.pipe(
      ofType(AuthActions.VerifyOTPTokenInitiatedAction),
      switchMap((action) => {
        return this.authHttpSrv.verifyToken(action.payload).pipe(
          map((data) => AuthActions.VerifyOTPTokenSuccessfulAction({ payload: data })),
          tap((data) => {
            if(data.payload === true) {
              this.router.navigate(["/auth", "reset-password"]);
            }
          }),
          catchError((error: Error) => of(AuthActions.VerifyOTPTokenFailedAction({ payload: error })))
        )
      })
  ));

  changePassword$ = createEffect(() => 
    this.actions$.pipe(
      ofType(AuthActions.ChangePasswordInitiatedAction),
      switchMap((action) => {
        return this.authHttpSrv.changePassword(action.payload).pipe(
          map((data) => AuthActions.ChangePasswordSuccessfulAction({ payload: data })),
          tap((data) => {
            if(data.payload?.userId) {
              this.router.navigate(["/auth", "login"]);
            }
          }),
          catchError((error: Error) => of(AuthActions.ChangePasswordFailedAction({ payload: error })))
        )
      })
  ));

  facebookLogin$ = createEffect(() => 
    this.actions$.pipe(
      ofType(AuthActions.FacebookLoginInitiatedAction),
      switchMap((action) => {
        const { facebookId } = action.payload;
        return this.authHttpSrv.facebookLogin(action.payload).pipe(
          map((data) => AuthActions.FacebookLoginSuccessfulAction({ payload: data })),
          tap(async (data) => {
            const { payload: { notExist, userId } } = data;
            //? Save the device token after successful login
            const deviceToken: string = await getDataFromLocalStorage(LocalStorageKey.DEVICE_KEY);
            if(deviceToken) {
              this.authSrv.registerDeviceToken(
                deviceToken, 
                userId
              );
            }

            if(notExist === true) {
              this.router.navigate(["/auth", "update-facebook-user-detail", facebookId]);
            }
            else {
              const authPayload: any = { 
                ...data.payload, 
                notExist: undefined 
              };
              await this.authSrv.afterLogin(authPayload);
            }
          }),
          catchError((error: Error) => of(AuthActions.FacebookLoginFailedAction({ payload: error })))
        )
      })
    ));

  updateFacebookUserDetail$ = createEffect(() => 
    this.actions$.pipe(
      ofType(AuthActions.UpdateFacebookUserDetailsInitiatedAction),
      switchMap((action) => {
        return this.authHttpSrv.updateFacebookUserDetails(action.payload).pipe(
          map((data) => AuthActions.UpdateFacebookUserDetailsSuccessfulAction({ payload: data })),
          tap(async (data) => {
            const authPayload: any = { 
              ...data.payload, 
              notExist: undefined 
            };
            await this.authSrv.afterLogin(authPayload);
          }),
          catchError((error: Error) => of(AuthActions.UpdateFacebookUserDetailsFailedAction({ payload: error })))
        )
      })
    ));

  saveDeviceToken$ = createEffect(() => 
    this.actions$.pipe(
      ofType(AuthActions.SaveDeviceTokenInitiatedAction),
      switchMap((action) => {
        const { payload: { token, userId } } = action;
        return this.authHttpSrv.savePushNotificationToken(userId, token).pipe(
          map((data) => AuthActions.SaveDeviceTokenSuccessfulAction({ payload: data })),
          catchError((error: Error) => of(AuthActions.SaveDeviceTokenFailedAction({ payload: error })))
        )
      })
    ));
}
