import { Injectable } from '@angular/core';
import { HttpClient} from "@angular/common/http";
import { 
  AuthResponseType,
  SignUpResponseType, 
  LoginType, 
  SignUpType, 
  SignUpOTPVerificationType,
  ChangePasswordType,
  FacebookAuthPayloadType,
  FacebookAuthResponseType,
  FacebookAccountUpdateType
} from '../model/auth.model';
import { Observable } from 'rxjs';
import { throttleTime, retry } from "rxjs/operators";
import { environment as env } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthHttpService {

  constructor(private readonly httpClientSrv: HttpClient) { }

  signUpCustomer(payload: SignUpType): Observable<SignUpResponseType> {
    try {
      return this.httpClientSrv
                .post<SignUpResponseType>(`${env.apiRoot}/api/Account/SignUp`, payload)
                .pipe(
                  throttleTime(500),
                  retry(3)
                );
    }
    catch(ex) {
      throw ex;
    }
  }

  verifyPhoneNumberAfterSignUp(payload: SignUpOTPVerificationType)
    : Observable<boolean> {
    try {
      const { PhoneNumber, token } = payload;
      return this.httpClientSrv
                .post<boolean>(`${env.apiRoot}/api/Account/SignUpOTPVerifyToken?PhoneNumber=${PhoneNumber}&token=${token}`, {})
                .pipe(
                  throttleTime(500),
                  retry(3)
                );
    }
    catch(ex) {
      throw ex;
    }
  }

  login(payload: LoginType): Observable<AuthResponseType> {
    try {
      return this.httpClientSrv
                .post<AuthResponseType>(`${env.apiRoot}/api/Account/Login`, payload)
                .pipe(
                  throttleTime(500),
                  retry(3)
                );
    }
    catch(ex) {
      throw ex;
    }
  }

  getVefiricationCodeAfterForgetPassword(phoneNumber: string)
    : Observable<boolean> {
    try {
      return this.httpClientSrv
                .post<boolean>(`${env.apiRoot}/api/Account/OTPVerificationRequestWithoutUsername`, { phoneNumber })
                .pipe(
                  throttleTime(500),
                  retry(3)
                );
    }
    catch(ex) {
      throw ex;
    }
  }

  verifyToken(payload: SignUpOTPVerificationType)
    : Observable<boolean> {
    const { PhoneNumber, token } = payload;
    return this.httpClientSrv
              .post<boolean>(`${env.apiRoot}/api/Account/OTPVerifyToken?PhoneNumber=${PhoneNumber}&token=${token}`, {})
              .pipe(
                throttleTime(500),
                retry(3)
              );
  }

  changePassword(payload: ChangePasswordType)
    : Observable<AuthResponseType> {
    try {
      const { PhoneNo, Password } = payload;
      return this.httpClientSrv
                .post<AuthResponseType>(`${env.apiRoot}/api/Account/changeUserPassword?PhoneNo=${PhoneNo}&Password=${Password}`, {})
                .pipe(
                  throttleTime(500),
                  retry(3)
                );
    }
    catch(ex) {
      throw ex;
    }
  }

  facebookLogin(payload: FacebookAuthPayloadType)
  : Observable<FacebookAuthResponseType> {
    try {
      return this.httpClientSrv
                .post<FacebookAuthResponseType>(`${env.apiRoot}/api/Account/FacebookLogin`, payload)
                .pipe(
                  throttleTime(500),
                  retry(3)
                );
    }
    catch(ex) {
      throw ex;
    }
  }

  updateFacebookUserDetails(payload: FacebookAccountUpdateType)
  : Observable<FacebookAuthResponseType> {
    try {
      return this.httpClientSrv
                .post<FacebookAuthResponseType>(`${env.apiRoot}/api/Account/UpdateFacebookUser`, payload)
                .pipe(
                  throttleTime(500),
                  retry(3)
                );
    }
    catch(ex) {
      throw ex;
    }
  }

  savePushNotificationToken(userId: string, token: string)
  : Observable<boolean> {
    try {
      return this.httpClientSrv
                  .post<boolean>(`${env.apiRoot}/api/Shopping/SavePushNotificationToken?UserId=${userId}&Token=${token}`, {})
                  .pipe(
                    throttleTime(500),
                    retry(3)
                  );
    }
    catch(ex) {
      throw ex;
    }
  }

}