import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Store } from '@ngrx/store';
import { SubSink } from "subsink";
import { AppState } from '../../..//utils/types/app.model';
import { AuthService } from '../auth.service';
import { actions as AuthActions } from "../store/actions/auth.action";

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.page.html',
  styleUrls: ['./forgot-password.page.scss'],
})
export class ForgotPasswordPage implements OnInit {
  forgotPasswordForm: FormGroup;
  subSink: SubSink = new SubSink();

  constructor(
    private readonly store: Store<AppState>,
    private readonly authSrv: AuthService
  ) { }

  ngOnInit() {
    this.initForm();
  }

  initForm(): void {
    this.forgotPasswordForm = new FormGroup({
      PhoneNumber: new FormControl(null, Validators.compose([
        Validators.required
      ]))
    });
  }

  onSubmit(): void {
    if(this.forgotPasswordForm.invalid) {
      return;
    }
    const { PhoneNumber } = this.forgotPasswordForm.value;
    this.store.dispatch(AuthActions.PasswordRecoveryInitiatedAction({ payload: PhoneNumber })); 
    this.subSink.sink = 
    this.store.select((data) => data.Auth.ActiveMessage).subscribe((data) => {
      if(data) {
        this.authSrv.afterPasswordRecoveryIsInitiated(PhoneNumber);
      }
    });
  }

}
