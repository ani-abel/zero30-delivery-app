import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { EffectsModule } from "@ngrx/effects";
import { StoreModule } from "@ngrx/store";
import { SharedModule } from "../shared/shared.module";
import { AuthRoutingModule } from "./auth-routing.module";
import { AuthEffectService } from "./store/effects/auth-effect.service";
import { AuthReducer } from "./store/reducer/auth.reducer";

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SharedModule,
    AuthRoutingModule,
    EffectsModule.forFeature([AuthEffectService]),
    StoreModule.forFeature("Auth", AuthReducer)
  ],
  exports: [
    AuthRoutingModule,
  ]
})
export class AuthModule { }
