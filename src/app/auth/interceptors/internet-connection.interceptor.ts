import { Injectable } from "@angular/core";
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse
} from "@angular/common/http";
import { Observable, of } from "rxjs";
import { AuthService } from "../auth.service";

@Injectable()
export class InternetConnectionInterceptor implements HttpInterceptor {
    constructor(private readonly authSrv: AuthService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler)
  : Observable<HttpEvent<unknown>> {
      // ? Check for internet connection
      if (!navigator.onLine) {
        this.authSrv.openInternetConnectionStatusModal();
        const response = new HttpResponse({ status: 400, body: { Message: "No internet connection detected" } });
        return of(response);
      }
      return next.handle(request);
  }
}
