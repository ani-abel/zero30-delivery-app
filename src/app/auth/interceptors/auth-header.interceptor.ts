import { Injectable } from "@angular/core";
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from "@angular/common/http";
import { from, Observable } from "rxjs";
import { LocalStorageKey } from "../../../utils/types/app.constant";
import { getDataFromLocalStorage } from "../../../utils/functions/app.functions";
import { AuthResponseType } from "../store/model/auth.model";

@Injectable()
export class AuthHeaderInterceptor implements HttpInterceptor {

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    try {
      // convert promise to observable using 'from' operator
      return from(this.handle(request, next));
    }
    catch (ex) {
      throw ex;
    }
  }

  async handle(request: HttpRequest<any>, next: HttpHandler) {
    const authData: AuthResponseType = await getDataFromLocalStorage(LocalStorageKey.ZERO_30_USER);
    if (authData?.authToken) {
        const authRequest = request.clone({
          headers: request.headers.set("Authorization", `Bearer ${authData.authToken}`)
        });
        return next.handle(authRequest).toPromise();
    }
    return next.handle(request).toPromise();
  }
}
