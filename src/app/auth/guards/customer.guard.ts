import { Injectable } from "@angular/core";
import {
  Router,
  CanActivate,
  CanActivateChild,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree
} from "@angular/router";
import { Observable } from "rxjs";
import { AuthService } from "../auth.service";

@Injectable({
  providedIn: "root"
})
export class CustomerGuard implements
CanActivate,
CanActivateChild {
  constructor(
    private readonly authSrv: AuthService,
    private readonly router: Router
  ) { }

  async canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot)
        : Promise<boolean> {
      return await this.handleRouteActivation();
  }

  async canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot)
        : Promise<boolean> {
      return await this.handleRouteActivation();
  }

  private async handleRouteActivation(): Promise<boolean> {
    const isCustomer = await this.authSrv.isUserCustomer();
    if (isCustomer) {
      return true;
    } else {
      this.router.navigate(["/auth", "login"]);
      return false;
    }
  }

}
