import { Injectable } from "@angular/core";
import {
  Router,
  CanActivate,
  CanActivateChild,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from "@angular/router";
import { AuthService } from "../auth.service";

@Injectable({
  providedIn: "root"
})
export class RiderGuard implements
CanActivate,
CanActivateChild {
  constructor(
    private readonly authSrv: AuthService,
    private readonly router: Router
  ) { }

  async canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot)
        : Promise<boolean> {
      return await this.handleRouteActivation();
  }

  async canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot)
        : Promise<boolean> {
      return await this.handleRouteActivation();
  }

  private async handleRouteActivation(): Promise<boolean> {
    const isRider = await this.authSrv.isUserRider();
    if (isRider) {
      return true;
    } else {
      this.router.navigate(["/auth", "login"]);
      return false;
    }
  }

}
