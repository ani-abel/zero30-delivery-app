import { Injectable } from "@angular/core";
import {
  Router,
  CanActivate,
  CanActivateChild,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from "@angular/router";
import { AuthService } from "../auth.service";

@Injectable({
  providedIn: "root"
})
export class AuthGuard implements
CanActivate,
CanActivateChild {
  constructor(
    private readonly authSrv: AuthService,
    private readonly router: Router
  ) { }

  async canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot)
        : Promise<boolean> {
      return await this.handleRouteActivation();
  }

  async canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot)
        : Promise<boolean> {
      return await this.handleRouteActivation();
  }

  private async handleRouteActivation(): Promise<boolean> {
    const userToken = this.authSrv.isUserAuthenticated();
    if (userToken) {
      return this.authSrv.isUserAuthenticated();
    } else {
      this.router.navigate(["/auth", "login"]);
      return false;
    }
  }

}
