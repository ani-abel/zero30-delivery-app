import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Store } from '@ngrx/store';
import { ActivatedRoute, Router } from '@angular/router';
import { SubSink } from "subsink";
import { AppState } from '../../../utils/types/app.model';
import { actions as AuthActions } from "../store/actions/auth.action";
import { NumberTypeValidator } from '../../../utils/validators/form.validators';

@Component({
  selector: 'app-validate-otp',
  templateUrl: './validate-otp.page.html',
  styleUrls: ['./validate-otp.page.scss'],
})
export class ValidateOtpPage implements OnInit, OnDestroy {
  validateOTPForm: FormGroup;
  subSink: SubSink = new SubSink();
  phoneNumber: string;

  constructor(
    private readonly store: Store<AppState>,
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router
  ) { }

  ngOnInit() {
    this.initForm();
    this.subSink.sink = 
    this.activatedRoute
        .params
        .subscribe((data) => {
          this.phoneNumber = data.phoneNumber;
        });
  }

  resendCode(): void {
    if(this.phoneNumber) {
      this.store.dispatch(AuthActions.PasswordRecoveryInitiatedAction({ payload: this.phoneNumber }));
    }
  }

  onSubmit(): void {
    if(this.validateOTPForm.invalid) {
      return;
    }
    const { Digit1, Digit2, Digit3, Digit4 } = this.validateOTPForm.value;
    const fullOTPCode: string = `${Digit1}${Digit2}${Digit3}${Digit4}`;
    this.store.dispatch(AuthActions.VerifyOTPTokenInitiatedAction({ payload: { token: fullOTPCode, PhoneNumber: this.phoneNumber } }));
    this.subSink.sink = 
    this.store.select((data) => data.Auth.ActiveMessage).subscribe((data) => {
      if(data) {
        this.router.navigate(["/auth", "reset-password", this.phoneNumber]);
        this.validateOTPForm.reset();
      }
    })
  }

  initForm(): void {
    this.validateOTPForm = new FormGroup({
      Digit1: new FormControl(null, Validators.compose([
        Validators.required,
        NumberTypeValidator
      ])),
      Digit2: new FormControl(null, Validators.compose([
        Validators.required,
        NumberTypeValidator
      ])),
      Digit3: new FormControl(null, Validators.compose([
        Validators.required,
        NumberTypeValidator
      ])),
      Digit4: new FormControl(null, Validators.compose([
        Validators.required,
        NumberTypeValidator
      ]))
    })
  }

  ngOnDestroy(): void {
    this.subSink.unsubscribe();
  }

}
