import { Component, OnInit } from '@angular/core';
import { 
  FormGroup, 
  FormControl, 
  Validators 
} from "@angular/forms";
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AppState } from '../../../utils/types/app.model';
import { actions as AuthActions } from "../store/actions/auth.action";
import { AuthService } from '../auth.service';
import { FacebookAuthPayloadType } from '../store/model/auth.model';
import { UserHttpService } from '../../user/store/effects/user-http.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loginForm: FormGroup;
  activeError$: Observable<Error>;
  ratusRatus$: Observable<any>;

  constructor(
    private readonly store: Store<AppState>,
    private readonly authSrv: AuthService,
    private readonly userSrv: UserHttpService
  ) { 
    this.authSrv.setupFbLogin();
  }

  async facebookLogin(): Promise<void> {
    const facebookData = await this.authSrv.facebookLogin();
    if(facebookData?.id) {
      const [firstName, lastName] = (facebookData.name as string).split(" ");
      const { id: facebookId, birthday: dob } = facebookData;
      const imageUrl = facebookData.picture.data?.url;

      const requestPayload: FacebookAuthPayloadType = {
        facebookId,
        firstName,
        lastName,
        dob: new Date(),
        imageUrl
      };
      this.store.dispatch(AuthActions.FacebookLoginInitiatedAction({ payload: requestPayload }));
    }
  }

  async ionViewWillEnter(): Promise<void> { 
    await this.authSrv.loginAsync();
  }

  async ngOnInit(): Promise<void> {
    this.initForm();
    this.activeError$ = this.store.select((data) =>  data.Auth.ActiveError);
    await this.authSrv.loginAsync();
  }

  initForm(): void {
    this.loginForm = new FormGroup({
      Username: new FormControl(null, Validators.compose([
        Validators.required,
        Validators.email
      ])),
      Password: new FormControl(null, Validators.compose([
        Validators.required
      ]))
    });
  }

  revealPassword(passwordField): void {
    const passwordHtmlField: HTMLInputElement = (passwordField as HTMLInputElement);
    if(passwordHtmlField.type === "password") {
      passwordHtmlField.type = "text";
    }
    else {
      passwordHtmlField.type = "password";
    }
  }

  onSubmit(): void {
    if(this.loginForm.invalid) {
      return;
    }
    const { Username: username, Password: password } = this.loginForm.value;
    this.store.dispatch(AuthActions.LoginInitiatedAction({ payload: { username, password } }));
  }
}
