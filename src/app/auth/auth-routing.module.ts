import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
    { path: "", children: [
        { path: "", pathMatch: "full", redirectTo: "login" },
        {
          path: 'login',
          loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
        },
        {
          path: 'forgot-password',
          loadChildren: () => import('./forgot-password/forgot-password.module').then( m => m.ForgotPasswordPageModule)
        },
        {
          path: 'sign-up',
          loadChildren: () => import('./sign-up/sign-up.module').then( m => m.SignUpPageModule)
        },
        {
          path: "sign-up-otp-validation/:phoneNumber",
          loadChildren: () => import ("./sign-up-otp-verification/sign-up-otp-verification.module").then(m => m.SignUpOtpVerificationPageModule)
        },
        {
          path: 'validate-otp/:phoneNumber',
          loadChildren: () => import('./validate-otp/validate-otp.module').then( m => m.ValidateOtpPageModule)
        },
        {
          path: 'reset-password/:phoneNumber',
          loadChildren: () => import('./reset-password/reset-password.module').then( m => m.ResetPasswordPageModule)
        },
        {
          path: 'update-facebook-user-detail/:facebookId',
          loadChildren: () => import('./update-facebook-user-detail/update-facebook-user-detail.module').then( m => m.UpdateFacebookUserDetailPageModule)
        },
      ]},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AuthRoutingModule { }
