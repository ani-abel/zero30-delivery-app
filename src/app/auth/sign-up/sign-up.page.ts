import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators} from "@angular/forms";
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { SubSink } from "subsink";
import { AppState } from "../../../utils/types/app.model";
import { SignUpType } from '../store/model/auth.model';
import { actions as AuthActions } from "../store/actions/auth.action";

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.page.html',
  styleUrls: ['./sign-up.page.scss'],
})
export class SignUpPage implements OnInit {
  signUpForm: FormGroup;
  subSink: SubSink = new SubSink();
  singleDatePickerOptions: any = {
    enableMonthSelector: true,
    showMultipleYearsNavigation: true,
    showWeekNumbers: true
  };

  constructor(
    private readonly store: Store<AppState>,
    private readonly router: Router
  ) { }

  ngOnInit() {
    this.initForm();
  }

  initForm(): void {
    this.signUpForm = new FormGroup({
      FirstName: new FormControl(null, Validators.compose([
        Validators.required
      ])),
      LastName: new FormControl(null, Validators.compose([
        Validators.required
      ])),
      Email: new FormControl(null, Validators.compose([
        Validators.required,
        Validators.email
      ])),
      Password: new FormControl(null, Validators.compose([
        Validators.required
      ])),
      PhoneNumber: new FormControl(null, Validators.compose([
        Validators.required
      ])),
      Location: new FormControl(null, Validators.compose([
        Validators.required
      ])),
      DOB: new FormControl(null, Validators.compose([
        Validators.required
      ]))
    });
  }

  onChangeSingle(value: Date) {
    console.log(value);
  }

  onSubmit(): void {
    try {
      if(this.signUpForm.invalid) {
        return;
      }
      const { 
        FirstName, 
        LastName, 
        Email, 
        DOB, 
        Password, 
        PhoneNumber, 
        Location 
      } = this.signUpForm.value;

      const payload: SignUpType = {
        isCustomer: true,
        password: Password,
        personModel: {
          address: Location,
          dob: DOB,
          firstName: FirstName,
          lastName: LastName,
          phoneNo: PhoneNumber
        },
        userModel: {
          username: Email
        }
      };

      this.store.dispatch(AuthActions.SignUpInitiatedAction({ payload }));
      this.subSink.sink = 
      this.store
          .select((data) => data.Auth.ActiveMessage)
          .subscribe((data) => {
            if(data) {
              this.signUpForm.reset();
              this.router.navigate(["/auth", "sign-up-otp-validation", PhoneNumber]);
            }
          });
    }
    catch(ex) {
      throw ex;
    }
  }

  // onSubmit(): void {
  //   try {
  //     if(this.signUpForm.invalid) {
  //       return;
  //     }
  //     const { FullName, Email, DOB, Password, PhoneNumber, Location } = this.signUpForm.value;
  //     const [firstName, lastName] = (FullName as string).split(" ");

  //     const payload: SignUpType = {
  //       isCustomer: true,
  //       password: Password,
  //       personModel: {
  //         address: Location,
  //         dob: DOB,
  //         firstName,
  //         lastName: lastName || "",
  //         phoneNo: PhoneNumber
  //       },
  //       userModel: {
  //         username: Email
  //       }
  //     };

  //     this.store.dispatch(AuthActions.SignUpInitiatedAction({ payload }));
  //     this.subSink.sink = 
  //     this.store
  //         .select((data) => data.Auth.ActiveMessage)
  //         .subscribe((data) => {
  //           if(data) {
  //             this.signUpForm.reset();
  //             this.router.navigate(["/auth", "sign-up-otp-validation", PhoneNumber]);
  //           }
  //         });
  //   }
  //   catch(ex) {
  //     throw ex;
  //   }
  // }

}
