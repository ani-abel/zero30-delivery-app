import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { Store } from '@ngrx/store';
import { SubSink } from 'subsink';
import { AppState } from '../../..//utils/types/app.model';
import { actions as AuthActions } from "../store/actions/auth.action";

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.page.html',
  styleUrls: ['./reset-password.page.scss'],
})
export class ResetPasswordPage implements OnInit {
  resetPasswordForm: FormGroup;
  phoneNumber: string;
  subSink: SubSink = new SubSink();

  constructor(
    private readonly store: Store<AppState>,
    private readonly activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.initForm();
    this.subSink.sink = 
    this.activatedRoute
        .params
        .subscribe((data) => {
          this.phoneNumber = data.phoneNumber;
        });
  }

  initForm(): void {
    this.resetPasswordForm = new FormGroup({
      Password: new FormControl(null, Validators.compose([
        Validators.required
      ])),
      ConfirmPassword: new FormControl(null, Validators.compose([
        Validators.required
      ]))
    })
  }

  comparePasswords(): void {
    if(this.resetPasswordForm.invalid) {
      return;
    }
    const { Password, ConfirmPassword } = this.resetPasswordForm.value;
    if(Password !== ConfirmPassword) {
      this.resetPasswordForm.get("ConfirmPassword").setErrors({
        wrongMatch: true
      });
    }
  }

  onSubmit(): void {
    if(this.resetPasswordForm.invalid) {
      return;
    }
    const { Password } = this.resetPasswordForm.value;
    this.store.dispatch(AuthActions.ChangePasswordInitiatedAction({ payload: { PhoneNo: this.phoneNumber, Password  } }));
    this.subSink.sink = 
    this.store.select((data) => data.Auth.ActiveMessage).subscribe((data) => {
      this.resetPasswordForm.reset();
    })
  }
}
