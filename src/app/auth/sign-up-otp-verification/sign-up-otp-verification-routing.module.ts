import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SignUpOtpVerificationPage } from './sign-up-otp-verification.page';

const routes: Routes = [
  {
    path: '',
    component: SignUpOtpVerificationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SignUpOtpVerificationPageRoutingModule {}
