import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { SubSink } from 'subsink';
import { AppState } from '../../..//utils/types/app.model';
import { NumberTypeValidator } from '../../../utils/validators/form.validators';
import { actions as AuthActions } from "../store/actions/auth.action";

@Component({
  selector: 'app-sign-up-otp-verification',
  templateUrl: './sign-up-otp-verification.page.html',
  styleUrls: ['./sign-up-otp-verification.page.scss'],
})
export class SignUpOtpVerificationPage implements OnInit, OnDestroy {
  validateOTPForm: FormGroup;
  subSink: SubSink = new SubSink();
  phoneNumber: string;

  constructor(
    private readonly store: Store<AppState>,
    private readonly activatedRoute: ActivatedRoute,
    private readonly router: Router
  ) { }

  ngOnInit() {
    this.initForm();
    this.subSink.sink = 
    this.activatedRoute
        .params
        .subscribe((data) => {
          this.phoneNumber = data?.phoneNumber;
        });
  }

  resendOTP(): void {
    if(this.phoneNumber) {
      this.store.dispatch(AuthActions.PasswordRecoveryInitiatedAction({ payload: this.phoneNumber }));
    }
  }

  onSubmit(): void {
    if(this.validateOTPForm.invalid) {
      return;
    }
    const { Digit1, Digit2, Digit3, Digit4 } = this.validateOTPForm.value;
    const fullOTPCode: string = `${Digit1}${Digit2}${Digit3}${Digit4}`;
    this.store.dispatch(AuthActions.VerifyOTPAfterSignUpInitiatedAction({ payload: { token: fullOTPCode, PhoneNumber: this.phoneNumber } }));
    this.subSink.sink = 
    this.store.select((data) => data.Auth.ActiveMessage).subscribe((data) => {
      if(data) {
        this.router.navigate(["/auth", "login"]);
        this.validateOTPForm.reset();
      }
    })
  }

  initForm(): void {
    this.validateOTPForm = new FormGroup({
      Digit1: new FormControl(null, Validators.compose([
        Validators.required,
        NumberTypeValidator
      ])),
      Digit2: new FormControl(null, Validators.compose([
        Validators.required,
        NumberTypeValidator
      ])),
      Digit3: new FormControl(null, Validators.compose([
        Validators.required,
        NumberTypeValidator
      ])),
      Digit4: new FormControl(null, Validators.compose([
        Validators.required,
        NumberTypeValidator
      ]))
    })
  }

  hightlightNextField(nextField: HTMLInputElement): void {
    nextField.focus();
  }

  ngOnDestroy(): void {
    this.subSink.unsubscribe();
  }

}
