import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SignUpOtpVerificationPageRoutingModule } from './sign-up-otp-verification-routing.module';

import { SignUpOtpVerificationPage } from './sign-up-otp-verification.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SignUpOtpVerificationPageRoutingModule,
    SharedModule,
  ],
  declarations: [SignUpOtpVerificationPage]
})
export class SignUpOtpVerificationPageModule {}
