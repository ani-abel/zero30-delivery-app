import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { FacebookLoginPlugin } from "@capacitor-community/facebook-login";
import { Plugins, registerWebPlugin } from "@capacitor/core";
import { FacebookLogin } from "@capacitor-community/facebook-login";
import { HttpClient } from "@angular/common/http";
import { isPlatform, ModalController } from "@ionic/angular";
import { Store } from "@ngrx/store";
import {
    saveDataToLocalStorage,
    deleteDataFromLocalStorage,
    getDataFromLocalStorage
} from "../../utils/functions/app.functions";
import { LocalStorageKey, Zero30Role } from "../../utils/types/app.constant";
import { AuthResponseType } from "./store/model/auth.model";
import { NoInternetModalComponent } from "../shared/no-internet-modal/no-internet-modal.component";
import { AppState } from "../../utils/types/app.model";
import { actions as AuthAction } from "./store/actions/auth.action";

registerWebPlugin(FacebookLogin);

@Injectable({
  providedIn: "root"
})
export class AuthService {
  fbLogin: FacebookLoginPlugin;
  fbToken = null;

  constructor(
    private readonly store: Store<AppState>,
    private readonly router: Router,
    private readonly httpClient: HttpClient,
    private readonly modalController: ModalController
  ) { }

  // ? Open on internet modal
  async openInternetConnectionStatusModal(): Promise<any> {
    const modal = await this.modalController.create({
      component: NoInternetModalComponent,
    });

      // ? Open
    await modal.present();

    setTimeout(async () => {
        // ? Close in 10 seconds
        await modal.dismiss();
      }, 5000);
    }

    registerDeviceToken(deviceToken: string, userId: string): void {
      this.store.dispatch(AuthAction.SaveDeviceTokenInitiatedAction({ payload: { token: deviceToken, userId } }));
    }

    // ? FB login (Begins)
    async setupFbLogin() {
        if (isPlatform("desktop")) {
          this.fbLogin = FacebookLogin;
        } else {
          // Use the native implementation inside a real app!
          // tslint:disable-next-line: no-shadowed-variable
          const { FacebookLogin } = Plugins;
          this.fbLogin = FacebookLogin;
        }
    }

    async facebookLogin() {
        // const FACEBOOK_PERMISSIONS = ['email', 'user_birthday'];
        const FACEBOOK_PERMISSIONS = ["email"];
        const result = await this.fbLogin.login({ permissions: FACEBOOK_PERMISSIONS });

        if (result.accessToken && result.accessToken.userId) {
          this.fbToken = result.accessToken;
          return await this.loadUserData();
        } else if (result.accessToken && !result.accessToken.userId) {
          // Web only gets the token but not the user ID
          // Directly call get token to retrieve it now
          return await this.getCurrentToken();
        } else {
          // Login failed
        }
    }

    async getCurrentToken(): Promise<any> {
        const result = await this.fbLogin.getCurrentAccessToken();

        if (result.accessToken) {
          this.fbToken = result.accessToken;
          return await this.loadUserData();
        } else {
          // Not logged in.
        }
    }

    async getUserJWTToken(): Promise<string> {
      const userData: AuthResponseType = await getDataFromLocalStorage(LocalStorageKey.ZERO_30_USER);
      return userData?.authToken;
    }

    async loadUserData(): Promise<any> {
      const url = `https://graph.facebook.com/${this.fbToken.userId}?fields=id,name,picture.width(720),birthday,email&access_token=${this.fbToken.token}`;
      const userData = await this.httpClient.get<any>(url).toPromise();
      return userData;
    }
    // ? FB login (Ends)

    async afterLogin(authPayload: AuthResponseType): Promise<void> {
        try {
            if (authPayload?.role) {
                await saveDataToLocalStorage(authPayload, LocalStorageKey.ZERO_30_USER);
                if (authPayload.role === Zero30Role.CUSTOMER) {
                    this.router.navigate(["/user"]);
                }
                if (authPayload.role === Zero30Role.RIDER) {
                    this.router.navigate(["/rider"]);
                }
            }
        }
        catch (ex) {
          throw ex;
        }
    }

    afterSignUpOTPValidation(): void {
      this.router.navigate(["/auth", "login"]);
    }

    async afterLogout(): Promise<void> {
        try {
          await deleteDataFromLocalStorage(LocalStorageKey.ZERO_30_USER);
          await deleteDataFromLocalStorage(LocalStorageKey.BOOKMARKS);
          await deleteDataFromLocalStorage(LocalStorageKey.URL_HISTORY);
          this.router.navigate(["/auth", "login"]);
        }
        catch (ex) {
          throw ex;
        }
    }

    afterPasswordRecoveryIsInitiated(phoneNumber: string): void {
      this.router.navigate(["/auth", "validate-otp", phoneNumber]);
    }

    async isUserCustomer(): Promise<boolean> {
        let isUserCustomer = true;
        const authData: AuthResponseType = await getDataFromLocalStorage(LocalStorageKey.ZERO_30_USER);
        if (!(authData?.role && authData.role === Zero30Role.CUSTOMER)) {
          isUserCustomer = false;
        }
        return isUserCustomer;
    }

    async isUserRider(): Promise<boolean> {
        let isUserRider = true;
        const authData: AuthResponseType = await getDataFromLocalStorage(LocalStorageKey.ZERO_30_USER);
        if (!(authData?.role && authData.role === Zero30Role.RIDER)) {
          isUserRider = false;
        }
        return isUserRider;
    }

    async isUserAuthenticated(): Promise<boolean> {
      let isUserRider = true;
      const authData: AuthResponseType = await getDataFromLocalStorage(LocalStorageKey.ZERO_30_USER);
      if (!authData?.authToken) {
        isUserRider = false;
      }
      return isUserRider;
  }

  async loginAsync(): Promise<void> {
    try {
      // ? check for token in localstorage
      const storageData: AuthResponseType = await getDataFromLocalStorage<AuthResponseType>(LocalStorageKey.ZERO_30_USER);

      if (storageData?.userId && storageData?.authToken) {
        const { role } = storageData;
        // const lastUrlVisited: string = await this.getLastExistingRoute();
        // if (lastUrlVisited?.includes(role.toLowerCase())) {
        //   console.log({ message: "Last url was used", lastUrlVisited });
        //   this.router.navigateByUrl(lastUrlVisited);
        // }
        // ? if exists check for role & navigate
        if (role === Zero30Role.RIDER) {
          this.router.navigate(["/rider"]);
          }
        if (role === Zero30Role.CUSTOMER) {
          this.router.navigate(["/user"]);
        }
      }
    }
    catch (ex) {
      throw ex;
    }
  }

    // async loginAsync(): Promise<void> {
    //   try {
    //     // ? check for token in localstorage
    //     const storageData: AuthResponseType = await getDataFromLocalStorage<AuthResponseType>(LocalStorageKey.ZERO_30_USER);

    //     if (storageData?.userId && storageData?.authToken) {
    //       const { role } = storageData;
    //       const lastUrlVisited: string = await this.getLastExistingRoute();
    //       if (lastUrlVisited) {
    //         this.router.navigateByUrl(lastUrlVisited);
    //       } else {
    //         console.log({ lastUrl: "Was not found" });
    //         // ? if exists check for role & navigate
    //         if (role === Zero30Role.RIDER) {
    //           this.router.navigate(["/rider"]);
    //         }
    //         if (role === Zero30Role.CUSTOMER) {
    //           this.router.navigate(["/user"]);
    //         }
    //       }
    //     }
    //   }
    //   catch (ex) {
    //     throw ex;
    //   }
    // }

    // async loginAsync(): Promise<void> {
    //   try {
    //     // ? check for token in localstorage
    //     const storageData: AuthResponseType = await getDataFromLocalStorage<AuthResponseType>(LocalStorageKey.ZERO_30_USER);
    //     if (storageData?.userId && storageData?.authToken) {
    //       const { role } = storageData;
    //       // ? if exists check for role & navigate
    //       if (role === Zero30Role.RIDER) {
    //         this.router.navigate(["/rider"]);
    //       }
    //       if (role === Zero30Role.CUSTOMER) {
    //         this.router.navigate(["/user"]);
    //       }
    //     }
    //   }
    //   catch (ex) {
    //     throw ex;
    //   }
    // }

    private async getLastExistingRoute(): Promise<string> {
      const urls: string[] = await getDataFromLocalStorage<string[]>(LocalStorageKey.URL_HISTORY);
      if (urls?.length > 0) {
        return urls[urls.length - 2];
      }
    }
}
