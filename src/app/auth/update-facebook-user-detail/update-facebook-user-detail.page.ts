import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { Store } from '@ngrx/store';
import { SubSink } from 'subsink';
import { AppState } from '../../../utils/types/app.model';
import { actions as AuthActions } from "../store/actions/auth.action";

@Component({
  selector: 'app-update-facebook-user-detail',
  templateUrl: './update-facebook-user-detail.page.html',
  styleUrls: ['./update-facebook-user-detail.page.scss'],
})
export class UpdateFacebookUserDetailPage implements OnInit, OnDestroy {
  updateFacebookDataForm: FormGroup;
  facebookId: string;
  subSink: SubSink = new SubSink();

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly store: Store<AppState>
  ) { }

  ngOnInit() {
    this.initForm();
    this.subSink.sink = 
    this.activatedRoute.params.subscribe((data) => {
      this.facebookId = data.facebookId;
    });
  }

  initForm(): void {
    this.updateFacebookDataForm = new FormGroup({
      Location: new FormControl(null, Validators.compose([
        Validators.required
      ])),
      Email: new FormControl(null, Validators.compose([
        Validators.required
      ])),
      PhoneNumber: new FormControl(null, Validators.compose([
        Validators.required
      ])),
    })
  }

  onSubmit(): void {
    if(this.updateFacebookDataForm.invalid) {
      return;
    }
    if(this.facebookId) {
      const { 
        Location: address, 
        Email: email, 
        PhoneNumber: phoneNo 
      } = this.updateFacebookDataForm.value;
      
      this.store.dispatch(AuthActions.UpdateFacebookUserDetailsInitiatedAction({ payload: { 
          address, 
          email, 
          phoneNo, 
          facebookId: this.facebookId 
        } 
      }));
      
      this.subSink.sink =
      this.store
          .select((data) => data.User.ActiveMessage)
          .subscribe((data) => {
            if(data) {
              this.updateFacebookDataForm.reset();
            }
      });
    }
  }

  ngOnDestroy(): void {
    this.subSink.unsubscribe();
  }
}