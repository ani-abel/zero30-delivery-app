import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UpdateFacebookUserDetailPage } from './update-facebook-user-detail.page';

const routes: Routes = [
  {
    path: '',
    component: UpdateFacebookUserDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UpdateFacebookUserDetailPageRoutingModule {}
