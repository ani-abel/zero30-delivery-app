import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UpdateFacebookUserDetailPageRoutingModule } from './update-facebook-user-detail-routing.module';

import { UpdateFacebookUserDetailPage } from './update-facebook-user-detail.page';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    UpdateFacebookUserDetailPageRoutingModule
  ],
  declarations: [UpdateFacebookUserDetailPage]
})
export class UpdateFacebookUserDetailPageModule {}
